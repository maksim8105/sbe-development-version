<?
## v5.25 -> jun. 26, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php?auctionid=".$_REQUEST['id']."'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");


$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['auctionid']."' AND deleted!=1");
$isAuction = getSqlNumber("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['auctionid']."' AND deleted!=1");

if ($setts['buyout_process']==1 && $auction['bn']=="Y" && $auction['closed']==0 && $auction['active']==1 && $isAuction && $auction['ownerid']!=$_SESSION['memberid']) { 
	header5(strtoupper($lang[makeoffer])); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="3" class="subitem">
   <tr class="contentfont">
      <td width="1%"><? 
			if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { 
		  		echo "<img src='themes/".$setts['default_theme']."/img/system/status1.gif' vspace=5>";
	  		} else { 
				echo "<img src='themes/".$setts['default_theme']."/img/system/status.gif' vspace=5>"; 
			} ?>
      </td>
      <td><? if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { ?>
         <?=$lang[welcome];?>
         , <br>
         <b><? echo $_SESSION['membername']; ?></b>
         <? } else { echo $lang[status_bidder_seller]; }?>
      </td>
      <td class="contentfont" width="50%" align="right">>> <a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']));?>">
         <?=$lang[retdetailspage];?>
         </a>&nbsp;&nbsp;</td>
   </tr>
</table>
<?
$isBanned = getSqlNumber("SELECT id FROM probid_blocked_users WHERE userid='".$_SESSION['memberid']."' AND ownerid='".$auction['ownerid']."'");
				
if ($isBanned) {
	$bannedDetails = getSqlRow("SELECT * FROM probid_blocked_users WHERE userid='".$_SESSION['memberid']."' AND ownerid='".$auctionOwner."'");
	echo '<br><table width="100%" border="0" cellspacing="2" cellpadding="2" class="errormessage"> ';
	echo '	<tr> ';
	echo '		<td>'.(($bannedDetails['show_reason']) ? "<strong>".$lang[excludedmsg_reason]."</strong><br>".$bannedDetails['block_reason'] : $lang[excludedmsg_noreason]).'</td> ';
	echo '	</tr> ';
	echo '</table> ';
} else {	
	
	$showForm = TRUE;
	$canOffer = TRUE;
	
	$isSameOffer = getSqlNumber("SELECT id FROM probid_auction_offers WHERE auctionid='".$_POST['auctionid']."' AND 
	buyerid='".$_SESSION['memberid']."' AND amount='".$_POST['amount']."'");

	if ($auction['quantity']<$_POST['quantity']) {
		echo "<p align=center>$lang[offermoreitemsthanavail]</p>";
		$canOffer = FALSE;
	} else if ($_POST['amount']<$auction['offer_range_min']||($_POST['amount']>$auction['offer_range_max']&&$auction['offer_range_max']>0)) {
		if (!$_REQUEST['noerror']) echo "<p align=center>$lang[offernotinrange]</p>"; ## doesnt display if we come directly from the link in itemPics()
		$canOffer = FALSE;
	} else if ($isSameOffer) {
		echo "<p align=center>$lang[alrdyofferedthisamount]</p>";
		$canOffer = FALSE;
	}

	if (isset($_POST['mook']) && $canOffer) {
		#### send mail to seller
		if ($auction['auctiontype']=="dutch") $quantity = ($_POST['quantity']<=0) ? 1 : $_POST['quantity'];
		else $quantity = 1;
			
		$expires = time() + $_POST['expires'] * 86400;
		
		$insertOfferRequest = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_offers
		(auctionid, sellerid, buyerid, quantity, amount, expires) VALUES 
		('".$_POST['auctionid']."', '".$auction['ownerid']."', '".$_SESSION['memberid']."', 
		'".$quantity."', '".$_POST['amount']."', '".$expires."')");
				
		echo "<br><div align=\"center\" class=\"contentfont\">$lang[offersentsuccess]<br><br>								\n";
		echo "<a href=\"".processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">$lang[retdetailspage]</a></div><br><br>			\n";
	
		$sellerId = $auction['ownerid'];
		$buyerId = $_SESSION['memberid'];
		$auctionId = $_POST['auctionid'];
						
		include ("mails/notifyselleroffer.php");
		$showForm = FALSE;
	} 
	
	if ($showForm) { ?>
<form action="makeoffer.php" method="post">
   <input type="hidden" name="auctionid" value="<?=$auction['id'];?>">
   <table width="100%" border="0" cellpadding="4" cellspacing="2">
      <tr class="c1">
         <td align="right" width="150"><b>
            <?=$lang[makeoffer]?>
            :</b></td>
         <td width="100%"><?=$auction['itemname'];?></td>
      </tr>
      <tr class="c5">
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" height="1" width="150" /></td>
         <td></td>
      </tr>
      <tr class="c3">
         <td align="right"><b>
            <?=$lang[currbid]?>
            :</b></td>
         <td><? echo displayAmount($auction['maxbid'],$auction['currency']);?></td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$lang[num_bids]?>
            :</b></td>
         <td><?=$auction['nrbids'];?>
         </td>
      </tr>
      <? if ($auction['auctiontype']=="dutch") { ?>
      <tr class="c3">
         <td align="right"><b>
            <?=$lang[quant]?>
            :</b></td>
         <td><input name="quantity" type="text" class="contentfont" id="quantity" value="1" size="10" <? echo ($auction['auctiontype']=="standard") ? "readonly" : ""; ?> /></td>
      </tr>
      <? } else { ?>
      <input name="quantity" type="hidden" id="quantity" value="1" />
      <? } ?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[offerrange];?>
            </strong> :</td>
         <td><?=offerRange($auction['offer_range_min'], $auction['offer_range_max'], $auction['currency']);?></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[makeoffer]?>
            </strong> :</td>
         <td><?=$auction['currency'];?>
            <input name="amount" type="text" class="contentfont" id="amount" value="<?=$_REQUEST['amount'];?>" size="7"></td>
      </tr>
      <tr class="c3" valign="top">
         <td align="right"><b>
            <?=$lang[shipping]?>
            :</b></td>
         <td><? 
				if ($auction['sc']=="BP") echo "".$lang[buyerpaysshipment].",&nbsp";
				else echo "".$lang[sellerpaysshipment].",&nbsp";
				if ($auction['scint']=="Y") echo "$lang[sellershipinternat]"; 
				else echo "$lang[seller_not_shipinternat]";  
				if ($setts['shipping_costs']==1) {
					echo "<br><strong>".$lang[postagecosts]." :</strong> ".
					$auction['currency']." ".$auction['postage_costs'].", <strong>".$lang[insurance]." :</strong> ".
      			$auction['insurance'].", <strong>".$lang[servicetype]." :</strong> ".$auction['type_service'];
	  			} ?>
         </td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><b>
            <?=$lang[payment]?>
            :</b></td>
         <td><? if ($auctionDetails['acceptdirectpayment']) { ?>
            <strong>
            <?=$lang[sellerpreferspaypal];?>
            </strong><br />
            <img src="http://images.paypal.com/images/x-click-but02.gif" align="absmiddle" /> <img src="themes/<?=$setts['default_theme'];?>/img/system/cards.gif" align="absmiddle" /><br />
            <? } 
				$paymentMethods=explode("<br>",addSpecialChars($auction['pm']));
				$nbPaymentMethods=count($paymentMethods); ?>
            <table border="0" cellspacing="4" cellpadding="4">
               <? 
					for ($i=0;$i<$nbPaymentMethods;$i+=4) { 
						$j=$i+1;	$k=$i+2;	$l=$i+3; ?>
               <tr valign="top" align="center" style="font: bold;">
                  <td><? 
							$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$i]."'","logourl"); 
							echo $paymentMethods[$i].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
                  <td><? 
							$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$j]."'","logourl");
							echo $paymentMethods[$j].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
                  <td><? 
							$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$k]."'","logourl");
							echo $paymentMethods[$k].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
   	            <td><? 
							$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$l]."'","logourl");
							echo $paymentMethods[$l].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
               </tr>
               <? } ?>
            </table></td>
      </tr>
   </table>
   <table width="100%" border="0" cellpadding="4" cellspacing="2" class="errormessage">
      <tr>
         <td width="150" align="center"><input name="mook" type="submit" id="mook" value="<?=$lang[proceed]?>"></td>
         <td><?=$lang[confirmbuyterms];?></td>
      </tr>
   </table>
</form>
<? 			} 
			}
	} else echo "<p align=center><strong>$lang[operationimpossible]</strong></p>"; 
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
