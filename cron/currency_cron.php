<?php
## v5.10 -> feb. 21, 2005
#///////////////////////////////////////////////////////
#//  MOD NAME: CURRENCY CONVERTER FOR PPB AUCTIONS
#//  v.1.0.0
#//  COPYRIGHT 2005 
#//  Henk C. Huitema & Kevin Thomas
#//  ALL RIGHTS RESERVED
#///////////////////////////////////////////////////////

include_once ("../config/config.php");

$fp = fopen("http://www.bankofcanada.ca/fmd/exchange.htm","r"); // OPEN THE FILE
$x=0;$g=0;
while(!feof($fp)) // START LOOP UNTIL END OF FILE
{
	$buf = fgets($fp, 4096);
	if(@eregi("U.S. Dollar",$buf)) $x=4;
	if(@eregi("</PRE>",$buf)) $x=0;
	if($x==4)
	{
		if(!@eregi("US/CA",$buf))
		{
			$ime = explode("/",$buf); //EXPLODE THE LINE, this breaks at the english/french country/currency description
			$s = explode(" ",$ime[1]); // EXPLODE THE LINE AGAIN, this time using a space
			$r = array_reverse ($s); // REVERSE THE ARRAY, to get the last value which is the latest currency value

			if(@eregi("Euro de",$buf)) // PICK UP ON THE EURO LINE
			{
				$ime[0]="European Monetary Union EURO";
				$s = explode(" ",$buf);
				$r = array_reverse ($s);
			}

			if($ime[0]<>"" and $r[0]<>"") //IF THE ARRAY KEYS HAVE VALUES THEN PROCESS THEM
			{
				$g++;
				if(@eregi("U.S. Dollar",$buf)) {$koef = (float)$r[0];} // GET THE VALUE FOR US DOLLAR (WHICH ALL WILL BE BASED ON)
				$k = ((float)$r[0]/(float)$koef); // DO THE MATH TO SET EACH VALUE TO ITS CONVERT RATE BASED ON USD
				$usd = 1/$k; // FINALISE THE MATH
				$res = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT * FROM probid_rates WHERE sifra=\"$ime[0]\"") or die("ERROR 42:".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); //CHECK FOR THE VALUE IN THE DATABASE
				$num = mysqli_num_rows($res);
				if($num == 0) // IF DOES NOT EXIST, THEN INSERT THE NEW VALUE
				{
                  mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_rates VALUES(
		 			          NULL,
		 			          \"$ime[0]\",
					          '',
					          $usd,
					          \"$ime[0]\")");
	          }
				else // EXISTS, SO JUST AN UPDATE HERE				
                  mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_rates SET rate='$usd' WHERE sifra=\"$ime[0]\"");				
			}
			if(@eregi("Venezuelan Bolivar",$buf)) $x=0; //STOP THE LOOP
		}
	}
}
fclose($fp); // CLOSE THE FILE

?>

