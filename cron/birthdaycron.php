<?
## v5.22 Mod -> Nov. 29 2005 - Birthday Modification Cron by Binarywebs

include_once ("../config/config.php");

	### Define the current Date

	$thetime = date( "Y-m-d H:i:s", time() );
	$fixtime = $thetime;
	$month = substr($fixtime,5,2); // The Month
	$day = substr($fixtime,8,2); // The Day
	$todaysdate = "$month$day";

	### Query users to Get the Birthdates

	$checkbday = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE gifted='N'");
	while ($birthdays=mysqli_fetch_array($checkbday)) {

	### Assign the Variables for Datecheck

	$bdaymonth	= 	substr($birthdays['birthdate'],5,2);
	$bdayday	=	substr($birthdays['birthdate'],8,2);
	$birthday	=	"$bdaymonth$bdayday";

	### Compare current date to birthdate on file

	if ($birthday==$todaysdate) {

	### If date match, Set as Preferred Seller and Flag as being Gifted

	$givepresent = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users set preferred_seller='Y', gifted='Y' WHERE id='".$birthdays['id']."'");

	### Send the user a Happy Birthday Email

	$recipientId = $birthdays['id'];
	include ("../mails/happybirthday.php");

	### End of Routine
	
	}}

	### Check for Gifted Users and Remove Preferred Seller if Birthdate Passed

	$checkgifted = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE gifted='Y'");
	while ($gifted=mysqli_fetch_array($checkgifted)) {

	### Assign the Variables for Comparison

	$bdaymonth	= 	substr($gifted['birthdate'],5,2);
	$bdayday	=	substr($gifted['birthdate'],8,2);
	$birthday	=	"$bdaymonth$bdayday";

	### Compare Dates

	if ($birthday <> $todaysdate) {

	### If date is not today, Remove the Preferred Seller and Gifted Status

	$removepresent = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users set preferred_seller='N', gifted='N' WHERE id='".$gifted['id']."'");

	### End of Routine

	}}

?>