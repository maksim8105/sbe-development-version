<?
## v5.25 -> jun. 16, 2006
#added $enable_email variable to enable or disable sending e-mails 14:41 29.04.2014
if (!$manualCron) {
	include_once ("../config/config.php");
	$dir_parent = "../";
} else {
	$dir_parent = "";
}

$enable_email = False;

### check for expired auctions and close them 
### v5.01 small tweak. Only check for opened auctions which have their enddate expired, and not for all auctions
### this will dramatically improve the loading times on auction sites that have 100+ opened auctions.

### close wanted ads.

$getExpiredWanteds = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_wanted_ads WHERE closed=0 AND enddate<'".$timeNowMysql."'");

$closedWantedDets = array();
while ($expWanted = mysqli_fetch_array($getExpiredWanteds)) {
	if ($expWanted['closed']==0&&$expWanted['active']==1&&$expWanted['deleted']!=1) { 
		delwantedcount ($expWanted['category']);
		delwantedcount ($expWanted['addlcategory']);
	}
	
	$closeAd[$wntcnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET closed=1 WHERE closed=0 AND enddate<'".$timeNowMysql."' AND id='".$expWanted['id']."'");
	
	$closedWantedDets[$expWanted['ownerid']]++;
}

if (count($closedWantedDets)>0) {
	## v5.23c addition - we go through the users table (active users only) and send which emails they should receive
	$getUserW = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, name, email FROM probid_users WHERE active='1'");
	while ($userW = mysqli_fetch_array($getUserW)) {
		$name = $userW['name'];
		$email = $userW['email'];

		if ($closedWantedDets[$userW['id']]==1) if($enable_email){ include ($dir_parent."mails/notifywantedadexpiredcron.php"); }		
		else if ($closedWantedDets[$userW['id']]>1) if($enable_email){ include ($dir_parent."mails/multiple-notifywantedadexpiredcron.php"); }		
	}
}

$getCronAuction=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND enddate<'".$timeNowMysql."' 
AND auctiontype!='wanted' AND close_in_progress='0' AND bid_in_progress='0' LIMIT 0,100");
## now mark all these auctions as "in progress", so that another cron1.php will not open them causing the same auction to be closed twice
## and the winner to be assigned twice or more times <- added in v5.22
$markInProgress=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET close_in_progress='1' WHERE  
active=1 AND closed=0 AND deleted!=1 AND enddate<'".$timeNowMysql."' AND auctiontype!='wanted'");

$counterA = array(); $counterB = array();

while ($cronAuction=mysqli_fetch_array($getCronAuction)) {
	$daysleft=daysleft($cronAuction['enddate'],$setts['date_format']);
	if ($daysleft<=0) {
		### we'll close this auction,
		### select a winner if avaliable, and enter his data in probid_winners table
		### and finally, send an email to both the buyer and the seller
		$closeAuction=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
		closed=1 WHERE id='".$cronAuction['id']."'");

		delcatcount($cronAuction['category'],$cronAuction['id']);
		delcatcount($cronAuction['addlcategory'],$cronAuction['id']);
		##only do this if reserve is met

		## we have closed the auction, now if auto relist for the auction is on, we will relist it.
		if ($cronAuction['auto_relist']=="Y" && $cronAuction['auto_relist_nb']>0) {
			if ($cronAuction['auto_relist_bids']=="Y"||$cronAuction['nrbids']==0) {
				$relistId = $cronAuction['id'];
				## if the item is a store item, first check if more store items can be listed.
				$itemListedIn = getSqlField("SELECT listin FROM probid_auctions WHERE id='".$relistId."'","listin");
	
				$userStore = getSqlRow("SELECT aboutpage_type, store_active, store_account_type FROM probid_users WHERE id='".$cronAuction['ownerid']."'");
				$nbStoreItems = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$cronAuction['ownerid']."' AND 
				active='1' AND closed='0' AND deleted!='1' AND listin!='auction'");
				$storeActive = FALSE;
				if ($userStore['aboutpage_type']==2&&$userStore['store_active']==1) {
					if ($userStore['store_account_type']==0) $storeActive = TRUE;
					else {
						$maxStoreItems = getSqlField("SELECT store_nb_items FROM probid_fees_tiers WHERE id='".$userStore['store_account_type']."'","store_nb_items");
						if ($maxStoreItems!="n/a"&&$maxStoreItems!=""&&$maxStoreItems>$nbStoreItems) $storeActive=TRUE;
					}
				}
				
				$canList = FALSE;
				if ($itemListedIn!="store") {
					$canList = TRUE;
				} else {
					if ($storeActive) $canList = TRUE;
					else $canList = FALSE;
				}
				
				if ($canList) {
				
					$today = date( "Y-m-d H:i:s", time() );
					$closingdate = closingdate($today,$cronAuction['duration']);
					$relistAuction[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auctions 
					(itemname, description, picpath, quantity, auctiontype, bidstart, rp, 
					rpvalue, bn, bnvalue, bi, bivalue, duration, country, zip, sc, scint, pm, 
					category, active, payment_status, startdate, enddate, closed, keywords, 
					nrbids, maxbid, clicks, ownerid, hpfeat, catfeat, bolditem, hlitem, private, 
					currency, swapped, postage_costs, insurance, type_service, isswap, acceptdirectpayment, 
					addlcategory, deleted, listin, shipping_details, bank_details, accept_payment_systems, apply_vat, 
					auto_relist, auto_relist_bids, videofile_path, offer_range_min, offer_range_max, auto_relist_nb, SBE_paid) SELECT
					itemname, description, picpath, quantity, auctiontype, bidstart, rp, 
					rpvalue, bn, bnvalue, bi, bivalue, duration, country, zip, sc, scint, pm, 
					category, active, payment_status, startdate, enddate, closed, keywords, 
					nrbids, maxbid, clicks, ownerid, hpfeat, catfeat, bolditem, hlitem, private, 
					currency, swapped, postage_costs, insurance, type_service, isswap, acceptdirectpayment, 
					addlcategory, deleted, listin, shipping_details, bank_details, accept_payment_systems, apply_vat, 
					auto_relist, auto_relist_bids, videofile_path, offer_range_min, offer_range_max, auto_relist_nb, SBE_paid 
					FROM probid_auctions b WHERE b.id='".$relistId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
													
					$newId = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
					$getCustomFields[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fields_data WHERE auctionid='".$relistId."'");
					while ($relistFld = mysqli_fetch_array($getCustomFields[$relist_counter])) {
						$relistCustomFields[$relist_counter][$custom_fld++] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fields_data 
						(auctionid,ownerid,boxid,boxvalue,active) VALUES 
						('".$newId."','".$relistFld['ownerid']."','".$relistFld['boxid']."','".$relistFld['boxvalue']."','".$relistFld['active']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));			
					}
		
					$getImages[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$relistId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					$addCnt=0;
					while ($relistAddImages=mysqli_fetch_array($getImages[$relist_counter])) {
						$fileExtension = getFileExtension($relistAddImages['name']);
						$addImageName = "a".$newId."_addpic".$addCnt.".".$fileExtension;				
						$isUpload = uploadFile($relistAddImages['name'],$addImageName,"uplimg/", FALSE, '', FALSE);
						if ($isUpload) {
							$addImage[$addCnt][$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_images
							(name, auctionid) VALUES ('uplimg/".$addImageName."','".$newId."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
							$addCnt++;
						}
					}
					$mainPicRelist = getSqlField("SELECT picpath FROM probid_auctions WHERE id='".$newId."'","picpath");
					if ($mainPicRelist != "") {
						$fileExtension = getFileExtension($mainPicRelist);
						$mainPicName = "a".$newId."_mainpic.".$fileExtension;
						$isUpload = uploadFile($mainPicRelist,$mainPicName,"uplimg/", FALSE, '', FALSE);
						if ($isUpload) {
							$updateAuctionPic[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
							picpath='uplimg/".$mainPicName."' WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						}
					}
					## video relist
					$videoRelist = getSqlField("SELECT videofile_path FROM probid_auctions WHERE id='".$newId."'","videofile_path");
					if (!empty($videoRelist)) {
						$fileExtension = getFileExtension($videoRelist);
						$videoName = "a".$newId."_video.".$fileExtension;
						$isUpload = uploadFile($videoRelist,$videoName,"uplimg/", FALSE, '', FALSE);
						if ($isUpload) {
							$updateAuctionVideo[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
							videofile_path='uplimg/".$videoName."' WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						}
					}
		
					### check for additional images
					
					$nbAddImages = getSqlNumber("SELECT * FROM probid_auction_images WHERE auctionid='".$newId."'");
					
					$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$newId."'");

					$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auctionDetails['id']."'","category");
			
					$category_id = getMainCat($itemCategory);
			
					$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
					$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
					
					$topay="";
					if ($fee['is_setup_fee']=="Y") $topay.="Auction Setup Fee; ";
					if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$nbAddImages>0) $topay.="Image Gallery Fee; ";
					if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$auctionDetails['hlitem']=="Y"&&$setts['hl_item']==1) $topay.="Highlighted Item Fee; ";
					if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$auctionDetails['bolditem']=="Y"&&$setts['bold_item']==1) $topay.="Bold Item Fee; ";
					if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$auctionDetails['hpfeat']=="Y"&&$setts['hp_feat']==1) $topay.="Home Page Featured Item Fee; ";
					if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$auctionDetails['catfeat']=="Y"&&$setts['cat_feat']==1) $topay.="Category Page Featured Item Fee; ";
					if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$auctionDetails['rp']=="Y"&&$auctionDetails['rpvalue']>0) $topay.="Reserve Price Fee; ";
					if ($fee['second_cat_fee']>0&&$auctionDetails['addlcategory']>0) { 
						$topay.="Second Category Fee; ";
						$issecondcat = "Y";
					} else $issecondcat = "N";
					if ($fee['bin_fee']>0&&$auctionDetails['bn']=="Y"&&$auctionDetails['bnvalue']>0) $topay.="Buy It Now Fee; ";
					if ($fee['videofile_fee']>0&&!empty($auctionDetails['videofile_path'])) { 
						$topay.="Movie Upload Fee; ";
						$ismoviefee = "Y";
					}
	
					$hlitem_chg = ($setts['hl_item']==1) ? $auctionDetails['hlitem'] : "N";
					$bolditem_chg = ($setts['bold_item']==1) ? $auctionDetails['bolditem'] : "N";
					$hpfeat_chg = ($setts['hp_feat']==1) ? $auctionDetails['hpfeat'] : "N";
					$catfeat_chg = ($setts['cat_feat']==1) ? $auctionDetails['catfeat'] : "N";
					
					## calculate quantity
					$qu_a = getSqlField("SELECT quantity FROM probid_auctions WHERE id='".$newId."'","quantity");
					$qu_b = getSqlField("SELECT sum(quant_offered) AS quant_o FROM probid_winners WHERE auctionid='".$relistId."'", "quant_o");
					$qu_total = $qu_a + $qu_b;
					$qu_total = ($auctionDetails['auctiontype']=="dutch") ? $qu_total : 1;
			
					## if item is listed in both, and storeactive=false, then only list in auction
					$listIn = ($storeActive) ? $itemListedIn : "auction";
					$relistAuctionDets_1[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
					startdate='".$timeNowMysql."',enddate='".$closingdate."',duration='".$cronAuction['duration']."',
					closed=0,nrbids=0,maxbid=0, quantity='".$qu_total."', 
					hlitem='".$hlitem_chg."', bolditem='".$bolditem_chg."', hpfeat='".$hpfeat_chg."', catfeat='".$catfeat_chg."', 
					listin='".$listIn."', clicks=0, auto_relist_nb=auto_relist_nb-1 
					WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					//echo "You have to pay the following fees: $topay";

					$isFee = array('hpfeat' => $hpfeat_chg, 'catfeat' => $catfeat_chg, 'bold' => $bolditem_chg, 'hl' => $hlitem_chg, 
					'rp' => $auctionDetails['rp'], 'pic_count' => $nbAddImages, 'secondcat' => $issecondcat, 'bin' => $auctionDetails['bn'], 
					'videofee' => $ismoviefee);

					if ($topay!=""&&!freeFees($auctionDetails['ownerid'])&&$fee['relist_fee_reduction']<100) {
						$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$cronAuction['ownerid']."'","payment_mode");
						if ($setts['account_mode_personal']==1) {
							$account_mode_local = ($tmp) ? 2 : 1;
						} else $account_mode_local = $setts['account_mode'];
			
						if ($account_mode_local==2) {
							setupFee($auctionDetails['bidstart'],$auctionDetails['currency'],$auctionDetails['id'],$isFee, FALSE, FALSE, '', 0, FALSE, TRUE);
							$payment_status="confirmed";
							$active=1;
						} else {
							$payment_status="unconfirmed";
							$active=0;
						}
					} else {
						$payment_status="confirmed";
						$active=1;
		// KEV START CHANGES - changed the following 2 lines to point to the new function 				
						addcatcount($auctionDetails['category'],$auctionDetails['id']);
						addcatcount($auctionDetails['addlcategory'],$auctionDetails['id']);
					}
					$relistAuctionDets_2[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
					active='".$active."',payment_status='".$payment_status."' 
					WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					
					### check in auction watch table for keywords matching ours and send email to the user interested.
					$keywords = getSqlField("SELECT keywords FROM probid_auctions WHERE id='".$newId."'","keywords");
					
					$getKeywords = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_keywords_watch") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					while ($keywordsW=mysqli_fetch_array($getKeywords)) {
						if (@eregi($keywordsW['keyword'],$keywords)) {
							$userId = $keywordsW['bidderid'];
							$auctionId = $newId;
							$keyword = $keywordsW['keyword'];
							if($enable_email){ include ("mails/keywordmatch.php"); }
						}
					}
		
					$userId = $auctionDetails['ownerid'];
					$auctionId = $newId;
					if ($nbRelistedAuctions>3) {
						if (!$mailSent) if($enable_email){ include ("mails/confirmtosellermultipleauctions.php"); }
						$mailSent = TRUE;
					} else {
						### send confirmation email to the seller
						if($enable_email){ include ("mails/confirmtoseller.php"); }
					}
					deleteAuction($relistId);
				}  
					
			}		
		}

		// Code Added by Kevin M. Bombino - 8/7/2004
		if ($cronAuction['rpvalue'] > $cronAuction['maxbid']) {
			$a = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT bid.bidamount, proxy.bidamount, bid.bidderid FROM 
			probid_bids AS bid, probid_proxybid AS proxy WHERE 
			bid.auctionid='".$cronAuction['id']."' AND bid.auctionid=proxy.auctionid 
			AND bid.bidderid=proxy.bidderid AND bid.invalid=0 AND bid.valja=0");
			## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
			if (mysqli_num_rows($a) > 0) {
				while ($b = mysqli_fetch_row($a)) { 
					if (($b[0] < $cronAuction['rpvalue']) && ($b[1] >= $cronAuction['rpvalue'])) {
						## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
						mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET bidamount='" . $cronAuction['rpvalue'] . "' WHERE auctionid='".$cronAuction['id']."' AND bidderid='" . $b[2] ."' AND invalid='0' AND valja='0'");
						$cronAuction['maxbid'] = $cronAuction['rpvalue'];
						mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET maxbid='" . $cronAuction['rpvalue'] . "' WHERE id='".$cronAuction['id']."'");
					}
				}
			}
		}

		$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$cronAuction['id']."'","category");

		$category_id = getMainCat($itemCategory);

		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

		$isWinner = FALSE;
		if ($cronAuction['rpvalue']<=$cronAuction['maxbid']) {		
			## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
			$getWinner=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE 
			auctionid='".$cronAuction['id']."' AND valja=0 AND invalid=0 ORDER BY bidamount DESC");
			$totalQuantity=0;
			while ($winnerDetails = mysqli_fetch_array($getWinner)) {
				$auctionWonDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$winnerDetails['auctionid']."'");
				### add to the seller another sale
				$addSale = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				items_sold=items_sold+1 WHERE id='".$auctionWonDetails['ownerid']."'");
				if ($totalQuantity==0) {
					$totalQuantity=$auctionWonDetails['quantity'];
				}
				$amount = $winnerDetails['bidamount'];
				if (($cronAuction['rpvalue'] > $cronAuction['maxbid']) && ($winnerDetails['bidamount'] >= $cronAuction['rpvalue']))  
					$cronAuction['maxbid'] = $cronAuction['rpvalue'];
					
				$addPurchase = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				items_bought=items_bought+1 WHERE id='".$winnerDetails['bidderid']."'");
				$quantityRequested = $winnerDetails['quantity'];
				if ($quantityRequested > 0) {
					//$amount = $quantityRequested*$amount;
					if ($quantityRequested<$totalQuantity) $quantityOffered = $quantityRequested;
					else $quantityOffered = $totalQuantity;
					$totalQuantity -= $quantityRequested;
				}
				
				$payerId = (@eregi('s', $fee['endauction_fee_applies'])) ? $cronAuction['ownerid'] : $winnerDetails['bidderid'];
				
				if ($fee['is_endauction_fee']=="Y"&&!freeFees($payerId)) {
					// if account mode is on, then add the end auction fee to the seller's balance, and activate the details of the winner
					
					$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$payerId."'","payment_mode");
					if ($setts['account_mode_personal']==1) {
						$account_mode_local = ($tmp) ? 2 : 1;
					} else $account_mode_local = $setts['account_mode'];
					
					if ($account_mode_local==2) {
						$newBalance=0;
						$endauctionFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE 
						fee_from<=".$amount." AND fee_to>".$amount." AND fee_type='endauction' AND category_id='".$category_id."'");
		
						if ($endauctionFee['calc_type']=="percent") {
							$newBalance+=($endauctionFee['fee_amount']/100)*$amount;
						} else {
							$newBalance+=$endauctionFee['fee_amount'];
						}

						### if there is a dutch auction, calculate the number of sold items times end of auction amount
						
						$auctionType = getSqlField("SELECT auctiontype FROM probid_auctions WHERE id='".$cronAuction['id']."'","auctiontype");
						if ($auctionType=="dutch") {
							$totalQuant = getSqlField("SELECT sum(quant_offered) AS total_quant FROM 
							probid_winners WHERE auctionid='".$cronAuction['id']."'","total_quant");
							$add_desc = " x ".$totalQuant;
						} else { 
							$totalQuant = 1;
							$add_desc = "";
						}
						##$pAmount = $amount * $totalQuant;
						$invoiceAmount = $invoiceAmount * $totalQuant;
				
						$prefSeller = "N";
						if ($setts['pref_sellers']=="Y") {
							$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
							WHERE id='".$payerId."'","preferred_seller");
						}
						
						$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);
						
						$payment_status="confirmed";
						$active=1;
						$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$payerId."'","balance");
						$updateSellerBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
						balance=balance+".$newBalance." WHERE id='".$payerId."'");
						// add end of auction fee on the invoices table 
						$currentTime = time();
						$currentBalance += $newBalance;
						$insinv = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
						(userid,auctionid,feename,feevalue,feedate,balance)	VALUES 
						('".$payerId."','".$winnerDetails['auctionid']."','End of Auction Fee.$add_desc','".$newBalance."','".$currentTime."','".$currentBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					} else {
						$payment_status="unconfirmed";
						$active=0;
					}
				} else {
					$payment_status="confirmed";
					$active=1;
				}
		
				$saveWinner = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_winners 
				(sellerid,buyerid,amount,quant_req,quant_offered,
				auctionid,auctiontype,payment_status,purchase_date) VALUES 
				('".$auctionWonDetails['ownerid']."','".$winnerDetails['bidderid']."','".$amount."','".$quantityRequested."',
				'$quantityOffered','".$winnerDetails['auctionid']."','".$auctionWonDetails['auctiontype']."',
				'".$payment_status."','".time()."')");
				### add the buyer another purchase
				$addPurchase=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				items_bought=items_bought+1 WHERE id='".$winnerDetails['bidderid']."'");
			
				$sellerUsername=getSqlField("SELECT * FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","username");
				$buyerUsername=getSqlField("SELECT * FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","username");
                                $prepareSellerFeedback = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
				(userid,usernick,fromid,submitted,auctionid,type) VALUES 
				('".$auctionWonDetails['ownerid']."','".$sellerUsername."','".$winnerDetails['bidderid']."',0,'".$winnerDetails['auctionid']."','sale')");
				$prepareBuyerFeedback = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
				(userid,usernick,fromid,submitted,auctionid,type) VALUES 
				('".$winnerDetails['bidderid']."','".$buyerUsername."','".$auctionWonDetails['ownerid']."',0,'".$winnerDetails['auctionid']."','purchase')");
				## variables needed for the included email files.
				$sellerId = getSqlField("SELECT id FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","id");
				$buyerId = getSqlField("SELECT id FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","id");
				$auctionId = $winnerDetails['auctionid'];
				$auctionName = getSqlField("SELECT itemname FROM probid_auctions WHERE id='".$auctionId."'","itemname");
				$maxBid = $amount;
	
				$email = getSqlField("SELECT email FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","email");;
				$name = getSqlField("SELECT name FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","name");
				
				$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","payment_mode");
				if ($setts['account_mode_personal']==1) {
					$account_mode_local = ($tmp) ? 2 : 1;
				} else $account_mode_local = $setts['account_mode'];
				
				$seller = getSqlRow("SELECT * FROM probid_users WHERE id='".$sellerId."'");
				$buyer = getSqlRow("SELECT * FROM probid_users WHERE id='".$buyerId."'");
				
				if ($account_mode_local!=2) if($enable_email){ include ($dir_parent."mails/notifysellercron.php"); }		

				$email = getSqlField("SELECT email FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","email");;
				$name = getSqlField("SELECT name FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","name");
				
				$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","payment_mode");
				if ($setts['account_mode_personal']==1) {
					$account_mode_local = ($tmp) ? 2 : 1;
				} else $account_mode_local = $setts['account_mode'];
				
				if ($account_mode_local!=2) if($enable_email){ include ($dir_parent."mails/notifybuyercron.php"); }		
				
				$isWinner = TRUE;
			}
		} else if ($cronAuction['rpvalue']>$cronAuction['maxbid']) {
			## send the high bidder an email telling him that the reserve wasnt met
			## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
			$highBidderId=getSqlField("SELECT bidderid FROM probid_bids 
			WHERE auctionid='".$cronAuction['id']."' AND valja=0 AND invalid=0 
			ORDER BY bidamount DESC LIMIT 0,1","bidderid");
			$email = getSqlField("SELECT email FROM probid_users WHERE id='".$highBidderId."'","email");
			$name = getSqlField("SELECT name FROM probid_users WHERE id='".$highBidderId."'","name");
			$isHighBidder=getSqlNumber("SELECT id FROM probid_users WHERE id='".$highBidderId."'");
			$auctionId = $cronAuction['id'];
			## only send this email if there was a high bidder, because otherwise it will return to return-path as error
			if ($isHighBidder>0) {
				if($enable_email){ include ($dir_parent."mails/notifyhighbiddercron.php"); }
			}
		}
		### notify seller if item didnt sell
		if (!$isWinner) {
			$sellerId = getSqlField("SELECT ownerid FROM probid_auctions WHERE id='".$cronAuction['id']."'","ownerid");
			$counterA[$sellerId] = $cronAuction['id'];
			$counterB[$sellerId] ++;
		}
	}
}

			//$email = getSqlField("SELECT email FROM probid_users WHERE id='".$sellerId."'","email");;
			//$name = getSqlField("SELECT name FROM probid_users WHERE id='".$sellerId."'","name");
			//$auctionId = $cronAuction['id'];
			//$seller = getSqlRow("SELECT * FROM probid_users WHERE id='".$sellerId."'");
			#5 - here
			//if($enable_email){ include ($dir_parent."mails/notifysellernobids.php"); }		

if (count($counterB)>0) {
	## v5.23c addition - we go through the users table (active users only) and send which emails they should receive
	$getUserNB = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, name, email, mail_auctionclosed FROM probid_users WHERE active='1'");
	while ($seller = mysqli_fetch_array($getUserNB)) {
		$name = $seller['name'];
		$email = $seller['email'];
		$auctionId = $counterA[$seller['id']];

		if ($counterB[$seller['id']]==1) if($enable_email){ include ($dir_parent."mails/notifysellernobids.php"); }		
		else if ($counterB[$seller['id']]>1) if($enable_email){ include ($dir_parent."mails/multiple-notifysellernobids.php"); }		
	}
}

## now if counters>0
$unmarkInProgress=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET close_in_progress='0' WHERE
active=1 AND closed=0 AND deleted!=1 AND close_in_progress='1' AND auctiontype!='wanted'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

### here, the cron will send the contact details to the seller and the winner(s)
$getWinners = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_winners WHERE emailsent=0");
while ($winner=mysqli_fetch_array($getWinners)) {
	## if payment is made or there is no end of auction fee, we will send the emails

##	E-MAIL TEST!!! 
##  if (($winner['payment_status']=="confirmed")||($fee['is_endauction_fee']=="N")) 
	
		$sellerId=$winner['sellerid'];
		$buyerId=$winner['buyerid'];
		$auctionId=$winner['auctionid'];
		$winnerId=$winner['id'];
		mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET emailsent=1 WHERE id='".$winner['id']."'");
		if($enable_email){ include($dir_parent."mails/buyerdetails.php"); }
		### mail the buyer
		if($enable_email){ include($dir_parent."mails/sellerdetails.php"); }						

}

// delete auctions older than 90 days
$thei=0;
$getAllAuctions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,enddate FROM probid_auctions WHERE closed=1");
	
while ($oldAuction=mysqli_fetch_array($getAllAuctions)) {
	$datenow=time();

    $arrDateTime = explode(" ", $oldAuction['enddate']);
    $arrDate = explode("-", $arrDateTime[0]);
    $arrTime = explode(":", $arrDateTime[1]);
	$daysleft=mktime($arrTime[0],$arrTime[1],$arrTime[2],$arrDate[1],$arrDate[2],$arrDate[0]);

	$daycount=intval($datenow-$daysleft); 
		
	$dayspassed=$daycount/86400;

	if ($dayspassed>$setts['auction_deletion']) {
		$inAdmin = FALSE;
		if ($_SESSION['adminarea']=="Active") $inAdmin = TRUE;		
		if (!$inAdmin) $_SESSION['adminarea']="Active";
		deleteAuction($oldAuction['id'],TRUE);
		if (!$inAdmin) $_SESSION['adminarea']="";
	}
}

// delete from probid_keywords_watch table, the empty search terms
$delinvkw = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_keywords_watch WHERE keyword=''");

### start auctions that are programmed to start at a certain date/time
// KEV CHANGES - we add the cat counts	

$today=date("Y-m-d H:i:s");
//$timeNowOffset = convertdate($today,"Y-m-d H:i:s");

$getCatCounts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,category,addlcategory FROM probid_auctions WHERE 
startdate<'".$today."' AND enddate>'".$today."' AND closed=1 
AND active=1 AND deleted!=1 AND swapped!=1");
while ($catid = mysqli_fetch_array($getCatCounts)) {
	addcatcount ($catid['category'],$catid['id']);
	addcatcount ($catid['addlcategory'],$cat['id']);
}

$startAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
active=1, closed=0 WHERE startdate<'".$today."' AND enddate>'".$today."' AND closed=1 
AND active=1 AND deleted!=1 AND swapped!=1");
### delete auctions from auction watch where the userid=0
$delAuctionWatch = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_watch WHERE userid=0");

### delete from the winners table if both the seller and the buyer have marked the row as deleted
$deleteWonItems=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_winners WHERE 
s_deleted=1 AND b_deleted=1");

## check for expired active stores, and if account mode = 2, charge the fee, otherwise suspend the store
$currentTime = time();
$getCronStores = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT u.id, u.store_account_type, f.fee_amount, f.store_recurring FROM probid_users u, probid_fees_tiers f WHERE 
u.store_active='1' AND u.aboutpage_type='2' AND f.store_recurring>0 AND f.fee_type='store' AND u.store_nextpayment<'".$currentTime."' AND u.active=1 AND u.store_account_type=f.id");

while ($cronStore = mysqli_fetch_array($getCronStores)) {
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$cronStore['id']."'","payment_mode");
	if ($setts['account_mode_personal']==1) {
		$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	if ($fee['is_store_fee']=="Y" && $cronStore['fee_amount'] > 0 && !freeFees($cronStore['id'])) { 
		if ($account_mode_local == 1) $updateStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active=0 WHERE id='".$cronStore['id']."'");
		else if ($account_mode_local == 2) storeSetupFee($cronStore['id'],$cronStore['store_account_type'],TRUE);
	} else { 
		$recurring = $cronStore['store_recurring'];
		$store_nextpayment = 0;
		if ($recurring>0) $store_nextpayment = time() + ($recurring*24*3600);

		$updateStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active=1, 
		store_lastpayment='".$currentTime."', store_nextpayment='".$store_nextpayment."' WHERE id = '".$cronStore['id']."'");
	}
}

## delete cached images that are older than 1 day -> v5.22 addition
$cache_directory = $dir_parent.'cache/';
$currentTime = time();
$oneday = 60*60*24;
$cacheDir=opendir($cache_directory);
	
while ($file = readdir($cacheDir)) {
	if($file != '..' && $file !='.' && $file !='' && $file !='index.htm') {
		$filestats = array();
		$filestats = stat($cache_directory.$file);
		if (($filestats[10]+$oneday)<$currentTime) @unlink($cache_directory.$file);
	}
}

closedir($cacheDir);
clearstatcache(); 

?>