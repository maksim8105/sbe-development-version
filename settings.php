<?


  function generatepin ($value)
  {
    return substr (md5 ($value), 15, 8);
  }

  function checkpin ($value, $pin)
  {
    $valid = FALSE;
    if (substr (md5 ($value), 15, 8) == $pin)
    {
      $valid = TRUE;
    }

    return $valid;
  }

  if (!(defined ('INCLUDED')))
  {
    exit ('Access Denied');
  }

  $setts = array ();
  if (!($getSetts = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT * FROM probid_gen_setts')))
  {
    exit (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
  }

  $row = mysqli_fetch_array($getSetts);
  $setts['sitename'] = $row['sitename'];
  $setts['siteurl'] = $row['siteurl'];
  $setts['adminemail'] = $row['adminemail'];
	if(empty($_GET)) 
	{
		$setts['default_theme'] = $row['default_theme'];
	}
	// elseif( {
		// if(isset($_GET["ct"])) {
			  // switch($_GET["ct"])
			  // {
				// case 'v6':
					// $setts['default_theme'] = 'v6'; 
					// break;
				// default:
					// $setts['default_theme'] = $row['default_theme'];
			  // }
			  
		// } else {
			// $setts['default_theme'] = $row['default_theme'];
		// }
	elseif ( isset($_GET['ct']) && $_GET['ct'] == 'v6' )
	{
		$setts['default_theme'] = 'v6';
	}
	else
	{
		$setts['default_theme'] = $row['default_theme'];
	}
  /*$setts['default_theme'] = $row['default_theme'];*/ /*<<here*/
  $setts['title_rus'] = $row['title_rus'];
  $setts['title_est'] = $row['title_est'];
  $setts['title_eng'] = $row['title_eng'];
  $setts['title_lat'] = $row['title_lat'];
  $setts['default_lang'] = $row['default_lang'];
  $setts['admin_lang'] = $row['admin_lang'];
  $setts['user_lang'] = $row['userlang'];
  $setts['is_ssl'] = $row['is_ssl'];
  $setts['ssl_address'] = $row['ssl_address'];
  if ($setts['is_ssl'] == 1)
  {
    $path = $setts['ssl_address'];
  }
  else
  {
    $path = $setts['siteurl'];
  }

  $setts['bidretraction'] = $row['bidretraction'];
  $setts['paypalemail'] = $row['paypalemail'];
  $setts['worldpayid'] = $row['worldpayid'];
  $setts['checkoutid'] = $row['checkoutid'];
  $setts['ikobombid'] = $row['ikobombid'];
  $setts['ikoboipn'] = $row['ikoboipn'];
  $setts['protxname'] = $row['protxname'];
  $setts['protxpass'] = $row['protxpass'];
  $setts['authnetid'] = $row['authnetid'];
  $setts['authnettranskey'] = $row['authnettranskey'];
  $setts['moneybookersemail'] = $row['moneybookersemail'];
  $setts['mailer'] = $row['mailer'];
  $setts['sendmail_path'] = $row['sendmail_path'];
  $setts['m_format'] = $row['money_format'];
  $setts['currency'] = $row['currency_symbol'];
  $setts['digits'] = $row['digits'];
  $setts['position'] = $row['position'];
  $setts['pic_gal_active'] = $row['pic_gal_active'];
  $setts['pic_gal_max_nb'] = $row['pic_gal_max_nb'];
  $setts['pic_gal_max_size'] = $row['pic_gal_max_size'];
  $setts['auction_deletion'] = $row['auction_deletion'];
  $setts['user_deletion'] = $row['user_deletion'];
  $setts['shipping_costs'] = $row['shipping_costs'];
  $setts['error_text'] = $row['error_text'];
  $setts['error_email'] = $row['error_email'];
  $setts['hp_feat'] = $row['hp_feat'];
  $setts['cat_feat'] = $row['cat_feat'];
  $setts['bold_item'] = $row['bold_item'];
  $setts['hl_item'] = $row['hl_item'];
  $setts['swap_items'] = $row['swap_items'];
  $setts['h_counter'] = $row['h_counter'];
  $setts['cron_job'] = $row['cron_job'];
  $setts['account_mode'] = $row['account_mode'];
  $setts['max_credit'] = $row['max_credit'];
  $setts['init_credit'] = $row['init_credit'];
  $setts['min_invoice_value'] = $row['min_invoice_value'];
  $setts['init_acc_type'] = $row['init_acc_type'];
  $setts['paypaldirectpayment'] = $row['paypaldirectpayment'];
  $setts['alwaysshowbuynow'] = $row['alwaysshowbuynow'];
  $setts['secondcategory'] = $row['secondcategory'];
  $setts['sniping_feature'] = $row['sniping_feature'];
  $setts['sniping_duration'] = $row['sniping_duration'];
  $setts['private_site'] = $row['private_site'];
  $setts['pref_sellers'] = $row['pref_sellers'];
  $setts['pref_sellers_reduction'] = $row['pref_sellers_reduction'];
  $setts['bcc'] = $row['enable_bcc'];
  $setts['verify_strikes'] = $row['verify_strikes'];
  $setts['verify_strikes_duration'] = $row['verify_strikes_duration'];
  $setts['enable_asq'] = $row['enable_asq'];
  $setts['enable_ra'] = $row['enable_ra'];
  $setts['enable_wantedads'] = $row['enable_wantedads'];
  $setts['stores_enabled'] = $row['stores_enabled'];
  $setts['hpfeat_desc'] = $row['hpfeat_desc'];
  $setts['vat_rate'] = $row['vat_rate'];
  $setts['auto_vat_exempt'] = $row['auto_vat_exempt'];
  $setts['mlc'] = $row['enable_mlc'];
  $setts['vat_number'] = $row['vat_number'];
  $setts['invoice_comments'] = $row['invoice_comments'];
  $setts['enable_bid_retraction'] = $row['enable_bid_retraction'];
  $setts['min_reg_age'] = $row['min_reg_age'];
  $setts['birthdate_type'] = $row['birthdate_type'];
  $setts['nb_other_items_adp'] = $row['nb_other_items_adp'];
  $setts['maintenance_mode'] = $row['maintenance_mode'];
  $setts['account_mode_personal'] = $row['account_mode_personal'];
  $setts['enable_bulk_lister'] = $row['enable_bulk_lister'];
  $setts['suspend_over_bal_users'] = $row['suspend_over_bal_users'];
  $setts['enable_cat_counters'] = $row['enable_cat_counters'];
  $setts['enable_display_phone'] = $row['enable_display_phone'];
  $setts['video_gal_max_size'] = $row['video_gal_max_size'];
  $setts['enable_auctions_approval'] = $row['enable_auctions_approval'];
  $setts['approval_categories'] = $row['approval_categories'];
  $setts['is_mod_rewrite'] = $row['is_mod_rewrite'];
  $setts['buyout_process'] = $row['buyout_process'];
  $setts['nb_autorelist_max'] = $row['nb_autorelist_max'];
  $layout = array ();
  if (!($getLayout = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT logo_path, site_align, nb_feat_hp, w_feat_hp, max_feat_hp, nb_feat_cat, 
w_feat_cat, max_feat_cat, nb_last_auct, nb_hot_auct, nb_end_auct, d_login_box, d_news_box, d_news_nb, 
act_newsletter, act_buynow, act_proxy, d_acc_text, acc_text, d_tc_text, tc_text, is_faq, is_about, 
is_terms, is_contact, is_pp, nb_want_ads  FROM probid_layout_setts')))
  {
    exit (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
  }

  $layout = mysqli_fetch_array($getLayout);
  $getDateFormat = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT type, value FROM probid_dateformat WHERE active=\'checked\'');
  $dateArray = mysqli_fetch_array($getDateFormat);
  $setts['date_format_type'] = $dateArray['type'];
  $setts['date_format'] = $dateArray['value'];
  $fee = array ();
  if (!($getFees = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT * FROM probid_fees WHERE category_id=0 LIMIT 0,1')))
  {
    exit (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
  }

  $fee = mysqli_fetch_array($getFees);
  $timeNowMysql = date ('Y-m-d H:i:s', time ());
  if (!(empty ($_SESSION['pin_value'])))
  {
    @unlink ('uplimg/tmp_pin_' . $_SESSION['pin_value'] . '.jpg');
    $_SESSION['pin_value'] = '';
  }

  if (!(empty ($_SESSION['admin_pin_value'])))
  {
    @unlink ('../uplimg/tmp_pin_' . $_SESSION['admin_pin_value'] . '.jpg');
    $_SESSION['admin_pin_value'] = '';
  }

  if (!($_SESSION['sess_lang']))
  {
    @include_once 'config/lang/' . $setts['default_lang'] . '/site.lang';
    $_SESSION['sess_lang'] = $setts['default_lang'];
    @include_once 'config/lang/' . $setts['default_lang'] . '/category.lang';
  }
  else
  {
    @include_once 'config/lang/' . $_SESSION['sess_lang'] . '/site.lang';
    @include_once 'config/lang/' . $_SESSION['sess_lang'] . '/category.lang';
  }

  @include_once 'config/lang/' . $_SESSION['sess_lang'] . '/catsarray.php';
  if (file_exists ('update.php'))
  {
    echo '<p align=center><img src="images/probidlogo.gif" border="0"></p><p align=center style="font:Verdana, Arial, Helvetica, sans-serif; font-size:14px;">The file <strong>update.php</strong> still exists on your server.<br /> Please delete it in order for the software to run properly.</p> ';
    exit ();
  }

?>