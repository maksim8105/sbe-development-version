<?
## v5.24 -> par. 28, 2006
session_start();

if ($_SESSION['membersarea']!="Active") {
	header ("Location: login.php");
}

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

if ($_GET['option']=="pay_auction_setup") $headerMessage = $lang[PAY_FOR_AUCTION_SETUP_FEE];
else if ($_GET['option']=="pay_endauction_fee") $headerMessage = $lang[PAY_END_OF_AUCTION_FEE];
header5($headerMessage); ?> 
<br>
 <table width="100%" border="0" cellspacing="4" cellpadding="4" class="border"> 
  <tr class="c3"> 
     <td class="contentfont"><? 
		if ($_GET['option']=="pay_auction_setup") {
			$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_GET['auctionid']."' AND ownerid='".$_SESSION['memberid']."'");
			$nbAddImages = getSqlNumber("SELECT * FROM probid_auction_images WHERE auctionid='".$_GET['auctionid']."'");

			$topay="";
			$isListingFee = getSqlNumber("SELECT * FROM probid_fees_tiers WHERE 
			fee_from<=".$auction['bidstart']." AND fee_to>".$auction['bidstart']." AND fee_type='setup'");
			if ($fee['is_setup_fee']=="Y"&&$isListingFee>0) $topay.="Auction Setup Fee; ";
			if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$nbAddImages>0) { 
				$topay.="Image Gallery Fee; ";
				$isFee['pic_count']=$nbAddImages;
			}
			if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$auction['hlitem']=="Y") {
				$topay.="Highlighted Item Fee; ";
				$isFee['hl']="Y";
			}
			if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$auction['bolditem']=="Y") {
				$topay.="Bold Item Fee; ";
				$isFee['bold']="Y";
			}
			if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$auction['hpfeat']=="Y") {
				$topay.="Home Page Featured Item Fee; ";
				$isFee['hpfeat']="Y";
			}
			if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$auction['catfeat']=="Y") {
				$topay.="Category Page Featured Item Fee; ";
				$isFee['catfeat']="Y";
			}
			if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$auction['rp']=="Y"&&$auction['rpvalue']>0) {
				$topay.="Reserve Price Fee; ";
				$isFee['rp']="Y";
			}
			if ($fee['second_cat_fee']>0&&$auction['addlcategory']>0) {
				$topay.="Second Category Fee; ";
				$isFee['secondcat']="Y";
			}
			if ($fee['bin_fee']>0&&$auction['bn']=="Y"&&$auction['bnvalue']>0) {
				$topay.="Buy It Now Fee; ";
				$isFee['bin']="Y";
			}
			if ($fee['videofile_fee']>0&&!empty($auction['videofile_path'])) {
				$topay.="Movie Upload Fee; ";
				$isFee['videofee']="Y";
			}
			if ($fee['customst_fee']>0&&($auction['endtime']=="customtime"||$auction['startdate']>$timeNowMysql)) {
				$topay.="Custom Start Time Fee; ";
				$isFee['customst']="Y";
			}

			if (!freeFees($_SESSION['memberid'])) {
				setupFee($auction['bidstart'],$auction['currency'],$auction['id'],$isFee);
			} else {
				echo $lang[aucsubmitted];
				addcatcount ($auction['category'],$auction['id']);
				addcatcount ($auction['addlcategory'],$auction['id']);
			}
		} 
		if ($_GET['option']=="pay_endauction_fee") {
			$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_GET['auctionid']."' AND ownerid='".$_SESSION['memberid']."'");

			$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auction['id']."'","category");
	
			$category_id = getMainCat($itemCategory);
	
			$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
			$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

			if (!freeFees($_SESSION['memberid'])) {
				endauctionFee($_GET['finalbid'],$auction['currency'],$_GET['auctionid'], $_SESSION['memberid']);
			} else {
				echo $lang[aucsubmitted];
			}
		}
		if ($_GET['option']=="pay_wantedad_setup") {
			if (!freeFees($_SESSION['memberid'])) {
				wantedAdFee($_GET['auctionid']);
			} else {
				echo $lang[aucsubmitted];
			}
		} ?></td> 
   </tr> 
</table> 
<? include ("themes/".$setts['default_theme']."/footer.php"); ?> 
