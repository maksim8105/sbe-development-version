<?
## v5.25 -> jun. 29, 2006
session_start();
if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php?redirect=reserveoffer&auctionid=".$_GET['id']."'</script>";
} else {

include_once ("config/config.php");

$enteredOffer = 0;
if (isset($_GET['enterofferok'])) {
	$canEnter = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."'");
	if ($canEnter>0&&$_GET['fixedoffer']>0) {
		$addValue = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET reserveoffer='".$_GET['fixedoffer']."' WHERE id='".$_GET['id']."'");
		$enteredOffer = 1;
		$getUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT bidderid, bidamount, id FROM probid_bids WHERE auctionid='".$_GET['id']."' AND 
		invalid=0 AND deleted=0 ORDER BY bidamount DESC");
		$delResOffers = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_reserve_offers WHERE auctionid='".$_GET['id']."'");
		while ($user = mysqli_fetch_array($getUsers)) {
			$addBidder[$addBCnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_reserve_offers 
			(auctionid, bidderid, bidamount, accepted, bidid) VALUES 
			('".$_GET['id']."','".$user['bidderid']."','".$user['bidamount']."', '0', '".$user['id']."')");
			$bidderId = $user['bidderid'];
			$auctionId = $_GET['id'];
			include ("mails/notifybidder_fpoffer.php");
			
		}
	} else {
		$enteredOffer = 2;
	}	
}
$auctionQuery = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions WHERE id='".$_GET['id']."' 
AND closed='1' AND rp='Y' AND maxbid<rpvalue AND nrbids>0");

$showPage = false;
if (getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."'")>0) $showPage = true;
else if (getSqlNumber("SELECT id FROM probid_bids WHERE auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."' AND invalid=0 AND deleted=0")>0) $showPage = true;

$isAuction = mysqli_num_rows($auctionQuery);
if ($isAuction>0&&$showPage) {
	$auction = mysqli_fetch_array($auctionQuery);

	if ($_GET['option']=="accept") {
		$currentTime = time();
		$updateOffer = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_reserve_offers SET accepted=1, regdate='".$currentTime."' WHERE auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		## notify seller of the accepted offer
		$sellerId = $auction['ownerid'];
		$auctionId = $auction['id'];
		include("mails/notifyseller_fpoffer.php");
	}
	
	if ($_GET['option']=="declare_winner") {
		## declare the winner
		$winnerDetails=getSqlRow("SELECT b.* FROM probid_reserve_offers o, probid_bids b WHERE 
		o.accepted='1' AND o.auctionid='".$_GET['id']."' AND o.id='".$_REQUEST['offerid']."' AND o.bidid=b.id") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

		$auctionWonDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$winnerDetails['auctionid']."'");

		$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auctionWonDetails['id']."'","category");

		$category_id = getMainCat($itemCategory);

		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
		
		### add the new bid in probid_bids and probid_bids_history and out the others.
		$outbidBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET out=1 WHERE auctionid='".$winnerDetails['auctionid']."'");
		$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
		(auctionid,bidderid,bidamount,date,proxy,rpwinner) VALUES 
		('".$winnerDetails['auctionid']."','".$winnerDetails['bidderid']."','".$auctionWonDetails['reserveoffer']."','".$timeNowMysql."','".$auctionWonDetails['reserveoffer']."','1')");
		$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
		$bidderUsername = getSqlField("SELECT username FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","username");
		
		$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
		(biddername,auctionid,amount,date,bidid) VALUES 
		('".$bidderUsername."','".$winnerDetails['auctionid']."','".$auctionWonDetails['reserveoffer']."','".$timeNowMysql."','".$bid_id."')");
		$updatAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
		maxbid='".$auctionWonDetails['reserveoffer']."',nrbids=nrbids+1 WHERE id='".$winnerDetails['auctionid']."'");						
		
		### add to the seller another sale
		$addSale = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		items_sold=items_sold+1 WHERE id='".$auctionWonDetails['ownerid']."'");
		$amount = $auctionWonDetails['reserveoffer'];

		$addPurchase = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		items_bought=items_bought+1 WHERE id='".$winnerDetails['bidderid']."'");
		$quantityRequested = $winnerDetails['quantity'];
		if ($quantityRequested > 0) {
			$amount = $quantityRequested*$amount;
			if ($quantityRequested<$totalQuantity) $quantityOffered = $quantityRequested;
			else $quantityOffered = $totalQuantity;
			$totalQuantity -= $quantityRequested;
		}
				
		$payerId = (@eregi('s', $fee['endauction_fee_applies'])) ? $auctionWonDetails['ownerid'] : $winnerDetails['bidderid'];

		if ($fee['is_endauction_fee']=="Y"&&!freeFees($payerId)) {
			// if account mode is on, then add the end auction fee to the seller's balance, and activate the details of the winner
			$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$payerId."'","payment_mode");
			if ($setts['account_mode_personal']==1) {
				$account_mode_local = ($tmp) ? 2 : 1;
			} else $account_mode_local = $setts['account_mode'];
			if ($account_mode_local==2) {
				$newBalance=0;
				$endauctionFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE 
				fee_from<=".$amount." AND fee_to>".$amount." AND fee_type='endauction' AND category_id='".$category_id."'");
		
				if ($endauctionFee['calc_type']=="percent") {
					$newBalance+=($endauctionFee['fee_amount']/100)*$amount;
				} else {
					$newBalance+=$endauctionFee['fee_amount'];
				}
				$payment_status="confirmed";
				$active=1;
				$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$payerId."'","balance");
				$updateSellerBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				balance=balance+".$newBalance." WHERE id='".$payerId."'");
				// add end of auction fee on the invoices table 
				$currentTime = time();
				$currentBalance += $newBalance;
				$insinv = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
				(userid,auctionid,feename,feevalue,feedate,balance)	VALUES 
				('".$payerId."','".$winnerDetails['auctionid']."','End of Auction Fee','".$newBalance."','".$currentTime."','".$currentBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				
			} else {
				$payment_status="unconfirmed";
				$active=0;
			}
		} else {
			$payment_status="confirmed";
			$active=1;
		}
		$delPreviousWinners = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_winners WHERE auctionid='".$winnerDetails['auctionid']."'");
		
		$saveWinner = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_winners 
		(sellerid,buyerid,amount,quant_req,quant_offered,
		auctionid,auctiontype,payment_status,purchase_date) VALUES 
		('".$auctionWonDetails['ownerid']."','".$winnerDetails['bidderid']."','".$amount."','".$quantityRequested."',
		'$quantityOffered','".$winnerDetails['auctionid']."','".$auctionWonDetails['auctiontype']."',
		'".$payment_status."','".time()."')");
		
		$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET rpwinner = '".$winnerDetails['bidderid']."', maxbid='".$amount."' WHERE id='".$winnerDetails['auctionid']."'");
		
		### add the buyer another purchase
		$addPurchase=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		items_bought=items_bought+1 WHERE id='".$winnerDetails['bidderid']."'");
			
		$sellerUsername=getSqlField("SELECT * FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","username");
		$buyerUsername=getSqlField("SELECT * FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","username");
		$prepareSellerFeedback = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
		(userid,usernick,fromid,submitted,auctionid,type) VALUES 
		('".$auctionWonDetails['ownerid']."','".$sellerUsername."','".$winnerDetails['bidderid']."',0,'".$winnerDetails['auctionid']."','sale')");
		$prepareBuyerFeedback = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
		(userid,usernick,fromid,submitted,auctionid,type) VALUES 
		('".$winnerDetails['bidderid']."','".$buyerUsername."','".$auctionWonDetails['ownerid']."',0,'".$winnerDetails['auctionid']."','purchase')");

		## variables needed for the included email files.
		$sellerId = getSqlField("SELECT id FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","id");
		$buyerId = getSqlField("SELECT id FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","id");
		$auctionId = $winnerDetails['auctionid'];
		$auctionName = getSqlField("SELECT itemname FROM probid_auctions WHERE id='".$auctionId."'","itemname");
		$maxBid = $amount;
	
		$email = getSqlField("SELECT email FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","email");;
		$name = getSqlField("SELECT name FROM probid_users WHERE id='".$auctionWonDetails['ownerid']."'","name");
		if ($setts['account_mode']!=2) include ($dir_parent."mails/notifysellercron.php");		

		$email = getSqlField("SELECT email FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","email");;
		$name = getSqlField("SELECT name FROM probid_users WHERE id='".$winnerDetails['bidderid']."'","name");
		if ($setts['account_mode']!=2) include ($dir_parent."mails/notifybuyercron.php");		
	}

	include ("themes/".$setts['default_theme']."/header.php");
	header5("$lang[fixedprofferfor] - ".$auction['itemname']); ?>
 <br> 
<? 
if ($enteredOffer == 1) echo "<p align=center>$lang[fixedofferenteredsucc]</p>";
else if ($enteredOffer == 2) echo "<p align=center>$lang[fixedofferenterederror]</p>";
?> 
<table width="100%"  border="0" cellspacing="0" cellpadding="0"> 
  <tr> 
    <td width="50%" valign="top"><table width="99%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
        <tr align="center" class="c1"> 
          <td><?=$lang[bidhistoryfor]?> 
            <?=$auction['itemname'];?></td> 
        </tr> 
      </table> 
      <table width="99%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
        <tr> 
          <td width="160" class="boldgrey"><?=$lang[username]?></td> 
          <td width="125" class="boldgrey" align="center"><?=$lang[bidamount]?></td> 
          <td align="center" class="boldgrey"><?=$lang[date]?></td> 
          <? if ($auction['quantity']>1) { ?> 
          <td align="center" class="boldgrey"><?=$lang[quant]?></td> 
          <? } ?> 
        </tr> 
        <tr class="c5"> 
          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
          <? if ($auction['quantity']>1) { ?> 
          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
          <? } ?> 
        </tr> 
        <?
		$isPrivate=$auction['private'];
		$currency=$auction['currency'];
		$getBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids_history WHERE 
		auctionid='".$_GET['id']."' ORDER BY id DESC");
		$cnt=0;
		while ($bidHistory = mysqli_fetch_array($getBidHistory)) {
			$cnt++;
			$bidderId[$cnt]=getSqlField("SELECT id FROM probid_users WHERE username='".$bidHistory['biddername']."'","id"); ?> 
        <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>"> 
          <td> <?
			$isOut = getSqlField("SELECT out FROM probid_bids WHERE id='".$bidHistory['bidid']."'","out");
			## this line was inserted to keep backwards compatibility
			## because bid id linkage was only inserted since v5.00
			if ($bidHistory['bidid']==0) $isOut = 1;
			
			echo ($isOut==0) ? "<strong>":""; 
			if ($isPrivate=="Y"&&$auction['ownerid']!=$_SESSION['memberid']) echo "$lang[bidderhidden]";
			else echo $bidHistory['biddername'].getFeedback($bidderId[$cnt]); 
			echo ($isOut==0) ? "</strong>":""; 
			echo (getSqlField("SELECT rpwinner FROM probid_bids WHERE id='".$bidHistory['bidid']."'","rpwinner")>0&&getSqlField("SELECT out FROM probid_bids WHERE id='".$bidHistory['bidid']."'","out")!=1) ? "[ <span class=greenfont>$lang[fixedpricewinner]</span> ]" : "";
			?> </td> 
          <td align="center"><? echo displayAmount($bidHistory['amount'],$currency);?></td> 
          <td align="center" class="smallfont"><? echo displaydatetime($bidHistory['date'],$setts['date_format']);?></td> 
          <? if ($_GET['quantity']>1) { ?> 
          <td align="center" class="contentfont"> <?=$bidHistory['quantity'];?> </td> 
          <? } ?> 
          <? } ?> 
      </table></td> 
    <td width="50%" valign="top" align="right"><table width="99%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
        <tr align="center" class="c1"> 
          <td><?=$lang[fixedproffer];?></td> 
        </tr> 
      </table> 
      <table width="99%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
        <tr> 
          <td width="150">&nbsp;</td> 
          <td>&nbsp;</td> 
        </tr> 
        <tr class="c2"> 
          <td><?=$lang[auc_id];?></td> 
          <td><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']));?>"># 
            <?=$auction['id'];?> 
            </a></td> 
        </tr> 
        <tr class="c3"> 
          <td><?=$lang[itemname];?></td> 
          <td><?=$auction['itemname'];?></td> 
        </tr> 
        <tr class="c2"> 
          <td><?=$lang[startbid];?></td> 
          <td><?=displayAmount($auction['bidstart'],$auction['currency']);?></td> 
        </tr> 
        <tr class="c3"> 
          <td><?=$lang[resprice];?></td> 
          <td><?=displayAmount($auction['rpvalue'],$auction['currency']);?></td> 
        </tr> 
        <tr class="c2"> 
          <td><strong> 
            <?=$lang[fixedproffer];?> 
            </strong></td> 
          <form action="reserveoffers.php" method="get"> 
            <input type="hidden" name="id" value="<?=$_GET['id'];?>"> 
            <td><? if ($_SESSION['memberid']==$auction['ownerid']) {
		  	echo $auction['currency']." <input type=\"text\" name=\"fixedoffer\" value=\"".$auction['reserveoffer']."\" size=\"10\"> 
			<input type=\"submit\" value=\"".$lang[submit]."\" name=\"enterofferok\"> ";
		  } else {
		  	echo ($auction['reserveoffer']>0) ? "<span class=greenfont><strong>".displayAmount($auction['reserveoffer'],$auction['currency'])."</strong></span>" : $lang[notyetset];
		  } ?></td> 
          </form> 
        </tr> 
      </table></td> 
  </tr> 
</table> 
<?
$getROffer = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_reserve_offers WHERE auctionid='".$_GET['id']."'");
$isROffer = mysqli_num_rows($getROffer);
if ($isROffer>0) { ?>
<br> 
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
  <tr align="center" class="c1"> 
    <td><?=$lang[fixedproffers]?> -  
      <?=$auction['itemname'];?></td> 
  </tr> 
</table> 
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
  <tr> 
    <td class="boldgrey"><?=$lang[username]?></td> 
    <td width="125" class="boldgrey" align="center"><?=$lang[bidamount]?></td> 
    <td width="150" align="center" class="boldgrey"><?=$lang[accepted]?></td> 
    <td width="140" align="center" class="boldgrey"><?=$lang[acceptance_date]?></td> 
    <? if ($_SESSION['memberid']==$auction['ownerid']) { ?>
	<td width="130" align="center" class="boldgrey"><?=$lang[options]?></td> 
	<? } ?>
  </tr> 
  <tr class="c5"> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <? if ($_SESSION['memberid']==$auction['ownerid']) { ?>
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
	<? } ?>
  </tr> 
  <? while ($roffer=mysqli_fetch_array($getROffer)) { ?> 
  <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>"> 
    <td><?=getSqlField("SELECT username FROM probid_users WHERE id='".$roffer['bidderid']."'","username"); ?> </td> 
    <td align="center"><? echo displayAmount($roffer['bidamount'],$auction['currency']);?></td> 
    <td align="center" class="smallfont"><? 
	echo ($roffer['accepted']==0) ? "<span class=redfont>$lang[no]</span>" : "<span class=greenfont>$lang[yes]</span>";
	echo ($roffer['accepted']==0&&$roffer['bidderid']==$_SESSION['memberid']) ? " [ <a href=\"reserveoffers.php?id=".$_GET['id']."&option=accept\"><strong>$lang[accept]</strong></a> ]" : "";
	?></td> 
    <td align="center" class="smallfont"><? echo ($roffer['regdate']>0) ? date($setts['date_format'],$roffer['regdate']) : $lang[na];?></td> 
    <? if ($_SESSION['memberid']==$auction['ownerid']) { ?>
	<td align="center">
	<? echo ($roffer['accepted']==1) ? "[ <a href=\"reserveoffers.php?id=".$_GET['id']."&option=declare_winner&offerid=".$roffer['id']."\">$lang[declwinner]</a> ]":""; ?>
	</td> 
	<? } ?>
  </tr>
  <? } ?> 
</table> 
<? } ?>
<br>
<div align="center" class="contentfont"><a href="<?=processLink('auctiondetails', array('id' => $_GET['id']));?>"> 
  <?=$lang[retdetailspage]?> 
  </a></div> 
<br> 
<? 	} else {
		echo "<div class=\"contentfont\" align=\"center\">$lang[invalidresopt]</span>";
		echo "<div align=\"center\" class=\"contentfont\"><a href=\"".processLink('auctiondetails', array('id' => $_GET['id']))."\">$lang[retdetailspage]</a></div> ";
	}
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?> 
