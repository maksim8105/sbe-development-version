﻿<?
## v5.25b -> jul. 11, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

$winDetails = getSqlRow("SELECT * FROM probid_winners WHERE id='".$_REQUEST['winnerid']."'");
$buyerDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$winDetails['buyerid']."'");
$sellerDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$winDetails['sellerid']."'");
$auctionDetails = getSqlRow("SELECT * FROM probid_winners WHERE auctionid='".$winDetails['auctionid']."'");

if (isset($_POST['message']) && !empty($_POST['message'])) {
	$updFlag = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_messages(senderid,receiverid,datentime,message,messagetitle, auctionid) VALUES
	(".$_SESSION['memberid'].", '".$_REQUEST['winnerid']."', ".time().", '".remSpecialChars($_POST['message'])."''".$_REQUEST['auctionid']."')");

	if($_SESSION['memberid']==$sellerDetails['id']){
		$username = $buyerDetails['name'];
		$mailto = $buyerDetails['email'];
	} else {
		$username = $sellerDetails['name'];
		$mailto = $sellerDetails['email'];
	}

	$plainMessage = 	"NB! Message encoding: UTF-8		\n".
						"									\n".
						"Hea ".$username.",					\n ".
						"Teadetetahvlis on uus teade Teile. Palun külastage linki \n".
                    	"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$_REQUEST['winnerid']."\n".
						"									\n".
						"------------------------------		\n".
						"									\n".
						"Здравствуйте, ".$username.",\n ".
						"В доске объявлений новое сообщение для Вас. Пожалуйста, пройдите по ссылке \n".
                    	"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$_REQUEST['winnerid']."\n".
						"									\n".
						"------------------------------		\n".
						"									\n".
						"Dear ".$username.",\n ".
						"There is a new message in your message board. Please follow the link \n".
                    	"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$_REQUEST['winnerid']."\n";

	$htmlMessage = 		"NB! Message encoding: UTF-8		<br>".
						"									<br>".
						"Hea ".$username.",	<br> ".
                    	"Teadetetahvlis on uus teade Teile. Palun <br>".
                    	"<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$_REQUEST['winnerid']."\">klikkige siia</a>, et minna üle oma teadetetahvli juurde. <br>".
						"									<br>".
						"-------------------------------	<br>".
						"									<br>".
						"Здравствуйте, ".$username.", <br> ".
                    	"В доске объявлений новое сообщение для Вас. Пожалуйста, <br> ".
                    	"<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$_REQUEST['winnerid']."\">нажмите сюда</a>, чтобы перейти к доске объявлений. <br>".
						"									<br>".
						"-------------------------------	<br>".
						"									<br>".
						"Dear ".$username.", <br> ".
                    	"There is a new message in your message board. Please <br> ".
                    	"<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$_REQUEST['winnerid']."\">click here</a> to go to your message board. <br>";

	htmlmail($mailto,"$setts[sitename] New Message",$plainMessage,$setts['adminemail'],$htmlMessage);

	echo "<script>document.location.href='msgboard.php?winnerid=".$_REQUEST['winnerid']."'</script>";
}

include ("themes/".$setts['default_theme']."/header.php");

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");

include("membersmenu.php");

header7($lang[message_board_title]);

if ($sellerDetails['id']==$_SESSION['memberid']) {
	echo '<table width="100%" border="0" cellpadding="4" cellspacing="2" class="border"> ';
	echo '	<tr> ';
	echo '      <td class="c1" colspan="2"> ';
	## display buyer contact detail
	echo '			<strong>'.$lang[buyercontactdets].'</strong></td></tr>';
      echo '	<tr class="c3"><td nowrap><b>'.$lang[email].':</b></td><td>'.$buyerDetails['email'].'</td></tr>';
	echo '</table> ';
} else if ($buyerDetails['id']==$_SESSION['memberid']) {
	## display seller contact details
	echo '<table width="100%" border="0" cellpadding="4" cellspacing="2" class="border"> ';
	echo '	<tr> ';
	echo '      <td class="c1" colspan="2"> ';
	## display buyer contact detail
	echo '			<strong>'.$lang[sellercontactdets].'</strong></td></tr>';
      echo '	<tr class="c3"><td nowrap><b>'.$lang[email].':</b></td><td>'.$sellerDetails['email'].'</td></tr>';
	echo '</table> ';
}
?>

<script language="javascript">
	function enableBtn(theform) {
		if (theform.message.value!='') theform.submitbtn.disabled = false;
		else theform.submitbtn.disabled = true;
	}
</script>
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="border">
   <?
	$getSoldItem = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_winners WHERE ((sellerid='".$_SESSION['memberid']."' AND s_deleted!=1) OR 
	(buyerid='".$_SESSION['memberid']."' AND b_deleted!=1)) AND id='".$_REQUEST['winnerid']."'");
	
	if(!($soldItem=mysqli_fetch_array($getSoldItem))) {
		echo "<script>document.location.href='index.php'</script>";
	}

	$getMessages = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_messages WHERE receiverid='".$_REQUEST['winnerid']."' ORDER BY datentime DESC");
	
	$auction = getSqlRow("SELECT a.* FROM probid_auctions a, probid_winners w WHERE w.auctionid=a.id AND w.id='".$_REQUEST['winnerid']."'");

	while ($messages = mysqli_fetch_array($getMessages)) {
		$count = 0;
		$getAuthor = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT username FROM probid_users WHERE id=".$messages['sender']);
		$author = mysqli_fetch_array($getAuthor); ?>
   <tr class="c4">
      <td><table cellpadding="4" cellspacing="1" border="0" width="100%">
            <tr class="c3">
               <td width="150" align="center"><?=' <b>'.$author['username'].'</b>';?>
                  (
                  <?=(($soldItem['sellerid']==$messages['sender'])?$lang[m_seller]:$lang[m_buyer]);?>
                  ) </td>
               <td><?=date($setts['date_format'],$messages['datentime']);?></td>
            </tr>
            <tr <?=(($soldItem['sellerid']==$messages['senderid'])?'':'class="c2"');?>>
               <td width="150"></td>
               <td valign="top"><?=str_replace("\r\n","<br>",addSpecialChars($messages['message']));?>
               </td>
            </tr>
         </table></td>
   </tr>
   <? } ?>
   <form action="msgboard.php" method="post" name="msgform">
      <input type="hidden" name="winnerid" value="<?=$_REQUEST['winnerid'];?>">
      <tr class="c4">
         <td align="center"><textarea id="message" style="width:99%" name="message" rows="5" onkeyup="enableBtn(msgform);"></textarea>
            <br>
            <input type="submit" value="<?=$lang[m_add_message];?>" name="submitbtn" id="submitbtn" disabled></td>
      </tr>
   </form>
</table>
<br />
<table width="100%" border="0" cellpadding="3" cellspacing="2" class="subitem">
   <?
	$winnerLogged = getSqlRow("SELECT * FROM probid_winners WHERE buyerid='".$_SESSION['memberid']."' AND auctionid='".$auction['id']."'");

	if ($auction['acceptdirectpayment']&&($auction['closed']!=0||$auction['auctiontype']=="dutch")&&$auction['ownerid']!=$_SESSION['memberid']&&$winnerLogged&&$winnerLogged['payment_status']=="confirmed") { 
    	$dpPayment = getSqlRow("SELECT amount, quant_req, quant_offered FROM probid_winners WHERE
		buyerid='".$_SESSION['memberid']."' AND auctionid='".$auction['id']."'");
		$dpQuant = 0;
		$postageC = 0;
		$dpQuant = ($dpPayment['quant_offered']>0) ? $dpPayment['quant_offered'] : 1;
		$paymentAmount = $dpQuant * $dpPayment['amount'];
		$paymentAmount += $dpQuant * $auction['postage_costs'];
	
		if (is_numeric($paymentAmount) && $paymentAmount>0) { ?>
   <!-- Header Direct Payment -->
   <tr height="21">
      <td colspan="5" class="c4"><b>&raquo; <? echo (strtoupper($lang[headdirectpayment]));?></b></td>
   </tr>
   <tr>
      <td colspan="5" class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr>
      <td colspan="5"><!-- End Header Direct Payment -->
         <table width="100%" border="0" cellspacing="1" cellpadding="0" class="c2">
            <tr valign="top">
               <td width="100%"><?
						if ($winnerLogged['directpayment_paid']||$winnerLogged['flag_paid']) {
							echo "<center>" . $lang[paiddirectpayment] . "</center>";
						} else {
							echo "<div style='padding: 6px;'>".$lang[you_won_item2]."</div>";
        					$acceptedPaymentSystems = explode(',',$auction['accept_payment_systems']);
							$paymentAmount=number_format($paymentAmount,2,'.','');
							$returnUrl=$path."paymentdone.php";
							$failureUrl=$path."paymentfailed.php";
						  	$paymentDetails = getSqlRow("SELECT * FROM probid_users WHERE id=".$winnerLogged['sellerid']);
     						$pmDesc = $lang[itempayment].': [ID#'.$auction['id'].'] '.$auction['itemname'];
						  	if($paymentDetails['paypalemail'] && in_array('1',$acceptedPaymentSystems)) {
								$notifyUrl=$path."paymentprocess.php?table=100";
            				paypalForm($_REQUEST['winnerid'],$paymentDetails['paypalemail'],$paymentAmount,$auction['currency'],$returnUrl,$failureUrl,$notifyUrl,100,$pmDesc,TRUE,'paydirpayment.php?s=paypal&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['worldpayid'] && in_array('2',$acceptedPaymentSystems)) {
								$notifyUrl=$path."worldpayprocess.php?table=100";
            				worldpayForm($_REQUEST['winnerid'],$paymentDetails['worldpayid'],$paymentAmount,$auction['currency'],$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=worldpay&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['nochexemail'] && in_array('4',$acceptedPaymentSystems)) {
           	 				$notifyUrl=$path."nochexprocess.php?table=100";
            				nochexForm($_REQUEST['winnerid'],$paymentDetails['nochexemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=nochex&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['checkoutid'] && in_array('5',$acceptedPaymentSystems)) {
            				//$notifyUrl=$path."checkoutprocess.php?table=100";
            				checkoutForm($_REQUEST['winnerid'],$paymentDetails['checkoutid'],$paymentAmount,100,TRUE,'paydirpayment.php?s=checkout&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['ikoboid']&&$paymentDetails['ikoboipn'] && in_array('3',$acceptedPaymentSystems)) {
            				$notifyUrl=$path."ikoboprocess.php?table=100";
								ikoboForm($_REQUEST['winnerid'],$paymentDetails['ikoboid'],0,$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=ikobo&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['protxname']&&$paymentDetails['protxpassword'] && in_array('7',$acceptedPaymentSystems)) {
            				$notifyUrl=$path."protxprocess.php?table=100";
            				protxForm($_REQUEST['winnerid'],$paymentAmount,$auction['currency'],$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=protx&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['authnetid']&&$paymentDetails['authnettranskey'] && in_array('6',$acceptedPaymentSystems)) {
            				//$notifyUrl=$path."authorize.net.process.php?table=100";
            				authorizeNetForm($_REQUEST['winnerid'],$paymentAmount,100,TRUE,'paydirpayment.php?s=authorize&winnerid='.$_REQUEST['winnerid']);
        					}
						} ?>
               </td>
            </tr>
         </table></td>
   </tr>
   <? } 
	} ?>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
