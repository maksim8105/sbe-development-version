<?php
## v5.24 -> may. 08, 2006
session_start();
include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php");


if ($setts['h_counter']==1) {
   	 	$nbShops=getSqlNumber("SELECT id FROM probid_users WHERE store_active=1");
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		$nbFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1","Total");
		$nbRegisteredUsers24h=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-86400));
		$nbRegisteredUsers7d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-604800));
		$nbRegisteredUsers30d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-2592000));
		$nbRegisteredUsers1y=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-31536000));
		$nbopenauctions24h=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbopenauctions7d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbopenauctions30d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbopenauctions1y=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbtotalauctions=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_auctions","Total1");
		$nbauctionsAmount24h=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'","Total1");
		$nbauctionsAmount7d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'","Total1");
		$nbauctionsAmount30d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'","Total1");
		$nbauctionsAmount1y=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'","Total1");
		$nbauctionsFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions","Total1");
		$nbsolds24h=getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3");
		$nbsolds7d=getSqlField("SELECT SUM(amount) AS 'Total4' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-604800),"Total4");
		$nbsolds30d=getSqlField("SELECT SUM(amount) AS 'Total5' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total5");
		$nbsolds1y=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total6");
		$nbsoldsTotal=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners","Total6");
		$nbWantAds24h=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbWantAds7d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbWantAds30d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbWantAds1y=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbWantAdsTotal=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_wanted_ads","Total1");
		if ($nbsolds24h>0) $effect24h=($nbsolds24h/$nbauctionsAmount24h)*100; else $effect24h=0;
		if ($nbsolds7d>0) $effect7d=($nbsolds7d/$nbauctionsAmount7d)*100; else $effect7d=0;
		if ($nbsolds30d>0) $effect30d=($nbsolds30d/$nbauctionsAmount30d)*100; else $effect30d=0;
		if ($nbsolds1y>0) $effect1y=($nbsolds1y/$nbauctionsAmount1y)*100; else $effect1y=0;
		if ($nbsoldsTotal>0) $effecttotal=($nbsoldsTotal/$nbauctionsFullAmount)*100; else $effecttotal=0;
		
		/*header5("$lang[Site_status]");*/
include "stat_menu.php";	
$ShopsQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE store_active=1 AND allow_stat=1");
echo "Статистика позволяет увидеть наиболее популярные магазины и количество клиентов, уже совершивших в них покупку.

Внимание: в статистику добавляются только те магазины, владельцы которых разрешили публикацию данных на данной страницы. Для разрешения публикации статистики Вашего магазина в общем рейтинге, пожалуйста установите соответствующие настройки на странице Вашего магазина. <table class='c2'>
<TR class='c1'><td colspan=6></td><td align='center' colspan=4>Käive</td></tr>
<TR class='c1'>
<TD align='center'>Nimi</TD>
<TD align='center' width=5%>Klientide arv</TD>
<TD align='center' width=5%>Müükide arv</TD>
<TD align='center' width=5%>Esemete arv</TD>
<TD align='center' width=5%>Reiting</TD>
<TD align='center'>24 tundi</TD>
<TD align='center'>7 päeva</TD>
<TD align='center'>30 päeva</TD>
<TD align='center'>1 aasta</TD>
<TD align='center'>Kokku</TD</TR>";
while ($ShopDetails = mysqli_fetch_array($ShopsQuery)) {	
/*echo $ShopDetails['id'];*/
	echo "<tr class='";
	echo (($count++)%2==0) ? "c2":"c3";
	echo "'><td align='right'>".$ShopDetails['store_name']."</td><td align='right'>".getsqlfield("SELECT COUNT(DISTINCT buyerid) AS 'Total1' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."'","Total1")."</TD>".
		"<td align='right'>".getsqlfield("SELECT COUNT(DISTINCT id) AS 'Total2' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."'","Total2")."</td>".
		"<td align='right'>".$ShopDetails['nb_items']."</td>".
		"<td align='right'>".calcFeedback($ShopDetails['id'])."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-604800),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."'","Total3"),2,',',' ')."</td></tr>";
		}
		}
echo "</table>";
	
include ("themes/".$setts['default_theme']."/footer.php"); ?>