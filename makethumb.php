<?php
// 'interface' to phpThumb.php
define('INCLUDED', 1);
$src=$_GET['pic'];
$sq=$_GET['sq'];
$w=$_GET['w'];
$h=$_GET['h'];
$b=$_GET['b'];
$bg='FFFFFF';
$q=100;
$redir=false;
// echo $src;
if(strstr($src, 'http://') || strstr($src, 'ftp://'))
{
	$src = rawurlencode($src);
	$redir=true;
	// header("Location: thumb/phpThumb.php?src=$src");
}
elseif(!file_exists(realpath($src)))
{
	header('Location: themes/v52/img/system/noimg.gif');
	return;
}
else
{
	$src = '../'.$src;
}

if($sq == 'Y')
{	
	if(isset($h))
	{
		$w = $h;
	}
	elseif(isset($w))
	{
		$h = $w;
	}
	else
	{
		die('dimensions not set');
	}
}

$_GET = array(src => $src, q => $q, w => $w, h => $h, bg => $bg);
$phpthumb_params="?src=$src&q=$q&w=$w&h=$h&bg=$bg";

if($sq == 'Y')
{	
	$_GET['far'] = 'C';
	$phpthumb_params .= '&far=C';
}

if($b=='Y')
{
	$_GET['fltr'] = array('bord'); //bord|1 changes the behavior with non-squared images
	$phpthumb_params .= '&fltr[]=bord';
}
else
{
	$_GET['fltr'] = array('');
	$phpthumb_params .= '&fltr[]';
}

// print_r($_GET);
//echo dirname(__FILE__);
if($redir)
	header('Location: thumb/phpThumb.php'.$phpthumb_params); //does not work if src is http link and included
else
	include('thumb/phpThumb.php');

?>