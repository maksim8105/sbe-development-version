<?php
## v5.24 -> may. 08, 2006
session_start();
include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php");


if ($setts['h_counter']==1) {
   	 	$nbShops=getSqlNumber("SELECT id FROM probid_users WHERE store_active=1");
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		$nbFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1","Total");
		$nbRegisteredUsers24h=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-86400));
		$nbRegisteredUsers7d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-604800));
		$nbRegisteredUsers30d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-2592000));
		$nbRegisteredUsers1y=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-31536000));
		$nbopenauctions24h=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbopenauctions7d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbopenauctions30d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbopenauctions1y=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbtotalauctions=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_auctions","Total1");
		$nbauctionsAmount24h=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'","Total1");
		$nbauctionsAmount7d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'","Total1");
		$nbauctionsAmount30d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'","Total1");
		$nbauctionsAmount1y=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'","Total1");
		$nbauctionsFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions","Total1");
		$nbsolds24h=getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3");
		$nbsolds7d=getSqlField("SELECT SUM(amount) AS 'Total4' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-604800),"Total4");
		$nbsolds30d=getSqlField("SELECT SUM(amount) AS 'Total5' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total5");
		$nbsolds1y=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total6");
		$nbsoldsTotal=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners","Total6");
		$nbWantAds24h=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbWantAds7d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbWantAds30d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbWantAds1y=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbWantAdsTotal=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_wanted_ads","Total1");
		if ($nbsolds24h>0) $effect24h=($nbsolds24h/$nbauctionsAmount24h)*100; else $effect24h=0;
		if ($nbsolds7d>0) $effect7d=($nbsolds7d/$nbauctionsAmount7d)*100; else $effect7d=0;
		if ($nbsolds30d>0) $effect30d=($nbsolds30d/$nbauctionsAmount30d)*100; else $effect30d=0;
		if ($nbsolds1y>0) $effect1y=($nbsolds1y/$nbauctionsAmount1y)*100; else $effect1y=0;
		if ($nbsoldsTotal>0) $effecttotal=($nbsoldsTotal/$nbauctionsFullAmount)*100; else $effecttotal=0;
		
		/*header5("$lang[Site_status]");*/
	echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang[Site_status]."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";
    echo "Üldine ! Kasutajad ! Ostusoovid ! Muuk ! E-poed ! <BR><BR>";
    	echo "
			<table align='center'>
       		<tr class='c3'> 
          		<td align='right'><b>".$nbShops."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>$lang[stores]</font></td>
        	</tr>
       		<tr class='c3'> 
          		<td align='right'><b>".$nbLiveAuctions."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>$lang[live_auctions]</font></td>
        	</tr>
        	<tr class='c3'> 
          		<td align='right'><b>".number_format($nbFullAmount,2,',',' ')."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>Müüdava kauba kogusumma</font></td>
        	</tr>";
        	
		if ($setts['enable_wantedads']=="Y") {	
        echo "<tr class='c3'> 
          		<td  align='right'><b>".$nbLiveWantAds."</b></td>
          		<td >&nbsp;<font style='font-size: 10px;'>$lang[live_wantads]</font></td>
        	</tr>";}
        echo "<tr class='c3'> 
          		<td align='right'><b>".$nbUsers."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>$lang[registered_users]</font></td>
        	</tr>
        	<tr class='c3'> 
          		<td align='right'><b>"; 
          		
          		include ("online.php");
          		echo "</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>Kasutajaid on-line</font></td>
        	</tr>";
		
	if ($nbauctionsAmount24h>0) 
	$nbauctionsAmount24h=$nbauctionsAmount24h;
	else $nbauctionsAmount24h=0;
		echo "</table><BR><div align='center' <h1>Trendid</h1></div><BR>
		<table align='center' class='c2'><tr class='c1'> 
          		<td align='center'>&nbsp;<font style='font-size: 10px;'></font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>24 tundi</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>7 päeva</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>30 päeva</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>1 aasta</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>Kokku</font></td>
        	</tr>
        	<tr> 
          		<td align='right'><b></b>Registreerimisi</td>
          		<td align='center'><b>".$nbRegisteredUsers24h."</b></td>
          		<td align='center'><b>".$nbRegisteredUsers7d."</b></td>
          		<td align='center'><b>".$nbRegisteredUsers30d."</b></td>
          		<td align='center'><b>".$nbRegisteredUsers1y."</b></td>
          		<td align='center'><b>".$nbUsers."</b></td>
          		
        	</tr>
        	<tr class='c1'> 
          		<td align='right'><b></b>Avatud oksjoneid</td>
          		<td align='center'><b>".$nbopenauctions24h."</b></td>
          		<td align='center'><b>".$nbopenauctions7d."</b></td>
          		<td align='center'><b>".$nbopenauctions30d."</b></td>
          		<td align='center'><b>".$nbopenauctions1y."</b></td>
          		<td align='center'><b>".$nbtotalauctions."</b></td>
        	</tr>
        	<tr> 
          		<td align='right'><b></b>Avatud oksjonite kogusumma</td>
          		<td align='right'><b>".number_format($nbauctionsAmount24h,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbauctionsAmount7d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbauctionsAmount30d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbauctionsAmount1y,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbauctionsFullAmount,2,',',' ')."</b></td>
        	</tr>
        		<tr> 
          		<td align='right'><b></b>Müüdud kauba kogusumma</td>
          		<td align='right'><b>".number_format($nbsolds24h,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsolds7d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsolds30d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsolds1y,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsoldsTotal,2,',',' ')."</b></td>
        	</tr>
        	<tr class='c3'> 
          		<td align='right'><b></b>%</td>
          		<td align='right'><b>".number_format($effect24h,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effect7d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effect30d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effect1y,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effecttotal,2,',',' ')."</b></td>
        	</tr>
        	<tr class='c1'> 
          		<td align='right'><b>Avaldatud ostusoove</b></td>
          		<td align='center'><b>".$nbWantAds24h."</b></td>
          		<td align='center'><b>".$nbWantAds7d."</b></td>
          		<td align='center'><b>".$nbWantAds30d."</b></td>
          		<td align='center'><b>".$nbWantAds1y."</b></td>
          		<td align='center'><b>".$nbWantAdsTotal."</b></td>
        	</tr></table>
        ";
echo "Kasutajad
    <TABLE class='c2'>
	<TR><TD>Kokku</TD><TD>Kokku</TD></TR>
	<TR><TD>Autenditud</TD><TD>Kokku</TD></TR>
	</table>
	TOP 5 riigid
	<table>
	<TR><TD>Eesti</TD><TD>Kokku</TD></TR>
	<TR><TD>Venemaa</TD><TD>Kokku</TD></TR>
	<TR><TD>Läti</TD><TD>Kokku</TD></TR>
	<TR><TD>Kokku</TD><TD>Kokku</TD></TR>
	</table>";

$ShopsQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE store_active=1 AND allow_stat=1");
echo "E-poed. Tähelepanu. Kui soovite teha oma e-poe statistikat avalikuks, pange vastavad hääletused oma kontol (leht e-pood) <table class='c2'>
<TR class='c1'><td colspan=6></td><td align='center' colspan=4>Käive</td></tr>
<TR class='c1'>
<TD align='center'>Nimi</TD>
<TD align='center' width=5%>Klientide arv</TD>
<TD align='center' width=5%>Müükide arv</TD>
<TD align='center' width=5%>Esemete arv</TD>
<TD align='center' width=5%>Reiting</TD>
<TD align='center'>24 tundi</TD>
<TD align='center'>7 päeva</TD>
<TD align='center'>30 päeva</TD>
<TD align='center'>1 aasta</TD>
<TD align='center'>Kokku</TD</TR>";
while ($ShopDetails = mysqli_fetch_array($ShopsQuery)) {	
/*echo $ShopDetails['id'];*/
	echo "<tr class='";
	echo (($count++)%2==0) ? "c2":"c3";
	echo "'><td align='right'>".$ShopDetails['store_name']."</td><td align='right'>".getsqlfield("SELECT COUNT(DISTINCT buyerid) AS 'Total1' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."'","Total1")."</TD>".
		"<td align='right'>".getsqlfield("SELECT COUNT(DISTINCT id) AS 'Total2' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."'","Total2")."</td>".
		"<td align='right'>".$ShopDetails['nb_items']."</td>".
		"<td align='right'>".calcFeedback($ShopDetails['id'])."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-604800),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."' AND purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total3"),2,',',' ')."</td>".
		"<td align='right'>".number_format(getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE sellerid='".$ShopDetails['id']."'","Total3"),2,',',' ')."</td></tr>";
		}
		}
echo "</table>";
include ("themes/".$setts['default_theme']."/footer.php"); ?>