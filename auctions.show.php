<?
## v5.25 -> jun. 27, 2006
session_start();

if (!$_GET['option']) $_GET['option']="recent";

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");
if ($_GET['option']=="featured") $head = $lang[Featured_Auctions];
else if ($_GET['option']=="recent") $head = $lang[Recently_Listed_Auction];
else if ($_GET['option']=="highbid") $head = $lang[Recently_Bid_On];
else if ($_GET['option']=="ending") $head = $lang[Ending_Soon];

header5($head);

if (!$_REQUEST['orderField']) $orderField = "enddate";
else $orderField=$_REQUEST['orderField'];
if (!$_REQUEST['orderType']) {
	$orderType = "ASC";
	$newOrder="DESC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}

$query = "SELECT id, itemname, maxbid, nrbids, enddate, startdate, bidstart, picpath, currency, bn, bnvalue FROM probid_auctions";

$tomorrow = date("Y-m-d H:i:s",time()+86400);
$yesterday = date("Y-m-d H:i:s",time()-86400);

if ($_GET['option']=="featured") 
	$query .= " WHERE hpfeat='Y' AND closed=0 AND active=1 AND deleted!=1 AND listin!='store' ORDER BY startdate DESC";
else if ($_GET['option']=="recent") 
	$query .= " WHERE closed=0 AND active=1 AND deleted!=1 AND listin!='store' ORDER BY startdate DESC";
else if ($_GET['option']=="highbid") 
	$query .= " WHERE maxbid>0 AND closed=0 AND active=1 AND deleted!=1 AND listin!='store' ORDER BY maxbid DESC";
else if ($_GET['option']=="ending") 
	$query .= " WHERE closed=0 AND active=1 AND deleted!=1 AND enddate>'".$timeNowMysql."' AND listin!='store' ORDER BY enddate ASC";

if ($_GET['start'] == "") $start = 0;
else $start = $_GET['start'];
$limit = 50;

$totalAuctions = getSqlNumber($query); 
$auctionsQuery = mysqli_query($GLOBALS["___mysqli_ston"], $query." LIMIT ".$start.",".$limit."");

$additionalVars = "&option=".$_REQUEST['option'];
?>

<br>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
   <tr class="c1" >
      <td width="50" align="center" style="font-size: 10px;"><?=$lang[picture]?></td>
      <td style="font-size: 10px;" class="submenu"><?=$lang[auc_name]?></td>
      <td width="120" align="center" style="font-size: 10px;" class="submenu"><?=$lang[currbid]?></td>
		<? /*if ($setts['buyout_process']==0) { ?>
      <td width="120" align="center" style="font-size: 10px;" class="submenu"><?=$lang[buyout]?></td>
		<? } */?>
      <td width="70" align="center" style="font-size: 10px;" class="submenu"><?=$lang[num_bids]?></td>
      <td width="120" align="center" style="font-size: 10px;" class="submenu"><?=$lang[endsin]?></td>
   </tr>
   <tr class="c5">
      <td><img src="images/pixel.gif" width="1" height="1"></td>
      <td><img src="images/pixel.gif" width="1" height="1"></td>
      <td><img src="images/pixel.gif" width="1" height="1"></td>
		<? if ($setts['buyout_process']==0) { ?>
      <td><img src="images/pixel.gif" width="1" height="1"></td>
      <td><img src="images/pixel.gif" width="1" height="1"></td>
		<? } ?>
      
   </tr>
   <?
	if ($totalAuctions==0) {
		echo 	"<tr>																				\n".
				"	<td colspan=5 class=contentfont align=center>".$lang[noauctionsthiscat]."</td>	\n".
				"</tr>";
	} else {
		while ($auctionDetails = mysqli_fetch_array($auctionsQuery)) { ?>
   <tr height="25" class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>">
      <td width="100" align="center"><? 
			echo "<a href=\"".processLink('auctiondetails', array('itemname' => $auctionDetails['itemname'], 'id' => $auctionDetails['id']))."\">";
			echo "<img src=\"$setts[siteurl]/makethumb.php?pic=".((!empty($auctionDetails['picpath'])) ? $auctionDetails['picpath'] : "themes/".$setts['default_theme']."/img/system/noimg.gif")."&w=100&sq=Y&b=Y\" border=\"0\"";
			echo "</a>"; ?>
      </td>
      <td><span class="<? echo setStyle($auctionDetails['id']);?>"><a href="<?=processLink('auctiondetails', array('itemname' => $auctionDetails['itemname'], 'id' => $auctionDetails['id'])); ?>">
         <?=$auctionDetails['itemname'];?>
         </a><? echo itemPics($auctionDetails['id']);?></span> </td>
      <td align="center"><? echo displayAmount((($auctionDetails['maxbid']==0)?$auctionDetails['bidstart']:$auctionDetails['maxbid']),$auctionDetails['currency']);?> </td>
		<? /* if ($setts['buyout_process']==0) { ?>
      <td align="center"><? echo (@eregi('Y',$auctionDetails['bn'])) ? displayAmount($auctionDetails['bnvalue'],$auctionDetails['currency'],'YES') : $lang[na];?></td>
		<? } */?>
      <td align="center"><?=$auctionDetails['nrbids'];?></td>
      <td align="center"><? echo timeleft($auctionDetails['enddate'],$setts['date_format']);?></td>
   </tr>
   <? } ?>
   <tr class="c4">
      <td align="center" class="contentfont" colspan="7"><? paginate($start,$limit,$totalAuctions,"auctions.show.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); ?></td>
   </tr>
   <? } ?>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>
