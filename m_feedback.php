<? 

## v5.24 -> apr. 05, 2006



if ($_SESSION['membersarea']=="Active") { 

	if ( !defined('INCLUDED') ) { die("Access Denied"); }



	## first we delete obsolete feedbacks (instead of cron where it would slow the whole site down)

	$getExpiredFeedbacks = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, auctionid FROM probid_feedbacks WHERE submitted=0 AND (fromid='".$_SESSION['memberid']."' OR userid='".$_SESSION['memberid']."')");

	

	$fbToDel = "";

	$isExp = FALSE;

	while ($expFdb = mysqli_fetch_array($getExpiredFeedbacks)) {

		$isAuction = getSqlNumber("SELECT id FROM probid_auctions WHERE id='".$expFdb['auctionid']."'");

		if (!$isAuction) {

			$fbToDel.=$expFdb['id']." ";

			$isExp = TRUE;

		}

	}

	

	if ($isExp) {

		$fbToDel="(".@eregi_replace(" ",",",trim($fbToDel)).")";

		$delExpiredFb = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_feedbacks WHERE submitted=0 AND id IN ".$fbToDel."");

	}



	$limit = 20;



	if ($_GET['srctype']=="fbleft") {

		$start = $_GET['start'];

	} else {

		$start = 0;

	}



	$additionalVars = "&page=feedback&srctype=fbleft";



	$getFeedback=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks 

	WHERE userid='".$_SESSION['memberid']."' AND submitted=1 

	ORDER BY date DESC LIMIT ".$start.", ".$limit);

	$nbFeedbacks = getSqlNumber("SELECT userid FROM probid_feedbacks 

	WHERE userid='".$_SESSION['memberid']."' AND submitted=1");

	

	if ($nbFeedbacks>0) { ?>

<br>

<? $isCRF = getSqlNumber("SELECT id FROM probid_custom_rep WHERE active=1"); ?>

<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">

  <tr>

    <td><table width="100%" border="0" cellpadding="3" cellspacing="1">

        <tr>

          <td class="c7 smallfont" colspan="6"><b>&nbsp;&nbsp;&raquo;&nbsp;

            <?=$lang[feedback_title]?>

            </b></td>

        </tr>

        <tr>

          <td width="10%" class="boldgrey"><?=$lang[from]?></td>

          <td width="10%" align="center" class="boldgrey"><?=$lang[rate]?></td>

          <td width="20%" align="center" class="boldgrey"><?=$lang[date]?></td>

          <td class="boldgrey"><?=$lang[feedback]?></td>

          <? if ($isCRF) { ?>

          <td width="10%" class="boldgrey" align="center"><?=$lang[details]?></td>

          <? } ?>

          <td width="10%" class="boldgrey" align="center"><?=$lang[type]?></td>

        </tr>

        <tr class="c5">

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <? if ($isCRF) { ?>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <? } ?>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

        </tr>

        <? while ($feedback = mysqli_fetch_array($getFeedback)) { ?>

        <tr class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>">

          <td><?

			$fromUsername = getSqlField("SELECT * FROM probid_users WHERE id='".$feedback['fromid']."'","username");

			echo $fromUsername;

			?></td>

          <td align="center" class="smallfont"><? echo showFeedback($feedback['rate']);?> </td>

          <td align="center" class="smallfont" nowrap><? echo format_date($feedback['date'],1);?> </td>

          <td  class="smallfont"><?=$feedback['feedback'];?></td>

          <? if ($isCRF) { ?>

          <td class="smallfont" align="center">[ <strong><a href="javascript://" onclick="popUp('repdetails.php?fbid=<?=$feedback['id'];?>');"><?=$lang[details];?></a></strong> ]</td>

          <? } ?>

          <td align="center"  class="smallfont"><?=$feedback['type'];?></td>

        </tr>

        <? } ?>

        <tr>

          <td align="center" colspan="6" class="contentfont c4"><? paginate($start,$limit,$nbFeedbacks,"membersarea.php",$additionalVars);?></td>

        </tr>

      </table>

    </td>

  </tr>

</table>

<? } else echo "<div align=center style='padding:5px;' class='alertfont'>$lang[feedback_notyet].</div>";?>

<br>

<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">

  <tr>

    <td><table width="100%" border="0" cellpadding="3" cellspacing="1" class="contentfont">

        <tr>

          <td class="c7 smallfont" colspan="5"><b>&nbsp;&nbsp;&raquo;&nbsp;

            <?=$lang[feedback_leave]?>

            </b></td>

        </tr>

        <tr>

          <td class="boldgrey" width="100"><?=$lang[username]?></td>

          <td class="boldgrey" width="70"><?=$lang[itemid];?></td>

          <td class="boldgrey"><?=$lang[itemname];?></td>

          <td class="boldgrey" width="100" align="center"><?=$lang[type]?></td>

          <td width="100" align="center" class="boldgrey"><?=$lang[options]?></td>

        </tr>

        <tr class="c5">

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

          <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>

        </tr>

        <? 

	$limit = 20;



	## modifications for qid 172, #1

	if ($_GET['srctype']=="leavefb") {

		$start = $_GET['start'];

	} else {

		$start = 0;

	}

	$additionalVars = "&page=feedback&srctype=leavefb";

  

  	$leaveFeedbacks=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks 

  	WHERE fromid='".$_SESSION['memberid']."' AND submitted=0 

	ORDER BY id DESC LIMIT $start, $limit");

	$nbLeaveFb = getSqlNumber("SELECT fromid FROM probid_feedbacks 

  	WHERE fromid='".$_SESSION['memberid']."' AND submitted=0");

  

  	while ($leaveFb=mysqli_fetch_array($leaveFeedbacks)) { 

  		$auction = getSqlRow("SELECT id, itemname FROM probid_auctions WHERE id='".$leaveFb['auctionid']."'");

  		$usernm=getSqlField("SELECT username FROM probid_users WHERE id='".$leaveFb['userid']."'","username");?>

        <tr class="<? echo (($count++)%2==0) ? "c2":"c3"; ?> contentfont">

          <td><?=$usernm;?>

          </td>

          <td><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']));?>">

            <?=$auction['id'];?>

            </a></td>

          <td><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']));?>">

            <?=$auction['itemname'];?>

            </a></td>

          <td align="center"><? echo $leaveFb['type'];?></td>

          <td align="center"><a href="leavefeedback.php?from=<?=$_SESSION['memberid'];?>&to=<?=$leaveFb['userid'];?>&nick=<?=$usernm;?>&auctionid=<?=$leaveFb['auctionid'];?>">

            <?=$lang[proceed]?>

            </a></td>

        </tr>

        <? } ?>

        <tr>

          <td colspan="5" align="center" class="contentfont c4"><? paginate($start,$limit,$nbLeaveFb,"membersarea.php",$additionalVars); ?>

          </td>

        </tr>

      </table></td>

  </tr>

</table>

<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?>

