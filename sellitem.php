<?
## v5.25 -> jun. 28, 2006
session_start();

## create a unique id for the images that are to be uploaded.

if (!$_SESSION['images_id']) $_SESSION['images_id'] = md5(uniqid(rand(2, 999999999)));

if ($_SESSION['membersarea']!="Active") {
	if ($_SESSION['accsusp']==2) echo "<script>document.location.href='membersarea.php'</script>";
	else echo "<script>document.location.href='login.php?redirect=sell'</script>";
} else { 

if ($_SESSION['is_seller']!="Y") header ("Location: membersarea.php");

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

## ADD AUTHENTICATION - ALEKSEI

$userDetails = getSqlRow("SELECT `authenticated`, `isikukood`, `realname` FROM probid_users WHERE id='".$_SESSION['memberid']."'");
if ( 'Y' == strtoupper( $userDetails['authenticated'] ) )
{
	// Я авторизованный пользователь

// PRICE LIST



$price["hpfeat"] = 1;
$price["catfeat"] = 0.5;
$price["bolditem"] = 0.15;
$price["hlitem"] = 0.15;

// BALANCE

$val = getSqlRow("SELECT balance FROM probid_users WHERE id='".$_SESSION['memberid']."'");
if ($user['balance']<=0) 
{
	$thevar = $lang[credit];
	$balance = abs($val['balance']);
} else {
	$thevar = $lang[debit];
	$balance = $val['balance'];
}
   


if ($_SESSION['accsusp']==2) {
	header5($lang[susp_title]);
	echo "<p class=contentfont align=center>$lang[susp_text]</p>";
} else {
	$getV = checkSetupVoucher(trim($_REQUEST['voucher_code']));
	
	if ($getV['valid']) {
		echo "<table cellpadding=4 width=100% class=errormessage><tr><td>$lang[voucher_valid]</td></tr></table>";
	} else if (!$getV['valid']&&trim($_REQUEST['voucher_code'])!="") {
		echo "<table cellpadding=4 width=100% class=errormessage><tr><td class=redfont>$lang[voucher_invalid]</td></tr></table>";
	}
	
	if ($_REQUEST['description_main']!="") $_REQUEST['description'] = $_REQUEST['description_main'];

	if (is_numeric($_REQUEST['resprice'])) {
		if ($_REQUEST['resprice'] == 0) $_REQUEST['respr'] = "N";
		else if ($_REQUEST['resprice'] < 0) $_REQUEST['respr'] = "N";
		else if ($_REQUEST['resprice'] > 0) $_REQUEST['respr'] = "Y";
	} else $_REQUEST['respr'] = "N";

	include ("formchecker.php");
	
	## get any prefilled fields
	if (trim($_REQUEST['step'])=="step1") {
		$prefilled = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");
		if (trim($prefilled['default_duration'])!="") $_REQUEST['duration'] = $prefilled['default_duration'];
		if (trim($prefilled['default_private'])!="") $_REQUEST['privateauct'] = $prefilled['default_private'];
		if (trim($prefilled['default_isswap'])!="") $_REQUEST['isswap'] = $prefilled['default_isswap'];
		if (trim($prefilled['default_sc'])!="") $_REQUEST['shipcond'] = $prefilled['default_sc'];
		if (trim($prefilled['default_scint'])!="") $_REQUEST['shipint'] = $prefilled['default_scint'];
		if (trim($prefilled['default_pm'])!="") $_REQUEST['pm'] = $prefilled['default_pm'];
		if ($prefilled['default_acceptdirectpayment']>0) $_REQUEST['acceptdirectpayment'] = $prefilled['default_acceptdirectpayment'];
		##if (trim($prefilled['default_directpaymentemail'])!="") $_REQUEST['directpaymentemail'] = $prefilled['default_directpaymentemail'];
		if ($prefilled['default_postage_costs']>0) $_REQUEST['postage_costs'] = $prefilled['default_postage_costs'];
		if (trim($prefilled['default_insurance'])!="") $_REQUEST['insurance'] = $prefilled['default_insurance'];
		if (trim($prefilled['default_type_service'])!="") $_REQUEST['type_service'] = $prefilled['default_type_service'];
		if (trim($prefilled['default_shipping_details'])!="") $_REQUEST['shipping_details'] = $prefilled['default_shipping_details'];		
		if (trim($prefilled['default_itemname'])!="") $_REQUEST['name'] = $prefilled['default_itemname'];		
		if (trim($prefilled['default_description'])!="") $_REQUEST['description'] = $prefilled['default_description'];		

		## if (trim($prefilled['default_offer_active'])!="") $_REQUEST['offer_active'] = $prefilled['default_offer_active'];		
		## if (trim($prefilled['default_offer_range_min'])!="") $_REQUEST['offer_range_min'] = $prefilled['default_offer_range_min'];		
		## if (trim($prefilled['default_offer_range_max'])!="") $_REQUEST['offer_range_max'] = $prefilled['default_offer_range_max'];		

	}
	
	if ($_GET['option']=="sellsimilar") {
		$_REQUEST['relist_similar']="YES";
		$similar = getSqlRow("SELECT * FROM probid_auctions WHERE 
		id='".$_GET['similarid']."' AND ownerid='".$_SESSION['memberid']."'");
    $_REQUEST['name'] = $similar['itemname'];
    $_REQUEST['category'] = $similar['category'];
    $_REQUEST['addlcategory'] = $similar['addlcategory'];
    $_REQUEST['apply_vat'] = $similar['apply_vat'];
    $_REQUEST['description'] = $similar['description'];
    $_REQUEST['mainpic'] = $similar['picpath']; 
    $_REQUEST['quantity'] = $similar['quantity'];
    $_REQUEST['auctiontype'] = $similar['auctiontype'];
    $_REQUEST['startprice'] = $similar['bidstart'];
    $_REQUEST['respr'] = $similar['rp'];
    $_REQUEST['resprice'] = ($similar['rpvalue']>0)?$similar['rpvalue']:"";
    $_REQUEST['buynow'] = $similar['bn'];
    $_REQUEST['bnprice'] = ($similar['bnvalue']>0)?$similar['bnvalue']:"";
    $_REQUEST['bidinc'] = $similar['bi'];
    $_REQUEST['bidincvalue'] = $similar['bivalue'];
    $_REQUEST['duration'] = $similar['duration'];
    $_REQUEST['country'] = $similar['country'];
    $_REQUEST['zip'] = $similar['zip'];
    $_REQUEST['hpfeat'] = $similar['hpfeat'];
    $_REQUEST['hpfeat_desc'] = $similar['hpfeat_desc'];
    $_REQUEST['catfeat'] = $similar['catfeat'];
    $_REQUEST['currency'] = $similar['currency'];
    $_REQUEST['privateauct'] = $similar['private'];
    $_REQUEST['bolditem'] = $similar['bolditem'];
    $_REQUEST['hlitem'] = $similar['hlitem'];
    $_REQUEST['shipcond'] = $similar['sc'];
    $_REQUEST['shipint'] = $similar['scint'];
    $_REQUEST['pm'] = $similar['pm'];
    $_REQUEST['postage_costs'] = $similar['postage_costs'];
    $_REQUEST['insurance'] = $similar['insurance'];
    $_REQUEST['shipping_details'] = $similar['shipping_details'];
    $_REQUEST['isswap'] = $similar['isswap'];
    $_REQUEST['type_service'] = $similar['type_service'];
    $_REQUEST['acceptdirectpayment'] = $similar['acceptdirectpayment'];
    $_REQUEST['tmp_dirpay'] = $similar['accept_payment_systems'];
    $_REQUEST['listin'] = $similar['listin'];
    $_REQUEST['auto_relist'] = $similar['auto_relist'];
    $_REQUEST['auto_relist_bids'] = $similar['auto_relist_bids'];
    $_REQUEST['auto_relist_nb'] = $similar['auto_relist_nb'];
    $_REQUEST['videofile_path'] = $similar['videofile_path']; 		
    $_REQUEST['listing_type'] = $similar['listing_type']; 		
    $_REQUEST['offer_active'] = $similar['offer_active']; 		
    $_REQUEST['offer_range_min'] = $similar['offer_range_min']; 		
    $_REQUEST['offer_range_max'] = $similar['offer_range_max']; 		
		$_REQUEST['step']="step1";



		### add the custom fields 
	  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT f.boxid, d.boxvalue, f.boxtype FROM probid_fields_data d, probid_fields f WHERE 
		d.auctionid='".$_GET['similarid']."' AND d.ownerid='".$_SESSION['memberid']."' AND d.boxid=f.boxid") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  		$isFields = mysqli_num_rows($getFields);
  		if ($isFields) {
			while ($fields=mysqli_fetch_array($getFields)) {
				if ($fields['boxtype']!="checkbox") $_REQUEST['box'.$fields['boxid']] = $fields['boxvalue'];
				else {
					$flds = explode(";",$fields['boxvalue']);
					for ($i=0; $i<count($flds); $i++) {
						if (trim($flds[$i])!="") $_REQUEST['box'.$fields['boxid']][$i] = $flds[$i];
					}
				}
			}
		}		
		
		$getAdditionalPics = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$_GET['similarid']."'"); 
		$_REQUEST['cnt1']= mysqli_num_rows($getAdditionalPics);
	  	$addPicCnt=0;
		while ($addPic = mysqli_fetch_array($getAdditionalPics)) {
			$_REQUEST['the_pic'][$addPicCnt] = $addPic['name'];
			$addPicCnt++;
		}
	}

	## delete pictures / movie
	if (!empty($_REQUEST['deletepic'])) {
		if (@eregi('mainpic', $_REQUEST['deletepic'])) {
			$isPic = getSqlNumber("SELECT id FROM probid_auctions WHERE picpath='".$_REQUEST['mainpic']."'");
			if (!$isPic) @unlink($_REQUEST['mainpic']);
			$_REQUEST['mainpic'] = "";
		} else if (@eregi('addpic', $_REQUEST['deletepic'])) {
			$picDets = explode('_',$_REQUEST['deletepic']);
			$isPic = getSqlNumber("SELECT id FROM probid_auction_images WHERE name='".$_REQUEST['the_pic'][$picDets[1]]."'");
			if (!$isPic) @unlink($_REQUEST['the_pic'][$picDets[1]]);
			$_REQUEST['the_pic'][$picDets[1]] = "";
		} else if (@eregi('movie', $_REQUEST['deletepic'])) {
			@unlink($_REQUEST['videofile_path']);
			$_REQUEST['videofile_path'] = "";
		}
	}

	$_REQUEST['name'] = remSpecialChars($_REQUEST['name']);
	$_REQUEST['description'] = remSpecialChars($_REQUEST['description']);
	$_REQUEST['hpfeat_desc'] = remSpecialChars($_REQUEST['hpfeat_desc']);

	if ($_REQUEST['step'] == "step2") {
		$_REQUEST['pm']="";
		for ($i=0;$i<count($_REQUEST['pmethod']);$i++) {
			$_REQUEST['pm'].=$_REQUEST['pmethod'][$i]."<br>";
		}

        //-----------alemiks---------------------------
        $_REQUEST['tmp_dirpay'] = '';
        if($_REQUEST['dirpay']){
            foreach($_REQUEST['dirpay'] as $k=>$v){
                $_REQUEST['tmp_dirpay'] .= $v . ',';
            }
        }
        $_REQUEST['tmp_dirpay'] = rtrim($_REQUEST['tmp_dirpay'],',');
        //-----------alemiks---------------------------
	}
	
	if ($_REQUEST['step'] == "step12" || ($_REQUEST['step']=="step11" && isset($_REQUEST['nextstep']))) {
		### this is the saver for the main image
		$tempNumber = $_SESSION['images_id'];
		if (($_FILES['file']['name']!=""||$_POST['mainpicurl'])||($_REQUEST['relist_similar']=="YES"&&$_REQUEST['mainpic']!="")) {
			if ($_POST['mainpicurl']) $_REQUEST['mainpic']="http://".str_replace("http://","",$_POST['mainpicurl']);
			else {
				$sourceFile = ($_REQUEST['relist_similar']=="YES"&&$_FILES['file']['name']=="") ? $_REQUEST['mainpic'] : $_FILES['file']['tmp_name'];
				$sourceFileName = ($_REQUEST['relist_similar']=="YES"&&$_FILES['file']['name']=="") ? $_REQUEST['mainpic'] : $_FILES['file']['name'];
				
				$fileExtension = getFileExtension($sourceFileName);
				$imageName = "mb".$_SESSION['memberid']."_".$tempNumber."_mainpic.".$fileExtension;
				$imgMaxSize = $setts['pic_gal_max_size']*1024;
				if ($_FILES['file']['size']<$imgMaxSize||$_REQUEST['relist_similar']=="YES") {
					$isUpload = uploadFile($sourceFile,$imageName,"uplimg/");
					if ($isUpload) {
						$_REQUEST['mainpic'] = "uplimg/".$imageName;
					}
				} else {
					$picerror.= "<strong>$lang[error_a]</strong> $lang[auc_errpicsize1] $setts[pic_gal_max_size] kb<br>";
				}			
			}
		}

		### this is the uploader for the movie file
		if (($_FILES['videofile']['name']!=""||$_POST['videofileurl'])||($_REQUEST['relist_similar']=="YES"&&$_REQUEST['videofile_path']!="")) {
			if ($_POST['videofileurl']) $_REQUEST['videofile_path']="http://".str_replace("http://","",$_POST['videofileurl']);
			else {
				$sourceFile = ($_REQUEST['relist_similar']=="YES"&&$_FILES['videofile']['name']=="") ? $_REQUEST['videofile_path'] : $_FILES['videofile']['tmp_name'];
				$sourceFileName = ($_REQUEST['relist_similar']=="YES"&&$_FILES['videofile']['name']=="") ? $_REQUEST['videofile_path'] : $_FILES['videofile']['name'];
				
				$fileExtension = getFileExtension($sourceFileName);
				$videoName = "mb".$_SESSION['memberid']."_".$tempNumber."_video.".$fileExtension;
				$videoMaxSize = $setts['video_gal_max_size']*1024;
				if ($_FILES['videofile']['size']<$videoMaxSize||$_REQUEST['relist_similar']=="YES") {
					$isUpload = uploadFile($sourceFile,$videoName,"uplimg/");
					if ($isUpload) {
						$_REQUEST['videofile_path'] = "uplimg/".$videoName;
					} else $picerror .= $lang[movieuplerror]." ".fileUploadError($_FILES['videofile']['error'])."<br>";
				} else {
					$picerror.= "<strong>$lang[error_a]</strong> $lang[auc_errvideosize1] $setts[video_gal_max_size] kb<br>";
				}			
			}
		}

		### this is the saver for the additional images
		for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
			if (($_FILES['addfile']['name'][$i]!=""||$_POST['picurl'][$i])||($_REQUEST['relist_similar']=="YES"&&$_REQUEST['the_pic'][$i]!="")) {
				$_REQUEST['cnt1']=0;
				$cnt1=0;	
			}
		}
		if ($_FILES['file']['name']!="") {
			$_REQUEST['cnt1']=0;
			$cnt1=0;
		}
		for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
			$imgMaxSize = $setts['pic_gal_max_size']*1024;
			if ($_FILES['addfile']['size'][$i]<$imgMaxSize||$_REQUEST['relist_similar']=="YES") {
				if(($_FILES['addfile']['name'][$i]!=""||$_POST['picurl'][$i])||($_REQUEST['relist_similar']=="YES"&&$_REQUEST['the_pic'][$i]!="")) {
					if ($_POST['picurl'][$i]) {
						$_REQUEST['the_pic'][$cnt1]="http://".str_replace("http://","",$_POST['picurl'][$i]);
						$_REQUEST['cnt1']++;
						$cnt1++;
					} else {
						$sourceFile = ($_REQUEST['relist_similar']=="YES"&&$_FILES['addfile']['name'][$i]=="") ? $_REQUEST['the_pic'][$i] : $_FILES['addfile']['tmp_name'][$i];
						$sourceFileName = ($_REQUEST['relist_similar']=="YES"&&$_FILES['addfile']['name'][$i]=="") ? $_REQUEST['the_pic'][$i] : $_FILES['addfile']['name'][$i];
						$fileExtension = getFileExtension($sourceFileName);
						$addImageName = "mb".$_SESSION['memberid']."_".$tempNumber."_addpic".$i.".".$fileExtension;
						$isUpload = uploadFile($sourceFile,$addImageName,"uplimg/");
						if ($isUpload) {
							$_REQUEST['the_pic'][$cnt1]="uplimg/".$addImageName;
							$_REQUEST['cnt1']++;
							$cnt1++;
						}
					}
				} 
			} else {
				$picerror .= "<strong>$lang[error_a]</strong> $lang[auc_errpicsize3] #".($i+1)." $lang[auc_errpicsize4] $setts[pic_gal_max_size] kb<br>";
			}
		}
		
		if (trim($picerror)) { 
			$picerrormsg = "<table width=100% class=border cellspacing=4 cellpadding=4><tr><td class=\"c3 contentfont\">$picerror<br><br>$lang[auc_errpicsize2]</td></tr></table>";
			echo $picerrormsg;
		}
		$cnt_temp = $_REQUEST['cnt1'];
		for ($i=0; $i<$_REQUEST['cnt1']; $i++) {
			if ($_REQUEST['the_pic'][$i] == "") $cnt_temp--;
		}
		$_REQUEST['cnt1'] = $cnt_temp;
		$cnt1 = $cnt_temp;
	}
	
	if ($_REQUEST['date17_year']!="") $_REQUEST['dyear']=$_REQUEST['date17_year'];
	if ($_REQUEST['date17_date']!="") $_REQUEST['ddate']=$_REQUEST['date17_date'];
	if ($_REQUEST['date17_month']!="") $_REQUEST['dmonth']=$_REQUEST['date17_month'];

	if ($_REQUEST['date18_year']!="") $_REQUEST['edyear']=$_REQUEST['date18_year'];
	if ($_REQUEST['date18_date']!="") $_REQUEST['eddate']=$_REQUEST['date18_date'];
	if ($_REQUEST['date18_month']!="") $_REQUEST['edmonth']=$_REQUEST['date18_month'];
	
	
	echo "<form action=\"sellitem.php\" method=\"post\" enctype=\"multipart/form-data\" name=\"sistep1\">\n";
  	echo "	<input type=\"hidden\" name=\"relist_similar\" value=\"".$_REQUEST['relist_similar']."\">\n";
  	echo "	<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST['category']."\">		\n";
	echo " 	<input type=\"hidden\" name=\"name\" value=\"".$_REQUEST['name']."\">				\n";
  	echo "	<input type=\"hidden\" name=\"description\" value=\"".$_REQUEST['description']."\">	\n";
  	echo "  <input type=\"hidden\" name=\"mainpic\" value=\"".$_REQUEST['mainpic']."\">			\n"; 
  	echo "  <input type=\"hidden\" name=\"quantity\" value=\"".$_REQUEST['quantity']."\">		\n"; 
  	echo "  <input type=\"hidden\" name=\"auctiontype\" value=\"".$_REQUEST['auctiontype']."\">	\n"; 
  	echo "  <input type=\"hidden\" name=\"startprice\" value=\"".$_REQUEST['startprice']."\">	\n";
  	echo "  <input type=\"hidden\" name=\"respr\" value=\"".$_REQUEST['respr']."\">				\n";				
  	echo "  <input type=\"hidden\" name=\"resprice\" value=\"".$_REQUEST['resprice']."\">		\n"; 
  	echo "  <input type=\"hidden\" name=\"buynow\" value=\"".$_REQUEST['buynow']."\">			\n"; 
  	echo "  <input type=\"hidden\" name=\"bnprice\" value=\"".$_REQUEST['bnprice']."\">			\n"; 
  	echo "  <input type=\"hidden\" name=\"bidinc\" value=\"".$_REQUEST['bidinc']."\"> 			\n";
  	echo "  <input type=\"hidden\" name=\"bidincvalue\" value=\"".$_REQUEST['bidincvalue']."\">	\n"; 
  	echo "  <input type=\"hidden\" name=\"duration\" value=\"".$_REQUEST['duration']."\">		\n"; 
  	echo "  <input type=\"hidden\" name=\"country\" value=\"".$_REQUEST['country']."\"> 		\n";
  	echo "	<input type=\"hidden\" name=\"zip\" value=\"".$_REQUEST['zip']."\"> 				\n";
  	echo "  <input type=\"hidden\" name=\"currency\" value=\"".$_REQUEST['currency']."\">		\n"; 
  	echo "  <input type=\"hidden\" name=\"privateauct\" value=\"".$_REQUEST['privateauct']."\">	\n"; 
  	echo "  <input type=\"hidden\" name=\"shipcond\" value=\"".$_REQUEST['shipcond']."\">		\n";
  	echo "  <input type=\"hidden\" name=\"shipint\" value=\"".$_REQUEST['shipint']."\">			\n";
  	echo "  <input type=\"hidden\" name=\"pm\" value=\"".$_REQUEST['pm']."\">					\n";
  	echo "  <input type=\"hidden\" name=\"hpfeat_desc\" value=\"".$_REQUEST['hpfeat_desc']."\">	\n";
  	echo "  <input type=\"hidden\" name=\"postage_costs\" value=\"".$_REQUEST['postage_costs']."\">\n";
  	echo "  <input type=\"hidden\" name=\"insurance\" value=\"".$_REQUEST['insurance']."\">		\n";
  	echo "  <input type=\"hidden\" name=\"shipping_details\" value=\"".remSpecialChars($_REQUEST['shipping_details'])."\">\n";
  	echo "  <input type=\"hidden\" name=\"type_service\" value=\"".$_REQUEST['type_service']."\">\n";
  	echo "  <input type=\"hidden\" name=\"isswap\" value=\"".$_REQUEST['isswap']."\">			\n";
  	echo "  <input type=\"hidden\" name=\"addlcategory\" value=\"".$_REQUEST['addlcategory']."\">\n"; 
  	echo "	<input type=\"hidden\" name=\"starttime\" value=\"".$_REQUEST['starttime']."\">		\n";
  	echo "	<input type=\"hidden\" name=\"dmonth\" value=\"".$_REQUEST['dmonth']."\">\n";
  	echo "	<input type=\"hidden\" name=\"ddate\" value=\"".$_REQUEST['ddate']."\">	\n";
  	echo "	<input type=\"hidden\" name=\"dyear\" value=\"".$_REQUEST['dyear']."\">	\n";
  	echo "	<input type=\"hidden\" name=\"shour\" value=\"".$_REQUEST['shour']."\">				\n";
  	echo "	<input type=\"hidden\" name=\"sminute\" value=\"".$_REQUEST['sminute']."\">			\n";

  	echo "	<input type=\"hidden\" name=\"endtime\" value=\"".$_REQUEST['endtime']."\">		\n";
  	echo "	<input type=\"hidden\" name=\"edmonth\" value=\"".$_REQUEST['edmonth']."\">\n";
  	echo "	<input type=\"hidden\" name=\"eddate\" value=\"".$_REQUEST['eddate']."\">	\n";
  	echo "	<input type=\"hidden\" name=\"edyear\" value=\"".$_REQUEST['edyear']."\">	\n";
  	echo "	<input type=\"hidden\" name=\"eshour\" value=\"".$_REQUEST['eshour']."\">				\n";
  	echo "	<input type=\"hidden\" name=\"esminute\" value=\"".$_REQUEST['esminute']."\">			\n";

  	echo "	<input type=\"hidden\" name=\"auto_relist\" value=\"".$_REQUEST['auto_relist']."\">	\n";
  	echo "	<input type=\"hidden\" name=\"auto_relist_bids\" value=\"".$_REQUEST['auto_relist_bids']."\">\n";

  	echo "	<input type=\"hidden\" name=\"auto_relist_nb\" value=\"".(($_REQUEST['auto_relist_nb_tmp']>0) ? $_REQUEST['auto_relist_nb_tmp'] : $_REQUEST['auto_relist_nb'])."\">\n";

  	echo "	<input type=\"hidden\" name=\"listin\" value=\"".$_REQUEST['listin']."\">			\n";
  	echo "	<input type=\"hidden\" name=\"tmp_dirpay\" value=\"".$_REQUEST['tmp_dirpay']."\">			\n";
  	echo "	<input type=\"hidden\" name=\"apply_vat\" value=\"".$_REQUEST['apply_vat']."\">			\n";
  	echo "	<input type=\"hidden\" name=\"voucher_code\" value=\"".$_REQUEST['voucher_code']."\">\n";

  	echo "	<input type=\"hidden\" name=\"videofile_path\" value=\"".$_REQUEST['videofile_path']."\">\n";

  	echo "	<input type=\"hidden\" name=\"listing_type\" value=\"".$_REQUEST['listing_type']."\">\n";

  	echo "	<input type=\"hidden\" name=\"offer_active\" value=\"".$_REQUEST['offer_active']."\">\n";
  	echo "	<input type=\"hidden\" name=\"offer_range_min\" value=\"".$_REQUEST['offer_range_min']."\">\n";
  	echo "	<input type=\"hidden\" name=\"offer_range_max\" value=\"".$_REQUEST['offer_range_max']."\">\n";
	
	if (isset($_REQUEST['nextstep'])) $next_step = 1;
	else if ($_REQUEST['relist_similar']=="YES") $next_step = 1;
	else $next_step=0;

	
	if (($_REQUEST['step'] != "step11"&&$next_step)||($_REQUEST['step'] != "step2"&&isset($_REQUEST['prevstep']))) {
	
	
  		echo "<input type=\"hidden\" name=\"hpfeat\" value=\"".$_REQUEST['hpfeat']."\"> 		\n";
  		echo "<input type=\"hidden\" name=\"catfeat\" value=\"".$_REQUEST['catfeat']."\">		\n"; 
  		echo "<input type=\"hidden\" name=\"bolditem\" value=\"".$_REQUEST['bolditem']."\"> 	\n";
  		echo "<input type=\"hidden\" name=\"hlitem\" value=\"".$_REQUEST['hlitem']."\">			\n";
  		
  		
		
		$mainCat_primary = getMainCat($_REQUEST['category']);
		$mainCat_secondary = getMainCat($_REQUEST['addlcategory']);
		### add the custom fields 
	  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_fields WHERE 
		(categoryid='".$mainCat_primary."' OR categoryid='".$mainCat_secondary."' OR categoryid='0')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  		$isFields = mysqli_num_rows($getFields);
  		if ($isFields) {
			while ($fields=mysqli_fetch_array($getFields)) {
				if ($fields['boxtype']=="checkbox") {
					for ($i=0; $i<count($_REQUEST['box'.$fields['boxid']]); $i++) 
						echo "<input type=\"hidden\" name=\"box".$fields['boxid']."[]\" value=\"".remSpecialChars($_REQUEST['box'.$fields['boxid']][$i])."\">\n";	
				} else { 
					echo "<input type=\"hidden\" name=\"box".$fields['boxid']."\" value=\"".remSpecialChars($_REQUEST['box'.$fields['boxid']])."\">\n";
				}
			}
		}		
    }
	if (($_REQUEST['step'] != "step12"&&isset($_REQUEST['nextstep']))||($_REQUEST['step'] != "step3"&&isset($_REQUEST['prevstep']))) {
  		echo "<input type=\"hidden\" name=\"acceptdirectpayment\" value=\"".$_REQUEST['acceptdirectpayment']."\">\n";
    }
  	for ($i=0;$i<$_REQUEST['cnt1'];$i++) { 
  		echo "<input type=\"hidden\" name=\"the_pic[]\" value=\"".$_REQUEST['the_pic'][$i]."\">	\n";
  	} 
  	echo "<input type=\"hidden\" name=\"cnt1\" value=\"".$_REQUEST['cnt1']."\"> 				\n";
	
		#### if store is enabled for the user first step is step 0
		$userStore = getSqlRow("SELECT aboutpage_type, store_active, store_account_type FROM probid_users WHERE id='".$_SESSION['memberid']."'");
		$nbStoreItems = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."' AND 
		active='1' AND closed='0' AND deleted!='1' AND listin!='auction'");
		$storeActive = FALSE;
		if ($userStore['aboutpage_type']==2&&$userStore['store_active']==1) {
			if ($userStore['store_account_type']==0) $storeActive = TRUE;
			else {
				$maxStoreItems = getSqlField("SELECT store_nb_items FROM probid_fees_tiers WHERE id='".$userStore['store_account_type']."'","store_nb_items");
				if ($maxStoreItems!="n/a"&&$maxStoreItems!=""&&$maxStoreItems>$nbStoreItems) $storeActive=TRUE;
			}
		}
		
		$category_id = getMainCat($_REQUEST['category']);
		
		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
		
		if (!$storeActive&&trim($_REQUEST['step'])=="") { 
			$_REQUEST['step']="step0";
			$_REQUEST['listin']="auction";
		}
			
		switch ($_REQUEST['step']) {
			case "step3": 
				if (isset($_REQUEST['nextstep'])) {
					echo "</form>";
					include ("sellitem-final.php");
					echo "<form>";
				}
				else if (isset($_REQUEST['prevstep'])) include ("sellitem-step12.php");
				break;
			case "step2": 
				if (isset($_REQUEST['nextstep'])) include ("sellitem-step2.php");
				else if (isset($_REQUEST['prevstep'])) include ("sellitem-step11.php");
				break;
			case "step12":
				if (isset($_REQUEST['nextstep'])) include ("sellitem-step12.php");
				else if (isset($_REQUEST['prevstep'])) include ("sellitem-step1.php");
				break;
			case "step11":
				if (isset($_REQUEST['nextstep'])||$_REQUEST['curchange']=="yes") include ("sellitem-step11.php");
				else if (isset($_REQUEST['prevstep'])) include ("sellitem-step0.php");
				break;
			case "step1":
				include ("sellitem-step1.php");
				break;
			case "step01":
				include ("sellitem-step01.php");
				break;
			case "step0":
				include ("sellitem-step0.php");
				break;
			default:
				include ("sellitem-step00.php");
		}  
	echo "</form>";
}	

}

## AUTHENTICATION CONTINUE - ALEKSEI
else
{
	// Я неавторизованный пользователь
	include 'pangalink/authenticate.php';
}
	
include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
