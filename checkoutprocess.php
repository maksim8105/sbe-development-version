<?php
## v5.24 -> apr. 06, 2006
//session_start();
include_once ("config/config.php");

## overwrite $setts['payment gateway']
$setts['payment_gateway']="2checkout";

if ($_POST['order_number']==1) $table_to_modify='probid_users';
if ($_POST['order_number']==2) $table_to_modify='probid_auctions';
if ($_POST['order_number']==3) $table_to_modify='probid_winners';
if ($_POST['order_number']==4) $accountMode=2;

if ($_POST['order_number']==7) $table_to_modify='probid_winners';

$payment_status = "Completed";
$payment_gross = $_POST['total'];
$txn_id = "CheckoutTXN";
$custom = $_POST['cart_order_id'];
$currentTime=time();

if ($_POST['cart_order_id']!=""&&$_POST['credit_card_processed']=="Y"){
	#-------------------------------------------
	if ($accountMode==2) {
		$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$custom."'","balance");
		$updatedBalance = $currentBalance - $payment_gross;
		if ($updatedBalance<=0) {
			$_SESSION['accsusp']=0;
		}
		## if user is suspended, activate the counters
		$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
		if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);

		$currentTime = time();
		$updateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		active='1', payment_status='confirmed', balance='".$updatedBalance."' WHERE id='".$custom."'");
		$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
		active='1' WHERE ownerid='".$custom."'");
		$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
		active='1' WHERE ownerid='".$custom."'");
		$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
		(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
		('".$custom."','".$lang[payment_fee]."','".$payment_gross."','".$currentTime."','".$updatedBalance."','payment','".$setts['payment_gateway']."')");
	} else {
		if ($_POST['order_number'] == 100) {
			//$pDetails = explode("_",$custom);
			//mysql_query("UPDATE probid_auctions SET	paidwithdirectpayment='1' WHERE id='".$pDetails[0]."'");
			/*
			mysql_query("UPDATE probid_winners SET flag_paid=1, directpayment_paid=1
			WHERE  buyerid='".$pDetails[1]."' AND auctionid='".$pDetails[0]."'");
			*/
			mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid=1, directpayment_paid=1 WHERE id='".$custom."'");
   	} else if ($_POST['order_number']==3) {
			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$table_to_modify." SET 
			active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',
			txnid='".$txn_id."',
			paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' WHERE auctionid='".$_POST['cart_order_id']."'");
		} else if ($_POST['order_number']==8) {
			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
			store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			## now we submit the accounting details
			$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
			(userid, amountpaid, paymentdate, processor) VALUES
			('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			## if we activate the users table, count all auctions
			if ($_POST['order_number']==1) counterAddUser($custom);

			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$table_to_modify." SET 
			active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',
			paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' WHERE id='".$_POST['cart_order_id']."'");
			## if we activate the auctions table, count the auction
			if ($_POST['order_number']==2) {
				$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
				if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
					addcatcount ($auctCat['category']);
					addcatcount ($auctCat['addlcategory']);
				}
			}
			if ($_POST['order_number']==7) {
				$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
				if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
					addwantedcount ($wantedCat['category']);
					addwantedcount ($wantedCat['addlcategory']);
				}
			}
		}
	}
	countCategories();
}
header ("Location: ".$path."paymentdone.php");
?>