<?
## v5.24 -> may. 10, 2006
session_start();
include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");


if ($_GET['start'] == "") $start = 0;
else $start = $_GET['start'];
$limit = 20;
if ($_GET['view'] == "") $view = "all";
else $view = $_GET['view'];

$additionalVars = "&owner=".$_GET['owner']."&auction=".$_GET['auction'];

$a_month = 60*60*24*30;
$time_month = time() - 60*60*24*30;
$date_month = date("Y-m-d H:i:s",$time_month);

/*header5("$lang[viewfeedback]")*/;
?>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
   <tr>
      <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="1">
            <tr height="30">
               <td><b>
                  <?
						echo $lang[feedbackfor]." ";
						echo getSqlField("SELECT username FROM probid_users WHERE id='".$_GET['owner']."'","username")." ";
						echo getFeedback($_GET['owner']); 
						$sellerDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$_GET['owner']."'");
						
						?>
                  </b></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <tr class="c2">
               <td><? if ($sellerDetails['regdate']!=0) echo $lang[regsince]." ".date("d.m.Y",$sellerDetails['regdate'])." ".$lang[inthe]." ".$sellerDetails['country']; ?></td>
            </tr>
         </table>
         <table width="100%" border="0" cellspacing="2" cellpadding="2">
            <tr class="c2">
               <td><strong>
                  <?=$lang[total_feedback];?>
                  </strong></td>
               <td nowrap><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND submitted=1"); ?></td>
            </tr>
            <tr class="c3">
               <td width="100%"><strong>
                  <?=$lang[positive_feedback];?>
                  </strong></td>
               <td nowrap><? echo calcFeedback($_GET['owner']);?></td>
            </tr>
         </table>
         <table width="100%" border="0" cellspacing="1" cellpadding="2" class="contentfont border">
            <tr class="c2">
               <td><a href="otheritems.php?owner=<?=$_GET['owner'];?>"><strong>
                  <?=$lang[view_my_auction];?>
                  </strong></a></td>
               <td align="right"><?
						$shopDets = getSqlRow("SELECT aboutpage_type, store_active, store_name FROM probid_users WHERE id='".$_GET['owner']."'");
						if ($shopDets['aboutpage_type']==2&&$shopDets['store_active']==1) { ?>
                  <a href="<?=processLink('shop', array('store' => $shopDets['store_name'], 'userid' => $_GET['owner'])); ?>"><strong>
                  <?=$lang[view_my_store]?>
                  </strong></a>
                  <? } ?></td>
            </tr>
         </table></td>
      <td width="55%" valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="2" class="border">
            <tr class="c1">
               <td colspan="6" align="center"><?=$lang[recent_ratings];?></td>
            </tr>
            <tr class="c4">
               <td align="center">&nbsp;</td>
               <td align="center" class="positive"><img src="images/5stars.gif" hspace="3"></td>
               <td align="center" class="positive"><img src="images/4stars.gif" hspace="3"></td>
               <td align="center" class="neutral"><img src="images/3stars.gif" hspace="3"></td>
               <td align="center" class="negative"><img src="images/2stars.gif" hspace="3"></td>
               <td align="center" class="negative"><img src="images/1stars.gif" hspace="3"></td>
            </tr>
            <?
				$one_month = date("Y-m-d H:i:s",time()-($a_month*1));
				$six_months = date("Y-m-d H:i:s",time()-($a_month*6));
				$twelwe_months = date("Y-m-d H:i:s",time()-($a_month*12));
				?>
            <tr class="c2">
               <td align="center" width="25%"><?=$lang[rate_1_month];?></td>
               <td align="center" width="15%" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$one_month' AND submitted=1 AND rate=5"); ?></td>
               <td align="center" width="15%" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$one_month' AND submitted=1 AND rate=4"); ?></td>
               <td align="center" width="15%" class="neutral"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$one_month' AND submitted=1 AND rate=3"); ?></td>
               <td align="center" width="15%" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$one_month' AND submitted=1 AND rate=2"); ?></td>
               <td align="center" width="15%" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$one_month' AND submitted=1 AND rate=1"); ?></td>
            </tr>
            <tr class="c3">
               <td align="center"><?=$lang[rate_6_month];?></td>
               <td align="center" width="15%" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$six_months' AND submitted=1 AND rate=5"); ?></td>
               <td align="center" width="15%" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$six_months' AND submitted=1 AND rate=4"); ?></td>
               <td align="center" width="15%" class="neutral"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$six_months' AND submitted=1 AND rate=3"); ?></td>
               <td align="center" width="15%" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$six_months' AND submitted=1 AND rate=2"); ?></td>
               <td align="center" width="15%" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$six_months' AND submitted=1 AND rate=1"); ?></td>
            </tr>
            <tr class="c2">
               <td align="center"><?=$lang[rate_12_month];?></td>
               <td align="center" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$twelwe_months' AND submitted=1 AND rate=5"); ?></td>
               <td align="center" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$twelwe_months' AND submitted=1 AND rate=4"); ?></td>
               <td align="center" class="neutral"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$twelwe_months' AND submitted=1 AND rate=3"); ?></td>
               <td align="center" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$twelwe_months' AND submitted=1 AND rate=2"); ?></td>
               <td align="center" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND date>='$twelwe_months' AND submitted=1 AND rate=1"); ?></td>
            </tr>
            <tr class="c5">
               <td colspan="6" align="center"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <tr class="c3">
               <td align="center"><?=$lang[rating_as_seller];?></td>
               <td align="center" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='sale' AND submitted=1 AND rate=5"); ?></td>
               <td align="center" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='sale' AND submitted=1 AND rate=4"); ?></td>
               <td align="center" class="neutral"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='sale' AND submitted=1 AND rate=3"); ?></td>
               <td align="center" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='sale' AND submitted=1 AND rate=2"); ?></td>
               <td align="center" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='sale' AND submitted=1 AND rate=1"); ?></td>
            </tr>
            <tr class="c3">
               <td align="center"><?=$lang[rating_as_buyer];?></td>
               <td align="center" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='purchase' AND submitted=1 AND rate=5"); ?></td>
               <td align="center" class="positive"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='purchase' AND submitted=1 AND rate=4"); ?></td>
               <td align="center" class="neutral"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='purchase' AND submitted=1 AND rate=3"); ?></td>
               <td align="center" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='purchase' AND submitted=1 AND rate=2"); ?></td>
               <td align="center" class="negative"><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$_GET['owner']."' AND type='purchase' AND submitted=1 AND rate=1"); ?></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
<?
if ($fb_type=="sale") echo $lang[sale];
else echo $lang[purchase];

if ($view=="all") $searchPattern = " userid='".$_GET['owner']."' ";
else if ($view=="positive") $searchPattern = " userid='".$_GET['owner']."' AND rate>3 ";
else if ($view=="neutral") $searchPattern = " userid='".$_GET['owner']."' AND rate=3 ";
else if ($view=="negative") $searchPattern = " userid='".$_GET['owner']."' AND rate<3 ";
else if ($view=="frombuyers") $searchPattern = " userid='".$_GET['owner']."' AND type='sale' ";
else if ($view=="fromsellers") $searchPattern = " userid='".$_GET['owner']."' AND type='purchase' ";
else if ($view=="left") $searchPattern = " fromid='".$_GET['owner']."' ";

$getFeedbacks = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks 
WHERE ".$searchPattern." AND submitted=1 ORDER BY date DESC LIMIT ".$start.",".$limit) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
$totFbs = getSqlNumber("SELECT * FROM probid_feedbacks 
WHERE ".$searchPattern." AND submitted=1");
?>
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="contentfont">
   <tr align="center" height="21">
      <td class="<? if ($view=="all") { echo "c1"; } else { echo "c4";} ?>"><? echo "<a href=\"viewfeedback.php?view=all&start=".$start."$additionalVars\">".$lang[all_ratings]."</a>"; ?></td>
      <td class="<? if ($view=="frombuyers") { echo "c1"; } else { echo "c4";} ?>"><? echo "<a href=\"viewfeedback.php?view=frombuyers&start=".$start."$additionalVars\">".$lang[from_buyers]."</a>";?></td>
      <td class="<? if ($view=="fromsellers") { echo "c1"; } else { echo "c4";} ?>"><? echo "<a href=\"viewfeedback.php?view=fromsellers&start=".$start."$additionalVars\">".$lang[from_sellers]."</a>";?></td>
      <td class="<? if ($view=="left") { echo "c1"; } else { echo "c4";} ?>"><? echo "<a href=\"viewfeedback.php?view=left&start=".$start."$additionalVars\">".$lang[left_for_others]."</a>";?></td>
   </tr>
   <tr>
      <td colspan="7" class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="4" class="contentfont">
   <? while ($fb = mysqli_fetch_array($getFeedbacks)) { 
  	$fromId = ($view=="left") ? $fb['userid'] : $fb['fromid'];
  	?>
   <tr class="<? echo (($count++)%2==0)?"c2":"c3";?>">
      <td><?	
	$isCRF = getSqlNumber("SELECT id FROM probid_custom_rep WHERE active=1");
	echo showFeedback($fb['rate'])."
	| <strong>$lang[date]</strong>: ".format_date($fb['date'],1)."
	| <strong>$lang[type]</strong>: ".$fb['type']."
	| <strong>".(($view=="left") ? $lang[to] : $lang[from])."</strong>: <a href=\"viewfeedback.php?owner=".$fromId."&auction=".$_REQUEST['auction']."\">".getSqlField("SELECT username FROM probid_users WHERE id='".$fromId."'","username")." ".getFeedback($fromId)."</a>
	| <strong>$lang[item]</strong>: <a href=\"".processLink('auctiondetails', array('id' => $fb['auctionid']))."\">".$fb['auctionid']."</a> 
	".(($isCRF) ? " | [ <strong><a href=\"javascript://\" onclick=\"popUp('repdetails.php?fbid=".$fb['id']."');\">$lang[details]</a></strong> ]":"")."
	<br>".$fb['feedback'];
	?></td>
   </tr>
   <? } ?>
   <tr>
      <td colspan="7" class="contentfont c4" align="center"><? paginate($start,$limit,$totFbs,"viewfeedback.php","&view=$view".$additionalVars);?></td>
   </tr>
</table>
<? 
if ($_GET['auction']!=0) { 
	echo "<br><div align=\"center\" class=\"contentfont\">\n";
	echo "<a href=\"".processLink('auctiondetails', array('id' => $_GET['auction']))."\">$lang[retdetailspage]</a></div>";
}
include ("themes/".$setts['default_theme']."/footer.php"); ?>
