<?php
## v5.24 -> may. 08, 2006
session_start();
include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php");


if ($setts['h_counter']==1) {
   	 	$nbShops=getSqlNumber("SELECT id FROM probid_users WHERE store_active=1");
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		$nbFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1","Total");
		$nbRegisteredUsers24h=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-86400));
		$nbRegisteredUsers7d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-604800));
		$nbRegisteredUsers30d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-2592000));
		$nbRegisteredUsers1y=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-31536000));
		$nbopenauctions24h=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbopenauctions7d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbopenauctions30d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbopenauctions1y=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbtotalauctions=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_auctions","Total1");
		$nbauctionsAmount24h=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'","Total1");
		$nbauctionsAmount7d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'","Total1");
		$nbauctionsAmount30d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'","Total1");
		$nbauctionsAmount1y=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'","Total1");
		$nbauctionsFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions","Total1");
		$nbsolds24h=getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3");
		$nbsolds7d=getSqlField("SELECT SUM(amount) AS 'Total4' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-604800),"Total4");
		$nbsolds30d=getSqlField("SELECT SUM(amount) AS 'Total5' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total5");
		$nbsolds1y=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total6");
		$nbsoldsTotal=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners","Total6");
		$nbWantAds24h=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbWantAds7d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbWantAds30d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbWantAds1y=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbWantAdsTotal=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_wanted_ads","Total1");
		if ($nbsolds24h>0) $effect24h=($nbsolds24h/$nbauctionsAmount24h)*100; else $effect24h=0;
		if ($nbsolds7d>0) $effect7d=($nbsolds7d/$nbauctionsAmount7d)*100; else $effect7d=0;
		if ($nbsolds30d>0) $effect30d=($nbsolds30d/$nbauctionsAmount30d)*100; else $effect30d=0;
		if ($nbsolds1y>0) $effect1y=($nbsolds1y/$nbauctionsAmount1y)*100; else $effect1y=0;
		if ($nbsoldsTotal>0) $effecttotal=($nbsoldsTotal/$nbauctionsFullAmount)*100; else $effecttotal=0;
		
		/*header5("$lang[Site_status]");*/
	echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang[Site_status]."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";
   include "stat_menu.php";	
	if (($nbRegisteredUsers24h/$nbRegisteredUsers7d)<0.1428) $regtr24h="red"; else $regtr24h="green";
	if (($nbRegisteredUsers7d/$nbRegisteredUsers30d)<0.2333) $regtr7d="red"; else $regtr7d="green";	
	if (($nbRegisteredUsers30d/$nbRegisteredUsers1y)<0.0821) $regtr30d="red"; else $regtr30d="green";
	if (($nbRegisteredUsers1y/$nbUsers)<(365/((time()-1173465021)/86400))) $regtr1y="'red'"; else $regtr1y="green";
	if (($nbopenauctions24h/$nbopenauctions7d)<0.1428) $opauc24h="red"; else $opauc24h="green";
	if (($nbopenauctions7d/$nbopenauctions30d)<0.2333) $opauc7d="red"; else $opauc7d="green";	
	if (($nbopenauctions30d/$nbopenauctions1y)<0.0821) $opauc30d="red"; else $opauc30d="green";
	if (($nbopenauctions1y/$nbtotalauctions)<(365/((time()-1173465021)/86400))) $opauc1y="'red'"; else $opauc1y="green";
	if (($nbauctionsAmount24h/$nbauctionsAmount7d)<0.1428) $aucam24h="red"; else $aucam24h="green";
	if (($nbauctionsAmount7d/$nbauctionsAmount30d)<0.2333) $aucam7d="red"; else $aucam7d="green";	
	if (($nbauctionsAmount30d/$nbauctionsAmount1y)<0.0821) $aucam30d="red"; else $aucam30d="green";
	if (($nbauctionsAmount1y/$nbauctionsFullAmount)<(365/((time()-1173465021)/86400))) $aucam1y="'red'"; else $aucam1y="green";			
	if ($nbauctionsAmount24h>0) 
	$nbauctionsAmount24h=$nbauctionsAmount24h;
	else $nbauctionsAmount24h=0;
		echo "<div align='justify'>Анализ трендов позволяет в реальном времени отслеживать эффективность продаж и динамику развития SBE.EE.</div><BR>
		<table align='center' class='c2'><tr class='c1'> 
          		<td align='center'>&nbsp;<font style='font-size: 10px;'></font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>24 tundi</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>7 päeva</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>30 päeva</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>1 aasta</font></td>
          		<td align='center'>&nbsp;<font style='font-size: 10px;'>Kokku</font></td>
        	</tr>
        	<tr> 
          		<td align='right'><b></b>Зарегистрировалось пользователей</td>
          		<td align='center'><font color=".$regtr24h."><b>".$nbRegisteredUsers24h."</b></font></td>
          		<td align='center'><font color=".$regtr7d."><b>".$nbRegisteredUsers7d."</b></font></td>
          		<td align='center'><font color=".$regtr30d."><b>".$nbRegisteredUsers30d."</b></font></td>
          		<td align='center'><font color=".$regtr1y."><b>".$nbRegisteredUsers1y."</b></font></td>
          		<td align='center'><b>".$nbUsers."</b></td>
          		
        	</tr>
        	<tr class='c2'> 
          		<td align='right'><b></b>Открыто аукионов</td>
          		<td align='center'><font color=".$opauc24h."><b>".$nbopenauctions24h."</b></td>
          		<td align='center'><font color=".$opauc7d."><b>".$nbopenauctions7d."</b></td>
          		<td align='center'><font color=".$opauc30d."><b>".$nbopenauctions30d."</b></td>
          		<td align='center'><font color=".$opauc1y."><b>".$nbopenauctions1y."</b></td>
          		<td align='center'><b>".$nbtotalauctions."</b></td>
        	</tr>
        	<tr class='c1'><td colspan='6'></td> </tr>
        	<tr> 
          		<td align='right'><b></b>На сумму</td>
          		<td align='right'><font color=".$aucam24h."><b>".number_format($nbauctionsAmount24h,2,',',' ')."</b></td>
          		<td align='right'><font color=".$aucam7d."><b>".number_format($nbauctionsAmount7d,2,',',' ')."</b></td>
          		<td align='right'><font color=".$aucam30d."><b>".number_format($nbauctionsAmount30d,2,',',' ')."</b></td>
          		<td align='right'><font color=".$aucam1y."><b>".number_format($nbauctionsAmount1y,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbauctionsFullAmount,2,',',' ')."</b></td>
        	</tr>
        		<tr class='c1'><td colspan='6'></td> </tr>
        		<tr> 
          		<td align='right'><b></b>Продано на сумму</td>
          		<td align='right'><b>".number_format($nbsolds24h,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsolds7d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsolds30d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsolds1y,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($nbsoldsTotal,2,',',' ')."</b></td>
        	</tr>
        	<tr class='c3'> 
          		<td align='right'><b></b>% успешности продаж</td>
          		<td align='right'><b>".number_format($effect24h,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effect7d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effect30d,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effect1y,2,',',' ')."</b></td>
          		<td align='right'><b>".number_format($effecttotal,2,',',' ')."</b></td>
        	</tr>
        	<tr class='c1'><td colspan='6'></td> </tr>
        	<tr class='c2'> 
          		<td align='right'>Объявлено заявок на покупку</td>
          		<td align='center'><b>".$nbWantAds24h."</b></td>
          		<td align='center'><b>".$nbWantAds7d."</b></td>
          		<td align='center'><b>".$nbWantAds30d."</b></td>
          		<td align='center'><b>".$nbWantAds1y."</b></td>
          		<td align='center'><b>".$nbWantAdsTotal."</b></td>
        	</tr>
        	<tr class='c1'><td colspan='6'></td> </tr>
        	</table>";
echo "<div align='justify'><br>Показатели за конкретный период считаются растущими (выделено зелёным), если их среднее значение в единицу времени больше, чем среднее значение для следующего более длинного периода.</div>";
}

include ("themes/".$setts['default_theme']."/footer.php"); ?>