<?
## v5.24 -> jun. 02, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php?redirect=swapitem&auctionid=".$_REQUEST['id']."'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

header5(strtoupper($lang[offerswap])); 

if (isset($_POST['swapitemok'])) {
	if ($_POST['quantity']>$_POST['quantavail']) {
		echo "$lang[swapmoreavail]";
	} else if ($_POST['quantity']<=$_POST['quantavail']) {
		#### send mail to seller
		$insertSwapRequest = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_swaps
		(auctionid, sellerid, buyerid, quantity, description) VALUES 
		('".$_POST['auctionid']."', '".$_POST['sellerid']."', '".$_SESSION['memberid']."', 
		'".$_POST['quantity']."', '".remSpecialChars($_POST['description'])."')");
			
		$subject="$lang[swapofferfrom] $buyername.";
		echo "<div align=\"center\" class=\"contentfont\">$lang[swapsubmitsuccess]<br><br>								\n";
		echo "<a href=\"".processLink('auctiondetails', array('id' => $_POST['auctionid']))."\">$lang[retdetailspage]</a></div><br><br>			\n";

		$sellerId = $_POST['sellerid'];
		$buyerId = $_POST['buyerid'];
		$auctionId = $_POST['auctionid'];
		$description = $_POST['description'];
					
		include ("mails/notifysellerswap.php");
	}
} else { 
 	$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."' AND deleted!=1");
	$isAuction = getSqlNumber("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."' AND deleted!=1");
	//&&$fee['is_swap_fee']=="Y"&&$fee['val_swap_fee']>0
	if ($isAuction>0&&$setts['swap_items']==1) {
		echo "<br>";
		header5($auction['itemname']." - ".strtoupper($lang[offerswap])); ?> 
<form action="swapitems.php" method="post"> 
  <input type="hidden" name="itemname" value="<?=$auction['itemname'];?>"> 
  <input type="hidden" name="sellerid" value="<?=$auction['ownerid'];?>"> 
  <input type="hidden" name="auctionid" value="<?=$auction['id'];?>"> 
  <input type="hidden" name="itemname" value="<?=$auction['itemname'];?>"> 
  <input type="hidden" name="quantavail" value="<?=$auction['quantity'];?>"> 
  <input type="hidden" name="auctiontype" value="<?=$auction['auctiontype'];?>"> 
  <table width="400" border="0" cellpadding="4" cellspacing="4" align="center" class="border"> 
    <tr class="c3"> 
      <td width="50%" align="right"><?=$lang[currbid]?> 
        :</td> 
      <td><? echo displayAmount($auction['maxbid'],$auction['currency']);?></td> 
    </tr> 
    <tr class="c2"> 
      <td align="right"><?=$lang[num_bids]?> 
        :</td> 
      <td> <?=$auction['nrbids'];?> </td> 
    </tr> 
    <tr class="c3"> 
      <td align="right"><?=$lang[quant]?> 
        :</td> 
      <td><input name="quantity" type="text" class="contentfont" id="quantity" value="1" size="10" <? echo ($auction['auctiontype']=="standard")?"readonly":""; ?>></td> 
    </tr> 
    <tr align="center" class="c2"> 
      <td colspan="2"><?=$lang[itemdescr]?> 
        : </td> 
    </tr> 
    <tr align="center" class="c3"> 
      <td colspan="2"><textarea name="description" cols="50" rows="8" id="description"></textarea></td> 
    </tr> 
    <tr align="center" class="c2"> 
      <td colspan="2"><input name="swapitemok" type="submit" id="swapitemok" value="<?=$lang[proceed]?>"></td> 
    </tr> 
  </table> 
</form> 
<? 		} 
		else echo "<p align=center><strong>$lang[swapimpossible]</strong></p>"; 
	} 
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>