<?
## v5.22 -> sep. 21, 2005
session_start();
if (($_SESSION['memberid']=="Active" && $_SESSION['accsusp']!=2) && ($_SESSION['adminarea']!="Active")) {
	echo "<script>document.location.href='login.php'</script>";
} else { 

include_once ("config/config.php");

if ($_SESSION['memberid']>0) $userid = $_SESSION['memberid'];
else if ($_SESSION['adminarea']=="Active") $userid = $_REQUEST['user_id'];
else $userid = 0;

$user = getSqlRow("SELECT * FROM probid_users WHERE id = '".$userid."'");
if (!$_SESSION['sess_lang']) {
	include ("config/lang/".$setts['default_lang']."/site.lang");
	$_SESSION['sess_lang']="".$setts['default_lang']."";
} else {
	include ("config/lang/".$_SESSION['sess_lang']."/site.lang");
}

$paidVat = 0;
if ($setts['vat_rate']>0&&$user['vat_exempted']=="N") $paidVat=1;

$auctionid = ( intval($_REQUEST['id'])) ? intval($_REQUEST['id']) : -1;
if ($auctionid>0) {
	switch ($_REQUEST['t']) {
		case '1':
			$total = getSqlField("SELECT amountpaid FROM probid_auctions WHERE ownerid = $userid AND id = $auctionid", 'amountpaid');
			$isInvoice = ($total > 0) ? 1 : 0;
			break;
		case '2':
			$total = getSqlField("SELECT amountpaid FROM probid_winners WHERE sellerid = $userid AND id = $auctionid", 'amountpaid');
			$isInvoice = ($total > 0) ? 1 : 0;
			break;
		default:
			$isInvoice = getSqlNumber ("SELECT id FROM probid_invoices WHERE userid = $userid AND auctionid = $auctionid");
			$total = getSqlField("SELECT sum(feevalue) AS total FROM probid_invoices WHERE userid = $userid AND auctionid = $auctionid", "total");
	}
} else $isInvoice = 0;
?>
<html>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lang[codepage];?>">
<body> 
<? if ($isInvoice > 0) { ?>
<table width="750" align="center" class="border"> 
  <tr> 
    <td> <table width="100%" border="0" cellpadding="3" cellspacing="3"> 
        <tr valign="top"> 
          <td class="categories"> 
		  <? $invHeader = getSqlField("SELECT invoice_header FROM probid_gen_setts","invoice_header");
		  if (trim($invHeader)!="") echo addSpecialChars($invHeader);
		  else echo "<img src=\"images/probidlogo.gif\" border=\"0\">"; ?>
            <div style="border-left: 5px solid #cccccc; padding-left: 10px;"> </div></td> 
        </tr> 
        <tr>  
      </table></td> 
  </tr> 
  <tr> 
    <td style="border-bottom: 2px solid #cccccc;">&nbsp;</td> 
  </tr> 
  <tr> 
    <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
        <tr valign="top"> 
          <td width="50%"> <table width="70%" border="0" cellpadding="3" cellspacing="2"> 
              <tr> 
                <td class="c1"><?=$lang['invp_bill_to'];?> 
                  :</td> 
              </tr> 
              <tr class="c2"> 
                <td><?=$user['name'];?><br> 
                  <?=$user['address'];?><br> 
                  <?=$user['city'];?><br> 
                  <?=$user['state'];?><br> 
                  <?=$user['zip'];?><br> 
                  <?=$user['country'];?></td> 
              </tr> 
            </table></td> 
          <td width="50%"> <table width="100%" border="0" cellpadding="3" cellspacing="2"> 
              <tr> 
                <td class="c1"><?=$lang['invp_transaction_id'];?></td> 
                <td class="c4" width="200">&nbsp;</td> 
              </tr> 
              <tr> 
                <td class="c1" nowrap><?=$lang['invp_invoice_tpd'];?></td> 
                <td class="c4"><b><?=date("d-M-Y",time());?></b></td> 
              </tr> 
              <tr> 
                <td class="c1"><?=$lang['invp_invoice_number'];?></td> 
                <td class="c4"><?=$auctionid;?></td> 
              </tr> 
            </table></td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr> 
    <td align="center"> <br> 
      <font size="+2" color="#666666"><b> 
      <?=$lang['invp_invoice']?> 
      </b></font> <br> </td> 
  </tr> 
  <tr> 
    <td> <table width="100%" border="0" cellpadding="3" cellspacing="2"> 
        <tr class="c1" align="center"> 
          <td><?=$lang['invp_quantity'];?></td> 
          <td><?=$lang['invp_description'];?></td> 
          <td><?=$lang['invp_price'];?></td> 
          <td><?=$lang['invp_vat_rate'];?></td> 
          <td><?=$lang['invp_vat'];?></td> 
          <td><?=$lang['invp_total'];?></td> 
        </tr> 
        <?
		$invoices = array();
		switch ($_REQUEST['t']) {
			case '1':
				$invoices[0]['feename'] = $lang['auc_setup_fee'];
				$invoices[0]['feevalue'] = $total;
				break;
			case '2':
				$invoices[0]['feename'] = $lang['end_auc_fee'];
				$invoices[0]['feevalue'] = $total;
				break;
			default:
				$getInvoices = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_invoices WHERE userid = '".$userid."' AND auctionid = '".$auctionid."'");
        while ($row = mysqli_fetch_array($getInvoices)) $invoices[] = $row;
				break;
		}
		
		foreach ($invoices as $invoice) {
			if ($invoice['balance']<=0) {
				$thevar = $lang[credit];
				$balance = abs($invoice['balance']);
			} else {
				$thevar = $lang[debit];
				$balance = $invoice['balance'];
            } ?> 
        <tr class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>"> 
          <td align="center">1</td> 
          <td><?=$invoice['feename'];?></td> 
          <td align="center"> <?
			if ($paidVat)
				echo displayAmount(($invoice['feevalue'] / (1 + $setts['vat_rate'] / 100)),$setts['currency'],"YES");
			else echo $lang[na];              
		  ?> </td> 
          <td align="center"> <? if ($paidVat) echo $setts['vat_rate'] . '%'; else echo $lang[na]; ?> </td> 
          <td align="center"> <?
			if ($paidVat)
				echo displayAmount(($invoice['feevalue'] - ($invoice['feevalue'] / (1 + $setts['vat_rate'] / 100))),$setts['currency'],"YES");
			else echo $lang[na];              
		  ?> </td> 
          <td align="center"> <? echo displayAmount($invoice['feevalue'],$setts['currency'],"YES"); ?> </td> 
        </tr> 
        <? } ?> 
      </table></td> 
  </tr> 
  <tr> 
    <td> <p>&nbsp;</p></td> 
  </tr> 
  <tr> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
        <tr valign="top"> 
          <td width="50%"> <table width="70%" border="0" cellpadding="3" cellspacing="2"> 
              <tr> 
                <td class="c1"><?=$lang['invp_delivery_address'];?> 
                  :</td> 
              </tr> 
              <tr class="c2"> 
                <td><?=$user['name'];?><br> 
                  <?=$user['address'];?><br> 
                  <?=$user['city'];?><br> 
                  <?=$user['state'];?><br> 
                  <?=$user['zip'];?><br> 
                  <?=$user['country'];?></td> 
              </tr> 
            </table></td> 
          <td width="50%"> <table width="100%" border="0" cellpadding="3" cellspacing="2"> 
              <tr> 
                <td class="c1"><?=$lang['invp_carriage'];?></td> 
                <td class="c4" width="200"><? echo displayAmount(0,$setts['currency'],"YES"); ?></td> 
              </tr> 
              <td class="c1"><?=$lang['invp_vat'];?></td> 
                <td class="c4"> <?
				if ($paidVat)
					echo displayAmount($total - ($total / (1 + ($setts['vat_rate'] / 100))), $setts['currency'], "YES");
				else echo $lang[na];
                ?> </td> 
              <tr> 
                <td class="c1"><?=$lang['invp_tev'];?></td> 
                <td class="c4"> <?
				if ($paidVat)
					echo displayAmount($total / (1 + $setts['vat_rate'] / 100),$setts['currency'],"YES");
				else echo displayAmount($total, $setts['currency'],"YES");
                ?> </td> 
              </tr> 
              <tr> 
                <td class="c1"><?=$lang['invp_vat'];?></td> 
                <td class="c4"> <? if ($paidVat) echo $setts['vat_rate'] . '%'; else echo $lang[na]; ?> </td> 
              </tr> 
              <tr> 
                <td class="c1"><?=$lang['invp_tiv'];?></td> 
                <td class="c4"><?=displayAmount($total, $setts['currency'], "YES");?></td> 
              </tr> 
            </table> 
            <table width="100%" border="0" cellpadding="3" cellspacing="2"> 
              <tr> 
                <td class="c1"><?=$lang['invp_invoice_total'];?></td> 
                <td class="c4"><b><? echo displayAmount($total, $setts['currency'],"YES"); ?></b> </td> 
              </tr> 
            </table></td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr> 
    <td><p>&nbsp;</p></td> 
  </tr> 
  <tr> 
    <td> <table width="100%" border="0" cellpadding="3" cellspacing="2"> 
        <tr> 
          <td class="c4"><b><?=$lang['invp_comments'];?> :</b></td> 
          <td width="300">&nbsp;</td> 
        </tr> 
        <tr> 
          <td colspan="2" class="border"> <p><?=$setts['invoice_comments'];?> &nbsp;</p></td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr> 
    <td><p>&nbsp;</p></td> 
  </tr> 
</table> 
<? } else { echo "<p align=\"center\" class=\"contentfont\">".$lang[error_no_invoice]."</p>"; } ?>
</body>
</html>
<? } ?>
