<?php
## v5.25 -> jun. 07, 2006
session_start();

include_once ("config/config.php");
include ("config/lang/$setts[default_lang]/site.lang");

## overwrite $setts['payment gateway']
$setts['payment_gateway']="Paypal";

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
  $value = urlencode(stripslashes($value));
  $req .= "&$key=$value";
}

// post back to PayPal system to validate
$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= 'Content-Length: ' . strlen($req) . "\r\n\r\n";
$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);

// assign posted variables to local variables
// note: additional IPN variables also available -- see IPN documentation
//$item_name = $_POST['item_name'];
$receiver_email = $_POST['receiver_email'];
$item_number = $_POST['item_number'];
$invoice = $_POST['invoice'];
$payment_status = $_POST['payment_status'];
$payment_gross = $_POST['mc_gross'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$payer_email = $_POST['payer_email'];
list($custom, $_REQUEST['table']) = explode('TBL',$_POST['custom']);
$payment_date = $_POST['payment_date'];
$pending_reason = $_POST['pending_reason'];

$currentTime=time();

if ($_REQUEST['table']==1) $_SESSION['table_to_modify']="probid_users";
if ($_REQUEST['table']==2) $_SESSION['table_to_modify']="probid_auctions";
if ($_REQUEST['table']==3) $_SESSION['table_to_modify']="probid_winners";
if ($_REQUEST['table']==4) $theoption=2;

if ($_REQUEST['table']==7) $_SESSION['table_to_modify']="probid_wanted_ads";

if (!$fp) {
  	// ERROR
  	echo "$errstr ($errno)";
} else {
  	fputs ($fp, $header . $req);
  	while (!feof($fp)) {
   	 	$res = fgets ($fp, 1024);
    	if (strcmp ($res, "VERIFIED") == 0) {
		// echo "WE ARE IN THE VERIFIED CYCLE<br>";
	  		if ($payment_status == "Completed"){
				$isTxnId = FALSE;
				$getTxn = getSqlNumber("SELECT * FROM probid_txn_ids WHERE txnid='".$txn_id."' AND processor='".$setts['payment_gateway']."'");
				if ($getTxn) $isTxnId = TRUE;
				
				## only do all this if there is no txn yet.
				if (!$isTxnId) { 
					$addTxn = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_txn_ids (txnid, processor) VALUES ('".$txn_id."','".$setts['payment_gateway']."')");
			
					#-------------------------------------------
					if ($_REQUEST['table'] == 101) {
						$pDetails = explode(";",$custom);
						for ($i=0; $i<count($pDetails); $i++) {
							$dets = explode("_",$pDetails);
							//mysql_query("UPDATE probid_auctions SET paidwithdirectpayment='1' WHERE id='".$pDetails[0]."'");
							mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid=1, directpayment_paid=1
							WHERE buyerid='".$pDetails[1]."' AND auctionid='".$pDetails[0]."'");
						}
					} 
					if ($_REQUEST['table'] == 100) {
						//$pDetails = explode("_",$custom);
						//mysql_query("UPDATE probid_auctions SET	paidwithdirectpayment='1' WHERE id='".$pDetails[0]."'");
						/*
						mysql_query("UPDATE probid_winners SET flag_paid=1, directpayment_paid=1
						WHERE  buyerid='".$pDetails[1]."' AND auctionid='".$pDetails[0]."'");
						*/
						mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid=1, directpayment_paid=1 WHERE  id='".$custom."'");
					} 
					if ($theoption==2) {
						$_SESSION['accsusp']=0;
						$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$custom."'","balance");
						$updatedBalance = (-1) * $currentBalance;
						if ($updatedBalance<=0) {
							$_SESSION['accsusp']=0;
						}
						## if user is suspended, activate the counters
						$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
						if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);
	
						$currentTime = time();
						$updateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
						active='1', payment_status='confirmed', balance='0' WHERE id='".$custom."'");
						$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
						active='1' WHERE ownerid='".$custom."'");
						$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
						active='1' WHERE ownerid='".$custom."'");
						$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
						(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
						('".$custom."','".$lang[payment_fee]."','".$payment_gross."','".$currentTime."','0','payment','".$setts['payment_gateway']."')");
					} else {
						if ($_REQUEST['table']==3) {
							$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
							active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',paymentdate='".$currentTime."',
							txnid='".$txn_id."',processor='".$setts['payment_gateway']."' WHERE auctionid='".$custom."'");
						} else if ($_REQUEST['table']==8) {
							$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
							store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
							## now we submit the accounting details
							$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
							(userid, amountpaid, paymentdate, processor) VALUES
							('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						} else {
							## if we activate the users table, count all auctions
							if ($_REQUEST['table']==1) counterAddUser($custom);
	
							$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
							active = '1',payment_status='confirmed',
							amountpaid='".$payment_gross."',paymentdate='".$currentTime."',
							processor='".$setts['payment_gateway']."' WHERE id='".$custom."'");
							## if we activate the auctions table, count the auction
							if ($_REQUEST['table']==2) {
								$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
								if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
									addcatcount ($auctCat['category']);
									addcatcount ($auctCat['addlcategory']);
								}
							}
							if ($_REQUEST['table']==7) {
								$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
								if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
									addwantedcount ($wantedCat['category']);
									addwantedcount ($wantedCat['addlcategory']);
								}
							}
						}
					}
					#-------------------------------------------
				}
			} else {
				if ($_REQUEST['table'] != 100) {
					if ($_REQUEST['table']==3) $query2 = "UPDATE ".$_SESSION['table_to_modify']." SET active = '0',payment_status='".$payment_status."' WHERE auctionid='".$custom."'";
					else $query2 = "UPDATE ".$_SESSION['table_to_modify']." SET active = '0',payment_status='".$payment_status."' WHERE id='".$custom."'";
					$result2=mysqli_query($GLOBALS["___mysqli_ston"], $query2) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				}
			}
      	} else if (strcmp ($res, "INVALID") == 0) {
			if ($_REQUEST['table'] != 100) {
		    	if ($_REQUEST['table']==3) $query6 = "UPDATE ".$_SESSION['table_to_modify']." SET active = '0',payment_status='INVALID' WHERE auctionid='".$custom."'";
				else $query6 = "UPDATE ".$_SESSION['table_to_modify']." SET active=0,payment_status='INVALID' WHERE id='".$custom."'";
				$result6=mysqli_query($GLOBALS["___mysqli_ston"], $query6) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
      	}
  	}
  	fclose ($fp);
}
?>