<?php
## v5.24 -> apr. 06, 2006

/////////////////////////////////////////////////////////////////
// PHP Pro Bid v4 - Ikobo Payment Module
/////////////////////////////////////////////////////////////////

function get_var($name, $default = 'none') {
  return (isset($_GET[$name])) ? $_GET[$name] : ((isset($_POST[$name])) ? $_POST[$name] : $default);
}

$currentTime=time();

if ($_REQUEST['success']) {
	session_start();
	include_once("config/config.php");
	include ("config/lang/$setts[default_lang]/site.lang");

	## overwrite $setts['payment gateway']
	$setts['payment_gateway']="Ikobo";

	$value = explode("-", get_var('custom'));
	$table = $value[1];
	$custom = $value[0];
	if($table == 100){
		$sellerid = getSqlField("SELECT sellerid FROM probid_winners WHERE id='".$custom."'",'sellerid');
		$sellerDetails = getSqlRow("SELECT ikoboid,ikoboipn FROM probid_users WHERE id='".$sellerid."'");
		$IKOBO['MERCHANT'] = $sellerDetails['ikoboid'];
		$IKOBO['PASSWORD'] = $sellerDetails['ikoboipn'];
	} else {
		if (strlen($IKOBO['MERCHANT']) < 1) $IKOBO['MERCHANT'] = $setts['ikobombid'];
		if (strlen($IKOBO['PASSWORD']) < 1) $IKOBO['PASSWORD'] = $setts['ikoboipn'];
	}

	$account_no = get_var('account_no');
  	if ((!empty($account_no) && strlen($account_no) >=8) && (get_var('pwd') == $IKOBO['PASSWORD'])) {
    	$value = explode("-", get_var('custom'));
    	$table = $value[1];
    	$custom = $value[0];
    	$txnid = get_var('confirmation');

   	if ($table == 1) $table_to_modify="probid_users";
   	if ($table == 2) $table_to_modify="probid_auctions";
   	if ($table == 3) $table_to_modify="probid_winners";
		if ($table == 7) $table_to_modify="probid_wanted_ads";
		
		$isTxnId = FALSE;
		$getTxn = getSqlNumber("SELECT * FROM probid_txn_ids WHERE txnid='".$txnid."' AND processor='".$setts['payment_gateway']."'");
		if ($getTxn) $isTxnId = TRUE;
		
		## only do all this if there is no txn yet.
		if (!$isTxnId) { 
			$addTxn = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_txn_ids (txnid, processor) VALUES ('".$txnid."','".$setts['payment_gateway']."')");
			
			$payment_gross = get_var('total');
			if ($table == 4) {
				$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$custom."'","balance");
				$updatedBalance = (-1) * $currentBalance;
				if ($updatedBalance<=0) {
					$_SESSION['accsusp']=0;
				}
				## if user is suspended, activate the counters
				$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
				if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);
	
				$currentTime = time();
				$updateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				active='1', payment_status='confirmed', balance='0' WHERE id='".$custom."'");
				$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
				active='1' WHERE ownerid='".$custom."'");
				$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
				active='1' WHERE ownerid='".$custom."'");
				$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
				(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
				('".$custom."','$lang[payment_fee]','".$invoiceBalance."','".$currentTime."','0','payment','".$setts['payment_gateway']."')");
			} else {
				if ($table == 100) {
					//$pDetails = explode("_",$custom);
					//mysql_query("UPDATE probid_auctions SET	paidwithdirectpayment='1' WHERE id='".$pDetails[0]."'");
					/*
					mysql_query("UPDATE probid_winners SET flag_paid=1, directpayment_paid=1
					WHERE  buyerid='".$pDetails[1]."' AND auctionid='".$pDetails[0]."'");
					*/
					mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid=1, directpayment_paid=1 WHERE  id='".$custom."'");
				}
	
				if ($table == 3) { 
					$query1 = "UPDATE ".$table_to_modify." SET 
					active = '1',payment_status='confirmed',amountpaid='".$payment_gross."', 
					txnid='".$txnid."',
					paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' WHERE auctionid='".$custom."'";
				} else if ($table == 8) {
					$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
					store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					## now we submit the accounting details
					$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
					(userid, amountpaid, paymentdate, processor) VALUES
					('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					} else { 
					$query1 = "UPDATE ".$table_to_modify." SET 
					active='1', payment_status='confirmed',amountpaid='".$payment_gross."',
					paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' WHERE id='".$custom."'";
					## if we activate the users table, count all auctions
					if ($table==1) counterAddUser($custom);
					## if we activate the auctions table, count the auction
					if ($table==2) {
						$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
						if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
							addcatcount ($auctCat['category']);
							addcatcount ($auctCat['addlcategory']);
						}
					}
					if ($table==7) {
						$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
						if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
							addwantedcount ($wantedCat['category']);
							addwantedcount ($wantedCat['addlcategory']);
						}
					}
				}
				mysqli_query($GLOBALS["___mysqli_ston"], $query1) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
    	}
		//echo "RESP=OK\r\n";
  	}
	countCategories();
	include ("paymentdone-endauction.php");
}
?>