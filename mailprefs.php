<? 
## v5.24 -> apr. 03, 2006
session_start();

if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	header ("Location: login.php");
}

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

/*header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] ".$_SESSION['membername']);*/

include("membersmenu.php");

if (isset($_POST['updatembinfook'])) {
	$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	mail_accsusp='1', mail_auctionsold='".$_POST['mail_auctionsold']."',
	mail_auctionwon='".$_POST['mail_auctionwon']."', mail_buyerdetails='".$_POST['mail_buyerdetails']."',
	mail_sellerdetails='".$_POST['mail_sellerdetails']."', mail_itemwatch='".$_POST['mail_itemwatch']."',
	mail_auctionclosed='".$_POST['mail_auctionclosed']."', mail_wantedoffer='".$_POST['mail_wantedoffer']."',
	mail_outbid='".$_POST['mail_outbid']."', mail_keywordmatch='".$_POST['mail_keywordmatch']."',
	mail_conftosell='".$_POST['mail_conftosell']."'     
	WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

	echo "<p align=center class=contentfont>$lang[mail_prefs_updated]</p>";
}
$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'"); ?>
<table border="0" cellspacing="0" cellpadding="0" height="30" align="center">
   <tr>
      <td class="categoryitem">
      <a href="membersarea.php?page=preferences"><?=$lang[Cap_members]?></a>&nbsp;|&nbsp;
      <a href="invoicelook.php"><?=$lang[invlook]?></a>&nbsp;|&nbsp;<a href="mailprefs.php">
         <?=$lang[mailprefs];?>
         </a></td>
   </tr>
</table>
<form action="mailprefs.php" method="post"> 
<p align="center" class="contentfont"><?=$lang[mailprefs_desc];?></p>
  <table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border"> 
    <tr> 
      <td colspan="3" class="c1"><b> 
        <?=$lang[mailprefs]?> 
      </b></td> 
    </tr> 
    <tr class="c2"> 
      <td nowrap class="contentfont"><?=$lang[mail_aucsoldnotif1];?></td> 
      <td class="contentfont"><input name="mail_auctionsold" type="radio" value="1" <? echo (($user['mail_auctionsold']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_auctionsold" value="0" <? echo (($user['mail_auctionsold']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_aucsoldnotif2];?></td> 
    </tr> 
    <tr class="c3"> 
      <td nowrap class="contentfont"><?=$lang[mail_buyerdets1];?></td> 
      <td class="contentfont"><input name="mail_buyerdetails" type="radio" value="1" <? echo (($user['mail_buyerdetails']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_buyerdetails" value="0" <? echo (($user['mail_buyerdetails']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_buyerdets2];?></td> 
    </tr> 
    <tr class="c2"> 
      <td nowrap class="contentfont"><?=$lang[mail_auctwonnotif1];?></td> 
      <td class="contentfont"><input name="mail_auctionwon" type="radio" value="1" <? echo (($user['mail_auctionwon']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_auctionwon" value="0" <? echo (($user['mail_auctionwon']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_auctwonnotif2];?></td> 
    </tr> 
    <tr class="c3"> 
      <td nowrap class="contentfont"><?=$lang[mail_auctsellerdets1];?></td> 
      <td class="contentfont"><input name="mail_sellerdetails" type="radio" value="1" <? echo (($user['mail_sellerdetails']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_sellerdetails" value="0" <? echo (($user['mail_sellerdetails']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_sellerdets2]?></td> 
    </tr> 
    <tr class="c2"> 
      <td nowrap class="contentfont"><?=$lang[mail_itemwatch];?></td> 
      <td class="contentfont"><input name="mail_itemwatch" type="radio" value="1" <? echo (($user['mail_itemwatch']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_itemwatch" value="0" <? echo (($user['mail_itemwatch']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_itemwatch2];?></td> 
    </tr> 
    <tr  class="c3"> 
      <td nowrap class="contentfont"><?=$lang[mail_auctclosed];?></td> 
      <td class="contentfont"><input name="mail_auctionclosed" type="radio" value="1" <? echo (($user['mail_auctionclosed']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_auctionclosed" value="0" <? echo (($user['mail_auctionclosed']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_auctclosed2];?></td> 
    </tr> 
	 <? if (@eregi("Y",$setts['enable_wantedads'])) { ?>
    <tr class="c2"> 
      <td nowrap class="contentfont"><?=$lang[mail_wantoffer1];?></td> 
      <td class="contentfont"><input name="mail_wantedoffer" type="radio" value="1" <? echo (($user['mail_wantedoffer']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_wantedoffer" value="0" <? echo (($user['mail_wantedoffer']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_wantoffer2];?></td> 
    </tr> 
	 <? } ?>
    <tr class="c3"> 
      <td nowrap class="contentfont"><?=$lang[mail_outbid1];?></td> 
      <td class="contentfont"> <input name="mail_outbid" type="radio" value="1" <? echo (($user['mail_outbid']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_outbid" value="0" <? echo (($user['mail_outbid']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_outbid2];?></td> 
    </tr> 
    <tr class="c2"> 
      <td nowrap class="contentfont"><?=$lang[mail_keywmatch1];?></td> 
      <td class="contentfont"><input name="mail_keywordmatch" type="radio" value="1" <? echo (($user['mail_keywordmatch']==1)?"checked":"");?>>
        <?=$lang[yes]?>
        <input type="radio" name="mail_keywordmatch" value="0" <? echo (($user['mail_keywordmatch']==0)?"checked":"");?>>
        <?=$lang[no]?></td> 
      <td class="smallfont"><?=$lang[mail_keywmatch2];?></td> 
    </tr> 
    <tr> 
      <td colspan="3" align="center" class="c4"><input name="updatembinfook" type="submit" id="updatembinfook" value="<?=$lang[updatebutton]?>"></td> 
    </tr> 
  </table> 
</form> 
<br> 
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>