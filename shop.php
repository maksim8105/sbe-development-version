<?
## v5.24 -> apr. 06, 2006
session_start();
include_once ("config/config.php");

if ($setts['stores_enabled']!="Y") {
	echo "<script>document.location.href='index.php'</script>";
} else { 

include ("themes/".$setts['default_theme']."/header.php"); 


$shopDets = getSqlRow("SELECT store_name, username, store_template, shop_logo, store_active, 
store_about, store_specials, store_shippinginfo, store_policies, store_featured_items, 
store_endingsoon_items, store_recentlylisted_items FROM probid_users WHERE id='".$_REQUEST['userid']."'");

if (@eregi("favourites",$_GET['option'])) {
	if ($_SESSION['memberid']) {
		$isFav = getSqlNumber("SELECT id FROM probid_favourite_stores WHERE storeid='".$_GET['userid']."' AND userid='".$_SESSION['memberid']."'");
		if (!$isFav) $addFavStore = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_favourite_stores (storeid, userid) VALUES ('".$_GET['userid']."', '".$_SESSION['memberid']."')");
		else $delFavStore = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_favourite_stores WHERE storeid='".$_GET['userid']."' AND userid='".$_SESSION['memberid']."'");
	}
}

$logo = $shopDets['shop_logo'];
if (!$_REQUEST['orderField']) $orderField = "enddate";
else $orderField=$_REQUEST['orderField'];
if (!$_REQUEST['orderType']) {
	$orderType = "ASC";
	$newOrder="DESC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}
	
$searchPattern = " WHERE ownerid='".$_REQUEST['userid']."' AND active='1' AND closed='0' AND deleted!='1' AND listin!='auction' ";
if ($_GET['start'] == "") $start = 0;
else $start = $_GET['start'];
$limit = 50;
	
$totalAuctions = getSqlNumber("SELECT * FROM probid_auctions 
".$searchPattern." ORDER BY ".$orderField." ".$orderType.""); 
$auctionsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions 
".$searchPattern." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");

$additionalVars = "&userid=".$_REQUEST['userid']."&parent=".$_REQUEST['parent'];

if ($shopDets['store_active']==1) { 
	echo '<link href="store_templates/store'.$shopDets['store_template'].'/store_style.css" rel="stylesheet" type="text/css"> ';
	include_once ("store_templates/store".$shopDets['store_template']."/header.php"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="menu_shop">
   <tr align="center">
      <td class="bordermenu_shop"><a href="aboutme.php?userid=<?=$_GET['userid'];?>">
         <?=$lang[store_home]?>
         </a></td>
      <td class="bordermenu_shop"><a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_about">
         <?=$lang[store_about]?>
         </a></td>
      <td class="bordermenu_shop"><a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_specials">
         <?=$lang[store_specials]?>
         </a></td>
      <td class="bordermenu_shop"><a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_shippinginfo">
         <?=$lang[store_shippinginfo]?>
         </a></td>
      <td class="bordermenu_shop"><a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_policies">
         <?=$lang[store_policies]?>
         </a></td>
      <td class="bordermenu_shop"><a href="otheritems.php?owner=<?=$_GET['userid'];?>">
         <?=$lang[view_auctions]?>
         </a></td>
      <td class="bordermenu_shop"><a href="viewfeedback.php?owner=<?=$_GET['userid'];?>">
         <?=$lang[view_feedback]?>
         </a></td>
   </tr>
</table>
<? 
## display special page if selected
if (!empty($_REQUEST['page'])) {
	echo '<p>'.addSpecialChars($shopDets[$_REQUEST['page']]).'</p>';
} else {

## seach in shop form
echo '<table width="100%" border="0" cellpadding="3" cellspacing="1" class="search_shop" align="center"> ';
echo '	<form action="shop.php" method="get"> ';
echo '	<input type="hidden" name="userid" value="'.$_REQUEST['userid'].'">';
echo '	<tr> ';
echo '		<td nowrap align="center">'.$lang[srcitemsinshop].'&nbsp;<INPUT size=25 name="basicsearch">&nbsp; ';
echo '			<input name="searchok" type="submit" value="'.$lang[search].'"> &nbsp;</td> ';
echo '	</tr> ';
echo '	</form> ';
echo '</table>';

## now we display featured items
if ($shopDets['store_featured_items']) { 
	$getFeatItems=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, itemname, picpath, bidstart, maxbid, enddate, currency FROM probid_auctions WHERE 
	active=1 AND closed=0 AND deleted!=1 AND listin!='auction' AND ownerid='".$_REQUEST['userid']."' ORDER BY rand() LIMIT 0,".$shopDets['store_featured_items']." ");
	$nbFeatItems=mysqli_num_rows($getFeatItems);
	
	(array) $featItem = NULL;
	(int) $featCnt = 0;
			
	while ($hpFeatured=mysqli_fetch_array($getFeatItems)) $featItem[$featCnt++] = $hpFeatured; 
	
	echo '<table width="100%" border="0" cellpadding="2" cellspacing="2"> ';
	echo '	<tr> ';

	$width=(100/$shopDets['store_featured_items'])."%";
	
	for ($i=0;$i<$shopDets['store_featured_items'];$i++) {
		if (!empty($featItem[$i]['itemname'])) { 
			echo '<td width="'.$width.'" align="center" valign="top"  class="border_shop"> ';
			echo '	<table width="100%" border="0" cellspacing="2" cellpadding="2" class="feat_borders"> ';
			echo '		<tr><td colspan="2" class="c1_shop"><a href="'.$path.processLink('auctiondetails', array('itemname' => $featItem[$i]['itemname'], 'id' => $featItem[$i]['id'])).'">'.$featItem[$i]['itemname'].'</a></td></tr> ';
			echo '		<tr class="c3_shop smallfont_shop"> ';
			echo '			<td align="center"><a href="'.$path.processLink('auctiondetails', array('itemname' => $featItem[$i]['itemname'], 'id' => $featItem[$i]['id'])).'"><img src="'.((!empty($featItem[$i]['picpath']))?"$setts[siteurl]/makethumb.php?pic=".$featItem[$i]['picpath']."&w=".$layout['w_feat_hp']."&sq=Y":"themes/".$setts['default_theme']."/img/system/noimg.gif").'" border="0"></a></td> ';
			echo '		</tr> ';
			echo '		<tr> ';
			echo '			<td colspan="2" class="c2_shop smallfont_shop"><b>'.strtoupper($lang[startbid]).':</b> '.displayAmount($featItem[$i]['bidstart'],$featItem[$i]['currency']).'<br><b>'.$lang[currbid].' :</b> '.displayAmount($featItem[$i]['maxbid'],$featItem[$i]['currency']).'</td> ';
			echo '		</tr> ';
			echo '		<tr> ';
			echo '			<td colspan="2" class="c3_shop smallfont_shop"><b>'.$lang[ends].' :</b> ';
			echo '				'.format_date($featItem[$i]['enddate'],2).' </td> ';
			echo '		</tr> ';
			echo '	</table> ';
			echo '</td> ';
		} else { echo '<td width="'.$width.'"></td>'; } 
	}
	echo '	</tr> ';
	echo '</table><br> ';
		
}

## now we display ending soon and recently listed items
echo '<table width="100%" cellpadding="0" cellspacing="0" border="0"> ';
echo '	<tr> ';
echo '		<td width="48%" valign="top"> ';

## ending soon first
if ($shopDets['store_endingsoon_items']) {
	echo '<table width="100%" border="0" cellpadding="3" cellspacing="1" class="feat_borders"> ';
	echo ' 	<tr height="17"> ';
	echo '		<td width="100%" class="c1_shop" colspan="5">&nbsp;&raquo;&nbsp;'.$lang[Ending_Soon].'</td> ';
	echo '	</tr> ';
	echo '	<tr height="15" class="c4_shop"> ';
	echo '		<td></td> ';
	echo '		<td class="smallfont_shop" width="100%">&nbsp;<b>'.$lang[auction_title].'</b></td> ';
	echo '		<td class="smallfont_shop" nowrap>&nbsp;</td> ';
	echo '	</tr> ';

	$getClosingAuctions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, enddate, itemname, bidstart, currency, maxbid FROM probid_auctions WHERE 
	active=1 AND closed=0 AND deleted!=1 AND listin!='auction' AND ownerid='".$_REQUEST['userid']."' ORDER BY enddate ASC LIMIT ".$shopDets['store_endingsoon_items']."");
	while ($closingAuctions=mysqli_fetch_array($getClosingAuctions)) { 
		echo '<tr height="15" class="'.((($count++)%2==0) ? "c2_shop":"c3_shop").'"> ';
		echo '	<td width="11"><img src="themes/'.$setts['default_theme'].'/img/arr_it.gif" width="11" height="11" hspace="4"></td> ';
		echo '	<td width="100%" class="contentfont_shop"><a href="'.$path.processLink('auctiondetails', array('itemname' => $closingAuctions['itemname'], 'id' => $closingAuctions['id'])).'">'.titleResize($closingAuctions['itemname']).'</a><br> ';
		echo '	<div class="smallfont_shop"><b>'.$lang[timeleft].'</b>: ';

		$daysleft=timeleft($closingAuctions['enddate'],$setts['date_format']);
		echo (($daysleft>=0)?"$daysleft":"bidding closed"); 
		echo '	</div> ';
		echo '	<div class="smallfont_shop"><b>'.$lang[currently].':</b> '.displayAmount((($closingAuctions['maxbid']>0)?$closingAuctions['maxbid']:$closingAuctions['bidstart']),$closingAuctions['currency']).'</div></td> ';
		echo '	<td nowrap>'.itemPics($closingAuctions['id']).'</td> ';
		echo '</tr> ';
	}
	echo '</table> ';
}
echo '		</td> ';
echo '		<td width="4%" valign="top"> </td> ';
echo '		<td width="48%" valign="top"> ';

## now recently listed
if ($shopDets['store_recentlylisted_items']) {
	echo '<table width="100%" border="0" cellpadding="3" cellspacing="1" class="feat_borders"> ';
	echo ' 	<tr height="17"> ';
	echo '		<td width="100%" class="c1_shop" colspan="3">&nbsp;&raquo;&nbsp;'.$lang[Recently_Listed_Auction].'</td> ';
	echo '	</tr> ';
	echo '	<tr height="15" class="c4_shop"> ';
	echo '		<td></td> ';
	echo '		<td class="smallfont_shop" nowrap="nowrap">&nbsp;<b>'.$lang[auction_title].'<b></td> ';
	echo '		<td class="smallfont_shop" nowrap>&nbsp;</td> ';
	echo '	</tr> ';
   
	$getOpenAuctions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, startdate, bidstart, currency, itemname FROM probid_auctions WHERE 
	closed=0 AND active=1 AND deleted!=1 AND listin!='auction' AND ownerid='".$_REQUEST['userid']."' ORDER BY startdate DESC LIMIT ".$shopDets['store_recentlylisted_items']."");
	while ($openAuctions = mysqli_fetch_array($getOpenAuctions)) {
		echo '<tr height="15" class="'.((($count++)%2==0) ? "c2_shop":"c3_shop").'"> ';
		echo '	<td width="11"><img src="themes/'.$setts['default_theme'].'/img/arr_it.gif" width="11" height="11" hspace="4"></td> ';
		echo '	<td width="100%" class="contentfont_shop"><a href="'.$path.processLink('auctiondetails', array('itemname' => $openAuctions['itemname'], 'id' => $openAuctions['id'])).'">'.titleResize($openAuctions['itemname']).'</a><br>';
		echo '	<div class="smallfont_shop"><b>'.$lang[start_date].':</b>&nbsp;'.format_date($openAuctions['startdate'],2).'<div>';
		echo '	<div class="smallfont_shop"><b>'.$lang[startbid].':</b>&nbsp;'.displayAmount($openAuctions['bidstart'],$openAuctions['currency']).'</div></td> ';
		echo '	<td nowrap>'.itemPics($openAuctions['id']).'</td> ';
		echo '</tr> ';
	}
	echo '</table> ';
}
echo '		</td> ';
echo '	</tr> ';
echo '</table> ';
echo '<br>';
?>

<?
$parent=$_REQUEST['parent'];
if($parent > 0) {
	$croot = $parent;
	$nav = "";
	$cntr = 0;
	while ($croot>0) {
		$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,parent FROM probid_categories WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$crw = mysqli_fetch_array($sbcts);
		if($cntr == 0) {
			$nav = $c_lang[$crw['id']];
		} else {
			if($parent != $croot) {
				$nav = "<a href=\"shop.php?parent=".$crw[id]."&userid=".$_GET['userid']."\">".$c_lang[$crw['id']]."</a> > ".$nav;
			}
		}
		$cntr++;
		$croot = $crw['parent'];
	}
	$navigation = "<a href=\"shop.php?userid=".$_GET['userid']."\"><b>$lang[shop]:</b></a> $nav";
} else {
	$navigation = "<a href=\"shop.php?userid=".$_GET['userid']."\"><b>$lang[shop]:</b></a> $lang[allcats]";		
}	
?>

<table width="100%" border="0" cellspacing="0" cellpadding="3">
   <tr><td class="catnav_shop"><?=$navigation;?></td></tr>
</table><br>

<table width="100%" border="0" cellpadding="3" cellspacing="0" class="contentfont_shop">

            <? 
				## if store is active and listin!=auction only activate the store categories
				$adCatQuery = "";
				$selectedCats = getSqlField("SELECT store_categories FROM probid_users WHERE id='".$_GET['userid']."'","store_categories");
				if ($selectedCats!=""&&($_REQUEST['parent']==0||trim($_REQUEST['parent'])=="")) {
					$store_cats_array = substr($selectedCats,0,-1);
					$adCatQuery = " AND id IN ($store_cats_array) ";
				}
		  
				$getSubCategories=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,items_counter FROM probid_categories WHERE parent='".$_REQUEST['parent']."' ".$adCatQuery." ORDER BY theorder ASC, name ASC");
				while ($subCategories=mysqli_fetch_array($getSubCategories)) { ?>
            <a href="shop.php?parent=<?=$subCategories['id'];?>&userid=<?=$_GET['userid'];?>"><? echo $c_lang[$subCategories['id']]?></a>
            |
           <? } ?>
         </table><br>


<table width="100%" border="0" cellspacing="2" cellpadding="0">
   <tr>
      <? $isSubcats = getSqlNumber("SELECT id FROM probid_categories WHERE parent='".$_REQUEST['parent']."'");
		if ($isSubcats>0) { ?>
     
      <? } ?>
      <td valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="3">
            <tr class="c1_shop">
               <td width="100" align="center" class="submenu_shop"><?=$lang[picture]?></td>
               <td align="center" class="submenu_submenu"><a href="shop.php?parent=<?=$_REQUEST['parent'];?>&start=<?=$_REQUEST['start'];?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_name]?>
                  </a></td>
               <td width="170" align="center" class="submenu_shop"><a href="shop.php?parent=<?=$_REQUEST['parent'];?>&start=<?=$_REQUEST['start'];?>&orderField=bidstart&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[currbid]?>
                  </a></td>
          <? /*     <td width="70" align="center" class="submenu_shop"><a href="shop.php?parent=<?=$_REQUEST['parent'];?>&start=<?=$_REQUEST['start'];?>&orderField=maxbid&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[lastbid]?>
                  </a></td>
					<? if ($setts['buyout_process']==0) { ?>
               <td width="70" align="center" class="submenu_shop"><a href="shop.php?parent=<?=$_REQUEST['parent'];?>&start=<?=$_REQUEST['start'];?>&orderField=bnvalue&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[buyout]?>
                  </a></td> 
					<? } ?> */ ?>
               <td width="40" align="center" class="submenu_shop"><a href="shop.php?parent=<?=$_REQUEST['parent'];?>&start=<?=$_REQUEST['start'];?>&orderField=nrbids&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[bids]?>
                  </a></td>
               <td width="130" align="center" class="submenu_shop"><a href="shop.php?parent=<?=$_REQUEST['parent'];?>&start=<?=$_REQUEST['start'];?>&orderField=enddate&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[endsin]?>
                  </a></td>
            </tr>
            <tr class="c5_shop">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
              <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <?
				// newest function 
		
				if ($_GET['start'] == "") $start = 0;
				else $start = $_GET['start'];
				$limit = 50;
				
				$croot = $_REQUEST['parent'];
				$cntr=0;
				while ($croot>0) {
					$crw = getSqlRow("SELECT * FROM probid_categories WHERE id='$croot'");
					if($cntr == 0) {
						$catname = $crw['name'];
					} else {
						if($crw['parent'] != $croot) {
							$catname = "$crw[name] : $catname";
						}
					}
					$cntr++;
					$croot = $crw['parent'];
				}
		
				$catname = trim($catname);
		
				## get all subcats
				reset($cat_array);
				if ($catname!="") {
					while (list($cat_array_id, $cat_array_details)=each($cat_array)) {
						list($cat_array_name, $userid) = $cat_array_details;
						$strResult = strpos($cat_array_name,$catname);
						if (trim($strResult)=="0") $cat_id[$catcnt++] = $cat_array_id;
					}
					$all_subcats = implode (",",$cat_id);
					$subcatquery="(a.category IN (".$all_subcats.") OR a.addlcategory IN (".$all_subcats.")) AND ";
				} else { 
					$subcatquery = "";
				}
		
				$mysqlVersion = substr(((is_null($___mysqli_res = mysqli_get_server_info($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res),0,1);
		
				$additionalVars = "&userid=".$_REQUEST['userid']."&parent=".$_REQUEST['parent']."&basicsearch=".$_REQUEST['basicsearch'];
				
				if (!empty($_REQUEST['basicsearch'])) $searchQuery = " AND keywords LIKE '%".$_REQUEST['basicsearch']."%' ";
				
				if ($mysqlVersion>=4) {
					$additionalQuery = "SQL_CALC_FOUND_ROWS ";
				} else {
					## this is only used if the mySql is older than v4. Slower version
					$nbItems = getSqlNumber("SELECT DISTINCT a.id, a.itemname, a.picpath, a.maxbid, a.currency, 
					a.nrbids, a.enddate FROM probid_auctions a WHERE
					".$subcatquery." 
					a.active=1 AND a.closed=0 AND a.deleted!=1 AND a.listin!='auction' AND ownerid='".$_GET['userid']."' ".$searchQuery." 
					ORDER BY a.".$orderField." ".$orderType);
				}
				$getAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT ".$additionalQuery." DISTINCT a.id, a.itemname, a.hpfeat, a.picpath, a.maxbid, a.currency, a.bn, a.bnvalue,
				a.nrbids, a.enddate, a.bidstart, a.bn, a.bnvalue FROM probid_auctions a WHERE
				".$subcatquery." 
				a.active=1 AND a.closed=0 AND a.deleted!=1 AND a.listin!='auction' AND ownerid='".$_GET['userid']."' ".$searchQuery."
				ORDER BY a.".$orderField." ".$orderType." LIMIT $start,$limit");
		
				$nbQuery = mysqli_query($GLOBALS["___mysqli_ston"], "Select FOUND_ROWS() AS nbr");
				$nbItems = mysqli_fetch_assoc($nbQuery)['nbr'];

				if ($nbItems==0) {
					echo "<tr><td colspan=5 class=contentfont_shop align=center>".$lang[nostore]."</td></tr>";
				} else {
					if ($nbItems>($start+$limit)) $final = $start+$limit;
					else $final = $nbItems;
						
					while ($auction = mysqli_fetch_array($getAuctions)) { ?>
            <tr class="<? echo (($count++)%2==0)?"c2_shop":"c3_shop"; ?>">
               <td align="center"><? 
						echo "<a href=\"".processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">";
						echo "<img src=\"$setts[siteurl]/makethumb.php?pic=".((!empty($auction['picpath'])) ? $auction['picpath'] : "themes/".$setts['default_theme']."/img/system/noimg.gif")."&w=100&sq=Y&b=Y\" border=\"0\">";
						echo "</a>"; ?>
               </td>
               <td class="contentfont_shop"><span class="<? echo setStyle($auction['id']);?>"><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']));?>">
                  <?=$auction['itemname'];?>
                  </a>
                  <? /* echo itemPics($auction['id']);
						if ($auction ['bn'] =="Y") echo "<img src=\"store_templates/images/buynow".$shopDets['store_template'].".gif\" align=\"absmiddle\">"; */?>
                  </span> </td>
      <? /*    <td align="center" class=contentfont_shop><? echo displayAmount($auction['bidstart'],$auction['currency']);?></td>
               <td align="center" class=contentfont_shop><? echo displayAmount($auction['maxbid'],$auction['currency']);?></td> */?>
					<? if ($setts['buyout_process']==0) { ?>
				<td align="center">
				<? 
                  if ($auction['nrbids']==0)
                  {
               	  echo displayAmount($auction['bidstart'],$auction['currency']);
               	  echo "<br>";
               	  echo itemPics($auction['id']);
               	  }
               	  else
               	  {
               	  echo displayAmount($auction['maxbid'],$auction['currency']);
               	  echo "<br><br>";
               	  
               	  if ($auction['bn']=='Y')  echo displayAmount($auction['bnvalue'],$auction['currency'])."<br>";
               	  echo itemPics($auction['id']);
               	  }
               	  
               	?>
				
				
				
				
				</td>
					<? } ?>
               <td class=contentfont align="center"><b>
                  <?=$auction['nrbids'];?>
                  </b> </td>
               <td align="center" class=contentfont_shop><? echo timeleft($auction['enddate'],$setts['date_format']);?></td>
            </tr>
            <? } ?>
            <tr>
               <td colspan=5 class=contentfont_shop align=center><? 
		  				paginate($start,$limit,$nbItems,"shop.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); 
		  			?></td>
            </tr>
            <? }?>
         </table></td>
   </tr>
</table>
<table width="100%" border="0" cellspacing="4" cellpadding="0" height="30" >
   <tr>
      <td class="footer_shop" align="center"><a href="aboutme.php?userid=<?=$_GET['userid'];?>">
         <?=$lang[store_home]?>
         </a> | <a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_about">
         <?=$lang[store_about]?>
         </a> | <a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_specials">
         <?=$lang[store_specials]?>
         </a> | <a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_shippinginfo">
         <?=$lang[store_shippinginfo]?>
         </a> | <a href="shop.php?userid=<?=$_GET['userid'];?>&page=store_policies">
         <?=$lang[store_policies]?>
         </a> <strong>|</strong> <a href="otheritems.php?owner=<?=$_GET['userid'];?>">
         <?=$lang[view_auctions]?>
         </a> | <a href="viewfeedback.php?owner=<?=$_GET['userid'];?>">
         <?=$lang[view_feedback]?>
         </a> </td>
   </tr>
</table>
<? }
	include_once ("store_templates/store".$shopDets['store_template']."/footer.php"); 
	
	} else echo $lang[storeinactive];
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
