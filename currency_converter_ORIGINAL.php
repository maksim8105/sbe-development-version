<?
## v5.24 -> apr. 04, 2006

session_start();
include ("config/config.php");
include ("config/currency_converter.inc.php");
include ("themes/".$setts['default_theme']."/title.php");

if(!$_SESSION['sess_lang']) { 
	include ("config/lang/".$setts['default_lang']."/site.lang"); 
	$_SESSION['sess_lang']="".$setts['default_lang'].""; 
} else { 
	include ("config/lang/".$_SESSION['sess_lang']."/site.lang"); 
}


$query = "SELECT id,country,symbol FROM probid_exchange ORDER BY country";
$res_ = @mysqli_query($GLOBALS["___mysqli_ston"], $query);
if(!$res_) {
  	print ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
  	exit;
} else if(@mysqli_num_rows($res_) > 0) {
  	while($row = @mysqli_fetch_array($res_)) {
		$CURRENCIES[$row[id]] = "$row[symbol] $row[country]";
		$CURRENCIES_SYMBOLS[$row[id]] = "$row[symbol]";
	}
}

?>
<HTML>
<HEAD>
<TITLE>
<?=$setts['sitename']?>
-
<?=$lang[convert_title]?>
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR="#ffffff" TEXT="#000000" LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0">
   <TR>
      <TD WIDTH="25%"><img src="images/probidlogo.gif" border="0"> </TD>
      <TD><center>
            <?=$lang[convert_attn]?>
         </center></TD>
   </TR>
</TABLE>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="4">
   <TR>
      <TD><FORM NAME="form1" METHOD="post" ACTION="">
            <BR>
            <TABLE WIDTH="95%" BORDER="0" CELLSPACING="0" CELLPADDING="1" ALIGN="CENTER" BGCOLOR="#666666">
               <TR>
                  <TD><TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="5" BGCOLOR="#ffffff"><? 
								if($_POST[action] == "convert") { 
									$convert_from = $_POST['from'];
									$convert_to = $_POST['to'];
									$convert_amount = $_POST['amount'];
									
									$money[0]=file("http://quote.yahoo.com/m5?a=".$convert_amount."&t=".$convert_to."&s=".$convert_from);
									
									for($i=0; $i<sizeof($money); $i++)
									{ 
										$money[$i] = join("",$money[$i]); 
										$money[$i] = @ereg_replace(".*<table border=1 cellpadding=2 cellspacing=0>",'',$money[$i]); 
										$money[$i] = @ereg_replace("</table>.*",'',$money[$i]); 
										$money[$i] = @ereg_replace("</b>.*",'',$money[$i]); 
										$money[$i] = @ereg_replace(".*<b>",'',$money[$i]); 
									
										$result = $money[$i];
									} ?>
                        <TR VALIGN="TOP" BGCOLOR="#eeeeee">
                           <TD COLSPAN="3" ALIGN=CENTER><b><font color="red">
                              <?=$convert_amount;?>
                              <?=$convert_from;?>
                              = <? echo $result; ?>
                              <?=$_POST['to'];?>
                              </font></b> </TD>
                        </TR>
                        <? } else { ?>
                        <TR VALIGN="TOP" BGCOLOR="#ffffff">
                           <TD COLSPAN="3" ALIGN=CENTER>&nbsp;</TD>
                        </TR>
                        <? } ?>
                        <TR VALIGN="TOP">
                           <TD WIDTH="29%"><?=$lang[convert_amount]?>
                              <BR>
                              <INPUT TYPE="text" NAME="amount" SIZE="10" VALUE=<?=$_GET['AMOUNT']?>>
                           </TD>
                           <TD WIDTH="39%"><?=$lang[convert_from]?>
                              <BR>
                              <SELECT NAME="from">
                                 <?
											#// reset($CURRENCIES);
											while(list($k,$v) = each($CURRENCIES)) {
												print "<OPTION VALUE=\"$CURRENCIES_SYMBOLS[$k]\"";
												if($CURRENCIES_SYMBOLS[$k] == $setts[currency]) {
													print " SELECTED";
												} else if($_POST[from] == $k) {
													print " SELECTED";
												}
												print ">$v</OPTION>\n";
											} ?>
                              </SELECT>
                           </TD>
                           <TD WIDTH="32%"><?=$lang[convert_to]?>
                              <BR>
                              <SELECT NAME="to">
                                 <?
											reset($CURRENCIES);
											while(list($k,$v) = each($CURRENCIES)) {
												print "<OPTION VALUE=\"$CURRENCIES_SYMBOLS[$k]\"";
												if($_POST[to] == $CURRENCIES_SYMBOLS[$k]) print " SELECTED";
												print ">$v</OPTION>\n";
											} ?>
                              </SELECT>
                           </TD>
                        </TR>
                        <TR VALIGN="TOP">
                           <TD COLSPAN="3"><CENTER>
                                 <INPUT TYPE="hidden" NAME="action" VALUE="convert">
                                 <INPUT TYPE="submit" NAME="Submit" VALUE="<?=$lang[convert_bttn]?>">
                              </CENTER></TD>
                        </TR>
                     </TABLE></TD>
               </TR>
            </TABLE>
         </FORM></TD>
   </TR>
   <TR>
      <TD><CENTER>
            <A HREF="javascript:window.close()">
            <?=$lang[convert_close]?>
            </A>
         </CENTER></TD>
   </TR>
</TABLE>
</BODY>
</HTML>
