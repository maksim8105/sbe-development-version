<? 
## v5.25 -> jun. 29, 2006
session_start();

if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php'</script>";
} else if ($_SESSION['is_seller']!="Y") {
	echo "<script>document.location.href='membersarea.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

/*header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] ".$_SESSION['membername']);*/

include("membersmenu.php");

include ("formchecker.php");

if ($action=="updateprefilled") {
	$pm="";
	for ($i=0;$i<count($_POST['pmethod']);$i++) {
		$pm.=$_POST['pmethod'][$i]."<br>";
	}
	$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	default_duration='".$_POST['default_duration']."', default_private='".$_POST['default_private']."',
	default_isswap='".$_POST['default_isswap']."', default_sc='".$_POST['default_sc']."',
	default_scint='".$_POST['default_scint']."', default_pm='".$pm."',
	default_acceptdirectpayment='".$_POST['default_acceptdirectpayment']."', default_directpaymentemail='".$_POST['default_directpaymentemail']."',
	default_postage_costs='".$_POST['default_postage_costs']."', default_insurance='".$_POST['default_insurance']."',
	default_type_service='".$_POST['default_type_service']."',
	default_shipping_details='".$_POST['default_shipping_details']."',
	default_public_questions='".$_POST['default_public_questions']."',
	default_bid_placed_email='".$_POST['default_bid_placed_email']."', 
	default_itemname='".remSpecialChars($_POST['default_itemname'])."', 
	default_description='".remSpecialChars($_POST['default_description'])."'      
	WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

	echo "<p align=center class=contentfont>$lang[pref_fields_updated]</p>";
}
$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'"); ?>

<form action="prefilledfields.php" method="post">
   <table width="100%" border="0" cellpadding="1" cellspacing="0" align="center" class="border2">
      <tr>
         <td colspan="2" class="c1"><b>
            <?=strtoupper($lang[prefilledfields])?>
            </b></td>
      </tr>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[itemname]?>
            </strong></td>
         <td class="contentfont"><input type="text" size="40" name="default_itemname" value="<?=$user['default_itemname'];?>" /></td>
      </tr>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[desc]?>
            </strong></td>
         <td class="contentfont"><textarea name="default_description" cols="50" rows="4"><?=$user['default_description'];?></textarea>
            <script> 
					var oEdit1 = new InnovaEditor("oEdit1");
					oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit1.height=300;
					oEdit1.REPLACE("default_description");//Specify the id of the textarea here
				</script>
			</td>
      </tr>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[duration]?>
            </strong></td>
         <td class="contentfont"><? echo "<SELECT name=\"default_duration\" class=\"contentfont\">";
	  $getdurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations");
	  echo "<option value=\"\" selected></option>";
	  while ($row=mysqli_fetch_array($getdurations)) {
		  echo "<OPTION value=\"".$row['days']."\" ".(($row['days']==$user['default_duration'])?"SELECTED":"").">".$row['description']."</option>";
	  }
	  echo "</SELECT>";
	  ?></td>
      </tr>
      <tr class="c3">
         <td nowrap class="contentfont"><strong>
            <?=$lang[privateauc]?>
            </strong></td>
         <td class="contentfont"><select name="default_private" id="default_private" class="contentfont">
               <option value="" selected></option>
               <option value="Y" <? echo (($user['default_private']=="Y")?"selected":"");?>>
               <?=$lang[yes]?>
               </option>
               <option value="N" <? echo (($user['default_private']=="N")?"selected":"");?>>
               <?=$lang[no]?>
               </option>
            </select>
         </td>
      </tr>
      <? if ($setts['swap_items']==1) { ?>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[acceptswap]?>
            </strong></td>
         <td class="contentfont"><input name="default_isswap" type="radio" value="N" <? echo(($user['default_isswap']=="N")?"checked":"");?>>
            <?=$lang[no]?>
            <input type="radio" name="default_isswap" value="Y" <? echo(($user['default_isswap']=="Y")?"checked":"");?>>
            <?=$lang[yes]?></td>
      </tr>
      <? } ?>
      <? if ($setts['paypaldirectpayment']==1) { ?>
      <tr class="c3">
         <td nowrap class="contentfont"><strong>
            <?=$lang[shippingcond]?>
            </strong></td>
         <td class="contentfont"><input type="radio" name="default_sc" value="BP" <? echo ($user['default_sc']=="BP")?"checked":""; ?>>
            <?=$lang[buyerpaysshipment]?>
            <br>
            <input type="radio" name="default_sc" value="SP" <? echo ($user['default_sc']=="SP")?"checked":""; ?>>
            <?=$lang[sellerpaysshipment]?>
            <br>
            <input name="default_scint" type="checkbox" id="default_scint" value="Y" <? echo ($user['default_scint']=="Y")?"checked":""; ?>>
            <?=$lang[sellershipinternat]?></td>
      </tr>
      <? } ?>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[paymethods]?>
            </strong></td>
         <td class="contentfont"><? 
	$getdurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_methods");
	$pct=0;
	while ($row=mysqli_fetch_array($getdurations)) {
	  	$pm_name[$pct]=$row['name'];
		$pct++;
	}
	echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	for ($i=0;$i<$pct;$i+=2) {
		$j=$i+1;
		echo "<tr>\n";
		echo "	<td width=\"130\" class=\"contentfont\"><input type=\"checkbox\" name=\"pmethod[]\" value=\"".$pm_name[$i]."\" ".((@eregi($pm_name[$i].'<br>',$user['default_pm']))?"checked":"").">".$pm_name[$i]."</td>\n";
		echo "	<td width=\"130\" class=\"contentfont\">".(($pm_name[$j]!="")?"<input type=\"checkbox\" name=\"pmethod[]\" value=\"".$pm_name[$j]."\" ".((@eregi($pm_name[$j].'<br>',$user['default_pm']))?"checked":"").">".$pm_name[$j]."":"")."</td>\n";
		echo "</tr>\n";
	}
	echo "</table>";
	?></td>
      </tr>
      <? /* if ($setts['paypaldirectpayment']) { ?>
      <tr  class="c3">
         <td nowrap class="contentfont"><strong>
            <?=$lang[directpayment]?>
            </strong></td>
         <td class="contentfont"><table border="0" cellpadding="0" cellspacing="0">
               <tr>
                  <td valign="top"><input type="checkbox" value="1" name="default_acceptdirectpayment" <? if ($user['default_acceptdirectpayment']) echo " checked"; ?>></td>
                  <td width="5">&nbsp;</td>
                  <td><?=$lang[directpaymentmsg2]?></td>
               </tr>
            </table>
            <input size="22" name="default_directpaymentemail" value="<?=$user['default_directpaymentemail'];?>">
            <?=$lang[directpaymentemail]?></td>
      </tr>
      <? } */ ?>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[postagecosts]?>
            </strong></td>
         <td class="contentfont"><?=$setts['currency'];?>
            <input class="contentfont" name="default_postage_costs" type="text" id="default_postage_costs" value="<?=$user['default_postage_costs'];?>" size="15"></td>
      </tr>
      <tr class="c3">
         <td nowrap class="contentfont"><strong>
            <?=$lang[insurance]?>
            </strong></td>
         <td class="contentfont"><?=$setts['currency'];?>
            <input name="default_insurance" type="text" id="default_insurance" value="<?=$user['default_insurance'];?>" class="contentfont" size="15"></td>
      </tr>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[sp_details]?>
            </strong></td>
         <td class="contentfont"><textarea name="default_shipping_details" cols="50" rows="4"><?=$user['default_shipping_details'];?></textarea></td>
      </tr>
      <tr class="c3">
         <td nowrap class="contentfont"><strong>
            <?=$lang[servicetype]?>
            </strong></td>
         <td class="contentfont"><? 
		echo "<SELECT name=\"default_type_service\" class=\"contentfont\">";
		echo "<option value=\"\" selected></option>";
	 	$getShippingOptions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_shipping_options");
	  	while ($shOpt=mysqli_fetch_array($getShippingOptions)) {
		  	echo "<OPTION value=\"".$shOpt['name']."\" ".(($shOpt['name']==$user ['default_type_service'])?"SELECTED":"").">".$shOpt['name']."</option>";
	  	}
	  	echo "</SELECT>"; ?></td>
      </tr>
      <tr>
         <td colspan="2" class="c1"><b>
            <?=strtoupper($lang[globalsetts])?>
            </b></td>
      </tr>
      <? if ($setts['enable_asq']=="Y") { ?>
      <tr class="c2">
         <td nowrap class="contentfont"><strong>
            <?=$lang[publicqs]?>
            </strong></td>
         <td class="contentfont"><input type="radio" name="default_public_questions" value="Y" checked>
            <?=$lang[yes]?>
            <input name="default_public_questions" type="radio" value="N" <? echo(($user['default_public_questions']=="N")?"checked":"");?>>
            <?=$lang[no]?>
      </tr>
      <? } ?>
      <tr class="c3">
         <td nowrap class="contentfont"><strong>
            <?=$lang[bidplacedemail]?>
            </strong></td>
         <td class="contentfont"><input type="radio" name="default_bid_placed_email" value="Y" checked>
            <?=$lang[yes]?>
            <input name="default_bid_placed_email" type="radio" value="N" <? echo(($user['default_bid_placed_email']=="N")?"checked":"");?>>
            <?=$lang[no]?>
      </tr>
      <tr>
         <td colspan="2" align="center" class="c4"><input name="updateprefilledok" type="submit" id="updateprefilledok" value="<?=$lang[updatebutton]?>"></td>
      </tr>
   </table>
</form>
<? include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
