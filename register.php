<? 
##session_start();
if (isset($_SESSION['memberid'])) {
	header("Location: index.php");
}

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

$bannedip=checkbanned($_SERVER['REMOTE_ADDR'],'IP');
if ($bannedip) {
	echo "<p align=center class=contentfont>$lang[reg_erripbanned]</p>";
} else {
	/*header5("$lang[registration]: $lang[enter_info]")*/;
	$phone="(".$dayphone1.") ".$dayphone2;
?>

<script language="javascript">
	function checkEmail() {
		if (document.registration_form.email_check.value==document.registration_form.email.value) document.registration_form.email_img.style.display="inline";
		else document.registration_form.email_img.style.display="none";
	}

	function checkPass() {
		if (document.registration_form.password.value==document.registration_form.password2.value) document.registration_form.pass_img.style.display="inline";
		else document.registration_form.pass_img.style.display="none";
	}
</script>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
   <tr>
      <td><? include ("formchecker.php"); ?>
      </td>
   </tr>
</table>
<?
	if ($action=="submit_user") {
		$isVoucher = checkSignupVoucher(trim($_POST['voucher_code']));

		$bannedemail=checkbanned($email,'EM');
		if ($bannedemail) {
			echo "<p align=center class=contentfont>$lang[reg_erremailbanned]</p>";
		} else {
			if ($setts['account_mode']==1) {
				if ($fee['is_signup_fee']=="Y"&&$fee['val_signup_fee']>0) {
					$payment_status="unconfirmed";
					$active=0;
					$mailactivated=1;
				} else {
					$payment_status="confirmed";
					$active=0;
					$regconfirm=1;
					$mailactivated=0;
				}
			} else if ($setts['account_mode']==2) {
				$active=0;
				$regconfirm=1;
				$mailactivated=0;
			}
			### override - if the registration approval is enabled, then set the regconfirm to 2, and the mailactivated to 0;
			if ($setts['enable_ra']=="Y") {
				$active=0;
				$regconfirm=2;
				$mailactivated=0;
			}
			### override - if coupon is valid then only request registration confirmation by email
			if ($isVoucher) {
				$payment_status="confirmed";
				$active=0;
				$regconfirm=1;
				$mailactivated=0;
			}

			### Setor: отменить проверку регистрации по E-mail
			$payment_status="unconfirmed"; // Не знаю что это означает
			$active=1;         // Какая-то глобальная активация (да)
			$regconfirm=0;     // Отправить ли письмо для активации (нет)
			$mailactivated=1;  // Был ли активирован аккаунт по емайл (да)
			###

			$bdate = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
			$currentTime = time();

			//if($_POST['usevat']==1){
         $vat_uid = $_POST['vat_uid_number'];
         //   $vat_rate = $_POST['vatrate'];
         //} else {
         //    $vat_uid = '';
         //    $vat_rate = 0;
         //}
         $companyname = ($_POST['accounttype']==1) ? "'".((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_POST['companyname']) : ((trigger_error("Error. Please try again, or contact website administration.", E_USER_ERROR)) ? "" : ""))."'" : 'NULL';

			if ($setts['vat_rate']>0&&$setts['auto_vat_exempt']=="Y") $vat_exempted="Y";
			else $vat_exempted="N";
			$valid_pin = checkPin ($_POST['pin_value'],$_POST['pin2']);
			
			


			if (trim($_POST['name'])!=""&&trim($_POST['email'])!=""&&trim($_POST['username'])!=""&&trim($_POST['password'])!=""&&$valid_pin) {
			
			
						 // Если емайл есть в приглашенных, и он не активирован то начисляем 1-ый бонус.
			 
		

       
       $sql = "SELECT id,refid,name,email FROM probid_users_friends_invites WHERE email='".$_POST['email']."' AND status_reg=0";
			 $result = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
        while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
          $invData[] = $row; 
        }
        

       if (intval($invData[0]["id"])>0) {
          // Change invite status [!]
          
          $update_invite = "UPDATE probid_users_friends_invites SET status_reg=1 WHERE id=".$invData[0]["id"]."";
          mysqli_query($GLOBALS["___mysqli_ston"], $update_invite);
          
          
          // Send Message To Referer

          $messageText = $lang[message_digital_agent_header].$invData[0]["name"]."(".$invData[0]["email"].") ".$lang[message_digital_agent_reg];
          $messageTitle = $lang[message_header_reg];
          
          $send_sql = "INSERT INTO probid_messages(senderid,receiverid,messagetype,message,messagetitle,opened,datentime) VALUES(28,".$invData[0]["refid"].",1,'".$messageText."','".$messageTitle."',0,'".strtotime(date("Y-m-d H:i:s"))."')";
          @mysqli_query($GLOBALS["___mysqli_ston"], $send_sql);   
          $id_msg = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
          
          // REFERER DATA
          
          $ref_mail = getSqlField("SELECT email FROM probid_users WHERE id=".$invData[0]["refid"]."","email");   
          $ref_name = getSqlField("SELECT name FROM probid_users WHERE id=".$invData[0]["refid"]."","name");   
          
          // SEND mail to referer
          
      	$headers = "MIME-Version: 1.0" . "\r\n";
        $headers.= "Content-type: text/plain; charset=utf-8" . "\r\n";
        // Additional headers
        $headers.= 'From: SBE.EE  <noreply@sbe.ee>' . "\r\n";
        $headers.= 'Cc: noreply@sbe.ee' . "\r\n";
        $headers.= 'Bcc: noreply@sbe.ee' . "\r\"";  
        
        $subject = $lang[message_header_reg];
        
        $mail = $lang[letter_digital_agent_header].$invData[0]["name"].$lang[letter_digital_agent_reg];
        $mail .="\n http://www.sbe.ee/membersarea.php?page=messages&msgid=".$id_msg."&msgsid=".md5($id_msg);
        mail($ref_mail, $subject, $mail, $headers); 
          
          
          // Add money to user [!]

         $currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$invData[0]["refid"]."'","balance");
         $updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET balance='".($currentBalance-0.20)."' WHERE id='".$invData[0]["refid"]."'");
          
         $addTransaction = mysqli_query($GLOBALS["___mysqli_ston"], 
         "INSERT INTO probid_users_transactions(userid,name,description,amount,balance,tdate,op) 
         VALUES(".$invData[0]["refid"].",
         'SBE',
         '".utf8_encode($lang[reg_fee])."',
         1,
         ".abs($currentBalance-0.2).",
         '".date("Y-m-d H:i:s")."',
         'in'
         ) ");
          
          
       }
      
          
      ## 07.06.2014 Simplified registration by MK
        

				
  		$insertUser =  mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_users 
						(name,email,companyname,country,username,password,
						active,payment_status,newsletter,regdate,mailactivated,mylang,mycurrency, 
						apply_vat_exempt,vat_uid_number,vat_exempted,referredby, payment_mode, allowed_credit) 
						VALUES	
						('".remSpecialChars($_POST['name'])."',
						'".$_POST['email']."',".$companyname.",
						'".remSpecialChars($_POST['country'])."',
						'".$_POST['username']."','".md5($_POST['password'])."',
				'".$active."','".$payment_status."','Y','".$currentTime."','".$mailactivated."','".get_current_lang()."','9',
				'".$_POST['apply_vat_exempt']."','".$vat_uid."','".$vat_exempted."',
				'".remSpecialChars($_POST['referral'])."','".$setts['init_acc_type']."','". $setts['max_credit'] ."')") or die(mysqli_error($GLOBALS["___mysqli_ston"]));
				$userid=mysql_insert_id();
				
		/* ('Ivan','wwoko@dkoko.clld',NULL,'Estonia','user334232','3fde6bb0541387e4ebdadf7c2ff31123','1',
		'unconfirmed','Y','1402147450','1','','','','','N','','1','50000.00')*/
			
			
				/*$insertUser = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_users
				(name,email,companyname,birthdate,address,city,state,country,zip,phone,username,
				password,active,payment_status,newsletter,regdate,mailactivated,mylang,mycurrency,
				apply_vat_exempt,vat_uid_number,vat_exempted,birthdate_year,referredby, payment_mode, allowed_credit) VALUES
				('".remSpecialChars($_POST['name'])."','".$_POST['email']."',".$companyname.",'".$bdate."','".remSpecialChars($_POST['address'])."',
				'".remSpecialChars($_POST['city'])."','".remSpecialChars($_POST['state'])."','".remSpecialChars($_POST['country'])."','".remSpecialChars($_POST['zip'])."',
				'".remSpecialChars($phone)."','".$_POST['username']."','".md5($_POST['password'])."',
				'".$active."','".$payment_status."','".$_POST['subsnl']."','".$currentTime."','".$mailactivated."','".$_POST['mylang']."',".$_POST["mycur"].",
				'".$_POST['apply_vat_exempt']."','".$vat_uid."','".$vat_exempted."','".$_POST['year']."',
				'".remSpecialChars($_POST['referral'])."','".$setts['init_acc_type']."','". $setts['max_credit'] ."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				$userid=((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);*/
			}
			$recipientId = $userid;
			if ($regconfirm==1) {
				include ("mails/register_confirm.php");
			} else if ($regconfirm==2) {
				## notify user
				include ("mails/register_approval.php");
				## notify admin
				include ("mails/register_approval_admin.php");
			} else {
				include ("mails/register.php");
			}

			## if user applied for VAT exempt
			if ($_POST['apply_vat_exempt']=="Y") include ("mails/register_apply_vat_exempt.php");
			if ((($fee['is_signup_fee']=="Y"&&$fee['val_signup_fee']>0&&$setts['account_mode']==1)||$setts['account_mode']==2)&&!$isVoucher) {
				### the function for the signup fee
				if ($fee['is_signup_fee']=="Y") $signup_fee = $fee['val_signup_fee'];
				else $signup_fee = 0;
				signupFee($signup_fee,$setts['currency'],$userid);
			} else {
				echo "<p class=contentfont>$lang[reg_emailsent]</p>";
			}

			#
			# Делаем авторизацию
			#

			$userDetails = getSqlRow("SELECT * FROM probid_users WHERE username='".$_POST['username']."'");

			if ($setts['account_mode_personal']==1)
			{
			  	$account_mode_local = ($userDetails['payment_mode']==1) ? 2 : 1;
			}
			else
			{
				$account_mode_local = $setts['account_mode'];
			}

			if ($userDetails['active']==1)
			{
				$_SESSION['membersarea']="Active";
				$_SESSION['memberid']=$userDetails['id'];
				$_SESSION['membername']=$userDetails['name'];
				$_SESSION['memberusern']=$userDetails['username'];
				//$_SESSION['sess_lang']=$userDetails['lang'];
			}
			else if ($userDetails['active']!=1&&$account_mode_local==2)
			{
				$_SESSION['memberid']=$userDetails['id'];
				$_SESSION['membername']=$userDetails['name'];
				$_SESSION['memberusern']=$userDetails['username'];
				//$_SESSION['sess_lang']=$userDetails['lang'];
				if ($_SESSION['accsusp']!=2)
				{
					$_SESSION['accsusp']=2;
				}
			}
			else if ($userDetails['active']!=1&&$account_mode_local!=2)
			{
				$_SESSION['accsusp']=1;
			}

			### Now we will activate the is_seller ariable, used by the Private Site feature
			### in case that the Private Site feature is disabled, all users are sellers
			### by default. Otherwise only enabled sellers can sell, other users can't

			if ($setts['private_site']=="Y")
			{
				$_SESSION['is_seller']=$userDetails['is_seller'];
			}
			else
			{
				$_SESSION['is_seller']="Y";
			}

			#
			# Предлагаем авторизацию Pangalink
			#

			include 'pangalink/authenticate.php';

			#
			#
			#
		}
	} else { ?>
<form action="register.php" method="post" name="registration_form">
   <? echo ($setts[account_mode]==2&&$setts['init_credit']>0) ? "
  <table border='0' cellpadding='6' cellspacing='0'  align='center'>
  <tr>
  <td class='regsignup'>
  ".$lang[signup_receive]." ".displayAmount($setts['init_credit'],$setts['currency'])." ".$lang[credit]."
  </td></tr></table><br>
  ":""; ?>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         <td width="100%"><table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg2">
                  <td width="1%" class="contentfont"><?=$imgarrowit;?></td>
                  <td width="30%" class="contentfont"><b>
                     <?=$lang[m_account_type]?>
                     </b></td>
                  <td width="69%" class="contentfont"><script type="text/javascript">
							function changestatusacc(state) {
                    		if(state==1) {
                        	document.getElementById('companyfield').innerHTML = '<input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($_POST['companyname']);?>" size="40" maxlength="50">';
                        } else {
                           document.getElementById('companyfield').innerHTML = '<input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($_POST['companyname']);?>" size="40" maxlength="50" disabled>';
                        }
                    	} </script>
                     <input name="accounttype" type="radio" onclick="changestatusacc(0);" class="contentfont" id="accounttype" value="0" <?=($_POST['accounttype']==0 || !isset($_POST['accounttype'])) ? 'checked' :'';?>>
                     <?=$lang[account_era]?><br>
                     <input name="accounttype" type="radio" onclick="changestatusacc(1);" class="contentfont" id="accounttype" value="1" <?=($_POST['accounttype']==1) ? 'checked' :'';?>>
                     <?=$lang[account_jur]?></td>
               </tr>
               <tr class="creg3">
                  <td class="contentfont"><?=$imgarrowit;?></td>
                  <a name=FullName></a>
                  <td class="contentfont"><b>
                     <?=$lang[fullname]?>
                     </b><br>
                  </td>
                  <td class="contentfont"><input name="name" type="text" class="contentfont" id="name" value="<?=$_POST['name'];?>" size="30" maxlength="50"></td>
               </tr>
               <tr class="creg2">
                  <td class="contentfont"><?=$imgarrowit;?></td>
                  <td class="contentfont"><b>
                     <?=$lang[m_company]?>
                     </b></td>
                  <td  class="contentfont" id="companyfield"><input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($_POST['companyname']);?>" size="30" maxlength="50" <?=($_POST['accounttype']==0)?'disabled':'';?>></td>
               </tr>
               <tr class="creg3">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
				  <td class="contentfont"><span class="contentfont"><b>
                     <?=$lang[country]?>
                     </b></span></td>
               
                  <td valign=top><select name="country" class="contentfont" id="country">
                        <?
								$getCountries=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries ORDER BY theorder ASC, name ASC");
								while ($country = mysqli_fetch_array($getCountries)) {
									echo "<option value=\"".$country['name']."\" ".(($country['name']==$_POST['country'])?"selected":"").">".$country['name']."</option>";
								} ?>
                     </select>
                  </td>
               </tr>

         

            </table>

                  </td>
               </tr>
               
   </table>
   <hr noShade size=1>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         
         <td class="creg4" width="100%">
		 <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <a name=UserId></a>
                  <td width="29%" class="contentfont"><b>
                     <?=$lang[userid]?>
                     </b></td>
                  <td class="contentfont"><input name="username" type="text" id="username" value="<?=$_POST['username'];?>" size="20" maxlength="30"></td>
				  <td width="350" class="reguser"><?=$lang[userid_text]?></td>
               </tr>
               <tr class="creg2">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <a name=Password></a>
                  <td  class="contentfont"><b>
                     <?=$lang[reg_pass]?>
                     </b></td>
                                    <td  class="contentfont"><input name="password" type="password" class="contentfont" id="password" size=20 maxlength="20"></td>
				  <td rowspan="2" width="350" class="reguser"><?=$lang[reg_passtext]?></td>
               </tr>
               <tr class="creg3">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <a name=PasswordReType></a>
                  <td  class="contentfont"><b>
                     <?=$lang[reg_pass2]?>
                     </b></td>
                  <td class="contentfont"><input name=password2 type=password  id="password2" size=20 maxlength="20" onkeyup="checkPass();"> <img src="images/check_img.gif" id="pass_img" align="absmiddle" style="display:none;" /></td>
				<!--td width="350" class="reguser"></td-->
               </tr>
            </table>
             <!--table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg2">
                  <td class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[interfacelanguage];?>
                     </b></td>
                  <td><?=myLang($_POST["mylang"])?></td>
               </tr>
               <tr class="creg2">
                  <td class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[currency];?>
                     </b></td>
                  <td><?=myCurrency($_POST["mycur"])?></td>
               </tr>
            </table-->
            <? if ($setts['vat_rate']>0&&$setts['auto_vat_exempt']=="N") { ?>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <td class="contentfont" nowrap><b>
                     <?=$lang[apply_vat_exempt]?>
                     </b></td>
                  <td class="contentfont" width="100%"><input name="apply_vat_exempt" type="radio" value="Y" <? echo (($_POST['apply_vat_exempt']=="Y")?"checked":"");?>>
                     <?=$lang[yes]?>
                     <input type="radio" name="apply_vat_exempt" value="N" <? echo (($_POST['apply_vat_exempt']=="N"||$_POST['apply_vat_exempt']=="")?"checked":"");?>>
                     <?=$lang[no]?></td>
               </tr>
               <tr class="reguser">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td><?=$lang[apply_vat_msg]?></td>
               </tr>
            </table>
            <? } ?>
            <br>
            <hr noShade size=1>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         
         <td class="creg4" width="100%"><table width="100%" border="0" cellpadding="3" cellspacing="1">
               <tr>
                  <td class="contentfont"><font class="redfont">
                     <?=$lang[reg_note]?>
                     </font></td>
               </tr>
            </table>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg2" >
                  <td width="11"  class="contentfont"><?=$imgwarning;?>
                  </td>
                  <td width="200"  class="contentfont"><b>
                     <?=$lang[email]?>
                     </b> </td>
                  <td class="contentfont"><input name="email" type="text" class="contentfont" id="email" value="<?=$_POST['email'];?>" size="40" maxlength="120">
                  </td>
               </tr>
               <tr class="creg3">
                  <td  class="contentfont"><?=$imgwarning;?></td>
                  <td nowrap class="contentfont"><strong>
                     <?=$lang[email_retype];?>
                     </strong></td>
                  <td class="contentfont"><input name="email_check" type="text" class="contentfont" id="email_check" value="" size="40" maxlength="120" onkeyup="checkEmail();"> <img src="images/check_img.gif" id="email_img" align="absmiddle" style="display:none;" /></td>
               </tr>

            </table></td>
      </tr>
   </table>
  <br>
                  <font  class="contentfont"><b>
                     <?=$lang[conf_pin]?>
                     </b>
                  <a name=UserId></a>
                  <?
							$_SESSION['pin_value']=md5(rand(2,99999999));
							$encodedPin = generatePin ($_SESSION['pin_value']);
							## create an image not a text for the pin
							$font  = 4;
							$width  = ImageFontWidth($font) * strlen($encodedPin);
							$height = ImageFontHeight($font);

							$im = @imagecreate ($width,$height);
							$background_color = imagecolorallocate ($im, 255, 255, 255); //white background
							$text_color = imagecolorallocate ($im, 0, 0,0);//black text
							imagestring ($im, $font, 0, 0,  $encodedPin, $text_color);
							touch("uplimg/tmp_pin_".$_SESSION['pin_value'].".jpg");
							imagejpeg($im,"uplimg/tmp_pin_".$_SESSION['pin_value'].".jpg");
							echo "<img src=\"uplimg/tmp_pin_".$_SESSION['pin_value'].".jpg\">";
							imagedestroy($im); ?>
                                          <input type="hidden" name="pin_value" value="<?=$_SESSION['pin_value'];?>">
					 <input name=pin2 type=text id="pin2" size=8>
            <?
				$nbSV = getSqlNumber("SELECT id FROM probid_vouchers WHERE voucher_type='signup'");
				if ($nbSV>0) { ?>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[voucher_code]?>
                     </b></td>
                  <td><input name="voucher_code" type="text" id="voucher_code" size=40></td>
               </tr>
               <tr class="reguser">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td><?=$lang[voucher_text]?>
                     </font></td>
               </tr>
            </table>
            <? }

				$getReferrals = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_referrals");
				$nbRef = mysqli_num_rows($getReferrals);
				if ($nbRef>0) { ?>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[referredby]?>
                     </b></td>
                  <td class="contentfont"><?
							echo "<SELECT name=\"referral\" class=\"contentfont\">";
							echo "<option value=\"\" selected>".$lang[ref_select]."</option>";
							while ($refs=mysqli_fetch_array($getReferrals)) {
								echo "<OPTION value=\"".$refs['name']."\" ".(($refs['name']==$_REQUEST['referral'])?"SELECTED":"").">".$refs['name']."</option>";
							}
							echo "</SELECT>"; ?>
                     (
                     <?=$lang[field_opt];?>
                     )
               </tr>
            </table>
            <? } ?></td>
      </tr>
   </table>
   <hr noShade size=1>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
          <td class="creg4" width="100%"><? if ($layout['d_acc_text']==1) { ?>
            <table width="100%" border="0" cellpadding="2" cellspacing="1">
               <tr>
                  <td class="contentfont"><b><font class="redfont">
                     <?=$lang[reg_termsnote]?>
                     </font></b></td>
               </tr>
            </table>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr>
                  <td class="contentfont">&nbsp;</td>
                  <td class="contentfont"><input type="checkbox" name="ack_terms" value="Y" <? echo ($_POST['ack_terms']=="Y") ? "checked":""; ?>>
                     <?=$lang[ack_terms];?></td>
               </tr>
            </table>
            <? } else { ?>
            <input type="hidden" name="ack_terms" value="Y">
            <? } ?>
            <hr noShade size=1>
            <input name="registerok" type=submit id="registerok" value="<?=$lang[reg_button]?>">
         </td>
      </tr>
   </table>
</form>
<? }
}
include ("themes/".$setts['default_theme']."/footer.php"); ?>
