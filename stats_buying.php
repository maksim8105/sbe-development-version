<?php
## v5.24 -> may. 08, 2006
session_start();
include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php");


if ($setts['h_counter']==1) {
   	 	$nbShops=getSqlNumber("SELECT id FROM probid_users WHERE store_active=1");
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		$nbFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1","Total");
		$nbRegisteredUsers24h=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-86400));
		$nbRegisteredUsers7d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-604800));
		$nbRegisteredUsers30d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-2592000));
		$nbRegisteredUsers1y=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-31536000));
		$nbopenauctions24h=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbopenauctions7d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbopenauctions30d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbopenauctions1y=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbtotalauctions=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_auctions","Total1");
		$nbauctionsAmount24h=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'","Total1");
		$nbauctionsAmount7d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'","Total1");
		$nbauctionsAmount30d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'","Total1");
		$nbauctionsAmount1y=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'","Total1");
		$nbauctionsFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions","Total1");
		$nbsolds24h=getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3");
		$nbsolds7d=getSqlField("SELECT SUM(amount) AS 'Total4' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-604800),"Total4");
		$nbsolds30d=getSqlField("SELECT SUM(amount) AS 'Total5' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total5");
		$nbsolds1y=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total6");
		$nbsoldsTotal=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners","Total6");
		$nbWantAds24h=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbWantAds7d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbWantAds30d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbWantAds1y=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbWantAdsTotal=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_wanted_ads","Total1");
		if ($nbsolds24h>0) $effect24h=($nbsolds24h/$nbauctionsAmount24h)*100; else $effect24h=0;
		if ($nbsolds7d>0) $effect7d=($nbsolds7d/$nbauctionsAmount7d)*100; else $effect7d=0;
		if ($nbsolds30d>0) $effect30d=($nbsolds30d/$nbauctionsAmount30d)*100; else $effect30d=0;
		if ($nbsolds1y>0) $effect1y=($nbsolds1y/$nbauctionsAmount1y)*100; else $effect1y=0;
		if ($nbsoldsTotal>0) $effecttotal=($nbsoldsTotal/$nbauctionsFullAmount)*100; else $effecttotal=0;
		
		
		
		/*header5("$lang[Site_status]");*/
	echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang[Site_status]."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";
    include "stat_menu.php";
    echo "Aнализ покупок позволяет видеть наиболее популярные товары на SBE.EE. Вы можете также просматривать в реальном времени, что именно ищут наши пользователи.<div align='center'><h3>На SBE.EE искали</h3></div>";	
    	echo "
			<table align='center'>
       		<tr class='c3'> 
          		<td align='right'><b></b></td>
          		<td align='right'><b>За последние 24 часа</b></td>
          		<td align='right'><b>За последнюю неделю</b></td>
          		<td align='right'><b>За последний месяц</b></td>
          		
          	</tr>";
         $Daysearchquery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT query FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-86400)."' AND query NOT LIKE 'empty' AND query NOT LIKE '' LIMIT 500");
         $DaySearchCount=getsqlfield("SELECT COUNT(query) AS 'DayQuery' FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-86400)."'","DayQuery");
         $Weekysearchquery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT query FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-604800)."' AND query NOT LIKE 'empty' AND query NOT LIKE '' LIMIT 500");
         $WeekySearchCount=getsqlfield("SELECT COUNT(query) AS 'WeekQuery' FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-604800)."'","WeekQuery");
         $Monthsearchquery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT query FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-2592000)."' AND query NOT LIKE 'empty' AND query NOT LIKE '' LIMIT 500");
         $MonthSearchCount=getsqlfield("SELECT COUNT(query) AS 'MonthQuery' FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-2592000)."'","MonthQuery");
         $ThreeMonthsearchquery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-7776000)."' AND query NOT LIKE 'empty' AND query NOT LIKE ''");
         $ThreeMonthSearchCount=getsqlfield("SELECT COUNT(query) AS 'ThreeMonthQuery' FROM probid_find WHERE date_time>'".date('Y-m-d H:i:s',time()-7776000)."'","ThreeMonthQuery");
         //Day
          	
         //Week
         
         //Month
         
         //3 Month
         
                   echo "
          <tr class='c2'> 
          		<td align='right'><b>Запрос</b></td>
          		<td align='right'><b>"; 
       $cnt_day=0;
          while ($Dayq=mysqli_fetch_array($Daysearchquery))
           {
            $day_array[$Dayq['query']]=getsqlfield("SELECT COUNT(query) AS 'Total2' FROM probid_find WHERE query='".$Dayq['query']."' AND date_time>'".date('Y-m-d H:i:s',time()-86400)."'","Total2");
            $cnt_day++;
           };
          arsort($day_array);
          foreach ($day_array as $key => $value)
			{
				echo $key."</b> (".$value.")<br><b>";}
       
          
          echo "</td>
            <td align='right'><b>";
          		$cnt_week=0;
           
           
           while ($Weekq=mysqli_fetch_array($Weekysearchquery))
          {
            $week_array[$Weekq['query']]=getsqlfield("SELECT COUNT(query) AS 'Total2' FROM probid_find WHERE query='".$Weekq['query']."' AND date_time>'".date('Y-m-d H:i:s',time()-604800)."'","Total2");
            $cnt_week++;
           };
          arsort($week_array);
          $cnt_add=0;
          foreach ($week_array as $key => $value)
			{
				$cnt_add++;
				echo $key."</b> (".$value.")<br><b>"; 
          		if ($cnt_add==10) break; }
          		
          	echo "</td>
          		<td align='right'><b>";
         $cnt_month=0;
         while ($Monthq=mysqli_fetch_array($Monthsearchquery))
         {
            $month_array[$Monthq['query']]=getsqlfield("SELECT COUNT(query) AS 'Total2' FROM probid_find WHERE query='".$Monthq['query']."' AND date_time>'".date('Y-m-d H:i:s',time()-2592000)."'","Total2");
            $cnt_month++;
           };
          arsort($month_array);
          $cnt_add=0;
          foreach ($month_array as $key => $value)
			{
				$cnt_add++;
				echo $key." </b> (".$value.")<br><b>"; 
         		if ($cnt_add==10) break; }
         echo "</b></td>
          		
          	</tr>
          <tr class='c2'> 
          		<td align='right'><b>Уникальных запросов</b></td>
          		<td align='right'><b>".$cnt_day."</b></td>
          		<td align='right'><b>".$cnt_week."</b></td>
          		<td align='right'><b>".$cnt_month."</b></td>
          		
          	</tr><tr class='c2'> 
          		<td align='right'><b>Всего поисковых запросов</b></td>
          		<td align='right'><b>".$DaySearchCount."</b></td>
          		<td align='right'><b>".$WeekySearchCount."</b></td>
          		<td align='right'><b>".$MonthSearchCount."</b></td>
          		
          	</tr></table>";
       		}
	
include ("themes/".$setts['default_theme']."/footer.php"); ?>