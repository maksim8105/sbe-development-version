<? 
## v5.22 -> oct. 07, 2005
include ("config/config.php"); 
include ("themes/".$setts['default_theme']."/title.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Additional Reputation Details</title>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<?
$fbtype = getSqlField("SELECT type FROM probid_feedbacks WHERE id='".$_REQUEST['fbid']."'","type");
		
$usertype = ($fbtype=="sale") ? "seller" : "buyer";

$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT f.boxid, f.boxname, f.boxtype, d.boxvalue FROM 
probid_custom_rep_data d, probid_custom_rep f WHERE d.feedbackid='".$_GET['fbid']."' AND f.active='1' 
AND f.usertype='".$usertype."' AND d.boxid = f.boxid  
ORDER BY f.fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
?>
<!-- Header Additional Custom Fields -->
<? header5(strtoupper($lang[addfields]));?>
<!-- End Header Additional Custom Fields -->
<table width="90%" border="0" cellspacing="1" cellpadding="4" align="center">
  <? while ($fields=mysqli_fetch_array($getFields)) { $toDisp = ""; ?>
  <tr class="<? echo (($count++)%2==0)?"c3":"c2"; ?>">
    <td align="right" width="50%"><strong>
      <?=$fields['boxname'];?>
      </strong></td>
    <td><?
	if ($fields['boxtype']=="checkbox") {
		$chkValues = explode(";",$fields['boxvalue']);
		for ($i=0; $i<count($chkValues); $i++) { 
			$chkRes = getSqlField("SELECT boxcaption FROM probid_custom_rep WHERE boxid='".$fields['boxid']."' AND boxvalue='".trim($chkValues[$i])."'","boxcaption");
			$toDisp .= ((trim($chkRes)!="n/a") ? $chkRes : "")."&nbsp; &nbsp; ";
		}	
	} else if ($fields['boxtype']=="radio") {
		$toDisp = getSqlField("SELECT boxcaption FROM probid_custom_rep WHERE boxid='".$fields['boxid']."' AND boxvalue='".$fields['boxvalue']."'","boxcaption")." ";
	} else if ($fields['boxtype']=="list") {
		$toDisp = getSqlField("SELECT boxcaption FROM probid_custom_rep WHERE boxid='".$fields['boxid']."' AND boxvalue='".$fields['boxvalue']."'","boxcaption")." ";
	} else {
		$toDisp = $fields['boxvalue'];
	}
	echo $toDisp;
	?>
    </td>
  </tr>
  <? } ?>
</table>
<br />
<table align="center"><tr><td class="contentfont">[ <a href="javascript:window.close(this);"><?=$lang[closewindow];?></a> ]</td></tr></table>
</body>
</html>
