<?
## v5.24 -> jun. 02, 2006
session_start();
include_once ("config/config.php");

if ($_GET['option']=="removebid"&&$_SESSION['membersarea']=="Active") {
	## only be able to do this is the user is the auction owner
	$isOwner = getSqlNumber("SELECT id FROM probid_auctions WHERE
	id='".$_GET['auctionid']."' AND ownerid='".$_SESSION['memberid']."'");
	if ($isOwner>0) {
		### we will implement the new bid retraction function
		retractBid ($_GET['auctionid'], $_GET['bidderid']);
	} else echo $lang[operation_impossible];
}

include ("themes/".$setts['default_theme']."/header.php");
header5("$lang[bidhistory]");
?>
<br> 
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
  <tr align="center" class="c1"> 
    <td><?=$lang[bidhistoryfor]?> 
      <?=$_GET['name'];?> </td> 
  </tr> 
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="contentfont"> 
  <tr class="c4"> 
    <td class="boldgrey"><?=$lang[username]?></td> 
    <td width="125" class="boldgrey" align="center"><?=$lang[bidamount]?></td> 
    <td align="center" class="boldgrey" width="150"><?=$lang[date]?></td> 
    <? if ($_GET['quantity']>1) { ?> 
    <td align="center" class="boldgrey" width="180"><?=$lang[quant_req]?> / <br><?=$lang[quant_awarded];?></td> 
    <? } ?> 
	<? 
	$auction = getSqlRow("SELECT private, ownerid, currency,closed, deleted FROM probid_auctions WHERE id='".$_GET['id']."'");
	if ($_SESSION['membersarea']=="Active"&&$auction['ownerid']==$_SESSION['memberid']&&$setts['enable_bid_retraction']=="Y") {
		echo "<td align=center width=200>$lang[remove_bids]</td>";
	} ?>
  </tr> 
  <tr class="c5"> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <? if ($_GET['quantity']>1) { ?> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <? } ?> 
	<? if ($_SESSION['membersarea']=="Active"&&$auction['ownerid']==$_SESSION['memberid']&&$setts['enable_bid_retraction']=="Y") {
		echo "<td></td>";
	} ?>
  </tr> 
  <?
	$isPrivate=$auction['private'];
	$currency=$auction['currency'];
	## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
	$getBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT h.* FROM probid_bids_history h, probid_bids b WHERE 
	h.auctionid='".$_GET['id']."' AND h.bidid=b.id ORDER BY b.valja ASC, h.id DESC");
	$cnt=0;
	$remQuant = getSqlField("SELECT quantity FROM probid_auctions WHERE id='".$_GET['id']."'","quantity");
	while ($bidHistory = mysqli_fetch_array($getBidHistory)) {
		$cnt++;
		$bidderId[$cnt]=getSqlField("SELECT id FROM probid_users WHERE username='".$bidHistory['biddername']."'","id"); ?> 
  <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>"> 
    <td>
	<?
	## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
	$isOut = getSqlField("SELECT valja FROM probid_bids WHERE id='".$bidHistory['bidid']."'","valja");
	## this line was inserted to keep backwards compatibility
	## because bid id linkage was only inserted since v5.00
	if ($bidHistory['bidid']==0) $isOut = 1;
	
	echo ($isOut==0) ? "<strong>":""; 
	if ($isPrivate=="Y") echo "$lang[bidderhidden]";
	else echo $bidHistory['biddername'].getFeedback($bidderId[$cnt]); 
	echo ($isOut==0) ? "</strong>":""; 
	## MySQL syntax fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
	echo (getSqlField("SELECT rpwinner FROM probid_bids WHERE id='".$bidHistory['bidid']."'","rpwinner")>0&&getSqlField("SELECT valja FROM probid_bids WHERE id='".$bidHistory['bidid']."'","valja")!=1) ? "[ <span class=greenfont>$lang[fixedpricewinner]</span> ]" : "";
	?> </td> 
    <td align="center"><? 
	echo ($isOut==0) ? "<strong>":""; 
	echo displayAmount($bidHistory['amount'],$currency);
	echo ($isOut==0) ? "</strong>":""; ?></td> 
    <td align="center" class="smallfont"><? 
	echo ($isOut==0) ? "<strong>":""; 
	echo displaydatetime($bidHistory['date'],$setts['date_format']); 
	echo ($isOut==0) ? "</strong>":""; ?></td> 
    <? if ($_GET['quantity']>1) { ?> 
    <td align="center" class="contentfont"> <? 
	echo ($isOut==0) ? "<strong>":""; 
	echo $bidHistory['quantity']." "; 
	if ($isOut==0) {
		if ($remQuant>=$bidHistory['quantity'])	echo "(".$bidHistory['quantity'].")";
		else echo "(".$remQuant.")";
		$remQuant -= $bidHistory['quantity'];	
	} else { echo "(0)"; } 
	echo ($isOut==0) ? "</strong>":""; ?></td> 
    <? } ?> 
	<? 
	if ($_SESSION['membersarea']=="Active"&&$auction['ownerid']==$_SESSION['memberid']&&$setts['enable_bid_retraction']=="Y") {
		if ($auction['closed']==0&&$auction['deleted']==0) {
			echo "<td align=center>[ <a href=\"bidhistory.php?option=removebid&id=".$_GET['id']."&auctionid=".$bidHistory['auctionid']."&bidderid=".$bidderId[$cnt]."&biddername=".$bidHistory['biddername']."&name=".$_GET['name']."&quantity=".$_GET['quantity']."\">".$lang[remove_bid]."</a> ]</td>";
		} else echo "<td align=center>$lang[removal_impossible]</td>"; 
	} ?>  </tr> 
  <? } ?> 
</table> 
<br> 
<div align="center" class="contentfont"><a href="<?=processLink('auctiondetails', array('id' => $_GET['id']));?>"> 
  <?=$lang[retdetailspage]?> 
  </a></div> 
<br> 
<? include ("themes/".$setts['default_theme']."/footer.php"); ?> 
