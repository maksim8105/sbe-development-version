<?
## v5.25 -> jun. 07, 2006

## ADD AUTHENTICATION - ALEKSEI

if ( !defined('INCLUDED') ) { die("Access Denied"); }

$userDetails = getSqlRow("SELECT `authenticated`, `isikukood`, `realname` FROM probid_users WHERE id='".$_SESSION['memberid']."'");
if ( 'Y' == strtoupper( $userDetails['authenticated'] ) )
{
	// I am authorized user

if ($_SESSION['membersarea']=="Active"&&$_SESSION['is_seller']=="Y") {

if (isset($_POST['rollbackok'])) {
	$actItemA = getSqlField("SELECT active FROM probid_auctions WHERE id='".$_POST['auctionid']."'","active");
	$rollBack = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
	hpfeat = '".$_POST['hpfeat']."',catfeat = '".$_POST['catfeat']."',
	bolditem = '".$_POST['bolditem']."',hlitem = '".$_POST['hlitem']."',active = '".$_POST['active']."',
	payment_status = '".$_POST['payment_status']."'
	WHERE id='".$_POST['auctionid']."' AND ownerid='".$_SESSION['memberid']."'") or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

	## if addpics_cnt = 0, remove additional images
	if ($_POST['addpics_cnt']==0) {
		$getAdditionalImages = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images 
		WHERE auctionid='".$_POST['auctionid']."'");
		while($adImage = mysqli_fetch_array($getAdditionalImages)) {
			deleteFile("../",$adImage['name']);
		}
		$remAdditionalImages = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_images WHERE auctionid='".$_POST['auctionid']."'");
	}
	
	##delete the invoices from the probid_invoices table
	if ($_POST['hlitem']="N"||$setts['hl_item']!=1) $delHlItem = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_invoices WHERE userid='".$_SESSION['memberid']."' AND 
	auctionid='".$_POST['id']."' AND feename='".$lang[highl_fee]."'");
	if ($_POST['bolditem']="N"||$setts['bold_item']!=1) $delBoldItem = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_invoices WHERE userid='".$_SESSION['memberid']."' AND 
	auctionid='".$_POST['id']."' AND feename='".$lang[bold_fee]."'");
	if ($_POST['hpfeat']="N"||$setts['hp_feat']!=1) $delHpFeat = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_invoices WHERE userid='".$_SESSION['memberid']."' AND 
	auctionid='".$_POST['id']."' AND feename='".$lang[home_page_fee]."'");
	if ($_POST['catfeat']="N"||$setts['cat_feat']!=1) $delCatFeat = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_invoices WHERE userid='".$_SESSION['memberid']."' AND 
	auctionid='".$_POST['id']."' AND feename='".$lang[cat_page_fee]."'");
	if ($_POST['respr']="N") $delRespr = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_invoices WHERE userid='".$_SESSION['memberid']."' AND 
	auctionid='".$_POST['id']."' AND feename='".$lang[res_price_fee]."'");
	
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
 	if ($setts['account_mode_personal']==1) {
    	$account_mode_local = ($tmp) ? 2 : 1;
  	} else $account_mode_local = $setts['account_mode'];
	
	if ($account_mode_local==2) {
		$rollBackTransaction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
			balance = balance - '".$_POST['payment_amount']."'
			WHERE id='".$_SESSION['memberid']."'") or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
// KEV's CHANGES - changed following line to grab extra required fields and edited code. 
	$actItemB = getSqlRow("SELECT active,category,addlcategory FROM probid_auctions WHERE id='".$_POST['auctionid']."'");
	if ($actItemA==0&&$actItemB['active']==1) {
		addcatcount($actItemB['category'],$_POST['auctionid']);
		addcatcount($actItemB['addlcategory'],$_POST['auctionid']);
	}
	if ($actItemA==1&&$actItemB['active']==0) {
		delcatcount($actItemB['category'],$_POST['auctionid']);
		delcatcount($actItemB['addlcategory'],$_POST['auctionid']);	
	}
	echo "<p align=\"center\" class=\"contentfont\">$lang[trans_rollback]</p>";		
}

if (isset($_POST['processauctions'])) {
	if (count($_POST['relist'])>0) {
		## added in v5.22 -> if more than 3 auctions are relisted, only a global email is sent
		$nbRelistedAuctions = count($_POST['relist']);
		$mailSent = FALSE;
		for ($relist_counter=0;$relist_counter<count($_POST['relist']);$relist_counter++) {
			$relistId = $_POST['relist'][$relist_counter];
			## if the item is a store item, first check if more store items can be listed.
			$itemListedIn = getSqlField("SELECT listin FROM probid_auctions WHERE id='".$relistId."' AND ownerid='".$_SESSION['memberid']."'","listin");

			$userStore = getSqlRow("SELECT aboutpage_type, store_active, store_account_type FROM probid_users WHERE id='".$_SESSION['memberid']."'");
			$nbStoreItems = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."' AND 
			active='1' AND closed='0' AND deleted!='1' AND listin!='auction'");
			$storeActive = FALSE;
			if ($userStore['aboutpage_type']==2&&$userStore['store_active']==1) {
				if ($userStore['store_account_type']==0) $storeActive = TRUE;
				else {
					$maxStoreItems = getSqlField("SELECT store_nb_items FROM probid_fees_tiers WHERE id='".$userStore['store_account_type']."'","store_nb_items");
					if ($maxStoreItems!="n/a"&&$maxStoreItems!=""&&$maxStoreItems>$nbStoreItems) $storeActive=TRUE;
				}
			}
			
			$canList = FALSE;
			if ($itemListedIn!="store") {
				$canList = TRUE;
			} else {
				if ($storeActive) $canList = TRUE;
				else $canList = FALSE;
			}
			
			if ($canList) {
			
				$today = date( "Y-m-d H:i:s", time() );
				$closingdate = closingdate($today,$_POST['duration'][$relistId]);
				$relistAuction[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auctions 
				(itemname, description, picpath, quantity, auctiontype, bidstart, rp, 
				rpvalue, bn, bnvalue, bi, bivalue, duration, country, zip, sc, scint, pm, 
				category, active, payment_status, startdate, enddate, closed, keywords, 
				nrbids, maxbid, clicks, ownerid, hpfeat, catfeat, bolditem, hlitem, private, 
				currency, swapped, postage_costs, insurance, type_service, isswap, acceptdirectpayment, 
				addlcategory, deleted, listin, shipping_details, bank_details, accept_payment_systems, apply_vat, 
				auto_relist, auto_relist_bids, videofile_path, offer_range_min, offer_range_max, auto_relist_nb) SELECT
				itemname, description, picpath, quantity, auctiontype, bidstart, rp, 
				rpvalue, bn, bnvalue, bi, bivalue, duration, country, zip, sc, scint, pm, 
				category, active, payment_status, startdate, enddate, closed, keywords, 
				nrbids, maxbid, clicks, ownerid, hpfeat, catfeat, bolditem, hlitem, private, 
				currency, swapped, postage_costs, insurance, type_service, isswap, acceptdirectpayment, 
				addlcategory, deleted, listin, shipping_details, bank_details, accept_payment_systems, apply_vat, 
				auto_relist, auto_relist_bids, videofile_path, offer_range_min, offer_range_max, auto_relist_nb 
				FROM probid_auctions b WHERE b.id='".$relistId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
				
				$newId = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
				
				$getCustomFields[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fields_data WHERE auctionid='".$relistId."'");
				while ($relistFld = mysqli_fetch_array($getCustomFields[$relist_counter])) {
					$relistCustomFields[$relist_counter][$custom_fld++] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fields_data 
					(auctionid,ownerid,boxid,boxvalue,active) VALUES 
					('".$newId."','".$relistFld['ownerid']."','".$relistFld['boxid']."','".$relistFld['boxvalue']."','".$relistFld['active']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));			
				}
	
				$getImages[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$relistId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				$addCnt=0;
				while ($relistAddImages=mysqli_fetch_array($getImages[$relist_counter])) {
					$fileExtension = getFileExtension($relistAddImages['name']);
					$addImageName = "a".$newId."_addpic".$addCnt.".".$fileExtension;				
					$isUpload = uploadFile($relistAddImages['name'],$addImageName,"uplimg/");
					if ($isUpload) {
						$addImage[$addCnt][$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_images
						(name, auctionid) VALUES ('uplimg/".$addImageName."','".$newId."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						$addCnt++;
					}
				}
				$mainPicRelist = getSqlField("SELECT picpath FROM probid_auctions WHERE id='".$newId."'","picpath");
				if ($mainPicRelist != "") {
					$fileExtension = getFileExtension($mainPicRelist);
					$mainPicName = "a".$newId."_mainpic.".$fileExtension;
					$isUpload = uploadFile($mainPicRelist,$mainPicName,"uplimg/");
					if ($isUpload) {
						$updateAuctionPic[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
						picpath='uplimg/".$mainPicName."' WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					}
				}
				## video relist
				$videoRelist = getSqlField("SELECT videofile_path FROM probid_auctions WHERE id='".$newId."'","videofile_path");
				if (!empty($videoRelist)) {
					$fileExtension = getFileExtension($videoRelist);
					$videoName = "a".$newId."_video.".$fileExtension;
					$isUpload = uploadFile($videoRelist,$videoName,"uplimg/");
					if ($isUpload) {
						$updateAuctionVideo[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
						videofile_path='uplimg/".$videoName."' WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					}
				}
	
				### check for additional images
				
				$nbAddImages = getSqlNumber("SELECT * FROM probid_auction_images WHERE auctionid='".$newId."'");
				
				$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$newId."'");

				$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auctionDetails['id']."'","category");
		
				$category_id = getMainCat($itemCategory);
			
				$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
				$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
				
				$topay="";
				if ($fee['is_setup_fee']=="Y") $topay.="Auction Setup Fee; ";
				if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$nbAddImages>0) $topay.="Image Gallery Fee; ";
				if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$auctionDetails['hlitem']=="Y"&&$setts['hl_item']==1) $topay.="Highlighted Item Fee; ";
				if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$auctionDetails['bolditem']=="Y"&&$setts['bold_item']==1) $topay.="Bold Item Fee; ";
				if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$auctionDetails['hpfeat']=="Y"&&$setts['hp_feat']==1) $topay.="Home Page Featured Item Fee; ";
				if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$auctionDetails['catfeat']=="Y"&&$setts['cat_feat']==1) $topay.="Category Page Featured Item Fee; ";
				if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$auctionDetails['rp']=="Y"&&$auctionDetails['rpvalue']>0) $topay.="Reserve Price Fee; ";
				if ($fee['second_cat_fee']>0&&$auctionDetails['addlcategory']>0) { 
					$topay.="Second Category Fee; ";
					$issecondcat = "Y";
				} else $issecondcat = "N";
				if ($fee['bin_fee']>0&&$auctionDetails['bn']=="Y"&&$auctionDetails['bnvalue']>0) $topay.="Buy It Now Fee; ";
				if ($fee['videofile_fee']>0&&!empty($auctionDetails['videofile_path'])) { 
					$topay.="Movie Upload Fee; ";
					$ismoviefee = "Y";
				}
	
				$hlitem_chg = ($setts['hl_item']==1) ? $auctionDetails['hlitem'] : "N";
				$bolditem_chg = ($setts['bold_item']==1) ? $auctionDetails['bolditem'] : "N";
				$hpfeat_chg = ($setts['hp_feat']==1) ? $auctionDetails['hpfeat'] : "N";
				$catfeat_chg = ($setts['cat_feat']==1) ? $auctionDetails['catfeat'] : "N";
				
				## calculate quantity
				$qu_a = getSqlField("SELECT quantity FROM probid_auctions WHERE id='".$newId."'","quantity");
				$qu_b = getSqlField("SELECT sum(quant_offered) AS quant_o FROM probid_winners WHERE auctionid='".$relistId."'", "quant_o");
				$qu_total = $qu_a + $qu_b;
				$qu_total = ($auctionDetails['auctiontype']=="dutch") ? $qu_total : 1;
		
				## if item is listed in both, and storeactive=false, then only list in auction
				$listIn = ($storeActive) ? $itemListedIn : "auction";
				$relistAuctionDets_1[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
				startdate='".$timeNowMysql."',enddate='".$closingdate."',duration='".$_POST['duration'][$relistId]."',
				closed=0,nrbids=0,maxbid=0, quantity='".$qu_total."', 
				hlitem='".$hlitem_chg."', bolditem='".$bolditem_chg."', hpfeat='".$hpfeat_chg."', catfeat='".$catfeat_chg."', 
				listin='".$listIn."', clicks=0 
				WHERE id='".$newId."' AND ownerid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				//echo "You have to pay the following fees: $topay";
				
				$isFee = array('hpfeat' => $hpfeat_chg, 'catfeat' => $catfeat_chg, 'bold' => $bolditem_chg, 'hl' => $hlitem_chg, 
				'rp' => $auctionDetails['rp'], 'pic_count' => $nbAddImages, 'secondcat' => $issecondcat, 'bin' => $auctionDetails['bn'], 
				'videofee' => $ismoviefee);
				
				if ($topay!=""&&!freeFees($auctionDetails['ownerid'])&&$fee['relist_fee_reduction']<100) {
					$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
					if ($setts['account_mode_personal']==1) {
						$account_mode_local = ($tmp) ? 2 : 1;
					} else $account_mode_local = $setts['account_mode'];
		
					if ($account_mode_local==2) {
						setupFee($auctionDetails['bidstart'],$auctionDetails['currency'],$auctionDetails['id'],$isFee, FALSE, FALSE, "", 0, TRUE, TRUE);
						$payment_status="confirmed";
						$active=1;
					} else {
						$payment_status="unconfirmed";
						$active=0;
					}
				} else {
					$payment_status="confirmed";
					$active=1;
	// KEV START CHANGES - changed the following 2 lines to point to the new function 				
					addcatcount($auctionDetails['category'],$auctionDetails['id']);
					addcatcount($auctionDetails['addlcategory'],$auctionDetails['id']);
				}
				$relistAuctionDets_2[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
				active='".$active."',payment_status='".$payment_status."' 
				WHERE id='".$newId."' AND ownerid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				
				### check in auction watch table for keywords matching ours and send email to the user interested.
				$keywords = getSqlField("SELECT keywords FROM probid_auctions WHERE id='".$newId."'","keywords");
				
				$getKeywords = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_keywords_watch") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				while ($keywordsW=mysqli_fetch_array($getKeywords)) {
					if (@eregi($keywordsW['keyword'],$keywords)) {
						$userId = $keywordsW['bidderid'];
						$auctionId = $newId;
						$keyword = $keywordsW['keyword'];
						include ("mails/keywordmatch.php");
					}
				}
	
				$userId = $_SESSION['memberid'];
				$auctionId = $newId;
			/*	if ($nbRelistedAuctions>3) {
					if (!$mailSent) include ("mails/confirmtosellermultipleauctions.php");
					$mailSent = TRUE;
				} else {
					### send confirmation email to the seller
					include ("mails/confirmtoseller.php");
				} */
				deleteAuction($relistId);

			} else { echo "<p align=center>$lang[no_more_items_relist_store]</p>"; } 
		}
	}
// KEV START CHANGES - should not need this!!! 
// countCategories();
	if (count($_POST['delete'])>0) {
		for ($i=0;$i<count($_POST['delete']);$i++) {
			deleteAuction($_POST['delete'][$i]);
		}
	}
}		

if ($_REQUEST['option']=="delete"||isset($_POST['confirmok'])) {
	$numberOfBids = getSqlField("SELECT nrbids FROM probid_auctions WHERE id='".$_REQUEST['id']."'","nrbids");
	if ($numberOfBids==0&&$numberOfBids!="n/a"&&underTime($_REQUEST['id'])) {
		deleteAuction($_REQUEST['id']);
	}
}

$today=date("Y-m-d H:i:s");
//$timeNowOffset = convertdate($today,"Y-m-d H:i:s");
$timeNowOffset = $today;

$limit = 20;
include("my.php");
if (!isset($data["auc_type"])) $data["auc_type"] = 0; // DEFAULT AUCTION
if (!isset($data["auc_bid"])) $data["auc_bid"] = "-"; 
if (!isset($data["auc_period"])) $data["auc_period"] = "-"; 




if (isset($data["act"])) {

    switch ($data["auc_type"]) {
    
      case "0":
      // Opened
      $where_type = "closed=0 AND deleted!=1 AND ownerid=".$_SESSION['memberid']."";
      $field = "probid_auctions.startdate";
      break;
      case "1":
      // Closed
      $where_type = "closed=1 AND deleted!=1 AND ownerid=".$_SESSION['memberid']."";
      $field = "probid_auctions.enddate";
      break;
      
      break;
    }


    if ($field) {

    switch ($data["auc_period"]) {
    
      case "-":
      $where_period = "";
      break;
      case "0":
      // 1 Week
      
      $where_period = "AND DATE_SUB(".$field.", INTERVAL 1 WEEK)";
      break;
      case "1":
      // 1 Month
      $where_period = "AND DATE_SUB(".$field.", INTERVAL 1 MONTH)";
      break;
      case "2":
      // 3
      $where_period = "AND DATE_SUB(".$field.", INTERVAL 3 MONTH)";
      break;
      case "3":
      //6
      $where_period = "AND DATE_SUB(".$field.", INTERVAL 6 MONTH)";
      
      break;
    }    
  }  
    
    switch ($data["auc_bid"]) {
    
      case "-":
      $where_bid = "";
      break;
      case "0":
      // GOOD
      $where_bid = "AND probid_bids.bidamount > probid_auctions.rp OR probid_auctions.rp = 0";
      $add_table.= ",probid_bids";
      break;
      case "1":
      // < START BID
      $where_bid = "AND probid_bids.bidamount < probid_auctions.rp AND probid_auctions.nrbids>0 ";
      $add_table.= ",probid_bids";
      break;
      case "2":
      // NO BIDS
      $where_bid = "AND probid_auctions.nrbids=0";
      break;
    }    

        
}




switch ($data["auc_type"]) {

    case "0":
    
    
    
    // OPENED
    
    $orderField = "startdate";
    $orderType = "DESC";
    
    if ($_GET['srctype']=="openauct") {
    	if ($_REQUEST['orderField']=="") $orderField = "enddate";
    	else $orderField=$_REQUEST['orderField'];
    	if ($_REQUEST['orderType']=="") {
    		$orderType = "DESC";
    		$newOrder="ASC";
    	} else {
    		$orderType=$_REQUEST['orderType'];
    		$newOrder=($orderType=="ASC")?"DESC":"ASC";
    	}
    	$start = $_GET['start'];
    } else {
    	$start = 0;
    }
    
    $additionalVars = "&page=selling&srctype=openauct";
    
    $nbOpenAuctions = getSqlNumber ("SELECT id FROM probid_auctions".$add_table." WHERE 
    closed=0 ".$where_period." AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' ".$where_bid."");  
    
    /* */
    
    //echo "SELECT id FROM probid_auctions".$add_table." WHERE 
    //closed=0 ".$where_period." AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' ".$where_bid;
    
    
    
    $getOpenAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions".$add_table." WHERE closed=0  ".$where_period." AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' ".$where_bid." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit);
    
    break;
    
    
    case "1":

    // CLOSED
    
    
    $orderField = "startdate";
    $orderType = "DESC";
    
    if ($_GET['srctype']=="closedauct") {
    	if ($_REQUEST['orderField']=="") $orderField = "enddate";
    	else $orderField=$_REQUEST['orderField'];
    	if ($_REQUEST['orderType']=="") {
    		$orderType = "DESC";
    		$newOrder="ASC";
    	} else {
    		$orderType=$_REQUEST['orderType'];
    		$newOrder=($orderType=="ASC")?"DESC":"ASC";
    	}
    	$start = $_GET['start'];
    } else {
    	$start = 0;
    }
    
    $additionalVars = "&page=selling&srctype=closedauct";
    
    
    
    $nbClosedAuctions = getSqlNumber ("SELECT id FROM probid_auctions".$add_table." WHERE 
    closed=1 ".$where_period." AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' AND enddate<'".$timeNowOffset."' ".$where_bid.""); 
    
    /**/
    //echo "SELECT id FROM probid_auctions".$add_table." WHERE 
    //closed=1 ".$where_period." AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' AND enddate<'".$timeNowOffset."' ".$where_bid."";
    
    $getClosedAuctions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions".$add_table." WHERE closed=1 ".$where_period." AND deleted!=1 AND ownerid='".$_SESSION['memberid']."'  AND enddate<'".$timeNowOffset."'  ".$where_bid." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit);
    
    break;
    
    case "2":

    // WAITING
    
    
    ## modifications for qid 172, #4
    $orderField = "enddate";
    $orderType = "DESC";
    
    ## modifications for qid 172, #1
    if ($_GET['srctype']=="unstartedauct") {
    	if ($_REQUEST['orderField']=="") $orderField = "enddate";
    	else $orderField=$_REQUEST['orderField'];
    	if ($_REQUEST['orderType']=="") {
    		$orderType = "DESC";
    		$newOrder="ASC";
    	} else {
    		$orderType=$_REQUEST['orderType'];
    		$newOrder=($orderType=="ASC")?"DESC":"ASC";
    	}
    	$start = $_GET['start'];
    } else {
    	$start = 0;
    }
    
    $additionalVars = "&page=selling&srctype=unstartedauct";
    
    $nbUnstartedAuctions=getSqlNumber ("SELECT id FROM probid_auctions WHERE 
    closed=1 AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' AND startdate>'".$timeNowOffset."'"); 
    $getUnstartedAuctions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions WHERE closed=1 AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' AND startdate>'".$timeNowOffset."' ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit);
    
    break;


}





?>
<script language="Javascript">
<!--
function checkAll(field, array_len, check) {
	if (array_len == 1) { 
		field.checked = check;
	} else {
		for (i = 0; i < array_len; i++)
			field[i].checked = check ;
	}
}
-->
</script>

<?


function byAucType($selected){

global $lang;

  $arrTypes = array(
              "0"=>$lang[opened_only],
              "1"=>$lang[closed_only],
              "2"=>$lang[unstartedauctions],
              "3"=>$lang[finishedauctions]
  );

    $selectbox="<select name='auc_type' style='width:200px;'>";
  
    foreach ($arrTypes as $row=>$key) {
    
      $selectbox.="<option ".($row==$selected?"selected":'')." value=".$row.">".$key."";
    
    }
    
    $selectbox.="</select>";
  
  return $selectbox;
}


function byAucPeriod($selected){

global $lang;

  $arrPeriod = array(
              "0"=>$lang[lastweek],
              "1"=>$lang[lastmonth],
              "2"=>$lang[lastthreemonth],
              "3"=>$lang[lastsixmonth]
  );
  


    $selectbox="<select name='auc_period' style='width:150px;'>";
    if ($selected=='-') {
        $no_sel = 1;
    }
    
    $selectbox.="<option value='-' ".($no_sel?'selected':'').">".$lang[all];

  
    foreach ($arrPeriod as $row=>$key) {
    
      $selectbox.="<option ".($row==$selected && !$no_sel?'selected':'')." value=".$row.">".$key."";

    }
    
    $selectbox.="</select>";
  
  return $selectbox;
}

function byAucBid($selected){

global $lang;

  $arrBid = array(
              "0"=>$lang[successful],
              "1"=>$lang[reservenotmet],
              "2"=>$lang[notsuccessful]
  );
 
     if ($selected=='-') {
        $no_sel = 1;
    } 
    $selectbox="<select name='auc_bid' style='width:150px;'>";
    $selectbox.="<option value='-' ".($no_sel?'selected':'').">".$lang[all];

  
    foreach ($arrBid as $row=>$key) {
    
      
    
      $selectbox.="<option ".($row==intval($selected) && !$no_sel?'selected':'')." value=".$row.">".$key."";
    
    }
    
    $selectbox.="</select>";
  
  return $selectbox;
}



?>

<? $nbWinners = getSqlNumber("SELECT w.id FROM probid_winners w, probid_auctions a WHERE w.sellerid='".$_SESSION['memberid']."' AND a.id=w.auctionid AND w.s_deleted!=1");?>

<table width="100%" cellspacing="1" cellpadding="4" class="selling">
   <tr>
      
      <td width="33%" class="sellingtd" align="center"><?=$lang[open];?> </td>
      <td width="33%" class="sellingtd" align="center"><?=$lang[notstarted];?></td>
      <td class="sellingtd" align="center"><?=$lang[closed];?></td>
   </tr>
   
   <tr>
      
      <td class="verybignumber" align="center">
         <?=getSqlNumber("SELECT id FROM probid_auctions WHERE closed=0 AND deleted!=1 AND ownerid='".$_SESSION['memberid']."'"); ?>
		</td>
      <td class="verybignumber" align="center">
         <?=getSqlNumber("SELECT id FROM probid_auctions WHERE closed=1 AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' AND startdate>'".$timeNowOffset."'"); ?></td>
      <td class="verybignumber" align="center">
         <?=getSqlNumber("SELECT id FROM probid_auctions WHERE closed=1 AND deleted!=1 AND ownerid='".$_SESSION['memberid']."' AND enddate<'".$timeNowOffset."'"); ?> </td>
   </tr>
</table>
<? /*
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
   <tr>
      <td class="contentfont"><a href="prefilledfields.php">
         <?=$lang[prefilledfields]?>
         </a> |
         <? if ($setts['enable_asq']=="Y") { ?>
         
         <? } ?>
         <a href="suggestcategory.php">
         <?=$lang[suggestcategory]?>
         </a> | <a href="abusereport.php">
         <?=$lang[abuse]?>
         </a> 
		 | <a href="m_blockusers.php">
         <?=$lang[blockusers]?></td>
   </tr>
</table>

*/?>
<form name="formFilter" method="post" action="membersarea.php">
<table class="border" width="100%" border="0" cellspacing="0" cellpadding="4" style="background-color:#F0F6FA;">
<tr>
    <td><?=byAucType($data["auc_type"])?></td>
    <td width="150"><? echo $lang[period]?></td><td><?=byAucPeriod($data["auc_period"])?></td>
    <td width="150"><? echo $lang[bids]?></td><td><?=byAucBid($data["auc_bid"])?></td>
    <td width="150"></td><td><input type="button" value=<?=$lang[sendquery]?> onclick="formFilter.submit();"></td>
    <td>

   <input type="hidden" value="selling" name="page">
   <input type="hidden" value="filter" name="act"></td>
</tr>

</table>
</form>
<?

  if ($data["auc_type"] == 0) {

?>

<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&nbsp;
                  <?=$lang[openedauctions]?></b> (<?=$nbOpenAuctions;?>) </td>
            </tr>
         </table>
         <? if ($nbOpenAuctions>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr>
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_id]?>
                  </a></td>
               <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_name]?>
                  </a></td>
               <td width="80" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=startdate&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[started]?>
                  </a></td>
               <td width="80" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=enddate&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[ends]?>
                  </a></td>
               <td width="50" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=nrbids&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[num_bids]?>
                  </a></td>
               <td width="50" align="center" class="boldgrey"><?=$lang[auto_relist];?></td>
               <td width="100" align="center" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=maxbid&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[max_bid]?>
                  </a></td>
               <td width="140" align="center" class="boldgrey"><?=$lang[options]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <? 
				while ($openAuctions = mysqli_fetch_array($getOpenAuctions)) {
					if ($setts['date_format_type']=="USA") $format="m/d/Y";
					else $format="d/m/Y"; ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $openAuctions['itemname'], 'id' => $openAuctions['id']));?>"><?=$openAuctions['id'];?>
                  </a></td>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $openAuctions['itemname'], 'id' => $openAuctions['id']));?>">
                  <?=$openAuctions['itemname'];?>
                  </a>
                  <?=auctionListedIn($openAuctions['id']); ?>
                  <?
						$currentTime = time();
						$isOffers = getSqlNumber("SELECT id FROM probid_auction_offers WHERE auctionid='".$openAuctions['id']."' AND 
						sellerid='".$_SESSION['memberid']."' AND accepted='no'"); 
						if ($isOffers&&$setts['buyout_process']==1) echo '[ <a href="auctionoffers.php?auctionid='.$openAuctions['id'].'">View offers for this auction</a> ]';
						?></td>
               <td align="center" class="smallfont"><? echo format_date($openAuctions['startdate'],2);?></td>
               <td align="center" class="smallfont"><? echo format_date($openAuctions['enddate'],2);?></td>
               <td align="center" class="smallfont"><? echo (($openAuctions['nrbids']>0)?$openAuctions['nrbids']:"-");?></td>
               <td align="center" class="smallfont"><? echo ($openAuctions['auto_relist']=="Y") ? $lang[yes].' ('.$openAuctions['auto_relist_nb'].')' : $lang[no]; ?></td>
               <td align="center" class="smallfont"><? echo (($openAuctions['nrbids']>0)?displayAmount($openAuctions['maxbid'],$openAuctions['currency']):"-");
						$getSwaps = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_swaps WHERE 
						sellerid='".$_SESSION['memberid']."' AND auctionid='".$openAuctions['id']."'");
						while ($swap = mysqli_fetch_array($getSwaps)) {
							echo "<br><a href=\"makeswap.php?id=".$swap['id']."\">".$lang[swapofferfrom_member]." ".getSqlField("SELECT * FROM probid_users WHERE id='".$swap['buyerid']."'","username")."</a>";
						} ?> </td>
               <td align="center" class="smallfont"><a href="sellitem.php?option=sellsimilar&similarid=<?=$openAuctions['id'];?>">
                  <?=$lang[sellsimilar]?>
                  </a><br>
                  <? 
						if ($openAuctions['payment_status']=="unconfirmed"&&$openAuctions['active']==0) {
							$nbAddImages = getSqlNumber("SELECT * FROM probid_auction_images WHERE auctionid='".$openAuctions['id']."'");
							echo "<a href=\"payfee.php?option=pay_auction_setup&auctionid=".$openAuctions['id']."&startprice=".$openAuctions['bidstart']."&nbimages=".$nbAddImages."\">".$lang[paysetupfee]."</a>";
						} else if ($openAuctions['nrbids']==0&&$openAuctions['active']==1) {
								// echo "<a href=\"editauction.php?id=".$openAuctions['id']."\">".$lang[edit]."</a><br> 	\n";
								if (underTime($openAuctions['id'])) { 
								echo "<a href=\"retractconfirm.php?page=selling&option=delete&id=".$openAuctions['id']."\">".$lang[remove]."</a>"; 
							}	
						} else if ($openAuctions['nrbids']>0&&$openAuctions['active']==1) { 
							echo "<a href=\"edit.description.php?id=".$openAuctions['id']."\">".$lang[edit_desc]."</a><br> 	\n";
						} 
						if ($openAuctions['active']==0&&$openAuctions['approval']==0&&$openAuctions['payment_status']=="confirmed") {
							echo "<br>".$lang[avaitingapproval];
						}
						?>
               </td>
            </tr>
            <? } ?>
         </table>
         <?
			## display pagination
			
			$additionalVars.="&auc_type=".$data["auc_type"]."&auc_period=".$data["auc_period"]."&auc_bid=".$data["auc_bid"]."";
			
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\" >\n";
			paginate($start,$limit,$nbOpenAuctions,"membersarea.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); 
			echo "</td></tr></table>";
			?>
         <? } else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>

<? 

} elseif ($data["auc_type"] == 1) {


?>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&nbsp;
                  <?=$lang[closedauctions]?>
                  :</b> (
                  <?=$nbClosedAuctions;?>) </td>
            </tr>
         </table>
         <? if ($nbClosedAuctions>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <form action="membersarea.php" method="post" name="closed_auctions">
               <input type="hidden" name="page" value="selling">
               <input type="hidden" name="auc_type" value="1">
               <input type="hidden" name="auc_bid" value="<?=$data["auc_bid"]?>">
               <input type="hidden" name="auc_period" value="<?=$data["auc_period"]?>">
               <tr>
                  <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=id&orderType=<?=$newOrder.$additionalVars;?>">
                     <?=$lang[auc_id]?>
                     </a></td>
                  <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>">
                     <?=$lang[auc_name]?>
                     </a></td>
                  <td class="boldgrey" width="70"><a href="membersarea.php?start=<?=$start;?>&orderField=startdate&orderType=<?=$newOrder.$additionalVars;?>">
                     <?=$lang[started]?>
                     </a></td>
                  <td class="boldgrey" width="70"><a href="membersarea.php?start=<?=$start;?>&orderField=enddate&orderType=<?=$newOrder.$additionalVars;?>">
                     <?=$lang[ended]?>
                     </a></td>
                  <td class="boldgrey" width="50" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=nrbids&orderType=<?=$newOrder.$additionalVars;?>">
                     <?=$lang[num_bids]?>
                     </a></td>
                  <td class="boldgrey" width="70" align="center" nowrap><a href="membersarea.php?start=<?=$start;?>&orderField=maxbid&orderType=<?=$newOrder.$additionalVars;?>">
                     <?=$lang[max_bid]?>
                     </a></td>
                  <td class="boldgrey" width="120" align="center"><? 
							echo $lang[relist]."
							<br>[ <a href=\"javascript:void(0);\" onclick=\"checkAll(document.closed_auctions['relist[]'], ".$nbClosedAuctions.", true);\">".$lang[all]."</a> | 
							<a href=\"javascript:void(0);\" onclick=\"checkAll(document.closed_auctions['relist[]'], ".$nbClosedAuctions.", false);\">".$lang[none]."</a> ]"; ?></td>
                  <td class="boldgrey" width="80" align="center"><?
							echo $lang[delete]."
							<br>[ <a href=\"javascript:void(0);\" onclick=\"checkAll(document.closed_auctions['delete[]'], ".$nbClosedAuctions.", true);\">".$lang[all]."</a> | 
							<a href=\"javascript:void(0);\" onclick=\"checkAll(document.closed_auctions['delete[]'], ".$nbClosedAuctions.", false);\">".$lang[none]."</a> ]"; ?></td>
               </tr>
               <tr class="c5">
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               </tr>
               <? 
					while ($closedAuctions=mysqli_fetch_array($getClosedAuctions)) {
						if ($setts['date_format_type']=="USA") $format="m/d/Y";
						else $format="d/m/Y"; ?>
               <tr>
                  <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $closedAuctions['itemname'], 'id' => $closedAuctions['id']));?>">#
                     <?=$closedAuctions['id'];?>
                     </a> </td>
                  <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $closedAuctions['itemname'], 'id' => $closedAuctions['id']));?>">
                     <?=$closedAuctions['itemname'];?>
                     </a>
                     <?=auctionListedIn($closedAuctions['id']); ?>
                     <? echo ($closedAuctions['rp']=="Y"&&$closedAuctions['closed']==1&&$closedAuctions['rpvalue']>$closedAuctions['maxbid']&&$_SESSION['memberid']==$closedAuctions['ownerid']&&$closedAuctions['nrbids']>0&&$closedAuctions['rpwinner']==0) ? "&nbsp; <span class=\"smallfont\">[ <a href=\"reserveoffers.php?id=".$closedAuctions['id']."\"><strong>$lang[makeresoffer]</strong></a> ]</span>" : ""; ?> </td>
                  <td class="smallfont"><? echo convertdate($closedAuctions['startdate'],$format);?></td>
                  <td class="smallfont"><? echo convertdate($closedAuctions['enddate'],$format);?></td>
                  <td align="center" class="smallfont"><? echo (($closedAuctions['nrbids']>0)?$closedAuctions['nrbids']:"-");?></td>
                  <td align="center" class="smallfont"><? echo (($closedAuctions['nrbids']>0)?displayAmount($closedAuctions['maxbid'],$closedAuctions['currency']):"-");?></td>
                  <td align="center" nowrap class="smallfont"><input name="relist[]" type="checkbox" id="<?=$closedAuctions['id'];?>" value="<?=$closedAuctions['id'];?>" class="checkrelist">
                     <?
							echo "<SELECT name=\"duration[".$closedAuctions['id']."]\" class=\"contentfont\">";
							$getDurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations");
							while ($durationDetails=mysqli_fetch_array($getDurations)) {
								echo "<OPTION value=\"".$durationDetails['days']."\" ".(($durationDetails['days']==$closedAuctions['duration'])?"SELECTED":"").">".$durationDetails['description']."</option>";
							}
							echo "</SELECT>"; ?>
                     <br>
                     <a href="sellitem.php?option=sellsimilar&similarid=<?=$closedAuctions['id'];?>">
                     <?=$lang[relist_similar];?>
                     </a> </td>
                  <td align="center"><input name="delete[]" type="checkbox" id="<?=$closedAuctions['id'];?>" value="<?=$closedAuctions['id'];?>" class="checkdelete"></td>
               </tr>
               <? } ?>
               <tr class="c4">
                  <td colspan="8" align="center"><input name="processauctions" type="submit" value="<?=$lang[makechanges]?>"></td>
               </tr>
               <tr>
                  <td colspan="8" class="smallfont"><strong>
                     <?=$lang[note]?>
                     </strong>
                     <?=$lang[sell_note]?></td>
               </tr>
            </form>
         </table>
         <?
			## display pagination
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\" >\n";
			$additionalVars.="&auc_type=".$data["auc_type"]."&auc_period=".$data["auc_period"]."&auc_bid=".$data["auc_bid"]."";
			paginate($start,$limit,$nbClosedAuctions,"membersarea.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); 
			echo "</td></tr></table>";

		} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<? 

} elseif ($data["auc_type"] == 2) {


?>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&raquo;&nbsp;
                  <?=$lang[unstartedauctions]?>
                  :</b> (
                  <?=$nbUnstartedAuctions;?>
                  <?=$lang[items]?>
                  ) </td>
            </tr>
         </table>
         <? if ($nbUnstartedAuctions>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr>
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_id]?>
                  </a></td>
               <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_name]?>
                  </a></td>
               <td width="80" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=startdate&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[starts]?>
                  </a></td>
               <td width="80" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=enddate&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[ends]?>
                  </a></td>
               <td width="100" align="center" class="boldgrey"><?=$lang[options]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <? 
				while ($unstartedAuctions = mysqli_fetch_array($getUnstartedAuctions)) {
					if ($setts['date_format_type']=="USA") $format="m/d/Y";
					else $format="d/m/Y"; ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $unstartedAuctions['itemname'], 'id' => $unstartedAuctions['id']));?>"> #
                  <?=$unstartedAuctions['id'];?>
                  </a></td>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $unstartedAuctions['itemname'], 'id' => $unstartedAuctions['id']));?>">
                  <?=$unstartedAuctions['itemname'];?>
                  </a>
                  <?=auctionListedIn($unstartedAuctions['id']); ?></td>
               <td align="center" class="smallfont"><? echo convertdate($unstartedAuctions['startdate'],$format);?></td>
               <td align="center" class="smallfont"><? echo convertdate($unstartedAuctions['enddate'],$format);?></td>
               <td align="center" class="smallfont"><? 
						if ($unstartedAuctions['payment_status']=="unconfirmed"&&$unstartedAuctions['active']==0) {
							$nbAddImages = getSqlNumber("SELECT * FROM probid_auction_images WHERE auctionid='".$unstartedAuctions['id']."'");
							echo "<a href=\"payfee.php?option=pay_auction_setup&auctionid=".$unstartedAuctions['id']."&startprice=".$unstartedAuctions['bidstart']."&nbimages=".$nbAddImages."\">".$lang[paysetupfee]."</a>";
						} else if ($unstartedAuctions['nrbids']==0) {
							echo "<a href=\"editauction.php?id=".$unstartedAuctions['id']."\">".$lang[edit]."</a><br> 	\n";
							echo "<a href=\"membersarea.php?page=selling&option=delete&id=".$unstartedAuctions['id']."\">".$lang[remove]."</a>"; 
						} 
						if ($unstartedAuctions['active']==0&&$unstartedAuctions['approval']==0&&$unstartedAuctions['payment_status']=="confirmed") {
							echo "<br>".$lang[avaitingapproval];
						}
						
						?>
               </td>
            </tr>
            <? } ?>
         </table>
         <? 
			## display pagination
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\" >\n";
			$additionalVars.="&auc_type=".$data["auc_type"]."&auc_period=".$data["auc_period"]."&auc_bid=".$data["auc_bid"]."";
			paginate($start,$limit,$nbUnstartedAuctions,"membersarea.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); 
			echo "</td></tr></table>";
			} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<?

} elseif ($data["auc_type"] == 3) {

    
    //FINISH

$soldItems=getSqlRow("SELECT count(w.id) AS nb_auctions, sum(w.quant_offered) AS nb_quant, sum(w.amount * w.quant_offered) AS nb_amount FROM 
probid_winners w, probid_auctions a WHERE w.sellerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.s_deleted!=1"); 

$start = (empty($_GET['start'])) ? 0 : $_GET['start'];
$limit = 10;

$orderField = (empty($_REQUEST['orderField'])) ? "a.id" : $_REQUEST['orderField'];
if (empty($_REQUEST['orderType'])) {
	$orderType = "DESC";
	$newOrder="ASC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}

$additionalVars = "&purchase_date=".$_REQUEST['purchase_date'];

$o86400 = ($_GET['purchase_date']==86400) ? "selected" : "";
$o604800 = ($_GET['purchase_date']==604800) ? "selected" : "";
$o2592000 = ($_GET['purchase_date']==2592000) ? "selected" : "";
$o31536000 = ($_GET['purchase_date']==31536000) ? "selected" : "";

$htmlPart = <<<BLOCK
<table width="100%" border="0" cellspacing="1" cellpadding="1">
	<tr>
   	<td width="50%" class="cathead"><b>{$lang[solditems]}</b> ({$soldItems['nb_auctions']} {$lang[items]})</td>
		<form action="w-contactinfo.php?start={$start}&orderField={$orderField}&orderType={$orderType}" method="get">
      <td width="50%" align="right" nowrap="nowrap" class="cathead">{$lang[viewitemssold]}
			<select name="purchase_date" onchange="form.submit(this);" style="font-size: 10px; height: 16px;">
         	<option value="0" selected="selected">{$lang[all]}</option>
				<option value="86400" {$o86400}>{$lang[lastday]}</option>
				<option value="604800" {$o604800}>{$lang[lastweek]}</option>
				<option value="2592000" {$o2592000}>{$lang[lastmonth]}</option>
				<option value="31536000" {$o31536000}>{$lang[lastyear]}</option>
			</select> &nbsp; &nbsp; &nbsp;</td>
		</form>
	</tr>
</table>
BLOCK;
headercat($htmlPart);
?>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="border">
   <tr>
      <td class="boldgrey"><a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.id&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[item_id]?></a>
<? if($_GET['orderField'] == 'w.id'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
 / <a href="w-contactinfo.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[item_name];?>
         </a>
<? if($_GET['orderField'] == 'a.itemname'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
</td>
      <td align="center" class="boldgrey"><a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.amount&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[bid]?>
         </a>
<? if($_GET['orderField'] == 'w.amount'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
</td>
      <td align="center" class="boldgrey"><?=$lang[quant]?></td>
      <td class="boldgrey"><?=$lang[buyerinfo]?></td>
      <td align="center" class="boldgrey"><a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.purchase_date&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[purchase_date];?>
         </a>
<? if($_GET['orderField'] == 'w.purchase_date'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
 / <a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.flag_paid&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[status];?>
         </a>
<? if($_GET['orderField'] == 'w.flag_paid'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?></td>
      <td align="center" class="boldgrey"><?=$lang[option]?></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? 
	$addQuery = ($_REQUEST['purchase_date']>0) ? " AND w.purchase_date>=(".time()."-".$_REQUEST['purchase_date'].") " : "";
	$nbBuyers = getSqlNumber("SELECT w.* FROM probid_winners w WHERE w.sellerid='".$_SESSION['memberid']."' AND w.s_deleted!=1".$addQuery);
	
	$getBuyers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.* FROM probid_winners w, probid_auctions a WHERE w.sellerid='".$_SESSION['memberid']."' 
	AND w.s_deleted!=1 AND w.auctionid=a.id ".$addQuery."
	ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	
	while ($buyerDetails = mysqli_fetch_array($getBuyers)) { 
		$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$buyerDetails['auctionid']."'");
		$buyer = getSqlRow("SELECT * FROM probid_users WHERE id='".$buyerDetails['buyerid']."'");

		$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auction['id']."'","category");

		$category_id = getMainCat($itemCategory);

		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

		if ($buyerDetails['payment_status']=="confirmed"||($fee['is_endauction_fee']=="N")) { ?>
   <tr class="<? echo (($count++)%2==0)?"c2":"c3";?>" valign="top">
      <td class="smallfont"><strong> <? echo ($auction['itemname']=="")?$lang[auctiondeleted]:'<a href="'.processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id'])).'">'.$auction['itemname'].'</a>';?>
      <td align="center" class="smallfont"><? echo displayAmount($buyerDetails['amount'],$auction['currency']); ?></td>
      <td class="smallfont">
      <? echo (($auction['auctiontype']=="dutch")?"".$buyerDetails['quant_req']."":"1");?> /
         <? echo (($auction['auctiontype']=="dutch")?"".$buyerDetails['quant_offered']."":"1");?> </td>
      <td class="smallfont"><table width="100%" border="0" cellpadding="2" cellspacing="1" class="c4">
            <tr valign="top" class="c2">
               <td class="smallfont"><? echo ($buyer['username']=="")?$lang[userdeleted]:$buyer['username'];?></td>
            </tr>
            
         </table></td>
      <td align="center"><? echo ($buyerDetails['purchase_date']>0) ? date($setts['date_format'],$buyerDetails['purchase_date']+$diff) : $lang[na]; ?><br />
         <table width="100%" border="0" cellpadding="2" cellspacing="1" class="border c4">
            <form action="w-contactinfo.php" method="post">
               <input type="hidden" name="win_id" value="<?=$buyerDetails['id'];?>">
               <tr>
                  <td align="center"><select name="flag_paid" style="font-size:10px; width: 100px;">
                        <option value="0" <? echo ($buyerDetails['flag_paid']==0) ? "selected":""; ?>>
                        <?=$lang[flag_paid_unpaid]; ?>
                        </option>
                        <option value="1" <? echo ($buyerDetails['flag_paid']==1) ? "selected":""; ?>>
                        <?=$lang[flag_paid_paid]; ?>
                        </option>
                     </select></td>
               </tr>
               <tr>
                  <td align="center"><select name="flag_status" style="font-size:10px; width: 100px;">
                        <option value="0" <? echo ($buyerDetails['flag_status']==0) ? "selected":""; ?>>
                        <?=$lang[flag_status_0]; ?>
                        </option>
                        <option value="1" <? echo ($buyerDetails['flag_status']==1) ? "selected":""; ?>>
                        <?=$lang[flag_status_1]; ?>
                        </option>
                        <option value="2" <? echo ($buyerDetails['flag_status']==2) ? "selected":""; ?>>
                        <?=$lang[flag_status_2]; ?>
                        </option>
                        <option value="3" <? echo ($buyerDetails['flag_status']==3) ? "selected":""; ?>>
                        <?=$lang[flag_status_3]; ?>
                        </option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <td align="center"><input type="submit" name="updflagok" value="<?=$lang[go];?>" style="font-size:10px; width: 100px;"></td>
               </tr>
            </form>
         </table></td>
      <td class="smallfont" style="line-height: 130%;"><? 
      echo '<b class=""><a href="write.php?toid='.$buyerDetails['buyerid'].'&amp;tosid='.md5($buyerDetails['buyerid']).'&amp;aucid='.$buyerDetails['auctionid'].'">'.$lang[sendmessage].'</a></b><br>';
	  
	  if($_REQUEST["x"]=="true"){
/* BEGIN SAFE ZONE */
echo "1688:".$auctionDetails['ownerid']."<br>";
echo "239389:".$auctionDetails['id']."<br>";
echo $auctionWinner['buyerid']."<br>";
echo "sum:".md5($auctionWinner['buyerid'])."<br>";
echo $buyerDetails['buyerid']."<br>";
echo "sum right:".md5($buyerDetails['buyerid'])."<br>";


/* END SAFE ZONE */}
			$isFeedback = getSqlNumber("SELECT * FROM probid_feedbacks WHERE fromid='".$_SESSION['memberid']."' AND userid='".$buyerDetails['buyerid']."' AND auctionid='".$buyerDetails['auctionid']."' AND submitted=0");
			if ($isFeedback>0) echo '<a href="leavefeedback.php?to='.$buyerDetails['buyerid'].'&from='.$_SESSION['memberid'].'&auctionid='.$buyerDetails['auctionid'].'">'.ucwords($lang[feedback_leave]).'</a>';

		##	if(!$buyerDetails['invoice_sent']) echo '<br>&nbsp;&#8226;&nbsp;<a href="sendinvoice.php?winnerid='.$buyerDetails['id'].'">'.$lang[m_send_invoice].'</a>';
        ## 	else echo'<br><a href="showinvoice.php?winnerid='.$buyerDetails['id'].'" target="_blank">'.$lang[m_show_invoice].'</a>';

			echo '<script type="text/javascript"> ';
			echo '	function openPopUp(url) { ';
			echo '		var bank = window.open(url,\'bankdetails\',\'width=300, height=300\'); ';
			echo '		bank.focus(); ';
			echo '	} ';
        	echo '</script> ';

		##Removed by MAK 05.12.2009
		##	echo '<br>&nbsp;&#8226;&nbsp;<a href="javascript:void(0);" onclick="openPopUp(\'bankdetails.php?winnerid='.$buyerDetails['id'].'&auctionid='.$buyerDetails['auctionid'].'\');">'.$lang[m_send_bank_details].'</a> <br>'; 
			echo '<b class="redfont"><a href="w-contactinfo.php?option=delete&id='.$buyerDetails['id'].'"><span class="redfont">'.$lang[delete].'</span></a></b>';
			?></td>
   </tr>
   <? if ($buyerDetails['amountpaid']>0) { ?>
   <!--<tr valign="top" class="c4">
      <td class="contentfont"><? 
			//	echo '<b>#'.$buyerDetails['auctionid'].'</b> </td><td colspan="3" class="smallfont"><a href="w-contactinfo.php?option=claimback&auctid='.$buyerDetails['auctionid'].'&buyerid='.$buyerDetails['buyerid'].'&amount='.$buyerDetails['endamount'].'&txnid='.$buyerDetails['txnid'].'">'.$lang[claimbackendauc].'</a>';
			 ?>
      </td>
  <!-- </tr>
   <? } } else { ?>
   <tr valign="top" class="c4">
      <td class="contentfont"><?
			echo '<b>#'.$buyerDetails['auctionid'].'</b></td><td colspan="3" class="smallfont">'.$lang[err_endofauc_fee].'<br>';

			if (@eregi('s', $fee['endauction_fee_applies'])) 
				echo '<a href="payfee.php?option=pay_endauction_fee&auctionid='.$buyerDetails['auctionid'].'&finalbid='.$buyerDetails['amount'].'">'.$lang[payfee].'</a>'; 
			?></td>
   </tr>
   <? }
	} ?>
   <tr>
      <td align="center" class="contentfont c4" colspan="6"><? paginate($start,$limit,$nbBuyers,"w-contactinfo.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); ?></td>
   </tr>
</table>
<? } else if ($_SESSION['is_seller']!="Y") {
	echo $lang[seller_error]; 
} else { 
	echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; 
} 
}
## AUTHENTICATION CONTINUE - ALEKSEI
}
else
{
	// Я неавторизованный пользователь
	include 'pangalink/authenticate.php';
}
?>
