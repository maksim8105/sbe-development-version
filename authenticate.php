<?
## v5.24 -> apr. 05, 2006
session_start();

// Для отображения данной страницы пользователь должен быть залогинен
if ( $_SESSION['membersarea'] != "Active" && $_SESSION['accsusp'] != 2 )
{
	echo "<script>document.location.href='http://www.sbe.ee/'</script>";
	exit;
}

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");
header5("$lang[authentication]"); ?>

<table width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr class="contentfont">
     <td>

<?php

// Содержимое страницы в этом файле
include 'pangalink/authenticate2.php';

?>
     </td>
   </tr>
</table>

<?php
include ("themes/".$setts['default_theme']."/footer.php"); ?>
