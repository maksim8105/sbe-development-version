<?php

$userDetails = getSqlRow("SELECT `authenticated`, `isikukood`, `realname` FROM probid_users WHERE id='".$_SESSION['memberid']."'");

// Подключаем базовый класс Pangalink'а
include_once 'Pangalink.class.php';

// Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
$Pangalink =& new Pangalink( 'config/pangalink.ini', true );

// Адрес URL, куда будет возвращён ответ
$pangalink_return_url = 'http://www.sbe.ee/success.php';

/*echo '<pre>';
echo "Details dump:\r\n";
print_r( $userDetails );
echo '</pre>';*/

echo '<p>';
if ( 'Y' == strtoupper( $userDetails['authenticated'] ) )
{
	##echo 'Vy uzhe avtorizovany!';
}
else
{
	##echo 'Vy esho ne avtorizovany!';
?>

     	<p><strong><?=$lang[authenticate];?></strong></p>
		
		<table width="100%" align="center" cellspacing="5" cellpadding="5" border="0">
			<tr>
				<td>Eesti</td>
			</tr>
			<tr>
				<td><strong>Estonia ja SEPA riigid (EuroopaLiit)</strong></td>
			</tr>
			<tr>
				<td>Kui olete teise Eesti pangakonto omanik siis võite end autentida sooritades sissemakset SBE.EE e-kontole. Kõik raha kantakse Teie e-kontole ühe tööpäeva jooksul. E-konto raha on alati võimalik kanda välja sama pangakontole.
				<br><br>Jätkamiseks klikkige oma pangalogole ning sooritage makse üleantud rekvisiititega
				</td>
				<td width="34%">
					<table>
						<tr><td colspan="2" align="center"><strong>Makse rekvisiidid</strong></td></tr>
						<tr><td>Saaja: </td><td> MK AND PARTNERS OÜ</td></tr>
						<tr><td>Kontonumber: </td><td> MK AND PARTNERS OÜ</td></tr>
						<tr><td>Kontonumber: </td><td> MK AND PARTNERS OÜ</td></tr>
						<tr><td>Selgitus: </td><td>SBE.EE: <? echo date('dmY', time())."-".$_SESSION['memberid'];?></td></tr>
					</table>
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<table width="100%">
					<tr>
						<td>Eesti</td>
						<td>Läti</td>
						<td>Soome</td>
						<td>Saksamaa</td>
						<td>Suurbritannia</td>
						<td>Hispaania</td>
					</tr>
					<tr>
						<td>
							<img src=img/bank/hanza.png width=44 height=15 alt="Swedbank" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="Nordea" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="Sampo" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="Krediidipank" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="DnB" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="LHV Pank" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="Tallinna Äripank" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="Handelsbanken" border=0><br><br>
							<img src=img/bank/hanza.png width=44 height=15 alt="MARFIN BANK" border=0><br><br>
							      </td>
						<td>Swedbank / SEB Banka  / Latvijas Krājbanka / DnB NORD Banka</td>
						<td>Nordea Bank Finland PLC / OP-Pohjola Group / Aktia Savings Bank PLC / LähiTapiola / DanskeBank PLC / Säästöpankki / Handelsbanken</td>
						<td>KfW / ING-DiBa / Deutsche Postbank / Baden-Württembergische Bank / HVB Group / Deutsche Bank / Commerzbank / DKB /Fidor</td>
						<td>Halifax / Nationwide / Santander / Lloyds Banking Group / Royal Bank of Scotland Group (RBS) / Barclays PLC / HSBC / Standard Chartered</td>
						<td>Banco Bilbao Vizcaya Argentaria (BBVA) / La Caixa / EVO Banco / Banco Sabadell / Bilbao Bizkaia Kutxa / Santander Group / Banco Popular Español</td>
					</tr>
				</table>
				
				</td>
			</tr>
			<tr>
				<td colspan="2">NB: Sissemaksed on seotud pankadevahelistele maksetele kehtestatud ajaliste piirangutega. Sissemakse kajastumine võib võtta aega kuni 1 tööpäev. Nädalavahetusel tehtud sissemaksed kajastuvad kontol uue töönädala alguses.</td>
			</tr>
			<tr>
				<td><strong>Teised riigid (NON-SEPA)</strong></td>
			</tr>
			<tr>
				<td>Kui olete teise Eesti pangakonto omanik siis võite end autentida sooritades sissemakset SBE.EE e-kontole. Kõik raha kantakse Teie e-kontole ühe tööpäeva jooksul. E-konto raha on alati võimalik kanda välja sama pangakontole.
				<br><br>Kui teie pank ei ole nimekirjas, siis saate teha sissemakset
				<br><br>NB: Sissemaksed on seotud pankadevahelistele maksetele kehtestatud ajaliste piirangutega. Sissemakse kajastumine võib võtta aega kuni 1 tööpäev. Nädalavahetusel tehtud sissemaksed kajastuvad kontol uue töönädala alguses.
				</td>
				<td width="34%">
					<table>
						<tr><td>Saaja: </td><td> MK AND PARTNERS OÜ</td></tr>
						<tr><td>Kontonumber: </td><td> MK AND PARTNERS OÜ</td></tr>
						<tr><td>Kontonumber: </td><td> MK AND PARTNERS OÜ</td></tr>
						<tr><td>Selgitus: </td><td> Sissemakse SBE.EE e-kontole</td></tr>
					</table>
				<td>
				</tr>
				<tr>
				<td>Jätkamiseks klikkige oma pangalogo</td>
				</tr>
				<tr>
				<td>
				<table>
					<tr>
					<td>Venemaa</td>
					<td>Ukraina</td>
					<td>Valgevenemaa</td>
					<td>Kazakhstan</td>
					<td>Gruusia</td>
					<td>Armeenia</td>
					</tr>
				</table>
				
				</td>
			</tr>
     	</table>
<? 
	if (eregi("register.php",$_SERVER['PHP_SELF'])) { ?>
		<div class="buttonsbox2" >  	
                		<a class="loginbutton" href="/membersarea.php?page=preferences"><?=$lang[wish_authenticate_later];?></a>
		</div>	    		
<? 
}
?>
<hr>
     		

<? }
echo '</p>'; ?>
