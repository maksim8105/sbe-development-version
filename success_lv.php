<?
## v5.24 -> apr. 05, 2006
session_start();

// Для отображения данной страницы пользователь должен быть залогинен
if ( $_SESSION['membersarea'] != "Active" && $_SESSION['accsusp'] != 2 )
{
	echo "<script>document.location.href='login.php?redirect=authenticate'</script>";
	exit;
}

include_once ("config/config.php");

// Подключаем базовый класс Pangalink'а
include_once 'pangalink/Pangalink.class.php';

// Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
$Pangalink =& new Pangalink( 'config/pangalink_lv.ini', true );

if ( ! $Pangalink->hasPangalinkReply() )
{
	echo "<script>document.location.href='authenticate.php'</script>";
	exit;
}

function get_value( $string, $num = 1, $separator = ':', $default = '' )
{
	$result = explode( $separator, $string );

	return isset( $result[ $num ] ) ? $result[ $num ] : $default;
}

// Инициализировать пангалинк. Скрипт сам определит от какого банка был получен ответ и можно ли его обработать
if ( false === ( $pank = $Pangalink->initializePangalink() ) )
{
	// Ошибка инициализации пангалинка. Получены неверные данные или данные не получены
	echo "<script>document.location.href='authenticate.php'</script>";
	exit;
}

include ("themes/".$setts['default_theme']."/header.php");
header5("$lang[authentication]"); ?>
<table width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr class="contentfont">
     <td>
     <p>&nbsp;</p>
     <p><strong><?php
     
/*echo '<pre>';
print_r( $_REQUEST );
echo '</pre>';*/

if ( $pank->validateForm( $_REQUEST, 3002 ) )
{
	## FIX FOR SEB Uhispank - ALEKSEI
	
	/*
	$vk_info = explode( ';', $_REQUEST['VK_INFO'] );

	$isikukood = get_value( $vk_info[0] );
	$isikukood = mysql_real_escape_string( $isikukood );

	$realname = isset( $vk_info[1] ) ? get_value( $vk_info[1] ) : '';
	$realname = mysql_real_escape_string( $realname );*/
	
	## End of fix
	
	$isikukood = $realname = '';
	$vk_info = explode( ';', $_REQUEST['VK_INFO'] );
	
	foreach( $vk_info as $field )
	{
        $result = explode( ':', $field );

        if ( 2 == count( $result ) )
        {
                list( $key, $value ) = $result;

                $key = strtoupper( trim( $key ) );
                $value = trim( $value );

                if ( 'NIMI' == $key )
                {
                        $realname = $value;
                }
                elseif ( 'ISIK' == $key || 'KOOD' == $key )
                {
                        $isikukood = $value;
                }
        }
	}
	
   /*$isikukood = $realname = '';
   $vk_info = explode( ';', $_REQUEST['VK_INFO'] );

   foreach( $vk_info as $field )
   {
       $result = explode( ':', $field );

       if ( 2 == count( $result ) )
       {
               list( $key, $value ) = $result;

               $key = strtoupper( trim( $key ) );
               $value = trim( $value );

               if ( 'ID' == $key )
               {
                        $isikukood = $value;
               }
               else
               {
                       $realname = $value;
               }
       }
   } */

	$isikukood = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"],  $isikukood ) : ((trigger_error("Error. Please contact administration.", E_USER_ERROR)) ? "" : ""));
	
	##$realname = mysql_real_escape_string( $realname );
	
	$realname = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"],  iconv( 'ISO-8859-4', 'UTF-8', $realname ) ) : ((trigger_error("Error. Please contact administration.", E_USER_ERROR)) ? "" : ""));
	
	## END OF FIX - ALEKSEI

	##mysql_query( "UPDATE `probid_users` SET `authenticated` = 'Y', `isikukood` = '$isikukood', `realname` = '$realname' WHERE id='{$_SESSION['memberid']}'");

	mysqli_query($GLOBALS["___mysqli_ston"],  "UPDATE `probid_users` SET `authenticated` = 'Y', `isikukood` = '$isikukood', `realname` = '$realname' WHERE id='{$_SESSION['memberid']}'") or die( ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)) );

	echo $lang['authentication_successful'];
}
else
{
	
/*$fp = fopen("test.txt", "w");
$testdata = "Test!!!\n";
fwrite($fp, $testdata); 
fclose($fp);*/

	echo $lang['authentication_unsuccessful'];
}

     ?></strong></p>
     <p>&nbsp;</p>
     </td>
   </tr>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>
