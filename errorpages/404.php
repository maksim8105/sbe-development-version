<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<!--[if gte IE 9]>
  <style type="text/css">
    .searchbar {
       filter: none;
    }
  </style>
<![endif]-->

<title>
Oksjonid, kuulutused, ostusoovid, e-poed - SBE.EE </title>
<meta name="author" content="www.sbe.ee">
<meta name="description" content="SBE.ee on Balti regiooni virtuaalne arikeskkond. SBE.EE arikeskkond annab suureparase voimaluse uute kliente leidmiseks Eestis ja valismaal.

">
<meta name="keywords" content="oksjonid, kuulutused, e-poed, ostusoovid, tasuta, margid, raamatud, mundid, BMW, varuosad">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="themes/v52/style.css" rel="stylesheet" type="text/css">
<link href="themes/v52/newengine.css" rel="stylesheet" type="text/css">

<!--[if IE]>
        <link  href="themes/v52/newengine_ie.css" rel="stylesheet" type="text/css"/>
<![endif]-->
		


<style type="text/css">
<!--

a.ln:link {
	background-image:  url(themes/v52/img/sidem.gif);
}
a.ln:visited {
	background-image:  url(themes/v52/img/sidem.gif);
}
a.ln:hover {
	background-image:  url(themes/v52/img/sidemo.gif);
}
-->
</style>
<script language="javascript" src="themes/v52/main.js" type="text/javascript"></script>
<script language=JavaScript src='scripts/innovaeditor.js'></script>
<script type="text/javascript">
var currenttime = 'September 14, 2012 21:51';
var serverdate=new Date(currenttime);

function padlength(what){
	var output=(what.toString().length==1)? "0"+what : what;
	return output;
}

function displaytime(){
	serverdate.setSeconds(serverdate.getSeconds()+1)
	var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes());
	document.getElementById("servertime").innerHTML=timestring;
}

window.onload=function(){
	setInterval("displaytime()", 1000);
}

</script>
</head>
<form name="hiddenLang" method="post" action="/">
<input type="hidden" value="" id="req_lng" name="lang">

</form>
<body bgcolor="#ffffff" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
	<div class="errormessage"></div>
</body>
</html>