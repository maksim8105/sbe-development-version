<?
## v5.25 -> jun. 26, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php"); ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td class="contentfont"><?
			if (isset($_POST['makeswapok'])) {
				if ($_POST['quantity']>$_POST['quantavail']) {
					echo $lang[not_enough_swap];
				} else if ($_POST['quantity']<=$_POST['quantavail']) {

					$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_POST['auctionid']."'");

					$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auction['id']."'","category");
				
					$category_id = getMainCat($itemCategory);
				
					$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
					$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

					$prefSeller1 = "N";
					if ($setts['pref_sellers']=="Y") {
						$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
						WHERE id='".$_POST['sellerid']."'","preferred_seller");
					}
		
					if ($_POST['quantity']==$_POST['quantavail']) {
						$closeAuction=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET closed=1,swapped=1,enddate='".$timeNowMysql."' WHERE id='".$_POST['auctionid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						delcatcount(getSqlField("SELECT category FROM probid_auctions WHERE id='".$_POST['auctionid']."'","category"),$_POST['auctionid']);
						delcatcount(getSqlField("SELECT addlcategory FROM probid_auctions WHERE id='".$_POST['auctionid']."'","addlcategory"),$_POST['auctionid']);
					}
					if ($_POST['auctiontype']=="dutch") $subtractItem=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET quantity=quantity-'".$_POST['quantity']."' WHERE id='".$_POST['auctionid']."'");

					$payerId = (@eregi('s', $fee['endauction_fee_applies'])) ? $_POST['sellerid'] : $_POST['buyerid'];

					if ($fee['is_swap_fee']=="Y"&&$fee['val_swap_fee']>0) {
						// if account mode is on, then add the end auction fee to the seller's balance, and activate the details of the winner
						$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$payerId."'","payment_mode");
						if ($setts['account_mode_personal']==1) {
								$account_mode_local = ($tmp) ? 2 : 1;
						} else $account_mode_local = $setts['account_mode'];
				
						if ($account_mode_local==2) {
							$amount=0;
							if ($fee['val_swap_fee']!="") {
								$amount+=$fee['val_swap_fee'];
							}
							$amount = applyVat($amount,$payerId);
							$amount = calcReduction ($amount, $prefSeller);
		
							$payment_status="confirmed";
							$active=1;
							$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$payerId."'","balance");
							$updateSellerBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
							balance=balance+".$amount." WHERE id='".$payerId."'");
							// add end of auction fee on the invoices table 
							$currentTime = time();
							$newBalance = $amount+$currentBalance;
							$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
							(userid,auctionid,feename,feevalue,feedate,balance)
							VALUES ('".$payerId."','".$_POST['auctionid']."','".$lang[swap_fee]."','".$amount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						} else {
							$payment_status="unconfirmed";
							$active=0;
						}
					} else {
						$payment_status="confirmed";
						$active=1;
					}
					$saveWinner=mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_winners 
					(sellerid,buyerid,amount,auctionid,auctiontype,quant_req,quant_offered,
					active,payment_status,amountpaid,purchase_date) VALUES 
					('".$_POST['sellerid']."','".$_POST['buyerid']."','-1','".$_POST['auctionid']."',
					'".$_POST['auctiontype']."','".$_POST['quantity']."','".$_POST['quantity']."',
					'".$active."','".$payment_status."','".$fee[val_swap_fee]."','".time()."')");
					
					$winnerInsertId = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
					
					$addSale=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET items_sold=items_sold+1 WHERE id='".$_POST['sellerid']."'");
					$addPurchase=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET items_bought=items_bought+1 WHERE id='".$_POST['buyerid']."'");
						
					$sellerUsername = getSqlField("SELECT * FROM probid_users WHERE id='".$_POST['sellerid']."'","username");
					$buyerUsername = getSqlField("SELECT * FROM probid_users WHERE id='".$_POST['buyerid']."'","username");
					$prepareFeedbackForSeller = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
					(userid,usernick,fromid,submitted,auctionid,type) VALUES 
					('".$_POST['sellerid']."','".$sellerUsername."','".$_POST['buyerid']."',0,
					'".$_POST['auctionid']."','sale')");
					$prepareFeedbackForBuyer = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
					(userid,usernick,fromid,submitted,auctionid,type) VALUES 
					('".$_POST['buyerid']."','".$buyerUsername."','".$_POST['sellerid']."',0,
					'".$_POST['auctionid']."','purchase')");
		
					echo "<div align=center>$lang[swap_success]</div>";
							
					$sellerId = $_POST['sellerid'];
					$buyerId = $_POST['buyerid'];
					$auctionId = $_POST['auctionid'];
					$winnerId = $winnerInsertId;
						
					include ("mails/notifysellerswap1.php");
					include ("mails/notifybuyerswap1.php");
							
					
					$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$winnerInsertId."'","payment_mode");
						if ($setts['account_mode_personal']==1) {
						$account_mode_local = ($tmp) ? 2 : 1;
						} else $account_mode_local = $setts['account_mode'];
		
					if ($account_mode_local==2) {
						mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET emailsent=1 WHERE id='".$winnerInsertId."'");				
						include ("mails/buyerdetails.php");		
						include ("mails/sellerdetails.php");		
					}
				}
			} else if (isset($_POST['makeswapcancel'])) {
				echo "<div align=center>$lang[swap_reject]<br>";
				// email the swapper
				$sellerId = $_POST['sellerid'];
				$buyerId = $_POST['buyerid'];
				$auctionId = $_POST['auctionid'];
				include ("mails/notifybuyerswapreject.php");
				$deleteOffer=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_swaps WHERE id='".$_POST['id']."'");
			} else { 
				$swappedAuction=getSqlRow("SELECT * FROM probid_swaps WHERE id='".$_REQUEST['id']."' AND sellerid='".$_SESSION['memberid']."'");
				$auction=getSqlRow("SELECT * FROM probid_auctions WHERE id='".$swappedAuction['auctionid']."' AND ownerid='".$_SESSION['memberid']."'");
				$isAuction=getSqlNumber("SELECT * FROM probid_swaps WHERE id='".$_REQUEST['id']."' AND sellerid='".$_SESSION['memberid']."'");
				if ($isAuction>0) {
				?>
         <form action="makeswap.php" method="post">
            <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
            <input type="hidden" name="itemname" value="<?=$auction['itemname'];?>">
            <input type="hidden" name="sellerid" value="<?=$_SESSION['memberid'];?>">
            <input type="hidden" name="buyerid" value="<?=$swappedAuction['buyerid'];?>">
            <input type="hidden" name="auctionid" value="<?=$auction['id'];?>">
            <input type="hidden" name="quantity" value="<?=$swappedAuction['quantity'];?>">
            <input type="hidden" name="quantavail" value="<?=$auction['quantity'];?>">
            <input type="hidden" name="auctiontype" value="<?=$auction['auctiontype'];?>">
            <table width="80%" border="0" align="center" cellpadding="4" cellspacing="0" class="chenar">
               <tr class="contentfont">
                  <td width="50%" align="right"><span style="font-weight: bold">
                     <?=$lang[quant_req]?>
                     :</span></td>
                  <td><?=$swappedAuction['quantity'];?>
                     <?=$lang[items_swap_available];?>
                     <?=$auction['quantity'];?>
                     <?=$lang[items_swap];?></td>
               </tr>
               <tr align="center" class="contentfont">
                  <td colspan="2"><span style="font-weight: bold">
                     <?=$lang[swap_offerdescr]?>
                     </span></td>
               </tr>
               <tr align="center" class="contentfont">
                  <td colspan="2"><?=$swappedAuction['description'];?></td>
               </tr>
               <tr align="center" class="contentfont">
                  <td colspan="2"><input name="makeswapok" type="submit" id="makeswapok" value="<?=$lang[swap_accept]?>">
                     &nbsp; &nbsp;
                     <input name="makeswapcancel" type="submit" id="makeswapcancel" value="<?=$lang[swap_reject]?>"></td>
               </tr>
            </table>
         </form>
         <? } else { 
	   			echo "<p align=center><strong>$lang[swapimpossible]</strong></p>"; 
				}
			} ?>
      </td>
   </tr>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
