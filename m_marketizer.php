﻿<?
## v5.24 -> apr. 06, 2006
## Authorize.net ADDON
if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {

	if ( !defined('INCLUDED') ) { die("Access Denied"); }

	include("formchecker.php");
	
$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
   <tr>
      <td class="contentfont"><b>
         <?=$lang['goto']?>
         : </b><a href="http://test.sbe.ee/membersarea.php?page=marketizer">Review</a> | 
	       <a href="http://test.sbe.ee/membersarea.php?page=clients">Clients</a> | 
               <a href="http://test.sbe.ee/membersarea.php?page=popularity">Popularity</a> |	
               <a href="http://test.sbe.ee/membersarea.php?page=sellings">Sellings</a> |	
               </td>
   </tr>
</table>

<table>
 <tr>
  <td align="center">Client</td>
  <td align="center">From</td>
  <td align="center">Items bought</td>
  <td align="center">Amount</td>
 </tr>
<?
$TotalSolds=getsqlfield("SELECT COUNT(amount) AS 'AMOUNT1' FROM probid_winners WHERE sellerid='".$_SESSION['memberid']."'","AMOUNT1");
$TotalClients=getsqlfield("SELECT COUNT(DISTINCT buyerid) AS 'CLIENTS' FROM probid_winners WHERE sellerid='".$_SESSION['memberid']."'","CLIENTS");
$TotalAmount=getsqlfield("SELECT SUM(amount) AS 'AMOUNT2' FROM probid_winners WHERE sellerid='".$_SESSION['memberid']."'","AMOUNT2");

$ClientsQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT buyerid FROM probid_winners WHERE sellerid='".$userDetails['id']."'");
while ($ClientsReview = mysqli_fetch_array($ClientsQuery)){
/*echo $userDetails['id'];*/
	
echo "<tr class='";
echo (($count++)%2==0) ? "c2":"c3";
echo "'><td>".getsqlfield("SELECT username AS 'USER' FROM probid_users WHERE id='".$ClientsReview['buyerid']."'","USER")."</td>";
echo "<td>".date('d.m.Y - H:i:s',getsqlfield("SELECT MIN(purchase_date) AS 'START' FROM probid_winners WHERE buyerid='".$ClientsReview['buyerid']."'","START"))."</td>";
echo "<td align='right'>".getsqlfield("SELECT COUNT(amount) AS 'AMOUNT1' FROM probid_winners WHERE buyerid='".$ClientsReview['buyerid']."' AND sellerid='".$_SESSION['memberid']."'","AMOUNT1")."</td>";
echo "<td align='right'>".getsqlfield("SELECT SUM(amount) AS 'AMOUNT2' FROM probid_winners WHERE buyerid='".$ClientsReview['buyerid']."' AND sellerid='".$_SESSION['memberid']."'","AMOUNT2")."</td></tr>";

}
echo "</table><br>Totally sold items: ".$TotalSolds."<B> How to increase solds?</B>";
echo "<br>Totally clients: ".$TotalClients."<B> How to increase number of clients?</B>";
echo "<br>Average solds number per client: ".$TotalSolds/$TotalClients."<B> How to increase solds number per client?</B>";
echo "<br>Total amount of solds: ".$TotalAmount."<B> How to increase amount of solds?</B>";
echo "<br>Average amount per client: ".$TotalAmount/$TotalClients."<B> How to increase average solds amount per client?</B>";
} else { echo "$lang[err_relogin]"; } ?>
