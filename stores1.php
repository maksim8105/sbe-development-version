<?
## v5.24 -> apr. 05, 2006
session_start();
include_once ("config/config.php");

if ($setts['stores_enabled']!="Y") {
	echo "<script>document.location.href='index.php'</script>";
} else { 

include ("themes/".$setts['default_theme']."/header.php");

header5("$lang[stores_directory]"); 

### first we sort the stores.
(string) $cntUser = NULL;
$getStoreUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT count(a.id) AS c_id, u.id AS u_id FROM probid_auctions a, probid_users u WHERE 
a.active=1 AND a.closed=0 AND a.deleted!=1 AND a.ownerid=u.id AND u.active=1 AND u.aboutpage_type=2 AND a.listin!='auction' GROUP BY u.id") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
while ($storeUsers=mysqli_fetch_array($getStoreUsers)) {
	$runQuery[$stcnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET nb_items = '".$storeUsers['c_id']."' WHERE id= '".$storeUsers['u_id']."'");
	## we save the user list in a variable
	$cntUser .= $storeUsers['u_id']." ";
}

$cntUser = @eregi_replace(" ",",",trim($cntUser));

$resetNbItemsQuery = (!empty($cntUser)) ? " AND id NOT IN (".$cntUser.") " : "";

$resetNbItems = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET nb_items='0' WHERE active=1 AND aboutpage_type=2 ".$resetNbItemsQuery);

if ($_GET['start'] == "") $start = 0;
else $start = $_GET['start'];
$limit = 20;

if ($_GET['search']=="yes"&&trim($_GET['username'])!="") {
	$addQuery = " AND username LIKE '%".$_GET['username']."%'";
}	
$additionalVars = "&search=".$_GET['search']."&username=".$_GET['username'];

$getStores = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, shop_logo, username, store_name, aboutmepage, nb_items FROM probid_users WHERE 
active='1' AND store_active='1' AND aboutpage_type='2'".$addQuery." ORDER BY nb_items DESC LIMIT ".$start.",".$limit) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
$totalStores = getSqlNumber("SELECT id FROM probid_users WHERE 
active='1' AND store_active='1' AND aboutpage_type='2'".$addQuery);
$isStores = mysqli_num_rows($getStores);
if ($isStores>0) { ?> 
<br>
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="border"> 
  <tr class="c1">
    <td align="center" width="115"><?=$lang[store_logo];?></td>
    <td width="150"><?=$lang[storename];?></td>
    <td width="100"><?=$lang[owner];?></td>
    <td><?=$lang[desc];?></td>
    <td width="110" align="center"><?=$lang[items_listed];?></td>
  </tr>
  <? while ($store=mysqli_fetch_array($getStores)) { ?>
  <tr class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>">
    <td align="center"><span class="contentfont"><a href="<?=processLink('shop', array('store' => $store['store_name'], 'userid' => $store['id'])); ?>"><img src="<? echo (($store['shop_logo']!="")?"makethumb.php?pic=".$store['shop_logo']."&w=80&sq=Y":"themes/".$setts['default_theme']."/img/system/noimg.gif");?>" border="0" alt="<?=$store['username'];?>"></a></span></td>
    <td class="contentfont"><a href="<?=processLink('shop', array('store' => $store['store_name'], 'userid' => $store['id'])); ?>"><strong><? echo $store['store_name']; ?></strong></a></td>
    <td class="contentfont"><a href="<?=processLink('shop', array('store' => $store['store_name'], 'userid' => $store['id'])); ?>"><strong><? echo $store['username']; ?></strong></a></td>
    <td><? echo substr(strip_tags(addSpecialChars($store['aboutmepage']), '<br>'),0,110)."..."; ?></td> 
    <td align="center"><? echo $store['nb_items']; ?></td>
  </tr> 
  <? } ?>
	<tr>
		<td align="center" class="contentfont c4" colspan="5"><? paginate($start,$limit,$totalStores,"stores.php",$additionalVars); ?></td>
	</tr>
</table> 
<? 	}
	else echo "<p align=\"center\">$lang[nostores]</p>";
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
