<?php

// Подключаем базовый класс Pangalink'а
include_once 'Pangalink.class.php';

$pangalink_return_url = $path . 'pangalink_return.php';

/**
 * Hansapank
 */
function hansaPangalinkForm( $customerId, $paymentAmount, $description, $mTable )
{
	global $lang, $setts, $path, $pangalink_return_url;

	// Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
	$Pangalink =& new Pangalink( 'config/pangalink.ini', true );

	if ( ! $ConcretePangalink = $Pangalink->initializePangalink( 'HP' ) )
	{
		return false;
	}

	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"hansa_88x31.gif\"></td>\n";
	### the description will go below
    echo "		<td class=\"paytable2\" width=\"100%\">Hansapank</td>\n";
	### the payment gateway from will go below

	// Сообщаем пангалинку, чтобы он вернул не ссылку, а поля для POST формы
	$ConcretePangalink->setMethodPost();
	$description = $ConcretePangalink->encodeString( $description );

	$pangalink_fields = $ConcretePangalink->simpleCreatePangalink( $customerId . 'X' . $mTable, $paymentAmount, $description, $pangalink_return_url );

	echo "		<form action=\"" . $ConcretePangalink->get( 'general', 'BILLING_URL' ) . "\" method=\"post\">\n";
   echo "		<td class=\"paytable3\">\n";

   foreach ( $pangalink_fields as $field_name => $field_value )
   {
   	echo "		 <input type=\"hidden\" name=\"$field_name\" value=\"$field_value\">\n";
   }

	echo "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\">\n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
	echo "<br/>";
}

/**
 * Krediidipank
 */
function krepPangalinkForm( $customerId, $paymentAmount, $description, $mTable )
{
	global $lang, $setts, $path, $pangalink_return_url;

	// Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
	$Pangalink =& new Pangalink( 'config/pangalink.ini', true );

	if ( ! $hansa = $Pangalink->initializePangalink( 'KREP' ) )
	{
		return false;
	}

	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"http://www.krediidipank.ee/ari/arveldus/pangalink/i-pank88x31.gif\"></td>\n";
	### the description will go below
    echo "		<td class=\"paytable2\" width=\"100%\">Krediidipank</td>\n";
	### the payment gateway from will go below

	$hansa->setMethodPost();
	$description = $ConcretePangalink->encodeString( $description );

	$pangalink_fields = $hansa->simpleCreatePangalink( $customerId . 'X' . $mTable, $paymentAmount, $description, $pangalink_return_url );

	echo "		<form action=\"" . $hansa->get( 'general', 'BILLING_URL' ) . "\" method=\"post\">\n";
   echo "		<td class=\"paytable3\">\n";

   foreach ( $pangalink_fields as $field_name => $field_value )
   {
   	echo "		 <input type=\"hidden\" name=\"$field_name\" value=\"$field_value\"> 	\n";
   }

	echo "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

// SEB Uhispank
function sebPangalinkForm( $customerId, $paymentAmount, $description, $mTable )
{
	global $lang, $setts, $path, $pangalink_return_url;

	// Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
	$Pangalink =& new Pangalink( 'config/pangalink.ini', true );

	if ( ! $ConcretePangalink = $Pangalink->initializePangalink( 'EYP' ) )
	{
		return false;
	}

	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"http://www.seb.ee/image/un_88x31.gif\"></td>\n";
	### the description will go below
    echo "		<td class=\"paytable2\" width=\"100%\">SEB Uhispank</td>\n";
	### the payment gateway from will go below

	$ConcretePangalink->setMethodPost();
	$description = $ConcretePangalink->encodeString( $description );

	$pangalink_fields = $ConcretePangalink->simpleCreatePangalink( $customerId . 'X' . $mTable, $paymentAmount, $description, $pangalink_return_url );

	echo "		<form action=\"" . $ConcretePangalink->get( 'general', 'BILLING_URL' ) . "\" method=\"post\">\n";
   echo "		<td class=\"paytable3\">\n";

   foreach ( $pangalink_fields as $field_name => $field_value )
   {
   	echo "		 <input type=\"hidden\" name=\"$field_name\" value=\"$field_value\">\n";
   }

	echo "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\">\n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
	echo "<br/>";
}

// Sampopank
function sampoPangalinkForm( $customerId, $paymentAmount, $description, $mTable )
{
}

?>