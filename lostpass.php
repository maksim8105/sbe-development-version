<?
## v5.22 -> oct. 26, 2005
session_start();
include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

header5("$lang[retrievepass]");

if (isset($_POST['retrieveok'])) {
	$user = getSqlRow("SELECT * FROM probid_users WHERE username='".$_POST['usern']."' AND email='".$_POST['emailaddr']."'");
	$isUser = getSqlNumber("SELECT * FROM probid_users WHERE username='".$_POST['usern']."' AND email='".$_POST['emailaddr']."'");
	if ($isUser>0) {
		// reset password
		$newPassword = substr(md5(rand(0,100000)),0,8);
		$resetPass = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		password ='".md5($newPassword)."' WHERE id='".$user['id']."'");
		$userId=$user['id'];
		include("mails/retrievepass.php");

		echo "<br> \n";
		echo "<table width=\"600\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" align=\"center\" class=border> \n";
  		echo "	<tr class=\"c1\" height=\"15\"> \n";
    	echo "		<td colspan=4>$lang[passresetnemailed]</td> \n";
  		echo "	</tr> \n";
  		echo "	<tr class=\"c3\"> \n";
    	echo "		<td class=contentfont>$lang[lostpassemsent1]<br><br> \n";
		echo "		<center><strong><u>$lang[lostpassemsent2]</u></strong></center><br><br> \n";
		echo "		$lang[lostpassemsent3]</td> \n";
	  	echo "	</tr> \n";
		echo "</table><br><br> \n";
	} else {
		echo "<br> \n";
		echo "<table width=\"600\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" align=\"center\" class=border> \n";
  		echo "	<tr class=\"c1\" height=\"15\"> \n";
    	echo "		<td colspan=4>$lang[lostpasserror1]</td> \n";
  		echo "	</tr> \n";
  		echo "	<tr class=\"c3\"> \n";
    	echo "		<td class=contentfont>$lang[lostpasserror2]</td> \n";
  		echo "	</tr> \n";
		echo "</table> \n";
	}
} else if (isset($_POST['retrievedone'])) {
	$user = getSqlRow("SELECT * FROM probid_users WHERE email='".$_POST['emailaddr']."'");
	$isUser = getSqlNumber("SELECT * FROM probid_users WHERE email='".$_POST['emailaddr']."'");
	if ($isUser>0) {
		$userId=$user['id'];
		include("mails/retrieveusername.php");

		echo "<br> \n";
		echo "<table width=\"600\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" align=\"center\" class=border> \n";
  		echo "	<tr class=\"c1\" height=\"15\"> \n";
    	echo "		<td colspan=4>$lang[usernretr1]</td> \n";
  		echo "	</tr> \n";
  		echo "	<tr class=\"c3\"> \n";
    	echo "		<td class=contentfont>$lang[usernretr2]<br> \n";
      	echo "		<br><center><strong><u>$lang[usernretr3]</u></strong></center> \n";
      	echo "		$lang[usernretr4]</td> \n";
  		echo "	</tr> \n";
		echo "</table> \n";
	} else { 
		echo "<br> \n";
		echo "<table width=\"600\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" align=\"center\" class=border> \n";
  		echo "	<tr class=\"c1\" height=\"15\"> \n";
		echo "		<td colspan=4>$lang[usernretrerr1]</td> \n";
  		echo "	</tr> \n";
  		echo "	<tr class=\"c3\"> \n";
    	echo "		<td class=contentfont>$lang[usernretrerr2]</td> \n";
  		echo "	</tr> \n";
		echo "</table> \n";
	}
} else { ?>
<form name="needpass" action="lostpass.php" method="post">
  <BR>
  <table width="500" border="0" cellpadding="3" cellspacing="1" align="center" class=border>
    <tr class="c1" height="15">
      <td colspan=4><?=$lang[retrieve_msg1];?></td>
    </tr>
    <tr class="c3">
      <td width="50%" align="right"><?=$lang[retrieve_email]?>
        :</td>
      <td width="50%" ><input name="emailaddr" type="text" id="emailaddr" size="30"></td>
    </tr>
    <tr class="c2">
      <td width="50%" align="right"><?=$lang[retrieve_user]?>
        :</td>
      <td width="50%" ><input name="usern" type="text" id="usern" size="30"></td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="c4"><input name="retrieveok" type="submit" id="retrieveok" value="<?=$lang[retrieve_button]?>"></td>
    </tr>
  </table>
</form>
<form name="needusername" action="lostpass.php" method="post">
  <BR>
  <table width="500" border="0" cellpadding="3" cellspacing="1" align="center" class=border>
    <tr class="c1" height="15">
      <td colspan=4><?=$lang[retrieve_msg2];?></td>
    </tr>
    <tr class="c3">
      <td width="50%" align="right"><?=$lang[retrieve_email]?>
        :</td>
      <td width="50%" ><input name="emailaddr" type="text" id="emailaddr" size="30"></td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="c4"><input name="retrievedone" type="submit" id="retrievedone" value="<?=$lang[retrieve_btn2];?>"></td>
    </tr>
  </table>
</form>
<? }
include ("themes/".$setts['default_theme']."/footer.php"); ?>