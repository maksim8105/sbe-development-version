<? 
## v5.25 -> jul. 06, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($_SESSION['membersarea']=="Active") { 
 ?>
<script language="JavaScript">
function submitform(theform) {
	theform.step.value = "step1";
	theform.curchange.value = "yes";
	theform.submit();
}
</script>

 <table width="100%" border="0" cellpadding="2" cellspacing="2"> 
  <tr class="contentfont" align="center"> 
     <td class="c4" width="20%"><?=$lang[sellstep1];?></td> 
     <td class="c1" width="20%"><?=$lang[sellstep3];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep4];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep5];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep6];?></td> 
   </tr> 
</table> 
<br>
<input type="hidden" name="step" value="step11"> 
<input type="hidden" name="curchange" value="">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border"> 
  <tr> 
    <td colspan="2" class="c1"><b><?=$lang[itemdetails]?></b></td> 
  </tr> 
 
  <tr class="c2">
     <td align="right"><?=$lang[listing_type];?></td>
     <td><select name="listing_type" onChange="submitform(sistep1);">
	  		<option value="full" selected><?=$lang[full_listing];?></option>
	  		<option value="quick" <? echo ($_REQUEST['listing_type']=='quick') ? 'selected' : ''; ?>><?=$lang[quick_listing];?></option>
	  </select></td>
  </tr>
  <tr class="c3"> 
    <td align="right"><?=$lang[itemname]?></td> 
    <td><input name="name" type="text" class="contentfont" id="name" value="<?=$_REQUEST['name'];?>" size="50" maxlength="50"></td> 
  </tr> 
  <tr class="c2"> 
    <td align="right"><b><?=$lang[descr]?></b></td> 
    <td><textarea id="description_main" name="description_main" style="width: 400px; height: 200px; overflow: hidden;"><?=$_REQUEST['description'];?></textarea> 
      		<script> 
					var oEdit1 = new InnovaEditor("oEdit1");
					oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit1.height=300;
					oEdit1.REPLACE("description_main");//Specify the id of the textarea here
				</script></td> 
  </tr> 
  <tr> 
    <td colspan="2" class="c1"><b><?=$lang[maincat]?></b></td> 
  </tr> 

  <tr class="c2"> 
    <td align="right"><strong> 
      <?=$lang[cat]?> 
      </strong></td> 
    <td><? 
		$nav = "";
		$parent=$_REQUEST['category'];
	 	if($parent > 0) {
	 		$croot = $parent;
	 		$cntr = 0;
	 		while ($croot>0) {
	 			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT id,parent FROM probid_categories WHERE id='".$croot."' ") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				$crw = mysqli_fetch_array($sbcts);
	 			if($cntr == 0) {
	 				$nav = $c_lang[$crw['id']];
	 			} else {
	 				if($parent != $croot) {
	 					$nav = $c_lang[$crw['id']]." > ".$nav;
	 				}
	 			}
	 			$cntr++;
	 			$croot = $crw['parent'];
	 		}
	 	} 
		echo $nav; ?></td> 
  </tr> 
  <? if ($_REQUEST['addlcategory']) { ?> 
  <tr> 
    <td colspan="2" class="c1"><b><?=$lang[addlcat]?></b></td> 
  </tr> 

<tr class="c2"> 
    <td align="right">
		<strong><?=$lang[cat]?></strong>
	</td> 
    <td><? 
		$nav = "";
		$parent=$_REQUEST['addlcategory'];
	 	if($parent > 0) {
	 		$croot = $parent;
	 		$cntr = 0;
	 		while ($croot>0) {
	 			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT id,parent FROM probid_categories WHERE id='".$croot."' ") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				$crw = mysqli_fetch_array($sbcts);
	 			if($cntr == 0) {
	 				$nav = $c_lang[$crw['id']];
	 			} else {
	 				if($parent != $croot) {
	 					$nav = $c_lang[$crw['id']]." > ".$nav;
	 				}
	 			}
	 			$cntr++;
	 			$croot = $crw['parent'];
	 		}
	 	} 
		echo $nav; ?></select></td> 
  </tr> 
  <? } ?> 
  <? 
  $nbSV = getSqlNumber("SELECT id FROM probid_vouchers WHERE voucher_type='setup' AND active='1'");
  if ($_REQUEST['listin']!="store"&&$nbSV>0) { ?> 
  <tr> 
    <td colspan="2" class="c1"><b><?=$lang[enter_voucher_code]?></b></td> 
  </tr> 
  <tr class="c2"> 
    <td align="right"><strong> 
      <?=$lang[voucher_code]?> 
      </strong></td> 
    <td><input name="voucher_code" type="text" id="voucher_code" size=40 value="<?=$_REQUEST['voucher_code'];?>"><br><?=$lang[voucher_text]?></td> 
  </tr> 
  <? } ?> 
  <tr class="c4"> 
    <td colspan="2" align="center"> <input type="hidden" name="category" value="<?=$_REQUEST['category']?>"> 
      <input type="hidden" name="addlcategory" value="<?=$_REQUEST['addlcategory']?>"> 
		<? echo displayNavButtons(); ?></td> 
  </tr> 
</table> 
<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?> 
