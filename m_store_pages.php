<? 
## v5.24 -> apr. 06, 2006
if ($_SESSION['membersarea']=="Active"&&$_SESSION['is_seller']=="Y"&&$setts['stores_enabled']=="Y") { 

if ( !defined('INCLUDED') ) { die("Access Denied"); }

$noDisplay = FALSE;

$prefSeller = "N";
if ($setts['pref_sellers']=="Y") {
	$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
	WHERE id='".$_SESSION['memberid']."'","preferred_seller");
	$reduction = (100-$setts['pref_sellers_reduction'])/100;
	$reduction = ($prefSeller=="Y") ? $reduction : 1;
} else $reduction = 1;

$vatExempted = "Y";
if ($setts['vat_rate']>0) $vatExempted = getSqlField("SELECT vat_exempted FROM probid_users
WHERE id='".$_SESSION['memberid']."'","vat_exempted");

include_once("formchecker.php"); 

if ($action=="store_pages_save") {
	$updateAboutSetts = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	store_about='".remSpecialChars($_POST['store_about'])."',
	store_specials='".remSpecialChars($_POST['store_specials'])."',   
	store_shippinginfo='".remSpecialChars($_POST['store_shippinginfo'])."',   
	store_policies='".remSpecialChars($_POST['store_policies'])."',
	store_featured_items='".intval($_POST['store_featured_items'])."',
	store_endingsoon_items='".intval($_POST['store_endingsoon_items'])."',
	store_recentlylisted_items='".intval($_POST['store_recentlylisted_items'])."' 
	WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
}

$shopDetails=getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'"); 

if (!$noDisplay) {
?>

<form action="membersarea.php?page=store_pages" method="post" enctype="multipart/form-data">
   <input type="hidden" name="oldlogo" value="<?=$shopDetails['shop_logo'];?>">
   <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border">
      <tr>
         <td colspan="2" align="center" class="c1"><?=$lang[store_setup_page]?>
            -
            <?=$lang[storepages]?></td>
      </tr>
      <? $storeInfo = storeAccountType($shopDetails['store_account_type']); ?>
      <tr class="c2">
         <td colspan="2" class="contentfont"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
               <tr>
                  <td class="contentfont"><a href="membersarea.php?page=store">
                     <?=$lang[storemainsetts]?></a> | <a href="membersarea.php?page=store_pages">
                     <?=$lang[storepages]?></a> <? /*| <a href="membersarea.php?page=cats_management">
                     <?=$lang[catsmanagement]?>
                     </a>*/?></td>
               </tr>
            </table></td>
      </tr>
      <tr class="c5">
         <td><img src="images/pixel.gif" width="1" height="1"></td>
         <td><img src="images/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr class="c3">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_about]?>
            </b></td>
         <td class="contentfont"><textarea name="store_about" cols="45" rows="10" id="store_about"><? echo addSpecialChars($shopDetails['store_about']);?></textarea>
            <script> 
					var oEdit1 = new InnovaEditor("oEdit1");
					oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit1.height=300;
					oEdit1.REPLACE("store_about");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c3">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_featured_items]?>
            </b></td>
         <td class="contentfont"><input name="store_featured_items" type="text" value="<?=$shopDetails['store_featured_items'];?>" size="6" maxlength="2" />
				<br /><?=$lang[storefeatnote];?></td>
      </tr>
      <tr class="c3">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_endingsoon_items]?>
            </b></td>
         <td class="contentfont"><input name="store_endingsoon_items" type="text" value="<?=$shopDetails['store_endingsoon_items'];?>" size="6" maxlength="2" />
				<br /><?=$lang[storeendsoonnote];?></td>
      </tr>
      <tr class="c3">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_recentlylisted_items]?>
            </b></td>
         <td class="contentfont"><input name="store_recentlylisted_items" type="text" value="<?=$shopDetails['store_recentlylisted_items'];?>" size="6" maxlength="2" />
				<br /><?=$lang[storereclistnote];?></td>
      </tr>
      <tr class="c5">
         <td><img src="images/pixel.gif" width="1" height="1"></td>
         <td><img src="images/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr class="c2">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_specials]?>
            </b></td>
         <td class="contentfont"><textarea name="store_specials" cols="45" rows="10" id="store_specials"><? echo addSpecialChars($shopDetails['store_specials']);?></textarea>
            <script> 
					var oEdit2 = new InnovaEditor("oEdit2");
					oEdit2.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit2.height=300;
					oEdit2.REPLACE("store_specials");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c5">
         <td><img src="images/pixel.gif" width="1" height="1"></td>
         <td><img src="images/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr class="c3">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_shippinginfo]?>
            </b></td>
         <td class="contentfont"><textarea name="store_shippinginfo" cols="45" rows="10" id="store_shippinginfo"><? echo addSpecialChars($shopDetails['store_shippinginfo']);?></textarea>
            <script> 
					var oEdit3 = new InnovaEditor("oEdit3");
					oEdit3.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit3.height=300;
					oEdit3.REPLACE("store_shippinginfo");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c5">
         <td><img src="images/pixel.gif" width="1" height="1"></td>
         <td><img src="images/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr class="c2">
         <td width="150" align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_policies]?>
            </b></td>
         <td class="contentfont"><textarea name="store_policies" cols="45" rows="10" id="store_policies"><? echo addSpecialChars($shopDetails['store_policies']);?></textarea>
            <script> 
					var oEdit4 = new InnovaEditor("oEdit4");
					oEdit4.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit4.height=300;
					oEdit4.REPLACE("store_policies");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c4">
         <td colspan="2" align="center" class="contentfont"><input type="submit" name="storepagessaveok" value="<?=$lang[savesetts]?>"></td>
      </tr>
   </table>
</form>
<br>
<? 
	} ## end of noDisplay check
} else if ($_SESSION['is_seller']!="Y") {
	echo $lang[seller_error]; 
} else { 
	echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; 
} ?>
