<?php
$allCategories = array(
"1436"=>"955",
);

global $data;

if ($_POST){

	$data = &$_POST;

} else {

	$data = &$_GET;

}

// FUNCTIONS

function removeEmptyKeys($array) {

  foreach ($array as $key => $value) {
    if (is_null($value)) {
      unset($array[$key]);
    } 
  }
  return $array;
}
 
 function catList($name="item_category",$selected) {
 
 global $c_lang;
 
 
 
  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_categories ORDER BY id ASC");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $catID[] = $row; 
  }
  

 
 foreach ($catID as $cat) {
 
    $i++;
 
    $cat_string.= $cat["id"].($i<count($catID)?",":"");
 
 }
 

 $i=0;
  
  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT parent,id FROM probid_categories WHERE ID IN(".$cat_string.") AND parent!=0 ORDER BY id ASC LIMIT 10");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $subcatList[] = $row; 
  }  
  
  $catSelect = "<select name='".$name."'>";
  $catSelect.="<option value='0'>-</option>";
  
  foreach ($subcatList as $item) {
  

      $catSelect.="<option ".(($selected==$item["id"])?'selected':'')." value='".$item["id"]."'>".$c_lang[$item["parent"]].": ".$c_lang[$item["id"]]."</option>";
  }
  $catSelect.="</select>";
  
  return $catSelect;

 }
 
 function auctionType($selected) {
 
 $arrTypes = array(
 0=>array("type"=>"standard","label"=>"Standard Auction"),
 1=>array("type"=>"dutch","label"=>"Dutch Auction"),
 
 );
 
 $sel = "<select name='item_auctype]'>";
 
 foreach ($arrTypes as $option) {
 
  $sel.="<option value='".$option["type"]."'>".$option["label"];
 
 }

  $sel .= "</select>";
  
  return $sel;

 }
 
 // CURRENCY LIST
 
 function currList($selected,$name,$id) {
 
 $result=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_currencies");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $curr[] = $row; 
  }  
  
  $sel = "<select name='".$name."' id='".$id."'>";
  foreach ($curr as $item) {
    $sel .= "<option ".(($selected==$item["symbol"])?'selected':'')." value='".$item["symbol"]."'>".$item["symbol"];
  }
  $sel .="</select>";
  return $sel;
  
 }
 
 // DURATION LIST
 
 function durationList($name,$selected,$id) {
 
 $arrPeriod = array(
 0=>array("value"=>"1","label"=>"1 day"),
 1=>array("value"=>"3","label"=>"3 days"),
 2=>array("value"=>"7","label"=>"7 days"),
 
 );
 
 $sel = "<select name='".$name."' id='".$id."'>";
 
 foreach ($arrPeriod as $option) {
 
  $sel.="<option ".(($selected==$option["value"])?'selected':'')." value='".$option["value"]."'>".$option["label"];
 
 }

  $sel .= "</select>";
  
  return $sel;

  
 }
 
function timeBox($sel_h,$sel_m,$name_h,$name_m,$id_hour,$id_min) {

$hour = '<select name="'.$name_h.'" class="time" id="'.$id_hour.'">';



for ($h=0;$h<24 ;$h++) {



  if ($h<10) $h='0'.$h;



  $hour.= '<option '.($h==$sel_h?"selected":"").' value="'.$h.'">'.$h;

	

}

$hour.='</select>';
$min = '<select name="'.$name_m.'" class="time" id="'.$id_min.'">';

for ($m=0;$m<4 ;$m++) {

  if ($m==0) $zero="0";

  $min.= '<option '.(($m*15).$zero==$sel_m?"selected":"").' value="'.($m*15).$zero.'">'.($m*15).$zero;
  
  $zero="";
	

}
$min.='</select>';



$box = $hour.':'.$min;

return $box;

}

function getExtension($filename) {

	$filename = strtolower($filename) ;

	$exts = split("[/\\.]", $filename) ;

	$n = count($exts)-1;

	$exts = $exts[$n];

	return strtolower($exts);

}




function deletePhotos($downloads,$temp) {

        $arrPhotos = array();
        $i=0;

        
        foreach ($temp as $tmp) {
          
          $arrPhotos[$i]["id"] = $tmp["id"];
          $arrFiles = explode(";",$tmp["images"]);
          $arrPhotos[$i]["images"] = $arrFiles;
          $i++;
        } 
        
        
        
        foreach ($arrPhotos as $file) {
            foreach ($file["images"] as $target) {
            // delete images,thumbs
                @unlink("./images_temp/".$_SESSION["memberid"]."/".$file["id"]."/".$target);
                @unlink("./images_temp/".$_SESSION["memberid"]."/".$file["id"]."/thumb/".$target);
            }

        }
        foreach ($temp as $tmp) {
            @rmdir("./images_temp/".$_SESSION["memberid"]."/".$tmp["id"]."/thumb");
            @rmdir("./images_temp/".$_SESSION["memberid"]."/".$tmp["id"]);
        }
    
    

}


 // VALIDATOR
 
 function validateForm($data) {
 
 

 
 $errors = array();
 
 switch ($_POST["action"]) {
 
    case "insert":
     if (intval($data["item_category"])==0) $errors["category"]="category er";
     if (strlen($data["item_name"])<3) $errors["name"]="name";
     if (strlen($data["item_desc"])==0) $errors["desc"]="description";
     if (strlen($data["item_location"])==0) $errors["location"]="location";
     if (intval($data["item_quantity"])==0) $errors["quantity"]="quantity";
     if (intval($data["item_price"])==0) $errors["price"]="price";
    // if (intval($data["item_reserve_price"])==0) $errors["reserve_price"]="reserve_price";
    // if (intval($data["item_buynow"])==0) $errors["buynow"]="buynow";
        $d = substr($data["item_startdate"],0,2);
        $m = substr($data["item_startdate"],3,2);
        $y = substr($data["item_startdate"],6,4);
     if (checkdate($m,$d,$y)==0)  $errors["start_date"]= "start_date";
  
  $num = 5; // Number of pictures
  for ($i=1;$i<=$num;$i++) {
  
    if ($_FILES["item_pic".$i]["error"]==0) {
    
      $filename[$i] = stripslashes($_FILES["item_pic".$i]["name"]);
      $extension[$i] = getExtension($filename[$i]);
      $arrFormats = array("jpg","jpeg","gif");
      if (!in_array($extension[$i],$arrFormats)) {
          $errors["pic_ext".$i]= "Nedopustimij format #".$i." kartinki";
      }
      // 1 Mb img
      if ($_FILES["item_pic".$i]["size"]>1048576) {
          $errors["pic_size".$i]= "Nedopustimij razmer #".$i." kartinki";
      }

    
    }
    
    

  }
  
   break;
   
   case "update":

      foreach ($data["downloads"] as $id) {

       if (intval($data["item_category_".$id])==0) {
          $errors["category"]="category";
          $errors["fields"].="item_category_".$id.";";
       }
       if (strlen($data["item_name_".$id])<3) { $errors["name"]="name"; $errors["fields"].="item_name_".$id.";";}
       if (strlen($data["item_desc_".$id])==0) { $errors["desc"]="description"; $errors["fields"].="item_desc_".$id.";";}
       if (strlen($data["item_location_".$id])==0) { $errors["location"]="location"; $errors["fields"].="item_location_".$id.";";}
       if (intval($data["item_quantity_".$id])==0) { $errors["quantity"]="quantity"; $errors["fields"].="item_quantity_".$id.";";}
       if (intval($data["item_price_".$id])==0) { $errors["price"]="price"; $errors["fields"].="item_price_".$id.";";}
    //   if (intval($data["item_reserve_price_".$id])==0) { $errors["reserve_price"]="reserve_price"; $errors["fields"].="item_reserve_price_".$id.";";}
    //   if (intval($data["item_buynow_".$id])==0) { $errors["buynow"]="buynow"; $errors["fields"].="item_buynow_".$id.";";}
        $d = substr($data["item_startdate_".$id],0,2);
        $m = substr($data["item_startdate_".$id],3,2);
        $y = substr($data["item_startdate_".$id],6,4);
     if (checkdate($m,$d,$y)==0)  { $errors["start_date"]= "start_date"; $errors["fields"].="item_start_date_".$id.";";}
     
     $num = 5;
     
      for ($i=1;$i<=$num;$i++) {
      
      
        if ($_FILES["item_pic".$i."_".$id]["error"]=="0") {
        
          
        
          $filename[$i] = stripslashes($_FILES["item_pic".$i."_".$id]["name"]);
          $extension[$i] = getExtension($filename[$i]);
          $arrFormats = array("jpg","jpeg","gif");
          if (!in_array($extension[$i],$arrFormats)) {
              $errors["pic_ext".$i."_".$id]= "Nedopustimij format #".$i." kartinki";
              $errors["fields"].="item_pic".$i."_".$id.";";
          }
          // 1 Mb img
          if ($_FILES["item_pic".$i."_".$id]["size"]>1048576) {
              $errors["pic_size".$i."_".$id]= "Nedopustimij razmer #".$i." kartinki";
              $errors["fields"].="item_pic".$i."_".$id.";";
          }
    
        
        }
        
        
    
      }
    }
    break;
    
    case "insert_tpl":
    
      if (strlen($data["tpl_name"])<3) $errors["tpl_name"]="template name";
      $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT count(name) FROM probid_auctions_downloads_tpl WHERE name='[".$data["tpl_name"]."]' AND ownerid=".$_SESSION["memberid"]." ");
      $cnt = mysqli_fetch_row($result);
      if ($cnt[0]!=0)  $errors["tpl_name_exist"]="Exist tpl name";
    break;
    case "save_tpl":
      $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT count(name) FROM probid_auctions_downloads_tpl WHERE name='[".$data["tpl_name"].$data["tpl"]."]' AND ownerid=".$_SESSION["memberid"]." ");
      $cnt = mysqli_fetch_row($result);
      if ($cnt[0]!=0)  $errors["tpl_name_exist"]="Exist tpl name";
    
    break;

 
 


 }
 return $errors;
} 
 
 function ListSelect($array,$selected) {

 $sel = "<select id='lid' name='list_id' width='30' onchange='submit();'>";
 
 foreach ($array as $option) {
 
  $sel.="<option ".($option["id"]==$selected?'selected':'')." value='".$option["id"]."'>".$option["name"];
 
 }

  $sel .= "</select>";
  
  return $sel;
    
 
 }

function paging($arg,$recordsPerPage,$count) {
  global $_POST;

    $pageNum = 1;
    if(isset($_POST['p'])) {
      $pageNum = $_POST['p'];
      settype($pageNum, 'integer');
    }
    $offset = ($pageNum - 1) * $recordsPerPage;
    
    if ($offset>=0) {
      $filter = " LIMIT ".$offset.",".$recordsPerPage."";
    }
    
    if ($count==1) {
      $filter="";
    }
    
	$result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * FROM probid_auctions_downloads WHERE owner_id=".$_SESSION["memberid"]." AND list_id=".$_POST["list_id"]." ORDER BY ID ASC ".$filter."");
    while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $items[] = $row; 
    } 

	return $items;

}

function link_bar($page, $count,$perpage) {
	global $data;


  if (!isset($page) || $page==0) $page = 1;
  
  $pages_count  = ceil($count / $perpage);


	for ($j = 1; $j <= $pages_count; $j++)
	{
		if ($j == $page) {
		$pages_bar.='<td class="selected">'.$j.'</td>';
		} else {
		$pages_bar.='<td class="pages"><a href="javascript:getPageContent('.$j.');" class="pages">'.$j.'</a></td>';
		}
		if ($j != $pages_count) $pages_bar.='<td></td>';
	}

    return $pages_bar;
}

function ListSel($array,$selected) {
	$sel = "<select  name='list_id' width='50' id='listid'>";
	foreach ($array as $option) {
	$sel.="<option ".($option["id"]==$selected?'selected':'')." value='".$option["id"]."'>".$option["name"];
	}
	$sel .= "</select>";
	return $sel; 
}
function just_clean($string)
{
$string = preg_replace('/\s/', '', $string);  
return $string;
}

function prepareCategory($data) {

}

function convertCategory($name) {
	global $allCategories;
	$id = str_replace("index.php?fuseaction=home.welcomeindex.php?fuseaction=listing.items&id=","",$name);	
	return $allCategories[$id];
	
}



?>
