<?php
## v5.25 -> jul. 10, 2006

// Set this to 1 to troubleshoot problems!
$log = 0;

session_start();

include_once ("config/config.php");

## overwrite $setts['payment gateway']
$setts['payment_gateway']="Nochex";

// Open logfile
if ($log) {
  	$fh = fopen("config/Nochex.log", "a");
  	fputs($fh, "\n------------\n");
}

function http_post($server, $port, $url, $vars) {
	// get urlencoded vesion of $vars array
	$urlencoded = "";
	foreach ($vars as $Index => $Value)
	$urlencoded .= urlencode($Index ) . "=" . urlencode($Value) . "&";
	$urlencoded = substr($urlencoded,0,-1);

	$headers = "POST $url HTTP/1.0\r\n"
	. "Content-Type: application/x-www-form-urlencoded\r\n"
	. "Content-Length: ". strlen($urlencoded) . "\r\n\r\n";

	$fp = fsockopen($server, $port, $errno, $errstr, 10);
	if ($log) {
		if (!$fp) fputs($fh,"ERROR: fsockopen failed.\r\nError no: $errno - $errstr\n");
		else fputs($fh,"Fsockopen success.\n");
	}

	fputs($fp, $headers);
	fputs($fp, $urlencoded);

	$ret = "";
	while (!feof($fp)) $ret .= fgets($fp, 1024);

	fclose($fp);
	return $ret;
}

$res = http_post("www.nochex.com", 80, "/nochex.dll/apc/apc", $_POST);

// assign posted variables to local variables
$receiver_email = $_POST['to_email'];
$item_number = $_POST['item_number'];
$invoice = $_POST['invoice'];
$payment_status = $_POST['status'];
$payment_gross = $_POST['amount'];
$txn_id = $_POST['transaction_id'];
$payer_email = $_POST['from_email'];
list ($custom, $table, $transaction_code) = explode("TBL",$_POST['order_id']);
$payment_date = $_POST['transaction_date'];
$pending_reason = $_POST['pending_reason'];

// Create postback request
/*$req = '';
foreach ($_POST as $key => $value) {
	$value = urlencode($value);
	if ($req != '') $req .= "&";
	$req .= "$key=$value";
}

// post back to Nochex system to validate
$header .= "POST /nochex.dll/apc/apc HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= 'Content-Length: ' . strlen($req) . "\r\n\r\n";
$fp = fsockopen('ssl://www.nochex.com', 443, $errno, $errstr, 30);
*/
if ($table==1) $_SESSION['table_to_modify']="probid_users";
if ($table==2) $_SESSION['table_to_modify']="probid_auctions";
if ($table==3) $_SESSION['table_to_modify']="probid_winners";
if ($table==4) $theoption=2;

if ($table==7) $_SESSION['table_to_modify']="probid_wanted_ads";

$auth = 0;
if ($log) fputs($fh, ">   $res");
		
if (strstr($res, 'AUTHORISED')) {
	if (trim($payment_status) == "live") {
		$isTxnId = FALSE;
		$getTxn = getSqlNumber("SELECT * FROM probid_txn_ids WHERE txnid='".$txn_id."' AND processor='".$setts['payment_gateway']."'");
		if ($getTxn) $isTxnId = TRUE;
				
		## only do all this if there is no txn yet.
		if (!$isTxnId) { 
			$addTxn = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_txn_ids (txnid, processor) VALUES ('".$txn_id."','".$setts['payment_gateway']."')");

			if ($log) fputs($fh, "\n* Nochex Payment Accepted.\n");
			$auth = 1;
	
			if ($table == 100) {
				//$pDetails = explode("_",$custom);
				//mysql_query("UPDATE probid_auctions SET	paidwithdirectpayment='1' WHERE id='".$pDetails[0]."'");
				/*
				mysql_query("UPDATE probid_winners SET flag_paid=1, directpayment_paid=1
				WHERE  buyerid='".$pDetails[1]."' AND auctionid='".$pDetails[0]."'");
				*/
				mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid=1, directpayment_paid=1 WHERE  id='".$custom."'");
			}
	
			if ($theoption == 2) {
				$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$custom."'","balance");
				$updatedBalance = $currentBalance - $payment_gross;
				if ($updatedBalance<=0) {
					$_SESSION['accsusp']=0;
				}
				## if user is suspended, activate the counters
				$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
				if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);
		
				$currentTime = time();
				$updateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				active='1', payment_status='confirmed', balance='".$updatedBalance."' WHERE id='".$custom."'");
				$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
				active='1' WHERE ownerid='".$custom."'");
				$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
				active='1' WHERE ownerid='".$custom."'");
				$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
				(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
				('".$custom."','".$lang[payment_fee]."','".$payment_gross."','".$currentTime."','".$updatedBalance."','payment','".$setts['payment_gateway']."')");
			} else {
				if ($table==3) {
					$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
					active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',
					txnid='".$txn_id."',
					paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' 
					WHERE auctionid='".$custom."'");
				} else if ($table==8) {
					$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
					store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					## now we submit the accounting details
					$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
					(userid, amountpaid, paymentdate, processor) VALUES
					('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				} else {
					## if we activate the users table, count all auctions
					if ($table==1) counterAddUser($custom);
	
					$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
					active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',
					paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' 
					WHERE id='".$custom."'");
					## if we activate the auctions table, count the auction
					if ($table==2) {
						$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
						if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
							addcatcount ($auctCat['category']);
							addcatcount ($auctCat['addlcategory']);
						}
					}
					if ($table==7) {
						$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
						if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
							addwantedcount ($wantedCat['category']);
							addwantedcount ($wantedCat['addlcategory']);
						}
					}
				}
			}
		}
		if ($log) fputs($fh, "\n* Nochex Payment Accepted (Live)\n");
	} else {
		if ($log) fputs($fh, "\n* Nochex Payment Accepted (Test Mode!)\n");
		$auth = 1;
	}
}

if (!$auth) {
  	if ($log) fputs($fh, "* Nochex Payment Declined.\n");
  	if ($_REQUEST['table']==3) $query6 = "UPDATE ".$_SESSION['table_to_modify']." SET active = '1',payment_status='INVALID' WHERE auctionid='".$custom."'";
  	else $query6 = "UPDATE ".$_SESSION['table_to_modify']." SET active=0,payment_status='INVALID' WHERE id='".$custom."'";
  	$setInvalidPayment = mysqli_query($GLOBALS["___mysqli_ston"], $query6) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
}
if (!$auth)
	echo "<script>document.location.href='".$_POST['cancelurl']."'</script>";	
else {
	countCategories();
	echo "<script>document.location.href='".$_POST['returnurl']."'</script>";	
}
?>