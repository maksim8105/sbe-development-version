<?
## v5.24 -> apr. 05, 2006
session_start();
include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/title.php");
include ("config/lang/$setts[default_lang]/site.lang");

if ($_SESSION['adminarea']!="Active") $addPattern = " AND deleted!=1";
else $addPattern = "";

$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_GET['id']."' AND active=1".$addPattern);
$isAuction = getSqlNumber("SELECT * FROM probid_auctions WHERE id='".$_GET['id']."' AND active=1".$addPattern);
if ($isAuction > 0) {
	$sellerDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$auctionDetails['ownerid']."'"); ?>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<table width="100%"  border="0" cellspacing="0" cellpadding="4" class="errormessage">
   <tr>
      <td class="contentfont"><?
	 	$nav = "";
	 	$nav2 = "";
		$parent=$auctionDetails['category'];
	 	if($parent > 0) {
	 		$croot = $parent;
	 		$cntr = 0;
	 		while ($croot>0) {
	 			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	 				
				$crw = mysqli_fetch_array($sbcts);
	 			if($cntr == 0) {
	 				$nav = $crw[name];
	 			} else {
	 				if($parent != $croot) {
	 					$nav = "<a href=\"".processLink('categories', array('category' => $crw['name'], 'parent' => $crw['id']))."\">$crw[name]</a> > $nav";
	 				}
	 			}
	 			$cntr++;
	 			$croot = $crw['parent'];
	 		}
	 	} 
		$parent=$auctionDetails['addlcategory'];
	 	if($parent > 0) {
	 		$croot = $parent;
	 		$cntr = 0;
	 		while ($croot>0) {
	 			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	 			
				$crw = mysqli_fetch_array($sbcts);
	 			if($cntr == 0) {
	 				$nav2 = $crw[name];
	 			} else {
	 				if($parent != $croot) {
	 					$nav2 = "<a href=\"".processLink('categories', array('category' => $crw['name'], 'parent' => $crw['id']))."\">$crw[name]</a> > $nav2";
	 				}
	 			}
	 			$cntr++;
	 			$croot = $crw['parent'];
	 		}
	 	} 
		if (strlen($nav) || strlen($nav2)) {
			echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td valign=\"top\"><b>$lang[itemlistedon]:</b></td><td width=\"7\">&nbsp;</td><td>$nav";
			if (strlen($nav2) && strlen($nav)) echo "<br>";
			echo "$nav2</td></tr></table>";
		} ?></td>
   </tr>
</table>
<? header1($auctionDetails['itemname']);?>
<table width="100%" border="0" cellpadding="6" cellspacing="0" class="errormessage">
   <tr>
      <td class="contentfont"><a href="#descr"><img src="themes/<?=$setts['default_theme'];?>/img/system/down.gif" align="absmiddle" border="0" hspace="5">
         <?=$lang[itemdescr]?>
         </a></td>
   </tr>
</table>
<!-- 3 cell table -->
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="3">
   <tr class="c2" valign="top">
      <? 
	if ($auctionDetails['picpath']!="") 
		echo 	"<td width=\"20%\" align=\"center\">								\n".
				"<table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"3\" class=\"border\">\n".
				"	<tr>															\n".
				"		<td height=\"110\" align=\"center\" class=\"c2\">			\n".
				"			<img src=\"makethumb.php?pic=".$auctionDetails['picpath']."&w=100\" width=\"100\">\n".
				"		</td>														\n".
				"	</tr>															\n".
				"	<tr class=c1>													\n".
				"		<td align=center>											\n".
				"			<a class=sell href=\"showpic.php?pic=".$auctionDetails['picpath']."\" target=\"_blank\"><b>$lang[largerpic]</b></a>\n".
				"		</td>														\n".
				"	</tr>															\n".
				"</table>															\n".
				"</td>";  
		$getpics=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$_GET['id']."'"); ?>
      <td width="50%"><!-- Start Table for item details -->
         <table width="100%" border="0" cellspacing="1" cellpadding="3">
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <tr valign="top" class="c3">
               <td nowrap><b>
                  <?=$lang[currbid]?>
                  :</b></td>
               <td><b><? echo displayAmount($auctionDetails['maxbid'],$auctionDetails['currency']);?></b></td>
            </tr>
            <tr valign="top" class="c2">
               <td nowrap><b>
                  <?=$lang[startbid]?>
                  :</b></td>
               <td><b><font class='redfont'><? echo displayAmount($auctionDetails['bidstart'],$auctionDetails['currency']);?></font></b></td>
            </tr>
            <? 
				if ($_SESSION['membersarea']=="Active") { 
					$currentBid = getSqlField("SELECT * FROM probid_bids WHERE 
					auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."' ORDER BY bidamount DESC","bidamount") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					$isCurrentBid = getSqlNumber("SELECT * FROM probid_bids WHERE auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."'");
					if ($isCurrentBid>0) { ?>
						<tr valign="top" class="c3">
							<td><b>
								<?=$lang[yourbid]?>
								:</b></td>
							<td><font class='greenfont'><b><? echo displayAmount($currentBid,$auctionDetails['currency']);?></b></font></td>
						</tr>
						<? }
				} ?>
            <tr valign="top" class="c2">
               <td><b>
                  <?=$lang[quant]?>
                  :</b></td>
               <td><b>
                  <?=$auctionDetails['quantity'];?>
                  </b></td>
            </tr>
            <tr valign="top" class="c3">
               <td nowrap><b>
                  <?=$lang[num_bids]?>
                  :</b></td>
               <td class="contentfont"><?=$auctionDetails['nrbids'];?>
                  <? 
		  	if ($auctionDetails['nrbids']>0) { 
            	echo "<a href=\"bidhistory.php?id=".$auctionDetails['id']."&name=".$auctionDetails['itemname']."&quantity=".$auctionDetails['quantity']."\">".$lang[viewhistory]."</a>";
			} ?></td>
            </tr>
            <tr valign="top" class="c2">
               <td><b>
                  <?=$lang[timeleft]?>
                  :</b></td>
               <td><? 
						$daysLeft = daysleft($auctionDetails['enddate'],$setts['date_format']);
						$timeLeft = timeleft($auctionDetails['enddate'],$setts['date_format']);
						echo ($daysLeft>0) ? $timeLeft : $lang[bidclosed]; ?>
               </td>
            </tr>
            <tr valign="top" class="c3">
               <td><b>
                  <?=$lang[location]?>
                  :</b></td>
               <td><? echo $auctionDetails['zip'].", ".$sellerDetails['city'].", ".$sellerDetails['state'];?></td>
            </tr>
            <tr valign="top" class="c2">
               <td><b>
                  <?=$lang[country]?>
                  :</b></td>
               <td><?=$auctionDetails['country'];?></td>
            </tr>
            <tr valign="top" class="c3">
               <td><b>
                  <?=$lang[started]?>
                  :</b></td>
               <td><? echo displaydatetime($auctionDetails['startdate'],$setts['date_format']);?></td>
            </tr>
            <tr valign="top" class="c2">
               <td><b>
                  <?=$lang[ends]?>
                  :</b></td>
               <td><? echo displaydatetime($auctionDetails['enddate'],$setts['date_format']);?></td>
            </tr>
            <tr valign="top" class="c3">
               <td></td>
               <td class="contentfont"><a href="itemwatch.php?id=<?=$auctionDetails['id'];?>&itemname=<?=$auctionDetails['itemname'];?>">
                  <?=$lang[watchthisitem]?>
                  </a></td>
            </tr>
            <tr class="c2">
               <td><b>
                  <?=$lang[status]?>
                  :</b></td>
               <td><? echo ($auctionDetails['closed']==0)?"<font class='greenfont'><b>".$lang[open]."</b></font>":"<font class='redfont'><b>".$lang[closed]."</b></font>";?></td>
            </tr>
            <tr class="c3">
               <td colspan="2" class="contentfont"><table border="0" width="100%" cellpadding="0" cellspacing="0">
                     <tr>
                        <?
								## if there is a reserve price, the BIN will exist until maxbid<resprice
								## otherwise it will exist only until a bid is placed
								$showBN = showBuyNow($auctionDetails['rp'],$auctionDetails['rpvalue'],$auctionDetails['maxbid'],$auctionDetails['nrbids']);
								if ($showBN) { 
									$bnValue=displayAmount($auctionDetails['bnvalue'],$auctionDetails['currency']);
									echo "<td align=\"center\">";
									if ($auctionDetails['ownerid']!=$_SESSION['memberid']&&$auctionDetails['closed']!=1) { 
										echo "<a href=\"buynow.php?id=".$auctionDetails['id']."\"><strong>".(($auctionDetails['bn']=="Y"&&$auctionDetails['active']==1&&$layout['act_buynow']==1)?"<img src=themes/".$setts['default_theme']."/img/system/buyitnow.gif border=0 alt=\"".$lang[buynow]."\"><br> ".$lang[bynow_for]." ".$bnValue."":"")."</strong></a>";
									} else {
										echo "<strong>".(($auctionDetails['bn']=="Y"&&$auctionDetails['active']==1&&$layout['act_buynow']==1)?"<img src=themes/".$setts['default_theme']."/img/system/buyitnow.gif border=0 alt=\"".$lang[buynow]."\"><br> ".$lang[bynow_for]." ".$bnValue."":"")."</strong>";
									}
									echo "</td>";
								} 
								echo "<td width=\"10\"></td>";
								/*if ($ADDONS['BidYourPrice']) {
									if ($row['ownerid']!=$memberid && $row['closed']!=1) echo "<td align=\"center\"><a href=bidyourprice.php?id=".$row['id']."><strong><img src=images/bidyourprice.gif border=0 alt=\"Bid Your Price\"><br>Bid Your Price</strong></a></td>";
								}*/?>
                     </tr>
                  </table></td>
            </tr>
            <tr class="c2">
               <td colspan="2" class="bluefont"><? 
						if ($auctionDetails['rp']=="Y") echo ($auctionDetails['rpvalue']>$auctionDetails['maxbid']) ? $lang[reservenotmet] : $lang[reservemet]; ?>
               </td>
            </tr>
            <tr class="c3">
               <td colspan="2" class="contentfont"><? 
						if ($setts['swap_items']==1&&$auctionDetails['ownerid']!=$_SESSION['memberid']&&$auctionDetails['closed']!=1&&$auctionDetails['isswap']=="Y") {
							echo "<a href=\"swapitems.php?id=".$auctionDetails['id']."\">".$lang[offerswap]."</a>";
						} ?>
               </td>
            </tr>
            <tr class="c2">
               <td><b>
                  <?=$lang[highbid]?>
                  :</b></td>
               <td><b>
                  <? 
						echo ($auctionDetails['rpvalue']<=$auctionDetails['maxbid'])?"<font class='greenfont'><b>":"";
						echo displayAmount($auctionDetails['maxbid'],$auctionDetails['currency']);
						echo ($auctionDetails['rpvalue']<=$auctionDetails['maxbid'])?"</b></font><br>":"";
								  
						if ($auctionDetails['maxbid']>0) {
							echo " - ";
							if ($auctionDetails['private']!="Y") {
								if ($auctionDetails['quantity'] == 1) {
									if ($auctionDetails['closed'] == 1) {
										$getAuctionWinner = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT buyerid FROM probid_winners WHERE auctionid='".$auctionDetails['id']."'");
										while ($auctionWinner = mysqli_fetch_array($getAuctionWinner)) {
											$bidderName = getSqlField("SELECT username FROM probid_users WHERE id='".$auctionWinner['buyerid']."'","username");
											echo $bidderName." <a href=\"viewfeedback.php?owner=".$auctionWinner['buyerid']."&auction=".$auctionDetails['id']."\">".getFeedback($auctionWinner['buyerid'])."</a>";
											$foundWinner = TRUE;
										}
									}
								}
								if (!$foundWinner) {
									$getBidderId=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE auctionid='".$auctionDetails['id']."' AND out=0 AND invalid=0");
									while ($bidder=mysqli_fetch_array($getBidderId)) {
										$bidderName = getSqlField("SELECT username FROM probid_users WHERE id='".$bidder['bidderid']."'","username");
										echo $bidderName." <a href=\"viewfeedback.php?owner=".$bidder['bidderid']."&auction=".$auctionDetails['id']."\">".getFeedback($bidder['bidderid'])."</a>";
									}
								}
							} else { 
								echo $lang[bidderhidden]; 
							}
						} ?>
                  </b></td>
            </tr>
         </table></td>
      <td width="30%"><table width="100%" border="0" cellspacing="1" cellpadding="3" class="contentfont">
            <tr>
               <td class="c1"><?=$lang[sellerinfo]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <tr>
               <td class="c2"><? echo $sellerDetails['username'];?> <a href="viewfeedback.php?owner=<?=$sellerDetails['id'];?>&auction=<?=$auctionDetails['id'];?>"> <? echo getFeedback($sellerDetails['id']);?> </a> </td>
            </tr>
            <tr class="c3">
               <td><? if ($sellerDetails['regdate']!=0) echo $lang[regsince] .date("M. j, Y",$sellerDetails['regdate'])." in the ".$sellerDetails['country']; ?>
               </td>
            </tr>
            <tr class="c2">
               <td><a href="viewfeedback.php?owner=<?=$auctionDetails['ownerid'];?>&auction=<?=$auctionDetails['id'];?>">
                  <?=$lang[viewfeedback1]?>
                  </a></td>
            </tr>
            <tr class="c3">
               <td><a href="askquestion.php?owner=<?=$auctionDetails['ownerid'];?>&auctionid=<?=$auctionDetails['id'];?>">
                  <?=$lang[askseller]?>
                  </a></td>
            </tr>
            <tr class="c2">
               <td><a href="otheritems.php?owner=<?=$auctionDetails['ownerid'];?>&nick=<?=$sellerDetails['username'];?>">
                  <?=$lang[otherselleritems]?>
                  </a></td>
            </tr>
            <tr class="c3">
               <td><a href="auctionfriend.php?owner=<?=$auctionDetails['ownerid'];?>&auctionid=<?=$auctionDetails['id'];?>">
                  <?=$lang[sendtofriend]?>
                  </a></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
<!-- Header Descriptions -->
<table width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr class="c1">
      <td nowrap><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c6">
      <td height="30" nowrap>&nbsp;
         <?=$imgarrowit;?>
         &nbsp;<b>
         <?=strtoupper($lang[descr])?>
         </b></td>
   </tr>
</table>
<!-- End Header Descriptions -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
   <tr> <a name="descr"></a>
      <td class="contentfont"><?	echo addSpecialChars($auctionDetails['description']); ?>
      </td>
   </tr>
   <tr>
      <td align="center"><table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr class="<? echo ($auctionDetails['picpath']!="")?"c2":""; ?>">
               <td width="100%" class="<? echo ($auctionDetails['picpath']!="")?"border":"";?>"><? 
						if ($auctionDetails['picpath']!="") { 
							echo "<table width=100% border=0 cellspacing=3 cellpadding=3>				\n".
								 "	<tr>																\n".
								 "		<td align=center valign=middle><img src=\"makethumb.php?pic=".$auctionDetails['picpath']."&w=400\"></td>\n".
								 "	</tr>																\n".
								 "</table>"; 
							$getPics=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$auctionDetails['id']."'");
							$nbPics=mysqli_num_rows($getPics); 
						} ?>
               </td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="10" height="1"></td>
               <?
						if ($nbPics>0) {
							echo "<td><table border=0 width=100% cellspacing=2 cellpadding=2>\n";
								while ($addlPicture=mysqli_fetch_array($getPics)) {
									echo "<tr><td class=\"c4\"><img src=\"makethumb.php?pic=".$addlPicture['name']."&w=100\"></td></tr>";
									echo "<tr class=\"c1\"><td align=\"center\">								\n".
										 "<a class=\"sell\" href=\"showpic.php?pic=".$addlPicture['name']."\" target=\"_blank\">	\n".
										 "<b>".$lang[largerpic]."</b></a>										\n".
										 "</td></tr>";
								}
							echo "</td></table>";
						} ?>
         </table></td>
   </tr>
   <tr>
      <td class="c4" align="center"><?=$lang[itemviewed]?>
         <strong>
         <?=$auctionDetails['clicks'];?>
         </strong>
         <?=$lang[times]?>
      </td>
   </tr>
</table>
<br>
<?
$mainCat_primary = getMainCat($auctionDetails['category']);
$mainCat_secondary = getMainCat($auctionDetails['addlcategory']);

$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT f.boxid, f.boxname, f.boxtype, d.boxvalue FROM 
probid_fields_data d, probid_fields f WHERE d.auctionid='".$_GET['id']."' AND f.active='1' AND d.boxid = f.boxid AND 
(f.categoryid='".$mainCat_primary."' OR f.categoryid='".$mainCat_secondary."' OR f.categoryid='0') ORDER BY f.fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
$isFields = mysqli_num_rows($getFields);
if ($isFields) { ?>
<!-- Header Additional Custom Fields -->
<table width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr class="c1">
      <td nowrap><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c6">
      <td height="30" nowrap>&nbsp;
         <?=$imgarrowit;?>
         &nbsp;<b>
         <?=strtoupper($lang[addfields])?>
         </b></td>
   </tr>
</table>
<!-- End Header Additional Custom Fields -->
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
   <? while ($fields=mysqli_fetch_array($getFields)) { $toDisp = ""; ?>
   <tr class="<? echo (($count++)%2==0)?"c3":"c2"; ?>">
      <td align="right" width="50%"><strong>
         <?=$fields['boxname'];?>
         </strong></td>
      <td><?
			if ($fields['boxtype']=="checkbox") {
				$chkValues = explode(";",$fields['boxvalue']);
				for ($i=0; $i<count($chkValues); $i++) { 
					$chkRes = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".trim($chkValues[$i])."'","boxcaption");
					$toDisp .= ((trim($chkRes)!="n/a") ? $chkRes : "")."&nbsp; &nbsp; ";
				}	
			} else if ($fields['boxtype']=="radio") {
				$toDisp = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$fields['boxvalue']."'","boxcaption")." ";
			} else if ($fields['boxtype']=="list") {
				$toDisp = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$fields['boxvalue']."'","boxcaption")." ";
			} else {
				$toDisp = $fields['boxvalue'];
			}
			echo $toDisp;
			?>
      </td>
   </tr>
   <? } ?>
</table>
<br>
<? } ?>
<!-- Header Payment -->
<table width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr class="c1">
      <td nowrap><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c6">
      <td height="30" nowrap>&nbsp;
         <?=$imgarrowit;?>
         &nbsp;<b>
         <?=$lang[payment]?>
         </b></td>
   </tr>
</table>
<!-- End Header Payment -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
   <tr valign="top">
      <td><?
		$paymentMethods=explode("<br>",addSpecialChars($auctionDetails['pm']));
		$nbPaymentMethods=count($paymentMethods);
		?>
         <table border="0" cellspacing="4" cellpadding="4" align="center">
            <? 
	  	for ($i=0;$i<$nbPaymentMethods;$i+=4) { 
			$j=$i+1;
			$k=$i+2;
			$l=$i+3; ?>
            <tr valign="top" align="center" style="font: bold;">
               <td><? 
		  	$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$i]."'","logourl"); 
            echo $paymentMethods[$i].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
               <td><? 
		  	$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$j]."'","logourl");
			echo $paymentMethods[$j].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
               <td><? 
		  	$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$k]."'","logourl");
            echo $paymentMethods[$k].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
               <td><? 
		  	$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$l]."'","logourl");
            echo $paymentMethods[$l].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
            </tr>
            <? } ?>
         </table></td>
   </tr>
</table>
<!-- Header SHIPPING -->
<table width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr class="c1">
      <td nowrap><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c6">
      <td height="30" nowrap>&nbsp;
         <?=$imgarrowit;?>
         &nbsp;<b>
         <?=$lang[shipping]?>
         </b></td>
   </tr>
</table>
<!-- End Header SHIPPING -->
<table width="100%" border="0" cellspacing="1" cellpadding="10">
   <tr valign="top">
      <td><?
	if ($auctionDetails['sc']=="BP") echo "<li>".$lang[buyerpaysshipment]."</li>";
	else echo "<li>".$lang[sellerpaysshipment]."</li>";
  	if ($auctionDetails['scint']=="Y") echo "<li>".$lang[sellershipinternat]."</li>"; ?>
      </td>
   </tr>
   <? if ($setts['shipping_costs']==1) { ?>
   <tr valign="top">
      <td class="contentfont"><strong>
         <?=$lang[postagecosts]?>
         :</strong>
         <?=displayAmount($auctionDetails['postage_costs'],$auctionDetails['currency'],"YES");?>
         <br>
         <strong>
         <?=$lang[insurance]?>
         :</strong>
         <?=displayAmount($auctionDetails['insurance'],$auctionDetails['currency'],"YES");?>
         <br>
         <strong>
         <?=$lang[servicetype]?>
         :</strong>
         <?=$auctionDetails['type_service'];?>
      </td>
   </tr>
   <? } ?>
</table>
<? } ?>
