<?
## v5.24 -> apr. 06, 2006
## Authorize.net ADDON
if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {




	if ( !defined('INCLUDED') ) { die("Access Denied"); }

	include("formchecker.php");
	if ($_REQUEST['action']=="updateinfo") {

		$payment_update_string = '';
		$payment_update_string .= "paypalemail='".$_POST['paypalemail']."',";
		$payment_update_string .= "worldpayid='".$_POST['worldpayid']."',";
		$payment_update_string .= "ikoboid='".$_POST['ikoboid']."',ikoboipn='".$_POST['ikoboipn']."',";
		$payment_update_string .= "checkoutid='".$_POST['checkoutid']."',";
		$payment_update_string .= "protxname='".$_POST['protxname']."',protxpassword='".$_POST['protxpassword']."',";
		$payment_update_string .= "authnetid='".$_POST['authnetid']."',authnettranskey='".$_POST['authnettranskey']."',";
		$payment_update_string .= "nochexemail='".$_POST['nochexemail']."',";
		$payment_update_string = rtrim($payment_update_string,',');

		$vat_uid = $_POST['vat_uid_number'];

		$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
		name='".remSpecialChars($_POST['name'])."', address='".remSpecialChars($_POST['address'])."', city='".remSpecialChars($_POST['city'])."',
		state='".remSpecialChars($_POST['state'])."', country='".remSpecialChars($_POST['country'])."', zip='".remSpecialChars($_POST['zip'])."',
		phone='".remSpecialChars($_POST['phone'])."', email='".$_POST['email']."', newsletter='".$_POST['subsnl']."',
		apply_vat_exempt='".$_POST['apply_vat_exempt']."', vat_uid_number='".$vat_uid."', ".$payment_update_string."
		WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

		$_SESSION['membername'] = $_POST['name'];

		if ($_POST['password']!="") {
			$updatePassword = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
			password='".md5($_POST['password'])."' WHERE id='".$_SESSION['memberid']."'");
		}

		### VAT exemption email
		$recipientId = $_SESSION['memberid'];
		if ($_POST['apply_vat_exempt']=="Y"&&$_POST['apply_vat_exempt_old']=="N")
			include ("mails/register_apply_vat_exempt.php");

		echo "<br><p class=contentfont align=center>$lang[inf_updated]</p>";
		$link = "membersarea.php";
		echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
  	} else {
		$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");

		$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
    	if ($setts['account_mode_personal']==1) {
    		$account_mode_local = ($tmp) ? 2 : 1;
  		} else $account_mode_local = $setts['account_mode'];

		if ($account_mode_local==2) {
			if ($user['balance']<=0) {
				$thevar = $lang[credit];
				$balance_mode = 1;
				$balance = abs($user['balance']);
			} else {
				$thevar = $lang[debit];
				$balance_mode = 0;
				$balance = $user['balance'];
			} ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
   <tr>
      <td class="contentfont"><b>
         </b><a href="membersarea.php?page=money"><?echo "$lang[balance1]"?></a> | 
	       <a href="membersarea.php?page=addmoney"><?echo "$lang[addmoney]"?></a> | 
               <a href="membersarea.php?page=withdrawmoney"><?echo "$lang[withdrawmoney]"?></a> |	
               <a href="membersarea.php?page=transactions"><?echo "$lang[viewtransactions]"?></a> |	
               <a href="membersarea.php?page=bankdetails"><?echo "$lang[bankdetails]"?></a> 	
         </a></td>
   </tr>
</table>

<?

####
## MY FUNCTIONS
####



function usersTrans($arg,$recordsPerPage,$count) {
    $pageNum = 1;
    if(isset($_POST['p'])) {
      $pageNum = $_POST['p'];
      settype($pageNum, 'integer');
    }
    $offset = ($pageNum - 1) * $recordsPerPage;
    
    if ($offset>=0) {
      $filter = " LIMIT ".$offset.",".$recordsPerPage;
    }
    
    if ($count==1) {
      $filter="";
    }
    
    $arrSort = array("tdate","name","description","amount","balance");
    $arrAw = array("asc","desc");
    
    if (($_POST['sort'])!='') {
    
      if ((in_array($_POST["sort"],$arrSort)) && (in_array($_POST["arrow"],$arrAw)))
      {
        $order = " ORDER BY ".$arg['sort']." ".$arg['arrow']."";
      }
    
      
    } else {
        $order = " ORDER BY id ASC";
    }
    
     if (($_POST['start_date']!='' AND $_POST['end_date']!='') AND (strtotime($_POST['start_date']) <= strtotime($_POST['end_date']))) 
     {
        $period = "AND probid_users_transactions.tdate BETWEEN '".$_POST['start_date']." 00:00:00' AND '".$_POST['end_date']." 23:59:59'";
     } else {
        $period = "AND (MONTH(CURDATE()) = MONTH(tdate))";
     }     
     
     $criterias = $period;
     
    $result = @mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * FROM probid_users_transactions
            WHERE userid=".$_SESSION["memberid"]." ".$criterias." ".$order.$filter."");
    while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $myTrans[] = $row; 
    }
return $myTrans;
}

function link_bar($page, $count,$perpage) {
	$data = $_POST;
  if (!isset($page) || $page==0) $page = 1;
  
  $pages_count  = ceil($count / $perpage);


	for ($j = 1; $j <= $pages_count; $j++)
	{
		if ($j == $page) {
		$pages_bar.='<td><b>'.intval($j).'</b></td>';
		} else {
		$pages_bar.='<td><a href="javascript:getPageContent('.$j.');">'.$j.'</a></td>';
		}
		if ($j != $pages_count) $pages_bar.='<td></td>';
	}

    return $pages_bar;
}


####
## MY FUNCTIONS
####


$results = usersTrans($_POST,40,0);
$data = $_POST;


if (intval($data['p'])==0) $data['p'] = 1;

?>
<form name="dataForm" method="post" action="membersarea.php">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
<tr>
    <td>
        <table>
          <tr>
              <td><?=$lang[begofperiod]?></td>
              <td>
                <table>
                <tr>
                    <td><input class="date" type="text"  name="start_date" value="<?=$_POST['start_date']?>"/></td>
                    <td><a href="#" onclick="displayDatePicker('start_date');"><img alt="" border="0" src="<?="images/calendar.jpg"?>"></a></td>
                    <td><?=$lang[endofperiod]?></td>
                    <td><input class="date" type="text"  name="end_date" value="<?=$_POST['end_date']?>"/></td>
                    <td><a href="#" onclick="displayDatePicker('end_date');"><img alt="" border="0" src="<?="images/calendar.jpg"?>"></a></td>
                    <td><input type="submit" value="<?=$lang[sendquery]?>"></td>
                </tr>
                </table>
              </td>
          </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table style="width:100%;">
        <tr>
        <td>#<a name="content"/></td>
        <td><?=$lang[date]?><a href="javascript:sortBy('tdate','asc');"><img border="0" src="images/sortup.gif"></a>
                 <a href="javascript:sortBy('tdate','desc',<?=isset($data['p'])?$data['p']:'1'?>);"><img border="0" src="images/sortdown.gif"></a>
        </td>
        <td><?=$lang[name]?><a href="javascript:sortBy('name','asc');"><img border="0" src="images/sortup.gif"></a>
                 <a href="javascript:sortBy('name','desc');"><img border="0" src="images/sortdown.gif"></a>
        </td>
        <td><?=$lang[descriptionofpayment]?><a href="javascript:sortBy('description','asc');" ><img border="0" src="images/sortup.gif"></a>
                 <a href="javascript:sortBy('description','desc');"><img border="0" src="images/sortdown.gif"></a>
        </td>
        <td><?=$lang[amount1]?><a href="javascript:sortBy('amount','asc');" ><img border="0" src="images/sortup.gif"></a>
                 <a href="javascript:sortBy('amount','desc');"><img border="0" src="images/sortdown.gif"></a>
        </td>
        <td><?=$lang[balance1]?><a href="javascript:sortBy('balance','asc');" ><img border="0" src="images/sortup.gif"></a>
                 <a href="javascript:sortBy('balance','desc');"><img border="0" src="images/sortdown.gif"></a>
        </td>
        </tr>
        <?
        if ((is_array($results)) && (sizeof($results)>0)) 
        {
            foreach ($results as $t) 
            {
              if ($data['p']>1) 
              {
                $number=(20*($data['p']-1));
                $x++;
              } else {
                $number++;
              }
                ?><tr class="<?=(($number%2==0)?'c2':'c3')?>"><td><?=$number?></td><td width="120"><?=format_date($t["tdate"],1)?></td><td><?=utf8_encode($t["name"])?></td><td><?=utf8_encode($t["description"])?></td><td style="<?=($t["op"]=="in"?'color:green':'color:red')?>"><?=($t["op"]=="in"?'+':'-')?><?=money_format('%i',$t["amount"])?> EUR</td><td><?=money_format('%i',$t["balance"])?> EUR</td></tr><?
            }
        } else {
        
          ?><tr><td colspan="6" align="center"> Транзакций не найдено </td></tr><?
        
        }
        ?>
    </td>
</tr>
<tr>
    <td>
    <input type="hidden" value="transactions"  name="page"/>
    <input type="hidden" value="<?=$_POST['arrow']?>"  name="arrow" id="arrow"/>
    <input type="hidden" value="<?=$_POST['sort']?>"  name="sort" id="sort"/>    
    </td>
</tr>
</table>
</form>

<?

?>


<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr valign="top" class="c3">
      <td align="right" colspan="3"><strong>
         <?=$lang[accbalance]?>
         <? echo $thevar." ".(($balance==0)?$balance.',00':DisplayAmount($balance));?></strong></td>
   </tr>
   <? if ($thevar==$lang[debit]&&$balance>=$setts['min_invoice_value']) { ?>
   <tr class="c2">
      <td align="center" class="contentfont" colspan="3"><?
			$amount = $balance;
			$returnUrl=$path."paymentdone.php";
			$failureUrl=$path."paymentfailed.php";
			if ($amount==0) $setts['payment_gateway']="none";

			$paymentAmount=number_format($amount,2,'.','');

			### new function that displays the payment message and amount
			displayPaymentMessage($paymentAmount);

			### new procedure to list all active payment gateways
			if ($setts['payment_gateway']=="none") {
				echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
				counterAddUser($_SESSION['memberid']);
				$activateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
				active = '1',payment_status='confirmed' WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			} else {
				$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
				while ($selectedPGs = mysqli_fetch_array($getPGs)) {
					// Pangalink
					include 'pangalink/invoice.php';

					if ($selectedPGs['name']=="Paypal") {
						$notifyUrl=$path."paymentprocess.php?table=4";
						paypalForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Nochex") {
						$notifyUrl=$path."nochexprocess.php?table=4";
						nochexForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="2Checkout") {
						$notifyUrl=$path."checkoutprocess.php?table=4";
						checkoutForm($_SESSION['memberid'],$setts['checkoutid'],$paymentAmount,4);
					}
					if ($selectedPGs['name']=="Worldpay") {
						$notifyUrl=$path."worldpayprocess.php?table=4";
						worldpayForm($_SESSION['memberid'],$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Ikobo") {
						$notifyUrl=$path."ikoboprocess.php?table=4";
						ikoboForm($_SESSION['memberid'],$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Protx") {
						$notifyUrl=$path."protxprocess.php?table=4";
						protxForm($_SESSION['memberid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Authorize.Net") {
						$notifyUrl=$path."authorize.net.process.php?table=4";
						authorizeNetForm($_SESSION['memberid'],$paymentAmount,4);
					}
					if ($selectedPGs['name']=="Moneybookers") {
						$notifyUrl=$path."moneybookers.process.php?table=4";
						moneybookersForm($_SESSION['memberid'],$paymentAmount,4);
					}
					if ($selectedPGs['name']=="Test Mode") {
						$notifyUrl=$path."paymentsimulator.php?table=4";
						testmodeForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
				}
			}
			?>
      </td>
   </tr>
</table>
<? } ?>
<? } ?>

<br>
<?
		}
} else { echo "$lang[err_relogin]"; } ?>
