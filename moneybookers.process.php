<?php
## v5.24 -> apr. 10, 2006
include_once ("config/config.php");

## overwrite $setts['payment gateway']
$setts['payment_gateway']="Moneybookers";

if ($_POST['M_table']==1) $table_to_modify='probid_users';
if ($_POST['M_table']==2) $table_to_modify='probid_auctions';
if ($_POST['M_table']==3) $table_to_modify='probid_winners';
if ($_POST['M_table']==4) $theoption=2;

if ($_POST['M_table']==7) $table_to_modify="probid_wanted_ads";

$payment_status = "Completed";
$payment_gross = $_POST['amount'];
$txn_id = $_POST['mb_transaction_id'];
$custom = $_POST['cartId'];
$currentTime=time();

if ($_POST['transStatus']=="Y"){
	#-------------------------------------------
	$isTxnId = FALSE;
	$getTxn = getSqlNumber("SELECT * FROM probid_txn_ids WHERE txnid='".$txn_id."' AND processor='".$setts['payment_gateway']."'");
	if ($getTxn) $isTxnId = TRUE;
				
	## only do all this if there is no txn yet.
	if (!$isTxnId) { 
		$addTxn = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_txn_ids (txnid, processor) VALUES ('".$txn_id."','".$setts['payment_gateway']."')");
		if ($theoption==2) {
			$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$custom."'","balance");
			$updatedBalance = $currentBalance - $payment_gross;
			if ($updatedBalance<=0) {
				$_SESSION['accsusp']=0;
			}
			## if user is suspended, activate the counters
			$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
			if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);
	
			$currentTime = time();
			$updateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			active='1', payment_status='confirmed', balance='".$updatedBalance."' WHERE id='".$custom."'");
			$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
			active='1' WHERE ownerid='".$custom."'");
			$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
			active='1' WHERE ownerid='".$custom."'");
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
			('".$custom."','".$lang[payment_fee]."','".$payment_gross."','".$currentTime."','".$updatedBalance."','payment','".$setts['payment_gateway']."')");
		} else {
			if ($_POST['M_table']==3) {
				$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
				active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',
				txnid='".$txn_id."',
				paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' WHERE auctionid='".$custom."'");
				## obsolete: include ("mails/endauctionbuyerdetails.php");
			} else if ($_POST['M_table']==8) {
				$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
				store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				## now we submit the accounting details
				$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
				(userid, amountpaid, paymentdate, processor) VALUES
				('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			} else {
				## if we activate the users table, count all auctions
				if ($_POST['M_table']==1) counterAddUser($custom);
	
				$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
				active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',
				paymentdate='".$currentTime."',processor='".$setts['payment_gateway']."' WHERE id='".$custom."'");
				## if we activate the auctions table, count the auction
				if ($_POST['M_table']==2) {
					$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
					if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
						addcatcount ($auctCat['category']);
						addcatcount ($auctCat['addlcategory']);
					}
				}
				if ($_POST['M_table']==7) {
					$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
					if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
						addwantedcount ($wantedCat['category']);
						addwantedcount ($wantedCat['addlcategory']);
					}
				}
			}
		}
		countCategories();
	}
}
include ("paymentdone-endauction.php");
?>