<?
## v5.24 -> jun. 02, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

$canSubmit = FALSE;
if ($_REQUEST['from']==$_SESSION['memberid']||$_REQUEST['to']==$_SESSION['memberid']) $canSubmit = TRUE;
$q = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks WHERE fromid='".$_REQUEST['from']."' AND userid='".$_REQUEST['to']."' AND auctionid='".$_REQUEST['auctionid']."' AND submitted='0'");
if ((mysqli_num_rows($q) < 1)||!$canSubmit) {
  	header("Location: membersarea.php");
  	exit();
}

include ("themes/".$setts['default_theme']."/header.php");

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");

include("membersmenu.php");

include_once("formchecker.php");

if ($action=="submit_feedback") { 
	$feedbackid=getSqlField("SELECT id FROM probid_feedbacks WHERE fromid='".$_POST['from']."' AND userid='".$_POST['to']."' AND auctionid='".$_POST['auctionid']."' AND submitted='0'","id");

	$updateFeedback = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_feedbacks SET 
	feedback='".remSpecialChars($_POST['feedback'])."',rate='".$_POST['rate']."',date='".$timeNowMysql."',submitted=1 
	WHERE fromid='".$_POST['from']."' AND userid='".$_POST['to']."' AND auctionid='".$_POST['auctionid']."' AND submitted='0'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

	### insert the custom fields for the auction (if available)
  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_custom_rep") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  	$isFields = mysqli_num_rows($getFields);
  	if ($isFields) {
		while ($fields=mysqli_fetch_array($getFields)) {
			$box_value = "";
			if ($fields['boxtype']=="checkbox") {
				for ($i=0; $i<count($_POST['box'.$fields['boxid']]); $i++) 
					$box_value .= $_POST['box'.$fields['boxid']][$i]."; ";
			} else { 
				$box_value = $_POST['box'.$fields['boxid']];
			}
			$addFieldData = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_custom_rep_data
			(feedbackid, boxid, boxvalue) VALUES
			('".$feedbackid."','".$fields['boxid']."','".remSpecialChars($box_value)."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}		

	echo "<br><p class=contentfont align=center>$lang[fbsubmitted]</p><p>&nbsp;</p>";
	$link = "membersarea.php?page=feedback";
	echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
} else { 
	$isCRF = getSqlNumber("SELECT id FROM probid_custom_rep WHERE active=1"); ?>
<table width="550" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
  <form action="leavefeedback.php" method="post">
    <input type="hidden" name="from" value="<?=$_REQUEST['from'];?>">
    <input type="hidden" name="to" value="<?=$_REQUEST['to'];?>">
    <input type="hidden" name="auctionid" value="<?=$_REQUEST['auctionid'];?>">
    <tr class="c1">
      <td colspan="2" align="center"><?=$lang[feedback_leaveto]?>
        <?=getSqlField("SELECT * FROM probid_users WHERE id='".$_REQUEST['to']."'","username");?>
      </td>
    </tr>
    <tr class="c7">
      <td><strong><?=$lang[feedback_text]?></strong></td>
      <td><strong><?=$lang[rate]?></strong></td>
    </tr>
    <tr class="c2">
      <td><textarea name="feedback" cols="50" rows="5" id="feedback"><?=$_POST['feedback'];?></textarea></td>
	  <td><input name="rate" type="radio" value="5" checked>
        <img src="images/5stars.gif" /><br>
        <input type="radio" name="rate" value="4" <? echo ($_POST['rate']==4) ? "checked" : ""; ?>>
        <img src="images/4stars.gif" /><br>
        <input type="radio" name="rate" value="3" <? echo ($_POST['rate']==3) ? "checked" : ""; ?>>
        <img src="images/3stars.gif" /><br>
        <input type="radio" name="rate" value="2" <? echo ($_POST['rate']==2) ? "checked" : ""; ?>>
        <img src="images/2stars.gif" /><br>
        <input type="radio" name="rate" value="1" <? echo ($_POST['rate']==1) ? "checked" : ""; ?>>
        <img src="images/1stars.gif" /></td>
    </tr>
    <? if ($isCRF) { 
		### first determine what type the feedback is
		$fbtype = getSqlField("SELECT type FROM probid_feedbacks WHERE 
		fromid='".$_REQUEST['from']."' AND userid='".$_REQUEST['to']."' AND auctionid='".$_REQUEST['auctionid']."'","type");
		
		$usertype = ($fbtype=="sale") ? "buyer" : "seller";
		
  		$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, active FROM probid_custom_rep WHERE usertype!='".$usertype."' ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); ?>
    <!-- Additional Custom Fields -->
    <tr>
      <td colspan=2 class="c1" align="center"><b>
        <?=$lang[addfields]?>
        </b></td>
    </tr>
    <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
    </tr>
    <? 
	while ($fields=mysqli_fetch_array($getFields)) { ?>
    <tr class="<? echo (($count++)%2==0)?"c3":"c2"; ?>">
      <td align="right"><strong>
        <?=$fields['boxname'];?>
        </strong></td>
      <td><? echo createField($fields['boxid'],$_REQUEST['box'.$fields['boxid']],'probid_custom_rep'); ?> </td>
    </tr>
    <? } ?>
    <? } ?>
    <tr class="c4">
      <td colspan="2" align="center"><input name="leavefbok" type="submit" id="leavefbok" value="<?=$lang[submit];?>"></td>
    </tr>
  </form>
</table>
<? } ?>
<br>
<? 	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
