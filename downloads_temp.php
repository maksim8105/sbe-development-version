<?
## v5.24 -> apr. 12, 2006
session_start();
if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php'</script>";
} else {

  include_once ("config/config.php");

 // GET USER DOWNLOADS

  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * FROM probid_auctions_downloads WHERE ownerid=".$_SESSION["memberid"]."");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $myDownloads[] = $row; 
  }
  
//  print_r($myDownloads);
 
 // GET CATEGORIES
 
 function catList() {
 
 global $c_lang;
 
  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_categories ORDER BY id ASC");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $catID[] = $row; 
  }
  

 
 foreach ($catID as $cat) {
 
    $i++;
 
    $cat_string.= $cat["id"].($i<count($catID)?",":"");
 
 }
 

 $i=0;
  
  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT parent,id FROM probid_categories WHERE ID IN(".$cat_string.") AND parent!=0 ORDER BY id ASC");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $subcatList[] = $row; 
  }  
  
  $catSelect = "<select name='item[category]'>";
  $catSelect.="<option value='0'>-</option>";
  
  foreach ($subcatList as $item) {
  

      $catSelect.="<option value='".$item["id"]."'>".$c_lang[$item["parent"]].": ".$c_lang[$item["id"]]."</option>";
  }
  $catSelect.="</select>";
  
  echo $catSelect;

 }
 
 function auctionType($selected) {
 
 $arrTypes = array(
 0=>array("type"=>"standard","label"=>"Standard Auction"),
 1=>array("type"=>"dutch","label"=>"Dutch Auction"),
 
 );
 
 $sel = "<select name='item[auc_type]'>";
 
 foreach ($arrTypes as $option) {
 
  $sel.="<option value='".$option["type"]."'>".$option["label"];
 
 }

  $sel .= "</select>";
  
  return $sel;

 }
 
 function currList() {
 
 $result=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_currencies");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $curr[] = $row; 
  }  
  
  $sel = "<select name='item[currency]'>";
  foreach ($curr as $item) {
    $sel .= "<option value='".$item["symbol"]."'>".$item["symbol"];
  }
  $sel .="</select>";
  return $sel;
  
 }
 
 function durationList() {
 
 $arrPeriod = array(
 0=>array("value"=>"1","label"=>"1 day"),
 1=>array("value"=>"3","label"=>"3 days"),
 2=>array("value"=>"7","label"=>"7 days"),
 
 );
 
 $sel = "<select name='item[duration]'>";
 
 foreach ($arrPeriod as $option) {
 
  $sel.="<option value='".$option["value"]."'>".$option["label"];
 
 }

  $sel .= "</select>";
  
  return $sel;

  
 }
 


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf8">
  <title>SBE.EE - Auctions - My Downloads</title>
  </head>
  <body>
  <form name="insertForm" method="post" action="">
  <table border="0" cellspacing="0" cellpadding="0">
  <tr style="background-color:gray;">
      <td valign="top" rowspan="2">
      <table style="background-color:#00ccff;">
      <tr>
          <td colspan="7" align="center"><b>Item data</b></td>
      </tr>
      <tr>
          <td valign="top" colspan="7">Category:  <?=catList();?>Add category<?=catList();?></td>
      </tr>
      <tr>
          <td>
          <table>
          <tr>
              <td valign="top">Itemname:</td><td> <input type="text" name="item[name]" size="20" /></td>
          </tr>
          <tr>
              <td>Location:</td><td><input type="text" name="item[location]" size="30" /></td>
          </tr>
          <tr>
              <td>Quantity:</td><td><input type="text" name="item[quantity]" size="10" /></td>
          </tr>
          <tr>
              <td>Dutch:</td><td><input type="checkbox" name="item[dutch]"></td>
          </tr>
          </table>
          </td>
          <td valign="top">Description:</td><td><textarea name="item[desc]" cols="60" rows="5"></textarea></td>
          <td valign="top">Images</td><td><input type="file" name="item[pic1]"><br><input type="file" name="item[pic2]"><br><input type="file" name="item[pic3]"><br><input type="file" name="item[pic4]"><br><input type="file" name="item[pic5]"></td>
      </tr>
      <tr>
          <td colspan="7" align="center"><b>Auction data</b></td>
      </tr>
    <tr>
        <td colspan="7">
        <table style="width:100%;">
          <tr>
              <td valign="top">Auction type: <?=auctionType(1);?></td>
              <td valign="top">
              <table>
              <tr>
                  <td>Price: </td><td><input type="text" name="item[price]" size="10" /></td>
              </tr>
              <tr>
                  <td>Reserve price:</td><td><input type="text" name="item[reserve_price]" size="10" /></td>
              </tr>
              <tr>
                  <td>Buy now: </td><td><input type="text" name="item[buy_now]" size="10" /></td>
              </tr>
              <tr>
                  <td>Currency:</td><td><?=currList();?></td>
              </tr>
              </table>
              </td>
              <td valign="top">
              <table>
              <tr>
                  <td>Start Date: </td><td><input type="text" value="DD.MM.YYYY" name="item[startdate]" /></td>
              </tr>
              <tr>
                  <td>Duration:</td><td><?=durationList();?></td>
              </tr>
              </table>  
              </td>
              <td valign="top">Transport and Payment description<br><input type="checkbox" name="item[s_included]"> Shipping included</td><td><textarea name="item[ps_details]" cols="30" rows="5"></textarea></td>
          </tr>
        </table>
        </td>
    </tr>
      </table>
      </td>
      <td>
      <input type="hidden" name="action" value="insert"><input type="submit" value="SAVE">
      </td>
  </tr>
  </table>
  </form>
  </body>
</html>
<?
}