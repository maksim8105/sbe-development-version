<?php
    /********************************************
     * Pangalingi näiteprogramm                 *
     * (C) 2006 Margus Kaidja, Zone Media OÜ    *
     ********************************************/

    require_once 'config.php';

    /**
     * Loome näitliku ostukorvi ning nende väärtuste põhjal loome
     * vormi tehingu parameetritega.
     * Ostukorvi sisu võiks tegelikult tulla andmebaasist, kus vastava
     * rea ID pannakse ka tehingu identifikaatoriks
     */
    $shoppingCart= Array(
                            /* Tehingu summa, mis klient tasuma peab */
                            'price'     => 1.5,

                            /* Valuuta, milles ta tasub */
                            'currency'  => 'EEK',

                            /* Teenuse/kauba kirjeldus */
                            'description' => 'Torso Tiger',

                            /* Tehingu identifikaator
                             * See on unikaalne väärtus, millega saab identifitseerida
                             * käesoleva tehingu. Kuna tehingu saatmine panka ning pangast
                             * vastuse lugemine toimuvad täiesti eraldiseisvate protsessidena,
                             * siis on pangast vastuse lugemise skriptis ID järgi konkreetset
                             * tehingut tuvastada. */
                            'transaction_id' => 12345,
                        );

    /**
     * Loome massiivi tehingu andmetega, mis lähevad panka
     */
    $macFields = Array(
                        'VK_SERVICE'    => '1002',
                        'VK_VERSION'    => '008',
                        'VK_SND_ID'     => $preferences['my_id'],
                        'VK_STAMP'      => $shoppingCart['transaction_id'],
                        'VK_AMOUNT'     => $shoppingCart['price'],
                        'VK_CURR'       => $shoppingCart['currency'],
                        /*'VK_ACC'        => $preferences['account_number'],
                        'VK_NAME'       => $preferences['account_owner'],*/
                        'VK_REF'        => '',
                        'VK_MSG'        => $shoppingCart['description'],
                        'VK_RETURN'     => 'http' . ($_SERVER['HTTPS'] ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] .
                                            dirname ($_SERVER['PHP_SELF']) . '/notify.php',
                    );

    /**
     * Genereerime tehingu väärtustest signatuuri
     */
    $key = openssl_pkey_get_private (file_get_contents ($preferences['my_private_key']),
                                            $preferences['my_private_key_password']);

$macFields['VK_MAC'] = str_replace( array( "\r", "\n" ), '', $macFields['VK_MAC'] );

    if (!openssl_sign (generateMACString ($macFields), $signature, $key)) {
        trigger_error ("Unable to generate signature", E_USER_ERROR);
    }

    $macFields['VK_MAC'] = base64_encode ($signature);

    /**
     * Genereerime maksmise vormi
     */
?>
<form method="POST" action="<?php echo $preferences['banklink_address']; ?>">
<?php

##$macFields['VK_MAC'] = str_replace( array( "\r", "\n" ), '', $macFields['VK_MAC'] );

    foreach ($macFields as $f => $v) {
        echo '<input type="hidden" name="' . $f . '" value="' . htmlspecialchars ($v) . '" />' . "\n";
    }



?>
<table cellpadding="0" cellspacing="0" border="2">
    <tr>
        <td>Summa:</td>
        <td><?php echo htmlspecialchars ($shoppingCart['price'] . ' ' . $shoppingCart['currency']); ?>
    </tr>

    <tr>
        <td>Kellele:</td>
        <td><?php echo htmlspecialchars ($preferences['account_owner']) . ' (' . htmlspecialchars ($preferences['account_number']) . ')'; ?>
    </tr>

    <tr>
        <td>Mille eest:</td>
        <td><?php echo htmlspecialchars ($shoppingCart['description']); ?></td>
    </tr>

    <tr>
        <td>Tehingu ID:</td>
        <td><?php echo htmlspecialchars ($shoppingCart['transaction_id']); ?></td>
    </tr>

    <tr>
        <td colspan="2" align="center"><input type="submit" value="MAKSMA" /></td>
    </tr>
</table>
</form>