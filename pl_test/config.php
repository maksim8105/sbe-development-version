<?php 
    /******************************************** 
     * Pangalingi näiteprogramm                 * 
     * (C) 2006 Margus Kaidja, Zone Media OÜ    * 
     ********************************************/ 

    /** 
     * Pangaga suhtlemiseks vajalikud parameetrid 
     */ 
    $preferences = Array( 
                            /* Sertifikaadipäringu loomisel saadud privaatvõtme fail */ 
                            'my_private_key'    => 'private_key.pem', 
                             
                            /* Juhul, kui privaatvõti on parooliga kaitstud, siis 
                             * peab ka parool alati siin kirjas olema */ 
                            'my_private_key_password' => '', 

                            /* Panga poolt saadetud sertifikaat */ 
                            'bank_certificate'  => 'hansa.pem', 

                            /* Panga poolt saadetud kaupmehe identifikaator */ 
                            'my_id'             => 'SBE', 

                            /* Pangakonto number, kuhu maksed laekuma hakkavad */ 
                            'account_number'    => '1234567890', 

                            /* Pangakonto omaniku nimi */ 
                            'account_owner'     => 'MK And Partners OÜ', 

                            /* Pangalingi aadress */ 
                            'banklink_address'  => 'https://www.hanza.net/cgi-bin/hanza/pangalink.jsp', 
                        ); 

    /** 
     * Päringute/vastuste muutujate järjekorrad 
     */ 
    $VK_variableOrder = Array( 
                                    1002 => Array( 
                                                    'VK_SERVICE','VK_VERSION','VK_SND_ID', 
                                                    'VK_STAMP','VK_AMOUNT','VK_CURR', 
                                                    'VK_REF','VK_MSG' 
                                                 ), 

                                    1101 => Array( 
                                                    'VK_SERVICE','VK_VERSION','VK_SND_ID', 
                                                    'VK_REC_ID','VK_STAMP','VK_T_NO','VK_AMOUNT','VK_CURR', 
                                                    'VK_REC_ACC','VK_REC_NAME','VK_SND_ACC','VK_SND_NAME', 
                                                    'VK_REF','VK_MSG','VK_T_DATE' 
                                                 ), 

                                    1901 => Array( 
                                                    'VK_SERVICE','VK_VERSION','VK_SND_ID', 
                                                    'VK_REC_ID','VK_STAMP','VK_REF','VK_MSG' 
                                                 ), 
                                ); 

    /** 
     * Genereerib sisseantud massiivi väärtustest jada. 
     * Jadasse lisatakse iga väärtuse pikkus (kolmekohalise arvuna) 
     * ning selle järel väärtus ise. 
     */ 
    function generateMACString ($macFields) { 
        global  $VK_variableOrder; 

        $requestNum = $macFields['VK_SERVICE']; 

        $data = ''; 

        foreach ((array)$VK_variableOrder[$requestNum] as $key) { 
            $v = $macFields[$key]; 
            $data .= str_pad (strlen ($v), 3, '0', STR_PAD_LEFT) . $v; 
        } 

        return $data;         
    } 
  
?>