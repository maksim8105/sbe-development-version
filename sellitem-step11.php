<? 
## v5.25 -> jun. 28, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($_SESSION['membersarea']=="Active") { 
?>
<script language="javascript">
	function is_numeric(num) {
		var exp = new RegExp("^[0-9]*$","g");
		return exp.test(num);
	}
	function revertMaxRelist(theform) {
		if (theform.auto_relist_nb_tmp.value<1 || !is_numeric(theform.auto_relist_nb_tmp.value)) {
			theform.auto_relist_nb_tmp.value = 1;
		} else if (theform.auto_relist_nb_tmp.value><?=$setts['nb_autorelist_max'];?>) { 
			theform.auto_relist_nb_tmp.value = <?=$setts['nb_autorelist_max'];?>;
		}
	}
</script>
 <table width="100%" border="0" cellpadding="2" cellspacing="2"> 
  <tr class="contentfont" align="center"> 
     <td class="c4" width="20%"><?=$lang[sellstep1];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep3];?></td> 
     <td class="c1" width="20%"><?=$lang[sellstep4];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep5];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep6];?></td> 
   </tr> 
</table>
<br>
<? 
$prefSeller = "N";
if ($setts['pref_sellers']=="Y") {
	$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
	WHERE id='".$_SESSION['memberid']."'","preferred_seller");
	$reduction = (100-$setts['pref_sellers_reduction'])/100;
}

echo "<script language=\"JavaScript\"> \n";
echo "	function delete_pic(theform,pic) { \n";
echo "		if (confirm('".$lang[remimgconf]."'))	{ \n";
echo "			theform.step.value = \"step11\"; \n";
echo "			theform.curchange.value = \"yes\"; \n";
echo "			theform.deletepic.value = pic; \n";
echo "			theform.submit(); \n";
echo "		} else { \n";
echo "			theform.del_chk_mainpic.checked = false; \n";
if ($setts['video_gal_max_size']) echo "theform.del_chk_video.checked = false; \n";
for ($i=0; $i<$setts['pic_gal_max_nb']; $i++) {
	if ($_REQUEST['the_pic'][$i])	echo "theform.del_chk_addpic".$i.".checked = false; \n";
}
echo "		} \n";
echo "	} \n";
echo "</script>";
?>
<script language="JavaScript">
function submitform(theform) {
	theform.step.value = "step11";
	theform.curchange.value = "yes";
	theform.submit();
}
</script>
<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" ID="js17">
var cal17 = new CalendarPopup();
cal17.setReturnFunction("setMultipleValues4");
function setMultipleValues4(y,m,d) {
	document.forms[2].date17_year.value=y;
	document.forms[2].date17_month.selectedIndex=m;
	for (var i=0; i<document.forms[2].date17_date.options.length; i++) {
		if (document.forms[2].date17_date.options[i].value==d) {
			document.forms[2].date17_date.selectedIndex=i;
			}
		}
	}
function getDateString(y_obj,m_obj,d_obj) {
	var y = y_obj.options[y_obj.selectedIndex].value;
	var m = m_obj.options[m_obj.selectedIndex].value;
	var d = d_obj.options[d_obj.selectedIndex].value;
	if (y=="" || m=="") { return null; }
	if (d=="") { d=1; }
	return str= y+'-'+m+'-'+d;
	}
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" ID="js18">
var cal18 = new CalendarPopup();
cal18.setReturnFunction("setMultipleValues5");
function setMultipleValues5(y,m,d) {
	document.forms[2].date18_year.value=y;
	document.forms[2].date18_month.selectedIndex=m;
	for (var i=0; i<document.forms[2].date18_date.options.length; i++) {
		if (document.forms[2].date18_date.options[i].value==d) {
			document.forms[2].date18_date.selectedIndex=i;
			}
		}
	}
function getDateString(y_obj,m_obj,d_obj) {
	var y = y_obj.options[y_obj.selectedIndex].value;
	var m = m_obj.options[m_obj.selectedIndex].value;
	var d = d_obj.options[d_obj.selectedIndex].value;
	if (y=="" || m=="") { return null; }
	if (d=="") { d=1; }
	return str= y+'-'+m+'-'+d;
	

   

	}
</SCRIPT>
<input type="hidden" name="step" value="step12">
<input type="hidden" name="curchange" value="">
<input type="hidden" name="deletepic" value="">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr>
      <td colspan="2" class="c1"><b>
         <?=$lang[auc_settings]?>
         </b></td>
   </tr>
	<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>	
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[auctype]?>
         </strong></td>
      <td><select name="auctiontype" class="contentfont" onChange="submitform(sistep1);">
            <option value="standard" selected>
            <?=$lang[standardauc]?>
            </option>
            <option value="dutch" <? echo ($_REQUEST['auctiontype']=="dutch")?"selected":"";?>>
            <?=$lang[dutchauc]?>
            </option>
         </select></td>
   </tr>
	<? } ?>
   <? if ($_REQUEST['auctiontype']=="standard") $_REQUEST['quantity']=1; ?>
   <!--tr class="c3">
      <td align="right"><strong>
         <?=$lang[auccurr]?>
         </strong></td>
      <? $getcurrencies=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_currencies"); ?>
      <?
      $data = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");
      ?>
      
      <td><select name="currency" class="contentfont" onChange="submitform(sistep1);">
            <? while ($row=mysqli_fetch_array($getcurrencies)) { 
					if ($_REQUEST['currency']!="") echo "<option value=\"".$row['symbol']."\" ".((($_REQUEST['currency']==$row['symbol']) || ($data["mycurrency"]==$row["id"]))?"selected":"").">".$row['symbol']." ".$row['caption']."</option>";
					else  echo "<option value=\"".$row['symbol']."\" ".$row['active'].">".$row['symbol']." ".$row['caption']."</option>";
			  	} ?>
         </select></td>
   </tr-->
	<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[itemsquant]?>
         </strong></td>
      <td><input name="quantity" type="text" class="contentfont" id="quantity" value="<? echo (($_REQUEST['quantity']>=1)?$_REQUEST['quantity']:"1");?>" size="8"  <? echo ($_REQUEST['auctiontype']=="dutch")?"":"readonly"; ?>>
      </td>
   </tr>
	<? } ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[aucstarts]?>
         </strong></td>
      <td><input name="startprice" type="text" class="contentfont" id="startprice" value="<?=$_REQUEST['startprice'];?>" size="8">
         <? echo ($_REQUEST['currency']=="")? $setts['currency'] : $_REQUEST['currency']; ?> 
			<? if ($setts['buyout_process']==0) { ?>
			<br>
         <span class="smallfont">
         <?=$lang[buynowonlyexplanation];?>
         </span>
			<? } ?></td>
   </tr>
   
   <? ## Reserve price option remove - ALEKSEI HODUNKOV
   
   /*<tr class="c3">
      <td align="right"><strong>
         <?=$lang[resprice]?>
         </strong></td>
      <? if ($_REQUEST['auctiontype']=="dutch") $_REQUEST['resprice']=""; ?>
      <td><input name="resprice" type="text" class="contentfont" id="resprice" value="<?=$_REQUEST['resprice'];?>" size="8" <? echo ($_POST[auctiontype]=="dutch")?"disabled":""; ?>>
         <? echo ($_REQUEST['currency']=="")? $setts['currency'] : $_REQUEST['currency']; ?> <? echo ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['val_rp_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"rp"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":""; ?> <a href="javascript:popUpSmall('popup_rp.php');">
         <?=$lang[whatsthis]?>
         </a> </td>
   </tr>*/ ?>
   <? if ($setts['buyout_process']==0) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=ucwords($lang[buynow])?>
         </strong></td>
      <td><input name="buynow" type="radio" value="N" checked>
         <?=$lang[no]?>
         <input type="radio" name="buynow" value="Y" <? echo ($_REQUEST['buynow']=="Y")?"checked":"";?>>
         <?=$lang[yes]?>
         <input name="bnprice" type="text" class="contentfont" id="bnprice" value="<?=$_REQUEST['bnprice'];?>" size="8">
         <? echo ($_REQUEST['currency']=="")? $setts['currency'] : $_REQUEST['currency']; ?> <? echo ($fee['bin_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['bin_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"bn"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":""; ?> <a href="javascript:popUpSmall('popup_bn.php');">
         <?=$lang[whatsthis]?>
         </a> </td>
   </tr>
	<? } else if ($setts['buyout_process']==1) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[makeoffer]?>
         </strong></td>
      <td><input name="buynow" type="radio" value="N" checked>
         <?=$lang[no]?>
         <input type="radio" name="buynow" value="Y" <? echo ($_REQUEST['buynow']=="Y")?"checked":"";?>>
         <?=$lang[yes]?><br />
			<?=$lang[offerrange];?>: 
         <input name="offer_range_min" type="text" class="contentfont" id="offer_range_min" value="<?=$_REQUEST['offer_range_min'];?>" size="8">
			<?=$lang[to];?>
         <input name="offer_range_max" type="text" class="contentfont" id="offer_range_max" value="<?=$_REQUEST['offer_range_max'];?>" size="8">
         <? echo ($_REQUEST['currency']=="")? $setts['currency'] : $_REQUEST['currency']; ?> <? echo ($fee['bin_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['bin_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"bn"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":""; ?> 
			<a href="javascript:popUpSmall('popup_mo.php');">
         <?=$lang[whatsthis]?>
         </a> </td>
   </tr>
	<? } ?>
	<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>	
	<tr class="c3">
      <td align="right"><strong>
         <?=$lang[bidincr]?>
         </strong></td>
      <td><input name="bidinc" type="radio" value="0" checked>
         <?=$lang[builtinincr]?>
         <br>
         <input type="radio" name="bidinc" value="1" <? echo ($_REQUEST['bidinc']==1)?"checked":""; ?>>
         <?=$lang[customincr]?>
         <input name="bidincvalue" type="text" class="contentfont" id="bidincvalue" value="<?=$_REQUEST['bidincvalue'];?>" size="8">
         <? echo ($_REQUEST['currency']=="")? $setts['currency'] : $_REQUEST['currency']; ?> </td>
   </tr>
   <? if ($_REQUEST['listin']!="store"&&($setts['hp_feat']!=0||$setts['cat_feat']!=0||$setts['bold_item']!=0||$setts['hl_item']!=0)) {
   

    ?>

    
   <tr class="c2">
      <td align="right"><strong><?=$lang[accbalance]?>:</strong></td>
      <td>
          <table>
          <tr>
              <td><? echo (($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar;?><div id="req_balance"></div></td>
          </tr>
          <tr>
              <td><span class="smallfont"><?
              if (($val['balance'])>0)
              {
                echo "Teie ei saa kasutada  tasulised teenused. <a href='membersarea.php?page=addmoney'>Raha sissemakse</a>";
              }
              
               ?></span></td>
          </tr>
          </table>
      </td>
   </tr>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[featitem]?>
         </strong></td>
      <td><? 

        echo "<input type=\"checkbox\" name=\"hpfeat\" value=\"Y\" ".(($_REQUEST['hpfeat']=="Y")?"checked":"").">$lang[featdhomepage]"." (+".displayAmount($price["hpfeat"]).")";
        echo "<br /><input type=\"checkbox\" name=\"catfeat\" value=\"Y\" ".(($_REQUEST['catfeat']=="Y")?"checked":"").">$lang[featcatpage]"." (+".displayAmount($price["catfeat"]).")";
        echo "<br /><input type=\"checkbox\" name=\"bolditem\" value=\"Y\" ".(($_REQUEST['bolditem']=="Y")?"checked":"").">$lang[bolditem]"." (+".displayAmount($price["bolditem"]).")";
        echo "<br /><input type=\"checkbox\" name=\"hlitem\" value=\"Y\" ".(($_REQUEST['hlitem']=="Y")?"checked":"").">$lang[highlighteditem]"." (+".displayAmount($price["hlitem"]).")";
      /*
	  		if ($setts['hp_feat']!=0) 
	  			echo "<input type=\"checkbox\" name=\"hpfeat\" value=\"Y\" ".(($_REQUEST['hpfeat']=="Y")?"checked":"").">$lang[featdhomepage] ".
				(($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['val_hpfeat_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"hpfeat"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":"")."<br>";
	  		if ($setts['cat_feat']!=0) 
	  			echo "<input type=\"checkbox\" name=\"catfeat\" value=\"Y\" ".(($_REQUEST['catfeat']=="Y")?"checked":"").">$lang[featcatpage] ".
				(($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['val_catfeat_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"catfeat"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":"")."<br>";
	  		if ($setts['bold_item']!=0) 
	  			echo "<input type=\"checkbox\" name=\"bolditem\" value=\"Y\" ".(($_REQUEST['bolditem']=="Y")?"checked":"").">$lang[bolditem] ".
				(($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['val_bolditem_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"bold"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":"")."<br>";
	  		if ($setts['hl_item']!=0) 
	 	 		echo "<input type=\"checkbox\" name=\"hlitem\" value=\"Y\" ".(($_REQUEST['hlitem']=="Y")?"checked":"").">$lang[highlighteditem] ".
				(($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['val_hlitem_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"hl"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":"")."<br>";
	  		*/
        ?>
      </td>
   </tr>
   <?/* if ($setts['hpfeat_desc']=="Y") { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[hpfeat_desc]?>
         </strong></td>
      <td><textarea name="hpfeat_desc" cols="50" rows="4"><?=$_REQUEST['hpfeat_desc'];?></textarea></td>
   </tr>
   <? } */?>
	<? } ?>
   <? } 
	$_REQUEST['date17_month'] = ($_REQUEST['date17_month']!="") ? $_REQUEST['date17_month'] : $_REQUEST['dmonth']; 
	$_REQUEST['date17_year'] = ($_REQUEST['date17_year']!="") ? $_REQUEST['date17_year'] : $_REQUEST['dyear']; 
	$_REQUEST['date17_date'] = ($_REQUEST['date17_date']!="") ? $_REQUEST['date17_date'] : $_REQUEST['ddate']; 
	
	$_REQUEST['date18_month'] = ($_REQUEST['date18_month']!="") ? $_REQUEST['date18_month'] : $_REQUEST['edmonth']; 
	$_REQUEST['date18_year'] = ($_REQUEST['date18_year']!="") ? $_REQUEST['date18_year'] : $_REQUEST['edyear']; 
	$_REQUEST['date18_date'] = ($_REQUEST['date18_date']!="") ? $_REQUEST['date18_date'] : $_REQUEST['eddate']; ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[auctionstarttime];?>
         </strong></td>
      <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="100"><input name="starttime" type="radio" value="NOW" checked>
                  <?=$lang[now];?></td>
               <td>&nbsp;</td>
            </tr>
				<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>				
            <tr>
               <td><input name="starttime" type="radio" value="custom" <? echo ($_REQUEST['starttime']=="custom")?"checked":"";?>>
                  <?=$lang[customtime];?></td>
               <td><select name="date17_month" id="date17_month" class="contentfont">
                     <option> </option>
                     <option value="01" <? echo ($_REQUEST['date17_month']==1)?"selected":"";?>>
                     <?=$lang[jan]?>
                     </option>
                     <option value="02" <? echo ($_REQUEST['date17_month']==2)?"selected":"";?>>
                     <?=$lang[feb]?>
                     </option>
                     <option value="03" <? echo ($_REQUEST['date17_month']==3)?"selected":"";?>>
                     <?=$lang[mar]?>
                     </option>
                     <option value="04" <? echo ($_REQUEST['date17_month']==4)?"selected":"";?>>
                     <?=$lang[apr]?>
                     </option>
                     <option value="05" <? echo ($_REQUEST['date17_month']==5)?"selected":"";?>>
                     <?=$lang[may]?>
                     </option>
                     <option value="06" <? echo ($_REQUEST['date17_month']==6)?"selected":"";?>>
                     <?=$lang[jun]?>
                     </option>
                     <option value="07" <? echo ($_REQUEST['date17_month']==7)?"selected":"";?>>
                     <?=$lang[jul]?>
                     </option>
                     <option value="08" <? echo ($_REQUEST['date17_month']==8)?"selected":"";?>>
                     <?=$lang[aug]?>
                     </option>
                     <option value="09" <? echo ($_REQUEST['date17_month']==9)?"selected":"";?>>
                     <?=$lang[sep]?>
                     </option>
                     <option value="10" <? echo ($_REQUEST['date17_month']==10)?"selected":"";?>>
                     <?=$lang[oct]?>
                     </option>
                     <option value="11" <? echo ($_REQUEST['date17_month']==11)?"selected":"";?>>
                     <?=$lang[nov]?>
                     </option>
                     <option value="12" <? echo ($_REQUEST['date17_month']==12)?"selected":"";?>>
                     <?=$lang[dec]?>
                     </option>
                  </select>
                  <select name="date17_date" class="contentfont">
                     <option> </option>
                     <option value="01" <? echo ($_REQUEST['date17_date']==1)?"selected":"";?>>1 </option>
                     <option value="02" <? echo ($_REQUEST['date17_date']==2)?"selected":"";?>>2 </option>
                     <option value="03" <? echo ($_REQUEST['date17_date']==3)?"selected":"";?>>3 </option>
                     <option value="04" <? echo ($_REQUEST['date17_date']==4)?"selected":"";?>>4 </option>
                     <option value="05" <? echo ($_REQUEST['date17_date']==5)?"selected":"";?>>5 </option>
                     <option value="06" <? echo ($_REQUEST['date17_date']==6)?"selected":"";?>>6 </option>
                     <option value="07" <? echo ($_REQUEST['date17_date']==7)?"selected":"";?>>7 </option>
                     <option value="08" <? echo ($_REQUEST['date17_date']==8)?"selected":"";?>>8 </option>
                     <option value="09" <? echo ($_REQUEST['date17_date']==9)?"selected":"";?>>9 </option>
                     <option value="10" <? echo ($_REQUEST['date17_date']==10)?"selected":"";?>>10 </option>
                     <option value="11" <? echo ($_REQUEST['date17_date']==11)?"selected":"";?>>11 </option>
                     <option value="12" <? echo ($_REQUEST['date17_date']==12)?"selected":"";?>>12 </option>
                     <option value="13" <? echo ($_REQUEST['date17_date']==13)?"selected":"";?>>13 </option>
                     <option value="14" <? echo ($_REQUEST['date17_date']==14)?"selected":"";?>>14 </option>
                     <option value="15" <? echo ($_REQUEST['date17_date']==15)?"selected":"";?>>15 </option>
                     <option value="16" <? echo ($_REQUEST['date17_date']==16)?"selected":"";?>>16 </option>
                     <option value="17" <? echo ($_REQUEST['date17_date']==17)?"selected":"";?>>17 </option>
                     <option value="18" <? echo ($_REQUEST['date17_date']==18)?"selected":"";?>>18 </option>
                     <option value="19" <? echo ($_REQUEST['date17_date']==19)?"selected":"";?>>19 </option>
                     <option value="20" <? echo ($_REQUEST['date17_date']==20)?"selected":"";?>>20 </option>
                     <option value="21" <? echo ($_REQUEST['date17_date']==21)?"selected":"";?>>21 </option>
                     <option value="22" <? echo ($_REQUEST['date17_date']==22)?"selected":"";?>>22 </option>
                     <option value="23" <? echo ($_REQUEST['date17_date']==23)?"selected":"";?>>23 </option>
                     <option value="24" <? echo ($_REQUEST['date17_date']==24)?"selected":"";?>>24 </option>
                     <option value="25" <? echo ($_REQUEST['date17_date']==25)?"selected":"";?>>25 </option>
                     <option value="26" <? echo ($_REQUEST['date17_date']==26)?"selected":"";?>>26 </option>
                     <option value="27" <? echo ($_REQUEST['date17_date']==27)?"selected":"";?>>27 </option>
                     <option value="28" <? echo ($_REQUEST['date17_date']==28)?"selected":"";?>>28 </option>
                     <option value="29" <? echo ($_REQUEST['date17_date']==29)?"selected":"";?>>29 </option>
                     <option value="30" <? echo ($_REQUEST['date17_date']==30)?"selected":"";?>>30 </option>
                     <option value="31" <? echo ($_REQUEST['date17_date']==31)?"selected":"";?>>31 </option>
                  </select>
                  <select name="date17_year" class="contentfont">
                     <option> </option>

                     <option value="2010" <? echo ($_REQUEST['date17_year']==2010)?"selected":"";?>>2010 </option>
                      <OPTION VALUE="2011" <? echo ($_REQUEST['date17_year']==2011)?"selected":"";?>>2011
                     <OPTION VALUE="2012" <? echo ($_REQUEST['date17_year']==2012)?"selected":"";?>>2012
                     <OPTION VALUE="2013" <? echo ($_REQUEST['date17_year']==2013)?"selected":"";?>>2013    
                     <OPTION VALUE="2014" <? echo ($_REQUEST['date17_year']==2014)?"selected":"";?>>2014  
                     <OPTION VALUE="2015" <? echo ($_REQUEST['date17_year']==2015)?"selected":"";?>>2015                        
                  </select>
                  <a href="#" onclick="cal17.showCalendar('anchor17',getDateString(document.forms[2].date17_year,document.forms[2].date17_month,document.forms[2].date17_date)); return false;" title="cal17.showCalendar('anchor17',getDateString(document.forms[2].date17_year,document.forms[2].date17_month,document.forms[2].date17_date)); return false;" name="anchor17" id="anchor17"><img src="themes/<?=$setts['default_theme'];?>/img/system/calendar_b2u.gif" border="0" align="absmiddle" /></a>
                  <select name="shour"  class="contentfont" id="shour">
                     <option value="0" selected="selected">00</option>
                     <option value="1" <? echo (($_REQUEST['shour']==1)?"selected":"");?>>01</option>
                     <option value="2" <? echo (($_REQUEST['shour']==2)?"selected":"");?>>02</option>
                     <option value="3" <? echo (($_REQUEST['shour']==3)?"selected":"");?>>03</option>
                     <option value="4" <? echo (($_REQUEST['shour']==4)?"selected":"");?>>04</option>
                     <option value="5" <? echo (($_REQUEST['shour']==5)?"selected":"");?>>05</option>
                     <option value="6" <? echo (($_REQUEST['shour']==6)?"selected":"");?>>06</option>
                     <option value="7" <? echo (($_REQUEST['shour']==7)?"selected":"");?>>07</option>
                     <option value="8" <? echo (($_REQUEST['shour']==8)?"selected":"");?>>08</option>
                     <option value="9" <? echo (($_REQUEST['shour']==9)?"selected":"");?>>09</option>
                     <option value="10" <? echo (($_REQUEST['shour']==10)?"selected":"");?>>10</option>
                     <option value="11" <? echo (($_REQUEST['shour']==11)?"selected":"");?>>11</option>
                     <option value="12" <? echo (($_REQUEST['shour']==12)?"selected":"");?>>12</option>
                     <option value="13" <? echo (($_REQUEST['shour']==13)?"selected":"");?>>13</option>
                     <option value="14" <? echo (($_REQUEST['shour']==14)?"selected":"");?>>14</option>
                     <option value="15" <? echo (($_REQUEST['shour']==15)?"selected":"");?>>15</option>
                     <option value="16" <? echo (($_REQUEST['shour']==16)?"selected":"");?>>16</option>
                     <option value="17" <? echo (($_REQUEST['shour']==17)?"selected":"");?>>17</option>
                     <option value="18" <? echo (($_REQUEST['shour']==18)?"selected":"");?>>18</option>
                     <option value="19" <? echo (($_REQUEST['shour']==19)?"selected":"");?>>19</option>
                     <option value="20" <? echo (($_REQUEST['shour']==20)?"selected":"");?>>20</option>
                     <option value="21" <? echo (($_REQUEST['shour']==21)?"selected":"");?>>21</option>
                     <option value="22" <? echo (($_REQUEST['shour']==22)?"selected":"");?>>22</option>
                     <option value="23" <? echo (($_REQUEST['shour']==23)?"selected":"");?>>23</option>
                  </select>
                  :
                  <select name="sminute" class="contentfont" id="sminute">
                     <option value="00" selected="selected">00</option>
                     <option value="15" <? echo (($_REQUEST['sminute']==15)?"selected":"");?>>15</option>
                     <option value="30" <? echo (($_REQUEST['sminute']==30)?"selected":"");?>>30</option>
                     <option value="45" <? echo (($_REQUEST['sminute']==45)?"selected":"");?>>45</option>
                  </select>
                  <? echo ($fee['custom_st_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['custom_st_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"customst"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":""; ?></td>
            </tr>
				<? } ?>
         </table></td>
   </tr>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[endtime]?>
         </strong></td>
      <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="100"><input name="endtime" type="radio" value="duration" checked="checked" />
                  <?=$lang[duration];?></td>
               <td><? echo "<SELECT name=\"duration\" class=\"contentfont\">";
				  		$getdurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations");
				  		while ($row=mysqli_fetch_array($getdurations)) {
					  		echo "<OPTION value=\"".$row['days']."\" ".(($row['days']==$_REQUEST['duration'])?"SELECTED":"").">".$row['description']."</option>";
				  		}
				  		echo "</SELECT>";
				  		?></td>
            </tr>
				<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>
            <tr>
               <td><input name="endtime" type="radio" value="customtime" <? echo ($_REQUEST['endtime']=="customtime") ? "checked" : ""; ?> />
                  <?=$lang[customtime];?></td>
               <td><SELECT NAME="date18_month" id="date18_month" class="contentfont">
                     <OPTION>
                     <OPTION VALUE="01" <? echo ($_REQUEST['date18_month']==1)?"selected":"";?>>
                     <?=$lang[jan]?>
                     <OPTION VALUE="02" <? echo ($_REQUEST['date18_month']==2)?"selected":"";?>>
                     <?=$lang[feb]?>
                     <OPTION VALUE="03" <? echo ($_REQUEST['date18_month']==3)?"selected":"";?>>
                     <?=$lang[mar]?>
                     <OPTION VALUE="04" <? echo ($_REQUEST['date18_month']==4)?"selected":"";?>>
                     <?=$lang[apr]?>
                     <OPTION VALUE="05" <? echo ($_REQUEST['date18_month']==5)?"selected":"";?>>
                     <?=$lang[may]?>
                     <OPTION VALUE="06" <? echo ($_REQUEST['date18_month']==6)?"selected":"";?>>
                     <?=$lang[jun]?>
                     <OPTION VALUE="07" <? echo ($_REQUEST['date18_month']==7)?"selected":"";?>>
                     <?=$lang[jul]?>
                     <OPTION VALUE="08" <? echo ($_REQUEST['date18_month']==8)?"selected":"";?>>
                     <?=$lang[aug]?>
                     <OPTION VALUE="09" <? echo ($_REQUEST['date18_month']==9)?"selected":"";?>>
                     <?=$lang[sep]?>
                     <OPTION VALUE="10" <? echo ($_REQUEST['date18_month']==10)?"selected":"";?>>
                     <?=$lang[oct]?>
                     <OPTION VALUE="11" <? echo ($_REQUEST['date18_month']==11)?"selected":"";?>>
                     <?=$lang[nov]?>
                     <OPTION VALUE="12" <? echo ($_REQUEST['date18_month']==12)?"selected":"";?>>
                     <?=$lang[dec]?>
                  </SELECT>
                  <SELECT NAME="date18_date" class="contentfont">
                     <OPTION>
                     <OPTION VALUE="01" <? echo ($_REQUEST['date18_date']==1)?"selected":"";?>>1
                     <OPTION VALUE="02" <? echo ($_REQUEST['date18_date']==2)?"selected":"";?>>2
                     <OPTION VALUE="03" <? echo ($_REQUEST['date18_date']==3)?"selected":"";?>>3
                     <OPTION VALUE="04" <? echo ($_REQUEST['date18_date']==4)?"selected":"";?>>4
                     <OPTION VALUE="05" <? echo ($_REQUEST['date18_date']==5)?"selected":"";?>>5
                     <OPTION VALUE="06" <? echo ($_REQUEST['date18_date']==6)?"selected":"";?>>6
                     <OPTION VALUE="07" <? echo ($_REQUEST['date18_date']==7)?"selected":"";?>>7
                     <OPTION VALUE="08" <? echo ($_REQUEST['date18_date']==8)?"selected":"";?>>8
                     <OPTION VALUE="09" <? echo ($_REQUEST['date18_date']==9)?"selected":"";?>>9
                     <OPTION VALUE="10" <? echo ($_REQUEST['date18_date']==10)?"selected":"";?>>10
                     <OPTION VALUE="11" <? echo ($_REQUEST['date18_date']==11)?"selected":"";?>>11
                     <OPTION VALUE="12" <? echo ($_REQUEST['date18_date']==12)?"selected":"";?>>12
                     <OPTION VALUE="13" <? echo ($_REQUEST['date18_date']==13)?"selected":"";?>>13
                     <OPTION VALUE="14" <? echo ($_REQUEST['date18_date']==14)?"selected":"";?>>14
                     <OPTION VALUE="15" <? echo ($_REQUEST['date18_date']==15)?"selected":"";?>>15
                     <OPTION VALUE="16" <? echo ($_REQUEST['date18_date']==16)?"selected":"";?>>16
                     <OPTION VALUE="17" <? echo ($_REQUEST['date18_date']==17)?"selected":"";?>>17
                     <OPTION VALUE="18" <? echo ($_REQUEST['date18_date']==18)?"selected":"";?>>18
                     <OPTION VALUE="19" <? echo ($_REQUEST['date18_date']==19)?"selected":"";?>>19
                     <OPTION VALUE="20" <? echo ($_REQUEST['date18_date']==20)?"selected":"";?>>20
                     <OPTION VALUE="21" <? echo ($_REQUEST['date18_date']==21)?"selected":"";?>>21
                     <OPTION VALUE="22" <? echo ($_REQUEST['date18_date']==22)?"selected":"";?>>22
                     <OPTION VALUE="23" <? echo ($_REQUEST['date18_date']==23)?"selected":"";?>>23
                     <OPTION VALUE="24" <? echo ($_REQUEST['date18_date']==24)?"selected":"";?>>24
                     <OPTION VALUE="25" <? echo ($_REQUEST['date18_date']==25)?"selected":"";?>>25
                     <OPTION VALUE="26" <? echo ($_REQUEST['date18_date']==26)?"selected":"";?>>26
                     <OPTION VALUE="27" <? echo ($_REQUEST['date18_date']==27)?"selected":"";?>>27
                     <OPTION VALUE="28" <? echo ($_REQUEST['date18_date']==28)?"selected":"";?>>28
                     <OPTION VALUE="29" <? echo ($_REQUEST['date18_date']==29)?"selected":"";?>>29
                     <OPTION VALUE="30" <? echo ($_REQUEST['date18_date']==30)?"selected":"";?>>30
                     <OPTION VALUE="31" <? echo ($_REQUEST['date18_date']==31)?"selected":"";?>>31
                  </SELECT>
                  <SELECT NAME="date18_year" class="contentfont">
                     <OPTION>
                     <OPTION VALUE="2010" <? echo ($_REQUEST['date18_year']==2010)?"selected":"";?>>2010
                     <OPTION VALUE="2011" <? echo ($_REQUEST['date18_year']==2011)?"selected":"";?>>2011
                     <OPTION VALUE="2012" <? echo ($_REQUEST['date18_year']==2012)?"selected":"";?>>2012
                     <OPTION VALUE="2013" <? echo ($_REQUEST['date18_year']==2013)?"selected":"";?>>2013    
                     <OPTION VALUE="2014" <? echo ($_REQUEST['date18_year']==2014)?"selected":"";?>>2014  
                     <OPTION VALUE="2015" <? echo ($_REQUEST['date18_year']==2015)?"selected":"";?>>2015                                                             
                  </SELECT>
                  <A HREF="#" onClick="cal18.showCalendar('anchor18',getDateString(document.forms[2].date18_year,document.forms[2].date18_month,document.forms[2].date18_date)); return false;" TITLE="cal18.showCalendar('anchor18',getDateString(document.forms[2].date18_year,document.forms[2].date18_month,document.forms[2].date18_date)); return false;" NAME="anchor18" ID="anchor18"><img src="themes/<?=$setts['default_theme'];?>/img/system/calendar_b2u.gif" border="0" align="absmiddle"></A>
                  <select name="eshour"  class="contentfont" id="eshour">
                     <option value="0" selected>00</option>
                     <option value="1" <? echo (($_REQUEST['eshour']==1)?"selected":"");?>>01</option>
                     <option value="2" <? echo (($_REQUEST['eshour']==2)?"selected":"");?>>02</option>
                     <option value="3" <? echo (($_REQUEST['eshour']==3)?"selected":"");?>>03</option>
                     <option value="4" <? echo (($_REQUEST['eshour']==4)?"selected":"");?>>04</option>
                     <option value="5" <? echo (($_REQUEST['eshour']==5)?"selected":"");?>>05</option>
                     <option value="6" <? echo (($_REQUEST['eshour']==6)?"selected":"");?>>06</option>
                     <option value="7" <? echo (($_REQUEST['eshour']==7)?"selected":"");?>>07</option>
                     <option value="8" <? echo (($_REQUEST['eshour']==8)?"selected":"");?>>08</option>
                     <option value="9" <? echo (($_REQUEST['eshour']==9)?"selected":"");?>>09</option>
                     <option value="10" <? echo (($_REQUEST['eshour']==10)?"selected":"");?>>10</option>
                     <option value="11" <? echo (($_REQUEST['eshour']==11)?"selected":"");?>>11</option>
                     <option value="12" <? echo (($_REQUEST['eshour']==12)?"selected":"");?>>12</option>
                     <option value="13" <? echo (($_REQUEST['eshour']==13)?"selected":"");?>>13</option>
                     <option value="14" <? echo (($_REQUEST['eshour']==14)?"selected":"");?>>14</option>
                     <option value="15" <? echo (($_REQUEST['eshour']==15)?"selected":"");?>>15</option>
                     <option value="16" <? echo (($_REQUEST['eshour']==16)?"selected":"");?>>16</option>
                     <option value="17" <? echo (($_REQUEST['eshour']==17)?"selected":"");?>>17</option>
                     <option value="18" <? echo (($_REQUEST['eshour']==18)?"selected":"");?>>18</option>
                     <option value="19" <? echo (($_REQUEST['eshour']==19)?"selected":"");?>>19</option>
                     <option value="20" <? echo (($_REQUEST['eshour']==20)?"selected":"");?>>20</option>
                     <option value="21" <? echo (($_REQUEST['eshour']==21)?"selected":"");?>>21</option>
                     <option value="22" <? echo (($_REQUEST['eshour']==22)?"selected":"");?>>22</option>
                     <option value="23" <? echo (($_REQUEST['eshour']==23)?"selected":"");?>>23</option>
                  </select>
                  :
                  <select name="esminute" class="contentfont" id="esminute">
                     <option value="00" selected>00</option>
                     <option value="15" <? echo (($_REQUEST['esminute']==15)?"selected":"");?>>15</option>
                     <option value="30" <? echo (($_REQUEST['esminute']==30)?"selected":"");?>>30</option>
                     <option value="45" <? echo (($_REQUEST['esminute']==45)?"selected":"");?>>45</option>
                  </select>
               </td>
            </tr>
				<? } ?>
         </table></td>
   </tr>
	<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[privateauc]?>
         </strong></td>
      <td><select name="privateauct" id="privateauct" class="contentfont">
            <option value="Y" <? echo (($_REQUEST['privateauct']=="Y")?"selected":"");?>>
            <?=$lang[yes]?>
            </option>
            <option value="N" <? echo (($_REQUEST['privateauct']=="N"||$_REQUEST['privateauct']=="")?"selected":"");?>>
            <?=$lang[no]?>
            </option>
         </select>
         <a href="javascript:popUpSmall('popup_pa.php');">
         <?=$lang[whatsthis]?>
         </a> </td>
   </tr>
   <? if ($setts['swap_items']==1) { ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[acceptswap]?>
         </strong></td>
      <td colspan><input name="isswap" type="radio" value="N" checked>
         <?=$lang[no]?>
         <input type="radio" name="isswap" value="Y" <? echo(($_REQUEST['isswap']=="Y")?"checked":"");?>>
         <?=$lang[yes]?></td>
   </tr>
   <? } ?>
	<? } ?>
   <? 
  	$mainCat_primary = getMainCat($_REQUEST['category']);
  	$mainCat_secondary = getMainCat($_REQUEST['addlcategory']);
  	
	if (@eregi('quick', $_REQUEST['listing_type'])) $addFieldsQuery = " AND mandatory=1";
	
  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, active FROM probid_fields 
  	WHERE	(categoryid='".$mainCat_primary."' OR categoryid='".$mainCat_secondary."' OR categoryid='0') ".$addFieldsQuery." 
 	ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  	$isFields = mysqli_num_rows($getFields);
  	if ($isFields) {
  	?>
   <!-- Additional Custom Fields -->
   <tr>
      <td colspan=2 class="c1"><b>
         <?=$lang[addfields]?>
         </b></td>
   </tr>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? 
	while ($fields=mysqli_fetch_array($getFields)) { ?>
   <tr class="<? echo (($count++)%2==0)?"c3":"c2"; ?>">
      <td align="right"><strong>
         <?=$fields['boxname'];?>
         </strong></td>
      <td><? echo createField($fields['boxid'],$_REQUEST['box'.$fields['boxid']]); ?> </td>
   </tr>
   <? } 
  	} ?>
   <!-- Auto Relist Settings -->
	<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>
   <tr>
      <td colspan=2 class="c1"><b>
         <?=$lang[auto_relist_title]?>
         </b></td>
   </tr>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[auto_relist];?>
         </strong></td>
      <td><input name="auto_relist" type="radio" value="N" checked>
         <?=$lang[no]?>
         <input type="radio" name="auto_relist" value="Y" <? echo(($_REQUEST['auto_relist']=="Y")?"checked":"");?>>
         <?=$lang[yes]?></td>
   </tr>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[auto_relist_bids];?>
         </strong></td>
      <td><input name="auto_relist_bids" type="radio" value="N" checked>
         <?=$lang[no]?>
         <input type="radio" name="auto_relist_bids" value="Y" <? echo(($_REQUEST['auto_relist_bids']=="Y")?"checked":"");?>>
         <?=$lang[yes]?>
         <br>
         <?=$lang[auto_relist_msg];?></td>
   </tr>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[auto_relist_nb];?>
         </strong></td>
      <td><input name="auto_relist_nb_tmp" type="text" value="<?=(($_REQUEST['auto_relist_nb']>0) ? $_REQUEST['auto_relist_nb'] : 1);?>" size="8" onchange="revertMaxRelist(sistep1);">
         <br>
         <?=$lang[auto_relist_nb_msg];?></td>
   </tr>
	<? } ?>
   <tr>
      <td colspan=2 class="c1"><b>
         <?=$lang[aucimages]?>
         </b></td>
   </tr>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[uploadpic]?>
         </strong><br>
         <?=$lang[optfield]?></td>
      <td><table border="0" cellspacing="2" cellpadding="2">
            <tr>
               <td><input type="file" name="file">
                  <br>
                  <b>
                  <?=$lang['orurl']?>
                  </b>http://
                  <input type="text" name="mainpicurl"></td>
               <? if (!empty($_REQUEST['mainpic'])) { ?>
               <td><? echo "&nbsp;$lang[current_pic]: <img src=\"makethumb.php?pic=".$_REQUEST['mainpic']."&w=50&sq=Y&b=Y\" align=absmiddle> ";?> </td>
               <td><? echo $lang[delete].": <input type=\"checkbox\" id=\"del_chk_mainpic\" onClick=\"delete_pic(sistep1,'mainpic');\"> "; ?></td>
               <? } ?>
            </tr>
         </table></td>
   </tr>
   <? if ($setts['pic_gal_active']==1) { ?>
   <tr class="c2">
      <td align="right" valign="top"><strong>
         <?=$lang[picturegallery]?>
         .</strong><br>
         <? echo ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['val_pic_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"pic"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":""; ?> <br>
         <?=$lang[youcanupload1]?>
         <?=$setts['pic_gal_max_nb'];?>
         <?=$lang[youcanupload2]?></td>
      <td valign="top"><? 
			for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
				echo "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\"> \n";
				echo "	<tr> \n";
				echo "		<td><input type=\"file\" name=\"addfile[]\"> \n";
				echo '		<br><b>'.$lang['orurl'].' </b>http:// <input type="text" name="picurl['.$i.']"></td>';
				if (!empty($_REQUEST['the_pic'][$i])) {
					echo "<td>&nbsp;$lang[current_pic]: <img src=\"makethumb.php?pic=".$_REQUEST['the_pic'][$i]."&w=50&sq=Y&b=Y\" align=absmiddle></td>";
	      		echo "<td>".$lang[delete].": <input type=\"checkbox\" id=\"del_chk_addpic".$i."\" onClick=\"delete_pic(sistep1,'addpic_".$i."');\"></td> ";
				}
				echo "	</tr>";
				echo "</table>";
			} ?>
         <?=$lang[youcanupload3]?></td>
   </tr>
   <? } ?>
   <tr class="c3">
      <td align="right" valign="top">&nbsp;</td>
      <td valign="top">
			<span style="color: red;"><?=$lang[imguplwarning];?></span><br />
			<?=$lang[imgmax_note1]." ".$setts['pic_gal_max_size']."KB";?>
         <? echo ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&!freeFees($_SESSION['memberid'])) ?"<br>$lang[picpaymentnote]":""; ?> </td>
   </tr>
	<? if (!@eregi('quick', $_REQUEST['listing_type'])) { ?>
	<? if ($setts['video_gal_max_size']>0) { ?>
   <tr>
      <td colspan=2 class="c1"><b>
         <?=$lang[movieupl]?>
         </b></td>
   </tr>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[uploadmov]?>
         </strong><br />         
			<? echo ($fee['videofile_fee']>0&&!freeFees($_SESSION['memberid'])) ?"(+ ".displayAmount(applyVat(calcReduction($fee['videofile_fee'],$prefSeller,trim($_REQUEST['voucher_code']),"video"),$_SESSION['memberid']),$setts['currency'],TRUE).") ":""; ?> <br>
         <?=$lang[optfield]?></td>
      <td><table border="0" cellspacing="2" cellpadding="2">
            <tr>
               <td><input type="file" name="videofile">
                  <br>
                  <b>
                  <?=$lang['orurl']?>
                  </b>http://
                  <input type="text" name="videofileurl"></td>
               <? if (!empty($_REQUEST['videofile_path'])) { ?>
               <td class="smallfont"><? echo "&nbsp;[ <strong>$lang[movieuploaded]</strong>] &nbsp;";?> </td>
               <td><? echo $lang[delete].": <input type=\"checkbox\" id=\"del_chk_video\" onClick=\"delete_pic(sistep1,'movie');\"> "; ?></td>
               <? } ?>
            </tr>
         </table></td>
   </tr>
   <tr class="c3">
      <td align="right" valign="top">&nbsp;</td>
      <td valign="top"><?=$lang[videomax_note1]." ".$setts['video_gal_max_size']."KB";?></td>
   </tr>
	<? } ?>
	<? } ?>
   <tr>
      <td colspan="2" class="c1"><b>
         <?=$lang[location]?>
         </b></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <?
  	$userDets = getSqlRow("SELECT zip, country FROM probid_users WHERE id='".$_SESSION['memberid']."'");
	$country = ($_POST['country']=="") ? $userDets['country'] : $_POST['country'];
	$_REQUEST['zip'] = ($_REQUEST['zip']=="") ? $userDets['zip'] : $_REQUEST['zip'];
  	?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[country]?>
         </strong></td>
      <td><? echo "<SELECT name=\"country\" class=\"contentfont\">";
	  		$getdurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries ORDER BY theorder ASC, name ASC");
	  		while ($row=mysqli_fetch_array($getdurations)) {
		  		echo "<OPTION value=\"".$row['name']."\" ".(($row['name']==$country)?"SELECTED":"").">".$row['name']."</option>";
	  		}
	  		echo "</SELECT>";
	  	?> </td>
   </tr>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[zip]?>
         </strong></td>
      <td><input name="zip" type="text" id="zip" value="<?=$_REQUEST['zip'];?>" size="12"></td>
   </tr>
   <tr class="c4">
      <td colspan="2" align="center"><? echo displayNavButtons(); ?></td>
   </tr>
</table>
<? if ($_REQUEST['auction']=="dutch") {?>
<input type="hidden" name="respr" value="N">
<input type="hidden" name="buynow" value="N">
<? } ?>
<? if (@eregi('quick', $_REQUEST['listing_type'])) {?>
<input type="hidden" name="auctiontype" value="standard">
<input type="hidden" name="quantity" value="1">
<input type="hidden" name="privateauct" value="N">
<input type="hidden" name="isswap" value="N">
<input type="hidden" name="auto_relist" value="N">
<input type="hidden" name="auto_relist_bids" value="N">
<input type="hidden" name="bidinc" value="0">
<? } ?>
<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p><p>&nbsp;</p>"; } ?>
