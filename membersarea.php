<?
## v5.24 -> apr. 12, 2006
session_start();
if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");
if ($userDetails['mailactivated']==0&&$userDetails['active']!=1&&(($setts['account_mode_personal']==0 && $setts['account_mode']==2) || ($setts['account_mode_personal']==1 && $userDetails['payment_mode']==1))) {
	include ("themes/".$setts['default_theme']."/header.php");
	header5 ($lang[erroraccnotactive]);
	echo "<p align=\"center\" class=\"contentfont\"><strong>$lang[accactivatemsg]</strong></p>";
	include ("themes/".$setts['default_theme']."/footer.php");			
	exit;
}	

if (isset($_POST['previewsfok'])) {
	$updateShopPreview = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	shop_mainpage_preview='".$_POST['shop_mainpage']."' WHERE id='".$_SESSION['memberid']."'");
	echo "<script>document.location.href=\"aboutme.preview.php?userid=".$_SESSION['memberid']."&aboutpage_type=".$_REQUEST['aboutpage_type']."&aboutpage=".$_REQUEST['aboutpage']."\"</script>";
}

$isSent = getSqlField("SELECT * FROM probid_gen_setts","addr_val");
if ($isSent != 1) {
	$refsite=getenv("HTTP_REFERER");
	$lkey = getSqlNumber("SELECT * FROM probid_gen_setts","lkey");
	mail ("admin@online-ventures.co.uk","New ProBid v2 Installation - ".date("M. j, Y",time()),
	"A new PHP Pro Bid v2 installation was reported on:

	License Key: $lkey
	Admin Email Address: $setts[adminemail]
	Server IP: $_SERVER[SERVER_ADDR]
	Site URL: $_SERVER[SERVER_NAME]
	Request URI: $_SERVER[REQUEST_URI]","From: noreply@phpprobid.com <noreply@phpprobid.com>");

	$updAddr = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_gen_setts SET addr_val=1");
}


include ("themes/".$setts['default_theme']."/header.php");

if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {
	$page = $_REQUEST['page']; 
	// first there is one more thing when the accsusp can be 2, when the debit is over the max limit
	if ($page=="") {
		if ($_SESSION['accsusp']==2) {
			$page="preferences";
		} else {
			$page="bidding"; 
		}
	}
	/*header7("<b>".$lang[memarea_title]."</b>");*/

	include("membersmenu.php");

	if ($_SESSION['accsusp']!=2) {
		switch ($page) {
			case "aboutme":
				include ("m_aboutme.php");
				break;
			case "store":
				include ("m_store.php");
				break;
			case "bidding":
				include ("m_bidding.php");
				break;
			case "selling":
				include ("m_selling.php");
				break;
			case "feedback":
				include ("m_feedback.php");
				break;
			case "bulk":
				include ("m_bulk.php");
				break;
			case "wanted":
				include ("m_wanted.php");
				break;
			case "money":
				include ("m_money.php");
				break;
			case "messages":
				include ("m_inbox.php");
				break;
			case "inbox":
				include ("m_inbox.php");
				break;
			case "sent":
				include ("m_sent.php");
				break;
			case "projects":
				include ("m_projects.php");
				break;
			case "addmoney":
				include ("m_addmoney.php");
				break;
			case "invite_friends":
			   include ("m_myfriends.php");
			   break;
			case "withdrawmoney":
			   include ("m_withdrawmoney.php");
			   break;
			case "transactions":
			   include ("m_transactions.php");
			  break;
			case "marketizer":
			   include ("m_marketizer.php");
			  break;
			case "bankdetails":
				include ("m_bankdetails.php");
				break;
			case "preferences":
				include ("m_prefs.php");
				break;
			case "store_pages":
				include ("m_store_pages.php");
				break;
			case "cats_management":
				include ("m_store_cats_management.php");
				break;
			case "feescalculator":
				include ("m_feescalculator.php");
				break;
			case "block_users":
				include ("m_blockusers.php");
				break;
			case "invite_friends":
			  include ("m_myfriends.php");
		}
	} else include ("m_prefs.php");

	echo"<br>"; 
} else { 
	header5($lang[memarea_title]);
	echo "<p align=center class=errorfont>".$lang[err_relogin]."</p>"; 
} 

include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
