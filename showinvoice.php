<?
## v5.24 -> apr. 06, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

if ($_SESSION['memberid']>0) {
	$userid = $_SESSION['memberid'];
} else {
   header("Location: /");
   exit;
}

$winnerid = ( intval($_REQUEST['winnerid'])) ? intval($_REQUEST['winnerid']) : -1;

$item        = getSqlRow("SELECT * FROM probid_winners WHERE id='".$winnerid."'");
$itemdetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$item['auctionid']."'");
$buyer       = getSqlRow("SELECT * FROM probid_users WHERE id = '".$item['buyerid']."'");
$seller      = getSqlRow("SELECT * FROM probid_users WHERE id = '".$item['sellerid']."'");


if(!$item['invoice_sent']){
   // what are you doing here ????
   if($_SESSION['memberid'] == $buyer['id']){
		header("Location: /");
		exit;
	}
}

if (!$_SESSION['sess_lang']) {
	include ("config/lang/".$setts['default_lang']."/site.lang");
	$_SESSION['sess_lang']="".$setts['default_lang']."";
} else {
	include ("config/lang/".$_SESSION['sess_lang']."/site.lang");
}

?>
<html>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lang[codepage];?>">
<body>
<?
/*
if($buyer['country']==='United States'){
    $lang[invp_vat_rate] = $lang[invp_vat_rate_usa];
    $lang[invp_vat] = $lang[invp_vat_usa];
    $lang[invp_tev] = $lang[invp_tev_usa];
    $lang[invp_tiv] = $lang[invp_tiv_usa];
    $lang[invp_vrn] = $lang[invp_vrn_usa];
    $lang[invp_ovrn] = $lang[invp_ovrn_usa];
}
*/

// GET VAT INCLUDING FLAG
$vatrate = 0;
$item['vat_included'] = 0;
if($itemdetails['apply_vat']==1){
	// get VAT permissions
   $country_id = getSqlField("SELECT id FROM probid_countries WHERE name='".$seller['country']."'",'id');
	$user_vat = getSqlRow("SELECT * FROM probid_vat_setts WHERE LOCATE(',".$country_id.",',CONCAT(',',countries,','))>0");
	if($user_vat){
   	$user_types = explode(',',$user_vat['users_no_vat']);
      if(($buyer['companyname']=="")&&($buyer['vat_uid_number']==""))$user_type='d';
		if(($buyer['companyname']=="")&&($buyer['vat_uid_number']!=""))$user_type='c';
		if(($buyer['companyname']!="")&&($buyer['vat_uid_number']==""))$user_type='b';
		if(($buyer['companyname']!="")&&($buyer['vat_uid_number']!=""))$user_type='a';
		if(!in_array($user_type,$user_types)){
      	$vatrate = getSqlField("SELECT rate FROM probid_vat_rates WHERE vat_id = '".$user_vat['id']."'", 'rate');
         $item['vat_included'] = 1;
         $lang[invp_vat_rate] = str_replace('TAX',$user_vat['symbol'],$lang[invp_vat_rate]);
         $lang[invp_vat] = str_replace('TAX',$user_vat['symbol'],$lang[invp_vat]);
         $lang[invp_tev] = str_replace('TAX',$user_vat['symbol'],$lang[invp_tev]);
			$lang[invp_tiv] = str_replace('TAX',$user_vat['symbol'],$lang[invp_tiv]);
			$lang[invp_vrn] = str_replace('TAX',$user_vat['symbol'],$lang[invp_vrn]);
			$lang[invp_ovrn] = str_replace('TAX',$user_vat['symbol'],$lang[invp_ovrn]);
		}
	}
}

$datentime          = date("d-M-Y",time());
$buyer_vat_number   = $buyer['vat_uid_number'] ? $buyer['vat_uid_number'] : $lang[na];
$seller_vat_number  = $seller['vat_uid_number'] ? $seller['vat_uid_number'] : $lang[na];
$quant_offered      = ($item['quant_offered']>0) ? $item['quant_offered'] : 1;
//$vatrate            = $vat_rate;//(double)$seller['vatrate'];
$category           = getSqlField("SELECT name FROM probid_categories WHERE id=".$itemdetails['category'],'name');

//$total = ($amount = $item['amount']) + $quant_offered * ($itemdetails['postage_costs']+$itemdetails['insurance']);
$total = ($amount = $quant_offered * $item['amount']) + $quant_offered * $itemdetails['postage_costs']+$item['insurance'];
if(!$item['delivery_included']){
	$amount += $quant_offered * $itemdetails['postage_costs'];
   $delivery_amount = 0;
} else {
   $delivery_amount = $quant_offered * $itemdetails['postage_costs'];
}
if(!$item['insurance_included']){
   //$amount += $quant_offered * $itemdetails['insurance'];
   $amount += $item['insurance']; // new edition
   $insurance_amount = 0;
} else {
   //$insurance_amount = $quant_offered * $itemdetails['insurance'];
   $insurance_amount = $item['insurance']; // new edition
}

$currency       = $itemdetails['currency'];
$amount_string  = displayAmount($amount,$currency,"YES");
//$item['vat_included'] = 0; // DEBUG !!!
if($vatrate && $item['vat_included']) { // with VAT
   $vat_rate_string    = $vatrate.'%';
   $vat_amount         = ($amount - ($item['insurance'] - $insurance_amount) ) * $vatrate / 100;
   $vat_amount_string  = displayAmount($vat_amount,$currency,"YES");
   if(!$item['delivery_included']) {
		$delivery_amount_string     = displayAmount(0,$currency,"YES");
      $delivery_vat_amount        = 0;
      $delivery_vat_amount_string = displayAmount(0,$currency,"YES");
   } else {
      $delivery_amount_string     = displayAmount($delivery_amount,$currency,"YES");
      $delivery_vat_amount        = $delivery_amount * $vatrate / 100;
      $delivery_vat_amount_string = displayAmount( $delivery_vat_amount,$currency,"YES");;
   }
   $total_amount_string    = displayAmount($amount + $vat_amount,$currency,"YES");
   $total                  = $amount + $vat_amount + $insurance_amount + $delivery_amount + $delivery_vat_amount;
   $total_string           = displayAmount( $total,$currency,"YES");
   $total_inc_vat_string   = $total_string;
   $total_exc_vat_string   = displayAmount($total - $vat_amount - $delivery_vat_amount,$currency,"YES");
} else { // without VAT
   $vat_rate_string            = $lang[na];
   $vat_amount_string          = $lang[na];
   $delivery_vat_amount_string = $lang[na];
   if(!$item['delivery_included']) {
		$delivery_amount_string = displayAmount(0,$currency,"YES");
   } else {
      $delivery_amount_string = displayAmount($delivery_amount,$currency,"YES");
   }
   $total_amount_string = displayAmount($amount,$currency,"YES");
   $total_string        = displayAmount($total,$currency,"YES");
   $total_inc_vat_string= $total_string;
   $total_exc_vat_string= $total_string;
}

// INSURANCE CALCULATING
if($item['insurance_included']){
   $insurance_amount_string = displayAmount( $insurance_amount, $itemdetails['currency'], "YES");
   $insurance_row = <<<INSURANCE
        <tr class="c2">
          <td align="center">1</td>
          <td>Insurance</td>
          <td align="center">{$insurance_amount_string}</td>
          <td align="center">{$lang[na]}</td>
          <td align="center">{$lang[na]}</td>
          <td align="center">{$insurance_amount_string}</td>
        </tr>
INSURANCE;
}
// INSURANCE CALCULATING

?>
<table width="750" align="center" class="border">
   <tr>
      <td><img src="images/probidlogo.gif" border="0"><br>
         <div style="padding-left: 10px;">
            <?=$seller['name'];?>
            <br>
            <?=$seller['address'].', '.$seller['city'];?>
            <br>
            <?=$seller['state'].', '.$seller['zip'];?>
            <br>
            <?=$seller['country'];?>
         </div></td>
   </tr>
   <tr>
      <td style="border-bottom: 2px solid #cccccc;">&nbsp;</td>
   </tr>
   <tr>
      <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
               <td width="50%"><table width="70%" border="0" cellpadding="3" cellspacing="2">
                     <tr>
                        <td class="c1"><?=$lang['invp_bill_to'];?>
                           :</td>
                     </tr>
                     <tr class="c2">
                        <td><?=$buyer['name'];?>
                           <br>
                           <?=$buyer['address'];?>
                           <br>
                           <?=$buyer['city'];?>
                           <br>
                           <?=$buyer['state'];?>
                           <br>
                           <?=$buyer['zip'];?>
                           <br>
                           <?=$buyer['country'];?></td>
                     </tr>
                  </table></td>
               <td width="50%"><table width="100%" border="0" cellpadding="3" cellspacing="2">
                     <tr>
                        <td class="c1"><?=$lang['invp_transaction_id'];?></td>
                        <td class="c4" width="200"><?=$item['txnid'];?></td>
                     </tr>
                     <tr>
                        <td class="c1" nowrap><?=$lang['invp_invoice_tpd'];?></td>
                        <td class="c4"><b>
                           <?=$datentime;?>
                           </b></td>
                     </tr>
                     <tr>
                        <td class="c1" nowrap><?=$lang['invp_vrn'];?></td>
                        <td class="c4"><?=$buyer_vat_number;?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_invoice_number'];?></td>
                        <td class="c4"><?=$item['id'];?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_order_method'];?></td>
                        <td class="c4">&nbsp;</td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_product_type'];?></td>
                        <td class="c4"><?=$category;?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_payment_terms'];?></td>
                        <td class="c4"><?=$lang['invp_on_demand'];?></td>
                     </tr>
                  </table></td>
            </tr>
         </table></td>
   </tr>
   <tr>
      <td align="center"><br>
         <font size="+2" color="#666666"><b>
         <?=$lang['invp_invoice'];?>
         </b></font> <br>
      </td>
   </tr>
   <tr>
      <td><table width="100%" border="0" cellpadding="3" cellspacing="2">
            <tr class="c1" align="center">
               <td><?=$lang['invp_quantity'];?></td>
               <td><?=$lang['invp_description'];?></td>
               <td><?=$lang['invp_price'];?></td>
               <td><?=$lang['invp_vat_rate'];?></td>
               <td><?=$lang['invp_vat'];?></td>
               <td><?=$lang['invp_total'];?></td>
            </tr>
            <tr class="c2">
               <td align="center"><?=$quant_offered;?></td>
               <td><?=$itemdetails['itemname'];?></td>
               <td align="center"><?=$amount_string;?></td>
               <td align="center"><?=$vat_rate_string;?></td>
               <td align="center"><?=$vat_amount_string;?></td>
               <td align="center"><?=$total_amount_string;?></td>
            </tr>
            <?=$insurance_row;?>
         </table></td>
   </tr>
   <tr>
      <td><p>&nbsp;</p></td>
   </tr>
   <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
               <td width="50%"><table width="70%" border="0" cellpadding="3" cellspacing="2">
                     <tr>
                        <td class="c1"><?=$lang['invp_delivery_address'];?>
                           :</td>
                     </tr>
                     <tr class="c2">
                        <td><?=$buyer['name'];?>
                           <br>
                           <?=$buyer['address'];?>
                           <br>
                           <?=$buyer['city'];?>
                           <br>
                           <?=$buyer['state'];?>
                           <br>
                           <?=$buyer['zip'];?>
                           <br>
                           <?=$buyer['country'];?></td>
                     </tr>
                  </table></td>
               <td width="50%"><table width="100%" border="0" cellpadding="3" cellspacing="2">
                     <tr>
                        <td class="c1"><?=$lang['invp_carriage'];?></td>
                        <td class="c4" width="200"><?=$delivery_amount_string;?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_vat'];?></td>
                        <td class="c4"><?=$delivery_vat_amount_string;?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_tev'];?></td>
                        <td class="c4"><?=$total_exc_vat_string;?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_vat'];?></td>
                        <td class="c4"><?=$vat_rate_string;?></td>
                     </tr>
                     <tr>
                        <td class="c1"><?=$lang['invp_tiv'];?></td>
                        <td class="c4"><?=$total_inc_vat_string;?></td>
                     </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="3" cellspacing="2">
                     <tr>
                        <td class="c1"><?=$lang['invp_invoice_total'];?></td>
                        <td class="c4"><b>
                           <?=$total_string;?>
                           </b></td>
                     </tr>
                  </table></td>
            </tr>
         </table></td>
   </tr>
	<!--
   <tr>
      <td><p>&nbsp;</p></td>
   </tr>
   <tr>
      <td><table width="100%" border="0" cellpadding="3" cellspacing="2">
            <tr>
               <td class="c4"><b>
                  <?=$lang['invp_comments'];?>
                  :</b></td>
               <td width="300">&nbsp;</td>
            </tr>
            <tr>
               <td colspan="2" class="border">&nbsp;<br>
                  <br>
                  <br>
                  <br></td>
            </tr>
         </table></td>
   </tr>
	-->
   <tr>
      <td><p>&nbsp;</p></td>
   </tr>
   <tr>
      <td><table width="100%">
            <tr>
               <td class="c1" width="30%" align="center"><?=$lang['invp_ovrn'];?></td>
               <td class="c4"><?=$seller_vat_number;?></td>
            </tr>
         </table></td>
   </tr>
</table>
</body>
</html>
<? } ?>
