<?php
class xpath {
	public $html;
	public $patten;
	public $childnodes = 0;
	public $attribute = 0;
	public $forbidden = 0;
	public $search = 0;
	public $br2nl = 1;
	public $return = "string";
	public $charset = "utf8";
	public $search_paym = array("sularahas","deposiidikandega","Osta.ee e-konto ülekandega","pangaülekandega");
	public $search_inloc = array("uus","kasutatud","использованный","новый");
	public $order = array("\r\n", "\n", "\r","\t","Osta Kohe:","(?)","SmartPOST pakiautomaat");
	public $currency = array("EUR","EEK",".00"," ");
	private $retrund;

	function __construct($html, $patten){
		$this->html = $html;
		$this->patten = $patten;
	}
	public function execute(){
		$xpath = new DOMXPath($this->html);
		$basenodes = $xpath->query($this->patten);
		foreach ($basenodes as $basenode){
			if($this->childnodes){
				foreach($basenode->childNodes as $childnode){
					$this->buffer($childnode);
				}
			}else{
				$this->buffer($basenode);
			}
		}
	}
	private function buffer($value){
		$preparedvalue = $this->prepare($value);
		if($preparedvalue){
			if($this->returnstring()){
				$this->returnd .= $preparedvalue;
			}else{
				$this->returnd[] = $preparedvalue;
			}
		}
	}
	private function returnstring(){
		if($this->return == "string"){
			return true;
		}else{
			return false;
		}
	}
	private function returnutf8(){
		if($this->charset == "utf8"){
			return true;
		}else{
			return false;
		}
	}

	private function prepare($before){
		if($before->tagName == "br" AND $this->br2nl){
			$before = "\n";
		}else{
			if($this->attribute){
				if($this->search){
					if(in_array($before->textContent, $this->search)){
						$before = $before->getAttribute($this->attribute);
					}else{
						$before = 0;
					}
				}else{
					$before = $before->getAttribute($this->attribute);
				}
			}else{
				if($this->search){
					if(in_array($before->textContent, $this->search)){
						$before = trim($before->textContent);
					}
				}else{
					$before = trim($before->textContent);
				}
			}
		}
		if(!$this->returnutf8()){
			$before = utf8_decode($before);
		}
		if($this->forbidden){
			if(in_array($before, $this->forbidden)){
				return false;
			}else{
				return $before;
			}
		}else{
			return $before;
		}
	}
	public function get(){
		return $this->returnd;
	}
	public function replace_loc($before) {
		return str_replace($this->search_inloc,"",$before);
	}
	public function parse_start($before) {
		//echo $before[0];
		$before_new = str_replace($this->order,"",$before[0]);
		$str = substr($before_new,37,19);
		if (substr($str,0,1)==" ") {
			return substr($str,1);
		} else {
			return $str;
		}
	}
	public function clear_buynow($before) {
		return str_replace($this->order,"",$before);
	}
	public function parse_transport($before) {
		return str_replace($this->order,"",$before[0]);
	}
	public function parse_payment($before) {
		$before[1] = str_replace("ülekandega","ülekandega, ",$before[1]);
		$tmp = explode(",",$before[1]);
		for ($i=0;$i<count($tmp);$i++) {
			if (substr($tmp[$i],0,1) == " ") {
				$new[] = substr($tmp[$i],1);
			} else {
				$new[] = $tmp[$i];
			}
		}
		for ($i=0;$i<count($new);$i++) {
			if (in_array($new[$i],$this->search_paym)) {
				$payments[] = $new[$i];
			}
		}
		$new_string = implode("<br>",$payments);
		//$new_string = str_replace(" ","",$str);
		//$new_string = str_replace(",","<br>",$new_string);
		$new_string = str_replace("pangaülekandega","Bank transfer - Pangamakse - Банковский перевод",$new_string);
		$new_string = str_replace("sularahas","Cash - Sularaha - Наличные",$new_string);
		$new_string = str_replace("Osta.ee e-konto ülekandega","E-account transfer - E-konto ülekanne - Оплата с е-счёта",$new_string);
		$new_string = str_replace("deposiidikandega","Other - Muu - Другое",$new_string);
		return $new_string;
	}
	public function get_currency($current) {
		return substr($current,($len-3),3);
	}
	public function format_price($price) {
		return str_replace($this->currency,"",$price);
		
	}


}
?>