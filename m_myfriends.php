<?php

function checkEmail($email) {

   //check for all the non-printable codes in the standard ASCII set,
   //including null bytes and newlines, and exit immediately if any are found.
   if (preg_match("/[\\000-\\037]/",$email)) {
      return false;
   }
   $pattern = "/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD";
   if(!preg_match($pattern, $email)){
      return false;
   }
   // Validate the domain exists with a DNS check
   // if the checks cannot be made (soft fail over to true)
   list($user,$domain) = explode('@',$email);
   if( function_exists('checkdnsrr') ) {
      if( !checkdnsrr($domain,"MX") ) { // Linux: PHP 4.3.0 and higher & Windows: PHP 5.3.0 and higher
         return false;
      }
   }
   else if( function_exists("getmxrr") ) {
      if ( !getmxrr($domain, $mxhosts) ) {
         return false;
      }
   }
   return true;
} 

function dbEmail($email) {

    $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  id FROM probid_users
            WHERE email='".$email."'");
    $row = mysqli_fetch_row($result);
    return intval($row[0]);
}

function isInvited($email) {
    $count = getSqlField("SELECT count(id) AS cnt FROM probid_users_friends_invites WHERE email='".$email."'","cnt");
    return $count;
    

}

$errEmails = array();

if ($_POST["act"]=="send") {

  for ($i=0;$i<5;$i++) {
  
  
      // Check fields
      if (($_POST["friend_name"][$i]!='') && ($_POST["friend_email"][$i]!='')) {
          
          // Check email
  
     $cnt = isInvited($_POST["friend_email"][$i]);
  
  
  
        if ((dbEmail($_POST["friend_email"][$i])>0) || (!checkEmail($_POST["friend_email"][$i])) || ($cnt>0) ) {
         
        
          $errEmails[] = $i;
          
        }  else {
          $trueEmails[] = $i;
        }
        
    }
  
  }
  
      
      $mymail = getSqlField("SELECT email FROM probid_users WHERE id=".$_SESSION['memberid']."","email");
      $SenderFullName = getSqlField("SELECT name FROM probid_users WHERE id=".$_SESSION['memberid']."","name");

      if (((count($errEmails))==0) && (count($trueEmails)>0)) {
      
      

      
        foreach ($trueEmails as $n) {
        
            // Send invite

            $uniq = substr(strtoupper(md5($_SESSION["memberid"]*strtotime("now")*rand(99,9999))),1,6);
  
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers.= "Content-type: text/html; charset=utf-8" . "\r\n";
            // Additional headers
            $headers.= 'From: <'.$mymail.'>' . "\r\n";
          //  $headers.= 'Cc: '.$mymail.'' . "\r\n";
           // $headers.= 'Bcc: '.$mymail.'' . "\r\"";  
            
            $subject = $SenderFullName.$lang[invitation_email_subject];
            
            $mail .= $lang[dear].$_POST["friend_name"][$n].",\n\n";
            $mail .= $SenderFullName." ".$lang[invitation_body];
        

            // Insert invite data DB

            $mail_cnt = getSqlField("SELECT count(email) as cnt FROM probid_users_friends_invites WHERE email='".$_POST["friend_email"][$n]."'","cnt");
            if ($mail_cnt==0) {
                $query = "INSERT INTO probid_users_friends_invites(name,email,refid,status_reg,status_auth,code,send_date) VALUES('".$_POST["friend_name"][$n]."','".$_POST["friend_email"][$n]."',".$_SESSION["memberid"].",0,0,'".$uniq."','".date("d.m.Y H:i:s")."')";
                @mail($_POST["friend_email"][$n], $subject, $mail, $headers);
                $res = @mysqli_query($GLOBALS["___mysqli_ston"], $query);  
               
            }
          
            $mail="";
            
            // Check inserted email

        }
        
        $sendedText = $lang[invite_sended];
          
      } else {
      
        $sendedText = $lang[invite_not_sended];
      
      }
        

}

echo "<font size=2>".$lang[digital_agent_description]."</font><BR><BR>"; //changed size from 3 to 2

?>


<form name="inviteForm" method="post">
<table class="border" style="width:100%" cellpadding="4" cellspacing="4" border="0">
<tr>
    <td class="c1" colspan="3"><?=$lang[invite_friends]?></td>
</tr>
<tr>
    <td colspan="3"><?=$sendedText?></td>
</tr>
<?php



for ($i=0;$i<5;$i++) {


    echo "<tr><td><b>".($i+1).".</b> ".$lang[name]." <input type='text' value='".$_POST["friend_name"][$i]."' name='friend_name[]'></td><td><span style=".(in_array($i,$errEmails)?'color:red':'').">".$lang[email]."</span> <input type='text' size='20' value='".$_POST["friend_email"][$i]."' name='friend_email[]'> </td></tr>";
    
}

?>
<tr><td></td><td><input type="hidden" value="send" name="act"><input type="submit" value=<?=$lang[send_invitation]?>></td></tr>
</table>
<table class="border" style="width:100%" cellpadding="4" cellspacing="4" border="0">
<tr>
    <td class="c1" colspan="4"><?=$lang[sended_invites]?></td>
</tr>
<tr>
    <td align='center'><?=$lang[registered_username]?></td><td align='center'><?=$lang[is_reg]?></td><td align='center'><?=$lang[is_auth]?></td><td align='center'><?=$lang[date_of_invitation]?></td>
</tr>
<?php

  $result = @mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * FROM probid_users_friends_invites WHERE refid='".$_SESSION["memberid"]."'");
    while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $myInvites[] = $row; 
    }
    
    // Translate for status
    
    $status[0] = $lang[no];
    $status[1] = $lang[yes];
    
  if (is_array($myInvites) && (sizeof($myInvites)>0)) {
  
  
      foreach ($myInvites as $row) {
        echo "<tr><td>".$row["name"]." (".$row["email"].")</td><td align='center'>".$status[$row["status_reg"]]."</td><td align='center'>".$status[$row["status_auth"]]."</td><td style='width:150'>".$row["send_date"]."</td></tr>";
      }
  
  }
    
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
  	echo "<tr><td align='center'>";
	echo $row['name']."</td><td align='center'>";
	echo ($row['status_reg']==1) ? "<font color=green><strong>+0.20 EUR</strong></font></td><td align='center'>" : $lang[not_reg]."</td><td align='center'>";
	echo ($row['status_auth']==1) ? "<font color=green><strong>+0.50 EUR</strong></font></td><td align='center'>" : $lang[not_auth]."</td><td align='center'>";
	echo $row['send_date']."</td></tr>";
}
echo "</table>";

?>




