<?
## v5.25 -> jun. 28, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($_SESSION['membersarea']=="Active"&&$_SESSION['auctionid_refresh']==0) {

$force_standart_auction=1; // only standart auctions available

?>

 <table width="100%" border="0" cellpadding="2" cellspacing="2"> 
  <tr class="contentfont" align="center"> 
     <td class="c4" width="20%"><?=$lang[sellstep1];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep3];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep4];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep5];?></td> 
     <td class="c1" width="20%"><?=$lang[sellstep6];?></td> 
   </tr> 
</table>
<br>

<?	

  

	if ($_REQUEST['starttime']=="NOW") {
		$startDate = date( "Y-m-d H:i:s", time() );
		$closed = 0;
	} else {
		$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");
		$startDate = date("Y-m-d H:i:s", 
  		mktime($_REQUEST['shour']-$diff,$_REQUEST['sminute'],0,$_REQUEST['dmonth'],$_REQUEST['ddate'],$_REQUEST['dyear'])); 
		$closed = 1;
	}

	if ($_REQUEST['endtime']=="duration") {
		$closingdate = closingdate($startDate,$_REQUEST['duration']);
	} else {
		$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");
		$closingdate = date("Y-m-d H:i:s", 
  		mktime($_REQUEST['eshour']-$diff,$_REQUEST['esminute'],0,$_REQUEST['edmonth'],$_REQUEST['eddate'],$_REQUEST['edyear'])); 
	}

	if ($_REQUEST['hpfeat']!="Y") $_REQUEST['hpfeat']="N";
	if ($_REQUEST['catfeat']!="Y") $_REQUEST['catfeat']="N";
	if ($_REQUEST['bolditem']!="Y") $_REQUEST['bolditem']="N";
	if ($_REQUEST['hlitem']!="Y") $_REQUEST['hlitem']="N";

	## set a maximum width for auction title words (the max char width can be set from functions.php)
	$_REQUEST['name'] = titleResize($_REQUEST['name']);
	
	$descwords = explode(" ",$_REQUEST['description']);
	for ($i=0;$i<count($descwords);$i++) {
		if (strlen($descwords[$i])>30) $descwords[$i] = substr($descwords[$i],0,30);
	}
	$getwordfilter=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_wordfilter");
	while ($frow=mysqli_fetch_array($getwordfilter)) {
		$_REQUEST['name']=@eregi_replace($frow['word']," ",$_REQUEST['name']);
		$_REQUEST['description']=@eregi_replace($frow['word']," ",$_REQUEST['description']);
		if ($_REQUEST['hpfeat_desc']!="") $_REQUEST['hpfeat_desc']=@eregi_replace($frow['word']," ",$_REQUEST['hpfeat_desc']);
	}
	$keywords=$_REQUEST['name']." ".$_REQUEST['description'];

	$topay="";
  	$isListingFee = getSqlNumber("SELECT * FROM probid_fees_tiers WHERE 
  	fee_from<=".$_REQUEST['startprice']." AND fee_to>".$_REQUEST['startprice']." AND fee_type='setup'");
	if ($fee['is_setup_fee']=="Y"&&$isListingFee>0) $topay.="Auction Setup Fee; ";
	if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$_REQUEST['cnt1']>0) { 
		$topay.="Image Gallery Fee; ";
		$isFee['pic_count'] = $_REQUEST['cnt1'];
	}
	if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$_REQUEST['hlitem']=="Y") {
		$topay.="Highlighted Item Fee; ";
		$isFee['hl']="Y";
	}
	if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$_REQUEST['bolditem']=="Y") {
		$topay.="Bold Item Fee; ";
		$isFee['bold']="Y";
	}
	if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$_REQUEST['hpfeat']=="Y") {
		$topay.="Home Page Featured Item Fee; ";
		$isFee['hpfeat']="Y";
	}
	if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$_REQUEST['catfeat']=="Y") {
		$topay.="Category Page Featured Item Fee; ";
		$isFee['catfeat']="Y";
	}
	if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$_REQUEST['respr']=="Y") {
		$topay.="Reserve Price Fee; ";
		$isFee['rp']="Y";
	}
	if ($fee['second_cat_fee']>0&&$_REQUEST['addlcategory']>0) {
		$topay.="Second Category Fee; ";
		$isFee['secondcat']="Y";
	}
	if ($fee['bin_fee']>0&&$_REQUEST['buynow']=="Y") {
		$topay.="Buy It Now Fee; ";
		$isFee['bin']="Y";
	}
	if ($fee['custom_st_fee']>0&& ($_REQUEST['starttime']=="custom" || $_REQUEST['endtime']=="customtime")) {
		$topay.="Custom Start Time Fee; ";
		$isFee['customst']="Y";
	}
	if ($fee['videofile_fee']>0&& !empty($_REQUEST['videofile_path'])) {
		$topay.="Video Upload Fee; ";
		$isFee['videofee']="Y";
	}

	$payAmount = setupFee($_REQUEST['startprice'],$_REQUEST['currency'],$auctionid,$isFee,FALSE,TRUE,trim($_REQUEST['voucher_code']));

	if ($topay!=""&&!freeFees($_SESSION['memberid'])&&$_REQUEST['listin']!="store"&&$payAmount>0) {
		$payment_status="unconfirmed";
		$active=0;
	} else {
		$payment_status="confirmed";
		$active=1;
	}

    $_REQUEST['acceptdirectpayment'] = $_REQUEST['tmp_dirpay'] ? 1 : 0;
	if (!$setts['paypaldirectpayment']) $_REQUEST['acceptdirectpayment'] = 0;
	if ($force_standart_auction==1) {
	$auction_type='standard';
	} else {
	$auction_type=$_REQUEST['auctiontype'];}
	$insertAuction = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auctions 
	(itemname,description,picpath,quantity,auctiontype,bidstart,
	rp,rpvalue,bn,bnvalue,bi,bivalue,duration,country,zip,sc,scint,
	pm,category,active,payment_status,startdate,enddate,closed,
	keywords,maxbid,ownerid,hpfeat,catfeat,bolditem,hlitem,private,
	currency,postage_costs,insurance,type_service,isswap,
	acceptdirectpayment,addlcategory,shipping_details,hpfeat_desc,listin,accept_payment_systems,apply_vat,
	auto_relist,auto_relist_bids, endtime_type, videofile_path,listing_type, offer_range_min, offer_range_max, auto_relist_nb) VALUES
	('".remSpecialChars($_REQUEST['name'])."','".remSpecialChars($_REQUEST['description'])."','".$_REQUEST['mainpic']."',
	'".$_REQUEST['quantity']."','".$auction_type."','".$_REQUEST['startprice']."',
	'".$_REQUEST['respr']."','".$_REQUEST['resprice']."','".$_REQUEST['buynow']."',
	'".$_REQUEST['bnprice']."','".$_REQUEST['bidinc']."','".$_REQUEST['bidincvalue']."',
	'".$_REQUEST['duration']."','".remSpecialChars($_REQUEST['country'])."','".$_REQUEST['zip']."',
	'".$_REQUEST['shipcond']."','".$_REQUEST['shipint']."',	'".$_REQUEST['pm']."',
	'".$_REQUEST['category']."','".$active."','".$payment_status."','".$startDate."','".$closingdate."','".$closed."',
	'".remSpecialChars($keywords)."',0,'".$_SESSION['memberid']."','".$_REQUEST['hpfeat']."','".$_REQUEST['catfeat']."',
	'".$_REQUEST['bolditem']."','".$_REQUEST['hlitem']."','".$_REQUEST['privateauct']."',
	'".$_REQUEST['currency']."','".$_REQUEST['postage_costs']."','".$_REQUEST['insurance']."',
	'".$_REQUEST['type_service']."','".$_REQUEST['isswap']."','".$_REQUEST['acceptdirectpayment']."',
	'".$_REQUEST['addlcategory']."','".remSpecialChars($_REQUEST['shipping_details'])."',
	'".remSpecialChars($_REQUEST['hpfeat_desc'])."','".$_REQUEST['listin']."','".$_REQUEST['tmp_dirpay']."','".(($_REQUEST['apply_vat']==1)?1:0)."',
	'".$_REQUEST['auto_relist']."','".$_REQUEST['auto_relist_bids']."', '".$_REQUEST['endtime']."', 
	'".$_REQUEST['videofile_path']."','".$_REQUEST['listing_type']."', '".$_REQUEST['offer_range_min']."', 
	'".$_REQUEST['offer_range_max']."', '".$_REQUEST['auto_relist_nb']."')") or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	$auctionid=((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
	
	// TRANSACTION
  if (($_REQUEST["hpfeat"])=="Y")
  {
    $arrTrans[0]["comment"] = $lang[featdhomepage];
    $arrTrans[0]["price"] = $price["hpfeat"];
     
  }

  if (($_REQUEST["catfeat"])=="Y")
  {
    $arrTrans[1]["comment"] = $lang[featcatpage];
    $arrTrans[1]["price"] = $price["catfeat"];

     
  }
  
  if (($_REQUEST["bolditem"])=="Y")
  {
    $arrTrans[2]["comment"] = $lang[bolditem];
    $arrTrans[2]["price"] = $price["bolditem"];

     
  }
  
  if (($_REQUEST["hlitem"])=="Y")
  {
    $arrTrans[3]["comment"] = $lang[highlighteditem];
    $arrTrans[3]["price"] = $price["hlitem"];

     
  }  
  
  if (is_array($arrTrans))
  {
    foreach ($arrTrans as $r)
    {
      #NOW BALANCE
      $cur = getSqlRow("SELECT balance FROM probid_users WHERE id='".$_SESSION['memberid']."'");  
      #UPDATE USER BALANCE
      $updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET balance='".($cur["balance"]+$r["price"])."' WHERE id='".$_SESSION['memberid']."'");  
  
      $sqlTran = "INSERT INTO probid_users_transactions(userid,name,description,amount,balance,op,tdate) 
                  VALUES (".$_SESSION["memberid"].",'OKSJONI TEENUSED','".$r["comment"]." #".$auctionid."','".$r["price"]."','".(abs($cur["balance"] + $r["price"]))."','out','".date("Y-m-d H:i:s")."')";
      mysqli_query($GLOBALS["___mysqli_ston"], "SET CHARACTER SET utf8");
      mysqli_query($GLOBALS["___mysqli_ston"], "SET NAMES utf8");
      mysqli_query($GLOBALS["___mysqli_ston"], $sqlTran);
    }
  }



	$_SESSION['auctionid_refresh'] = $auctionid;
	$_SESSION['images_id'] = 0;

	### now we check for favourite stores and notify the users who are watching the seller's store
	if ($_REQUEST['listin']!="auction") {
		$getFavStores = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_favourite_stores WHERE storeid='".$_SESSION['memberid']."'");
		while($favStore = mysqli_fetch_array($getFavStores)) {
			$sellerId = $_SESSION['memberid'];
			$buyerId = $favStore['userid'];
			$auctionId = $auctionid;
			include("mails/notifyfavstorenewitem.php");
		}
	}
	
	### insert the custom fields for the auction (if available)
  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_fields") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  	$isFields = mysqli_num_rows($getFields);
  	if ($isFields) {
		while ($fields=mysqli_fetch_array($getFields)) {
			$box_value = "";
			if ($fields['boxtype']=="checkbox") {
				for ($i=0; $i<count($_POST['box'.$fields['boxid']]); $i++) 
					$box_value .= $_POST['box'.$fields['boxid']][$i]."; ";
			} else { 
				$box_value = $_POST['box'.$fields['boxid']];
			}
			$addFieldData = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fields_data
			(auctionid, ownerid, boxid, boxvalue) VALUES
			('".$auctionid."','".$_SESSION['memberid']."','".$fields['boxid']."','".remSpecialChars($box_value)."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}		

	for ($i=0;$i<count($_REQUEST['the_pic']);$i++) {
		$insertAdditionalImage[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_images 
		(name,auctionid) VALUES ('".$_REQUEST['the_pic'][$i]."','".$auctionid."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}

	### check in auction watch table for keywords matching ours and send email to the user interested.
	$getKeywords = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_keywords_watch");
	while ($keywordsW=mysqli_fetch_array($getKeywords)) {
		if (@eregi($keywordsW['keyword'],$keywords)) {
			$userId = $keywordsW['bidderid'];
			$auctionId = $auctionid;
			$keyword = $keywordsW['keyword'];
			include ("mails/keywordmatch.php");
		}
	}

	### send confirmation email to the seller
	echo "<table class='errormessage' width='100%'><tr><td align='center'>";
	$userId = $_SESSION['memberid'];
	$auctionId = $auctionid;
	###include ("mails/confirmtoseller.php");
	//echo "TOPAY: $topay";

	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userid."'","payment_mode");
	if ($setts['account_mode_personal']==1) {
		$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];

	if ($topay!=""&&!freeFees($_SESSION['memberid'])&&$_REQUEST['listin']!="store"&&$payAmount>0) {
		### the function for the auction setup fees
		setupFee($_REQUEST['startprice'],$_REQUEST['currency'],$auctionid,$isFee,FALSE,FALSE,trim($_REQUEST['voucher_code']));
		if ($account_mode_local==2) {
			echo "<p align=center class=contentfont>[ <a href=\"membersarea.php?page=selling\">".$lang[clicktoreturntombarea]."</a> ] &nbsp;";
			echo "[ <a href=\"sellitem.php?option=sellsimilar&similarid=".$auctionid."\">".$lang[clicktosellsimilar]."</a> ]</p>";
		}
	} else {
// KEV START CHANGES - changed to make sure that the auction is active NOW and not on timer
		// if there is no fee to pay, add the counter (auction is also activated)
		if ($_REQUEST['starttime']=="NOW") {
			addcatcount ($_REQUEST['category'],$auctionid);
			addcatcount ($_REQUEST['addlcategory'],$auctionid);
		}
		echo "<br><p class=contentfont align=center>$lang[aucsubmitted1] #".$auctionid." $lang[aucsubmitted2]</p>";
		echo "<p align=center class=contentfont>[ <a href=\"membersarea.php?page=selling\">".$lang[clicktoreturntombarea]."</a> ] &nbsp;";
		echo "[ <a href=\"sellitem.php?option=sellsimilar&similarid=".$auctionid."\">".$lang[clicktosellsimilar]."</a> ]</p>";
	}
	echo "</td></tr></table>";
	$getVo = checkSetupVoucher(trim($_REQUEST['voucher_code']));
	if ($getVo['nbuses']>0) $updateVoucher=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_vouchers SET usesleft=usesleft-1 WHERE id='".$getVo['id']."'");

} else if ($_SESSION['auctionid_refresh']>0) { 
	echo "<p align=center class=errorfont>$lang[err_refreshimpossible]</p>";
} else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?>
