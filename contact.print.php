<?
## v5.25 -> jun. 26, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/title.php");
include ("config/lang/$setts[default_lang]/site.lang");

$addQuery = ($_REQUEST['option']=='buyer') ? " AND w.buyerid='".$_SESSION['memberid']."' AND w.b_deleted!=1" : " AND w.sellerid='".$_SESSION['memberid']."' AND w.s_deleted!=1";
$items=getSqlRow("SELECT count(w.id) AS nb_auctions, sum(w.quant_offered) AS nb_quant, sum(w.amount * w.quant_offered) AS nb_amount 
FROM probid_winners w, probid_auctions a WHERE w.auctionid=a.id".$addQuery); 

##$start = (empty($_GET['start'])) ? 0 : $_GET['start'];
##$limit = 10;

$orderField = (empty($_REQUEST['orderField'])) ? "a.id" : $_REQUEST['orderField'];
if (empty($_REQUEST['orderType'])) {
	$orderType = "DESC";
	$newOrder="ASC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}

$additionalVars = "&purchase_date=".$_REQUEST['purchase_date']."&option=".$_REQUEST['option'];

?>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<table border="0" cellspacing="1" cellpadding="4" class="errormessage" align="center">
   <tr>
      <td width="100" class="contentfont" align="center"><b><a href="javascript:;" onClick="parent.print();"><?=$lang[printthepage];?></a>
		</b></td>
	</tr>
</table>
<table border="0" cellspacing="1" cellpadding="4">
   <tr>
      <td width="100" class="c1"><b>
         <? echo ($_REQUEST['option']=='buyer') ? $lang[buyingtotals] : $lang[sellingtotals]; ?>
         :</b></td>
      <td class="border smallfont">Auctions
         : <b>
         <?=$items['nb_auctions']; ?>
         </b> </td>
      <td class="border smallfont">Quantity
         : <b>
         <?=$items['nb_quant']; ?>
         </b> </td>
      <td class="border smallfont">Amount
         : <b>
         <?=displayAmount($items['nb_amount'], $setts['currency'], 'YES'); ?>
         </b> </td>
   </tr>
</table>
<br />
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="border">
   <tr align="center" class="c1">
      <td colspan="6"><table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
               <td width="100%"><b>
                  <? echo ($_REQUEST['option']=='buyer') ? $lang[wonitems] : $lang[solditems]; ?>
                  </b> ( <? echo $items['nb_auctions']." ".$lang[items]; ?> )</td>
               <form action="contact.print.php?start=<?=$start;?>&orderField=<?=$orderField;?>&orderType=<?=$orderType;?>" method="get">
					<input type="hidden" name="option" value="<?=$_REQUEST['option'];?>" />
					<td align="right" nowrap="nowrap"><?=$lang[viewitemspurchased];?>
						<select name="purchase_date" onchange="form.submit(this);">
						   <option value="0" selected="selected"><?=$lang[all];?></option>
						   <option value="86400" <? echo ($_GET['purchase_date']==86400) ? "selected" : ""; ?>><?=$lang[lastday];?></option>
						   <option value="604800" <? echo ($_GET['purchase_date']==604800) ? "selected" : ""; ?>><?=$lang[lastweek];?></option>
						   <option value="2592000" <? echo ($_GET['purchase_date']==2592000) ? "selected" : ""; ?>><?=$lang[lastmonth];?></option>
						   <option value="31536000" <? echo ($_GET['purchase_date']==31536000) ? "selected" : ""; ?>><?=$lang[lastyear];?></option>
                  </select>&nbsp; &nbsp; &nbsp;</td>
					</form>
            </tr>
         </table></td>
   </tr>
   <tr>
      <td class="boldgrey"><a href="contact.print.php?start=<?=$start;?>&orderField=w.id&orderType=<?=$newOrder.$additionalVars;?>"><?=$lang[item_id]?></a> / 
			<a href="contact.print.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>"><?=$lang[item_name];?></a></td>
      <td align="center" class="boldgrey"><a href="contact.print.php?start=<?=$start;?>&orderField=w.amount&orderType=<?=$newOrder.$additionalVars;?>"><?=$lang[bid]?></a></td>
      <td align="center" class="boldgrey"><?=$lang[quant]?></td>
      <td class="boldgrey"><?=$lang[contact_info]?></td>
      <td align="center" class="boldgrey"><a href="contact.print.php?start=<?=$start;?>&orderField=w.purchase_date&orderType=<?=$newOrder.$additionalVars;?>"><?=$lang[purchase_date];?></a> /
			<a href="contact.print.php?start=<?=$start;?>&orderField=w.flag_paid&orderType=<?=$newOrder.$additionalVars;?>"><?=$lang[status];?></a></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td align="center"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? 	
	$addQuery .= ($_REQUEST['purchase_date']>0) ? " AND w.purchase_date>=(".time()."-".$_REQUEST['purchase_date'].") " : "";
	$nbSellers = getSqlNumber("SELECT w.* FROM probid_winners w WHERE w.id!=0".$addQuery);
	
	$getUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.* FROM probid_winners w, probid_auctions a WHERE w.auctionid=a.id ".$addQuery."
	ORDER BY ".$orderField." ".$orderType."") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	while ($userDetails = mysqli_fetch_array($getUsers)) { 
		$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$userDetails['auctionid']."'");
		$user = getSqlRow("SELECT * FROM probid_users WHERE id='".(($_REQUEST['option']=='buyer') ? $userDetails['sellerid'] : $userDetails['buyerid'])."'");

		$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auction['id']."'","category");

		$category_id = getMainCat($itemCategory);

		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

		if ($userDetails['payment_status']=="confirmed"||($fee['is_endauction_fee']=="N")) { ?>
	<tr class="<? echo (($count++)%2==0)?"c2":"c3";?>" valign="top">
      <td class="smallfont"><strong> <? echo ($auction['itemname']=="")?$lang[auctiondeleted]:("#".$auction['id']." / ".$auction['itemname']);?> </strong> </td>
      <td align="center" class="smallfont"><? echo displayAmount($userDetails['amount'],$auction['currency']); ?></td>
      <td class="smallfont"><?=$lang[requested]?>
         : <? echo (($auction['auctiontype']=="dutch")?"".$userDetails['quant_req']."":"1");?><br>
         <?=$lang[offered]?>
         : <? echo (($auction['auctiontype']=="dutch")?"".$userDetails['quant_offered']."":"1");?> </td>
      <td class="smallfont"><table width="100%" border="0" cellpadding="2" cellspacing="1" class="c4">
            <tr valign="top" class="c2">
               <td class="smallfont"><b>
                  <?=$lang[username]?>
                  </b></td>
               <td class="smallfont"><? echo ($user['username']=="")?$lang[userdeleted]:$user['username'];?></td>
            </tr>
            <tr valign="top" class="c3">
               <td class="smallfont"><b>
                  <?=$lang[fullname]?>
                  </b></td>
               <td class="smallfont"><? echo ($user['name']=="")?$lang[na]:$user['name'];?></td>
            </tr>
         </table></td>
      <td align="center" class="smallfont">
			<? echo ($userDetails['purchase_date']>0) ? date($setts['date_format'],$userDetails['purchase_date']+$diff) : $lang[na]; ?><br />
			<? echo showPaidFlag($userDetails['flag_paid']); ?><br>
			<? echo showStatusFlag($userDetails['flag_status']); ?>
        	</td>
   </tr>
   <? } else { ?>
   <tr valign="top">
      <td colspan="6" class="smallfont"><?
			echo $auction['itemname'].'<br>'.$lang[err_endofauc_fee];
			?></td>
   </tr>
   <? }
	} ?>
</table>
<?	} ?>