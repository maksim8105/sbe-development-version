<?
## v5.24 -> apr. 04, 2006

session_start();
include ("config/config.php");
include ("config/currency_converter.inc.php");
include ("themes/".$setts['default_theme']."/title.php");

if(!$_SESSION['sess_lang']) { 
	include ("config/lang/".$setts['default_lang']."/site.lang"); 
	$_SESSION['sess_lang']="".$setts['default_lang'].""; 
} else { 
	include ("config/lang/".$_SESSION['sess_lang']."/site.lang"); 
}


$query = "SELECT id,country,symbol FROM probid_exchange ORDER BY country";
$res_ = @mysqli_query($GLOBALS["___mysqli_ston"], $query);
if(!$res_) {
  	print ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
  	exit;
} else if(@mysqli_num_rows($res_) > 0) {
  	while($row = @mysqli_fetch_array($res_)) {
		$CURRENCIES[$row[id]] = "$row[symbol] $row[country]";
		$CURRENCIES_SYMBOLS[$row[id]] = "$row[symbol]";
	}
}

?>
<HTML>
<HEAD>
<TITLE>
<?=$setts['sitename']?>
-
<?=$lang[convert_title]?>
</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
</HEAD>
<BODY BGCOLOR="#ffffff" TEXT="#000000" LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" MARGINHEIGHT="0">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0">
   <TR>
      <TD WIDTH="25%"><img src="images/probidlogo.gif" border="0"> </TD>
      <TD><center>
            <?=$lang[convert_construction]?>
         </center></TD>
   </TR>
</TABLE>
</BODY>
</HTML>
