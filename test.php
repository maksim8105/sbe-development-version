<?

function getSqlNumber($sqlQuery) {
	$query=@mysqli_query($GLOBALS["___mysqli_ston"], $sqlQuery);
	$result=@mysqli_num_rows($query);
	@((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

function getSqlField($sqlQuery,$field) {
	$isQuery = getSqlNumber($sqlQuery);
	$query = @mysqli_query($GLOBALS["___mysqli_ston"], $sqlQuery) or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))."<br>INVALID QUERY: ".$sqlQuery);
	if ($isQuery>0) {
		$result=mysqli_fetch_assoc($query)[$field];
	} else $result="n/a";
	@((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

//alguseks loome ühendust andmebaasiga

include_once ("config/db_login.php");										
																					
$dbh = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname)) or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));															
																			

//loome andmebaasi struktuuri
//4 tabelit
//lähtamdmed
//asukohad koodidega
//margid
//autode nimekiri
//loom´ulikult saaks panna rohkem tabeleid (nt. tootjad, margid, kuid, kuna aeg on piiratud, ma ei hakka seda tegema, samal ajal see ei mõjuta andmebaasi suurusele)
mysqli_query($GLOBALS["___mysqli_ston"], "CREATE TABLE  `".$dbname."`.`data` (`country` VARCHAR(50) NOT NULL ,`city` VARCHAR(50) NOT NULL ,`cartype` VARCHAR(50) NOT NULL ,`price` FLOAT NOT NULL)");
mysqli_query($GLOBALS["___mysqli_ston"], "CREATE TABLE  `".$dbname."`.`places` (`place_id` INT NOT NULL ,`city` VARCHAR(50) NOT NULL ,`country` VARCHAR(50) NOT NULL, INDEX (`place_id`))");
mysqli_query($GLOBALS["___mysqli_ston"], "CREATE TABLE  `".$dbname."`.`cartypes` (`cartype_id` INT NOT NULL ,`cartype` VARCHAR(50) NOT NULL, INDEX (`cartype_id`))");
mysqli_query($GLOBALS["___mysqli_ston"], "CREATE TABLE  `".$dbname."`.`orders` (`order_id` INT NOT NULL ,`place_id` INT NOT NULL ,`cartype_id` INT NOT NULL ,`price` FLOAT NOT NULL,INDEX (`order_id`))");
//impordime lähtandmed tabellisse data
//esimene leht (peab olema samas direktooriumis, kus skript)
$f = fopen("a.csv", "rt") or die("Viga!");
for ($i=1; $data=fgetcsv($f,1000,";"); $i++) {
  $num = count($data);
  mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO data (country,city,cartype,price) VALUES ('".$data[0]."','".$data[1]."','".$data[2]."','".$data[3]."')");
  
}

fclose($f);
//teine leht (peab olema samas direktooriumis, kus skript)
$f = fopen("b.csv", "rt") or die("Viga!");
for ($i=1; $data=fgetcsv($f,1000,";"); $i++) {
  $num = count($data);
  mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO data (country,city,cartype,price) VALUES ('".$data[0]."','".$data[1]."','".$data[2]."','".$data[3]."')");

}
fclose($f);
//loome unikaalset identifikaatorid kohtadele
$query1=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT country,city FROM data");
$i=1;
while ($query2= mysqli_fetch_array($query1))
{
$i++;
mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO places (place_id,country,city) VALUES ('".$i."','".$query2['country']."','".$query2['city']."')");
}
//loome unikaalset identifikaatorid automargidele
$query5=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT cartype FROM data");
$i=1;
while ($query6= mysqli_fetch_array($query5))
{
$i++;
mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO cartypes (cartype_id,cartype) VALUES ('".$i."','".$query6['cartype']."')");
}
//Paneme unikaalsed kohtade j amarkide identifikaatorid tabelisse orders
$query3=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM data");
$i=1;
while ($query4= mysqli_fetch_array($query3))
{
$i++;
$place=getsqlfield("SELECT place_id AS 'place' FROM places WHERE country='".$query4['country']."' AND city='".$query4['city']."'","place");
$type=getsqlfield("SELECT cartype_id AS 'cartype' FROM cartypes WHERE cartype='".$query4['cartype']."'","cartype");
mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO orders (order_id,place_id,cartype_id,price) VALUES ('".$i."','".$place."','".$type."','".$query4['price']."')");
}
?>