<? 
## v5.24 -> apr. 05, 2006
if ($_SESSION['membersarea']=="Active"&&$_SESSION['is_seller']=="Y") { 

if ( !defined('INCLUDED') ) { die("Access Denied"); }

$noDisplay = FALSE;

include_once ("formchecker.php");

if ($action=="aboutme_save") {
	if ($_POST['aboutpage_type']==1) $isaboutme = 'Y';
	else $isaboutme = 'N';
	
	$updateAboutSetts = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	isaboutme = '".$isaboutme."', aboutpage_type='".$_POST['aboutpage_type']."', aboutmepage='".remSpecialChars($_POST['content'])."' 
	WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
}
		
if (!$noDisplay) {
	$shopDetails=getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'"); 
?> 
<form action="membersarea.php?page=aboutme" method="post" enctype="multipart/form-data"> 
  <input type="hidden" name="oldlogo" value="<?=$shopDetails['shop_logo'];?>"> 
  <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border"> 
    <tr> 
      <td colspan="2" align="center" class="c1"><?=$lang[aboutme_title]?></td> 
    </tr> 
    <tr class="c4"> 
      <td colspan="2" class="contentfont"><?=$lang[aboutme_msg_new]?><br><br>
	  <? echo "<strong>".$lang[store_status]."</strong>: ".(($shopDetails['aboutpage_type']==2) ? "<span class=greenfont>$lang[enabled]</span>" : "<span class=redfont>".$lang[disabled]."</span>");?></td> 
    </tr> 
    <tr class="c2"> 
      <td class="contentfont"><?=$lang[enable_aboutme]?> </td> 
      <td class="contentfont">
	  	<input type="radio" name="aboutpage_type" value="1" <? echo (($shopDetails['aboutpage_type']==1)?"checked":"");?>> <?=$lang[yes]?> 
        <input type="radio" name="aboutpage_type" value="" <? echo (($shopDetails['aboutpage_type']!=1)?"checked":"");?>> <?=$lang[no]?></td> 
    </tr> 
    <tr class="c3"> 
      <td class="contentfont"><?=$lang[aboutmepagecontent]?></td> 
      <td class="contentfont"><textarea name="content" cols="45" rows="10" id="content"><? echo addSpecialChars($shopDetails['aboutmepage']);?></textarea>
		<script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script></td> 
    </tr> 
    <tr class="c2"> 
      <td colspan="2" align="center" class="verdana12black"><input type="submit" name="aboutmesaveok" value="<?=$lang[savesetts]?>"></td> 
    </tr> 
  </table> 
</form> 
<br> 
<? 
} ## end of noDisplay check
} else if ($_SESSION['is_seller']!="Y") {
	echo $lang[seller_error]; 
} else { 
	echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; 
} ?> 
