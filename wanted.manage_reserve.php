<?
## v5.24 -> apr. 04, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

/*header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");*/

include("membersmenu.php");

$_REQUEST['description']=$_POST['description_main'];
include ("formchecker.php");

if ($_REQUEST['action']=="submit_wanted_ad") {
	### V4 Request: here we calculate the closing date, which can change if the duration is changed. 
	if ($_REQUEST['mode']=="edit") $startDate = getSqlField("SELECT startdate FROM probid_wanted_ads WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'","startdate");
	else $startDate = date( "Y-m-d H:i:s", time() );
	
	$closingdate = closingdate($startDate,$_REQUEST['duration']);
	$name = titleResize($_POST['name']);
	$name = remSpecialChars($name);
	$description = remSpecialChars($_POST['description_main']);
	$getWordFilter = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_wordfilter");
	while ($wordFilter = mysqli_fetch_array($getWordFilter)) {
		$name=@eregi_replace($wordFilter['word']," ",$name);
		$description=@eregi_replace($wordFilter['word']," ",$description);
	}
	
	$keywords = $name." ".$description;

	if ($_REQUEST['mode']=="edit") {
		$oldMainImage = getSqlField("SELECT * FROM probid_auctions WHERE id='".$_POST['id']."'","picpath");
		$updateitem = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
		itemname='".$name."',description='".$description."',
		duration='".$_POST['duration']."',country='".$_POST['country']."',
		zip='".$_POST['zip']."',category='".$_POST['category']."',enddate='".$closingdate."',keywords='".$keywords."',
		addlcategory='".$_POST['addlcategory']."' WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		
		addwantedcount ($_POST['category']);
		addwantedcount ($_POST['addlcategory']);
		delwantedcount ($_POST['cat_prv']);
		delwantedcount ($_POST['addlcat_prv']);

		$auctionid = $_POST['id'];
		
		echo "<br><p class=contentfont align=center>$lang[aucupdate_success]</p><p>&nbsp;</p>";
		$link = "membersarea.php?page=wanted";
		echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
	} else {
		## check for fee.
		if ($fee['wantedad_fee']>0&&!freeFees($_SESSION['memberid'])) {
			$payment_status="unconfirmed";
			$active=0;
		} else {
			$payment_status="confirmed";
			$active=1;
			## also add the counter
			addwantedcount ($_POST['category']);
			addwantedcount ($_POST['addlcategory']);
		}
		
		$additem = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_wanted_ads 
		(itemname, description, duration, country, zip, category, startdate, enddate, keywords, addlcategory, active, payment_status, ownerid)
		VALUES
		('".$name."','".$description."','".$_POST['duration']."','".$_POST['country']."',
		'".$_POST['zip']."','".$_POST['category']."','".$startDate."','".$closingdate."','".$keywords."','".$_POST['addlcategory']."','".$active."','".$payment_status."','".$_SESSION['memberid']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		
		$auctionid=((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);

		if ($fee['wantedad_fee']>0&&!freeFees($_SESSION['memberid'])) {
			### the function for the auction setup fees
			wantedAdFee($auctionid);
		} else {
			echo "<br><p class=contentfont align=center>$lang[aucupdate_success]</p><p>&nbsp;</p>";
			$link = "membersarea.php?page=wanted";
			echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
		}
	}

	### insert the custom fields for the auction (if available)
  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_wanted_fields") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  	$isFields = mysqli_num_rows($getFields);
  	if ($isFields) {
		$delFields = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_wanted_fields_data WHERE wantedadid='".$auctionid."' AND ownerid='".$_SESSION['memberid']."'");
		while ($fields=mysqli_fetch_array($getFields)) {
			$box_value = "";
			if ($fields['boxtype']=="checkbox") {
				for ($i=0; $i<count($_POST['box'.$fields['boxid']]); $i++) 
					$box_value .= $_POST['box'.$fields['boxid']][$i]."; ";
			} else { 
				$box_value = $_POST['box'.$fields['boxid']];
			}
			$addFieldData = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_wanted_fields_data
			(wantedadid, ownerid, boxid, boxvalue) VALUES
			('".$auctionid."','".$_SESSION['memberid']."','".$fields['boxid']."','".remSpecialChars($box_value)."')");
		}
	}		

	## pics upload -> qid=881 addon
	### this is the saver for the main image
	$tempNumber = md5(uniqid(rand(2, 999999999)));
	if ($_FILES['file']['name']!=""||$_POST['mainpicurl']) {
		$imgMaxSize = $setts['pic_gal_max_size']*1024;
		if ($_FILES['file']['size']<$imgMaxSize||$_POST['mainpicurl']) {
			if (!preg_match("/^http:\/\//is",$oldMainImage)) {
				deleteFile("",$oldMainImage);
			}
			if ($_POST['mainpicurl']) {
				$imageName="http://".str_replace("http://","",$_POST['mainpicurl']);
				$isUpload=true;
			} else {
				$fileExtension = getFileExtension($_FILES['file']['name']);
				$imageName = "mb".$_SESSION['memberid']."_".$tempNumber."_mainpic.".$fileExtension;
				$isUpload = uploadFile($_FILES['file']['tmp_name'],$imageName,"uplimg/");
				$imageName="uplimg/".$imageName;
			}
			if ($isUpload) {
				$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
				picpath='".$imageName."' WHERE id='".$auctionid."' AND ownerid='".$_SESSION['memberid']."'");
			}
		} else {
			echo "$lang[auc_errpicsize1] $setts[pic_gal_max_size] kb<br>$lang[auc_errpicsize2]<br>";
		}			
	}
} else {
	if ($_REQUEST['mode']=="edit") {
		$ad = getSqlRow("SELECT * FROM probid_wanted_ads WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
		$isAd = getSqlNumber("SELECT * FROM probid_wanted_ads WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."' AND active=1");
	}

	## delete pictures
	if (!empty($_REQUEST['deletepic'])) {
		if (@eregi('mainpic', $_REQUEST['deletepic'])) {
			$isPic = getSqlNumber("SELECT id FROM probid_wanted_ads WHERE picpath='".$ad['picpath']."'");
			if (!$isPic) @unlink($_REQUEST['mainpic']);
			$remPicDb = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET picpath='' WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
			$ad['picpath'] = '';
			
			## we also delete all additional pics
			/*$getAddImages = mysql_query("SELECT i.* FROM probid_auction_images i, probid_auctions a WHERE i.auctionid='".$_REQUEST['id']."' AND a.id=i.auctionid AND a.ownerid='".$_SESSION['memberid']."'");
			while ($addImage = mysql_fetch_array($getAddImages)) {
				deleteFile("",$addImage['name']);
			}
			$deleteImages=mysql_query("DELETE FROM probid_auction_images WHERE auctionid='".$_POST['id']."'");*/

		} 
		/*else if (@eregi('addpic', $_REQUEST['deletepic'])) {
			$picDets = explode('_',$_REQUEST['deletepic']);
			$getAP = mysql_query("SELECT i.* FROM probid_auction_images i, probid_auctions a WHERE i.auctionid='".$_REQUEST['id']."' AND a.id=i.auctionid AND a.ownerid='".$_SESSION['memberid']."'");
			$addPicRes = @mysql_result($getAP,$picDets[1],'name');
			$addPicId = @mysql_result($getAP,$picDets[1],'id');
			@unlink($addPicRes);
			$remAddPicDb = mysql_query("DELETE FROM probid_auction_images WHERE id='".$addPicId."'");
		}*/
	}
	
	if ($_REQUEST['mode']=="edit"&&$isAd==0) {
		echo "<br><p align=center class=contentfont>$lang[editauctionerror1]</p>\n";
		echo "<p class=contentfont align=center><strong><a href=membersarea.php?page=selling>$lang[editreditectmsg]</a></strong></p>";
	} else if ($_REQUEST['mode']=="edit"&&$ad['nrbids']>0) {
		echo "<br><p align=center class=contentfont>$lang[editauctionerror2]</p>\n";
		echo "<p class=contentfont align=center><strong><a href=membersarea.php?page=selling>$lang[editreditectmsg]</a></strong></p>";
	} else { 
	
		$adEdit['itemname'] = (trim($_POST['name'])!="") ? $_POST['name'] : $ad['itemname'];
		$adEdit['description'] = (trim($_POST['description_main'])!="") ? remSpecialChars($_POST['description_main']) : $ad['description'];
		$adEdit['category'] = (trim($_POST['category'])!="") ? $_POST['category'] : $ad['category'];
		$adEdit['addlcategory'] = (trim($_POST['addlcategory'])!="") ? $_POST['addlcategory'] : $ad['addlcategory'];
		$adEdit['duration'] = (trim($_POST['duration'])!="") ? $_POST['duration'] : $ad['duration'];	
?>
<script language="JavaScript">
function delete_pic(theform,pic) {
	theform.deletepic.value = pic;
	theform.submit();
}

function submitform() {
	document.wanted_manage.onsubmit();
	document.wanted_manage.submit();
}
</script>

<form action="wanted.manage.php" method="post" enctype="multipart/form-data" name="wanted_manage">
   <input type="hidden" name="deletepic" value="">
   <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
   <input type="hidden" name="mode" value="<?=$_REQUEST['mode'];?>">
   <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border">
      <tr align="center" class="c1">
         <td colspan="3"><? echo ($_REQUEST['mode']=="edit") ? $lang[editwanted] : $lang[newwanted]; ?></td>
      </tr>
      <tr class="c2">
         <td colspan="3"><strong>
            <?=$lang[choosecat]?>
            </strong><br>
            <br>
            <input type="hidden" name="cat_prv" value="<?=$ad['category'];?>">
            <select name="category" class="contentfont" onchange="submitform();">
               <?
					reset($cat_array);
					while (list($catid, $cat_array_details) = each ($cat_array)){
						list($catname, $userid) = $cat_array_details;
						if ($userid==0) echo "<option value=\"".$catid."\" ".(($adEdit['category'] == $catid)?"selected":"").">".$catname."</option>";
					} ?>
            </select></td>
      </tr>
      <? if ($setts['secondcategory']) { ?>
      <tr class="c2">
         <td colspan="3"><strong>
            <?=$lang[chooseaddlcat]?>
            </strong><br>
            <br>
            <input type="hidden" name="addlcat_prv" value="<?=$ad['addlcategory'];?>">
            <select name="addlcategory" class="contentfont" onchange="submitform();">
               <?
					reset($cat_array);
					echo "<option value=\"\" selected>-- no additional category --</option>";
					while (list($catid, $cat_array_details) = each ($cat_array)){
						list($catname, $userid) = $cat_array_details;
						if ($userid==0) echo "<option value=\"".$catid."\" ".(($adEdit['addlcategory'] == $catid)?"selected":"").">".$catname."</option>";
					} ?>
            </select></td>
      </tr>
      <? } ?>
      <tr class="c3">
         <td width="110" align="right"><strong>
            <?=$lang[itemname]?>
            </strong></td>
         <td colspan="2"><input name="name" type="text" id="name" value="<? echo addSpecialChars($adEdit['itemname']);?>" maxlength="60"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><strong>
            <?=$lang[descr]?>
            </strong></td>
         <td colspan="2"><textarea name="description_main" cols="45" rows="8" id="description_main"><? echo addSpecialChars($adEdit['description']);?></textarea>
            <script> 
					var oEdit1 = new InnovaEditor("oEdit1");
					oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit1.height=300;
					oEdit1.REPLACE("description_main");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[auctype]?>
            </strong></td>
         <td colspan="2">Wanted Ad </td>
      </tr>
      <?
		if ($_REQUEST['mode']=="edit") {
			$country = $ad['country'];
			$zip = $ad['zip'];
		} else {
			$userDets = getSqlRow("SELECT zip, country FROM probid_users WHERE id='".$_SESSION['memberid']."'");
			$country = ($_POST['country']=="") ? $userDets['country'] : $_POST['country'];
			$zip = ($_REQUEST['zip']=="") ? $userDets['zip'] : $_REQUEST['zip'];
		}
		?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[uploadpic]?>
            </strong><br>
            <?=$lang[optfield];?></td>
         <td colspan="2"><table border="0" cellspacing="2" cellpadding="2">
               <tr>
                  <td><input type="file" name="file" class="contentfont" />
                     <br />
                     <b>
                     <?=$lang['orurl']?>
                     </b>http://
                     <input type="text" name="mainpicurl" />
                  </td>
                  <? if (!empty($ad['picpath'])) { ?>
                  <td><? echo "&nbsp;$lang[current_pic]: <img src=\"makethumb.php?pic=".$ad['picpath']."&w=50&sq=Y&b=Y\" align=absmiddle> ";?> </td>
                  <td><? echo $lang[delete].": <input type=\"checkbox\" onClick=\"delete_pic(this.form,'mainpic');\"> "; ?></td>
                  <? } ?>
               </tr>
            </table>
            <?=$lang[imgmax_note1]." ".$setts['pic_gal_max_size']."KB";?></td>
      </tr>
      <? 
		$mainCat_primary = getMainCat($adEdit['category']);
		$mainCat_secondary = getMainCat($adEdit['addlcategory']);
		$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_wanted_fields WHERE 
		(categoryid='".$mainCat_primary."' OR categoryid='".$mainCat_secondary."' OR categoryid='0') ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
		$isFields = mysqli_num_rows($getFields);
		if ($isFields) {
			while ($fields=mysqli_fetch_array($getFields)) {
				$fieldData = getSqlRow("SELECT boxvalue FROM probid_wanted_fields_data WHERE wantedadid='".$ad['id']."' AND ownerid='".$_SESSION['memberid']."' AND boxid='".$fields['boxid']."'"); 
				if ($fields['boxtype']=="checkbox") $selectedValue = explode(";",$fieldData['boxvalue']); 
				else $selectedValue = $fieldData['boxvalue']; ?>
      <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>">
         <td align="right"><strong>
            <?=$fields['boxname'];?>
            </strong></td>
         <td colspan="2"><? echo createField($fields['boxid'],$selectedValue,'probid_wanted_fields'); ?> </td>
      </tr>
      <? } 
  		} ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[country]?>
            </strong></td>
         <td colspan="2"><? 
				echo "<SELECT name=\"country\" class=\"contentfont\">";
				$getCountries=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries");
				while ($countryDetails=mysqli_fetch_array($getCountries)) {
					echo "<OPTION value=\"".$countryDetails['name']."\" ".(($countryDetails['name']==$country)?"SELECTED":"").">".$countryDetails['name']."</option>";
				}
				echo "</SELECT>";
				?>
         </td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[zip]?>
            </strong></td>
         <td colspan="2"><input name="zip" type="text" id="zip" value="<?=$zip;?>" size="12"></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[duration]?>
            </strong></td>
         <td colspan="2"><? echo "<SELECT name=\"duration\" class=\"contentfont\">";
				$getdurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations");
				while ($row=mysqli_fetch_array($getdurations)) {
					echo "<OPTION value=\"".$row['days']."\" ".(($row['days']==$adEdit['duration'])?"SELECTED":"").">".$row['description']."</option>";
				}
				echo "</SELECT>";
				?> </td>
      </tr>
      <tr align="center" class="c4">
         <td colspan="3"><input name="wantedadok" type="submit" id="wantedadok" value="<?=(($_REQUEST['mode']=='edit') ? $lang[modify] : $lang[submit]);?>"></td>
      </tr>
   </table>
</form>
<? 	}
	}
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
