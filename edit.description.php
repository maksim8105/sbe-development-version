<?
## v5.24 -> may. 03, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

if ($_SESSION['is_seller']!="Y") header ("Location: membersarea.php");

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");
//<body onLoad="changepage(document.sistep1);"> 

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");

include("membersmenu.php");

$_REQUEST['description']=$_POST['description_main'];
include ("formchecker.php");

if ($_REQUEST['action']=="updatedesc") {
	$auctionDetails = getSqlRow("SELECT itemname, description FROM probid_auctions WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");	
	
	$timenow = date($setts['date_format'],$currentTimeOffset);
	
	$description = $auctionDetails['description']."<br><hr>$lang[on] ".$timenow.", ".$_SESSION['memberusern']." $lang[hasadded]:<br><br>".$_REQUEST['description'];

	$updateDescription = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
	description='".remSpecialChars($description)."' WHERE 
	id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

	echo "<br><p class=contentfont align=center>$lang[aucupdate_success]</p><p>&nbsp;</p>";
	$link = "membersarea.php?page=selling";
	echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
} else {
	$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
	$isAuction = getSqlNumber("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
	## get time before auction ends in seconds
	## if under the set number of hours are left, the auction owner wont be able to edit it anymore.
	$minHours = 12; ## set to 12 hours.
	$timeLeft = daysleft($auction['enddate'],$setts['date_format']);
	$isTime = $timeLeft - ($minHours * 60 * 60);
	if ($isAuction==0) {
		echo "<br><p align=center class=contentfont>$lang[editauctionerror1]</p>\n";
		echo "<p class=contentfont align=center><strong><a href=membersarea.php?page=selling>$lang[editreditectmsg]</a></strong></p>";
	} else { ?>
 <form action="edit.description.php" method="post" enctype="multipart/form-data"> 
  <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>"> 
  <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border"> 
     <tr align="center" class="c1"> 
      <td colspan="3"><?=$lang[editdesctitle]?></td> 
    </tr> 
     <tr class="c3"> 
      <td width="110" align="right" valign="top"><? echo "<strong>".$lang[descr]."</strong>";?></td> 
      <td colspan="2"> 
			<p><?=$lang[editdescexpl2]; ?></p>
			<textarea name="description_main" cols="45" rows="8" id="description_main"></textarea> 
			<script> 
				var oEdit1 = new InnovaEditor("oEdit1");
				oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
				oEdit1.height=300;
				oEdit1.REPLACE("description_main");//Specify the id of the textarea here
			</script>
	  </td> 
    </tr> 
     <tr align="center" class="c4"> 
      <td colspan="3"><input name="editdescok" type="submit" id="editdescok" value="<?=$lang[modify]?>"></td> 
    </tr> 
   </table> 
</form> 
<? 	}
	}
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?> 
