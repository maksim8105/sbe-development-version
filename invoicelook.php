<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

/*header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] ".$_SESSION['membername']);*/

include("membersmenu.php"); ?>

<table border="0" cellspacing="0" cellpadding="0" height="30" align="center">
   <tr>
      <td class="categoryitem">
      <a href="membersarea.php?page=preferences"><?=$lang[Cap_members]?></a>&nbsp;|&nbsp;
      <a href="invoicelook.php"><?=$lang[invlook]?></a>&nbsp;|&nbsp;<a href="mailprefs.php">
         <?=$lang[mailprefs];?>
         </a></td>
   </tr>
</table>

<table width="100%"  border="0" cellspacing="2" cellpadding="4" class="border">
  <tr class="c1">
    <td nowrap><?=$lang[date]?></td>
    <td width="100%" ><?=$lang[type]?></td>
    <td nowrap><?=$lang[itemid]?>
    </td>
    <td nowrap><?=$lang[amount]?></td>
    <td nowrap><?=$lang[balance]?></td>
  </tr>
  <? 
  	$sql = "SELECT *, sum(feevalue) as total FROM probid_invoices WHERE userid = $_SESSION[memberid] AND auctionid > 0 AND feedate > UNIX_TIMESTAMP(SUBDATE('".$timeNowMysql."',INTERVAL 1 MONTH)) GROUP BY auctionid 
	UNION 
	SELECT *, feevalue as total FROM probid_invoices WHERE userid = $_SESSION[memberid] AND auctionid = 0 AND feedate > UNIX_TIMESTAMP(SUBDATE('".$timeNowMysql."',INTERVAL 1 MONTH)) GROUP BY id ORDER BY id DESC";
	$getInvoices = mysqli_query($GLOBALS["___mysqli_ston"], $sql) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	$invoices = array();
  	while ($invoice = mysqli_fetch_array($getInvoices)) { 
		$invBalance = getSqlField("SELECT balance FROM probid_invoices WHERE auctionid='".$invoice['auctionid']."' AND userid='".$_SESSION['memberid']."' ORDER BY id DESC LIMIT 0,1","balance");
		if ($invBalance<=0) {
			$thevar = $lang[credit];
			$balance = abs($invBalance);
		} else {
			$thevar = $lang[debit];
			$balance = $invBalance;
		} ?>
  <tr class="<? echo (($count++)%2)?"c2":"c3"; ?> contentfont">
    <td nowrap><? 
	$feeDate = date("Y-m-d H:i:s",$invoice['feedate']);
	echo displaydatetime($feeDate,$setts['date_format']); 
	?></td>
    <td width="100%"><? 
	if($invoice['auctionid']) {  
    	echo "<a href=\"invoice_print.php?id=".$invoice['auctionid']."\" target=\"_blank\">".$lang[viewinvoice]."</a>"; 
	} else echo $invoice['feename']; ?>
    </td>
    <td align="center" nowrap><? 
	echo ($invoice['auctionid']!=0) ? "<a href=\"".((@eregi($lang[wantedad_fee],$invoice['feename'])) ? "wanted.details" : "auctiondetails").".php?id=".$invoice['auctionid']."\"># ".$invoice['auctionid']."</a>" : ""; 
	?></td>
    <td align="center" nowrap><? echo displayAmount((($invoice['auctionid']==0) ? $invoice['feevalue'] : $invoice['total']),$setts['currency'],"YES"); ?></td>
    <td align="center" nowrap><? echo displayAmount((($invoice['auctionid']==0) ? $invoice['balance'] : $balance),$setts['currency'],"YES")." ".$thevar; ?></td>
  </tr>
  <? } ?>
</table>
<? 	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
