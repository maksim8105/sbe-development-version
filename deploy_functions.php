<?php

function check_login()
{
	if($_SESSION['adminarea'] == 'Active' || $_SERVER['SERVER_NAME']=='localhost')
	{
		echo 'Logged in as administrator. <a href="admin/index.php?option=logout">Logout</a><br>';
	}
	else
	{
		echo 'Not logged in. Log in using <a href = "admin/login.php">administrator panel</a> and run the script again.';
		die();
	}
}

function copy_site( $src, $dest, $exclude )
{
	//find symlinks TODO
	// print_r( system('find ./ -maxdepth 2 -iname \'index.php\'') );
	
	// $command = " -arfvP $src/ $dest/";
		/* a = save all attributes and dates
		r = recursive
		f = force
		v = verbose (print all info)
		P = do not follow symbolic links (because some of links in project point to root directory, the entry root can be copied
			without -P parameter. ) */
	
	copydirect($src, $dest, $exclude);
	
	// $exclude_list = array();
	// foreach($exclude as $k => $v)
	// {
		// $exclude_list[] = $src.'/'.$v;
	// }
	// ?>excl. list <br><?
	
	print_r($exclude_list);

	echo "command is: $command";
	
	//print_r( system('find ./ -type l') );
	// print_r(system('pwd'));
}

function delete_uplimg_enabled()
{
	if($_POST['delete_dest_uplimg']=='yes')
		return True;
	else
		return False;
}

function delete_uplimg($dir)
{
	echo "Deleting uplimg folder: $dir\n";
}

function rm_rf($dir, $exclude)
{
	// print_r($exclude);
	if($handle = opendir($dir))
	{
		while(($file = readdir($handle)) != false)
		{
			// echo "working on file $file\n";
			if($file != '.' && $file != '..')
			{
				$path = $dir.'/'.$file;
				// echo "Path is $path\n";
				if ( in_array($path, $exclude) )
				{
					echo "\tIgnoring $path\n";
				}
				elseif(is_dir($path))
				{
					// echo "Entering $path\n";
					rm_rf($path, $exclude);
					@rmdir($path);
				}
				else
				{
					// echo "Deleting $path\n";
					unlink($path);
				}
			}
		}
		closedir($handle);
	}
}

/* function taken from http://hashcode.ru/questions/143138/php-%D1%80%D0%B5%D0%BA%D1%83%D1%80%D1%81%D0%B8%D0%B2%D0%BD%D0%BE%D0%B5-%D0%BA%D0%BE%D0%BF%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B4%D0%B8%D1%80%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%B8%D0%B9
*/
function copydirect($source, $dest, $exclude)
{
	if($handle = opendir($source))
	{  
		while(false !== ($item = readdir($handle)))
		{
			if($item != '.' && $item != '..')
			{
				$fullsrcpath = $source . '/' . $item;
				if(in_array($fullsrcpath, $exclude))
				{
					echo "\tIgnoring $fullsrcpath\n";
				}
				elseif(is_file($fullsrcpath))
				{
					if(!copy($fullsrcpath, $dest . '/' . $item))
					{
						echo "\tError copying $fullsrcpath.\n";
					}
					echo "\tCopying file $fullsrcpath\n";
				} 
				elseif(is_dir($fullsrcpath))
				{
					if(!is_dir($dest . '/' . $item))
						mkdir($dest . '/' . $item);
					$fulldestpath = $dest.'/'.$item;
					copydirect($fullsrcpath, $fulldestpath, $exclude);
				}
				/* added these lines */
				elseif(is_link($fullsrcpath))
				{
					echo "Ignoring link. Please manually copy it.\n";
				}
			}
		}
		closedir($handle);
	}
}

?>