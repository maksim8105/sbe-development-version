<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$prefSeller = "N";
if ($setts['pref_sellers']=="Y") {
	$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
	WHERE id='".$_SESSION['memberid']."'","preferred_seller");
}
?>

<table border="0" cellpadding="5" cellspacing="2" width="100%" class="membmenu">
   <tr class="membmenucell"  height="30" valign="center" align="center" >
      <? if ($_SESSION['accsusp']!=2) { ?>
      <td class="membutt" align="center">
         <a href="membersarea.php?page=bidding"><?=$lang[bidding_img]?></a></td>
      <? if ($_SESSION['is_seller']=="Y") { ?>
      <td class="membutt" align="center">
      <a href="membersarea.php?page=selling"><?=$lang[selling_img]?></a></td>
      <? } ?>
      <td class="membutt" align="center">
      <a href="membersarea.php?page=feedback"><?=$lang[feedback_img]?></a></td>
      <? if ($_SESSION['is_seller']=="Y"&&$setts['enable_bulk_lister']==1) { ?>
      <td><a class="membutt" href="membersarea.php?page=bulk"> <img src="themes/<?=$setts['default_theme'];?>/img/system/ma_bulk.gif" border="0"><br>
         <?=$lang[bulk_img]?>
         </a></td>
      <? } ?>
      <? if ($setts['stores_enabled']=="Y") { ?>
      <td class="membutt" align="center">
      <a href="membersarea.php?page=store"><?=$lang[store_img]?></a></td>
      <? } ?>
      <? if ($setts['enable_wantedads']=="Y") { ?>
      <td class="membutt" align="center">
      <a href="membersarea.php?page=wanted"><?=$lang[wanted_img]?></a></td>
      <? } ?>
      <? } ?>
      <td class="membutt" align="center">
      <a href="membersarea.php?page=preferences"><?=$lang[details_img]?></a></td>
      <td class="membutt" align="center">
      <a href="history.php"><?=$lang['acc_history'];?></a></td>
      <td class="membutt" align="center">
      <a href="membersarea.php?page=block_users"><?=$lang['blockusers'];?></a></td>
   </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
   <tr>
      <td class="c4"><? echo (($prefSeller=="Y")?"<div align=\"center\" style=\"padding: 4px;\"> [ <strong>".$lang[pref_seller]." - ".$setts['pref_sellers_reduction']."% ".$lang[reduction]."</strong> ] </div>":"");?> </td>
   </tr>
</table>
<br>
<? 
## v5.22 addition - if the user is in account mode, and his account is approaching the credit limit, he will be warned by a message in the 
## members area
$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
$info = getSqlRow("SELECT payment_mode, allowed_credit FROM probid_users WHERE id='".$_SESSION['memberid']."'");
$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$_SESSION['memberid']."'","balance");

if ($setts['account_mode_personal']==1) {
   	$account_mode_local = ($tmp) ? 2 : 1;
} else $account_mode_local = $setts['account_mode'];

if ($setts['account_mode_personal']==0 && $setts['account_mode']==2) {
  	$max_cr = $setts['max_credit'];
} else if ($setts['account_mode_personal']==1 && $info['payment_mode']==1) { 
	$max_cr = $info['allowed_credit'];
}

if ($account_mode_local==2&&$max_cr<=(2+$currentBalance)) { ?>
<table border="0" cellpadding="4" cellspacing="4" width="100%" class="errormessage">
   <tr>
      <td><?=$lang[warning_approaching_credit_limit];?></td>
   </tr>
</table>
<br>
<? } ?>
