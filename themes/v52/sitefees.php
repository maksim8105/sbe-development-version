<?
## v5.25 -> jun. 08, 2006
session_start();
include_once ("config/config.php");
$category_id = (!empty($_POST['sel_cat'])) ? $_POST['sel_cat'] : 0;

### we overwrite the $fees array with what we need
$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

include ("themes/".$setts['default_theme']."/header.php");
/*header5("$lang[aucfees]");*/ ?>
<table width="100%" border="0" cellpadding="4" cellspacing="4" class="border">
   <? if ($fee['is_setup_fee']=="Y") { ?>
   <tr align="center" class="c4">
      <td colspan="7"><strong>
         <?=$lang[listfee]?>
         </strong></td>
   </tr>
   <tr align="center">
      <td colspan="2">
            <? $getTiers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='setup' AND category_id='".$category_id."'");
		while ($feeTiers = mysqli_fetch_array($getTiers)) { ?>
            <tr>
               <td width="100%"><? echo "$lang[from] ".displayAmount($feeTiers['fee_from'])."ssssssss $lang[to] ".displayAmount($feeTiers['fee_to'])." (".number_format($feeTiers['fee_to']/15.6466,2,',','.')." EUR)";?></td>
               <td nowrap><?
		  	if ($feeTiers['calc_type']=="flat") echo displayAmount($feeTiers['fee_amount']);
		   	else echo $feeTiers['fee_amount']."%"; ?></td>
            </tr>
            <? } ?>
         </td>
   </tr>
   <? } ?>
   <? if ($fee['second_cat_fee']>0) { ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[secondcatfee]?>
         </strong></td>
      <td><? echo displayAmount($fee['second_cat_fee']); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[addpicfee]?>
         </strong></td>
      <td><? echo displayAmount($fee['val_pic_fee']); ?> </td>
   </tr>
   <? } ?>

   <? if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0) { ?>
   <tr>
      <td align="right" colspan="4"><strong>
         <?=$lang[highlightfee]?>
         </strong></td>
      <td colspan="2"><? echo displayAmount($fee['val_hlitem_fee']); ?>
         <? if ($_REQUEST['addlcategory']>0) echo " x 2 = " . displayAmount($fee['val_hlitem_fee'] * 2)?>(<?=displayAmount($fee['val_hlitem_fee']/15.64,"EUR")?>)</td>
   </tr>
   <? } ?>
   <? if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0) { ?>
   <tr>
      <td align="right" colspan="4"><strong>
         <?=$lang[boldfee]?>
         </strong></td>
      <td colspan="2"><? echo displayAmount($fee['val_bolditem_fee']); ?>
         <? if ($_REQUEST['addlcategory']>0) echo " x 2 = " . displayAmount($fee['val_bolditem_fee'] * 2); ?>(<?=displayAmount($fee['val_bolditem_fee']/15.64,"EUR")?>)</td>
   </tr>
   <? } ?>
   <? if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0) { ?>
   <tr>
      <td align="right" colspan="4"><strong>
         <?=$lang[catfee]?>
         </strong></td>
      <td colspan="2"><? echo displayAmount($fee['val_catfeat_fee']); ?>
         <? if ($_REQUEST['addlcategory']>0) echo " x 2 = " . displayAmount($fee['val_catfeat_fee'] * 2); ?>(<?=displayAmount($fee['val_catfeat_fee']/15.64,"EUR")?>)
      </td>
   </tr>
   <? } ?>
   <? if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0) { ?>
   <tr>
      <td align="right" colspan="4"><strong>
         <?=$lang[homepagefee]?>
         </strong></td>
      <td colspan="2"><? echo displayAmount($fee['val_hpfeat_fee']); ?>(<?=displayAmount(($fee['val_hpfeat_fee'])/15.64,"EUR")?>) </td>
   </tr>
   <? } ?>
   <? if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0) { ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[respricefee]?>
         </strong></td>
      <td><? echo displayAmount($fee['val_rp_fee']); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_swap_fee']=="Y"&&$fee['val_swap_fee']>0) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[swap_fee]?>
         </strong></td>
      <td><? echo displayAmount($fee['val_swap_fee']); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['bin_fee']>0) { ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[bin_item_fee]?>
         </strong></td>
      <td><? echo displayAmount($fee['bin_fee']); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['custom_st_fee']>0) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[custom_st_fee]?>
         </strong></td>
      <td><? echo displayAmount($fee['custom_st_fee']); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['videofile_fee']>0) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[videofile_fee]?>
         </strong></td>
      <td><? echo displayAmount($fee['videofile_fee']); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['wantedad_fee']>0) { ?>
   <tr class="c3">
      <td align="right"><strong>
         <?=$lang[wantedad_fee]?>
         </strong></td>
      <td><? echo displayAmount($fee['wantedad_fee']); ?> </td>
   </tr>
   <? } ?>
   
   <?
	$nbStoreAccTypes = getSqlNumber("SELECT id FROM probid_fees_tiers WHERE fee_type='store'"); 
	if ($fee['is_store_fee'] == "Y" && $nbStoreAccTypes > 0) { ?>
   
   <tr><td align="center" colspan="7"></td></tr>
   <tr class="c4">
      <td align="center" colspan="7"><strong> <?=$lang[stores_price_header]?> </strong><br></td>
   </tr>
   <tr class="hblueline"><td class="hblueline" align="center" colspan="7"></td></tr><!--class="c2"-->
   
   <td align="center" class="blueline"><strong><?=$lang[subscription_type]?></strong></td>
   <td align="center"><strong><?=$lang[itemsquant]?></strong></td>
   <td align="center"><strong><?=$lang[store_design]?></strong></td>
   <td align="center"><strong><?=$lang[store_consulting]?></strong></td>
   <td align="center"><strong><?=$lang[third_level_domain]?></strong></td>
   <td align="center"><strong><?=$lang[monthly_amount]?> (EEK)</strong></td>
   <td align="center"><strong><?=$lang[monthly_amount]?> (EUR)</strong></td>
   </tr>
   <tr class="c5">
    </tr>
   
   <?
	$getStoreTypes = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='store' ORDER BY store_nb_items"); 
	while ($storeTypes = mysqli_fetch_array($getStoreTypes)) { ?>
   <tr>
      <td class="contentfont"><? 
		echo "<strong>".$storeTypes['store_name']."</strong>"; ?>
      </td>
      <td class="contentfont" align="center"><? 
      
       $fee_amount= number_format($storeTypes['fee_amount']/15.6466,2,',','.');
      
		echo $storeTypes['store_nb_items']."</td><td>";
		 if ($storeTypes['service1']==0)
		  {
		  echo $lang[standart_design];
		  }
		 else 
		 {
		 echo $lang[uniq_design];
		 }
		 ;
		 echo "</td><td align='center'>";
		  if ($storeTypes['service3']==0)
		  {
		  echo "-";
		  }
		 else 
		 {
		 echo "+";
		 }
		 ;
		 echo "</td><td align='center'>";
		 if ($storeTypes['service2']==0)
		  {
		  echo "-";
		  }
		 else 
		 {
		 echo "+";
		 }
		 ;
		 echo "</td><td align='center'>".displayAmount($storeTypes['fee_amount'],$setts['currency'], 'YES')."</td><td align='center'>".$fee_amount." EUR"."</td>"; ?>
      </td>
   </tr>
   
   <? } ?>
   <? } ?>
   <tr class="c2"><td align="center" colspan="7"></td></tr>
   <tr><td colspan="7" align="justify"><?=$lang[store_comm_1]?><br><?=$lang[store_comm_2]?><br><?=$lang[store_comm_3]?><br><?=$lang[store_comm_4]?><br></td></tr>
   <? if ($fee['is_endauction_fee']=="Y") { ?>
   <tr class="c5">
      </tr>
   <tr class="c4">
      <td align="center" colspan="2"><strong>
         <?=$lang[endaucfee]?>
         <? echo (@eregi('s', $fee['endauction_fee_applies'])) ? $lang[paidbyseller] : $lang[paidbybuyer]; ?></strong> </td>
   </tr>
   <tr>
      <td colspan="2" align="center"><table width="60%"  border="0" cellspacing="2" cellpadding="2" class="border">
            <? $getTiers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='endauction' AND category_id='".$category_id."'");
				while ($feeTiers = mysqli_fetch_array($getTiers)) { ?>
            <tr>
               <td width="100%"><? echo "$lang[from_price] ".displayAmount($feeTiers['fee_from'])." $lang[to] ".displayAmount($feeTiers['fee_to']);?></td>
               <td nowrap><?
		  	if ($feeTiers['calc_type']=="flat") echo displayAmount($feeTiers['fee_amount']);
		   	else echo $feeTiers['fee_amount']."%"; ?></td>
            </tr>
            <? } ?>
         
         </table></td>
   </tr>
   <? } ?>
   <? if ($setts['vat_rate']>0) { ?>
   <tr class="c5">
      </tr>
   <tr class="c3">
      <td align="center" colspan="2"><?=$lang[vat_rate_expl_1]." ".$setts['vat_rate']."% ".$lang[vat_rate_expl_2];?>
      </td>
   </tr>
   <? } ?>
   <tr class="c5">
      </tr>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>
