<!-- Put this script tag to the <head> of your page -->

<script type="text/javascript">

/* Modified Opera */
function bookmarksite(title,url){
if (window.sidebar) // firefox
	window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ // opera
	var elem = document.createElement('a');
	elem.setAttribute('href',url);
	elem.setAttribute('title',title);
	elem.setAttribute('rel','sidebar');
	elem.click();
}
else if(document.all)// ie
	window.external.AddFavorite(url, title);
}
</script>
<?
## v5.24 -> apr. 11, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }
### enter hit into stats table
if ($setts['maintenance_mode']==1&&$_SESSION['adminarea']!="Active") {
	include_once ("maintenance_splash_page.php");
	die();
}

function format_date($date, $mode, $delimiter = ' - ', $clock = 0) {
//2002-04-16 22:01:45
//0123456789012345678
 global $msg;
 global $month;
 global $month2;
 $year=substr($date, 0, 4);
 $month_val=substr($date, 5, 2);
 $month_num=intval($month_val);
 $day=substr($date, 8, 2);
 $hour=substr($date, 11, 2);
 $min=substr($date, 14, 2);
 $sec=substr($date, 17, 2);
 //10.03.2002 - 02:10
 if ($mode==1) {$output=$day.".".$month_val.".".$year." - ".$hour.":".$min;}
 //10.03.2002
 if ($mode==2) {$output=$day.".".$month_val.".".$year;}
 //12:15
 if ($mode==3) {$output=$hour.":".$min;}
 //20. october 2004
 if ($mode==4) {$output="$day. $month[$month_num] $year";}
 //octjabrja 8, 2004
 if ($mode==5) {$output="$month2[$month_num] $day, $year";}
 if ($mode==7) {$output=$day.'.'.$month_val.'.'.$year; $clock = 1;} //alfa.fi
 if ($mode==6) {
	# from dd.mm.yyyy - hh:mm
	# to yyyy-mm-dd hh:mm
	$d_length  = strlen($delimiter);
	$pos = strpos($date, $delimiter);
	if ($pos > 0) {
		$aeg_ee  = substr($date, 0, $pos);
		$time_ee = trim(substr($date, $pos + $d_length));
	} else {
		$aeg_ee  = $date;
	}
	$format = 'dd.mm.yyyy';
	$month  = substr($aeg_ee, strpos($format, 'mm'), 2);
	$day    = substr($aeg_ee, strpos($format, 'dd'), 2);
	if(strstr($format, 'yyyy')) {
		$year = substr($aeg_ee, strpos($format, 'yyyy'), 4);
	} else {
		$year = '20'.substr($aeg_ee, strpos($format, 'yy'), 2);
	}
	$output = $year.'-'.$month.'-'.$day.($time_ee ? " ".$time_ee : "");
 }
 if ($clock) $output.=" ".$msg[1000][35];
 return $output;
}


function getmicrotime()
{
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}

$time_start = getmicrotime();
$currentTime = time();
### Site Stats currently disabled
/* $refererSite=getenv("HTTP_REFERER");
$enterhit = mysql_query("INSERT INTO probid_stats (userip,userbrowser,clickdate,referralsite)
VALUES ('".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."','".$currentTime."','".$refererSite."')") or die(mysql_error());
*/
### session language settings





/*


if(!$_SESSION['sess_lang']) {
	include_once ("config/lang/".$setts['default_lang']."/site.lang");
	$_SESSION['sess_lang']="".$setts['default_lang']."";
	include_once ("config/lang/".$setts['default_lang']."/category.lang");
}else{
	include_once ("config/lang/".$_SESSION['sess_lang']."/site.lang");
	include_once ("config/lang/".$_SESSION['sess_lang']."/category.lang");
}
*/


## modification regarding the SSL - only the registration and login page will be in SSL.
//$path = $setts['siteurl'];
$path_ssl = ($setts['is_ssl']==1) ? $setts['ssl_address'] : $setts['siteurl'];

## show sell button
if ($setts['private_site']=="Y") $showSell = $_SESSION['is_seller'];
else $showSell="Y";

if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {
	$btn2_link="sellitem.php";
	$btn3_caption=$lang[Cap_members];
	$btn3_link="membersarea.php";
	$btn4_caption=$lang[Cap_logout];
	$btn4_link="index.php?option=logout";
} else {
	$btn2_link="login.php?redirect=sell";
	$btn3_caption=$lang['Cap_register'];
	$btn3_link="register.php";
	$btn4_caption=$lang['Cap_login'];
	$btn4_link="login.php";
}
include ("themes/".$setts['default_theme']."/title.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<!--[if gte IE 9]>
  <style type="text/css">
    .searchbar {
       filter: none;
    }
  </style>
<![endif]-->

<title>
<?


if (@eregi("auctiondetails.php",$_SERVER['PHP_SELF'])) echo getSqlField("SELECT itemname FROM probid_auctions WHERE id='".$_GET['id']."'","itemname")." - ";
if (@eregi("wanted.details.php",$_SERVER['PHP_SELF'])) echo getSqlField("SELECT itemname FROM probid_wanted_ads WHERE id='".$_GET['id']."'","itemname")." - ";
if (@eregi("displaynews.php",$_SERVER['PHP_SELF']) && @eregi("id=",$QUERY_STRING)) echo $lang['hotnews']." - ".getSqlField("SELECT title FROM probid_news WHERE id='".$_GET['id']."'","title")." - ";
if (@eregi("displaynews.php",$_SERVER['PHP_SELF']) && @eregi("option=all",$QUERY_STRING)) echo $lang[newsarchive1]." - ";
if (@eregi("stores.php",$_SERVER['PHP_SELF'])) echo $lang['SEO_shops']." - ";
if (@eregi("shop.php",$_SERVER['PHP_SELF'])) echo getSqlField("SELECT store_name FROM probid_users WHERE id='".$_REQUEST['userid']."'",store_name)." - ";
if (@eregi("viewfeedback.php",$_SERVER['PHP_SELF'])) echo $lang[recent_ratings]." ".getSqlField("SELECT username FROM probid_users WHERE id='".$_REQUEST['owner']."'",username)." - ";
if (@eregi("wanted.categories.php",$_SERVER['PHP_SELF'])) echo $lang[open_wanted]." - ";
if (@eregi("help.php",$_SERVER['PHP_SELF'])) echo $lang[help]." - ".(getSqlField("SELECT title FROM probid_help_themes WHERE id=".intval($_GET['id'])."","title")!='n/a'?getSqlField("SELECT title FROM probid_help_themes WHERE id=".intval($_GET['id'])."","title"):'')." ";
if (@eregi("sitefees.php",$_SERVER['PHP_SELF'])) echo $lang[Site_fees]." - ";
if (@eregi("register.php",$_SERVER['PHP_SELF'])) echo $lang[registerforacc]." - ";
if (@eregi("team.php",$_SERVER['PHP_SELF'])) echo $lang[team]." - ";
if (@eregi("terms.php",$_SERVER['PHP_SELF'])) echo $lang[terms_cond]." - ";
if (@eregi("pp.php",$_SERVER['PHP_SELF'])) echo $lang[Privacy_Policy]." - ";
if (@eregi("faq.php",$_SERVER['PHP_SELF'])) echo $lang[faq]." - ";
if (@eregi("membersarea.php",$_SERVER['PHP_SELF'])) echo $lang[memarea_title]." - ";
if (@eregi("membersarea.php",$_SERVER['PHP_SELF']) && @eregi("page=money",$QUERY_STRING)) echo $lang['e_account']." - ";
if (@eregi("membersarea.php",$_SERVER['PHP_SELF']) && @eregi("page=withdrawmoney",$QUERY_STRING)) echo $lang[withdrawmoney]." - ";
if (@eregi("w-contactinfo.php",$_SERVER['PHP_SELF'])) echo $lang[sellingtotals]." - ";
if (@eregi("abusereport.php",$_SERVER['PHP_SELF'])) echo $lang[abuse]." - ";
if (@eregi("editauction.php",$_SERVER['PHP_SELF'])) echo $lang[modifyauc]." - ";
if (@eregi("sellitem.php",$_SERVER['PHP_SELF'])) echo $lang[selling]." - ";
if (@eregi("search.php",$_SERVER['PHP_SELF'])) echo $lang[search]." - ";
if (@eregi("otheritems.php",$_SERVER['PHP_SELF'])) echo $lang[otherselleritems]." - ";
if (@eregi("buynow.php",$_SERVER['PHP_SELF'])) echo $lang[buynow]." - ";
if (@eregi("auctionfriend.php",$_SERVER['PHP_SELF'])) echo $lang[sendtofriend]." - ";
if (@eregi("itemwatch.php",$_SERVER['PHP_SELF'])) echo $lang[watchthisitem]." - ";
if (@eregi("auctionwatch.php",$_SERVER['PHP_SELF'])) echo $lang[auctionwatch]." - ";
if (@eregi("leavefeedback.php",$_SERVER['PHP_SELF'])) echo $lang[feedback_leave]." - ";
if (@eregi("mailprefs.php",$_SERVER['PHP_SELF'])) echo $lang[mailprefs]." - ";
if (@eregi("lostpass.php",$_SERVER['PHP_SELF'])) echo $lang[retrievepass]." - ";
if (@eregi("404.php",$_SERVER['PHP_SELF'])) echo $lang[searchnoresilts]." - ";



else if (@eregi("categories.php",$_SERVER['PHP_SELF'])) {
	//$cat_title = getSqlField("SELECT parent FROM probid_categories WHERE id='".$_REQUEST['parent']."'","name");

	$cat_title = $c_lang[$_GET["parent"]];

	if ($cat_title!=""&&$cat_title!="n/a") echo $cat_title." - ";


}



if ($domain_lang=="sbe") $domain_lang="est";

if (@eregi("index.php",$_SERVER['PHP_SELF'])) echo $setts['title_'.$domain_lang]." - ";

echo $setts['sitename']." ";

?>
</title>
<?



   $filename = str_replace("/","",$_SERVER["SCRIPT_URL"]);
   // Description
   $meta_author = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='author'","words");
   $meta_tag.= '<meta name="author" content="'.$meta_author.'">';
   $meta_tag.="\n";

       // More functional pages
       // Added by Roman Potter
    $metaPages = array ("auctiondetails.php","custompage.php","help.php","displaynews.php","categories.php","shop.php","stores.php","index.php");
	if ($filename=='') $filename="index.php";
    if (in_array($filename,$metaPages))
    {
      if ($filename=="stores.php") {

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='stores.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='stores.php'" ,"words");


      } elseif ($filename=="displaynews.php") {

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='displaynews.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='displaynews.php'" ,"words");


          if (intval($_GET["id"])>0)
          {
              $meta_desc.=", ".getSqlField("SELECT description FROM probid_news WHERE id=".$_GET["id"]."","description");
              $meta_key.=", ".getSqlField("SELECT words FROM probid_news WHERE id=".$_GET["id"]."","words");
          }

   	   } elseif ($filename=="index.php") {

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='index.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='index.php'" ,"words");


          if (intval($_GET["id"])>0)
          {
              $meta_desc.=", ".getSqlField("SELECT description FROM probid_news WHERE id=".$_GET["id"]."","description");
              $meta_key.=", ".getSqlField("SELECT words FROM probid_news WHERE id=".$_GET["id"]."","words");
          }

      } elseif ($filename=="help.php") {

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='help.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='help.php'" ,"words");


          if (intval($_GET["id"])>0)
          {
              $meta_desc.=", ".getSqlField("SELECT description FROM probid_help_themes WHERE id=".$_GET["id"]."","description");
              $meta_key.=", ".getSqlField("SELECT words FROM probid_help_themes WHERE id=".$_GET["id"]."","words");
          }
       } elseif ($filename=="shop.php") {

           $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='custompage.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='custompage.php'" ,"words");

           if (intval($_GET["userid"])>0)
          {
              $meta_desc.=", ".substr(strip_tags(addspecialchars(getSqlField("SELECT aboutmepage  FROM  probid_users  WHERE id=".$_GET["userid"]."","aboutmepage"))),0,200);
              $meta_key.=", ".getSqlField("SELECT store_name FROM probid_users  WHERE id=".$_GET["userid"]."","store_name");
          }

      } elseif ($filename=="custompage.php") {

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='custompage.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='custompage.php'" ,"words");


          if (intval($_GET["id"])>0)
          {
              $meta_desc.=", ".getSqlField("SELECT description FROM  probid_pages_additional  WHERE id=".$_GET["id"]."","description");
              $meta_key.=", ".getSqlField("SELECT words FROM probid_pages_additional  WHERE id=".$_GET["id"]."","words");
          }
      } elseif ($filename=="auctiondetails.php") {

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='auctiondetails.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='auctiondetails.php'" ,"words");


          if (intval($_GET["id"])>0)
          {

               $auc_cat = getSqlField("SELECT category FROM  probid_auctions  WHERE id=".$_GET["id"]."","category");
               $auc_parent = getSqlField("SELECT  parent FROM  probid_categories  WHERE id=".intval($auc_cat)."","parent");
                $res = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_categories WHERE parent=".intval($auc_parent)."");
                while ($row= mysqli_fetch_array($res,  MYSQLI_ASSOC)) {
                  $arrCatId[] = $row;
                }
				if ((is_array($arrCatId)) && (sizeof($arrCatId)>0)) {
					foreach ($arrCatId as $cid) {
					  $subcats.=", ".$c_lang[$cid["id"]];
					}
				  // $meta_key.=$subcats;
				   $meta_key.=", ".$c_lang[$auc_cat];
				   $meta_key.=", ".$c_lang[$auc_parent];
				}



              $meta_desc.=", ".substr(strip_tags(addspecialchars(getSqlField("SELECT description FROM  probid_auctions  WHERE id=".$_GET["id"]."","description"))),0,200);
              $itemname = getSqlField("SELECT itemname FROM  probid_auctions  WHERE id=".$_GET["id"]."","itemname");
              $meta_desc.=", ".$itemname;
              $arrKeys = explode(" ",$itemname);
              foreach($arrKeys as $key) {

                  if (strlen($key)>4)
                  {
                    $meta_key.=", ".str_replace(".","",$key);
                  }
              }

              $meta_desc.=", ".getSqlField("SELECT country FROM  probid_auctions  WHERE id=".$_GET["id"]."","country");
              // $meta_key.=", ".getSqlField("SELECT meta_keys FROM probid_auctions  WHERE id=".$_GET["id"]."","meta_keys");

              $meta_key =  strtolower(str_replace(", ",",",$meta_key));
              $keys = explode(",",$meta_key);
              $keys_uniq = array_unique($keys);
              $meta_key = implode(",",$keys_uniq);


          }
       }    elseif ($filename=="categories.php") {

          $getCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,name,items_counter, hover_title FROM probid_categories WHERE parent=0 AND hidden=0 AND userid=0 ORDER BY theorder,name");
          while ($row= mysqli_fetch_array($getCats,  MYSQLI_ASSOC)) {
            $arrCategories[] = $row;
          }
          foreach ($arrCategories as $cat)
          {
              $cats_main.=",".$c_lang[$cat['id']];
          }

          $meta_desc = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='description' AND filename='categories.php'" ,"words");
          $meta_key = getSqlField("SELECT words FROM probid_search_meta WHERE lang='".$domain_lang."' AND name='keywords' AND filename='categories.php'" ,"words");
          if (intval($_GET["parent"])>0)
          {

                $sub = ",".$c_lang[$_GET["parent"]];
                $res = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_categories WHERE parent=".$_GET["parent"]."");
                while ($row= mysqli_fetch_array($res,  MYSQLI_ASSOC)) {
                  $arrCatId[] = $row;
                }
                if (is_array($arrCatId) && sizeof($arrCatId)>0) {
                foreach ($arrCatId as $cid) {
                  $subcats.=",".$c_lang[$cid["id"]];
                }
                }
          }
          }
          $cats_main="";

          if (($filename=="auctiondetails.php") && (intval($_GET["id"])>0))
          {
              $meta_key = $meta_key;
          } else {
              $meta_key.=$cats_main.$sub.$subcats;
          }


          $meta_desc.=$sub;




         $meta_tag.= '<meta name="description" content="'.$meta_desc.'">';
         $meta_tag.="\n";
         $meta_tag.= '<meta name="keywords" content="'.$meta_key.'">';
         $meta_tag.="\n";

   } else {

      $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_search_meta WHERE lang='".$domain_lang."' AND filename='".$filename."'");
      while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
        $arrTags[] = $row;
      }

      if (is_array($arrTags) && sizeof($arrTags))
      {
        foreach ($arrTags as $tag)
        {
            $meta_tag.='<meta name="'.$tag["name"].'" content="'.$tag["words"].'">';
            $meta_tag.="\n";

        }
      }

   }



   echo $meta_tag;



/*
if (@eregi("categories.php",$_SERVER['PHP_SELF'])) {
	$meta_cat = getMainCat($_REQUEST['parent']);
	$meta_details = getSqlRow("SELECT meta_description, meta_keywords FROM probid_categories WHERE id='".$meta_cat."'");
}
*/
?>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lang[codepage];?>">
<?
/*
if ($meta_details['meta_description']!=""||$meta_details['meta_keywords']!="") {
	echo "<meta name=\"description\" content=\"".$meta_details['meta_description']."\"> \n";
	echo "<meta name=\"keywords\" content=\"".$meta_details['meta_keywords']."\"> \n";
} else {
	echo addSpecialChars(getSqlField ("SELECT metatags FROM probid_gen_setts","metatags"));
}


if (@eregi("shop", $_SERVER['PHP_SELF'])) {
	$store_metatags = getSqlField("SELECT store_metatags FROM probid_users WHERE id='".$_REQUEST['userid']."'", "store_metatags");
	if (!empty($store_metatags)) echo "<meta name=\"keywords\" content=\"".$store_metatags."\"> \n";
}
*/
?>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<link href="themes/<?=$setts['default_theme'];?>/newengine.css" rel="stylesheet" type="text/css">

<!--[if IE]>
        <link  href="themes/<?=$setts['default_theme'];?>/newengine_ie.css" rel="stylesheet" type="text/css"/>
<![endif]-->
		


<style type="text/css">
<!--

a.ln:link {
	background-image:  url(themes/v52/img/sidem.gif);
}
a.ln:visited {
	background-image:  url(themes/v52/img/sidem.gif);
}
a.ln:hover {
	background-image:  url(themes/v52/img/sidemo.gif);
}
-->
</style>
<script language="javascript" src="themes/<?=$setts['default_theme'];?>/main.js" type="text/javascript"></script>
<script language=JavaScript src='scripts/innovaeditor.js'></script>
<script type="text/javascript">
<? $diff=getSqlField("SELECT * FROM probid_timesettings WHERE active='selected'","value"); ?>
var currenttime = '<? print date("F d, Y H:i", time()+$diff*3600)?>';
var serverdate=new Date(currenttime);

function padlength(what){
	var output=(what.toString().length==1)? "0"+what : what;
	return output;
}

function displaytime(){
	serverdate.setSeconds(serverdate.getSeconds()+1)
	var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes());
	document.getElementById("servertime").innerHTML=timestring;
}

window.onload=function(){
	setInterval("displaytime()", 1000);
}

</script>
</head>
<?

?>
<form name="hiddenLang" method="post" action="<?=$_SERVER['REQUEST_URI']?>">
<input type="hidden" value="" id="req_lng" name="lang">
</form>
<body bgcolor="#ffffff" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<table width="982" border="0" align="center" cellpadding="0" cellspacing="2" style="border-bottom: 1px solid #999999;">
<tr>
    <td width="100%">
    	<table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#ffffff" style="height:84px">
         <tr heigth="70">
            <td><a href="<?=VAR_HOST?>"><img src="images/probidlogo.gif?<?=rand(2,9999);?>" alt="Sell, Buy, Enjoy - SBE.EE" border=0></a></td>
            <td width=450 align="left" valign="middle"><font color="#0C6CBB" size="+1" face="Georgia">&nbsp;</font></td>
            <td width="100%" align="right"><?
					if ($setts['is_ssl']==1&&(@eregi("register.php",$_SERVER['PHP_SELF'])||@eregi("login.php",$_SERVER['PHP_SELF']))) {
						## do nothing, but dont display the banners.
					} else {
						## banner ads management
						$nbAds = 0;
						if (@eregi("categories.php",$_SERVER['PHP_SELF'])) {
							$catName = getSqlField("SELECT name FROM probid_categories WHERE id='".$_REQUEST['parent']."'","name");
							$getAdvert = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE
							cat_filter LIKE '%".$catName."%' AND (views_p=0 OR views_p>=views) AND (clicks_p=0 OR clicks_p>=clicks) ORDER BY RAND() LIMIT 0,1");
							$nbAds = mysqli_num_rows($getAdvert);
						}
						if (@eregi("auctiondetails.php",$_SERVER['PHP_SELF'])) {
							$advertId=0;
							$allAdverts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE
							(views_p=0 OR views_p>=views) AND (clicks_p=0 OR clicks_p>=clicks) AND keyword_filter!='' ORDER BY RAND()");
							while ($advertsQuery = mysqli_fetch_array($allAdverts)) {
								$isAdvert = getSqlNumber("SELECT id FROM probid_auctions WHERE keywords LIKE '%".$advertsQuery['keyword_filter']."%' AND id='".$_GET['id']."'");
								if ($isAdvert>0) $advertId = $advertsQuery['id'];
							}
							$getAdvert = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE id='".$advertId."'");
							$nbAds = mysqli_num_rows($getAdvert);
						}
						// if we are not on one of the pages, or there are no ads with filter, display an ad with no filter
						if ($nbAds==0) {
							$getAdvert=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE
							cat_filter='' AND keyword_filter='' AND (views_p=0 OR views_p>=views) AND (clicks_p=0 OR clicks_p>=clicks)  ORDER BY RAND() LIMIT 0,1");
						}
						while ($bannerDetails = mysqli_fetch_array($getAdvert)) {
							$addView = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_adverts SET views=views+1 WHERE id='".$bannerDetails['id']."'");
							if ($bannerDetails['type']=="custom") {
								echo "<a href=\"click.php?refid=".$bannerDetails['id']."\" target=\"new\"><img src=\"".$bannerDetails['imgpath']."\" alt=\"".$bannerDetails['alttext']."\" border=\"0\"><br>".$bannerDetails['textunder']."</a>";
								// echo "<a href=\"".$bannerDetails['url']."\"><img src=\"".$bannerDetails['imgpath']."\" alt=\"".$bannerDetails['alttext']."\" border=\"0\"><br>".$bannerDetails['textunder']."</a>";
							} else 	if ($bannerDetails['type']=="code") {
								echo addSpecialChars($bannerDetails['banner_code']);
							}
						}
						((mysqli_free_result($getAdvert) || (is_object($getAdvert) && (get_class($getAdvert) == "mysqli_result"))) ? true : false);
					} ?>
            </td>
         </tr>
      </table>

      <? ## calculate width
		$headerCells = 6;
		if ($showSell == "Y") $headerCells ++;
		if ($setts['enable_wantedads'] == "Y") $headerCells ++;
		if ($setts['stores_enabled'] == "Y") $headerCells++;
		if ($layout['is_about'] == "Y") $headerCells ++;
		if ($layout['is_contact'] == "Y") $headerCells ++;
		$headBtnWidth = 100/$headerCells."%";
		$path="";
		?>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr height="31">

        <? if (@eregi("index.php",$_SERVER['PHP_SELF'])) { ?>

	<td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>index.php">&nbsp;
               <?=$lang[Cap_home];?>
               </a>&nbsp;</td>

	<? } else {?>

	<td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>index.php">&nbsp;
               <?=$lang[Cap_home];?>
               </a>&nbsp;</td>

	<? }
/*
	if (@eregi("displaynews.php",$_SERVER['PHP_SELF'])) { ?>

            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="displaynews.php?option=all">&nbsp;
               <?=$lang[hotnews];?>
               </a>&nbsp;</td>

	<? } else { ?>

            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="displaynews.php?option=all">&nbsp;
               <?=$lang[hotnews];?>
               </a>&nbsp;</td>

	<? }*/

	if ($showSell=="Y") {
	             if (@eregi("sellitem.php",$_SERVER['PHP_SELF'])) { ?>

	<td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn2_link;?>">&nbsp;
               <?=$lang[Cap_sell];?>
               </a>&nbsp;</td>

    <? } else { ?>

	<td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn2_link;?>">&nbsp;
               <?=$lang[Cap_sell];?>
               </a>&nbsp;</td>

	            <? } }


    if ($setts['stores_enabled']=="Y") {
    	if (@eregi("stores.php",$_SERVER['PHP_SELF'])) { ?>

            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>stores.php">&nbsp;
               <?=$lang[Cap_stores];?>
               </a>&nbsp;</td>

	<? } else { ?>

            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>stores.php">&nbsp;
               <?=$lang[Cap_stores];?>
               </a>&nbsp;</td>

	<? } }

    if ($setts['enable_wantedads']=="Y") {
		if (@eregi("wanted.categories.php",$_SERVER['PHP_SELF'])) { ?>

            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>wanted.categories.php">&nbsp;
               <?=$lang[Cap_wantedads];?>
               </a>&nbsp;</td>

	<? } else { ?>

            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>wanted.categories.php">&nbsp;
               <?=$lang[Cap_wantedads];?>
               </a>&nbsp;</td>

    <? } }

    if (@eregi("help.php",$_SERVER['PHP_SELF'])) { ?>

            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>help.php">&nbsp;
               <?=$lang[Cap_help];?>
               </a>&nbsp;</td>

	<? } else { ?>

            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>help.php">&nbsp;
               <?=$lang[Cap_help];?>
               </a>&nbsp;</td>

	<? }

	if (@eregi("sitefees.php",$_SERVER['PHP_SELF'])) { ?>

            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>sitefees.php">&nbsp;
               <?=$lang[Cap_fees];?>
               </a>&nbsp;</td>

	<? } else { ?>

            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>sitefees.php">&nbsp;
               <?=$lang[Cap_fees];?>
               </a>&nbsp;</td>

	<? }

	if (@eregi("team.php",$_SERVER['PHP_SELF'])) { ?>

            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>team.php">&nbsp;
               <?=$lang[team];?>
               </a>&nbsp;</td>

	<? } else { ?>

            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>team.php">&nbsp;
               <?=$lang[team];?>
               </a>&nbsp;</td>

	<? } ?>

    <?
    ## About us & Contact page remove - ALEKSEI

    /*
    if ($layout['is_about']=="Y") {
		if (@eregi("aboutus.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>aboutus.php">&nbsp;
               <?=$lang[Cap_about];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>aboutus.php">&nbsp;
               <?=$lang[Cap_about];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } }

    if ($layout['is_contact']=="Y") {
		if (@eregi("contactus.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>contactus.php">&nbsp;
               <?=$lang[Cap_contact];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_s_rev.gif" width="6" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>contactus.php">&nbsp;
               <?=$lang[Cap_contact];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_s_rev.gif" width="6" height="31"></td>
	<? } } ?>*/

	## End of remove
	?>

         </tr>
      </table>

      <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="2"></div>
      
      <div class="searchbar_wrapper">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="searchbar">
         <tr>
            <td><table width="100%" height="29" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                     <td width="194" nowrap align="center" class="search">&nbsp;&nbsp;&nbsp;
                     	<?php
                        echo date("d.m.Y");
                        ?>
                     	<div id="servertime">
                        
                        </div>
                      </td>
			<form action="auctionsearch.php" method="post">
                        <td align="center" width="100%" class="search" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;
                        <INPUT size=25 name="basicsearch" class="search16">
                           &nbsp;
                        <button id="main_search_button" name="searchok" type="submit" value="searchok"><?=$lang[search]?></button>
                           &nbsp;&nbsp;&nbsp;</td>
                     </form>
                     <? /*<td class="search" nowrap>&nbsp;&nbsp;&nbsp;
                        <?=$lang[browse];?>
                        &nbsp;&nbsp;</td>
                     <form name="catbrowse" method="post" action="<?=VAR_HOST;?>categories.php">
                        <input type="hidden" name="show" value="subcats">
                        <? $getCategories = mysql_query("SELECT id,name FROM probid_categories WHERE parent=0 AND hidden=0 AND userid=0 ORDER BY theorder,name");?>
                        <td width="100%" class="search"><select name="parent" onChange="javascript:catbrowse.submit()">
                              <option value="" selected>
                              <?=$lang[choosecat];?>
                              </option>
                              <? while($category=mysql_fetch_array($getCategories)) { ?>
                              <option value="<?=$category['id'];?>" <? echo (($category['id']==$_REQUEST['parent'])?"selected":"");?>>
                              <?=$c_lang[$category['id']];?>
                              </option>
                              <? } ?>
                              <option value="">------------------------</option>
                              <option value="0">
                              <?=$lang[allcats];?>
                              </option>
                           </select>
                        </td>
                     </form> */ ?>

                    <?/*
                     if ($setts['is_ssl']==1) {
                     	if (@eregi("register.php",$_SERVER['PHP_SELF'])) {
                     		 echo "<td>&nbsp;</td>"; }
                     		else {
                     			if (@eregi("login.php",$_SERVER['PHP_SELF'])) {
                     			echo "<td>&nbsp;</td>"; }
                     			}*/
                     if ($setts['is_ssl']==1&&(@eregi("register.php",$_SERVER['PHP_SELF'])||@eregi("login.php",$_SERVER['PHP_SELF']))) {

                     ## if SSL enabled, don't display language flags - ALEKSEI

                     	echo "<td>&nbsp;</td>";
                     }
                     else {
                     ?>
                     <td width="100%" nowrap class="language_td"><!-- ADD THE FOLLOWING CODE WHEREEVER YOU WANT YOUR LANGUAGE OPTIONS TO SHOW-->
                     <div class="language_div" align="center"> &nbsp;&nbsp;
                           <?

									if ($setts['user_lang']==1) {
										include ("config/lang/list2.php");
										#$langlist = explode(" ", $langlist);
										#flags
										$langlist = array("a","b", "english","latvian");
										$sizeofarray = count($langlist) ;
                     $varHost[1] = "http://rus.$domain_top";
                     $varHost[0] = "http://eng.$domain_top";
                     $varHost[2] = "http://$domain_top";
                     $varHost[3] = "http://lat.$domain_top";
                     $redirect = "";
                     if ($_SERVER["REQUEST_URI"]!='') $redirect = $_SERVER["REQUEST_URI"];



                     if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {
                          $varHost[0] = "#";
                          $click[0] = 'onclick="slang(\'eng\',\''.VAR_HOST.$_SERVER['REQUEST_URI'].'\')"';
                          $varHost[1] = "#";
                          $click[1] = 'onclick="slang(\'rus\',\''.VAR_HOST.$_SERVER['REQUEST_URI'].'\')"';
                          $varHost[2] = "#";
                          $click[2] = 'onclick="slang(\'est\',\''.VAR_HOST.$_SERVER['REQUEST_URI'].'\')"';
                          $varHost[3] = "#";
                          $click[3] = 'onclick="slang(\'lat\',\''.VAR_HOST.$_SERVER['REQUEST_URI'].'\')"';
                          $redirect="";

                     }


										for ($z=0; $z < $sizeofarray; $z++) {
											echo '<a '.$click[$z].' href="'.$varHost[$z].$redirect.'">';
											echo "<img src=\"themes/".$setts['default_theme']."/img/".$langlist[$z].".gif\" border=\"0\" alt=\"$langlist[$z]\"></a>&nbsp;&nbsp;";
										}
									} ?>
                        </div>
                        <!-- END ADDITION -->
                     </td>
                    <? } ?>
                  </tr>
               </table></td>
         </tr>
      </table>
      </div>
      <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="2"></div>
      <table width="100%" border="0" cellspacing="1" cellpadding="5" bgcolor="#ffffff">
      <tr valign="top">
         <td id="leftcolumn" style="padding-left:0px;" bgcolor="#FFFFFF" >
				<script language="javascript">
					var ie4 = false;
					if(document.all) { ie4 = true; }

					function getObject(id) { if (ie4) { return document.all[id]; } else { return document.getElementById(id); } }
					function toggle(link, divId) {
						var lText = link.innerHTML;
						var d = getObject(divId);
						if (lText == '+') { link.innerHTML = '&#8211;'; d.style.display = 'block'; }
						else { link.innerHTML = '+'; d.style.display = 'none'; }
					}
				</script>
            <?
				if ($_SESSION['membersarea']=="Active") {
					$getAn=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,date,title FROM probid_announcements WHERE active=1 AND lang='".$_SESSION['sess_lang']."' ORDER BY date DESC");
					$nbAn=mysqli_num_rows($getAn);
					if ($nbAn>0) {
						header6("$lang[member_announcements2] [<a title=\"show/hide\" class=\"hidelayer\" id=\"exp1102170555_link\" href=\"javascript: void(0);\" onclick=\"toggle(this, 'exp1102170555');\">&#8211;</a>]"); ?>
            <div id="exp1102170555">
               <table width="100%" border="0" cellspacing="0" cellpadding="3">
  <!--                <tr>
                     <td class="c4"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                     <td class="c3"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  </tr>-->
                  <? while ($ans=mysqli_fetch_array($getAn)) { ?>
                  <tr>
                     <td class="c3"><img src="themes/<?=$setts['default_theme'];?>/img/arrow.gif" width="8" height="8" hspace="4"></td>
                     <td width="100%" class="c2 smallfont"><b><? echo displaydatetime($ans['date'],substr($setts['date_format'],0,7));?></b></td>
                  </tr>
                  <tr class="contentfont">
                     <td class="c4"></td>
                     <td class="c3"><a href="<?=$path;?>viewannouncement.php?id=<?=$ans['id'];?>">
                        <?=$ans['title'];?>
                        </a> </td>
                  </tr>
                  <? } ?>
               </table>
            </div>
            <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
            <? } } ?>
            <div id="exp1102170142">
               <? if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { ?>
	            <? /*header5("$lang[memarea_title]");*/
			echo "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
		<tr height='21'>
		<td class='mainmenu'  align='center'><b>".$lang[memarea_title]."</b></td>
		</tr>
		</table>";?>
               <table border="0" cellpadding="1" cellspacing="1" width="100%" class="c4">
                  <tr>
                     <td align="center" height="35"><b style="font-size: 10px;">
                        <?=$lang[welcome];?>,
                        <br>
                        <? echo $_SESSION['membername']; ?></b> </td>
                  </tr>
                  <tr>
                     <td class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  </tr>
               </table>

               <table width="100%" cellpadding="1" cellspacing="1" border="0" class="c4" >
                     <tr class="c2">
                        <td nowrap>&nbsp;<b>
                           <?=$lang[feedback_rating];?></b>:</td>
                        <td width="100%"><? echo calcFeedback($_SESSION['memberid']);?></td>
                     </tr>
                     <tr class="c2">
                        <td colspan="2" align="center"><a href="viewfeedback.php?owner=<?=$_SESSION['memberid'];?>">
                           <?=$lang[view_fb];?>
                           </a></td>
                     </tr>
                     </table>

                     <table width="100%" cellpadding="1" cellspacing="1" border="0" class="c4" >
                     	<tr>
                     	<td class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  		</tr>
                  	</table>


               <? if ($showSell=="Y") {

               define("CALC_NOT_READED",1);
               include("m_actions.php");

               if ($not_readed_count>0)
               {
                  $box = "(<b>".$not_readed_count."</b>)";
               } else {
                  $box = "(".$not_readed_count.")";
               }

                ?>
               <table border="0" cellpadding="3" cellspacing="2" width="100%" class="c4">
                  <? if ($_SESSION['accsusp']!=2) { ?>
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=bidding"><?=$lang[bidding_img]?></a></td></tr>

                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=selling"><?=$lang[selling_img]?></a></td></tr>

                  <tr align="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=feedback"><?=$lang[feedback_img]?></a></td></tr>

                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="abusereport.php"><?=$lang[abuse]?></a></td></tr>

                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=wanted"><?=$lang[wanted_img]?></a></td></tr>
                     <? } ?>

                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=messages"><?=$lang[message1]?> <?=$box?></a></td></tr>
                     <? } ?>

                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=money"><?=$lang['e_account']?> </a></td></tr>
                     <? } ?>

                     <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=invite_friends"><?=$lang[invite_friends_menu]?></a></td></tr>


                  <?
                  	if ($_SERVER['REMOTE_ADDR']=='85.29.234.78')
                  	{
                  ?>
                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=projects">Projects <FONT COLOR = RED>New</FONT></a></td></tr>
                     <? } ?>
                  <? }

?>
                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['stores_enabled']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=store"><?=$lang[store_img]?></a></td></tr>
                     <? } ?>

                 <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['stores_enabled']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=block_users"><?=$lang[blockusers]?></a></td></tr>
                     <? } ?>
                        <? if (getenv("REMOTE_ADDR")=="80.66.246.104") { ?>
                        <tr valign="center" bgcolor="white" height="21">
                <td class="membutt" align="center">
                <a href="membersarea.php?page=invite_friends">Invite friends </a></td>
                </tr>
                  <? } ?>
                  <?/*<tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['stores_enabled']=="Y") { ?>
                     <td align="center" class="membutt"><a href="history.php"><?=$lang[acc_history]?></a></td></tr>
                     <? } ?>*/?>

                  <? } ?>
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=preferences"><?=$lang[details_img]?></a></td></tr>

                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="index.php?option=logout"><?=$lang[logout]?></a></td></tr>
                  </tr>
               </table>
               <? } else { ?>
               <table border="0" cellpadding="3" cellspacing="2" width=177 bgcolor="#e9e9eb">
                  <? if ($_SESSION['accsusp']!=2) { ?>
                  <tr align="center" bgcolor="white">
                     <td width="50%"><a class="membutt" href="membersarea.php?page=bidding"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_bidding.gif" width="40" height="40" border="0"><br>
                        <?=$lang[bidding_img]?>
                        </a></td>
                     <td width="50%"><a class="membutt" href="membersarea.php?page=feedback"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_feedback.gif" width="40" height="40" border="0"><br>
                        <?=$lang[feedback_img]?>
                        </a></td>
                  </tr>
                  <? } ?>
				  
                  <tr align="center" bgcolor="white">
                     <td><a class="membutt" href="membersarea.php?page=preferences"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_details.gif" width="40" height="40" border="0"><br>
                        <?=$lang[details_img]?>
                        </a></td>
                     <td><a class="membutt" href="index.php?option=logout"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_logout.gif" width="40" height="40" border="0"><br>
                        <?=$lang[logout]?>
                        </a></td>
                  </tr>
               </table>
               <? } ?>
               <? } else { ?>
               <? if ($_SESSION['membersarea']!="Active"&&$layout['d_login_box']==1&&$setts['is_ssl']!=1) { ?>
	            <? /*header5("$lang[memarea_title]");*/
			echo "<table width=177 border='0' cellpadding='0' cellspacing='0'>
		<tr height='21'>
		<!--<td class='mainmenu' width='100%' align='center'><b>".$lang[memarea_title]."</b></td>-->
		<td class='mainmenu' align='center'><b>".$lang[memarea_title]."</b></td>
		</tr>
		</table>";?>
               <table border="0" cellpadding="2" cellspacing="2">
               <tr>
                    <td>
                    	<div class="buttonsbox" >
                    		<a class="register" href="<?=$btn3_link?>"><?=$btn3_caption?></a>                   			
                    	</div>
				        <div class="buttonsbox" >  	
                		<a class="loginbutton" href="<?=$btn4_link?>"><?=$btn4_caption?></a>
                		</div>	
                    </td>
               </tr>
               <tr>
                  <td align="center">
                  <a style="text-decoration:none;color:#2e5b9a;font-weight:bold;font-size:9px;" href="<?=$path?>lostpass.php">
                  <?=$lang[forgotpass]?></a>
                  </td>
               </tr>
               </table>
               <? } else if ($_SESSION['membersarea']!="Active"&&$layout['d_login_box']==1&&$setts['is_ssl']==1) {
		  				echo "<p align=\"center\" class=\"contentfont\">[ <a href=\"".$path_ssl."login.php\"><strong>".$lang[login_secure]."</strong></a> ]</p>";
		  	 		}
		  		} 	?>
            </div>
            <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
            <noscript>
            JS not supported
            </noscript>
            <!-- flooble Expandable Content box end  -->
            <? /*header5("$lang[Cap_categories]");*/
		echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' >
		<tr height='21'>
		<td class='mainmenu_halfrounded' width='100%' align='center'><b>".$lang[Cap_categories]."</b></td>
		</tr>
		</table>";?>
        <!--Kategooriad-->
            <div id="exp1102170166">
               <table border="0" cellspacing="0" cellpadding="0" width="177"> <!--WIDTH-->
                  <?
						$getCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,name,items_counter, hover_title FROM probid_categories WHERE parent=0 AND hidden=0 AND userid=0 ORDER BY theorder,name");
						while ($catBrowse=mysqli_fetch_array($getCats)) {
							$nbSubCats = getSqlNumber("SELECT id FROM probid_categories WHERE parent='".$catBrowse['id']."'");

							if ($nbSubCats>0) $catLink=processLink('categories', array('category' => $c_lang[$catBrowse['id']], 'parent' => $catBrowse['id'], 'show' => 'subcats'));
							else $catLink=processLink('categories', array('category' => $c_lang[$catBrowse['id']], 'parent' => $catBrowse['id'])); ?>
                  <? if ($catBrowse['items_counter']>0) {
                  echo "<tr><td class=\"category_item\"><a class=\"ln\" href=\"".$catLink."\">".$c_lang[$catBrowse['id']];
                  echo "(<strong>".$catBrowse['items_counter']."</strong>)";
                  echo "</a></td></tr>";
                  } ?>
                  <? } ?>
               </table>
               <p>&nbsp;</p>

  <?/*
  <table width='100%' border='0' cellpadding='0' cellspacing='0' >
		<tr height='21'>
		<td class='mainmenu' width='100%' align='center'><b><?=$lang[reklaam]?></b></td>
		</tr>
		</table><br> */?>

   <table width="100%" border="0" cellspacing="0" cellpadding="5">
   	<tr><td width="100%" align=center>
      &nbsp;
    </td></tr>
   </table> <br>
            </div>
            <noscript>
            JS not supported
            </noscript>
            <div><!--<img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="180" height="1"></div>!--></td>
         <td width="100%">