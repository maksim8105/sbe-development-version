<?
## v5.25b -> jul. 11, 2006
session_start();
include_once ("config/config.php");



if ($setts['cron_job']==2) {
	$manualCron = TRUE;
	include_once ("cron/cron1.php");
}

include ("themes/".$setts['default_theme']."/header.php");

if ($_SESSION['counted'][$_GET['id']]!="cntd") {
	$addClick = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET clicks=clicks+1 WHERE id='".$_GET['id']."'");
	$_SESSION['counted'][$_GET['id']]="cntd";
}
	
if ($_SESSION['adminarea']!="Active") $addPattern = " AND active=1";
else $addPattern = "";

$getAuction = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions WHERE id='".$_GET['id']."' ".$addPattern);
$auctionDetails = mysqli_fetch_array($getAuction);
$isAuction = mysqli_num_rows($getAuction);
if ($isAuction > 0) {
	$sellerDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$auctionDetails['ownerid']."'"); 

	## we check if the user that visits is banned
	$isBanned = getSqlNumber("SELECT id FROM probid_blocked_users WHERE userid='".$_SESSION['memberid']."' AND ownerid='".$auctionDetails['ownerid']."'");

	include_once("formchecker.php");

	### add the messaging function
	if ($action=="submit_question" && !$isBanned) {
		if ($_GET['question_type']=="private") {			
			$ownerId = $auctionDetails['ownerid'];
			$senderId = $_SESSION['memberid'];
			$auctionId = $_GET['id'];
			$message = $_GET['message'];
			include ("mails/askquestion.php");
			$msgSys_msg = "<p align=center><strong>$lang[askquestionokmsg]</strong></p>";
		} else if ($_GET['question_type']=="public") {
			if (trim($_GET['message'])!="") {
				$currentTime = time();
				$message = remSpecialChars(trim($_GET['message']));
				## we check if an exact message hasnt been posted yet
				$ismsg = getSqlRow("SELECT id FROM probid_public_msg WHERE auctionid='".$_GET['id']."' AND 
				ownerid='".$auctionDetails['ownerid']."' AND posterid='".$_SESSION['memberid']."' AND 
				msgtype='Q' AND content='".$message."'");
				
				if (!$ismsg&&$_SESSION['memberid']>0) {
					$addMsg = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_public_msg 
					(auctionid, ownerid, posterid, msgtype, content, regdate) VALUES
					('".$_GET['id']."', '".$auctionDetails['ownerid']."', '".$_SESSION['memberid']."', 
					'Q', '".$message."', '".$currentTime."')"); 
					$answerid = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
					$prepareSellerMsg = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_public_msg 
					(auctionid, ownerid, posterid, msgtype, content, regdate, answerid) VALUES
					('".$_GET['id']."', '".$auctionDetails['ownerid']."', '".$auctionDetails['ownerid']."', 
					'A', '', '', '".$answerid."')"); 
					$msgSys_msg = "<p align=center><strong>$lang[askquestionokmsg2]</strong></p>";
					$sellerId = $auctionDetails['ownerid'];
					$auctionId = $auctionDetails['id'];
					include ("mails/notifysellerquestionposted.php");
				}
			} else {
				$msgSys_msg = "<p align=center><strong>$lang[askquestionokmsg3]</strong></p>";
			}
		}
		/*echo "<script>document.location.href='auctiondetails.php?id=".$_GET['id']."'</script>";*/
	}
	
	if ($action=="submit_answer") {
		$currentTime = time();
		$message = remSpecialChars(trim($_GET['content']));
		$addSellerMsg = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_public_msg SET
		content='".$message."', regdate='".$currentTime."' WHERE 
		answerid='".$_GET['answerid']."' AND ownerid='".$_SESSION['memberid']."' 
		AND posterid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

		$questionUserId = getSqlField("SELECT posterid FROM probid_public_msg WHERE id='".$_GET['answerid']."'","posterid");
		$auctionId = $auctionDetails['id'];
		include ("mails/notifybuyeranswerposted.php");

		$msgSys_msg = "<p align=center><strong>$lang[askquestionokmsg4]</strong></p>";
		echo "<script>document.location.href='auctiondetails.php?id=".$_GET['id']."&msgsys=".$msgSys_msg."'</script>";
	}
	### end of messaging function
	
	$binAuction=FALSE;
	if ($auctionDetails['bidstart']==$auctionDetails['bnvalue']&&$auctionDetails['bn']=="Y") $binAuction=TRUE;	
	### We decide if the auction is about to start, if it didnt, then only the owner can see it.
	$notStarted=FALSE;
	if (strtotime($auctionDetails['startdate'])>time()) $notStarted=TRUE;
	if ($auctionDetails['ownerid']==$_SESSION['memberid']) $notStarted=FALSE;
	if ($notStarted==TRUE) {
		headerdetails($lang[auctdetserror]);
		echo "<p class=contentfont align=center>$lang[auctiondidntstart]</p>";
	} else { ?>
<SCRIPT LANGUAGE="JavaScript"><!--
myPopup = '';

function openPopup(url) {
	myPopup = window.open(url,'popupWindow','width=640,height=150,status=yes');
   if (!myPopup.opener) myPopup.opener = self; 
}
//--></SCRIPT>
<script language="javascript"> /*
function countdown_clock(year, month, day, hour, minute, format) {
	//I chose a div as the container for the timer, but
	//it can be an input tag inside a form, or anything
	//who's displayed content can be changed through
	//client-side scripting.
	html_code = '<div id="countdown"></div>';
         
	document.write(html_code);
         
	countdown(year, month, day, hour, minute, format);                
}
         
function countdown(year, month, day, hour, minute, format) {
	Today = new Date('<? print date('F d, Y H:i:s',time()); ?>');
	Todays_Year = Today.getFullYear() - 2000;
	Todays_Month = Today.getMonth() + 1;                  
         
	//Convert both today's date and the target date into miliseconds.                           
	Todays_Date = (new Date(Todays_Year, Todays_Month, Today.getDate(), 
						Today.getHours(), Today.getMinutes(), Today.getSeconds())).getTime();                                 
	Target_Date = (new Date(year, month, day, hour, minute, 00)).getTime();                  
         
	//Find their difference, and convert that into seconds.                  
	Time_Left = Math.round((Target_Date - Todays_Date) / 1000);
         
	if(Time_Left < 0) Time_Left = 0;
         
	switch(format) {
		case 0:
			//The simplest way to display the time left.
			document.all.countdown.innerHTML = Time_Left + ' seconds';
			break;
		case 1:
			//More datailed.
			days = Math.floor(Time_Left / (60 * 60 * 24));
			Time_Left %= (60 * 60 * 24);
			hours = Math.floor(Time_Left / (60 * 60));
			Time_Left %= (60 * 60);
			minutes = Math.floor(Time_Left / 60);
			Time_Left %= 60;
			seconds = Time_Left;
                    
			dps = 's'; hps = 's'; mps = 's'; sps = 's';
			//ps is short for plural suffix.
			if(days == 1) dps ='';
			if(hours == 1) hps ='';
			if(minutes == 1) mps ='';
			if(seconds == 1) sps ='';
                    
			document.all.countdown.innerHTML = days + ' day' + dps + ', ';
			document.all.countdown.innerHTML += hours + 'h ';
			document.all.countdown.innerHTML += minutes +  'm ';
			document.all.countdown.innerHTML += seconds + 's';
			break;
		default: 
			document.all.countdown.innerHTML = Time_Left + ' seconds';
	}
               
	//Recursive call, keeps the clock ticking.
	setTimeout('countdown(' + year + ',' + month + ',' + day + ',' + hour + ',' + minute + ',' + format + ');', 1000);
} */
</script>

<form name="hiddenForm" action="auctiondetails.php" method="get">
   <input type="hidden" name="pqAnswer" />
   <input type="hidden" name="id" />
   <input type="hidden" name="content" />
   <input type="hidden" name="answerid" />
</form>
<table width="100%"  border="0" cellspacing="0" cellpadding="4">
   <tr>
      <td class="topitempage" width="100%"><?
			$nav = "";
			$nav2 = "";
			$parent=$auctionDetails['category'];
			if($parent > 0) {
				$croot = $parent;
				$cntr = 0;
				while ($croot>0) {
					$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT id,parent FROM probid_categories WHERE id='".$croot."' ") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					$crw = mysqli_fetch_array($sbcts);
					if($cntr == 0) {
						$nav = $c_lang[$crw['id']];
					} else {
						if($parent != $croot) {
							$nav = "<a href=\"".processLink('categories', array('category' => $c_lang[$crw['id']], 'parent' => $crw['id']))."\">".$c_lang[$crw['id']]."</a> > ".$nav;
						}
					}
					$cntr++;
					$croot = $crw['parent'];
				}
			} 
			$parent=$auctionDetails['addlcategory'];
			if($parent > 0) {
				$croot = $parent;
				$cntr = 0;
				while ($croot>0) {
					$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,parent FROM probid_categories WHERE id='".$croot."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					
					$crw = mysqli_fetch_array($sbcts);
					if($cntr == 0) {
						$nav2 = $c_lang[$crw['id']];
					} else {
						if($parent != $croot) {
							$nav2 = "<a href=\"".processLink('categories', array('category' => $c_lang[$crw['id']], 'parent' => $crw['id']))."\">".$c_lang[$crw['id']]."</a> > ".$nav2;
						}
					}
					$cntr++;
					$croot = $crw['parent'];
				}
			} 
			if (strlen($nav) || strlen($nav2)) {
				echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
							<tr class=\"topitem\" valign=\"top\">
							<td nowrap><b>$lang[itemlistedon]:</b></td>
							<td width=\"7\">&nbsp;</td>
							<td width=\"100%\">$nav";
				if (strlen($nav2) && strlen($nav)) echo "<br>";
				echo "$nav2</td></tr></table>";
			} ?></td>
   </tr>
</table>
<div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
<? headerdetails("<table cellpadding=0 cellspacing=0 border=0 width=100%><tr><td class='itemid'>".$imgarritem.$auctionDetails['itemname']."</td><td align=right class='itemidend'><b>$lang[itemid]: ".$_GET['id']."</b>&nbsp;&nbsp;</td></tr></table>");

$fields=array(); 
$getPics=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$auctionDetails['id']."'"); 
$nbPics=mysqli_num_rows($getPics); 

if ($nbPics>0) { 
	while ($addlPicture=mysqli_fetch_array($getPics)) { 
		array_push($fields, $addlPicture['name']); 
	}
} 
?>

<SCRIPT LANGUAGE="JavaScript"> 
	<!-- Begin 
	NewImg = new Array ("$setts[siteurl]/makethumb.php?pic=<?=$auctionDetails['picpath']?>&w=60&sq=Y" 
	<? foreach($fields as $field) { ?> 
	,"$setts[siteurl]/makethumb.php?pic=<?=$field;?>&w=100&sq=Y" 
	<? } ?> 
	); 
	var ImgNum = 0; 
	var ImgLength = NewImg.length - 1; 
	
	//Time delay between Slides in milliseconds 
	var delay = 2000; 
	
	var lock = false; 
	var run; 
	function chgImg(direction) { 
		if (document.images) { 
			ImgNum = ImgNum + direction; 
			if (ImgNum > ImgLength) { 
				ImgNum = 0; 
			} 
			if (ImgNum < 0) { 
				ImgNum = ImgLength; 
			} 
			document.slideshow.src = NewImg[ImgNum]; 
		} 
	} 
	
	function auto() { 
		if (lock == true) { 
			lock = false; 
			window.clearInterval(run); 
		} else if (lock == false) { 
			lock = true; 
			run = setInterval("chgImg(1)", delay); 
		} 
	} 
	//  End --> 
</script>

<!-- Table for subhead -->
<!-- was deleted -->

   <?
   
	$winnerLogged = getSqlRow("SELECT * FROM probid_winners WHERE buyerid='".$_SESSION['memberid']."' AND auctionid='".$_GET['id']."'");

	if ($auctionDetails['acceptdirectpayment']&&($auctionDetails['closed']!=0||$auctionDetails['auctiontype']=="dutch")&&$auctionDetails['ownerid']!=$_SESSION['memberid']&&$winnerLogged&&$winnerLogged['payment_status']=="confirmed") { 
		## new function, calculate payment, it's intended for dutch auctions especially

    	//------alemiks---------------
    	/*
		$getDpPayment = mysql_query("SELECT amount, quant_req, quant_offered FROM probid_winners WHERE
		buyerid='".$_SESSION['memberid']."' AND auctionid='".$_GET['id']."' AND directpayment_paid=0"); 
    	*/
    	$dpPayment = getSqlRow("SELECT amount, quant_req, quant_offered FROM probid_winners WHERE
		buyerid='".$_SESSION['memberid']."' AND auctionid='".$_GET['id']."'");
    	//------alemiks---------------
		$dpQuant = 0;
		//$paymentAmount = 0;PBxr97
		
		$postageC = 0;
		//while ($dpPayment = mysql_fetch_array($getDpPayment)) {
		$dpQuant = ($dpPayment['quant_offered']>0) ? $dpPayment['quant_offered'] : 1;
		$paymentAmount = $dpQuant * $dpPayment['amount'];
		$paymentAmount += $dpQuant * $auctionDetails['postage_costs'];
		//$paymentAmount += $dpQuant * $auctionDetails['insurance'];
		//}
		//$paymentAmount += $postageC;
	
		if (is_numeric($paymentAmount) && $paymentAmount>0) { ?>
   <!-- Header Direct Payment -->
<table width="100%" border="0" cellpadding="3" cellspacing="2" class="subitem">
   <tr height="21">
      <td colspan="5" class="c4"><b>&raquo; <? echo (strtoupper($lang[headdirectpayment]));?></b></td>
   </tr>
   <tr>
      <td colspan="5" class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr>
      <td colspan="5"><img src="themes/v52/img/btn.gif" height=100 width=200 /><!-- End Header Direct Payment -->
         <table width="100%" border="0" cellspacing="1" cellpadding="0" class="c2">
            <tr valign="top">
               <td width="100%"><?
						if ($winnerLogged['directpayment_paid']||$winnerLogged['flag_paid']) {
							echo "<center>" . $lang[paiddirectpayment] . "</center>";
						} else {
							echo "<div style='padding: 6px;'>".$lang[you_won_item]."";
							echo "<b>".$auctionDetails[itemname]."</b>!<br>";
							echo "".$lang[you_won_item2]."</div>";
        					$acceptedPaymentSystems = explode(',',$auctionDetails['accept_payment_systems']);
							$paymentAmount=number_format($paymentAmount,2,'.','');
							$returnUrl=$path."paymentdone.php";
							$failureUrl=$path."paymentfailed.php";
						  	$paymentDetails = getSqlRow("SELECT * FROM probid_users WHERE id=".$winnerLogged['sellerid']);
						  	if($paymentDetails['paypalemail'] && in_array('1',$acceptedPaymentSystems)) {
								$notifyUrl=$path."paymentprocess.php?table=100";

     							$pmDesc = $lang[itempayment].': [ID#'.$auctionDetails['id'].'] '.$auctionDetails['itemname'];
								
								paypalForm($_REQUEST['winnerid'],$paymentDetails['paypalemail'],$paymentAmount,$auctionDetails['currency'],$returnUrl,$failureUrl,$notifyUrl,100,$pmDesc,TRUE,'paydirpayment.php?s=paypal&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['worldpayid'] && in_array('2',$acceptedPaymentSystems)) {
								$notifyUrl=$path."worldpayprocess.php?table=100";
            				worldpayForm($_REQUEST['winnerid'],$paymentDetails['worldpayid'],$paymentAmount,$auctionDetails['currency'],$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=worldpay&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['nochexemail'] && in_array('4',$acceptedPaymentSystems)) {
           	 				$notifyUrl=$path."nochexprocess.php?table=100";
            				nochexForm($_REQUEST['winnerid'],$paymentDetails['nochexemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=nochex&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['checkoutid'] && in_array('5',$acceptedPaymentSystems)) {
            				//$notifyUrl=$path."checkoutprocess.php?table=100";
            				checkoutForm($_REQUEST['winnerid'],$paymentDetails['checkoutid'],$paymentAmount,100,TRUE,'paydirpayment.php?s=checkout&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['ikoboid']&&$paymentDetails['ikoboipn'] && in_array('3',$acceptedPaymentSystems)) {
            				$notifyUrl=$path."ikoboprocess.php?table=100";
								ikoboForm($_REQUEST['winnerid'],$paymentDetails['ikoboid'],0,$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=ikobo&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['protxname']&&$paymentDetails['protxpassword'] && in_array('7',$acceptedPaymentSystems)) {
            				$notifyUrl=$path."protxprocess.php?table=100";
            				protxForm($_REQUEST['winnerid'],$paymentAmount,$auctionDetails['currency'],$returnUrl,$failureUrl,$notifyUrl,100,TRUE,'paydirpayment.php?s=protx&winnerid='.$_REQUEST['winnerid']);
            				echo('<br>');
        					}
        					if($paymentDetails['authnetid']&&$paymentDetails['authnettranskey'] && in_array('6',$acceptedPaymentSystems)) {
            				//$notifyUrl=$path."authorize.net.process.php?table=100";
            				authorizeNetForm($_REQUEST['winnerid'],$paymentAmount,100,TRUE,'paydirpayment.php?s=authorize&winnerid='.$_REQUEST['winnerid']);
        					}
						} ?>
               </td>
            </tr>
         </table></td>
   </tr>
   </table>
   <? } } ?>

<br>
<? echo (!empty($msgSys_msg)) ? addSpecialChars($msgSys_msg) : addSpecialChars($_REQUEST['msgsys']);?>
<!-- 3 cell table -->
<table width="100%" border="0" align="center" cellpadding="1" cellspacing="3">
   <tr valign="top">
      <td width="20%" align="center" class="contentfont"><? 
			if (file_exists($auctionDetails['picpath'])) { 
				echo	"<table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"3\" class=\"border\">\n". 
						"   <tr>                                             \n". 
						"      <td height=\"110\" align=\"center\" class=\"c2\">         \n". 
						"         <img src=\"$setts[siteurl]/makethumb.php?pic=".$auctionDetails['picpath']."&w=100&sq=Y\" name=\"slideshow\"> \n". 
						"      </td>                                          \n". 
						"   </tr>                                             \n". 
						"   <tr class=\"contentfont\">                                             \n". 
						"      <td align=center class=\"c3\">                        \n". 
						"         <a href=\"javascript:chgImg(-1)\"><img src=\"themes/".$setts['default_theme']."/img/system/back.gif\" width=\"15\" height=\"15\" border=\"0\" align=\"absmiddle\"></a>&nbsp;&nbsp;<a href=\"javascript:auto()\" align=\"absmiddle\">$lang[autostop]</a>&nbsp;&nbsp;<a href=\"javascript:chgImg(1)\"><img src=\"themes/".$setts['default_theme']."/img/system/right.gif\" width=\"15\" height=\"15\" border=\"0\" align=\"absmiddle\"></a>                          \n". 
						"      </td>                                          \n". 
						"   </tr>                                             \n". 
						"</table><br>                                         \n"; 
            } else {
            ?>
            <table>
            <tr>
                <td><img src="images/noimage.gif" border="0" width="120" /></td>
            </tr>
             
            </table>
            <?
            } 
				
			$getpics=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$_GET['id']."'"); 

			$daysLeft = daysleft($auctionDetails['enddate'],$setts['date_format']);
			$timeLeft = timeleft($auctionDetails['enddate'],$setts['date_format']);
			$currentBid = getSqlField("SELECT * FROM probid_bids WHERE auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."' ORDER BY bidamount DESC","bidamount") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$isCurrentBid = getSqlNumber("SELECT * FROM probid_bids WHERE auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."'"); 
			$showBN = showBuyNow($auctionDetails['rp'],$auctionDetails['rpvalue'],$auctionDetails['maxbid'],$auctionDetails['nrbids']);

         if ($auctionDetails['closed']==0&&$daysLeft>0&&!$binAuction) {
         	if ($_SESSION['membersarea']!="Active") {
					echo '<p align="center"><b><font class="redfont">'.$lang[bid_errreg].'</font></b>';
					echo '<br><br><a href="'.$path_ssl.'login.php?auctionid='.$_REQUEST['id'].'">'.$lang[memberlogin].'</a> </p>';
				} else {
					if (!$binAuction) { ?>
         <table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
            <form action="bid.php" method="post">
               <? if ($auctionDetails['auctiontype']=="dutch") { ?>
               <tr>
                 
                  <td align=center><?=$lang[quant]?>
                     :
                     <input name="quantity" type="text" id="quantity" value="1" size="3"></td>
               </tr>
               <? } ?>
               <tr>
                  <? if ($auctionDetails['auctiontype']=="dutch") { } else {?>
                  
                  <?    } ?>
                  <td width="100%" align=center><strong>
                     <?=$auctionDetails['currency'];?>
                     </strong>
                     <input name="maxbid" type="text" id="maxbid" size="7">
                     <? 
							if ($auctionDetails['auctiontype']=="standard") {
								if ($auctionDetails['maxbid']==0) $minimumBid = $auctionDetails['bidstart'];
								else $minimumBid = setMinBid($auctionDetails['maxbid'],$auctionDetails['bi'],$auctionDetails['bivalue'],$auctionDetails['auctiontype']);
							} else {
								$maximumBid = $auctionDetails['maxbid'];
								$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE 
								bidamount='".$maximumBid."' AND auctionid='".$auctionDetails['id']."'");
								$quantity = 0;
								while ($bids = mysqli_fetch_array($getBids)) {
									$quantity += $bids['quantity'];
								}
								if ($auctionDetails['maxbid']==0) $minimumBid=$auctionDetails['bidstart'];
								else if ($quantity<$auctionDetails['quantity']) $minimumBid=$auctionDetails['maxbid'];
								else $minimumBid=setMinBid($auctionDetails['maxbid'],$auctionDetails['bi'],$auctionDetails['bivalue'],$auctionDetails['auctiontype']);
							} ?>
                     <input type="hidden" name="itemname" value="<?=remSpecialChars($auctionDetails['itemname']);?>">
                     <input type="hidden" name="auctionid" value="<?=$auctionDetails['id'];?>">
                     <input type="hidden" name="action" value="bid_CONF">
                     <input type="hidden" name="quant_avail" value="<?=$auctionDetails['quantity'];?>">
                  </td>
               </tr>
               <tr>
                  <td width="100%" align=center><input name="placebidok" type="submit" id="placebidok" value="<?=$lang[placebid]?>" <? echo ($isBanned||$_SESSION['memberid']==$auctionDetails['ownerid']) ? "disabled" : ""; ?>></td>
               </tr>
            </form>
         </table>
         <? 		if ($auctionDetails['ownerid']==$_SESSION['memberid']&&$auctionDetails['closed']==0&&!$binAuction) 
							echo "<br><table width=100% border=0 cellspacing=0 cellpadding=0><tr><td align='center' class='topitempage alertfont'>$lang[biderr_youpost]</td></tr></table>";
					} 
				} 
			}
			else if ($auctionDetails['closed']!=0&&!$binAuction) echo "<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td align='center' class='topitempage alertfont'>$lang[biderr_bidclosed]</td></tr></table>";
			else if ($daysleft<=0&&!$binAuction) echo "<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td align='center' class='topitempage alertfont'>$lang[biderr_bidclosed]</td></tr></table>";
			else if ($binAuction) echo "<table width=100% border=0 cellspacing=0 cellpadding=0><tr><td align='center' class='topitempage alertfont'>$lang[buynowonlyauctionalalert]</td></tr></table>";
			?>
		

      </td>
     
      <td width="50%"><!-- Start Table for item details -->
         <table width="100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <? if (!$binAuction) { ?>
            <tr valign="top">
               <td nowrap><b>
                  <?=$lang[currbid]?>
                  :</b></td>
               <td><b><? echo displayAmount($auctionDetails['maxbid'],$auctionDetails['currency']);?></b></td>
            </tr>
            <tr valign="top">
               <td nowrap><b>
                  <?=$lang[startbid]?>
                  :</b></td>
               <td><b><font class="redfont"><? echo displayAmount($auctionDetails['bidstart'],$auctionDetails['currency']);?></font></b><? echo (($auctionDetails['currency']=="EEK") ? '<b><font class="redfont"> ('.round(($auctionDetails['bidstart']/15.6466),2).' EUR)</b></font>': "" )?>
               </td>
            </tr>
          
            <? 
				if ($_SESSION['membersarea']=="Active") { 
					$currentBid = getSqlField("SELECT * FROM probid_bids WHERE 
					auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."' ORDER BY bidamount DESC","bidamount") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					$isCurrentBid = getSqlNumber("SELECT * FROM probid_bids WHERE auctionid='".$_GET['id']."' AND bidderid='".$_SESSION['memberid']."'");
					if ($isCurrentBid>0) { ?>
            <tr valign="top" class="c3">
               <td><b>
                  <?=$lang[yourbid]?>
                  :</b></td>
               <td><font class='greenfont'><b><? echo displayAmount($currentBid,$auctionDetails['currency']);?></b></font></td>
            </tr>
            <?	}
				} ?>
            <? } ?>
            <tr valign="top">
               <td><b>
                  <?=$lang[quant]?>
                  :</b></td>
               <td><b>
                  <?=$auctionDetails['quantity'];?>
                  </b></td>
            </tr>
            <? if (!$binAuction) { ?>
            <tr valign="top">
               <td nowrap><b>
                  <?=$lang[num_bids]?>
                  :</b></td>
               <td class="contentfont"><?=$auctionDetails['nrbids'];?>
                  <? 
		  				if ($auctionDetails['nrbids']>0) { 
		            	echo "<a href=\"bidhistory.php?id=".$auctionDetails['id']."&name=".remSpecialChars($auctionDetails['itemname'])."&quantity=".$auctionDetails['quantity']."\">".$lang[viewhistory]."</a>";
						} ?></td>
            </tr>
            <? } ?>
            <tr valign="top">
               <td><b>
                  <?=$lang[timeleft]?>
                  :</b></td>
               <td><? 

						$daysLeft = daysleft($auctionDetails['enddate'],$setts['date_format']);
						$timeLeft = timeleft($auctionDetails['enddate'],$setts['date_format']);
						echo ($daysLeft>0) ? $timeLeft : $lang[bidclosed];	?></td>
            </tr>
            <tr valign="top">
               <td><b>
                  <?=$lang[location]?>
                  :</b></td>
               <td><? echo $auctionDetails['zip'].", ".$sellerDetails['city'].", ".$sellerDetails['state'];?></td>
            </tr>
            <tr valign="top">
               <td><b>
                  <?=$lang[country]?>
                  :</b></td>
               <td><?=$auctionDetails['country'];?></td>
            </tr>
            <tr valign="top">
               <td><b>
                  <?=$lang[started]?>
                  :</b></td>
               <td><?=format_date($auctionDetails['startdate'],1)?></td>
            </tr>
            <tr valign="top">
               <td><b>
                  <?=$lang[ends]?>
                  :</b></td>
               <td><?=format_date($auctionDetails['enddate'],1)?></td>
            </tr>
            <tr>
               <td><b>
                  <?=$lang[status]?>
                  :</b></td>
               <td><? echo ($auctionDetails['closed']==0)?"<font class='greenfont'><b>".$lang[open]."</b></font>":"<font class='redfont'><b>".$lang[closed]."</b></font>";?></td>
            </tr>
				<? if ($setts['buyout_process']==1&&$auctionDetails['bn']=="Y") { ?>
            <form action="makeoffer.php" method="post">
               <input type="hidden" name="auctionid" value="<?=$_GET['id'];?>" />
               <tr>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               </tr>
               <tr>
                  <td><b>
                     <?=$lang[makeoffer]?>
                     :</b></td>
                  <td><strong><?=$lang[offerrange];?></strong> : <?=offerRange($auctionDetails['offer_range_min'], $auctionDetails['offer_range_max'], $auctionDetails['currency']);?></td>
               </tr>
               <tr>
                  <td></td>
                  <td><?
							if ($_SESSION['membersarea']!="Active") {
								echo $lang[please].' <a href="'.$path_ssl.'login.php?auctionid='.$_GET['id'].'">'.$lang[login].'</a> '.$lang[tomakeoffer];
							} else if ($_SESSION['memberid']==$auctionDetails['ownerid']) {
								echo $lang[offerimpossible];
							} else if ($auctionDetails['ownerid']==1) {
								echo $lang[auctclosed];
							} else {
								echo ($auctionDetails['auctiontype']=="dutch") ? '<strong>Quantity:</strong> <input type="text" size="3" name="quantity"><br>' : '';
								echo '<strong>'.$lang[amountitem].':</strong> '.$auctionDetails['currency'].' <input type="text" size="7" name="amount"> ';
								echo '<input type="submit" name="makeofferok" value="'.$lang[go].'">';
							}
						 ?></td>
               </tr>
               <tr>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               </tr>
            </form>
            
				<? } else if ($setts['buyout_process']==0) { ?>
            <tr>
               <td colspan="2" class="contentfont"><table border="0" width="100%" cellpadding="0" cellspacing="0">
                     <tr>
                        <?
								## if there is a reserve price, the BIN will exist until maxbid<resprice
								## otherwise it will exist only until a bid is placed
								$showBN = showBuyNow($auctionDetails['rp'],$auctionDetails['rpvalue'],$auctionDetails['maxbid'],$auctionDetails['nrbids']);
								if ($showBN&&$auctionDetails['closed']==0) { 
									$bnValue=displayAmount($auctionDetails['bnvalue'],$auctionDetails['currency']);
									echo "<td align=\"center\"><br>";
									$current_language=get_current_lang();
									if ($auctionDetails['ownerid']!=$_SESSION['memberid']&&$auctionDetails['closed']!=1) { 
										echo (($auctionDetails['bn']=="Y"&&$auctionDetails['active']==1&&$layout['act_buynow']==1)?"<div onclick=document.location.href=\"buynow.php?id=".$auctionDetails['id']."\"; class=\"buyitnow_button_image\"><a class=\"buyitnow_button_a\" href=\"buynow.php?id=".$auctionDetails['id']."\">$lang[buynow]</a></div><a href=\"buynow.php?id=".$auctionDetails['id']."\">".$lang[bynow_for]." <strong>".$bnValue."</strong> ":""); echo(($auctionDetails['currency']=="EEK")?"<BR>(".number_format($auctionDetails['bnvalue']/15.6466,2,',','.')." EUR)</a>":"");
										} else if ($auctionDetails['ownerid']==$_SESSION['memberid']&&$auctionDetails['closed']!=1) { 
											echo (($auctionDetails['bn']=="Y"&&$auctionDetails['active']==1&&$layout['act_buynow']==1)?"<div class=\"buyitnow_button_image\"><span class=\"buyitnow_button_a\" href=\"buynow.php?id=".$auctionDetails['id']."\">$lang[buynow]</span></div><br>".$lang[bynow_for]." <strong>".$bnValue."</strong> ":"");
										} else {
											echo (($auctionDetails['bn']=="Y"&&$auctionDetails['active']==1&&$layout['act_buynow']==1)?"<div class=\"buyitnow_button_image\"><span class=\"buyitnow_button_a\" href=\"buynow.php?id=".$auctionDetails['id']."\">$lang[buynow]</span></div><br>".$lang[bynow_for]." <strong>".$bnValue."</strong>":"");
									}
									/*
									echo "<div onclick=document.location.href=\"buynow.php?id=".$auctionDetails['id']."\"; class=\"buyitnow_button_image\"><a class=\"buyitnow_button_a\" href=\"buynow.php?id=".$auctionDetails['id']."\">$lang[buynow]</a></div><br><br>".$lang[bynow_for]." <strong>".$bnValue."</strong> "; echo(($auctionDetails['currency']=="EEK")?"<BR>(".number_format($auctionDetails['bnvalue']/15.6466,2,',','.')." EUR)":"");
									echo "<div class=\"buyitnow_button_image\"><span class=\"buyitnow_button_a\" href=\"buynow.php?id=".$auctionDetails['id']."\">$lang[buynow]</span></div><br>".$lang[bynow_for]." <strong>".$bnValue."</strong> ";
									echo "".(($auctionDetails['bn']=="Y"&&$auctionDetails['active']==1&&$layout['act_buynow']==1)?"<div class=\"buyitnow_button_image\"><span class=\"buyitnow_button_a\" href=\"buynow.php?id=".$auctionDetails['id']."\">$lang[buynow]</span></div><br>".$lang[bynow_for]." <strong>".$bnValue."</strong>":"");
									echo "test<br>";
									*/
									echo "</td>";
									
							
								} 
									
								
								echo "<td width=\"10\"></td>"; ?>
                     </tr>
                  </table></td>
            </tr>
				<? } ?>
            <tr>
               <td colspan="2"><font class='bluefont'>
                  <? 
						if ($auctionDetails['rp']=="Y") { 
							if ($auctionDetails['rpwinner']==0) echo ($auctionDetails['rpvalue']>$auctionDetails['maxbid']) ? $lang[reservenotmet] : $lang[reservemet]; 
							echo ($auctionDetails['closed']==1&&$auctionDetails['rpvalue']>$auctionDetails['maxbid']&&$_SESSION['memberid']==$auctionDetails['ownerid']&&$auctionDetails['nrbids']>0&&$auctionDetails['rpwinner']==0) ? "&nbsp; <span class=\"contentfont\">[ <a href=\"reserveoffers.php?id=".$auctionDetails['id']."\"><strong>$lang[makeresoffer]</strong></a> ]</span>" : "";
						} ?>
                  </font> </td>
            </tr>
            <tr>
               <td colspan="2" class="contentfont"><? 
						if ($setts['swap_items']==1&&$auctionDetails['ownerid']!=$_SESSION['memberid']&&$auctionDetails['closed']!=1&&$auctionDetails['isswap']=="Y"&&$auctionDetails['deleted']!=1) {
							echo "<a href=\"swapitems.php?id=".$auctionDetails['id']."\">".$lang[offerswap]."</a>";
						} ?>
               </td>
            </tr>
            <? if (!$binAuction||$auctionDetails['closed']==1) { ?>
            <tr>
               <td><b> <? echo ($auctionDetails['closed']==1) ? $lang[winnerss] : $lang[highbid]; ?> :</b></td>
               <td class="contentfont"><b>
                  <? 
						if ($auctionDetails['closed'] == 1) { 
							$getAuctionWinner = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT buyerid, bnpurchase, amount FROM probid_winners WHERE auctionid='".$auctionDetails['id']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
							$nbWinners = mysqli_num_rows($getAuctionWinner);
							while ($auctionWinner = mysqli_fetch_array($getAuctionWinner)) {
								$bidderName = getSqlField("SELECT username FROM probid_users WHERE id='".$auctionWinner['buyerid']."'","username");
								echo "<font class='greenfont'>".displayAmount($auctionWinner['amount'],$auctionDetails['currency'])."</font> - ".$bidderName." <a href=\"viewfeedback.php?owner=".$auctionWinner['buyerid']."&auction=".$auctionDetails['id']."\">".getFeedback($auctionWinner['buyerid'])."</a>";
								if ($auctionWinner['bnpurchase']==1) echo "</b> [ ".$lang[purchasedwithbn]." ] <b>";
								if ($nbWinners>0) echo "<br>";
								$foundWinner = TRUE;
							}
							if (!$foundWinner) echo $lang[na];
						} else { 
							echo ($auctionDetails['rpvalue']<=$auctionDetails['maxbid'])?"<font class='greenfont'><b>":"";
							echo displayAmount($auctionDetails['maxbid'],$auctionDetails['currency']);
							echo ($auctionDetails['rpvalue']<=$auctionDetails['maxbid'])?"</b></font><br>":"";
								  
							if ($auctionDetails['maxbid']>0) {
								echo " - ";
								if ($auctionDetails['private']!="Y") {
									
									##MySQL error fix - ALEKSEI HODUNKOV
									$getBidderId=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE auctionid='".$auctionDetails['id']."' AND valja=0 AND invalid=0 ORDER BY id DESC");
									##$getBidderId=mysql_query("SELECT * FROM probid_bids WHERE auctionid='".$auctionDetails['id']."'");
									$nbBidders = mysqli_num_rows($getBidderId);
									## display only one high bidder, and a (more...) link to a popup if there are more high bidders
									$highBidder=mysqli_fetch_array($getBidderId);
										
									$bidderName = getSqlField("SELECT username FROM probid_users WHERE id='".$highBidder['bidderid']."'","username");
									echo $bidderName." <a href=\"viewfeedback.php?owner=".$highBidder['bidderid']."&auction=".$auctionDetails['id']."\">".getFeedback($highBidder['bidderid'])."</a>";
										
									##if ($nbBidders>1) echo " [ <a href=\"javascript:popUpSmall('popup_allbidders.php?id=$_GET[id]');\">".$lang[more_bidders]."</a> ]";
								} else { 
									echo $lang[bidderhidden]; 
								}
							} 
						} ?>
                  </b></td>
            </tr>
            <? } ?>
            <!-- VAT NOTICE BLOCK -->
            <?
            if(($setts['enable_vat']==1)&&($auctionDetails['apply_vat']==1)){
                // get VAT permissions
                $seller = getSqlRow("SELECT * FROM probid_users WHERE id=".$auctionDetails['ownerid']);
                $country_id = getSqlField("SELECT id FROM probid_countries WHERE name='".$seller['country']."'",'id');
                $user_vat = getSqlRow("SELECT * FROM probid_vat_setts WHERE LOCATE(',".$country_id.",',CONCAT(',',countries,','))>0");
                if($user_vat){
                    $user_types = explode(',',$user_vat['users_no_vat']);
                    $vat_rate = getSqlField("SELECT rate FROM probid_vat_rates WHERE vat_id = '".$user_vat['id']."'", 'rate');
                    $msg = str_replace('{AMOUNT}',$vat_rate,$lang[m_charge_vat]);
                    $msg = str_replace('{VATNAME}',$user_vat['symbol'],$msg);
                    $msg2 = str_replace('{VATNAME}',$user_vat['symbol'],$lang[m_vat_not_applied]);

                    // get countries list
                    $countries = explode(',',$user_vat['countries']);
                    $counties_list = '';
                    foreach($countries as $v) {
                        $country_name = getSqlField("SELECT name FROM probid_countries WHERE id = '".$v."'", 'name');
                        $counties_list .= $country_name.', ';
                    }
                    $counties_list = rtrim(trim($counties_list),',');

                    // get no vat user types
                    $no_vat_listing = '<li>'.$lang[m_from_other_countries].'</li>';
                    foreach(array('a','b','c','d') as $v) {
                        if(in_array($v,$user_types)){
                            $no_vat_listing .= '<li>'.str_replace('tax',$user_vat['symbol'],$lang[m_users_vat][$v]).'</li>';
                        }
                    }
          	?>
            <tr>
               <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <tr>
               <td colspan="2"><?='<b>'.$msg.' :</b><br>'.$counties_list.'<br><b>'.$msg2.'</b><br><ul style="margin:0;padding:0;list-style-position:inside;">'.$no_vat_listing.'</ul>';?>
               </td>
            </tr>
            <?
				if($_SESSION['memberid'] && ($auctionDetails['ownerid'] != $_SESSION['memberid'])){
            	$buyer = getSqlRow("SELECT * FROM probid_users WHERE id=".$_SESSION['memberid']);
               if(($buyer['companyname']=="")&&($buyer['vat_uid_number']==""))$user_type='d';
               if(($buyer['companyname']=="")&&($buyer['vat_uid_number']!=""))$user_type='c';
               if(($buyer['companyname']!="")&&($buyer['vat_uid_number']==""))$user_type='b';
               if(($buyer['companyname']!="")&&($buyer['vat_uid_number']!=""))$user_type='a';
               if(!in_array($user_type,$user_types)){
               	$msg_for_buyer = str_replace('TAX',$user_vat['symbol'],$lang[m_liable]);
					} else {
               	$msg_for_buyer = str_replace('TAX',$user_vat['symbol'],$lang[m_exempted]);
					} ?>
            <tr>
               <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <tr>
               <td colspan="2"><?='<b>'.$msg_for_buyer.'</b>';?>
               </td>
            </tr>
            <? } // if($_SESSION['memberid'] && ($auctionDetails['ownerid'] != $seller['id']))
					} // if($user_vat)
				} // if(($setts['enable_vat']==1)&&($auctionDetails['apply_vat']==1))
				?>
            <!-- VAT NOTICE BLOCK -->
            <tr>
               <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
				<? 
				$category_id = getMainCat($auctionDetails['category']);

				### we overwrite the $fees array with what we need
				$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
				
				if (@eregi('b', $fee['endauction_fee_applies'])) { ?>
            <tr>
               <td colspan="2"><?=$lang[eafeepaidbybuyer];?></td>
            </tr>
            <tr>
               <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
				<? } ?>
            <tr>
               <td colspan="2"><strong>
                  <?/*COMMENTED BY MAK=$lang[item_is_watched_by];?>
                  <?=getSqlNumber("SELECT DISTINCT userid FROM probid_auction_watch WHERE auctionid='".$_GET['id']."'"); */?>
                  </strong></td>
            </tr>
			
         </table>
         <? if ($isBanned) { 
				$bannedDetails = getSqlRow("SELECT * FROM probid_blocked_users WHERE userid='".$_SESSION['memberid']."' AND ownerid='".$auctionDetails['ownerid']."'"); ?>
         <table width="100%" border="0" cellspacing="2" cellpadding="2" class="errormessage">
            <tr>
               <td><? echo ($bannedDetails['show_reason']) ? "<strong>".$lang[excludedmsg_reason]."</strong><br>".$bannedDetails['block_reason'] : $lang[excludedmsg_noreason]; ?></td>
            </tr>
         </table>
		 
         <? } 
		 ?>

		<!-- Begin social networks block -->	
		<? echo $lang[share_and_add];?><br><br>
		<!-- E-mail -->
	   <a href="auctionfriend.php?owner=<?=$auctionDetails['ownerid'];?>&auctionid=<?=$auctionDetails['id'];?>"><img src="themes/<?=$setts['default_theme'];?>/img/icon-email-small.png" border="0" hspace="1"></a>
		<!-- Add to watch list -->
	   <a href="itemwatch.php?id=<?=$auctionDetails['id'];?>&itemname=<?=$auctionDetails['itemname'];?>"><img src="themes/<?=$setts['default_theme'];?>/img/icon-watch-small.png" border="0" hspace="2" class="socialbutton"></a>

		<!-- Facebook -->
	   <script>
	  function fbs_click()
	  {u=location.href;
	   t=document.title;
	   window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');return false;}
	   </script>
	   <!--<style> 
	   html .fb_share_button 
	   { 
	   display: -moz-inline-block; 
	   display:inline-block; 
	   padding:0px 40px 0 0px; 
	   height:40px; 
	   border:1px solid #d8dfea; 
	   background:url(themes/<?=$setts['default_theme'];?>/img/icon-facebook-small.png) no-repeat top right; 
	   } 
	   html .fb_share_button:hover 
	   { 
	   color:#fff; 
	   border-color:#295582; 
	   background:#3b5998 url(<?=$setts['default_theme'];?>/img/icon-facebook-small.png) no-repeat top right; 
	   text-decoration:none; 
	   } 
	   </style> -->
	   <a rel="nofollow" href="http://www.facebook.com/share.php?u=<;url>" class="fb_share_button" onclick="return fbs_click()" target="_blank" style="text-decoration:none;"><img src="themes/<?=$setts['default_theme'];?>/img/icon-facebook-small.png" class="socialbutton" hspace="2"/></a>
		<!-- Vkontakte -->
		<script type="text/javascript">
	  	document.write(VK.Share.button(false,{type: "custom", text: "<img src=\"http://vk.com/images/vk32.png?1\" / width=28 height=28 hspace=2 class=\"socialbutton\">"}));
		</script>
		<!-- LinkedIn -->
		<a href="http://www.linkedin.com/shareArticle?mini=true&title=<? echo $auctionDetails[itemname];?>&summary=<? echo $auctionDetails[itemname];?>&source=SBE.EE&url=<? echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">
		<img src="themes/<?=$setts['default_theme'];?>/img/icon-linkedin-small.png" class="socialbutton" alt="Share in Linkedin" title="linkedin share button" /></a>
		<!-- Twitter -->
		<a href="http://twitter.com/share" data-count="none" data-via="SBEauction" background="none"><IMG SRC="http://www.sbe.ee/themes/v52/img/icon-twitter.png" class="socialbutton" width=28 height=28 ></IMG></a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>	
		<!-- Bookmarks -->
		<a href="javascript:bookmarksite(document.title, location.href);" onMouseOver="window.status='Bookmark Us!'; return true" onMouseOut="window.status='';return true;"><img src="http://www.sbe.ee/themes/v52/img/system/gold_star.gif" class="socialbutton" alt="Bookmark us!"></img>
        
			<!-- End social networks block -->
		 <?
			
			if ($auctionDetails['closed']==1) { 
				$showMsgBoardLink = FALSE;
				$getWinners = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.id, w.buyerid, u.username FROM probid_winners w, probid_users u WHERE 
				w.auctionid='".$_GET['id']."' AND w.payment_status='confirmed' AND w.buyerid=u.id"); 
				(int) $winCnt = 0;
				while ($winn = mysqli_fetch_array($getWinners)) {
					if ($winn['buyerid']==$_SESSION['memberid']) { 
						$showMsgBoardLink = TRUE;
						$linkWinIdBuyer = $winn['id'];
					}
					$linkWinId[$winCnt] = $winn['id'];
					$linkWinUsername[$winCnt++] = $winn['username'];
				} 
				if ($auctionDetails['ownerid']==$_SESSION['memberid']) $showMsgBoardLink = TRUE;
				
				if ($showMsgBoardLink) { ?>

         <table width="100%" border="0" cellspacing="2" cellpadding="2" class="errormessage">
            <? if ($auctionDetails['ownerid']==$_SESSION['memberid']) { 
					for($i=0; $i<$winCnt; $i++) { ?>
				<tr>
               <td class="contentfont" style="font-weight: bold;">&nbsp;&#8226;&nbsp;[ <?=$lang[buyerusern].' : '.$linkWinUsername[$i];?> ] <a href="msgboard.php?winnerid=<?=$linkWinId[$i];?>"><span class="greenfont"><?=$lang[go_m_view_messages];?></a></b></td>
            </tr>
				<? }
				} else { ?>
				<tr>
               <td class="contentfont" style="font-weight: bold;">&nbsp;&#8226;&nbsp;<a href="msgboard.php?winnerid=<?=$linkWinIdBuyer;?>"><span class="greenfont"><?=$lang[go_m_view_messages];?></a></b></td>
            </tr>
				<? } ?>
         </table>
         <? } 
			} ?>
      </td>
      <td width="30%"><table width="100%" border="0" cellspacing="1" cellpadding="3" class="contentfont">
            <tr>
               <td class="mainmenu" style="color: #FFFFFF; font-weight: bold; padding-left: 3px;"><?=$lang[sellerinfo]?></td>
            </tr>
            <tr>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
           
            <tr>
               <td><? echo $sellerDetails['username'];?> <a href="viewfeedback.php?owner=<?=$sellerDetails['id'];?>&auction=<?=$auctionDetails['id'];?>"> <? echo getFeedback($sellerDetails['id']);?> </a> </td>
            </tr>
            <tr>
               <td><b>&raquo;</b>
                  <? if ($sellerDetails['regdate']!=0) echo $lang[regsince]." ".date("d.m.Y",$sellerDetails['regdate'])." ".$lang[inthe]." ".$sellerDetails['country']; ?>
               </td>
            </tr>
            <tr>
               <td><b>&raquo;</b> <a href="otheritems.php?owner=<?=$auctionDetails['ownerid'];?>&nick=<?=$sellerDetails['username'];?>">
                  <?=$lang[seeallauctsseller]?>
                  </a></td>
            </tr>
            <?
				$shopDets = getSqlRow("SELECT aboutpage_type, store_active, store_name FROM probid_users WHERE id='".$sellerDetails['id']."'");
				if ($shopDets['aboutpage_type']==2&&$shopDets['store_active']==1) { ?>
            <tr>
               <td><b>&raquo;</b> <a href="<?=processLink('shop', array('store' => $shopDets['store_name'], 'userid' => $auctionDetails['ownerid'])); ?>">
                  <?=$lang[seeshop]?>
                  </a></td>
            </tr>
            <? } ?>
            <tr>
               <td align="center"><table width="100%" cellpadding="2" cellspacing="1" border="0" class="c4" >
                     <tr>
                        <td nowrap>&nbsp;<b>
                           <?=$lang[feedback_rating];?>
                           </b>:</td>
                        <td width="100%"><? echo calcFeedback($sellerDetails['id']);?></td>
                     </tr>
                     <tr class="c2 positive">
                        <td nowrap><img src="images/5stars.gif"></td>
                        <td><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$auctionDetails['ownerid']."' AND submitted=1 AND rate=5"); ?></td>
                     </tr>
                     <tr class="c3 positive">
                        <td nowrap><img src="images/4stars.gif"></td>
                        <td><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$auctionDetails['ownerid']."' AND submitted=1 AND rate=4"); ?></td>
                     </tr>
                     <tr class="c2 neutral">
                        <td nowrap><img src="images/3stars.gif"></td>
                        <td><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$auctionDetails['ownerid']."' AND submitted=1 AND rate=3"); ?></td>
                     </tr>
                     <tr class="c3 negative">
                        <td nowrap><img src="images/2stars.gif"></td>
                        <td><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$auctionDetails['ownerid']."' AND submitted=1 AND rate=2"); ?></td>
                     </tr>
                     <tr class="c2 negative">
                        <td nowrap><img src="images/1stars.gif"></td>
                        <td><? echo getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$auctionDetails['ownerid']."' AND submitted=1 AND rate=1"); ?></td>
                     </tr>
                     <tr>
                        <td colspan="2" align="center"><a href="viewfeedback.php?owner=<?=$sellerDetails['id'];?>&auction=<?=$auctionDetails['id'];?>">
                           <?=$lang[view_fb];?>
                           </a></td>
                     </tr>
                  </table></td>
            </tr>
         </table></td>
   </tr>
</table>
<br>
<!-- Header Descriptions -->
<? header2(strtoupper($lang[descr]));?>
<!-- End Header Descriptions -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
   <tr> <a name="descr"></a>
      <td class="contentfont"><?	echo addSpecialChars($auctionDetails['description']); ?>
      </td>
   </tr>
   <tr>
      <td><? 
			$getPics=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$auctionDetails['id']."'"); 
			$nbPics=mysqli_num_rows($getPics); 
	 		if (file_exists($auctionDetails['picpath'])) { ?>
         <table width="100%" cellpadding="6" cellspacing="1" border="0">
            <tr align="center">
               <td valign="top" class="picselect"><table cellpadding="3" cellspacing="1" border="0">
                     <tr align="center">
                        <td><b>
                           <?=$lang[selectapicture];?>
                           </b></td>
                     </tr>
                     <tr align="center">
                        <td><a href="javascript:doPic('$setts[siteurl]/makethumb.php?pic=<?=$auctionDetails['picpath'];?>&w=500&sq=Y');"> <img src="$setts[siteurl]/makethumb.php?pic=<?=$auctionDetails['picpath'];?>&w=100&sq=Y" border="1"></a> </td>
                     </tr>
                     <?
							if ($nbPics>0) { 
								while ($addlPicture=mysqli_fetch_array($getPics)) { ?>
                     <tr>
                        <td><a href="javascript:doPic('$setts[siteurl]/makethumb.php?pic=<?=$addlPicture['name'];?>&w=500&sq=Y');"> <img src="$setts[siteurl]/makethumb.php?pic=<?=$addlPicture['name'];?>&w=60&sq=Y" border="1"></a> </td>
                     </tr>
                     <? } 
							} ?>
                  </table></td>
               <td width="100%" class="picselectmain"align="center"><img name="mainpic" src="$setts[siteurl]/makethumb.php?pic=<?=$auctionDetails['picpath'];?>&w=500&sq=Y" border="1" alt="<?=$auctionDetails['itemname'];?>"></td>
            </tr>
         </table>
         <? } ?>
      </td>
   </tr>
   <tr>
      <td align="center"><table cellpadding="3" cellspacing="1" border="0" class="counter">
            <tr>
               <td nowrap><?=$lang[itemviewed]?>
                  <strong>
                  <?=$auctionDetails['clicks'];?>
                  </strong>
                  <?=(($auctionDetails['clicks']==1) ? $lang[onetime] : $lang[times]);?>
               </td>
            </tr>
         </table></td>
   </tr>
</table>
<? if ($auctionDetails['videofile_path']!="") { 
	$video_name_raw = $auctionDetails['videofile_path'];
	$video_name = explode(".",$video_name_raw);
	$video_file_cnt = count($video_name) - 1;
	$video_extension = $video_name[$video_file_cnt];
	##$base_path = substr($_SERVER['SCRIPT_FILENAME'],0,count($_SERVER['SCRIPT_FILENAME'])-19);

	if (trim($video_extension)=="mov") {
		## run quicktime
		$display_video = '<OBJECT id="QTPlay" 
			CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" 
			CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab" 
			align="baseline" border="0"
			standby="Loading Video Player components..." 
			type="image/x-quicktime" WIDTH="480" HEIGHT="360" > 
        	<PARAM NAME="src" VALUE="http://movies.apple.com/movies/qt_posters/qtstart5a_480x228.mov"> 
	        <PARAM NAME="controller" VALUE="false"> 
	        <PARAM NAME="target" VALUE="myself"> 
	        <PARAM NAME="href" VALUE="'.$video_name_raw.'"> 
	        <PARAM NAME="pluginspage" VALUE="http://www.apple.com/quicktime/download/"> 
		</OBJECT> ';
	} else {
		## run windows media player
		/*$display_video = '<object id="WMPlay"
			classid="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95"
			codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715"
			align="baseline" border="0"
			standby="Loading Video Player components..."
			type="application/x-oleobject" width="480" height="360"> 
  			<param name="FileName" value="'.$video_name_raw.'"> 
  			<param name="ShowControls" value="1"> 
			<param name="ShowPositionControls" value="0"> 
			<param name="ShowAudioControls" value="1"> 
			<param name="ShowTracker" value="1"> 
			<param name="ShowDisplay" value="0"> 
			<param name="ShowStatusBar" value="1"> 
			<param name="AutoSize" value="0"> 
			<param name="ShowGotoBar" value="0"> 
			<param name="ShowCaptioning" value="0"> 
			<param name="AutoStart" value="0"> 
			<param name="AnimationAtStart" value="false"> 
			<param name="TransparentAtStart" value="false"> 
			<param name="AllowScan" value="1"> 
			<param name="EnableContextMenu" value="1"> 
			<param name="ClickToPlay" value="0"> 
			<param name="InvokeURLs" value="1"> 
			<param name="DefaultFrame" value="datawindow"> 
		</object> ';*/
		$display_video = '<object data="'.$video_name_raw.'" type="video/x-ms-wmv" 
       width="320" height="320">
       <param name="src" value="'.$video_name_raw.'">
  			<param name="ShowControls" value="1"> 
			<param name="ShowPositionControls" value="0"> 
			<param name="ShowAudioControls" value="1"> 
			<param name="ShowTracker" value="1"> 
			<param name="ShowDisplay" value="0"> 
			<param name="ShowStatusBar" value="1"> 
			<param name="AutoSize" value="0"> 
			<param name="ShowGotoBar" value="0"> 
			<param name="ShowCaptioning" value="0"> 
			<param name="AutoStart" value="0"> 
			<param name="AnimationAtStart" value="false"> 
			<param name="TransparentAtStart" value="false"> 
			<param name="AllowScan" value="1"> 
			<param name="EnableContextMenu" value="1"> 
			<param name="ClickToPlay" value="0"> 
			<param name="InvokeURLs" value="1"> 
			<param name="DefaultFrame" value="datawindow"> 
      </object>';
			
	} 
	?>
<br>
<? header2(strtoupper($lang[movie]));?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
   <tr>
      <td class="contentfont" align="center"><?	echo $display_video; ?>
      </td>
   </tr>
</table>
<? } ?>
<br>
<? if ($setts['enable_asq']=="Y") { ?>
<!-- Header Ask Seller a Question Section (Public and Private questions) -->
<? header2(strtoupper($lang[askseller]));?>
<? $getPublicMsg = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_public_msg WHERE auctionid='".$_GET['id']."' AND msgtype='Q'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

$ownerLogged = ($_SESSION['memberid']==$auctionDetails['ownerid']) ? TRUE : FALSE;

$isPublicMsg = mysqli_num_rows($getPublicMsg); 
if ($isPublicMsg>0) { ?>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
   <? while ($publicMsg = mysqli_fetch_array($getPublicMsg)) { ?>
   <tr>
      <td width="11" align="center"><img src="themes/<?=$setts['default_theme'];?>/img/system/q.gif"></td>
      <td width="60"><strong>
         <?=$lang[question];?>
         </strong></td>
      <td><?=$publicMsg['content'];?></td>
   </tr>
   <? 
	$answer = getSqlField("SELECT content FROM probid_public_msg WHERE answerid='".$publicMsg['id']."'","content");

  	if (!empty($answer) && !@eregi("n/a",$answer)) { ?>
   <tr>
      <td width="11" align="center"><img src="themes/<?=$setts['default_theme'];?>/img/system/a.gif"></td>
      <td width="60"><strong>
         <?=$lang[answer];?>
         </strong></td>
      <td><? echo $answer; ?></td>
   </tr>
   <? } ?>
   <? if ($ownerLogged) { ?>
   <tr>
      <td width="11" align="center"></td>
      <td width="60"></td>
      <form method="get">
         <td><? echo '<input type="button" value="'.$lang[submeditanswer].'" onClick="openPopup(\'popup_editpublicquestion.php?id='.$_GET['id'].'&answerid='.$publicMsg['id'].'\')"> '; ?></td>
      </form>
   </tr>
   <? } ?>
   <tr>
      <td align="center" colspan="3" class="c4"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? } ?>
</table>
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
   <? if ($_SESSION['membersarea']=="Active"&&$_SESSION['accsusp']!=2) { ?>
   <? if ($_SESSION['memberid']==$auctionDetails['ownerid']) { ?>
   <? if ($isPublicMsg>0) { ?>
   <tr>
      <td align="center"><?=$lang[plsanswerqs];?></td>
   </tr>
   <? } ?>
   <? } else if ($auctionDetails['closed']==1) { ?>
   <tr>
      <td align="center"><?=$lang[errorauctclosednoq];?></td>
   </tr>
   <? } else { ?>
   <form action="auctiondetails.php" method="get">
      <input type="hidden" name="id" value="<?=$_GET['id'];?>">
      <tr>
         <td><table width="100%" border="0" cellpadding="2" cellspacing="2" align="center" class="border">
               <tr class="contentfont" valign="top">
                  <td width="10"><img src="themes/<?=$setts['default_theme'];?>/img/system/i_faq.gif"></td>
                  <td align="right" width="20%"><strong>
                     <?=$lang[askseller];?>
                     </strong></td>
                  <td><textarea name="message" cols="40" rows="3" class="contentfont"></textarea></td>
                  <td width="80%"><div style="padding: 2px;">
                        <select name="question_type">
                           <? $publicQ = getSqlField("SELECT default_public_questions FROM probid_users WHERE id='".$auctionDetails['ownerid']."'","default_public_questions");
									if ($publicQ == "Y"&&$setts['enable_asq']=="Y") { ?>
                           <option value="public" selected>
                           <?=$lang[postqpublic];?>
                           </option>
                           <? } ?>
                          
                           <? ### Private question remove - ALEKSEI HODUNKOV ?>
                           
                           <?/*<option value="private">
                           <?=$lang[postqprivate];?>
                           </option>*/?>
                        </select>
                     </div>
                     <div style="padding: 2px;">
                        <input name="postquestionok" type="submit" id="postquestionok" value="<?=$lang[submit]?>" <? echo ($isBanned) ? "disabled" : ""; ?>>
                     </div></td>
               </tr>
            </table></td>
      </tr>
   </form>
   <? } ?>
   <? } else { ?>
   <tr>
      <td align="center" class="contentfont">[ <a href="<?=$path_ssl;?>login.php?auctionid=<?=$_GET['id'];?>">
         <?=$lang[logintopostqs];?>
         </a> ]</td>
   </tr>
   <? } ?>
</table>
<br>
<? }
$mainCat_primary = getMainCat($auctionDetails['category']);
$mainCat_secondary = getMainCat($auctionDetails['addlcategory']);

$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT f.boxid, f.boxname, f.boxtype, d.boxvalue FROM 
probid_fields_data d, probid_fields f WHERE d.auctionid='".$_GET['id']."' AND f.active='1' AND d.boxid = f.boxid AND 
(f.categoryid='".$mainCat_primary."' OR f.categoryid='".$mainCat_secondary."' OR f.categoryid='0') ORDER BY f.fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
$isFields = mysqli_num_rows($getFields);
if ($isFields) { ?>
<!-- Header Additional Custom Fields -->
<? header2(strtoupper($lang[addfields]));?>
<!-- End Header Additional Custom Fields -->
<table width="90%" border="0" cellspacing="1" cellpadding="4" align="center">
   <? while ($fields=mysqli_fetch_array($getFields)) { $toDisp = ""; ?>
   <tr class="<? echo (($count++)%2==0)?"c3":"c2"; ?>">
      <td align="right" width="50%"><strong>
         <?=$fields['boxname'];?>
         </strong></td>
      <td><?
			if ($fields['boxtype']=="checkbox") {
				$chkValues = explode(";",$fields['boxvalue']);
				for ($i=0; $i<count($chkValues); $i++) { 
					$chkRes = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".trim($chkValues[$i])."'","boxcaption");
					$toDisp .= ((trim($chkRes)!="n/a") ? $chkRes : "")."&nbsp; &nbsp; ";
				}	
			} else if ($fields['boxtype']=="radio") {
				$toDisp = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$fields['boxvalue']."'","boxcaption")." ";
			} else if ($fields['boxtype']=="list") {
				$toDisp = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$fields['boxvalue']."'","boxcaption")." ";
			} else {
				$toDisp = $fields['boxvalue'];
			}
			echo $toDisp;
			?>
      </td>
   </tr>
   <? } ?>
</table>
<br>
<? } ?>
<? if ($auctionDetails['acceptdirectpayment']) { ?>
<!-- Header Direct Payment -->
<? header2(strtoupper($lang[m_dir_pay_block]));?>
<!-- End Header Direct Payment -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
   <tr valign="top">
      <td class="c2" align="center"><?
			$acceptedPaymentSystems = explode(',',$auctionDetails['accept_payment_systems']);
			foreach($acceptedPaymentSystems as $v)	{
				if ($v) echo '<img src="themes/'.$setts[default_theme].'/img/system/pay_system_'.$v.'.gif">';
			} ?>
      </td>
   </tr>
</table>
<? } ?>
<!-- Header Payment -->
<? header2(strtoupper($lang[payment]));?>
<!-- End Header Payment -->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
   <tr valign="top">
      <td><?
			$paymentMethods=explode("<br>",addSpecialChars($auctionDetails['pm']));
			$nbPaymentMethods=count($paymentMethods);
			?>
         <table border="0" cellspacing="4" cellpadding="4" align="center">
            <? 
				for ($i=0;$i<$nbPaymentMethods;$i+=4) { 
					$j=$i+1;
					$k=$i+2;
					$l=$i+3; ?>
            <tr valign="top" align="center" style="font: bold;">
               <td><? 
						$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$i]."'","logourl"); 
		            echo $paymentMethods[$i].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
            </tr>
            <tr valign="top" align="center" style="font: bold;">   
               <td><? 
					  	$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$j]."'","logourl");
						echo $paymentMethods[$j].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
            </tr>
            <tr valign="top" align="center" style="font: bold;">
               <td><? 
		  				$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$k]."'","logourl");
		            echo $paymentMethods[$k].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
            </tr>
            <tr valign="top" align="center" style="font: bold;">
               <td><? 
					  	$paymentLogo=getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$paymentMethods[$l]."'","logourl");
		            echo $paymentMethods[$l].(($paymentLogo!=""&&$paymentLogo!="n/a")?"<br><img src=\"".$paymentLogo."\" border=0>":"");?></td>
            </tr>
            <? } ?>
         </table></td>
   </tr>
</table>
<a name="payment"></a>
<!-- Header SHIPPING -->
<? header2(strtoupper($lang[shipping]));?>
<!-- End Header SHIPPING -->
<table width="100%" border="0" cellspacing="1" cellpadding="10">
   <tr valign="top">
      <td><?
			if ($auctionDetails['sc']=="BP") echo "<li>".$lang[buyerpaysshipment]."</li>";
			else echo "<li>".$lang[sellerpaysshipment]."</li>";
			if ($auctionDetails['scint']=="Y") echo "<li>".$lang[sellershipinternat]."</li>"; 
			else echo "<li>".$lang[seller_not_shipinternat]."</li>";?>
      </td>
   </tr>
   <? if ($setts['shipping_costs']==1) { ?>
   <tr valign="top">
      <td class="contentfont"><strong>
         <?=$lang[postagecosts]?>
         :</strong>
         <?=displayAmount($auctionDetails['postage_costs'],$auctionDetails['currency'],"YES");?>
         <br>
         <strong>
         <?=$lang[insurance]?>
         :</strong>
         <?=displayAmount($auctionDetails['insurance'],$auctionDetails['currency'],"YES");?>
         <br>
         <strong>
         <?=$lang[servicetype]?>
         :</strong>
         <?=$auctionDetails['type_service'];?>
         <? if ($auctionDetails['shipping_details']!="") 
	  	echo "<br><br><strong>$lang[sp_details]</strong><br>".$auctionDetails['shipping_details'];
	  ?></td>
   </tr>
   <? } ?>
</table>
<? 
$getOtherItems = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions WHERE id!='".$auctionDetails['id']."' AND 
active=1 AND closed=0 AND deleted!=1 AND listin!='store' AND ownerid='".$auctionDetails['ownerid']."' ORDER BY rand()"); 
$nbOtherItems = mysqli_num_rows($getOtherItems);

if ($setts['nb_other_items_adp']>0&&$nbOtherItems>0) { 
	$numb = 0;
	$name = array();
	$id = array();
	$pic = array();
	$currency = array();
	$maxbid = array();
	$enddate = array();
	$bidstart = array();
	while ($otherItems = mysqli_fetch_array($getOtherItems)) {
		$id[$numb] = $otherItems['id'];
		$name[$numb] = $otherItems['itemname'];
		$pic[$numb] = $otherItems['picpath'];
		$maxbid[$numb] = $otherItems['maxbid'];
		$enddate[$numb] = $otherItems['enddate'];
		$bidstart[$numb] = $otherItems['bidstart'];
		$currency[$numb] = $otherItems['currency'];
		$numb++;
	} ?>
<br>
<table width=100% border=0 cellspacing=0 cellpadding=0>
   <tr>
      <td align='center' class='topitempage alertfont'><?=$lang[seller_assumes_resp1];?>
         <strong>
         <?=getSqlField("SELECT username FROM probid_users WHERE id='".$auctionDetails['ownerid']."'","username");?>
         </strong>,
         <?=$lang[seller_assumes_resp2];?>
      </td>
   </tr>
</table>
<br />
<!-- Header Other Items -->
<? header2(strtoupper($lang[otheritems]));?>
<table width="100%" border="0" cellspacing="1" cellpadding="10">
   <tr valign="top">
      <td class="contentfont"><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
               <? 
				$fct = 0;
				for ($k=0;$k<$setts['nb_other_items_adp'];$k++) { 
					$w=100/$setts['nb_other_items_adp'];
					$width=$w."%"; ?>
               <td width="<?=$width;?>" align="center" valign="top"><? if ($name[$fct]!="") { ?>
                  <table width="100%" border="0" cellspacing="5" cellpadding="3">
                     <tr>
                        <td height="110" align="center" class="gradient"><a href="<?=$site_url.processLink('auctiondetails', array('id' => $id[$fct]));?>"><img src="<? echo (($pic[$fct]!="")?"$setts[siteurl]/makethumb.php?pic=".$pic[$fct]."&w=".$layout['w_feat_hp']."&sq=Y":"images/noimg.gif");?>" border="0"></a> </td>
                     </tr>
                     <tr>
                        <td class="sell"><img src="themes/default/img/arrow1.gif" width="9" height="9" vspace="0" hspace="4" align="absmiddle"><a href="<?=$site_url.processLink('auctiondetails', array('id' => $id[$fct]));?>"> <? echo titleResize($name[$fct]);?> </a></td>
                     </tr>
                     <tr>
                        <td valign="top" class="smallfont"><strong>
                           <?=$lang[startbid];?>
                           :</strong> <? echo displayAmount($bidstart[$fct],$currency[$fct]);?><br>
                           <strong>
                           <?=$lang[currbid]?>
                           :</strong> <? echo displayAmount($maxbid[$fct],$currency[$fct]).'<BR>'.round((displayAmount($maxbid[$fct],$currency[$fct])/15.6466),2);?><br>
                           <strong>
                           <?=$lang[ends]?>
                           :</strong> <? echo displaydatetime($enddate[$fct],$setts['date_format']); $fct++; ?> </td>
                     </tr>
                  </table>
                  <? } ?></td>
               <? } ?>
            </tr>
         </table></td>
   </tr>
</table>
<? } ?>
<? if ($auctionDetails['closed']==0&&$daysLeft>0&&!$binAuction) { ?>
<!-- Header BID ON THIS ITEM -->
<a name="bid"></a>
<? header2(strtoupper($lang[bidthisitem]));?>
<!-- End Header BID ON THIS ITEM -->
<!-- Body BID ON THIS ITEM -->
<? if ($_SESSION['membersarea']!="Active") { 
	echo "<p align=\"center\"><b><font class='redfont'>".$lang[bid_errreg]."</font></b></p>";
	
	## Login box cut - Aleksei
	
	} else { 
	if (!$binAuction) { ?>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
   <form action="bid.php" method="post">
      <tr>
         <td colspan="2"><b>
            <?=$auctionDetails['itemname'];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr>
         <td><?=$lang[currbid]?>
            :</td>
         <td width="100%"><font class='redfont'><b><? echo displayAmount($auctionDetails['maxbid'],$auctionDetails['currency']);?></b></font></td>
      </tr>
      <? if ($auctionDetails['auctiontype']=="dutch") { ?>
      <tr>
         <td><?=$lang[quant]?>
            :</td>
         <td><input name="quantity" type="text" id="quantity" value="1" size="8"></td>
      </tr>
      <? } ?>
      <tr>
         <td nowrap><?=$lang[yourmaxbid]?>
            :</td>
         <td width="100%"><strong>
            <?=$auctionDetails['currency'];?>
            </strong>
            <input name="maxbid" type="text" id="maxbid" size="15">
            &nbsp;&nbsp;
            <?=$lang[minbid]?>
            :
            <? 
				if ($auctionDetails['auctiontype']=="standard") {
					if ($auctionDetails['maxbid']==0) $minimumBid = $auctionDetails['bidstart'];
					else $minimumBid = setMinBid($auctionDetails['maxbid'],$auctionDetails['bi'],$auctionDetails['bivalue'],$auctionDetails['auctiontype']);
					echo displayAmount($minimumBid,$auctionDetails['currency']);
				} else {
					$maximumBid = $auctionDetails['maxbid'];
					$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE 
					bidamount='".$maximumBid."' AND auctionid='".$auctionDetails['id']."'");
					$quantity = 0;
					while ($bids = mysqli_fetch_array($getBids)) {
						$quantity += $bids['quantity'];
					}
					if ($auctionDetails['maxbid']==0) $minimumBid=$auctionDetails['bidstart'];
					else if ($quantity<$auctionDetails['quantity']) $minimumBid=$auctionDetails['maxbid'];
					else $minimumBid=setMinBid($auctionDetails['maxbid'],$auctionDetails['bi'],$auctionDetails['bivalue'],$auctionDetails['auctiontype']);
					echo displayAmount($minimumBid,$auctionDetails['currency']);
				} ?>
            <input type="hidden" name="itemname" value="<?=remSpecialChars($auctionDetails['itemname']);?>">
            <input type="hidden" name="auctionid" value="<?=$auctionDetails['id'];?>">
            <input type="hidden" name="action" value="bid_CONF">
            <input type="hidden" name="quant_avail" value="<?=$auctionDetails['quantity'];?>">
         </td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td width="100%"><input name="placebidok" type="submit" id="placebidok" value="<?=$lang[placebid]?>" <? echo ($isBanned||$auctionDetails['ownerid']==$_SESSION['memberid']) ? "disabled" : ""; ?>></td>
      </tr>
   </form>
</table>
<? 			
					if ($auctionDetails['ownerid']==$_SESSION['memberid']&&$auctionDetails['closed']==0&&!$binAuction) 
						echo "<br><table width=100% border=0 cellspacing=0 cellpadding=0><tr><td align='center' class='topitempage alertfont'>$lang[biderr_youpost]</td></tr></table>";
		
				}  
			} 
		} 
		##else if ($auctionDetails['ownerid']==$_SESSION['memberid']&&$auctionDetails['closed']==0&&!$binAuction) echo $lang[biderr_youpost];
		else if ($auctionDetails['closed']!=0&&!$binAuction) echo $lang[biderr_bidclosed];
		else if ($daysleft<=0&&!$binAuction) echo $lang[biderr_bidclosed];
		else if ($binAuction) echo $lang[buynowonlyauctionalalert];
	}
} else { 
	header2($lang[auctdetserror]);
	echo "<p align=center><strong>$lang[aucterrormsg]</strong></p>";

	$auctionSusp = getSqlNumber("SELECT id FROM probid_auctions WHERE id='".$_GET['id']."' AND active='0'");

	$approvalCats = ','.$setts['approval_categories'].',';
	$isApprovalUser = getSqlField("SELECT auction_approval FROM probid_users WHERE id='".$_SESSION['memberid']."'", "auction_approval");
	$isApprovalCats = (@eregi($auctionDetails['category'], $approvalCats) || @eregi($auctionDetails['addlcategory'], $approvalCats)) ? 'Y' : 'N';
		
	$isApproval = (@eregi('Y', $isApprovalUser) || @eregi('Y', $isApprovalCats) || @eregi('Y', $setts['enable_auctions_approval'])) ? 'Y' : 'N';

	if (@eregi('Y', $isApproval)) echo "<p align=center><strong>$lang[auctionawaitsapprovalmsg]</strong></p>";
	else if ($auctionSusp) echo "<p align=center><strong>$lang[aucterrorsellermsg]</strong></p>";
}
include ("themes/".$setts['default_theme']."/footer.php"); ?>
