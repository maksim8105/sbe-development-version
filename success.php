<?
## v5.24 -> apr. 05, 2006
session_start();

// Для отображения данной страницы пользователь должен быть залогинен
if ( $_SESSION['membersarea'] != "Active" && $_SESSION['accsusp'] != 2 )
{
	echo "<script>document.location.href='login.php?redirect=authenticate'</script>";
	exit;
}

include_once ("config/config.php");

// Подключаем базовый класс Pangalink'а
include_once 'pangalink/Pangalink.class.php';

// Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
$Pangalink =& new Pangalink( 'config/pangalink.ini', true );

if ( ! $Pangalink->hasPangalinkReply() )
{
	echo "<script>document.location.href='authenticate.php'</script>";
	exit;
}

function get_value( $string, $num = 1, $separator = ':', $default = '' )
{
	$result = explode( $separator, $string );

	return isset( $result[ $num ] ) ? $result[ $num ] : $default;
}

// Инициализировать пангалинк. Скрипт сам определит от какого банка был получен ответ и можно ли его обработать
if ( false === ( $pank = $Pangalink->initializePangalink() ) )
{
	// Ошибка инициализации пангалинка. Получены неверные данные или данные не получены
	echo "<script>document.location.href='authenticate.php'</script>";
	exit;
}

include ("themes/".$setts['default_theme']."/header.php");
header5("$lang[authentication]"); ?>
<table width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr class="contentfont">
     <td>
     <p>&nbsp;</p>
     <p><strong><?php
     
/*echo '<pre>';
print_r( $_REQUEST );
echo '</pre>';*/

if ( $pank->validateForm( $_REQUEST, 3002 ) )
{
	## FIX FOR SEB Uhispank - ALEKSEI
	
	/*
	$vk_info = explode( ';', $_REQUEST['VK_INFO'] );

	$isikukood = get_value( $vk_info[0] );
	$isikukood = mysql_real_escape_string( $isikukood );

	$realname = isset( $vk_info[1] ) ? get_value( $vk_info[1] ) : '';
	$realname = mysql_real_escape_string( $realname );*/
	
	## End of fix
	
	$isikukood = $realname = '';
	$vk_info = explode( ';', $_REQUEST['VK_INFO'] );
	
	foreach( $vk_info as $field )
	{
        $result = explode( ':', $field );

        if ( 2 == count( $result ) )
        {
                list( $key, $value ) = $result;

                $key = strtoupper( trim( $key ) );
                $value = trim( $value );

                if ( 'NIMI' == $key )
                {
                        $realname = $value;
                }
                elseif ( 'ISIK' == $key || 'KOOD' == $key )
                {
                        $isikukood = $value;
                }
        }
	}
	
   /*$isikukood = $realname = '';
   $vk_info = explode( ';', $_REQUEST['VK_INFO'] );

   foreach( $vk_info as $field )
   {
       $result = explode( ':', $field );

       if ( 2 == count( $result ) )
       {
               list( $key, $value ) = $result;

               $key = strtoupper( trim( $key ) );
               $value = trim( $value );

               if ( 'ID' == $key )
               {
                        $isikukood = $value;
               }
               else
               {
                       $realname = $value;
               }
       }
   } */

	$isikukood = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"],  $isikukood ) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
	
	##$realname = mysql_real_escape_string( $realname );
	
	$realname = ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"],  iconv( 'ISO-8859-4', 'UTF-8', $realname ) ) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));
	
	## END OF FIX - ALEKSEI

	##mysql_query( "UPDATE `probid_users` SET `authenticated` = 'Y', `isikukood` = '$isikukood', `realname` = '$realname' WHERE id='{$_SESSION['memberid']}'");

	mysqli_query($GLOBALS["___mysqli_ston"],  "UPDATE `probid_users` SET `authenticated` = 'Y', `isikukood` = '$isikukood', `realname` = '$realname' WHERE id='{$_SESSION['memberid']}'") or die( ((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)) );
  
  
  
  
  
  
  // INVITE BONUS AUTH DONE
  $myemail = getSqlField("SELECT email FROM probid_users WHERE id=".$_SESSION['memberid']."","email");
  mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users_friends_invites SET status_auth=1 WHERE email='".$myemail."'");
  
  // Referer ID
  
  $refid = getSqlField("SELECT refid FROM probid_users_friends_invites WHERE email='".$myemail."'","refid");
  
  
  // Send Message To Referer
  
  $messageText = $lang[message_digital_agent_header].$_SESSION["membername"]."(".$_SESSION["memberusern"].") ".$lang[message_digital_agent_auth];
  $messageTitle = $lang[message_header_auth];
  
  $send_sql = "INSERT INTO probid_messages(senderid,receiverid,messagetype,message,messagetitle,opened,datentime) VALUES(28,".$refid.",1,'".$messageText."','".$messageTitle."',0,'".strtotime(date("Y-m-d H:i:s"))."')";
  @mysqli_query($GLOBALS["___mysqli_ston"], $send_sql);
  $id_msg = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
  
    // REFERER DATA
    
    $ref_mail = getSqlField("SELECT email FROM probid_users WHERE id=".$refid."","email");   
    $ref_name = getSqlField("SELECT name FROM probid_users WHERE id=".$refid."","name");   
    
    // SEND mail to referer
    
  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers.= "Content-type: text/plain; charset=utf-8" . "\r\n";
  // Additional headers
  $headers.= 'From: SBE.EE  <noreply@sbe.ee>' . "\r\n";
  $headers.= 'Cc: noreply@sbe.ee' . "\r\n";
  $headers.= 'Bcc: noreply@sbe.ee' . "\r\"";  
  
  $subject = $lang[message_header_auth];
  
  $mail = $lang[letter_digital_agent_header].$_SESSION["memberusern"].$lang[letter_digital_agent_auth];
  $mail .="\n http://www.sbe.ee/membersarea.php?page=messages&msgid=".$id_msg."&msgsid=".md5($id_msg);
  mail($ref_mail, $subject, $mail, $headers); 
  
    // Add money to user [!]
  
 $currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$refid."'","balance");
 $updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET balance='".($currentBalance-2)."' WHERE id='".$refid."'");
  
 $addTransaction = mysqli_query($GLOBALS["___mysqli_ston"], 
 "INSERT INTO probid_users_transactions(userid,name,description,amount,balance,tdate,op) 
 VALUES(".$refid.",
 'SBE',
 \
 '".$lang[auth_fee]."',
 2,
 ".abs($currentBalance-2).",
 '".date("Y-m-d H:i:s")."',
 'in'
 ) ");

	echo $lang['authentication_successful'];
}
else
{
	
/*$fp = fopen("test.txt", "w");
$testdata = "Test!!!\n";
fwrite($fp, $testdata); 
fclose($fp);*/

	echo $lang['authentication_unsuccessful'];
}

     ?></strong></p>
     <p>&nbsp;</p>
     </td>
   </tr>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>
