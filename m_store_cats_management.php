<? 
## v5.24 -> apr. 06, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }
?>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">    <!--    
	function MoveOption(objSourceElement, objTargetElement) {        
		var aryTempSourceOptions = new Array();        
		var x = 0;                //looping through source element to find selected options        
		for (var i = 0; i < objSourceElement.length; i++) {            
			if (objSourceElement.options[i].selected) {                //need to move this option to target element                
				var intTargetLen = objTargetElement.length++;                
				objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;                
				objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;            
			} else {                //storing options that stay to recreate select element                
				var objTempValues = new Object();                
				objTempValues.text = objSourceElement.options[i].text;                
				objTempValues.value = objSourceElement.options[i].value;                
				aryTempSourceOptions[x] = objTempValues;                
				x++;            
			}        
		}                //resetting length of source        
		objSourceElement.length = aryTempSourceOptions.length;        //looping through temp array to recreate source select element        
		for (var i = 0; i < aryTempSourceOptions.length; i++) {            
			objSourceElement.options[i].text = aryTempSourceOptions[i].text;            
			objSourceElement.options[i].value = aryTempSourceOptions[i].value;            
			objSourceElement.options[i].selected = false;        
		}    
	}    //-->    
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript"> <!-- 
	function SelectOption(objTargetElement) { 	
		for (var i = 0; i < objTargetElement.length; i++) { 
			objTargetElement.options[i].selected = true; 
		} 
	} //--> 
</SCRIPT>
<?
if ($_SESSION['membersarea']=="Active"&&$_SESSION['is_seller']=="Y"&&$setts['stores_enabled']=="Y") { 
	$noDisplay = FALSE;
			
	if (isset($_POST['generatorok'])) {
		$getAllCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE userid='".$_SESSION['memberid']."' ORDER BY parent ASC");
		while ($gencat = mysqli_fetch_array($getAllCats)) { 
			$isSubcat=0;
			$isSubcat = getSqlNumber("SELECT id FROM probid_categories WHERE parent='".$gencat['id']."'");
			if ($isSubcat>0) $updateCat = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET issubcat='>' WHERE id='".$gencat['id']."'");
		}
	}
				
	if (isset($_POST['storesaveok'])) { 
		$categoriesFilter="";
		for ($i=0;$i<count($_POST['categories']);$i++) {
			$categoriesFilter.=$_POST['categories'][$i].",";
		}
		$updateAboutSetts = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		store_categories='".$categoriesFilter."'   
		WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
				
	if (isset($_POST['custom_cats_save'])) {
		$_SESSION['cats']="1";
		$c=10000;
		$order = $_POST['order'];

		for ($i=0;$i<count($_POST['name']);$i++) {
			if (!empty($order[$i])=="") {
				$order[$i] = $c; 
				$c++;
			}
	
			$updCatsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET 
			name='".remSpecialChars($_POST['name'][$i])."', theorder='".$order[$i]."' 
			WHERE id='".$_POST['id'][$i]."' AND userid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	
		$isMainCat = ($_POST['parent']==0) ? "Y" : "N";
	
		for ($i=0;$i<count($_POST['delete']);$i++) {
			$delCatsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_categories 
			WHERE id='".$_POST['delete'][$i]."' AND userid='".$_SESSION['memberid']."'");
		}
	
		$searchSubsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent!=0 AND userid=0");
		while ($subcatsArray = mysqli_fetch_array($searchSubsQuery)) {
			if (getSqlNumber("SELECT * FROM probid_categories WHERE id='".$subcatsArray['parent']."' AND userid=0")==0) {
				$deleterow=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_categories WHERE parent='".$subcatsArray['parent']."'");
			}
		}
	
		for ($i=0; $i<count($_POST['newname']); $i++) {	
			if ($_POST['newname'][$i]!="") {
				$c++;
				$insertCatQuery = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_categories 
				(parent, name, theorder, userid) VALUES 
				('".$_REQUEST['parent']."','".remSpecialChars($_POST['newname'][$i])."','".$c."', '".$_SESSION['memberid']."')");
			}
		}
	}
			
	$shopDetails=getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'"); 
			
	if (!$noDisplay) { ?>
   <? if ($_SESSION['cats']=="1"){?>
   <table width="100%" class="c4 border" cellpadding="4" cellspacing="4">
      <tr>
         <td align="center" class="contentfont"><?=$lang[storemodifmsg];?> </td>
      </tr>
   </table>
   <? } ?>
<form action="membersarea.php?page=cats_management" method="post" name="MoveList" onSubmit="SelectOption(this.categories)">
   <input type="hidden" name="parent" value="<?=$_REQUEST['parent'];?>">
   <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border">
      <tr>
         <td colspan="2" align="center" class="c1"><?=$lang[store_setup_page]?> - <?=$lang[catsmanagement];?></td>
      </tr>
      <? $storeInfo = storeAccountType($shopDetails['store_account_type']); ?>
      <tr class="c2">
         <td class="contentfont"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
               <tr>
                  <td class="contentfont"><a href="membersarea.php?page=store">
                     <?=$lang[storemainsetts]?>
                     </a> | <a href="membersarea.php?page=store_pages">
                     <?=$lang[storepages]?>
                     </a> | <a href="membersarea.php?page=cats_management">
                     <?=$lang[catsmanagement]?></td>
               </tr>
            </table></td>
      </tr>
      <tr valign="top">
         <td><?
				if($_REQUEST['parent'] > 0) {
					$croot = $_REQUEST['parent'];
					$nav = "";
					$cntr = 0;
					while ($croot>0) {
						$crw = getSqlRow("SELECT * FROM probid_categories WHERE id='$croot' AND userid='".$_SESSION['memberid']."'");
						if($cntr == 0) {
							$nav = $crw['name'];
						} else {
							if($_REQUEST['parent'] != $croot) {
								$nav = "<a href=\"".$_SERVER['PHP_SELF']."?page=cats_management&parent=".$crw['id']."&name=".$crw['name']."\">$crw[name]</a> > ".$nav;
							}
						}
						$cntr++;
						$croot = $crw['parent'];
					}
					echo "<A HREF=\"membersarea.php?page=cats_management\"><b> $lang[category]:</b></A> ".$nav;
				} ?>
         </td>
      </tr>
      <? $categoriesQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories 
	  	WHERE parent='".$_REQUEST['parent']."' AND userid='".$_SESSION['memberid']."' ORDER BY theorder ASC, name ASC"); ?>
      <tr class="c4">
         <td class="contentfont"><strong><?=$lang[storecatssubtitle];?></strong></td>
      </tr>
      <tr>
         <td class="contentfont"><?=$lang[storecatsmanagnotea];?></td>
      </tr>
      <tr>
         <td class="contentfont"><?=$lang[storecatsmanagnoteb];?></td>
      </tr>
      <tr>
         <td align="center" class="contentfont"><table width="100%" border="0" cellpadding="4" cellspacing="2" class="contentfont border">
               <tr class="c4">
                  <td width="20">&nbsp;</td>
                  <td><strong>
                     <?=$lang[cat_name];?>
                     </strong></td>
                  <td width="150" align="center"><strong>
                     <?=$lang[order];?>
                     </strong></td>
                  <td width="150" align="center"><strong>
                     <?=$lang[delete];?>
                     </strong></td>
               </tr>
               <? while ($categoriesArray = mysqli_fetch_array($categoriesQuery)) { 
		  				## get conflicts
						$isConflict = getSqlNumber("SELECT id FROM probid_categories WHERE name LIKE '".$categoriesArray['name']."%' AND parent='".$_REQUEST['parent']."'"); ?>
               <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>">
                  <td><a href="<?=$_SERVER['PHP_SELF'];?>?page=cats_management&parent=<?=$categoriesArray['id'];?>"><img src="admin/images/catplus.gif" alt="<?=$lang[category_subalt];?>" width="20" height="20" border="0" /></a></td>
                  <td><input name="name[]" type="text" id="name[]" value="<?=$categoriesArray['name'];?>" style="width:75%" />
                     <input type="hidden" name="id[]" value="<?=$categoriesArray['id'];?>" />
                     <? echo ($isConflict>1)?"<br>$lang[conflict_cat_warning]":""; ?> </td>
                  <? if ($categoriesArray['theorder']>=1000||$categoriesArray['theorder']==0) $categoriesArray['theorder']=""; ?>
                  <td align="center"><input name="order[]" type="text" id="order[]" value="<?=$categoriesArray['theorder'];?>" size="8" /></td>
                  <td align="center"><input name="delete[]" type="checkbox" id="delete[]" value="<?=$categoriesArray['id'];?>" /></td>
               </tr>
               <? } ?>
               <tr class="c4">
                  <td>&nbsp;</td>
                  <td><strong>
                     <?=$lang[add];?>
                     </strong></td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
               </tr>
               <tr class="c2">
                  <td>&nbsp;</td>
                  <td><?
							## addon so more categories can be added simultaneously
							$newAuctsCounter = 5;
							for ($i=0; $i<$newAuctsCounter; $i++) echo "<input name=\"newname[]\" type=\"text\" id=\"newname[]\"><br>\n"; ?></td>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
               </tr>
               <tr class="c4">
                  <td colspan="4" align="center"><input type="submit" name="custom_cats_save" value="<?=$lang[savesetts];?>" />
                     &nbsp; 
                     &nbsp;
                     <input name="generatorok" type="submit" value="<?=$lang[BUTT_CATS_GENERATOR];?>"></td>
               </tr>
            </table>
            <br />
            <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border contentfont">
               <tr>
                  <td><p>Once generated, your customised categories will appear with the main categories listed below. To restrict what categories appear in your store, select the ones you want to use.</p>
                     <p><strong>Note: </strong>If you don't select any categories for your store, all the site's categories will be available by default. Otherwise you will only be able to list items in the categories you have selected for your store. </p></td>
               </tr>
               <tr class="c2">
                  <td class="contentfont"><table width="100%" border="0" cellspacing="2" cellpadding="2">
                        <tr>
                           <td>&nbsp;</td>
                           <td width="35%">[ All categories ] </td>
                           <td width="10%">&nbsp;</td>
                           <td width="35%">[ Store categories ] </td>
                        </tr>
                        <? $storeCats = (!empty($shopDetails['store_categories'])) ? substr($shopDetails['store_categories'],0,-1) : 0; ?>
                        <tr>
                           <td valign="top" align="right"><strong>Store Categories</strong> </td>
                           <td><select name="store_categories" size="10" multiple="multiple" id="store_categories" style="width: 100%;">
                                 <? $getcats=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent='0' AND (userid=0 OR userid='".$_SESSION['memberid']."') AND id NOT IN (".$storeCats.") ORDER BY theorder ASC, name ASC");
											while ($cat_row=mysqli_fetch_array($getcats)) {
												echo "<option value=\"".$cat_row['id']."\">".$cat_row['name']."</option>";
											} ?>
                              </select></td>
                           <td align="center"><input type="button" name="Disable" value=" -&gt; " style="width: 50px;" onClick="MoveOption(this.form.store_categories, this.form.categories)">
                              <br>
                              <br>
                              <input type="button" name="Enable" value=" &lt;- " style="width: 50px;" onClick="MoveOption(this.form.categories, this.form.store_categories)"></td>
                           <td><select name="categories[]" size="10" multiple="multiple" id="categories" style="width: 100%;">
                                 <? $getcats=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent='0' AND (userid=0 OR userid='".$_SESSION['memberid']."') AND id IN (".$storeCats.") ORDER BY theorder ASC, name ASC");
											while ($cat_row=mysqli_fetch_array($getcats)) {
												echo "<option value=\"".$cat_row['id']."\" selected>".$cat_row['name']."</option>";
											} ?>
                              </select></td>
                        </tr>
                     </table></td>
               </tr>
               <tr class="c4">
                  <td colspan="2" align="center" class="contentfont"><input type="submit" name="storesaveok" value="<?=$lang[savesetts]?>"></td>
               </tr>
            </table></td>
      </tr>
   </table>
</form>
<br>
<? 
	} ## end of noDisplay check
} else if ($_SESSION['is_seller']!="Y") {
	echo $lang[seller_error]; 
} else { 
	echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; 
} ?>