<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

// FormValidator.class.inc
// class to perform form validation

class FormValidator
{
	//
	// private variables
	// 
	
	var $_errorList;

	//
	// methods (private)
	// 
	
	// function to get the value of a variable (field)
	function _getValue($field)
	{
		global ${$field};
		return ${$field};
	}

	//
	// methods (public)
	// 

	// constructor
	// reset error list
	function FormValidator()
	{
		$this->resetErrorList();
	}
	
	function isChecked($field, $msg)
	{
		$value = $this->_getValue($field);
		if (strcmp($value,"yes")!=0)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is empty
	function isEmpty($field, $msg)
	{
		$value = $this->_getValue($field);
		if (trim($value) == "")
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	function passConfirm($field1, $field2, $msg) {
		$pass=$this->_getValue($field1);
		$cnfpass=$this->_getValue($field2);
		if(strcmp($pass,$cnfpass)!=0) 
		{
			$this->_errorList[] = array("field" => $field1, "value" => $pass, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a string
	function isString($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_string($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a valid gif or jpg file
	function isImage($field, $msg)
	{
	
		$value = $this->_getValue($field);
		if(($value=="image/gif")||($value=="image/jpeg")||($value=="image/pjpeg"))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	// check whether input is a number
	function isNumber($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is an integer
	function isInteger($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_integer($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a float
	function isFloat($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_float($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is alphabetic
	function isAlpha($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^[a-zA-Z0-9 ]+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	function isHtml($field, $msg)
	{
		$value = $this->_getValue($field);
		$fieldConverted = htmlspecialchars($value);
		if(@ereg('^([а-яА-Яa-zA-Z0-9@*#\"\'])', $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	function hasJs($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!@eregi('<script', $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	function hasIframes($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!@eregi('<iframe', $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	function isWithinLength($field, $msg, $min, $max)
	{
		$value = $this->_getValue($field);
		if((strlen($value)<$min)||(strlen($value)>$max))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is within a valid numeric range
	function isWithinRange($field, $msg, $min, $max)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value) || $value < $min || $value > $max)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a valid email address
	function isEmailAddress($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}
	
	// return the current list of errors
	function getErrorList()
	{
		return $this->_errorList;
	}
	
	// check whether any errors have occurred in validation
	// returns Boolean
	function isError()
	{
		if (sizeof($this->_errorList) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// reset the error list
	function resetErrorList()
	{
		$this->_errorList = array();
	}
	
// end 
}

?>
