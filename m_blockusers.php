<?
## v5.24 -> apr. 04, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

if ($_GET['option']=="delete") {
	$deleteBlockedUser = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_blocked_users WHERE id='".$_GET['id']."' AND ownerid='".$_SESSION['memberid']."'");
}

if (isset($_POST['modifyuser'])&&!empty($_POST['username'])) {
	$isUser = getSqlNumber("SELECT id FROM probid_users WHERE username='".$_POST['username']."'");
	if ($isUser) {
		$userDetails = getSqlRow("SELECT id, username FROM probid_users WHERE username='".$_POST['username']."'");
		
		$isBlocked = getSqlNumber("SELECT id FROM probid_blocked_users WHERE userid='".$userDetails['id']."' AND 
		username='".$userDetails['username']."' AND ownerid='".$_SESSION['memberid']."'");
		
		if ($isBlocked) {
			$updateBlockedUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_blocked_users SET block_reason='".remSpecialChars($_POST['block_reason'])."', 
			min_fb='".$_POST['min_fb']."', show_reason='".$_POST['show_reason']."'  WHERE 
			id='".$_REQUEST['id']."'AND ownerid='".$_SESSION['memberid']."'");
			$outputMsg = "<p align=center>The record has been updated successfully</p>";
		} else {
			$insertBlockedUser = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_blocked_users 
			(userid, username, ownerid, block_reason, regdate, min_fb, show_reason) VALUES
			('".$userDetails['id']."', '".$userDetails['username']."', '".$_SESSION['memberid']."', 
			'".remSpecialChars($_POST['block_reason'])."', '".$currentTime."', '".$_POST['min_fb']."', 
			'".$_POST['show_reason']."')");
			$outputMsg = "<p align=center>The record has been added successfully</p>";
		}
	} else $outputMsg = "<p align=center>ERROR: The user does not exist!</p>";
}

echo $outputMsg;
?>

<table width="100%" border="0" cellspacing="1" cellpadding="3">
   <tr class="c1">
      <td width="120"><?=$lang[username]?></td>
      <td align="center"><?=$lang[reason]?></td>
      <td width="80" align="center"><?=$lang[show_reason]?></td>
      <!--<td width="80" align="center"><?=$lang[fb_treshold]?></td>-->
      <td width="100" align="center"><?=$lang[options]?></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <!--<td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>-->
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   </tr>
   
   <? 
  	$getBlocked = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_blocked_users WHERE ownerid='".$_SESSION['memberid']."'");
  	while ($blocked=mysqli_fetch_array($getBlocked)) { ?>
   <tr class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>">
      <td><?=$blocked['username'];?></td>
      <td class="smallfont"><?=$blocked['block_reason'];?></td>
      <td align="center"><? echo ($blocked['show_reason']) ? "<font color=green>$lang[yes]</font>" : "<font color=red>$lang[no]</font>" ;?></td>
      <!--<td align="center" class="smallfont"><?=$blocked['min_fb'];?></td>-->
      <td align="center" class="smallfont">
			[ <a href="membersarea.php?page=block_users&option=edit&id=<?=$blocked['id'];?>"><?=$lang[edit];?></a> ]<br />
			[ <a href="membersarea.php?page=block_users&option=delete&id=<?=$blocked['id'];?>"><?=$lang[delete];?></a> ]
		</td>
   </tr>
   <? } ?>
</table>
<br />

<table width="550" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
	<? 
	if ($_REQUEST['option']=="edit") 
		$blockedDets = getSqlRow("SELECT * FROM probid_blocked_users WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
	?>
   <form action="membersarea.php?page=block_users" method="post">
		<input type="hidden" name="id" value="<?=$blockedDets['id'];?>" />
      <tr class="c1">
         <td colspan="2" align="center"><?=$lang[addblockeduser]?></td>
      </tr>
      <tr class="c3">
         <td width="30%" align="right"><strong>
            <?=$lang[username]?>
         </strong></td>
         <td width="70%"><input type="text" name="username" value="<?=$blockedDets['username'];?>" <? echo ($_REQUEST['option']=="edit") ? "readonly" : ""; ?> /></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[block_reason]?>
         </strong></td>
         <td><textarea name="block_reason" cols="50" rows="5" id="block_reason"><?=$blockedDets['block_reason'];?></textarea></td>
      </tr>
      <tr class="c3">
         <td align="right"><?=$lang[show_reason]?></td>
         <td><input name="show_reason" type="radio" value="1" <? echo ($blockedDets['show_reason']==1) ? "checked" : ""; ?> /> <?=$lang[yes];?>
         <input name="show_reason" type="radio" value="0" <? echo ($blockedDets['show_reason']!=1) ? "checked" : ""; ?> /> <?=$lang[no];?></td>
      </tr>
      <!--
		<tr class="c2">
         <td align="right">Feedback Treshold</td>
         <td><input type="text" name="min_fb" size="7" value="<?=$blockedDets['min_fb'];?>" /></td>
      </tr>
		-->
      <tr class="c4">
         <td>&nbsp;</td>
         <td><input name="modifyuser" type="submit" id="modifyuser" value="<?=$lang[submit];?>" /></td>
      </tr>
   </form>
</table>
<? } ?>
