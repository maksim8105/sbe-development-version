<?
## v5.24 -> apr. 12, 2006
session_start();
if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php'</script>";
} else {

  include_once ("config/config.php");
  include_once ("download.functions.php");

  switch ($_POST["action"]) {
  
  
  // DELETE DOWNLOADS
  case "delete":
    if (count($_POST["downloads"]) > 0) {
    
        foreach ($_POST["downloads"] as $id) {
            $i++;
            $ids.=$id.($i<count($_POST["downloads"])?",":"");
        }
        mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auctions_downloads WHERE id IN(".$ids.") AND owner_id=".$_SESSION["memberid"]."");
    
    }
  break;
  // INSERT DOWNLOADS
  case "insert":
  
    // 1-step: VALIDATE DATA
  
  $errors = validateForm($_POST);

  if (count($errors)==0) {
  
    // 2-step: INSERT DATA
    
    $list_id = 1;
    
    $d = substr($_POST["item_startdate"],0,2);
    $m = substr($_POST["item_startdate"],3,2);
    $y = substr($_POST["item_startdate"],6,4);
    
    $start_date =  $y."-".$m."-".$d." ".$_POST["date_h"].":".$_POST["date_m"].":".date("s");
    $run = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DATE_ADD('".$start_date."', INTERVAL ".$_POST["item_duration"]." DAY)");
    $row = mysqli_fetch_row($run);
    $end_date = $row[0];
    
    $s_inc = "NO";
    if ($_POST["item_shipincl"]=="on") $s_inc = "YES";
    
    $insert_sql = "INSERT INTO probid_auctions_downloads(owner_id, list_id, itemname, description, category_id, addcat_id, country, quantity,
    price, reserve_price, buy_now, currency, start_date, end_date, duration, s_included, pat_details
    ) VALUES(
    ".$_SESSION["memberid"].",".$list_id.",'".$_POST["item_name"]."','".$_POST["item_desc"]."',".$_POST["item_category"].",".intval($_POST["item_addcat"]).",'".$_POST["item_location"]."','".$_POST["item_quantity"]."',
    '".$_POST["item_price"]."','".$_POST["item_reserve_price"]."','".$_POST["item_buynow"]."','".$_POST["item_currency"]."','".$start_date."','".$end_date."',".$_POST["item_duration"].",'".$s_inc."','".$_POST["item_psdetails"]."'
    )";
    
    $run = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT ".$start_date." + INTERVAL ".$_POST["item_duration"]." DAY");
    
    $result = mysqli_query($GLOBALS["___mysqli_ston"], $insert_sql);
    $new_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);

    /* UPLOAD - IMAGES - UPLOAD */

  $num = 5;
  for ($i=1;$i<$num;$i++) {
  
    if ($_FILES["item_pic".$i]["error"]==0) {   
    
      $filename[$i] = stripslashes($_FILES["item_pic".$i]["name"]);
      $extension[$i] = getExtension($filename[$i]);
      $imagename[$i] = "pic_".$i."".$extension[$i];
      define("DIR_PHOTOS_UPLOAD",$_SERVER['DOCUMENT_ROOT']."images_temp/".$_SESSION["memberid"]."/".$new_id."/");
       mkdir("./images_temp/".$_SESSION["memberid"]."/".$new_id."/",0777);
       mkdir("./images_temp/".$_SESSION["memberid"]."/".$new_id."/thumb/",0777);
      mkdir("./test",0777);
      // $name = (strtotime(date("Y-m-d H:i:s"))+999999999); IMAGE NAME DATABASE ?!
      
      $source[$i] = $_FILES["item_pic".$i]['tmp_name'];
      $target[$i] = DIR_PHOTOS_UPLOAD.$imagename[$i];

    if (move_uploaded_file($source[$i], $target[$i])) {
    
    $imagepath[$i] = $imagename[$i];
    
    $save[$i] = DIR_PHOTOS_UPLOAD. $imagepath[$i]; //This is the new file you saving
    $file[$i] = DIR_PHOTOS_UPLOAD. $imagepath[$i]; //This is the original file
    
    $small_save[$i] = DIR_PHOTOS_UPLOAD."thumb/".$imagepath[$i]; //This is the new file you saving
    $small_file[$i] = DIR_PHOTOS_UPLOAD.$imagepath[$i]; //This is the new file you saving

    list($width[$i], $height[$i]) = getimagesize($file[$i]) ; // Original W x H

    $modwidth[$i] = 700;
    $w[$i] = 120;
    

    $diff[$i] = $width[$i] / $modwidth[$i];

    $modheight[$i] = $height[$i] / $diff[$i]; 
    $tn[$i] = imagecreatetruecolor($modwidth[$i], $modheight[$i]);
    $dest[$i] = imagecreatetruecolor($w[$i],$w[$i]); 

    if ($extension[$i]=='jpg' || $extension[$i]=='jpeg') {
        $image[$i] = imagecreatefromjpeg($file[$i]);
        imagecopyresampled($tn[$i], $image[$i], 0, 0, 0, 0, $modwidth[$i], $modheight[$i], $width[$i], $height[$i]);
        imagejpeg($tn[$i], $save[$i],100);  
        
        if ($width[$i]>$height[$i]) {
          imagecopyresampled($dest[$i], $image[$i], 0, 0,round((max($width[$i],$height[$i])-min($width[$i],$height[$i]))/2),0, $w[$i], $w[$i], min($width[$i],$height[$i]), min($width[$i],$height[$i]));
        } 
        if ($width[$i]<$height[$i]) {
          imagecopyresampled($dest[$i], $image[$i], 0, 0, 0, 0, $w[$i], $w[$i],min($width[$i],$height[$i]), min($width[$i],$height[$i])); 
        }
        if ($width[$i]==$height[$i]) {
          imagecopyresampled($dest[$i], $src[$i], 0, 0, 0, 0, $w[$i], $w[$i], $width[$i], $height[$i]); 
        }
          imagejpeg($dest[$i], $small_save[$i],100);   
    } elseif ($extension[$i]=='gif') {

        $image[$i] = imagecreatefromgif($file[$i]); 
        imagecopyresampled($tn[$i], $image[$i], 0, 0, 0, 0, $modwidth[$i], $modheight[$i], $width[$i], $height[$i]) ; 
        imagegif($tn[$i], $save[$i], 100) ; 
        
        if ($width[$i]>$height[$i]) {
          imagecopyresampled($dest[$i], $image[$i], 0, 0,round((max($width[$i],$height[$i])-min($width[$i],$height[$i]))/2),0, $w[$i], $w[$i], min($width[$i],$height[$i]), min($width[$i],$height[$i]));
        } 
        if ($width[$i]<$height[$i]) {
          imagecopyresampled($dest[$i], $image[$i], 0, 0, 0, 0, $w[$i], $w[$i],min($width[$i],$height[$i]), min($width[$i],$height[$i])); 
        }
        if ($width[$i]==$height[$i]) {
          imagecopyresampled($dest[$i], $src[$i], 0, 0, 0, 0, $w[$i], $w[$i], $width[$i], $height[$i]); 
        }
          imagegif($dest[$i], $small_save[$i],100);   
    }

    
    imagedestroy($image[$i]);
    imagedestroy($tn[$i]);
    imagedestroy($dest[$i]); 
    }
    
    unset($_POST);

  }
  
  }}
  
  break;
  
  case "update":
  

  if (count($_POST["downloads"]) > 0) {
  
      foreach ($_POST["downloads"] as $id) {

        $d = substr($_POST["item_startdate_".$id],0,2);
        $m = substr($_POST["item_startdate_".$id],3,2);
        $y = substr($_POST["item_startdate_".$id],6,4);

        $start_date =  $y."-".$m."-".$d." ".$_POST["date_h_".$id].":".$_POST["date_m_".$id].":".date("s");
        $run = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DATE_ADD('".$start_date."', INTERVAL ".$_POST["item_duration_".$id]." DAY)");
        $row = mysqli_fetch_row($run);
        $end_date = $row[0];
        
        $s_inc = "NO";
        if ($_POST["item_shipincl_".$id]=="on") $s_inc = "YES";
  
        $update_sql = "UPDATE probid_auctions_downloads SET 
        itemname='".$_POST["item_name_".$id]."', description='".$_POST["item_desc_".$id]."', category_id=".$_POST["item_category_".$id].",
        addcat_id=".$_POST["item_addcat_".$id].", country='".$_POST["item_location_".$id]."', quantity='".$_POST["item_quantity_".$id]."',
        price='".$_POST["item_price_".$id]."', reserve_price='".$_POST["item_reserve_price_".$id]."', buy_now = '".$_POST["item_buynow_".$id]."',
        currency='".$_POST["item_currency_".$id]."',start_date='".$start_date."',end_date='".$end_date."',duration='".$_POST["item_duration_".$id]."',
        s_included='".$s_inc."',pat_details='".$_POST["item_psdetails_".$id]."' WHERE id=".$id." AND owner_id=".$_SESSION["memberid"]."";
        mysqli_query($GLOBALS["___mysqli_ston"], $update_sql);
      }
  
  }
  
  break;
  
  }
  
 // GET USER DOWNLOADS

  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * FROM probid_auctions_downloads WHERE owner_id=".$_SESSION["memberid"]."");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $myDownloads[] = $row; 
  }
  

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf8">
  <title>SBE.EE - Auctions - My Downloads</title>
  <STYLE TYPE="text/css" MEDIA="screen, projection">
  @import url("calendar.css");
  </style>
  <script type="text/javascript" src="calendar.js"></script>
  </head>
  <body>
  
  <?php
  
  if (is_array($myDownloads) && (sizeof($myDownloads)>0)) {
  
    ?>
    <form name="dataForm" method="post" action="">
    <table cellspacing="0" cellpadding="0" border>
    <tr>
        <td style="text-align:right;padding-right:11px;"><input type="checkbox" onclick="javascript:ckeck_uncheck_all()" title="Check all" name="master_box"/></td>
    </tr>
    <tr>
        <td>
    <?
  
    
  
    foreach ($myDownloads as $item) {
    
    $nr++;
        
    ?>
    
    
      <table border="1" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" width="15">
          <h2><?=$nr?></h2>
          </td>
          <td valign="top" rowspan="2">
          <table style="background-color:#dddddd;">
          <tr>
              <td valign="top" colspan="7">Category:  <?=catList("item_category_".$item["id"],$item["category_id"]);?>Add category<?=catList("item_addcat_".$item["id"],$item["addcat_id"]);?></td>
          </tr>
          <tr>
              <td>
              <table>
              <tr>
                  <td valign="top">Itemname:</td><td> <input type="text" name="item_name_<?=$item["id"]?>" size="20" value="<?=$item["itemname"]?>"  /></td>
              </tr>
              <tr>
                  <td>Location:</td><td><input type="text" name="item_location_<?=$item["id"]?>" size="30" value="<?=$item["country"]?>" /></td>
              </tr>
              <tr>
                  <td>Quantity:</td><td><input type="text" name="item_quantity_<?=$item["id"]?>" size="10" value="<?=$item["quantity"]?>" /></td>
              </tr>
              </table>
              </td>
              <td valign="top">Description:</td><td><textarea name="item_desc_<?=$item["id"]?>" cols="40" rows="2"><?=$item["description"]?></textarea></td>
              <!-- CONTROL IMAGES -->
              <td valign="top">Images</td><td><input type="file" size="1" name="item_pic1"><input type="file" size="1" name="item_pic2"><input type="file" size="1" name="item_pic3"><input type="file" size="1" name="item_pic4"><input type="file" size="1" name="item_pic5"></td>
          </tr>
          <tr>
            <td colspan="7">
            <table style="width:100%;">
              <tr>
                  <td valign="top">
                  <table>
                  <tr>
                      <td>Price: </td><td>Reserve</td><td>Buy now</td><td></td>
                  </tr>
                  <tr>
                      <td><input type="text" name="item_price_<?=$item["id"]?>" size="10" value="<?=$item["price"]?>" /></td>
                      <td><input type="text" name="item_reserve_price_<?=$item["id"]?>" size="10" value="<?=$item["reserve_price"]?>" /></td>
                      <td><input type="text" name="item_buynow_<?=$item["id"]?>" size="10" value="<?=$item["buy_now"]?>" /></td>
                      <td><?=currList($item["currency"],"item_currency_".$item["id"]);?></td>
                  </tr>
                  </table>
                  </td>
                  <td valign="top">
                    <table>
                    <!-- CONVERT DATA-->
                    <?
                        $d = substr($item["start_date"],8,2);
                        $m = substr($item["start_date"],5,2);
                        $y = substr($item["start_date"],0,4);
                        $min = substr($item["start_date"],14,2);
                        $h = substr($item["start_date"],11,2);
                    ?>
                    <tr>
                        <td>Start Date: </td>
                        <td>
                            <input type="text"  name="item_startdate_<?=$item["id"]?>" value="<?=$d.".".$m.".".$y?>"/><a href="javascript:displayDatePicker('item_startdate_<?=$item["id"]?>',false,'dmy','.');" ><img style="vertical-align:middle;" alt="" border="0" src="<?="images/calendar.jpg"?>"></a>
                        </td>
                        <td><?=timeBox($h,$min,"date_h_".$item["id"],"date_m_".$item["id"])?></td>
                    </tr>
                    <tr>
                        <td>Duration:</td><td><?=durationList("item_duration_".$item["id"],$item["duration"]);?></td>
                    </tr>
                    </table>  
                  </td>
                  <td valign="top">Transport and Payment description<br><input type="checkbox" "<?=($item["s_included"]=="YES"?'checked':'')?>" name="item_shipincl_<?=$item["id"]?>"> Shipping included</td>
                  <td><textarea name="item_psdetails_<?=$item["id"]?>" cols="30" rows="1"><?=$item["pat_details"]?></textarea></td>
              </tr>
            </table>
            </td>
          </tr>
          </table>
          </td>
            <td style="width:40px;text-align:center;">
            <input type="hidden" name="action" value="insert"><input type="checkbox" name="downloads[]" value="<?=$item["id"]?>">
            </td>
        </tr>
        </table>    
    <?
    
    
    }
  
    ?>
          </td>
        </tr>
        <tr>
          <td>
          <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
        <tr>
        <td align="right">Select command:
            <select name="action">
              <option value="-"></option>
              <option value="update">Save</option>
              <option value="copy">Copy</option>
              <option value="delete">Delete</option>
          </select>
          <input type="submit" value="OK">
        </td>
        </tr>
        </table>
        </td>
      </tr>
    </table>
    </form><?
  }
  
  
  ?>

  <form name="insertForm" enctype="multipart/form-data" method="post" action="">
  <table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" rowspan="2">
    <table style="background-color:#00ccff;">
    <tr>
        <td valign="top" colspan="7">Category:  <?=catList("item_category",$_POST["item_category"]);?>Add category<?=catList("item_addcat",$_POST["item_addcat"]);?></td>
    </tr>
    <tr>
        <td>
        <table>
        <tr>
            <td valign="top">Itemname:</td><td> <input type="text" name="item_name" size="20" value="<?=$_POST["item_name"]?>" /></td>
        </tr>
        <tr>
            <td>Location:</td><td><input type="text" name="item_location" size="30" value="<?=$_POST["item_location"]?>" /></td>
        </tr>
        <tr>
            <td>Quantity:</td><td><input type="text" name="item_quantity" size="10" value="<?=(intval($_POST["item_quantity"])==0?"1":$_POST["item_quantity"])?>"/></td>
        </tr>
        </table>
        </td>
        <td valign="top">Description:</td><td><textarea name="item_desc" cols="40" rows="2"><?=$_POST["item_desc"]?></textarea></td>
        <td valign="top">Images</td><td><input type="file" size="1" name="item_pic1"><input type="file" size="1" name="item_pic2"><input type="file" size="1" name="item_pic3"><input type="file" size="1" name="item_pic4"><input type="file" size="1" name="item_pic5"></td>
    </tr>
    <tr>
      <td colspan="7">
      <table style="width:100%;">
        <tr>
            <td valign="top">
                  <table>
                  <tr>
                      <td>Price: </td><td>Reserve</td><td>Buy now</td><td></td>
                  </tr>
                  <tr>
                      <td><input type="text" name="item_price" size="8" value="<?=$_POST["item_price"]?>"/></td>
                      <td><input type="text" name="item_reserve_price" size="8" value="<?=$_POST["item_reserve_price"]?>" /></td>
                      <td><input type="text" name="item_buynow" size="8" value="<?=$_POST["item_buynow"]?>" /></td>
                      <td><?=currList($_POST["item_currency"],"item_currency");?></td>
                  </tr>
                  </table>
            </td>
            <td valign="top">
              <table>
              <tr>
                  <td>Start Date: </td><td>
                  <input type="text"  name="item_startdate" value="<?=($_POST["item_startdate"]==""?date("d.m.Y"):$_POST["item_startdate"])?>"/><a href="javascript:displayDatePicker('item_startdate',false,'dmy','.');" ><img style="vertical-align:middle;" alt="" border="0" src="<?="images/calendar.jpg"?>"></a>
                  </td>
                  <td><?=timeBox($_POST["date_h"],$_POST["date_m"],"date_h","date_m")?></td>
              </tr>
              <tr>
                  <td>Duration:</td><td><?=durationList("item_duration",$_POST["item_duration"]);?></td>
              </tr>
              </table>  
            </td>
            <td valign="top">Transport and Payment description<br><input type="checkbox" name="item_shipincl" "<?=($_POST["item_shipincl"]=="on"?'checked':'')?>"> Shipping included</td>
            <td><textarea name="item_psdetails" cols="30" rows="1"><?=$_POST["item_psdetails"]?></textarea></td>
        </tr>
      </table>
      </td>
    </tr>
    </table>
    </td>
      <td>
      <input type="hidden" name="action" value="insert"><input type="submit" value="ADD">
      </td>
  </tr>
  </table>
  </form>
  </body>
</html>
<?
}