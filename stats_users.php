<?php
## v5.24 -> may. 08, 2006
session_start();
include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php");

function Vanus($isikukood)
{
$Birth_year=1900+substr($isikukood,1,2);
$Birth_month=0+substr($isikukood,3,2);
$Birth_day=0+substr($isikukood,3,2);

//Теперь вычислим метку Unix для указанной даты
$birthdate_unix = mktime(0, 0, 0, $Birth_month, $Birth_day, $Birth_year);

//Вычислим метку unix для текущего момента
$current_unix = time();

//Просчитаем разность меток
$period_unix=$current_unix - $birthdate_unix;

// Получаем искомый возраст

// Возраст измеряемый годами
return floor($period_unix / (365*24*60*60));
}
//updating script cities
$query1=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE city='Kohtla-Jarve'");
while ($query2= mysqli_fetch_array($query1))
{
mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET city='Kohtla-Järve' WHERE id='".$query2['id']."'");
}
//Updating ages man
 $auth_man=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and country='Estonia'");
   while ($mout = mysqli_fetch_array($auth_man))
   {
   $res=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET age='".Vanus($mout['isikukood'])."'  WHERE id='".$mout['id']."'");
   };
//Updating ages woman
 $auth_woman=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and country='Estonia'");
   while ($wout = mysqli_fetch_array($auth_woman)){
   $res=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET age='".Vanus($wout['isikukood'])."'  WHERE id='".$wout['id']."'");
}
;

if ($setts['h_counter']==1) {
   	 	$nbShops=getSqlNumber("SELECT id FROM probid_users WHERE store_active=1");
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbAuthUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y'");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		$nbFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1","Total");
		$nbRegisteredUsers24h=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-86400));
		$nbRegisteredUsers7d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-604800));
		$nbRegisteredUsers30d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-2592000));
		$nbRegisteredUsers1y=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-31536000));
		$nbopenauctions24h=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbopenauctions7d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbopenauctions30d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbopenauctions1y=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbtotalauctions=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_auctions","Total1");
		$nbauctionsAmount24h=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'","Total1");
		$nbauctionsAmount7d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'","Total1");
		$nbauctionsAmount30d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'","Total1");
		$nbauctionsAmount1y=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'","Total1");
		$nbauctionsFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions","Total1");
		$nbsolds24h=getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3");
		$nbsolds7d=getSqlField("SELECT SUM(amount) AS 'Total4' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-604800),"Total4");
		$nbsolds30d=getSqlField("SELECT SUM(amount) AS 'Total5' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total5");
		$nbsolds1y=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total6");
		$nbsoldsTotal=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners","Total6");
		$nbWantAds24h=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbWantAds7d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbWantAds30d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbWantAds1y=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbWantAdsTotal=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_wanted_ads","Total1");
		if ($nbsolds24h>0) $effect24h=($nbsolds24h/$nbauctionsAmount24h)*100; else $effect24h=0;
		if ($nbsolds7d>0) $effect7d=($nbsolds7d/$nbauctionsAmount7d)*100; else $effect7d=0;
		if ($nbsolds30d>0) $effect30d=($nbsolds30d/$nbauctionsAmount30d)*100; else $effect30d=0;
		if ($nbsolds1y>0) $effect1y=($nbsolds1y/$nbauctionsAmount1y)*100; else $effect1y=0;
		if ($nbsoldsTotal>0) $effecttotal=($nbsoldsTotal/$nbauctionsFullAmount)*100; else $effecttotal=0;
		$nbman=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and country='Estonia'");
		$nbwoman=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and country='Estonia'");
		$nball=$nbman+$nbwoman;
		
		$nbmana18=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age<18 and country='Estonia'");
		$nbman1825=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age>18 and age<25 and country='Estonia'");
		$nbman2635=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age>25 and age<35 and country='Estonia'");
		$nbman3645=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age>35 and age<45 and country='Estonia'");
		$nbman4655=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age>45 and age<55 and country='Estonia'");
		$nbman5665=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age>55 and age<65 and country='Estonia'");
		$nbmanm65=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and age>65 and country='Estonia'");
		
		$nbwomana18=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age<18 and country='Estonia'");
		$nbwoman1825=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age>18 and age<25 and country='Estonia'");
		$nbwoman2635=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age>25 and age<35 and country='Estonia'");
		$nbwoman3645=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age>35 and age<45 and country='Estonia'");
		$nbwoman4655=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age>45 and age<55 and country='Estonia'");
		$nbwoman5665=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age>55 and age<65 and country='Estonia'");
		$nbwomanm65=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and age>65 and country='Estonia'");



include "stat_menu.php";
echo "<div align='center'> <a href='#common'>Общая статистика</a> ! <a href='#agesex'>Возраст и пол</a> ! <a href='#states'>Государства</a> ! <a href='#city'>Города</a> </div> <br><br>";
echo "<div align='justify'>Статистика по пользователям позволяет оценить аудиторию интернет-аукциона SBE.EE. Данная информация предназначеная прежде всего для рекламодателей.<br><br>Внимание: статистика пользователей даётся лишь обобщённо. Ни при каких обстоятельствах SBE.EE не выдаёт личные данные отдельного пользователя. С политикой приватности Вы можете ознакомиться здесь.</div>";
echo "<div align='center'><a name='common'><h3>Kasutajad</h3></div>
				<table align='center'><tr class='c3'> 
          		<td align='right'><b>".$nbUsers."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>$lang[registered_users]</font></td>
        	</tr>
        	</tr><tr class='c3'> 
          		<td align='right'><b>".$nbAuthUsers."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>Autentinud kasutajaid</font></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>".round(($nbAuthUsers/$nbUsers)*100,2)." %</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>Autentinud kasutajate %</font></td>
        	</tr>";
echo "</table>";
echo "<div align='center'><a name='agesex'><h3>Kasutajate jaotus vanuse ja soo järgi Eestis</h3></a></div>
				<table align='center'><tr class='c3'> 
          		<td align='right'><b></b></td>
          		<td colspan='2' align='center'><b>Soo</b></td>
          		<td align='right'><b></b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>Vanus</b></td>
          		<td align='right'><b>Mehed</b></td>
          		<td align='right'><b>Naised</b></td>
          		<td align='right'><b>Kokku</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>Alla 18</b></td>
          		<td align='right'><b>".round((($nbmana18/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwomana18/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbmana18+$nbwomana18)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>18-25 a.</b></td>
          		<td align='right'><b>".round((($nbman1825/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwoman1825/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbman1825+$nbwoman1825)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>26-35 a.</b></td>
          		<td align='right'><b>".round((($nbman2635/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwoman2635/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbman2635+$nbwoman2635)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>36-45 a.</b></td>
          		<td align='right'><b>".round((($nbman3645/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwoman3645/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbman3645+$nbwoman3645)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>46-55 a.</b></td>
          		<td align='right'><b>".round((($nbman4655/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwoman4655/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbman4655+$nbwoman4655)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>56-65 a.</b></td>
          		<td align='right'><b>".round((($nbman5665/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwoman5665/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbman5665+$nbwoman5665)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>Vanem kui 65 a.</b></td>
          		<td align='right'><b>".round((($nbmanm65/$nbman)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwomanm65/$nbwoman)*100),1)." %</b></td>
          		<td align='right'><b>".round(((($nbmanm65+$nbwomanm65)/$nball)*100),1)." %</b></td>
        	</tr><tr class='c3'> 
          		<td align='right'><b>Kokku</b></td>
          		<td align='right'><b>".round((($nbman/$nball)*100),1)." %</b></td>
          		<td align='right'><b>".round((($nbwoman/$nball)*100),1)." %</b></td>
          		<td align='right'><b>100%</b></td>
        	</tr>";
echo "</table>";
$cnt_country=0;
echo "<div align='center'><a name='states'><h3>Kasutajate jaotus riikide järgi</h3></a></div>
				<table align='center'>
				<tr class='c3'> 
          		
          		<td align='right'><b>Riik</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>Kasutajate arv</font></td>
        	</tr>";
$CountryQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT country FROM probid_users ");
while ($CountryDetails = mysqli_fetch_array($CountryQuery))
{
$country_array[$CountryDetails['country']]=getsqlfield("SELECT COUNT(country) AS 'Total2' FROM probid_users WHERE country='".$CountryDetails['country']."'","Total2");
}

arsort($country_array);

foreach ($country_array as $key => $value)
{
	echo "<tr class='c3'> 
				
          		<td align='right'><b>".$key."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>".$value."</font></td>
        	</tr>";}
echo "</table>";
echo "<div align='center'><a name='city'><h3>Kasutajate jaotus linnade järgi</h3></a></div>
				<table align='center'>
				<tr class='c3'> 
          		<td align='right'><b>Riik</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>Kasutajate arv</font></td>
        	</tr>";
$CityQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT city FROM probid_users ");
$cnt_city=0;
while ($CityDetails = mysqli_fetch_array($CityQuery))
{$cnt_city++;
	echo "<tr class='c3'> 
          		<td align='right'><b>".$cnt_city."</b></td>
          		<td align='right'><b>".$CityDetails['city']."</b></td>
          		<td>&nbsp;<font style='font-size: 10px;'>".getsqlfield("SELECT COUNT(city) AS 'Total2' FROM probid_users WHERE city='".$CityDetails['city']."'","Total2")."</font></td>
          		<td align='right'><b>".getsqlfield("SELECT id AS 'Total2' FROM probid_users WHERE city='".$CityDetails['city']."'","Total2")."</b></td>
        	</tr>";}
echo "</table>";

	}
include ("themes/".$setts['default_theme']."/footer.php"); ?>