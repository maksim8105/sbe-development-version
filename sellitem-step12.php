<? 
## v5.25 -> jun. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($_SESSION['membersarea']=="Active") { 
?>

 <table width="100%" border="0" cellpadding="2" cellspacing="2"> 
  <tr class="contentfont" align="center"> 
     <td class="c4" width="20%"><?=$lang[sellstep1];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep3];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep4];?></td> 
     <td class="c1" width="20%"><?=$lang[sellstep5];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep6];?></td> 
   </tr> 
</table>
<br>
<input type="hidden" name="step" value="step2">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr>
      <td colspan=2 class="c1"><b>
         <?=$lang[shipping]?>
         &
         <?=$lang[payment]?>
         </b></td>
   </tr>
   <tr class="c5">
      <td width="35%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c3">
      <td align="right" valign="top"><strong>
         <?=$lang[shippingcond]?>
         </strong></td>
      <td><input type="radio" name="shipcond" value="BP" <? echo ($_REQUEST['shipcond']=="BP")?"checked":""; ?>>
         <?=$lang[buyerpaysshipment]?>
         <br>
         <input type="radio" name="shipcond" value="SP" <? echo ($_REQUEST['shipcond']=="SP")?"checked":""; ?>>
         <?=$lang[sellerpaysshipment]?>
         <br>
         <input name="shipint" type="checkbox" id="shipint" value="Y" <? echo ($_REQUEST['shipint']=="Y")?"checked":""; ?>>
         <?=$lang[sellershipinternat]?>
      </td>
   </tr>
   <? if($setts['paypaldirectpayment']):?>
   <tr class="c2">
      <td align="right" valign="top"><strong>
         <?=$lang[directpayment]?>
         </strong> </td>
      <td><table border="0" cellpadding="0" cellspacing="0">
            <?
            $paymentSystems = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_direct_payment WHERE status=1");
            $seller = getSqlRow("SELECT * FROM probid_users WHERE id=".$_SESSION['memberid']);
            $isnotenabled = false;
            $counter = 0;
        		?>
            <tr>
               <? while($pay_system = mysqli_fetch_array($paymentSystems)):?>
               <td width="130" class="contentfont"><?
                	$enabled = false;
                	switch($pay_system['id']){
							case 1:
                     	if($seller['paypalemail']) $enabled = true;
                        break;
                    	case 2:
                        if($seller['worldpayid']) $enabled = true;
                        break;
                     case 3:
                        if($seller['ikoboid']&&$seller['ikoboipn']) $enabled = true;
                        break;
                    	case 4:
                        if($seller['nochexemail']) $enabled = true;
                        break;
                    	case 5:
                        if($seller['checkoutid']) $enabled = true;
                        break;
                    	case 6:
                        if($seller['authnetid']&&$seller['authnettranskey']) $enabled = true;
                        break;
                    	case 7:
                        if($seller['protxname']&&$seller['protxpassword']) $enabled = true;
                        break;
                	}
                	if(!$enabled) $isnotenabled=true; ?>
                  <input type="checkbox" value="<?=$pay_system['id'];?>" name="dirpay[<?=$pay_system['id'];?>]" <? if ( strpos( ','.$_REQUEST['tmp_dirpay'].',' , ','.$pay_system['id'].',' ) !== false) echo " checked"; ?> <?=($enabled?'':'disabled');?>>
                  <?=$pay_system['name'];?>
               </td>
               <?
					if($counter++==1){
               	echo('</tr><tr>');
                  $counter=0;
               } ?>
               <? endwhile?>
               <?	if($counter==1) echo('<td></td>'); ?>
            </tr>
         </table>
         <? if($isnotenabled):?>
         <?=$lang[m_dir_pay_notice];?>
         <? endif?>
      </td>
   </tr>
   <? endif //if($setts['paypaldirectpayment']) ?>

   <tr class="c2">
      <td align="right" valign="top"><strong>
         <?=$lang[paymethods]?>
         </strong></td>
      <td><? 
			$getdurations=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_methods");
			$pct=0;
			while ($row=mysqli_fetch_array($getdurations)) {
				$pm_name[$pct]=$row['name'];
				$pct++;
			}
			echo '<table border="0" cellspacing="0" cellpadding="0">';
			for ($i=0;$i<$pct;$i+=2) {
				$j=$i+1;
				echo "<tr>\n";
				echo "	<td width=\"200\" class=\"contentfont\"><input type=\"checkbox\" name=\"pmethod[]\" value=\"".$pm_name[$i]."\" ".((@eregi($pm_name[$i].'&lt;br&gt;',$_REQUEST['pm']))?"checked":"").">".$pm_name[$i]."</td>\n";
				echo "	<td width=\"200\" class=\"contentfont\">".(($pm_name[$j]!="")?"<input type=\"checkbox\" name=\"pmethod[]\" value=\"".$pm_name[$j]."\" ".((@eregi($pm_name[$j].'&lt;br&gt;',$_REQUEST['pm']))?"checked":"").">".$pm_name[$j]."":"")."</td>\n";
				echo "</tr>\n";
			}
			echo "</table>";
			?></td>
   </tr>
   <!-- VAT CHECKBOX -->
   <? if($setts['enable_vat']==1):?>
   <? 
	// get VAT permissions
   $seller = getSqlRow("SELECT * FROM probid_users WHERE id=".$_SESSION['memberid']);
   $country_id = getSqlField("SELECT id FROM probid_countries WHERE name='".$seller['country']."'",'id');
	$user_vat = getSqlRow("SELECT * FROM probid_vat_setts WHERE LOCATE(',".$country_id.",',CONCAT(',',countries,','))>0");
   if($user_vat){
   	$user_types = explode(',',$user_vat['users_sale_vat']);
      if(($seller['companyname']=="")&&($seller['vat_uid_number']==""))$user_type='d';
      if(($seller['companyname']=="")&&($seller['vat_uid_number']!=""))$user_type='c';
      if(($seller['companyname']!="")&&($seller['vat_uid_number']==""))$user_type='b';
      if(($seller['companyname']!="")&&($seller['vat_uid_number']!=""))$user_type='a';
      if(in_array($user_type,$user_types)) { ?>
   <tr class="c3">
      <td align="right" valign="top"><strong>
         <?=str_replace('VAT',$user_vat['symbol'],$lang[m_apply_vat]);?>
         </strong> </td>
      <td><input name="apply_vat" type="checkbox" id="apply_vat" value="1" <?=($_REQUEST['apply_vat']==1)?'checked':'';?>>
      </td>
   </tr>
   <? } // if(in_array($user_type,$user_types))
	} // if($user_vat) ?>
   <? endif?>
   <!-- VAT CHECKBOX -->
   <? if ($setts['shipping_costs']==1) { ?>
   <tr class="c3">
      <td align="right" valign="top"><strong>
         <?=$lang[postagecosts]?>
         </strong></td>
      <td><?=$_POST[currency];?>
         <input class="contentfont" name="postage_costs" type="text" id="postage_costs" value="<?=$_REQUEST['postage_costs'];?>" size="15"></td>
   </tr>
   <tr class="c2">
      <td align="right" valign="top"><strong>
         <?=$lang[insurance]?>
         </strong></td>
      <td><?=$_POST[currency];?>
         <input name="insurance" type="text" id="insurance" value="<?=$_REQUEST['insurance'];?>" class="contentfont" size="15"></td>
   </tr>
   <tr class="c3">
      <td align="right" valign="top"><strong>
         <?=$lang[sp_details]?>
         </strong></td>
      <td><textarea name="shipping_details" cols="50" rows="4"><?=remSpecialChars($_REQUEST['shipping_details']);?></textarea></td>
   </tr>
   <tr class="c2">
      <td align="right" valign="top"><strong>
         <?=$lang[servicetype]?>
         </strong></td>
      <td><? 
			echo "<SELECT name=\"type_service\" class=\"contentfont\">";
			echo "<option value=\"\" selected>".$lang[servicetypeselect]."</option>";
			$getShippingOptions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_shipping_options");
			while ($shOpt=mysqli_fetch_array($getShippingOptions)) {
				echo "<OPTION value=\"".$shOpt['name']."\" ".(($shOpt['name']==$_REQUEST['type_service'])?"SELECTED":"").">".$shOpt['name']."</option>";
			}
			echo "</SELECT>"; ?></td>
   </tr>
   <? } ?>
   <?
  	$prefSeller = "N";
	$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']));
	$reduction = 1;
	if ($setts['pref_sellers']=="Y") {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$_SESSION['memberid']."'","preferred_seller");
		if ($prefSeller=="Y") $reduction = (100-$setts['pref_sellers_reduction'])/100;
	}
	$vatExempted = "Y";
	if ($setts['vat_rate']>0) $vatExempted = getSqlField("SELECT vat_exempted FROM probid_users
	WHERE id='".$_SESSION['memberid']."'","vat_exempted");

	$itemCategory = $_REQUEST['category'];
	
	$category_id = ($itemCategory > 0) ? getMainCat($itemCategory) : 0;

	$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
	### we overwrite the $fees array with what we need
	$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
  	?>
   <tr>
      <td colspan="2" class="c1"><b>
         <? 
			echo $lang[aucfees].(($prefSeller=="Y")?" [ ".$lang[pref_seller]." - ".$setts['pref_sellers_reduction']."% ".$lang[reduction]." ]":"")." ".(($vatExempted=="Y")?"":" [ ".$lang[all_fees_incl]." ".$setts['vat_rate']."% ".$lang[tax_vat]." ]");
			echo ($voucher['valid']) ? "[ $lang[voucher_reduction] ]" : "" ;
			?>
         </b></td>
   </tr>
   <? if (!freeFees($_SESSION['memberid'])&&$_REQUEST['listin']!="store") { ?>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? 
  	$isListingFee = getSqlNumber("SELECT * FROM probid_fees_tiers WHERE 
  	fee_from<=".$_REQUEST['startprice']." AND fee_to>".$_REQUEST['startprice']." AND fee_type='setup' AND category_id='".$category_id."'");
  	if ($fee['is_setup_fee']=="Y"&&$isListingFee>0) { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[listfee]?>
         </strong></td>
      <td><? 
			$listingFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE 
			fee_from<=".$_REQUEST['startprice']." AND fee_to>".$_REQUEST['startprice']." AND fee_type='setup' AND category_id='".$category_id."'");
		
			$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"setup");
			$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100));
		
			$exchange_rate = 1;
			if (trim($setts['currency'])!=trim($_REQUEST['currency'])) {
				$converter = getSqlField("SELECT converter FROM probid_currencies WHERE symbol = '".trim($_REQUEST['currency'])."'","converter");
				$converter = ($converter == 0) ? 0.01 : $converter;
				$exchange_rate = 1/$converter;
			}			
			if($exchange_rate<=0) $exchange_rate = 1;
		
			if ($listingFee['calc_type']=="flat") echo displayAmount(applyVat($listingFee['fee_amount'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($listingFee['fee_amount']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":"");
			else echo $listingFee['fee_amount']."% = ".displayAmount(applyVat($_REQUEST['startprice']*$listingFee['fee_amount']/100*$exchange_rate,$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat(($_REQUEST['startprice']*$listingFee['fee_amount']/100)*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); 
			?>
      </td>
   </tr>
   <? } ?>
   <? 
  	if ($fee['second_cat_fee']>0&&$_REQUEST['addlcategory']>0) { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"seccat");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[second_cat_fee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['second_cat_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['second_cat_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$_REQUEST['cnt1']>0) { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"pic");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[addpicfee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_pic_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_pic_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$_REQUEST['hlitem']=="Y") {
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"hl");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[highlightfee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_hlitem_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_hlitem_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?></td>
   </tr>
   <? } ?>
   <? if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$_REQUEST['bolditem']=="Y") { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"bold");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[boldfee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_bolditem_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_bolditem_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?></td>
   </tr>
   <? } ?>
   <? if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$_REQUEST['catfeat']=="Y") { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"catfeat");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[catfee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_catfeat_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_catfeat_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?>
         <? if ($_REQUEST['addlcategory']>0) echo " x 2 = " . displayAmount(applyVat($fee['val_catfeat_fee'] * 2,$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_catfeat_fee']*2*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?>
      </td>
   </tr>
   <? } ?>
   <? if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$_REQUEST['hpfeat']=="Y") { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"hpfeat");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[homepagefee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_hpfeat_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_hpfeat_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$_REQUEST['respr']=="Y") { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"rp");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[respricefee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_rp_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['val_rp_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['bin_fee']>0&&$_REQUEST['buynow']=="Y") { 
		$voucher = checkSetupVoucher(trim($_REQUEST['voucher_code']),"bn");
		$total_reduction = $reduction - ($reduction * ($voucher['reduction']/100)); ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[buyitnowfee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['bin_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['bin_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['custom_st_fee']>0 && ($_REQUEST['starttime']=="custom" || $_REQUEST['endtime']=="customtime")) { 
		$total_reduction = $reduction; ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[custom_st_fee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['custom_st_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['custom_st_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['videofile_fee']>0 && !empty($_REQUEST['videofile_path'])) { 
		$total_reduction = $reduction; ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[videofile_fee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['videofile_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y"||$voucher['valid']>0) ? "[ ".(($prefSeller=="Y") ? $setts['pref_sellers_reduction']."% ".$lang[off] : "")." ".(($voucher['valid']>0) ? (($prefSeller=="Y") ? $lang[and_msg] : "")." ".$voucher['reduction']."% ".$lang[voucher_reduct]."" : "")." = ".displayAmount(applyVat($fee['videofile_fee']*$total_reduction,$_SESSION['memberid']),$setts['currency'],TRUE)." ]":""); ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_swap_fee']=="Y"&&$fee['val_swap_fee']>0&&$_REQUEST['isswap']=="Y") { ?>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[swap_fee]?>
         </strong></td>
      <td><? echo displayAmount(applyVat($fee['val_swap_fee'],$_SESSION['memberid']))." ".(($prefSeller=="Y") ? "[ ".$setts['pref_sellers_reduction']."% ".$lang[off]." = ".displayAmount(applyVat($fee['val_swap_fee']*$reduction,$_SESSION['memberid']))." ]":""); ?> </td>
   </tr>
   <? } ?>
   <?
  	$isListingFee = getSqlNumber("SELECT * FROM probid_fees_tiers WHERE 
  	fee_from<=".$_REQUEST['startprice']." AND fee_to>".$_REQUEST['startprice']." AND fee_type='setup' AND category_id='".$category_id."'");
	if ($fee['is_setup_fee']=="Y"&&$isListingFee>0) $topay.="Auction Setup Fee; ";
	if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$_REQUEST['cnt1']>0) { 
		$topay.="Image Gallery Fee; ";
		$isFee['pic_count']=$_REQUEST['cnt1'];
	}
	if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$_REQUEST['hlitem']=="Y") {
		$topay.="Highlighted Item Fee; ";
		$isFee['hl']="Y";
	}
	if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$_REQUEST['bolditem']=="Y") {
		$topay.="Bold Item Fee; ";
		$isFee['bold']="Y";
	}
	if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$_REQUEST['hpfeat']=="Y") {
		$topay.="Home Page Featured Item Fee; ";
		$isFee['hpfeat']="Y";
	}
	if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$_REQUEST['catfeat']=="Y") {
		$topay.="Category Page Featured Item Fee; ";
		$isFee['catfeat']="Y";
	}
	if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$_REQUEST['respr']=="Y") {
		$topay.="Reserve Price Fee; ";
		$isFee['rp']="Y";
	}
	if ($fee['second_cat_fee']>0&&$_REQUEST['addlcategory']>0) {
		$topay.="Second Category Fee; ";
		$isFee['secondcat']="Y";
	}
	if ($fee['bin_fee']>0&&$_REQUEST['buynow']=="Y") {
		$topay.="Buy It Now Fee; ";
		$isFee['bin']="Y";
	}
	if ($fee['custom_st_fee']>0&&($_REQUEST['starttime']=="custom"||$_REQUEST['endtime']=="customtime")) {
		$topay.="Custom Start Time Fee; ";
		$isFee['customst']="Y";
	}
	if ($fee['videofile_fee']>0&&!empty($_REQUEST['videofile_path'])) {
		$topay.="Movie Upload Fee; ";
		$isFee['videofee']="Y";
	}
	
	$prefSeller = "N";
	if ($setts['pref_sellers']=="Y") {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$_SESSION['memberid']."'","preferred_seller");
	}

  ?>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c2">
      <td align="right"><strong>
         <?=$lang[total_fees]?>
         </strong></td>
      <td><? 
			$setupFee = setupFee($_REQUEST['startprice'],$_REQUEST['currency'],$auctionid,$isFee,FALSE,TRUE,trim($_REQUEST['voucher_code']),$_REQUEST['category']);
			echo displayAmount($setupFee,$setts['currency'],TRUE); ?>
      </td>
   </tr>
   <? 
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
	$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$_SESSION['memberid']."'","balance");
	if ($setts['account_mode_personal']==1) {
		$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	if ($account_mode_local==2&&$setts['max_credit']<=($setupFee+$currentBalance)) { ?>
   <tr class="c5">
      <td colspan="2" align="center"><? echo $lang[warn_auct_over_credit]; ?> </td>
   </tr>
   <? } ?>
   <? if ($fee['is_endauction_fee']=="Y" && @eregi('s', $fee['endauction_fee_applies'])) { ?>
   <tr class="c4">
      <td align="center" colspan="2"><strong>
         <?=$lang[endaucfee]?>
         </strong></td>
   </tr>
   <tr>
      <td align="center" colspan="2"><table width="60%"  border="0" cellspacing="2" cellpadding="2" class="border">
            <? $getTiers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='endauction'");
				while ($feeTiers = mysqli_fetch_array($getTiers)) { ?>
            <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>">
               <td width="100%"><? echo "$lang[from] ".displayAmount($feeTiers['fee_from'])." $lang[to] ".displayAmount($feeTiers['fee_to']);?></td>
               <td nowrap><?
		  				if ($feeTiers['calc_type']=="flat") echo displayAmount($feeTiers['fee_amount'])." ".(($prefSeller=="Y") ? "[ ".$setts['pref_sellers_reduction']."% ".$lang[off]." = ".displayAmount($feeTiers['fee_amount']*$reduction)." ]":"");
		   			else echo $feeTiers['fee_amount']."%"." ".(($prefSeller=="Y") ? "[ ".$setts['pref_sellers_reduction']."% ".$lang[off]." = ".$feeTiers['fee_amount']*$reduction."% ]":""); ?></td>
            </tr>
            <? } ?>
         </table></td>
   </tr>
   <? } ?>
   <? } ?>
   <? if ($layout['d_tc_text']==1) { ?>
   <tr>
      <td colspan="2" class="c1"><b>
         <?=strtoupper($lang[reg_terms])?>
         </b></td>
   </tr>
   <tr class="c5">
      <td width="30%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td width="70%"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="c3">
      <td colspan="2" class="contentfont"><?=$lang[auc_terms]?></td>
   </tr>
   <tr align="center" class="c2">
      <td colspan="2" class="contentfont"><textarea name="textarea" readonly class="smallfont" style="width:500px; height=200px;"><? echo @eregi_replace("<br>","\n",addSpecialChars($layout['tc_text']));?></textarea></td>
   </tr>
   <? } ?>
   <tr class="c4">
      <td colspan="2" align="center"><? echo displayNavButtons(); ?></td>
   </tr>
</table>
<? if ($_REQUEST['auctiontype']=="dutch") {?>
<input type="hidden" name="respr" value="N">
<? } ?>
</form>
<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?>
