<?
## v5.24 -> apr. 05, 2006
session_start();
include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");
header5("$lang[wantedsrc]");
	
if ($_REQUEST['orderField']=="") $orderField = "enddate";
else $orderField=$_REQUEST['orderField'];
if ($_REQUEST['orderType']=="") {
	$orderType = "ASC";
	$newOrder="DESC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}
	
$query = "SELECT * FROM probid_wanted_ads 
WHERE keywords LIKE '%".$_REQUEST['basicsearch']."%' AND active=1 AND closed=0 AND deleted!=1";

$additionalVars = "&basicsearch=".$_REQUEST['basicsearch'];
if ($_GET['start'] == "") $start = 0;
else $start = $_GET['start'];
$limit = 20;
	
$totalResults = getSqlNumber($query." ORDER BY ".$orderField." ".$orderType.""); 
$resultsQuery = mysqli_query($GLOBALS["___mysqli_ston"], $query." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");
?>
<br> 
<table height="31" border="0" cellpadding="0" cellspacing="0" class="border" align="center"> 
  <tr class="c4"> 
    <form action="wanted.search.php" method="post"> 
      <td class="contentfont" nowrap>&nbsp;&nbsp;&nbsp; 
        <?=$lang[srcwanted];?> 
&nbsp;&nbsp;&nbsp;</td> 
      <td class="search" nowrap><INPUT size=25 name="basicsearch"> 
&nbsp;&nbsp;&nbsp;</td> 
      <td class="search" nowrap><input name="searchok" type="submit" value="<?=$lang[search]?>"> 
&nbsp;&nbsp;&nbsp;</td> 
    </form> 
  </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="1" cellpadding="3"> 
  <tr class="c1"> 
    <td class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>"> 
      <?=$lang[wa_name]?> 
      </a></td> 
    <td width="70" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=nrbids&orderType=<?=$newOrder.$additionalVars;?>"> 
      <?=$lang[offers]?> 
      </a></td> 
    <td width="120" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=enddate&orderType=<?=$newOrder.$additionalVars;?>"> 
      <?=$lang[endsin]?> 
      </a></td> 
  </tr> 
  <tr class="c5"> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
    <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td> 
  </tr> 
  <?
	if ($totalResults==0) {
		echo "<tr><td colspan=3 align=center>$lang[nowantedsrc]</td></tr>";	
	} else { 
		$i=0;
		while ($searchDetails = mysqli_fetch_array($resultsQuery)) { ?> 
  <tr height="25" class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>"> 
    <td class="contentfont"><a href="wanted.details.php?id=<?=$searchDetails['id'];?>"> 
      <?=$searchDetails['itemname'];?> 
      </a></td> 
    <td align="center"> <?=$searchDetails['nrbids'];?> </td> 
    <td align="center"><? 
	$daysLeft = daysleft($searchDetails['enddate'],$setts['date_format']);
	$timeLeft = timeleft($searchDetails['enddate'],$setts['date_format']);
	echo ($daysLeft>0) ? $timeLeft : $lang[adclosed]; ?></td> 
  </tr> 
  <? } ?> 
  <tr class="c4"> 
    <td colspan=3 class=contentfont align=center><? 
	paginate($start,$limit,$totalResults,"wanted.search.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); 
	?></td> 
  </tr> 
  <? } ?> 
</table> 
<? include ("themes/".$setts['default_theme']."/footer.php"); ?> 
