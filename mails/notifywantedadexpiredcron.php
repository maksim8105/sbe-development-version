<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$name.",															\n".
				"																		\n".
				"Üks Teie ostusoov oksjonil ".$setts['sitename']." on aegunud.			\n".
				"																		\n".
				"Täpsema info saamiseks logige palun sisse:								\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Täname,																\n".
				"Virtuaalse oksjoni ".$setts['sitename']."	administratsioon			\n".
				
				"																		\n".
				"----------------------------											\n".
				"																		\n".

				"Здравствуйте, ".$name.",												\n".
				"																		\n".
				"Одна из Ваших заявок на аукционе ".$setts['sitename']." устарела.		\n".
				"																		\n".
				"Для более подробной информации войдите в систему:						\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Спасибо,																\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".
				
				"																		\n".
				"----------------------------											\n".
				"																		\n".
				
				"Dear ".$name.",														\n".
				"																		\n".
				"One of your wanted ads that you have placed on ".$setts['sitename']." has expired.\n".
				"																		\n".
				"To get more details about the wanted ad that has expired, please access\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$name.",															<br>".
				"																		<br>".
				"Üks Teie ostusoov oksjonil ".$setts['sitename']." on aegunud.		<br>".
				"																		<br>".
				"Täpsema info saamiseks logige palun sisse:								<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">Kasutajakonto</a> ]	<br>".
				"																		<br>".
				"Täname,																<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"-----------------------------------									<br>".
				"																		<br>".

				"Здравствуйте, ".$name.",												<br>".
				"																		<br>".
				"Одна из Ваших заявок на аукционе ".$setts['sitename']." устарела.		<br>".
				"																		<br>".
				"Для более подробной информации войдите в систему:						<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">Аккаунт</a> ]		<br>".
				"																		<br>".
				"Спасибо,																<br>".
				"Администрация интернет-аукциона ".$setts['sitename']."					<br>".
				
				"																		<br>".
				"-----------------------------------									<br>".
				"																		<br>".
				
				"Dear ".$name.",														<br>".
				"																		<br>".
				"One of your wanted ads that you have placed on ".$setts['sitename']." has expired.<br>".
				"																		<br>".
				"To get more details about the wanted ad that has expired, please access<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">Members area</a> ]<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Wanted Ad Expired",$plainMessage,
$setts['adminemail'],$htmlMessage);

?>