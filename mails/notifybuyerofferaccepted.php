<?php
## v5.24 -> apr. 05, 2006
$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage =		"NB! Message encoding: UTF-8																			\n".
					"																												\n".
					"Hea ".$buyer['name']."																				\n".
					"																												\n".
					"Olete ostnud eseme ".$auction['itemname']." kasutades Ostusoovi võimalust.		\n".
					"																												\n".
					"Oksjoni URL: ".$setts['siteurl']."auctiondetails.php?id=".$auctionId."					\n".
					"																												\n".
					"Selle tehingu lõpetamiseks külastage palun oma kontot, valige 'Minu võidetud esemed' ja siis \n".
					"'Teadetetahvel' valitud eseme kõrval.  \n";
					"																												\n".
					"Teadetetahvel on ette nähtud Teie suhtlemiseks müüjaga.					\n".
					"																												\n".
					"Vältimaks võimalikke arusaamatusi kasutage palun alati küsimuste korral teadetetahvli.		\n".
					"																												\n".
					"Täname,																									\n".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon									\n".
					
					"																												\n".
					"-------------------------------------------------------														\n".
					"																												\n".

					"Здравствуйте, ".$buyer['name']."																				\n".
					"																												\n".
					"Вы купили лот ".$auction['itemname']." с помощью функции Заявки.		\n".
					"																												\n".
					"URL аукциона: ".$setts['siteurl']."auctiondetails.php?id=".$auctionId."					\n".
					"																												\n".
					"Для завершения сделки войдите в свой аккаунт, откройте раздел 'Выигранные мною лоты'  \n".
					"и возле выбранного лота нажмите 'Доска объявлений'.\n";
					"																												\n".
					"Доска объявлений предназначена для прямого общения с продавцом.					\n".
					"																												\n".
					"Во избежание возможных недоразумений всегда используйте доску объявлений, если у Вас есть вопросы к продавцу.	\n".
					"																												\n".
					"Спасибо,																									\n".
					"Администрация интернет-аукциона ".$setts['sitename']." 						\n".
					
					"																												\n".
					"-------------------------------------------------------														\n".
					"																												\n".
					
					"Dear ".$buyer['name']."																				\n".
					"																												\n".
					"You have purchased the ".$auction['itemname']." item using the offer feature.		\n".
					"																												\n".
					"Auction URL: ".$setts['siteurl']."auctiondetails.php?id=".$auctionId."					\n".
					"																												\n".
					"To conclude this purchase please login to the members area  buying and visit the \n".
					"\"Items Ive Won in detail\" section.  From here click on the \"Message Board\" link next to each item won.\n";
					"																												\n".
					"This message board is your direct communication board with the seller.					\n".
					"																												\n".
					"Please use this board to ask any post sale questions you may have.						\n".
					"																												\n".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.\n".
					"																												\n".
					"Thank you,																									\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage =		"NB! Message encoding: UTF-8																		<br>".
					"																												<br>".
					"Hea ".$buyer['name']."																				<br>".
					"																												<br>".
					"Olete ostnud eseme ".$auction['itemname']." kasutades Ostusoovi võimalust.		<br>".
					"																												<br>".
					"<a href=\"".$setts['siteurl']."auctiondetails.php?id=".$auctionId."\">Oksjoni vaatamiseks klikkige siia.</a><br>".
					"																												<br>".
					"Selle tehingu lõpetamiseks külastage palun oma kontot, valige 'Minu võidetud esemed' ja siis <br>".
					"'Teadetetahvel' valitud eseme kõrval.<br>";
					"																												<br>".
					"Teadetetahvel on ette nähtud Teie suhtlemiseks müüjaga.					<br>".
					"																												<br>".
					"Vältimaks võimalikke arusaamatusi kasutage palun alati küsimuste korral teadetetahvli.		<br>".
					"																												<br>".
					"Täname,																									<br>".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon						<br>".
					
					"																												<br>".
					"------------------------------------------------------------													<br>".
					"																												<br>".

					"Здравствуйте, ".$buyer['name']."																				<br>".
					"																												<br>".
					"Вы купили лот ".$auction['itemname']." с помощью функции заявки.		<br>".
					"																												<br>".
					"<a href=\"".$setts['siteurl']."auctiondetails.php?id=".$auctionId."\">Для просмотра аукциона нажмите сюда.</a><br>".
					"																												<br>".
					"Для завершения сделки войдите в свой аккаунт, откройте раздел 'Выигранные мною лоты' <br>".
					"и возле выбранного лота нажмите 'Доска объявлений'.<br>";
					"																												<br>".
					"Доска объявлений предназначена для прямого общения с продавцом.					<br>".
					"																												<br>".
					"Во избежание возможных недоразумений всегда используйте доску объявлений, если у Вас есть вопросы к продавцу.	<br>".
					"																												<br>".
					"Спасибо,																									<br>".
					"Администрация интернет-аукциона ".$setts['sitename']." 							<br>".
					
					"																												<br>".
					"------------------------------------------------------------													<br>".
					"																												<br>".
					
					"Dear ".$buyer['name']."																				<br>".
					"																												<br>".
					"You have purchased the ".$auction['itemname']." item using the offer feature.		<br>".
					"																												<br>".
					"<a href=\"".$setts['siteurl']."auctiondetails.php?id=".$auctionId."\">Click here to view the auction details page.</a><br>".
					"																												<br>".
					"To conclude this purchase please login to the members area  buying and visit the <br>".
					"\"Items Ive Won in detail\" section.  From here click on the \"Message Board\" link next to each item won.<br>";
					"																												<br>".
					"This message board is your direct communication board with the seller.					<br>".
					"																												<br>".
					"Please use this board to ask any post sale questions you may have.						<br>".
					"																												<br>".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.<br>".
					"																												<br>".
					"Thank you,																									<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Item Swap Confirmation",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>