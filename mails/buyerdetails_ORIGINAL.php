<?php
## v5.24 -> apr. 05, 2006

## commented because of the new message board feature
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
$winner = getSqlRow ("SELECT * FROM probid_winners WHERE id='".$winnerId."'");

##$sendMail = ($seller['mail_buyerdetails']==1);

$sendMail = ($seller['mail_buyerdetails']==1) ? TRUE : FALSE;

if ($setts['enable_display_phone']=="N") $buyer['phone'] = "Undisclosed";

$plainMessage ="Dear ".$seller['name'].",																\n".
					"																								\n".
					"You have sold the following item through ".$setts['sitename'].".			\n".
					"																								\n".
					"Auction # ".$auctionId."																\n".
					"Name:	".$auction['itemname']."													\n".
					"Price:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Buyer Contact Details:																	\n".
					"																								\n".
					"Name: ".$buyer['name']."																\n".
					"Address: ".$buyer['address'].", ".$buyer['city'].", ".$buyer['state'].", ".$buyer['zip'].", ".$buyer['country']."\n".
					"																								\n".
					"Phone: ".$buyer['phone']."															\n".
					"Email Address: ".$buyer['email']."													\n".
					"																								\n".
					"You can also contact the buyer in the message board. Follow the link	\n".
					$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Thank you,																					\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage = "Dear ".$seller['name'].",																<br>".
					"																								<br>".
					"You have sold the following item through <b>".$setts['sitename']."</b>.<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Auction # </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Price:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																					\n".
						"		<td>".$htmlfont."Auction Image:</td>									\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																					\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Buyer's Contact Details:</b>														<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$buyer['name']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Address:</td>												\n".
					"		<td>".$htmlfont.$buyer['address'].", ".$buyer['city'].", ".$buyer['state'].", ".$buyer['zip'].", ".$buyer['country']."</td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Phone: </td>													\n".
					"		<td>".$htmlfont.$buyer['phone']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Email Address: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$buyer['email']."\">".$buyer['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Follow the link on the right to contact the buyer in the message board</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Click Here</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Thank you,																					<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"Auction Closed - Item Sold!",$plainMessage,
$setts['adminemail'],$htmlMessage, $sendMail);
?>