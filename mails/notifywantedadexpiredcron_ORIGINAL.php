<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "Dear ".$name.",														\n".
				"																		\n".
				"One of your wanted ads that you have placed on ".$setts['sitename']." has expired.\n".
				"																		\n".
				"To get more details about the wanted ad that has expired, please access\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"Dear ".$name.",														<br>".
				"																		<br>".
				"One of your wanted ads that you have placed on ".$setts['sitename']." has expired.<br>.".
				"																		<br>".
				"To get more details about the wanted ad that has expired, please access<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">Members area</a> ]<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Wanted Ad Expired",$plainMessage,
$setts['adminemail'],$htmlMessage);

?>