<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow("SELECT id, itemname FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$name.",															\n".
				"																		\n".
				"Oksjon, kus Teie pakkumine oli kõrgeim, on suletud.					\n".
				"																		\n".
				"Võitja ei ole selgunud, kuna reservhinda ei olnud saavutatud.			\n".
				"																		\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Täname,																\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"----------------------------------------								\n".
				"																		\n".
				
				"Здравствуйте, ".$name.",												\n".
				"																		\n".
				"Аукцион, на котором Ваша ставка была наивысшей, закрылся.				\n".
				"																		\n".
				"Победитель не определен, потому что резервная цена не была достигнута.	\n".
				"																		\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Спасибо,																\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".
				
				"																		\n".
				"----------------------------------------								\n".
				"																		\n".
				
				"Dear ".$name.",														\n".
				"																		\n".
				"The following auction, on which you were the high bidder, was closed.	\n".
				"																		\n".
				"There were no winners because the reserve price was not met.			\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$name.",															<br>".
				"																		<br>".
				"Oksjon, kus Teie pakkumine oli kõrgeim, on suletud.					<br>".
				"																		<br>".
				"<table border=\"0\">													<br>".
				"	<tr>																<br>".
				"		<td>".$htmlfont."Oksjoni URL:</td>								<br>".
				"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td><br>".
				"	</tr>																<br>".
				"</table>																<br>".
				"																		<br>".
				"Täname,																<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"---------------------------------------								<br>".
				"																		<br>".

				"Здравствуйте, ".$name.",												<br>".
				"																		<br>".
				"Аукцион, на котором Ваша ставка была наивысшей, закрылся.				<br>".
				"																		<br>".
				"<table border=\"0\">													<br>".
				"	<tr>																<br>".
				"		<td>".$htmlfont."URL аукциона:</td>								<br>".
				"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td><br>".
				"	</tr>																<br>".
				"</table>																<br>".
				"																		<br>".
				"Спасибо,																<br>".
				"Администрация интернет-аукциона ".$setts['sitename']."					<br>".
				
				"																		<br>".
				"---------------------------------------								<br>".
				"																		<br>".
				
				"Dear ".$name.",														<br>".
				"																		<br>".
				"The following auction, on which you were the high bidder, was closed.	<br>".
				"																		<br>".
				"<table border=\"0\">													<br>".
				"	<tr>																<br>".
				"		<td>".$htmlfont."Auction URL:</td>								<br>".
				"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td><br>".
				"	</tr>																<br>".
				"</table>																<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Auction Closed - Item Not Won!",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>