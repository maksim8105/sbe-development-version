<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$watcher = getSqlRow ("SELECT * FROM probid_users WHERE id='".$watcherId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($watcher['mail_itemwatch']==1) ? TRUE : FALSE;

$plainMessage = "Dear Subscriber,											\n".
				"																		\n".
				"A bid has been submitted on an item that is in your Item Watch List, ".$auction['itemname']."\n".
				"																		\n".
				"To view the auction's details, please click on the following link:		\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"To view the bid history for the item, click on the following link:		\n".
				"																		\n".
				"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\n".
				"																		\n".	
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"Dear Subscriber,														<br>".
				"																		<br>".
				"A bid has been submitted on an item that is in your Item Watch List, ".$auction['itemname']."<br>".
				"																		<br>".
				"To view the auction's details, please click on the following link:		<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction</a><br>".
				"																		<br>".
				"To view the bid history for the item, click <a href=\"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\">here</a><br>".
				"																		<br>".
				"Thank you for your interest,											<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($watcher['email'],$setts['sitename']." - Item watching program",
$plainMessage,$setts['adminemail'],$htmlMessage, $sendMail);
?>