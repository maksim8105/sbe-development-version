<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

## main category
$nav = "";
$parent=$auction['category'];
if($parent > 0) {
	$croot = $parent;
	$cntr = 0;
	while ($croot>0) {
		$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT id,parent FROM probid_categories WHERE id='".$croot."' ") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$crw = mysqli_fetch_array($sbcts);
		if($cntr == 0) {
			$nav = $c_lang[$crw['id']];
		} else {
			if($parent != $croot) {
				$nav = $c_lang[$crw['id']]." > ".$nav;
			}
		}
		$cntr++;
		$croot = $crw['parent'];
	}
} 
$category_name = $nav;

## additional category
$nav = "";
$parent=$auction['addlcategory'];
if($parent > 0) {
	$croot = $parent;
	$cntr = 0;
	while ($croot>0) {
		$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT id,parent FROM probid_categories WHERE id='".$croot."' ") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$crw = mysqli_fetch_array($sbcts);
		if($cntr == 0) {
			$nav = $c_lang[$crw['id']];
		} else {
			if($parent != $croot) {
				$nav = $c_lang[$crw['id']]." > ".$nav;
			}
		}
		$cntr++;
		$croot = $crw['parent'];
	}
} 
$addl_category_name = $nav;

$sendMail = ($user['mail_conftosell']==1) ? TRUE : FALSE;
$plainMessage = "The following auction has been posted on ".$setts['sitename'].":				\n".
				"																				\n".
				"Auction Name: ".$auction['itemname']."											\n".
				"Auction Type: ".$auction['auctiontype']."										\n".
				"Quantity Offered: ".$auction['quantity']."										\n".
				"																				\n".
				"Category: ".$category_name."													\n".
				"Additional Category: ".$addl_category_name."									\n".
				"																				\n".
				"Starting Bid: ".displayAmount($auction['bidstart'],$auction['currency'])."		\n".
				"Buy Now Option: ".(($auction['bn']=="Y")?"Available":"Not Available")."		\n".
				"Reserve Price: ".(($auction['rp']=="Y")?"Set":"Not Set")."						\n".
				"																				\n".
				"Closing Date: ".displaydatetime($auction['enddate'],$setts['date_format'])."	\n".
				"																				\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."			\n".
				"																				\n".
				"Thank you for your submission.													\n".
				"The ".$setts['sitename']." staff";
				
$htmlMessage = 	"The following auction has been posted on ".$setts['sitename'].":				<br>".
				"																				<br>".
				"Auction Name: ".$auction['itemname']."											<br>".
				"Auction Type: ".$auction['auctiontype']."										<br>".
				"Quantity Offered: ".$auction['quantity']."										<br>".
				"																				<br>".
				"Category: ".$category_name."													<br>".
				"Additional Category: ".$addl_category_name."									<br>".
				"																				<br>".
				"Starting Bid: ".displayAmount($auction['bidstart'],$auction['currency'])."		<br>".
				"Buy Now Option: ".(($auction['bn']=="Y")?"Available":"Not Available")."		<br>".
				"Reserve Price: ".(($auction['rp']=="Y")?"Set":"Not Set")."						<br>".
				"																				<br>".
				"Closing Date: ".displaydatetime($auction['enddate'],$setts['date_format'])."	<br>".
				"																				<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click to view the auction</a><br>".
				"																				<br>".
				"Thank you for your submission.													<br>".
				"The ".$setts['sitename']." staff";

htmlmail($user['email'],"Auction Setup Confirmation",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>