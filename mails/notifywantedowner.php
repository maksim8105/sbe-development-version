<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$owner = getSqlRow ("SELECT * FROM probid_users WHERE id='".$ownerId."'");

$sendMail = ($owner['mail_wantedoffer']==1) ? TRUE : FALSE;
$plainMessage = "NB! Message encoding: UTF-8														\n".
				"																					\n".
				"Teie ostusoovile on tehtud pakkumine.												\n".
				"																					\n".
				"Ostusoovi URL: ".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."			\n".
				"																					\n".
				"Täname,																			\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon							\n".
				
				"																					\n".
				"-----------------------------------------------									\n".
				"																					\n".

				"На Вашу заявку поступило предложение.												\n".
				"																					\n".
				"URL заявки: ".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."				\n".
				"																					\n".
				"Спасибо,																			\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 							\n".
				
				"																					\n".
				"-----------------------------------------------									\n".
				"																					\n".
				
				"An offer has been posted on one of your wanted ads.								\n".
				"																					\n".
				"Wanted Ad URL: ".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."			\n".
				"																					\n".
				"Thank you,																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8														<br>".
				"																					<br>".
				"Teie ostusoovile on tehtud pakkumine.												<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."\">Ostusoovi vaatamiseks klikkige siia.</a><br>".
				"																					<br>".
				"Täname,																			<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon							<br>".
				
				"																					<br>".
				"---------------------------------------------										<br>".
				"																					<br>".

				"На Вашу заявку поступило предложение.												<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."\">Для просмотра заявки нажмите сюда.</a><br>".
				"																					<br>".
				"Спасибо,																			<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 							<br>".
				
				"																					<br>".
				"---------------------------------------------										<br>".
				"																					<br>".
				
				"An offer has been posted on one of your wanted ads.								<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."\">Click here to view the wanted ad details page.</a><br>".
				"																					<br>".
				"Thank you,																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($owner['email'],"New Wanted Ad Offer.",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);

?>