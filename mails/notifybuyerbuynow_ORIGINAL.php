<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($buyer['mail_auctionwon']==1) ? TRUE : FALSE;
$plainMessage ="Dear ".$buyer['name']."																				\n".
					"																												\n".
					"You have purchased the ".$auction['itemname']." item using the BUY NOW feature.		\n".
					"																												\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
					"																												\n";

if ($auction['acceptdirectpayment'])
	$plainMessage.="The seller prefers PayPal payments, and you can pay for the item						\n".
						"you won through PayPal, by accessing the won items page in your						\n".
						"members area.																							\n".
						"																											\n";

$plainMessage.="To conclude this purchase please login to the member�s area � buying and visit the \n".
					"\"Items� I�ve Won in detail\" section.  From here click on the \"Message Board\" link next to each item won.\n";
					"																												\n".
					"This message board is your direct communication board with the seller.					\n".
					"																												\n".
					"Please use this board to ask any post sale questions you may have.						\n".
					"																												\n".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.\n".
					"																												\n".
					"Thank you,																									\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage = "Dear ".$buyer['name']."																				<br>".
					"																												<br>".
					"You have purchased the ".$auction['itemname']." item using the BUY NOW feature.		<br>".
					"																												<br>";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"Auction Image:																						<br>".
						"<img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"><br>".
						"																											<br>";
}

$htmlMessage.=	"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
					"																												<br>";

if ($auction['acceptdirectpayment'])
	$htmlMessage.=	"The seller prefers PayPal payments, and you can pay for the item						<br>".
						"you won through PayPal, by accessing the won items page in your						<br>".
						"members area.																							<br>".
						"																											<br>";

$htmlMessage.=	"To conclude this purchase please login to the member�s area � buying and visit the <br>".
					"\"Items� I�ve Won in detail\" section.  From here click on the \"Message Board\" link next to each item won.<br>";
					"																												<br>".
					"This message board is your direct communication board with the seller.					<br>".
					"																												<br>".
					"Please use this board to ask any post sale questions you may have.						<br>".
					"																												<br>".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.<br>".
					"																												<br>".
					"Thank you,																									<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],$lang[buynowwonsubj],
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>