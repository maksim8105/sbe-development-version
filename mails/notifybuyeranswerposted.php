&lt;?php

## v5.24 -&gt; apr. 05, 2006
if ( !defined('INCLUDED') ) { die(&quot;Access Denied&quot;); }

$buyer = getSqlRow (&quot;SELECT * FROM probid_users WHERE id='&quot;.$questionUserId.&quot;'&quot;);
$auction = getSqlRow (&quot;SELECT * FROM probid_auctions WHERE id='&quot;.$auctionId.&quot;'&quot;);

$plainMessage = &quot;NB! Message encoding: UTF-8														\n&quot;.
				&quot;																					\n&quot;.
				&quot;Hea &quot;.$buyer['name'].&quot;																\n&quot;.
				&quot;																					\n&quot;.
				&quot;Teie küsimusele teemal &quot;.$auction['itemname'].&quot; on postitatud vastus.				\n&quot;.
				&quot;																					\n&quot;.
				&quot;Oksjoni URL: &quot;.$setts['siteurl'].processLink('auctiondetails', array('itemname' =&gt; $auction['itemname'], 'id' =&gt; $auction['id'])).&quot;				\n&quot;.
				&quot;																					\n&quot;.
				&quot;Täname,																			\n&quot;.
				&quot;Virtuaalse oksjoni &quot;.$setts['sitename'].&quot; administratsioon							\n&quot;.
				
				&quot;																					\n&quot;.
				&quot;-----------------------------------------------------------------------			\n&quot;.
				&quot;																					\n&quot;.

				&quot;Здравствуйте, &quot;.$buyer['name'].&quot;													\n&quot;.
				&quot;																					\n&quot;.
				&quot;На Ваш вопрос об аукционе &quot;.$auction['itemname'].&quot; опубликован ответ.				\n&quot;.
				&quot;																					\n&quot;.
				&quot;URL аукциона: &quot;.$setts['siteurl'].processLink('auctiondetails', array('itemname' =&gt; $auction['itemname'], 'id' =&gt; $auction['id'])).&quot;				\n&quot;.
				&quot;																					\n&quot;.
				&quot;Спасибо,																			\n&quot;.
				&quot;Администрация интернет-аукциона &quot;.$setts['sitename'].&quot; 							\n&quot;.
				
				&quot;																					\n&quot;.
				&quot;-----------------------------------------------------------------------			\n&quot;.
				&quot;																					\n&quot;.

				&quot;Dear &quot;.$buyer['name'].&quot;															\n&quot;.
				&quot;																					\n&quot;.
				&quot;An answer was posted for the question you asked regarding &quot;.$auction['itemname'].&quot;	\n&quot;.
				&quot;																					\n&quot;.
				&quot;Auction URL: &quot;.$setts['siteurl'].processLink('auctiondetails', array('itemname' =&gt; $auction['itemname'], 'id' =&gt; $auction['id'])).&quot;				\n&quot;.
				&quot;																					\n&quot;.
				&quot;Thank you,																			\n&quot;.
				&quot;The &quot;.$setts['sitename'].&quot; Staff&quot;;

$htmlMessage = 	&quot;NB! Message encoding: UTF-8														&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;Hea &quot;.$buyer['name'].&quot;																&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;Teie küsimusele teemal &quot;.$auction['itemname'].&quot;  on postitatud vastus.				&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;&lt;a href=\&quot;&quot;.$setts['siteurl'].processLink('auctiondetails', array('itemname' =&gt; $auction['itemname'], 'id' =&gt; $auction['id'])).&quot;\&quot;&gt;Oksjoni vaatamiseks klikkige siia.&lt;/a&gt;&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;Täname,																			&lt;br&gt;&quot;.
				&quot;Virtuaalse oksjoni &quot;.$setts['sitename'].&quot; administratsioon							&lt;br&gt;&quot;.
				
				&quot;																					&lt;br&gt;&quot;.
				&quot;-------------------------------------------------------------------				&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.

				&quot;Здравствуйте, &quot;.$buyer['name'].&quot;													&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;На Ваш вопрос об аукционе &quot;.$auction['itemname'].&quot;	опубликован ответ.			 &lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;&lt;a href=\&quot;&quot;.$setts['siteurl'].processLink('auctiondetails', array('itemname' =&gt; $auction['itemname'], 'id' =&gt; $auction['id'])).&quot;\&quot;&gt;Для просмотра аукциона нажмите сюда.&lt;/a&gt;&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;Спасибо,																			&lt;br&gt;&quot;.
				&quot;Администрация интернет-аукциона &quot;.$setts['sitename'].&quot; 							&lt;br&gt;&quot;.
				
				&quot;																					&lt;br&gt;&quot;.
				&quot;-------------------------------------------------------------------				&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.

				&quot;Dear &quot;.$buyer['name'].&quot;															&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;An answer was posted for the question you asked regarding &quot;.$auction['itemname'].&quot;	&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;&lt;a href=\&quot;&quot;.$setts['siteurl'].processLink('auctiondetails', array('itemname' =&gt; $auction['itemname'], 'id' =&gt; $auction['id'])).&quot;\&quot;&gt;Click here to view the auction details page.&lt;/a&gt;&lt;br&gt;&quot;.
				&quot;																					&lt;br&gt;&quot;.
				&quot;Thank you,																			&lt;br&gt;&quot;.
				&quot;The &quot;.$setts['sitename'].&quot; Staff&quot;;

htmlmail($buyer['email'],&quot;New Answer Posted on Item ID #&quot;.$auctionId,
$plainMessage,$setts['adminemail'],$htmlMessage);
?&gt;