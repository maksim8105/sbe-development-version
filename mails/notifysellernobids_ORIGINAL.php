<?php
## v5.25 -> jun. 27, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($seller['mail_auctionclosed']==1) ? TRUE : FALSE;
$plainMessage ="Dear ".$name.",																			\n".
					"																								\n".
					"Auction #".$auctionId." has closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.\n".
					"																								\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Thank you,																					\n".
					"The ".$setts['sitename']." Staff";
				
$htmlMessage =	"Dear ".$name.",																			<br>".
					"																								<br>".
					"Auction #".$auctionId." has closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n";
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Auction Image:</td>								\n".
						"		<td><img src=\"".$setts['siteurl']."makethumb.php?pic=".$setts['siteurl'].$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}
$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Thank you,																					<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($email,"Auction Closed - Item Not Sold",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);

?>