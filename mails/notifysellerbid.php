<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "NB! Message encoding: UTF-8														\n".
				"																					\n".
				"Hea ".$seller['name']."															\n".
				"																					\n".
				"Teie esemele on tehtud pakkumine, ".$auction['itemname']."							\n".
				"																					\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Täname,																			\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon							\n".
				
				"																					\n".
				"-------------------------------------------------------------------------------	\n".
				"																					\n".

				"Здравствуйте, ".$seller['name']."													\n".
				"																					\n".
				"На Ваш лот была сделана ставка, ".$auction['itemname']."							\n".
				"																					\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Спасибо,																			\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 							\n".
				
				"																					\n".
				"-------------------------------------------------------------------------------	\n".
				"																					\n".

				"Dear ".$seller['name']."															\n".
				"																					\n".
				"A new bid has been placed on your item, ".$auction['itemname']."					\n".
				"																					\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Thank you,																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8														<br>".
				"																					<br>".
				"Hea ".$seller['name']."															<br>".
				"																					<br>".
				"Teie esemele on tehtud pakkumine, ".$auction['itemname']."							<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia.</a><br>".
				"																					<br>".
				"Täname,																			<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon							<br>".
				
				"																					<br>".
				"-------------------------------------------------------------------------------	<br>".
				"																					<br>".

				"Здравствуйте, ".$seller['name']."													<br>".
				"																					<br>".
				"На Ваш лот была сделана ставка, ".$auction['itemname']."							<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Нажмите сюда для просмотра аукциона.</a><br>".
				"																					<br>".
				"Спасибо,																			<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 							<br>".
				
				"																					<br>".
				"-------------------------------------------------------------------------------	<br>".
				"																					<br>".

				"Dear ".$seller['name']."															<br>".
				"																					<br>".
				"A new bid has been placed on your item, ".$auction['itemname']."					<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
				"																					<br>".
				"Thank you,																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"New Bid Placed on Item ID #".$auctionId,
$plainMessage,$setts['adminemail'],$htmlMessage);
?>