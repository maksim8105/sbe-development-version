<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow("SELECT id, itemname FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "Dear ".$name.",														\n".
				"																		\n".
				"The following auction, on which you were the high bidder, was closed.	\n".
				"																		\n".
				"There were no winners because the reserve price was not met.			\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"Dear ".$name.",														<br>".
				"																		<br>".
				"The following auction, on which you were the high bidder, was closed.	<br>".
				"																		<br>".
				"<table border=\"0\">													\n".
				"	<tr>																\n".
				"		<td>".$htmlfont."Auction URL:</td>								\n".
				"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
				"	</tr>																\n".
				"</table>																\n".
				"																		<br>".
				"Thank you,																<br>".
				"The $sitename Staff";

htmlmail($email,"Auction Closed - Item Not Won!",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>