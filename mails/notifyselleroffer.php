<?php
## v5.24 -> apr. 05, 2006
$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage ="A new offer has been made for one of your auctions.				\n".
					"																					\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\n".
					"																					\n".
					"Quantity: ".$quantity."													\n".
					"Amount:	".displayAmount($_POST['amount'],$auction['currency'])."\n".
					"Offer expires: ".date($setts['date_format'],$expires)."			\n".
					"																					\n".
					"For further details, please access the following link:			\n".
					"																					\n".
					"".$setts['siteurl']."membersarea.php									\n".
					"																					\n".
					"and select the selling tab.												\n".
					"																					\n".
					"Thank you,																		\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage = "A new offer has been made for one of your auctions.				<br>".
					"																					<br>".
					"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
					"																					<br>".
					"Quantity: ".$quantity."													<br>".
					"Amount:	".displayAmount($_POST['amount'],$auction['currency'])."<br>".
					"Offer expires: ".date($setts['date_format'],$expires)."			<br>".
					"																					<br>".
					"For further details, please access the members area by clicking <a href=\"".$setts['siteurl']."membersarea.php\">here</a>, and select the winners contact information page.<br>".
					"																					<br>".
					"and select the selling tab.												<br>".
					"																					<br>".
					"Thank you,																		<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"Auction #".$auctionId." - New Offer",
$plainMessage,$setts['adminemail'],$htmlMessage);

?>