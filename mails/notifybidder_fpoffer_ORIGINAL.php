<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$bidderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "Dear ".$buyer['name'].",												\n".
				"																		\n".
				"A fixed price offer has been made for the following auction you have placed a bid on:\n".
				"																		\n".
				"Auction # ".$auctionId."												\n".
				"Name:	".$auction['itemname']."										\n".
				"Fixed Price Offer:	".displayAmount($auction['reserveoffer'],$auction['currency'])."\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"You can view more detailed information regarding this offer, and also accept the offer by clicking on the link below:\n".
				"																		\n".
				$setts['siteurl']."reserveoffers.php?id=".$auctionId."					\n".
				"																		\n".
				"The seller will review the accepted offers and declare a winner manually. You will be notified by email if you have won the auction.\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$buyer['name'].",												<br>".
				"																		<br>".
				"A fixed price offer has been made for the following auction you have placed a bid on:<br>".
				"																		<br>".
				"Auction # ".$auctionId."												<br>".
				"Name:	".$auction['itemname']."										<br>".
				"Fixed Price Offer:	".displayAmount($auction['reserveoffer'],$auction['currency'])."<br>".
				"Auction URL: ".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a><br>".
				"																		<br>".
				"You can view more detailed information regarding this offer, and also accept the offer by clicking on the link below:<br>".
				"																		<br>".
				"".$htmlfont."<a href=\"".$setts['siteurl']."reserveoffers.php?id=".$auctionId."\">".$setts['siteurl']."reserveoffers.php?id=".$auctionId."</a><br>".
				"																		<br>".
				"The seller will review the accepted offers and declare a winner manually. You will be notified by email if you have won the auction.<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Fixed Price Offer",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>