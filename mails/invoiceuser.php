<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$recipientId."'");
$template = getSqlRow("SELECT invoice_header, invoice_footer FROM probid_gen_setts");
$plainMessage = $template['invoice_header']
        . "Dear ".$userDetails['name'].",											\n"
				."																		\n"
				."This is an invoice for clearing your account balance with our site,	\n"
				.$setts['sitename']."													\n"
				."Your balance is: ".$setts['currency']." ".$userDetails['balance']."	\n"
				."																		\n"
				."Please follow the link below to proceed to the payment gateway:		\n"
				."( or press button DETAILS in your Member Area )						\n"
				."																		\n"
				.$setts['siteurl']."membersarea.php?page=preferences					\n"
				."																		\n"
				."Thank you in advance,													\n"
				."The ".$setts['sitename']." staff" . $template['invoice_footer'];
				
$htmlMessage = 	$template['invoice_header'] . "\n" 
        . "Dear ".$userDetails['name'].",											<br>"
				."																		<br>"
				."This is an invoice for clearing your account balance with our site, 	<br>"
				.$setts['sitename']."													<br>"
				."																		<br>"
				."Your balance is: <b>".$setts['currency']." ".$userDetails['balance']."</b><br>"
				."																		<br>"
				."Please <a href=\"".$setts['siteurl']."membersarea.php?page=preferences\">click here</a> to proceed to the payment gateway (or press the DETAILS button in your Member Area).<br>"
				."																		<br>"
				."Thank you in advance,													<br>"
				."The ".$setts['sitename']." Staff \n"
				. $template['invoice_footer'];

htmlmail($userDetails['email'],"$setts[sitename] Invoice",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>