<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "NB! Message encoding: UTF-8													\n".
				"																				\n".
				"Oksjonitasu tagasinõue:														\n".
				"																				\n".
				"Kellelt: ".$seller['name']."; Kasutajatunnus: ".$seller['username']."				\n".
				"																				\n".
				"Oksjoni ID: ".$auctionId."														\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."			\n".
				"																				\n".
				"Ostja ID: ".$buyer['id']."; Ostja kasutajatunnus: ".$buyer['username']."				\n".
				"																				\n".
				"Oksjonitasu: ".displayAmount($amount)."											\n".
				"																				\n".	
								
				"																				\n".
				"----------------------------------------------------------------------------	\n".
				"																				\n".

				"Запрос на возврат платы за окончание аукциона:									\n".
				"																				\n".
				"От кого: ".$seller['name']."; Признак пользователя: ".$seller['username']."					\n".
				"																				\n".
				"ID аукциона: ".$auctionId."														\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."			\n".
				"																				\n".
				"ID покупателя: ".$buyer['id']."; Признак пользователя покупателя: ".$buyer['username']."				\n".
				"																				\n".
				"Размер платы: ".displayAmount($amount)."											\n".
				"																				\n".	
				
				"																				\n".
				"----------------------------------------------------------------------------	\n".
				"																				\n".
				
				"End of auction fee claim back request:											\n".
				"																				\n".
				"From: ".$seller['name']."; Username: ".$seller['username']."					\n".
				"																				\n".
				"Auction ID: ".$auctionId."														\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."			\n".
				"																				\n".
				"Buyer ID: ".$buyer['id']."; Buyer Username: ".$buyer['username']."				\n".
				"																				\n".
				"Fee Amount: ".displayAmount($amount)."											\n".
				"																				\n".	
				"											";

$htmlMessage = 	"NB! Message encoding: UTF-8													<br>".
				"																				<br>".
				"Oksjonitasu tagasinõue:														<br>".
				"																				<br>".
				"Kellelt: ".$seller['name']."; Kasutajatunnus: ".$seller['username']."			<br>".
				"																				<br>".
				"Oksjoni ID: ".$auctionId."														<br>".
				"Oksjoni URL: <a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia</a><br>".
				"																				<br>".
				"Ostja ID: ".$buyer['id']."; Ostja kasutajatunnus: ".$buyer['username']."		<br>".
				"																				<br>".
				"Oksjonitasu: ".displayAmount($amount)."										<br>".
				
				"																				<br>".
				"------------------------------------------------------------------------------	<br>".
				"																				<br>".
				
				"Запрос на возврат платы за окончание аукциона:									<br>".
				"																				<br>".
				"От кого: ".$seller['name']."; Признак пользователя: ".$seller['username']."	<br>".
				"																				<br>".
				"ID аукциона: ".$auctionId."														<br>".
				"URL аукциона: <a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Нажмите сюда для просмотра аукциона</a><br>".
				"																				<br>".
				"ID покупателя: ".$buyer['id']."; Признак пользователя покупателя: ".$buyer['username']."				<br>".
				"																				<br>".
				"Размер платы: ".displayAmount($amount)."											<br>".
				
				"																				<br>".
				"------------------------------------------------------------------------------	<br>".
				"																				<br>".

				"End of auction fee claim back request:											<br>".
				"																				<br>".
				"From: ".$seller['name']."; Username: ".$seller['username']."					<br>".
				"																				<br>".
				"Auction ID: ".$auctionId."														<br>".
				"Auction URL: <a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction</a><br>".
				"																				<br>".
				"Buyer ID: ".$buyer['id']."; Buyer Username: ".$buyer['username']."				<br>".
				"																				<br>".
				"Fee Amount: ".displayAmount($amount)."											<br>".
				"																				<br>".	
				"																";

htmlmail($setts['adminemail'],"End of Auction Fee Claim Back Request",
$plainMessage,$seller['email'],$htmlMessage);
?>