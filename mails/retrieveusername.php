&lt;?
## v5.24 -&gt; apr. 05, 2006
if ( !defined('INCLUDED') ) { die(&quot;Access Denied&quot;); }

$user = getSqlRow (&quot;SELECT * FROM probid_users WHERE id='&quot;.$userId.&quot;'&quot;);

$plainMessage = &quot;NB! Message encoding: UTF-8											\n&quot;.
				&quot;																		\n&quot;.
				&quot;Hea kasutaja,															\n&quot;.
				&quot;																		\n&quot;.
				&quot;Teie sisselogimise info oksjonil &quot;.$setts['sitename'].&quot; on:			\n&quot;.
				&quot;																		\n&quot;.
				&quot;	Kasutajanimi: &quot;.$user['username'].&quot;									\n&quot;.
				&quot;																		\n&quot;.
				&quot;Lugupidamisega,														\n&quot;.
				&quot;Virtuaalse oksjoni &quot;.$setts['sitename'].&quot; administratsioon				\n&quot;.
				
				&quot;																		\n&quot;.
				&quot;-------------------------------										\n&quot;.
				&quot;																		\n&quot;.

				&quot;Уважаемый пользователь,												\n&quot;.
				&quot;																		\n&quot;.
				&quot;Ваша пользовательская информация на аукционе &quot;.$setts['sitename'].&quot;:	\n&quot;.
				&quot;																		\n&quot;.
				&quot;	Имя пользователя: &quot;.$user['username'].&quot;								\n&quot;.
				&quot;																		\n&quot;.
				&quot;С уважением,															\n&quot;.
				&quot;Администрация интернет-аукциона &quot;.$setts['sitename'].&quot;					\n&quot;.
				
				&quot;																		\n&quot;.
				&quot;-------------------------------										\n&quot;.
				&quot;																		\n&quot;.
				
				&quot;Dear Subscriber,														\n&quot;.
				&quot;																		\n&quot;.
				&quot;Your login information to &quot;.$setts['sitename'].&quot; is:					\n&quot;.
				&quot;																		\n&quot;.
				&quot;	Username: &quot;.$user['username'].&quot;										\n&quot;.
				&quot;																		\n&quot;.
				&quot;Best Regards,															\n&quot;.
				&quot;The &quot;.$setts['sitename'].&quot; Staff&quot;;

$htmlMessage = 	&quot;NB! Message encoding: UTF-8											&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;Hea kasutaja,															&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;Teie sisselogimise info oksjonil &quot;.$setts['sitename'].&quot; on:			&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;	Kasutajanimi: &quot;.$user['username'].&quot;									&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;Lugupidamisega,														&lt;br&gt;&quot;.
				&quot;Virtuaalse oksjoni &quot;.$setts['sitename'].&quot; administratsioon				&lt;br&gt;&quot;.
				
				&quot;																		&lt;br&gt;&quot;.
				&quot;----------------------------											&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.

				&quot;Уважаемый пользователь,												&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;Ваша пользовательская информация на аукционе &quot;.$setts['sitename'].&quot;:	&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;	Имя пользователя: &quot;.$user['username'].&quot;								&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;С уважением,															&lt;br&gt;&quot;.
				&quot;Администрация интернет-аукциона &quot;.$setts['sitename'].&quot; Staff			&lt;br&gt;&quot;.
				
				&quot;																		&lt;br&gt;&quot;.
				&quot;----------------------------											&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				
				&quot;Dear Subscriber,														&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;Your login information to &quot;.$setts['sitename'].&quot; is:					&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;	Username: &quot;.$user['username'].&quot;										&lt;br&gt;&quot;.
				&quot;																		&lt;br&gt;&quot;.
				&quot;Best Regards,															&lt;br&gt;&quot;.
				&quot;The &quot;.$setts['sitename'].&quot; Staff&quot;;

htmlmail($user['email'],$setts['sitename'].&quot; Login Information&quot;,
$plainMessage,$setts['adminemail'],$htmlMessage);
?&gt;