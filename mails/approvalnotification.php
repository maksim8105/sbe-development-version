<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$auction['ownerid']."'");

##$sendMail = ($user['mail_conftosell']==1) ? TRUE : FALSE;
$sendMail = TRUE;

$plainMessage ="The following auction you have posted on ".$setts['sitename']." has been approved by the administrators and its now active:				\n".
					"																								\n".
					"Auction Name: ".$auction['itemname']."											\n".
					"Auction Type: ".$auction['auctiontype']."										\n".
					"Quantity Offered: ".$auction['quantity']."										\n".
					"																								\n".
					"Starting Bid: ".displayAmount($auction['bidstart'],$auction['currency'])."\n".
					"Buy Now/Make Offer Option: ".(($auction['bn']=="Y")?"Available":"Not Available")."\n".
					"Reserve Price: ".(($auction['rp']=="Y")?"Set":"Not Set")."					\n".
					"																								\n".
					"Closing Date: ".displaydatetime($auction['enddate'],$setts['date_format'])."\n".
					"																								\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."			\n".
					"																								\n".
					"Thank you for your submission.														\n".
					"The ".$setts['sitename']." staff";
				
$htmlMessage = "The following auction you have posted on ".$setts['sitename']." has been approved by the administrators and its now active:				<br>".
					"																								<br>".
					"Auction Name: ".$auction['itemname']."											<br>".
					"Auction Type: ".$auction['auctiontype']."										<br>".
					"Quantity Offered: ".$auction['quantity']."										<br>".
					"																								<br>".
					"Starting Bid: ".displayAmount($auction['bidstart'],$auction['currency'])."<br>".
					"Buy Now Option: ".(($auction['bn']=="Y")?"Available":"Not Available")."<br>".
					"Reserve Price: ".(($auction['rp']=="Y")?"Set":"Not Set")."					<br>".
					"																								<br>".
					"Closing Date: ".displaydatetime($auction['enddate'],$setts['date_format'])."<br>".
					"																								<br>".
					"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click to view the auction</a><br>".
					"																								<br>".
					"Thank you for your submission.														<br>".
					"The ".$setts['sitename']." staff";

htmlmail($user['email'],"Auction Approval Confirmation",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>