<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$bidderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$buyer['name'].",												\n".
				"																		\n".
				"Oksjonile, millele olete teinud pakkumise, on tehtud fikseeritud hinnapakkumine: \n".
				"																		\n".
				"Oksjoni ID: ".$auctionId."												\n".
				"Nimetus:	".$auction['itemname']."										\n".
				"Fikseeritud hinnapakkumine:	".displayAmount($auction['reserveoffer'],$auction['currency'])."\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Hinnapakkumise vaatamiseks ja hinnapakkumise aktsepteerimiseks külastage linki :\n".
				"																		\n".
				$setts['siteurl']."reserveoffers.php?id=".$auctionId."					\n".
				"																		\n".
				"Müüja vaatab pakkumised üle ja kulutab võitjat. Saate teavituse e-postile, kui olete oksjoni võitnud.\n".
				"																		\n".
				"Täname,																\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"-------------------------------------------------------				\n".
				"																		\n".

				"Здравствуйте, ".$buyer['name'].",										\n".
				"																		\n".
				"На аукцион, на котором Вы делали ставку, поступило предложение фиксированной цены:\n".
				"																		\n".
				"ID аукциона: ".$auctionId."											\n".
				"Название:	".$auction['itemname']."									\n".
				"Фиксированная цена:	".displayAmount($auction['reserveoffer'],$auction['currency'])."\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Для просмотра предложения и его принятия пройдите по ссылке:			\n".
				"																		\n".
				$setts['siteurl']."reserveoffers.php?id=".$auctionId."					\n".
				"																		\n".
				"Продавец просмотрит принятые предложения и объявит победителя. Вам будет отправлено сообщение, если Вы окажетесь победителем.\n".
				"																		\n".
				"Спасибо,																\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".
				
				"																		\n".
				"-------------------------------------------------------				\n".
				"																		\n".

				"Dear ".$buyer['name'].",												\n".
				"																		\n".
				"A fixed price offer has been made for the following auction you have placed a bid on:\n".
				"																		\n".
				"Auction # ".$auctionId."												\n".
				"Name:	".$auction['itemname']."										\n".
				"Fixed Price Offer:	".displayAmount($auction['reserveoffer'],$auction['currency'])."\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"You can view more detailed information regarding this offer, and also accept the offer by clicking on the link below:\n".
				"																		\n".
				$setts['siteurl']."reserveoffers.php?id=".$auctionId."					\n".
				"																		\n".
				"The seller will review the accepted offers and declare a winner manually. You will be notified by email if you have won the auction.\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$buyer['name'].",												<br>".
				"																		<br>".
				"Oksjonile, millele olete teinud pakkumise, on tehtud fikseeritud hinnapakkumine:<br>".
				"																		<br>".
				"Oksjoni ID: ".$auctionId."												<br>".
				"Nimetus:	".$auction['itemname']."									<br>".
				"Fikseeritud hinnapakkumine:	".displayAmount($auction['reserveoffer'],$auction['currency'])."<br>".
				"Oksjoni URL: ".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a><br>".
				"																		<br>".
				"Hinnapakkumise vaatamiseks ja hinnapakkumise aktsepteerimiseks külastage linki: <br>".
				"																		<br>".
				"".$htmlfont."<a href=\"".$setts['siteurl']."reserveoffers.php?id=".$auctionId."\">".$setts['siteurl']."reserveoffers.php?id=".$auctionId."</a><br>".
				"																		<br>".
				"Müüja vaatab pakkumised üle ja kulutab võitjat. Saate teavituse e-postile, kui olete oksjoni võitnud.<br>".
				"																		<br>".
				"Täname,																<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"--------------------------------------------------------				<br>".
				"																		<br>".

				"Здравствуйте, ".$buyer['name'].",										<br>".
				"																		<br>".
				"На аукцион, на котором Вы делали ставку, поступило предложение фиксированной цены:<br>".
				"																		<br>".
				"ID аукциона: ".$auctionId."											<br>".
				"Название:	".$auction['itemname']."									<br>".
				"Фиксированная цена:	".displayAmount($auction['reserveoffer'],$auction['currency'])."<br>".
				"URL аукциона: ".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a><br>".
				"																		<br>".
				"Для просмотра предложения и его принятия пройдите по ссылке:			<br>".
				"																		<br>".
				"".$htmlfont."<a href=\"".$setts['siteurl']."reserveoffers.php?id=".$auctionId."\">".$setts['siteurl']."reserveoffers.php?id=".$auctionId."</a><br>".
				"																		<br>".
				"Продавец просмотрит принятые предложения и объявит победителя. Вам будет отправлено сообщение, если Вы окажетесь победителем.<br>".
				"																		<br>".
				"Спасибо,																<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 				<br>".
				
				"																		<br>".
				"--------------------------------------------------------				<br>".
				"																		<br>".

				"Dear ".$buyer['name'].",												<br>".
				"																		<br>".
				"A fixed price offer has been made for the following auction you have placed a bid on:<br>".
				"																		<br>".
				"Auction # ".$auctionId."												<br>".
				"Name:	".$auction['itemname']."										<br>".
				"Fixed Price Offer:	".displayAmount($auction['reserveoffer'],$auction['currency'])."<br>".
				"Auction URL: ".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a><br>".
				"																		<br>".
				"You can view more detailed information regarding this offer, and also accept the offer by clicking on the link below:<br>".
				"																		<br>".
				"".$htmlfont."<a href=\"".$setts['siteurl']."reserveoffers.php?id=".$auctionId."\">".$setts['siteurl']."reserveoffers.php?id=".$auctionId."</a><br>".
				"																		<br>".
				"The seller will review the accepted offers and declare a winner manually. You will be notified by email if you have won the auction.<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Fixed Price Offer",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>