<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($seller['mail_auctionsold']==1) ? TRUE : FALSE;
$plainMessage ="Dear ".$name.",																							\n".
					"																												\n".
					"One of your items has been sold on ".$setts['sitename'].".									\n".
					"																												\n".
					"Auction # ".$auctionId."																				\n".
					"Name:	".$auctionName."																				\n".
					"Price:	".displayAmount($maxBid)."																	\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																												\n".
					"To conclude this sale please login to the member�s area - selling and visit the 	\n".
					"\"Item�s I've Sold in detail\" section.  From here click on the \"Message Board\" 	\n".
					"link next to each item sold.																			\n".
					"																												\n".
					"This message board is your direct communication board with your buyer.					\n".
					"																												\n".
					"Please use this board to answer any questions the buyer may have regarding payment and delivery.\n".
					"																												\n".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.\n".
					"																												\n".
					"Thank you,																									\n".
					"The ".$setts['sitename']." Staff";
				
$htmlMessage = "Dear ".$name.",																							<br>".
					"																												<br>".
					"One of your items has been sold on <b>".$setts['sitename']."</b>.						<br>".
					"																												<br>".
					"<table border=\"0\">																					\n".
					"	<tr>																										\n".
					"		<td>".$htmlfont."Auction # </td>																\n".
					"		<td>".$htmlfont.$auctionId."</td>															\n".
					"	</tr>																										\n".
					"	<tr>																										\n".
					"		<td>".$htmlfont."Name:</td>																	\n".
					"		<td>".$htmlfont.$auctionName."</td>															\n".
					"	</tr>																										\n".
					"	<tr>																										\n".
					"		<td>".$htmlfont."Price:</td>																	\n".
					"		<td>".$htmlfont.displayAmount($maxBid)."</td>											\n".
					"	</tr>																										\n";
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																									\n".
						"		<td>".$htmlfont."Auction Image:</td>													\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																									\n";
}
$htmlMessage.=	"	<tr>																										\n".
					"		<td>".$htmlfont."Auction URL:</td>															\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																										\n".
					"</table>																									\n".
					"																												<br>".
					"To conclude this sale please login to the member�s area - selling and visit the 	<br>".
					"\"Item�s I've Sold in detail\" section.  From here click on the \"Message Board\" 	<br>".
					"link next to each item sold.																			<br>".
					"																												<br>".
					"This message board is your direct communication board with your buyer.					<br>".
					"																												<br>".
					"Please use this board to answer any questions the buyer may have regarding payment and delivery.<br>".
					"																												<br>".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.<br>".
					"																												<br>".
					"Thank you,																									<br>".
					"The $sitename Staff";

htmlmail($email,"Auction Closed",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);

?>