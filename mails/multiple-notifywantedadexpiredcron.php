<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$name.",															\n".
				"																		\n".
				"Mitu Teie ostusoovi lehel ".$setts['sitename']." on aegunud.			\n".
				"																		\n".
				"Aegunud ostusoovide vaatamiseks külastage palun linki 					\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Täname,																\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"-------------------------------------------------------				\n".
				"																		\n".

				"Здравствуйте, ".$name.",												\n".
				"																		\n".
				"Несколько Ваших заявок на странице ".$setts['sitename']." устарели.	\n".
				"																		\n".
				"Для просмотра устаревших заявок пройдите по ссылке						\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Благодарим,															\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".
				
				"																		\n".
				"-------------------------------------------------------				\n".
				"																		\n".
				
				"Dear ".$name.",														\n".
				"																		\n".
				"Several wanted ads that you have placed on ".$setts['sitename']." have expired.\n".
				"																		\n".
				"To get more details about the wanted ads that have expired, please access\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$name.",															<br>".
				"																		<br>".
				"Mitu Teie ostusoovi lehel ".$setts['sitename']." on aegunud.			<br>.".
				"																		<br>".
				"Aegunud ostusoovide vaatamiseks külastage palun 						<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">oma kasutajakonto</a> ]<br>".
				"																		<br>".
				"Täname,																<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"---------------------------------------------------------------		<br>".
				"																		<br>".

				"Здравствуйте, ".$name.",												<br>".
				"																		<br>".
				"Несколько Ваших заявок на странице ".$setts['sitename']." устарели.	<br>.".
				"																		<br>".
				"Для просмотра устаревших заявок зайдите								<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">в свой аккаунт</a> ]<br>".
				"																		<br>".
				"Благодарим,															<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 				<br>".
				
				"																		<br>".
				"---------------------------------------------------------------		<br>".
				"																		<br>".
				
				"Dear ".$name.",														<br>".
				"																		<br>".
				"Several wanted ads that you have placed on ".$setts['sitename']." have expired.<br>.".
				"																		<br>".
				"To get more details about the wanted ad that have expired, please access<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">Members area</a> ]<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Multiple Wanted Ads Expired",$plainMessage,
$setts['adminemail'],$htmlMessage);

?>