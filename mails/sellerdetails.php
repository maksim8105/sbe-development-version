<?php
## v5.24 -> apr. 05, 2006
## commented because of the new message board feature
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
$winner = getSqlRow ("SELECT * FROM probid_winners WHERE id='".$winnerId."'");

$sendMail = ($buyer['mail_sellerdetails']==1);

## $sendMail = ($buyer['mail_sellerdetails']==1) ? TRUE : FALSE;

if ($setts['enable_display_phone']=="N") $seller['phone'] = "Undisclosed";


$plainMessage =		"NB! Message encoding: UTF-8										\n".
					"																								\n".
					"Hea ".$buyer['name'].",																\n".
					"																								\n".
					"Olete võitnud oksjoni süsteemis ".$setts['sitename'].".			\n".
					"																								\n".
					"Oksjoni ID:	".$auctionId."																\n".
					"Nimetus:	".$auction['itemname']."													\n".
					"Hind:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Müüja kontaktinfo:														\n".
					"																								\n".
					"Kasutajatunnus: ".$seller['username']."																\n".
					"E-posti aadress: ".$seller['email']."												\n".
					"Telefon: ".$seller['phone']."															\n".
					"																								\n".
					"Võite samuti müüjaga ühendust võtta meie teadete tahvli abil. Külastage linki  \n".
					"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."	\n".
					"																								\n".
					"Kohaletoimetamine:																			\n".
					"																								\n".
					"Postikulud: ".$auction['postage_costs']."									\n".
					"Kindlustus: ".$seller['insurance']."												\n".
					"Teenuse tüüp: ".$seller['type_service']."									\n".
					"																								\n".
					"Täname,																					\n".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon									\n".
					
					"																								\n".
					"----------------------------------------------------------------------	\n".
					"																								\n".

					"Здравствуйте, ".$buyer['name'].",																\n".
					"																								\n".
					"Вы выиграли аукцион на странице ".$setts['sitename'].".			\n".
					"																								\n".
					"ID аукциона:	".$auctionId."																\n".
					"Название:	".$auction['itemname']."													\n".
					"Цена:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Контактные данные продавца:																\n".
					"																								\n".
					"Признак пользователя: ".$seller['username']."																\n".
					"E-mail: ".$seller['email']."												\n".
					"Телефон: ".$seller['phone']."															\n".
					"																								\n".
					"Вы можете также связаться с продавцом через нашу доску объявлений. Пройдите по ссылке  \n".
					"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Доставка:																			\n".
					"																								\n".
					"Почтовые расходы: ".$auction['postage_costs']."									\n".
					"Страховка: ".$seller['insurance']."												\n".
					"Тип услуги: ".$seller['type_service']."									\n".
					"																								\n".
					"Спасибо,																					\n".
					"Администрация интернет-аукциона ".$setts['sitename']." 								\n".
								
					"																								\n".
					"----------------------------------------------------------------------	\n".
					"																								\n".
					
					"Dear ".$buyer['name'].",																\n".
					"																								\n".
					"You have won the following item through ".$setts['sitename'].".			\n".
					"																								\n".
					"Auction ID:	".$auctionId."																\n".
					"Name:	".$auction['itemname']."													\n".
					"Price:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Seller Contact Details:																\n".
					"																								\n".
					"Login: ".$seller['username']."																\n".
					"Phone: ".$seller['phone']."															\n".
					"Email Address: ".$seller['email']."												\n".
					"																								\n".
					"You can also contact the seller in the message board. Follow the link  \n".
					"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Postage Details:																			\n".
					"																								\n".
					"Postage Costs: ".$auction['postage_costs']."									\n".
					"Insurance: ".$seller['insurance']."												\n".
					"Type of Service: ".$seller['type_service']."									\n".
					"																								\n".
					"Thank you,																					\n".
					"The ".$setts['sitename']." Staff";


$htmlMessage =		"NB! Message encoding: UTF-8													<br>".
					"																								<br>".
					"Hea ".$buyer['name'].",																<br>".
					"																								<br>".
					"Olete võitnud oksjoni süsteemis <b>".$setts['sitename']."</b>.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Oksjoni ID: </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Nimetus:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Hind:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";
					
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Oksjoni pilt:</td>								\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Oksjoni URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Müüja kontaktinfo:</b>													<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Kasutajanimi:</td>													\n".
					"		<td>".$htmlfont.$seller['username']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."E-posti aadress: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$seller['email']."\">".$seller['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Telefon: </td>													\n".
					"		<td>".$htmlfont.$seller['phone']."</td>									\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Klikkige paremal olevale lingile, et võtta müüjaga ühendust meie teadete tahvli abil</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Klikkige siia</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Kohaletoimetamine:</b>																<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Postikulud: </td>										\n".
					"		<td>".$htmlfont.$auction['postage_costs']."</td>						\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Kindlustus: </td>											\n".
					"		<td>".$htmlfont.$auction['insurance']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Teenuse tüüp: </td>									\n".
					"		<td>".$htmlfont.$auction['type_service']."</td>							\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Täname,																					<br>".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon															<br>".
					
					"																								<br>".
					"----------------------------------------------------------------	<br>".
					"																								<br>".

					"Здравствуйте, ".$buyer['name'].",																<br>".
					"																								<br>".
					"Вы выиграли аукцион на странице <b>".$setts['sitename']."</b>.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."ID аукциона: </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Название:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Цена:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";
					
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Изображение:</td>								\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."URL аукциона:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Контактные данные продавца:</b>													<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Признак пользователя:</td>													\n".
					"		<td>".$htmlfont.$seller['username']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."E-mail: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$seller['email']."\">".$seller['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Телефон: </td>													\n".
					"		<td>".$htmlfont.$seller['phone']."</td>									\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Нажмите на ссылку справа, чтобы связаться с продавцом через нашу доску объявлений</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Нажмите сюда</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Доставка:</b>																<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Почтовые расходы: </td>										\n".
					"		<td>".$htmlfont.$auction['postage_costs']."</td>						\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Страховка: </td>											\n".
					"		<td>".$htmlfont.$auction['insurance']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Тип доставки: </td>									\n".
					"		<td>".$htmlfont.$auction['type_service']."</td>							\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Спасибо,																					<br>".
					"Администрация интернет-аукциона ".$setts['sitename']." 						<br>".
					
					"																								<br>".
					"----------------------------------------------------------------	<br>".
					"																								<br>".
					
					"Dear ".$buyer['name'].",																<br>".
					"																								<br>".
					"You have won the following item through <b>".$setts['sitename']."</b>.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Auction # </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Price:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";
					
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Auction Image:</td>								\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Seller's Contact Details:</b>													<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Login:</td>													\n".
					"		<td>".$htmlfont.$seller['username']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Phone: </td>													\n".
					"		<td>".$htmlfont.$seller['phone']."</td>									\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Email Address: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$seller['email']."\">".$seller['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Follow the link on the right to contact the seller in the message board</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Click Here</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Postage Details:</b>																<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Postage Costs: </td>										\n".
					"		<td>".$htmlfont.$auction['postage_costs']."</td>						\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Insurance: </td>											\n".
					"		<td>".$htmlfont.$auction['insurance']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Type of Service: </td>									\n".
					"		<td>".$htmlfont.$auction['type_service']."</td>							\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Thank you,																					<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Auction Closed - Item Won!",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);
?>