<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$owner = getSqlRow ("SELECT * FROM probid_users WHERE id='".$ownerId."'");
$sender = getSqlRow ("SELECT * FROM probid_users WHERE id='".$senderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "NB! Message encoding: UTF-8												\n".
				"																			\n".
				"Tere, ".$_POST['friendname'].",											\n".
				"																			\n".
				"Teie sõber, ".$sender['name'].", on saatnud Teile vaatamiseks oksjoni:		\n".
				"																			\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."		\n".
				"																			\n".
				"Lisakommentaar: ".$_POST['comments']."										\n".
				"																			\n".
				"Parimate soovidega,														\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon					\n".
				
				"																			\n".
				"-----------------------------------------------------------------------	\n".
				"																			\n".

				"Здравствуйте, ".$_POST['friendname'].",											\n".
				"																			\n".
				"Ваш друг, ".$sender['name'].", отправил Вам для просмотра аукцион:			\n".
				"																			\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."		\n".
				"																			\n".
				"Дополнительные комментарии: ".$_POST['comments']."							\n".
				"																			\n".
				"С наилучшими пожеланиями,																\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 					\n".
				
				"																			\n".
				"-----------------------------------------------------------------------	\n".
				"																			\n".

				"Hello ".$_POST['friendname'].",											\n".
				"																			\n".
				"Your friend, ".$sender['name'].", has sent you this auction for you to look at:\n".
				"																			\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."		\n".
				"																			\n".
				"Additional comments: ".$_POST['comments']."								\n".
				"																			\n".
				"Best regards,																\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8												<br>".
				"																			<br>".
				"Tere, ".$_POST['friendname'].",											<br>".
				"																			<br>".
				"Teie sõber, ".$sender['name'].", on saatnud Teile vaatamiseks oksjoni:		<br>".
				"																			<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia</a><br>".
				"																			<br>".
				"Lisakommentaar: ".$_POST['comments']."										<br>".
				"																			<br>".
				"Parimate soovidega,														<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon					<br>".
				
				"																			<br>".
				"-----------------------------------------------------------------------	<br>".
				"																			<br>".

				"Здравствуйте, ".$_POST['friendname'].",											<br>".
				"																			<br>".
				"Ваш друг, ".$sender['name'].", отправил Вам для просмотра аукцион:			<br>".
				"																			<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Нажмите сюда для просмотра аукциона</a><br>".
				"																			<br>".
				"Дополнительные комментарии: ".$_POST['comments']."							<br>".
				"																			<br>".
				"С наилучшими пожеланиями,													<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 					<br>".
				
				"																			<br>".
				"-----------------------------------------------------------------------	<br>".
				"																			<br>".
				
				"Hello ".$_POST['friendname'].",											<br>".
				"																			<br>".
				"Your friend, ".$sender['name'].", has sent you this auction for you to look at:<br>".
				"																			<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click to view the auction</a><br>".
				"																			<br>".
				"Additional comments: ".$_POST['comments']."								<br>".
				"																			<br>".
				"Best regards,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($_POST['friendemail'],"Check out this Auction",
$plainMessage,$sender['email'],$htmlMessage);
?>