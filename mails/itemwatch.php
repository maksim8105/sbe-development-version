<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$watcher = getSqlRow ("SELECT * FROM probid_users WHERE id='".$watcherId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($watcher['mail_itemwatch']==1) ? TRUE : FALSE;

$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea kasutaja,															\n".
				"																		\n".
				"Teie jälgimislistis olevale esemele on tehtud pakkumine, ".$auction['itemname']."\n".
				"																		\n".
				"Oksjoni vaatamiseks külastage palun järgmist linki:					\n".
				"																		\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Pakkumiste ajaloo vaatamiseks külastage palun järgmist linki:			\n".
				"																		\n".
				"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\n".
				"																		\n".	
				"Täname tähelepanu eest,												\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon										\n".
				
				"																		\n".
				"-------------------------------------------------------------------	\n".
				"																		\n".

				"Уважаемый пользователь,												\n".
				"																		\n".
				"На аукционе, который Вы отслеживаете, была сделана ставка, ".$auction['itemname']."\n".
				"																		\n".
				"Для просмотра аукциона пройдите по ссылке:								\n".
				"																		\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Для просмотра истории всех ставок пройдите по ссылке:					\n".
				"																		\n".
				"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\n".
				"																		\n".	
				"Благодарим за внимание,												\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 										\n".
				
				"																		\n".
				"-------------------------------------------------------------------	\n".
				"																		\n".

				"Dear Subscriber,											\n".
				"																		\n".
				"A bid has been submitted on an item that is in your Item Watch List, ".$auction['itemname']."\n".
				"																		\n".
				"To view the auction's details, please click on the following link:		\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"To view the bid history for the item, click on the following link:		\n".
				"																		\n".
				"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\n".
				"																		\n".	
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea kasutaja,															<br>".
				"																		<br>".
				"Teie jälgimislistis olevale esemele on tehtud pakkumine, ".$auction['itemname']."<br>".
				"																		<br>".
				"Oksjoni vaatamiseks külastage palun järgmist linki:					<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia</a><br>".
				"																		<br>".
				"Pakkumiste ajaloo vaatamiseks klikkige palun <a href=\"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\">siia</a><br>".
				"																		<br>".
				"Täname tähelepanu eest,												<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"-------------------------------------------------------------------	<br>".
				"																		<br>".

				"Уважаемый пользователь,												<br>".
				"																		<br>".
				"На аукционе, который Вы отслеживаете, была сделана ставка, ".$auction['itemname']."<br>".
				"																		<br>".
				"Для просмотра аукциона пройдите по ссылке:								<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Нажмите сюда для просмотра аукциона</a><br>".
				"																		<br>".
				"Для просмотра истории всех ставок нажмите <a href=\"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\">сюда</a><br>".
				"																		<br>".
				"Благодарим за внимание,												<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 				<br>".
				
				"																		<br>".
				"-------------------------------------------------------------------	<br>".
				"																		<br>".
				
				"Dear Subscriber,														<br>".
				"																		<br>".
				"A bid has been submitted on an item that is in your Item Watch List, ".$auction['itemname']."<br>".
				"																		<br>".
				"To view the auction's details, please click on the following link:		<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction</a><br>".
				"																		<br>".
				"To view the bid history for the item, click <a href=\"".$setts['siteurl']."bidhistory.php?id=".$auctionId."&name=".$auction['itemname']."&quantity=".$_POST['quantity']."\">here</a><br>".
				"																		<br>".
				"Thank you for your interest,											<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($watcher['email'],$setts['sitename']." - Item watching program",
$plainMessage,$setts['adminemail'],$htmlMessage, $sendMail);
?>