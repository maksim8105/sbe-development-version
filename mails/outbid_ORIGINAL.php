<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$bidder = getSqlRow ("SELECT * FROM probid_users WHERE id='".$bidderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($bidder['mail_outbid']==1) ? TRUE : FALSE;
$plainMessage = "Dear ".$bidder['name'].",												\n".
				"																		\n".
				"You have been outbid on an auction you placed a bid on.				\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"You can place another bid using the bid placement form from the bottom	\n". 
				"of the auction details page.											\n".
				"																		\n".
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$bidder['name'].",												<br>".
				"																		<br>".
				"You have been outbid on an auction you placed a bid on.				<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction</a><br>".
				"																		<br>".
				"You can place another bid using the bid placement form from the bottom	<br>". 
				"of the auction details page.											<br>".
				"																		<br>".
				"Thank you for your interest,											<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($bidder['email'],$setts['sitename']." - Outbid Notice",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>