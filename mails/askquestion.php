<?php 
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$owner = getSqlRow ("SELECT * FROM probid_users WHERE id='".$ownerId."'");
$sender = getSqlRow ("SELECT * FROM probid_users WHERE id='".$senderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage ="".$lang[mess_send_seller]."									\n".
					"																		\n".
					"URL: ".$setts['siteurl']."".processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																		\n".
					" ".$message;

$htmlMessage =	"".$lang[mess_send_seller]."									<br>".
					"																		<br>".
					"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$lang[retdetailspage]."</a><br>".
					"																		<br>".
					" ".$message;

htmlmail($owner['email'],$lang[question_for_seller]." ".$auction['itemname']." from ".$sender['name'],
$plainMessage,$sender['email'],$htmlMessage);
?>