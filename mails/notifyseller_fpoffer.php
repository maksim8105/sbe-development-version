<?php
## v5.24 -> apr. 10, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "Dear ".$seller['name']."																					\n".
					 "																													\n".
					 "A fixed price offer was accepted for your item, ".$auction['itemname']."					\n".
					 "																													\n".
					 "Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."					\n".
					 "																													\n".
					 "Reserve Offer URL: ".$setts['siteurl']."reserveoffers.php?id=".$auctionId."				\n".
					 "																													\n".
					 "Thank you,																									\n".
					 "The ".$setts['sitename']." Staff";

$htmlMessage =  "Dear ".$seller['name']."																					<br>".
					 "																													<br>".
					 "A fixed price offer was accepted for your item, ".$auction['itemname']."					<br>".
					 "																													<br>".
					 "<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
					 "																													<br>".
					 "<a href=\"".$setts['siteurl']."reserveoffers.php?id=".$auctionId."\">Click here to view the reserve offers page for this auction.</a><br>".
					 "																													<br>".
					 "Thank you,																									<br>".
					 "The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"Reserve offer accepted for Item ID #".$auctionId,
$plainMessage,$setts['adminemail'],$htmlMessage);
?>