<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Full Page Editing - CKEditor Sample</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
	<script type="text/javascript" src="scripts/ckeditor/ckeditor.js"></script>
</head>
<body>
	<form action="sample_posteddata.php" method="post">
		<p>
			In this sample the editor is configured to edit entire HTML pages, from the &lt;html&gt;
			tag to &lt;/html&gt;.</p>
		<p>
			<label for="editor1">
				Editor 1:</label><br />
			<textarea cols="80" id="editor1" name="editor1" rows="10">&lt;html&gt;&lt;head&gt;&lt;title&gt;CKEditor Sample&lt;/title&gt;&lt;/head&gt;&lt;body&gt;&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href="http://ckeditor.com/"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</textarea>
			<script type="text/javascript">
			//<![CDATA[

				CKEDITOR.replace( 'editor1',
					{
						fullPage : true
					});

			//]]>
			</script>
		</p>
		<p>
			<input type="submit" value="Submit" />
		</p>
	</form>
</body>
</html>
