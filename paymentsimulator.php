<?php
## v5.12 -> mar. 23, 2005
session_start();
// read the post from PayPal system and add 'cmd'
include_once ("config/config.php");

## overwrite $setts['payment gateway']
$setts['payment_gateway']="Test Mode";

if ($_REQUEST['table']==1) $_SESSION['table_to_modify']="probid_users";
if ($_REQUEST['table']==2) $_SESSION['table_to_modify']="probid_auctions";
if ($_REQUEST['table']==3) $_SESSION['table_to_modify']="probid_winners";
if ($_REQUEST['table']==4) $theoption=2;

if ($_REQUEST['table']==7) $_SESSION['table_to_modify']="probid_wanted_ads";

// assign posted variables to local variables
// note: additional IPN variables also available -- see IPN documentation
$payment_status = "Completed";
$payment_gross = $_POST['amount'];
$txn_id = "Test Transaction";
$custom = $_POST['custom'];
$currentTime = time();

if ($payment_status == "Completed"){
	#-------------------------------------------
	if ($theoption==2) {
		$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$custom."'","balance");
		$updatedBalance = $currentBalance - $payment_gross;
		if ($updatedBalance<=0) {
			$_SESSION['accsusp']=0;
		}
		## if user is suspended, activate the counters
		$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
		if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);

		$currentTime = time();
		$updateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		active='1', payment_status='confirmed', balance='".$updatedBalance."' WHERE id='".$custom."'");
        $updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
		active='1' WHERE ownerid='".$custom."'");
		$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
		active='1' WHERE ownerid='".$custom."'");
        $insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
		(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
        ('".$custom."','".$lang[payment_fee]."','".$payment_gross."','".$currentTime."','".$updatedBalance."','payment','".$setts['payment_gateway']."')");
	} else {
		if ($_REQUEST['table']==3) {
			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
			active = '1',payment_status='confirmed',amountpaid='".$payment_gross."',paymentdate='".$currentTime."',
			txnid='".$txn_id."',processor='".$setts['payment_gateway']."' WHERE auctionid='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else if ($_REQUEST['table']==8) {
			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
			store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			## now we submit the accounting details
			$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
			(userid, amountpaid, paymentdate, processor) VALUES
			('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			## if we activate the users table, count all auctions
			if ($_REQUEST['table']==1) counterAddUser($custom);

			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE ".$_SESSION['table_to_modify']." SET 
			active = '1',payment_status='confirmed',
			amountpaid='".$payment_gross."',paymentdate='".$currentTime."',
			processor='".$setts['payment_gateway']."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			## if we activate the auctions table, count the auction
			if ($_REQUEST['table']==2) {
				$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
				if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
					addcatcount ($auctCat['category']);
					addcatcount ($auctCat['addlcategory']);
				}
			}
			if ($_REQUEST['table']==7) {
				$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
				if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
					addwantedcount ($wantedCat['category']);
					addwantedcount ($wantedCat['addlcategory']);
				}
			}
		}
	}
	countCategories();	
}
echo "<script>document.location.href='".$_POST['return']."'</script>";

?>