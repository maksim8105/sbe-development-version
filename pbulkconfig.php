<?php
## v5.22 -> oct. 25, 2005
include ("config/config.php");

if(!isset($_REQUEST['username']) || ($_REQUEST['username']=="") || !isset($_REQUEST['password']) || ($_REQUEST['password']=="") || !isset($_REQUEST['email']) || ($_REQUEST['email']=="") || !isset($_REQUEST['request']) || ($_REQUEST['request']==""))
	die("<BADLOGIN>");
    
$username 			= $_REQUEST['username'];
$pwd 				= $_REQUEST['password'];
$email 				= $_REQUEST['email'];
$request 			= $_REQUEST['request'];
$version_five 		= "32";
$version_six 		= "30";
$onlypaypalbuyers 	= "N";
$usevouchers      	= "N";
$useareaid        	= "N";

# do not edit these values
$prefilled        	= "";
$customcatfields  	= "";
$customcattypes   	= "";
$active_store     	= "0"; 
$store_cats       	= "";
$sep              	= "[]";

## Set your version
$pwdlen = $version_six;


if($request == "config") { 
	## if config is required	
	$sep="[]"; 
	$result=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE username='$username' AND email = '$email' AND password='".substr(md5($pwd),0,$pwdlen)."'");
	if ($userinfo=mysqli_fetch_array($result)) {
		## Get prefilled defaults
		$active_store            = $userinfo['store_active'];
		$store_cats              = $userinfo['store_categories'];
		$def_duration            = $userinfo['default_duration'];
		$def_private             = $userinfo['default_private'];
		$def_isswap              = $userinfo['default_isswap'];
		$def_sc                  = $userinfo['default_sc'];
		$def_scint               = $userinfo['default_scint'];
		$def_pm                  = $userinfo['default_pm'];
		$def_acceptdirectpayment = $userinfo['default_acceptdirectpayment'];
		$def_directpaymentemail  = $userinfo['default_directpaymentemail'];
		$def_postage_costs       = $userinfo['default_postage_costs'];
		$def_insurance           = $userinfo['default_insurance'];
		$def_type_service        = $userinfo['default_type_service'];
		$def_shipping_details    = $userinfo['default_shipping_details'];
		$prefilled               = "$def_duration$sep$def_private$sep$def_isswap$sep$def_sc$sep$def_scint$sep$def_pm$sep$def_acceptdirectpayment$sep$def_directpaymentemail$sep$def_postage_costs$sep$def_insurance$sep$def_type_service$sep$def_shipping_details$sep";

		## Get durations...  
		$getdurations = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations WHERE 1");
		$num_durations = mysqli_num_rows($getdurations);
		if ($num_durations) {
			while ($myrow = mysqli_fetch_row($getdurations)) echo "<PDUR>$myrow[1]\n";
		}
		((mysqli_free_result($getdurations) || (is_object($getdurations) && (get_class($getdurations) == "mysqli_result"))) ? true : false);
		
		## Get currencies...  
		$getcurrencies=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_currencies WHERE 1");
		$num_currencies = mysqli_num_rows($getcurrencies);
		if ($num_currencies) {
			while ($myrow = mysqli_fetch_row($getcurrencies)) echo "<PCURR>$myrow[1]\n";
		}
		((mysqli_free_result($getcurrencies) || (is_object($getcurrencies) && (get_class($getcurrencies) == "mysqli_result"))) ? true : false);
		
		## Get countries...  
		# 1 = name ; 2 = theorder
		$getcountries=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries WHERE 1");
		$num_countries = mysqli_num_rows($getcountries);
		if ($num_countries) {
			while ($myrow = mysqli_fetch_row($getcountries)) echo "<PLOC>$myrow[1]$sep$myrow[2]$sep\n";
		}
		((mysqli_free_result($getcountries) || (is_object($getcountries) && (get_class($getcountries) == "mysqli_result"))) ? true : false);
		
		## Get payment methods...  
		$getpaymethods=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_methods WHERE 1");
		$num_paymethods = mysqli_num_rows($getpaymethods);
		if ($num_paymethods) {
			while ($myrow = mysqli_fetch_row($getpaymethods)) echo "<PMETH>$myrow[1]\n";
		}
		((mysqli_free_result($getpaymethods) || (is_object($getpaymethods) && (get_class($getpaymethods) == "mysqli_result"))) ? true : false);
		
		## Get shipping options...  
		$getshipoptions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_shipping_options WHERE 1");
		$num_shipoptions = mysqli_num_rows($getshipoptions);
		if ($num_shipoptions){
			while ($myrow = mysqli_fetch_row($getshipoptions)) echo "<PSHIP>$myrow[1]\n";
		}
		((mysqli_free_result($getshipoptions) || (is_object($getshipoptions) && (get_class($getshipoptions) == "mysqli_result"))) ? true : false);
		
		## Get categories... 
		# 2 = name ; 0 = id ; 1 = parent ; 5 = thorder
		$getcategories=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE 1");
		$num_categories = mysqli_num_rows($getcategories);
		if ($num_categories) {
			while ($myrow = mysqli_fetch_row($getcategories)) {
				echo "<PCATS>$myrow[2]$sep$myrow[0]$sep$myrow[1]$sep$myrow[5]$sep\n";
			}
		}
		((mysqli_free_result($getcategories) || (is_object($getcategories) && (get_class($getcategories) == "mysqli_result"))) ? true : false);
	
		## Get custom categories fields... 
		$getcustomcatfields  = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fields WHERE 1");
		$num_customcatfields = mysqli_num_rows($getcustomcatfields);
		if ($num_customcatfields) {
			while ($myrow = mysqli_fetch_row($getcustomcatfields)) {
				echo "<PCATFIELDS>$myrow[0]$sep$myrow[1]$sep$myrow[2]$sep$myrow[3]$sep$myrow[4]$sep$myrow[5]$sep$myrow[6]$sep$myrow[7]$sep$myrow[8]$sep$myrow[9]$sep$myrow[10]$sep\n";
			}
		}
		((mysqli_free_result($getcustomcatfields) || (is_object($getcustomcatfields) && (get_class($getcustomcatfields) == "mysqli_result"))) ? true : false);

		## Get custom categories types... 
		$getcustomcattypes  = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fields_types WHERE 1");
		$num_customcattypes = mysqli_num_rows($getcustomcattypes);
		if ($num_customcattypes) {
			while ($myrow = mysqli_fetch_row($getcustomcattypes)) {
				echo "<PCATTYPES>$myrow[0]$sep$myrow[1]$sep$myrow[2]$sep\n";
			}
		}
		((mysqli_free_result($getcustomcattypes) || (is_object($getcustomcattypes) && (get_class($getcustomcattypes) == "mysqli_result"))) ? true : false);		

        echo "<PREFILLED>$prefilled\n";
        echo "<STOREACTIVE>$active_store\n";
        echo "<STORECATS>$store_cats\n";
        echo "<PAYPALONLY>$onlypaypalbuyers\n";
        echo "<VOUCHERS>$usevouchers\n";
        echo "<AREAID>$useareaid\n";
		echo "<PPICSIZE>$setts[pic_gal_max_size]\n";      
		echo "<PSITENAME>$setts[sitename]\n";      
		echo "<PSITEURL>$setts[siteurl]\n";      
		echo "<PCURRSYM>$setts[currency]\n";      
		echo "<ALLOWHPDESC>$setts[hpfeat_desc]\n"; 
		echo "<ALLOWSWAP>$setts[swap_items]\n"; 
	} else {
		echo "<BADLOGIN>";
	}
	## end of if config is required
} else if($request == "loginchk") {
	## if login check is required
	$result=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE username='$username' AND email = '$email' AND password='".substr(md5($pwd),0,$pwdlen)."'");
	if (!$userinfo=mysqli_fetch_array($result)) {
		echo "<BADLOGIN>";
		die;
	} else {
		echo "<GOODLOGIN>";
	}
	## end of if login check is required
}
?>
