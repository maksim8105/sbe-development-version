<?php

class ConcretePangalink
{
	var
		$VK_SND_ID, // ID Банка
		$config = array(), //
		$pangalink = null, // Ссылка на объект Pangalink
		$privKey = null, // Наш key
		$pubKey = null, // key банка
		$pangaUrl = '',
		$methodName = 'GET',

		$vkLenghts = array(),  // Размер полей
		$vkServices = array();  // Массив запросов и возможных полей

	function ConcretePangalink( $VK_SND_ID, $pangalink )
	{
		$this->VK_SND_ID = $VK_SND_ID;
		$this->pangalink = $pangalink;
		$this->config['pangalink'] = $this->pangalink->get( $VK_SND_ID );

		$this->configurePangalink();
	}

	/**
	 * Получить значение конфигурационной переменной
	 */
	function get( $key, $name = null, $default = null )
	{
		if ( array_key_exists( $key, $this->config ) )
		{
			if ( is_null( $name ) )
			{
				return $this->config[ $key ];
			}
			elseif ( array_key_exists( $name, $this->config[ $key ] ) )
			{
				return $this->config[ $key ][ $name ];
			}
		}

		return $default;
	}

	/**
	 * Загрузить конфигурацию
	 */
	function configurePangalink()
	{
		$this->loadPangaConfig( $this->get( 'pangalink', 'config_name' ) );

		$this->vkLenghts = $this->get( 'VK_LENGHTS' );

		// Определяем доступные сервисы vkServices (задаются в .ini файлах банков цифрами)
		foreach ( $this->config as $config_key => $config_value )
		{
			if ( is_numeric( $config_key ) )
			{
				$this->vkServices[ $config_key ] = $config_value;
			}
		}
	}

	/**
	 * Установить вручную наш приватный ключ
	 */
	function setPrivKey( $privKey )
	{
		$this->privKey = $privKey;
	}

	/**
	 * Загрузить наш приватный ключ
	 */
	function loadPrivKey()
	{
		if ( null !== $this->privKey )
		{
			return $this->privKey;
		}

		$key_path = $this->pangalink->thisDir . $this->get( 'pangalink', 'my_private_key' );

		if ( ! is_readable( $key_path ) )
		{
			$this->printDebugMessage( sprintf( 'Не найден приватный ключ (%s)! (%s)', $key_path, __LINE__ ), E_USER_WARNING );
			return false;
		}

		return $this->privKey = file_get_contents( $key_path );
	}

	/**
	 * Установить вручную публичный ключ банка
	 */
	function setPubKey( $pubKey )
	{
		$this->pubKey = $pubKey;
	}

	/**
	 * Загрузить публичный ключ банка
	 */
	function loadPubKey()
	{
		if ( null !== $this->pubKey )
		{
			return $this->pubKey;
		}

		$key_path = $this->pangalink->thisDir . $this->get( 'pangalink', 'bank_public_key' );

		if ( ! is_readable( $key_path ) )
		{
			$this->printDebugMessage( sprintf( 'Не найден публичный ключ банка (%s)! (%s)', $key_path, __LINE__ ), E_USER_WARNING );
			return false;
		}

		return $this->pubKey = file_get_contents( $key_path );
	}

	/**
	 * Инициализация конфигурации pangalink
	 */
	function loadPangaConfig( $config )
	{
		if ( is_array( $config ) && count( $config ) )
		{
			$this->config = $config;
		}
		elseif ( is_string( $config ) && is_readable( $this->pangalink->thisDir . $config ) )
		{
			$this->config = $this->config + parse_ini_file( $this->pangalink->thisDir . $config, true );
		}
		else
		{
			$this->printDebugMessage( sprintf( 'Не найдена конфигурация Pangalink! (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		return true;
	}

	/**
	 * Быстрое создание банковской ссылки для платежа
	 *
	 * @var $orderId          Номер заказа
	 * @var $orderAmout       Сумма заказа
	 * @var $orderDescription Описание заказа
	 * @var $returnUrl        Адрес URL для ответа
	 * @var $language         Язык страницы оплаты банка
	 * @var $vkService        Номер услуги (1001, 1002)
	 *
	 * @return string Возвращает pangalink URL
	 */
	function simpleCreatePangalink( $orderId, $orderAmout, $orderDescription, $returnUrl = '', $language = 'EST', $vkService = '1002' )
	{
		$formFields = array(
			'VK_SERVICE' => $vkService,        // Номер услуги
			'VK_STAMP'   => $orderId,          // ID запроса
			'VK_AMOUNT'  => $orderAmout,       // Подлежащая уплате сумма
			'VK_MSG'     => $orderDescription, // Пояснение платежного поручения
			'VK_RETURN'  => $returnUrl,
			'VK_LANG'    => $language          // Язык общения
		);

		// Создать банковскую ссылку на основе указанных полей
		if ( false === ( $url = $this->createPangalink( $vkService, $formFields ) ) )
		{
			$this->printDebugMessage( sprintf( 'Дана некорректная информация для создания ссылки! (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		return $url;
	}

	/**
	 * Быстрое создание банковской ссылки для авторизации
	 *
	 * @var $returnUrl Адрес URL для ответа
	 * @var $vkService Номер услуги (1001, 1002)
	 * @var $vkReply   Номер ответа (3002)
	 *
	 * @return string Возвращает pangalink URL
	 */
	function simpleCreateAuthPangalink( $returnUrl = '', $vkService = 4001, $vkReply = 3002 )
	{
		$formFields = array(
			'VK_REPLY'  => $vkReply,  // Номер ответа (3002)
			'VK_RETURN' => $returnUrl // Адрес URL для ответа
		);

		// Создать банковскую ссылку на основе указанных полей
		if ( false === ( $url = $this->createPangalink( $vkService, $formFields ) ) )
		{
			$this->printDebugMessage( sprintf( 'Дана некорректная информация для создания ссылки! (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		return $url;
	}

	/**
	 * Получить ассоциативный массив полей для Pangalink
	 *
	 * @param $vkService Номер услуги
	 * @param $formFields Поля для подписи
	 * @param $encodeFields Кодировать ли значение полей (ставить true, для ссылки методом GET)
	 *
	 * @return array
	 */
	function getPangalinkFields( $vkService, $formFields, $encodeFields = true )
	{
		if ( ! isset( $this->vkServices[ $vkService ] ) )
		{
			// Указанный запрос не поддерживается
			$this->printDebugMessage( sprintf( 'Указанный запрос не поддерживается! (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		// Значения некоторых полей по-умолчанию
		$defaultFields = array(
			'VK_SERVICE' => $vkService,                             // Номер услуги
			'VK_VERSION' => $this->get( 'VK', 'VK_VERSION' ),       // Используемый криптоалгоритм (008)
			'VK_SND_ID'  => $this->get( 'pangalink', 'VK_SND_ID' ), // ID составителя запроса (ID продавца)
			'VK_CURR'    => $this->get( 'VK', 'VK_CURR' ),          // Название валюты: EEK/DEM/FIM и т.д.
			'VK_LANG'    => $this->get( 'VK', 'VK_LANG' ),          // Язык общения
			'VK_DATE'    => date( $this->get( 'general', 'DATE_FORMAT' ) ), // Текущая дата
			'VK_TIME'    => date( $this->get( 'general', 'TIME_FORMAT' ) ) // Текущее время
		);

		$formFields = array_merge( $defaultFields, $formFields );

		// Подписываем полученную форму
		if ( false === ( $formFields['VK_MAC'] = $this->SignForm( $vkService, $formFields ) ) )
		{
			// Не удалось подписать форму
			return false;
		}

		$returnFields = array();

		// Добавляем поля в ссылку
		foreach ( $this->vkServices[ $vkService ] as $field_name => $required )
		{
			if ( isset( $formFields[ $field_name ] ) )
			{
				$field_value = trim( $formFields[ $field_name ] );
				$returnFields[ $field_name ] = $encodeFields ? urlencode( $field_value ) : $field_value;
			}
			else
			{
				$returnFields[ $field_name ] = '';
			}
		}

		return $returnFields;
	}

	function getPangaUrl()
	{
		return $this->pangaUrl;
	}

	function setMethodGet()
	{
		$this->methodName = 'GET';
	}

	function setMethodPost()
	{
		$this->methodName = 'POST';
	}

	/**
	 * Создаёт ссылку Pangalink
	 */
	function createPangalink( $vkService, $formFields, $encodeFields = true )
	{
		if ( false === ( $pangalinkFields = $this->getPangalinkFields( $vkService, $formFields, $encodeFields ) ) )
		{
			return false;
		}

		// Для услуг 4001, 4002 будет подставлен специальный URL
		$this->pangaUrl = $this->get( 'general', ( $vkService >= 4000 && $vkService <= 4999 ) ? 'AUTH_URL' : 'BILLING_URL' );

		if ( 'GET' == $this->methodName )
		{
			$returnFields = array();

			foreach ( $pangalinkFields as $key => $value )
			{
				$returnFields[] = $key . '=' . $value;
			}

			// URL банка
			return $this->pangaUrl . '?'. implode( '&', $returnFields );
		}
		else
		{
			return $pangalinkFields;
		}
	}

	#
	# Функции подписания формы
	#

	/**
	 * Подписать форму
	 */
	function signForm( $vkService, $formFields )
	{
		if ( false === ( $vk_mac = $this->CreateMacString( $vkService, $formFields ) ) )
		{
			$this->printDebugMessage( sprintf( 'Не удалось подписать форму! (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		return $this->signMacString( $vk_mac );
	}

	/**
	 * Создать VK_MAC из полей формы
	 */
	function createMacString( $vkService, $formFields )
	{
		if ( ! array_key_exists( $vkService, $this->vkServices ) )
		{
			// Указанный запрос не поддерживается
			return false;
		}

		$vk_mac = '';

		foreach ( $this->vkServices[ $vkService ] as $field_name => $required )
		{
			if ( ! $required )
			{
				// Подписать только необходимые поля
				continue;
			}

			if ( ! array_key_exists( $field_name, $formFields ) )
			{
				// Не задан обязательный параметр
				return false;
			}

			$current_field_val = $formFields[ $field_name ];
			$current_field_len = strlen( $current_field_val );

			if ( $current_field_len > $this->vkLenghts[ $field_name ] )
			{
				return false;
			}

			//$vk_mac .= substr( '000' . $current_field_len, -3 ) . $current_field_val;
			$vk_mac .= str_pad( $current_field_len, 3, '0', STR_PAD_LEFT ) . $current_field_val;
		}

		return $vk_mac;
	}

	/**
	 * Подписывает строку
	 */
	function signMacString( $vkMac )
	{
		if ( false === $this->loadPrivKey() )
		{
			// Ошибка при открытии приватного ключа
			return false;
		}

		// Открываем приватный ключ
		if ( false === ( $key_result = openssl_get_privatekey( $this->privKey ) ) )
		{
			// Ошибка при открытии приватного ключа
			return false;
		}

		// Подписывается строка
		$sign_result = openssl_sign( $vkMac, $signature, $key_result );

		// Освобождается память
		openssl_free_key( $key_result );

		if ( false === $sign_result )
		{
			// Ошибка подписи строки
			return false;
		}

		// Подпись кодируется по алгоритму Base64
		$signature = base64_encode( $signature );

		// В закодированной подписи удаляются символы новых строк
		$signature = str_replace( array( "\r", "\n" ), '', $signature );

		return $signature;
	}

	#
	# Функции валидации формы
	#

	/**
	 * Проверить подписанную банком форму
	 */
	function validateForm( $formFields, $vkService = null )
	{
		// Обязательное поле
		if ( ! array_key_exists( 'VK_SERVICE', $formFields ) )
		{
			$this->printDebugMessage( sprintf( 'Не указан номер услуги! (%s)', __LINE__ ), E_USER_WARNING );

			// Не указан номер услуги
			return false;
		}

		if ( ! is_null( $vkService ) && $vkService != $formFields['VK_SERVICE'] )
		{
			$this->printDebugMessage( sprintf( 'Ожидался иной номер услуги! (%s)', __LINE__ ), E_USER_WARNING );

			// Пришёл неверный номер услуги!
			return false;
		}

		// Обязательное поле
		if ( ! array_key_exists( 'VK_MAC', $formFields ) )
		{
			$this->printDebugMessage( sprintf( 'Не указана строка с подписью (MAC String)! (%s)', __LINE__ ), E_USER_WARNING );

			// Не указан MAC String
			return false;
		}

		// Создаём строку подписи
		if ( false === ( $vk_mac = $this->CreateMacString( $formFields['VK_SERVICE'], $formFields ) ) )
		{
			// Не удалось получить MAC
			return false;
		}

		return $this->VerifyMacString( $vk_mac, $formFields['VK_MAC'] );
	}

	/**
	 * Проверить подписанную строку
	 */
	function verifyMacString( $vkMac, $signature )
	{
		if ( false === $this->loadPubKey() )
		{
			// Ошибка при открытии публичного ключа банка
			return false;
		}

		// Открываем публичный ключ банка
		if ( false === ( $key_result = openssl_get_publickey( $this->pubKey ) ) )
		{
			// Ошибка при открытии публичного ключа банка
			$this->printDebugMessage( sprintf( 'Ошибка при открытии публичного ключа банка! (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		$signature = base64_decode( $signature );

		// Проверяется подпись строки
		if ( -1 === ( $verify_result = openssl_verify( $vkMac, $signature, $key_result ) ) )
		{
			$this->printDebugMessage( sprintf( 'Ошибка при валидации MAC String! (%s)', __LINE__ ), E_USER_WARNING );
			$verify_result = false;
		}

		// Освобождается память
		openssl_free_key( $key_result );

		return (bool) $verify_result;
	}

	/**
	 * Вывести отладочное сообщение
	 */
	function printDebugMessage( $debugMessage )
	{
		$args = func_get_args();
		call_user_func_array( array( $this->pangalink, 'PrintDebugMessage' ), (array)$args );
	}
}

?>