<?
## v5.24 -> apr. 06, 2006
## Authorize.net ADDON
if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {

	if ( !defined('INCLUDED') ) { die("Access Denied"); }

	include("formchecker.php");
	if ($_REQUEST['action']=="updateinfo") {

		
		$vat_uid = $_POST['vat_uid_number'];

		$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
		name='".remSpecialChars($_POST['name'])."', address='".remSpecialChars($_POST['address'])."', city='".remSpecialChars($_POST['city'])."',
		state='".remSpecialChars($_POST['state'])."', country='".remSpecialChars($_POST['country'])."', zip='".remSpecialChars($_POST['zip'])."',
		phone='".remSpecialChars($_POST['phone'])."', email='".$_POST['email']."', newsletter='".$_POST['subsnl']."',
		apply_vat_exempt='".$_POST['apply_vat_exempt']."', vat_uid_number='".$vat_uid."', ".$payment_update_string."
		WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

		$_SESSION['membername'] = $_POST['name'];

		if ($_POST['password']!="") {
			$updatePassword = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
			password='".md5($_POST['password'])."' WHERE id='".$_SESSION['memberid']."'");
		}

		### VAT exemption email
		$recipientId = $_SESSION['memberid'];
		if ($_POST['apply_vat_exempt']=="Y"&&$_POST['apply_vat_exempt_old']=="N")
			include ("mails/register_apply_vat_exempt.php");

		echo "<br><p class=contentfont align=center>$lang[inf_updated]</p>";
		$link = "membersarea.php";
		echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
  	} else {
		$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");

		$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
    	if ($setts['account_mode_personal']==1) {
    		$account_mode_local = ($tmp) ? 2 : 1;
  		} else $account_mode_local = $setts['account_mode'];

		if ($account_mode_local==2) {
			if ($user['balance']<=0) {
				$thevar = $lang[credit];
				$balance = abs($user['balance']);
			} else {
				$thevar = $lang[debit];
				$balance = $user['balance'];
			} ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
   <tr>
      <td class="contentfont"><b>
         <?=$lang['goto']?>
         : </b><a href="http://test.sbe.ee/membersarea.php?page=money">Balance</a> | 
	       <a href="http://test.sbe.ee/membersarea.php?page=addmoney">Add money</a> | 
               <a href="http://test.sbe.ee/membersarea.php?page=withdrawmoney">Withdraw money</a> |	
               <a href="http://test.sbe.ee/membersarea.php?page=transactions">View transactions</a> |	
               <a href="http://test.sbe.ee/membersarea.php?page=bankdetails">Bank details</a> |	
         </a></td>
   </tr>
</table>
Please select method of your payment

<?php

  // Подключаем базовый класс Pangalink'а
  include_once 'pangalink/Pangalink.class.php';
  
  // Инициализируем Pangalink, указываем путь к файлу конфигурации и устанавливаем режим отладки
  $Pangalink =& new Pangalink( 'config/pangalink.ini', true );
  
  // Адрес URL, куда будет возвращён ответ
  $pangalink_return_url = 'http://www.sbe.ee/success.php';
  
  

?>

     		<table width="70%" align="center" cellspacing="10" cellpadding="10" border="0">
     		<tr><td><strong>ESTONIA:</strong></td></tr>
     		</table>
     		<table width="70%" align="center" cellspacing="10" cellpadding="10" class="border">
     		<tr>
     		<?php $pank = $Pangalink->initializePangalink( 'HP' ); ?>
     		<td width="100"><a href="<?=$pank->simpleCreatePangalink(6,"10.00","test",$pangalink_return_url)?>"><img src=img/bank/hansapank.gif width=88 height=31 alt="Swedbank" border=0></a></td>
     		<td>Swedbank</td>
     		</tr>
     		<tr>
     		<?php $pank = $Pangalink->initializePangalink( 'EYP' ); ?>
     		<td width="100"><a href="<?=$pank->simpleCreateAuthPangalink( $pangalink_return_url )?>"><img src=img/bank/seb.gif width=88 height=31 alt="SEB" border=0></a></td>
     		<td>SEB</td>
     		</tr>
     		<tr>
     		<? /*
     		<tr>
     		<td width="100"><img src=img/bank/sampo.gif width=88 height=31 alt="Sampo Pank" border=0></td>
     		<td>Sampo Pank</td>
     		</tr>
			*/ ?>
     		<tr>
     		<?php $pank = $Pangalink->initializePangalink( 'KREP' ); ?>
     		<td width="100"><a href="<?=$pank->simpleCreateAuthPangalink( $pangalink_return_url )?>"><img src=img/bank/krediidipank.gif width=88 height=31 alt="Krediidipank" border=0></a></td>
     		<td>Krediidipank</td>
     		</tr>
     		<tr>
     		<td width="100"><a href="https://www.sbe.ee/idkaart/id.php"><img src=img/bank/idkaart.gif width=88 height=31 alt="ID-kaart" border=0></a></td>
     		<td><?=$lang[idkaart];?></td>
     		</tr>
     		</table>
     		
          <p>&nbsp;</p>
          
          	<table width="70%" align="center" cellspacing="10" cellpadding="10" border="0">
     		<tr><td><strong>LATVIA:</strong></td></tr>
     		</table>
     		<table width="70%" align="center" cellspacing="10" cellpadding="10" class="border">
     		<tr><td width="100"><a href="http://www.sbe.ee/authenticate_lv.php"><img src=img/bank/hansapank.gif width=88 height=31 alt="Swedbank Latvia" border=0></a></td>
     		<td>Swedbank</td></tr>
     		</table>
     		
     		<p>&nbsp;</p>
     		<p>&nbsp;</p>


<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr class="c1">
      <td colspan=3><b>
         <?=$lang[accmanage]?>
         </b></td>
   </tr>
   <tr valign="top" class="c3">
      <td align="center" colspan="3"><strong>
         <?=$lang[accbalance]?>
         :</strong> <? echo (($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar;?></td>
   </tr>
   <? if ($thevar==$lang[debit]&&$balance>=$setts['min_invoice_value']) { ?>
   <tr class="c2">
      <td align="center" class="contentfont" colspan="3"><?
			$amount = $balance;
			$returnUrl=$path."paymentdone.php";
			$failureUrl=$path."paymentfailed.php";
			if ($amount==0) $setts['payment_gateway']="none";

			$paymentAmount=number_format($amount,2,'.','');

			### new function that displays the payment message and amount
			displayPaymentMessage($paymentAmount);

			### new procedure to list all active payment gateways
			if ($setts['payment_gateway']=="none") {
				echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
				counterAddUser($_SESSION['memberid']);
				$activateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
				active = '1',payment_status='confirmed' WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			} else {
				$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
				while ($selectedPGs = mysqli_fetch_array($getPGs)) {
					// Pangalink
					include 'pangalink/invoice.php';

					
				}
			}
			?>
      </td>
   </tr>
</table>
<? } ?>
<? } ?>

<br>
<?
		}
} else { echo "$lang[err_relogin]"; } ?>
