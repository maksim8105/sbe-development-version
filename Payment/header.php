<?
## v5.24 -> apr. 11, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }
### enter hit into stats table
if ($setts['maintenance_mode']==1&&$_SESSION['adminarea']!="Active") { 
	include_once ("maintenance_splash_page.php");
	die();
}

function getmicrotime() 
{ 
   list($usec, $sec) = explode(" ", microtime()); 
   return ((float)$usec + (float)$sec); 
}

$time_start = getmicrotime();
$currentTime = time();
### Site Stats currently disabled 
/* $refererSite=getenv("HTTP_REFERER");
$enterhit = mysql_query("INSERT INTO probid_stats (userip,userbrowser,clickdate,referralsite) 
VALUES ('".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."','".$currentTime."','".$refererSite."')") or die(mysql_error());
*/
### session language settings
if(!$_SESSION['sess_lang']) {
	include_once ("config/lang/".$setts['default_lang']."/site.lang");
	$_SESSION['sess_lang']="".$setts['default_lang']."";
	include_once ("config/lang/".$setts['default_lang']."/category.lang");
}else{
	include_once ("config/lang/".$_SESSION['sess_lang']."/site.lang");
	include_once ("config/lang/".$_SESSION['sess_lang']."/category.lang");
}

## modification regarding the SSL - only the registration and login page will be in SSL.
$path = $setts['siteurl'];
$path_ssl = ($setts['is_ssl']==1) ? $setts['ssl_address'] : $setts['siteurl'];

## show sell button
if ($setts['private_site']=="Y") $showSell = $_SESSION['is_seller'];
else $showSell="Y";

if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {
	$btn2_link=$path."sellitem.php";
	$btn3_caption=$lang[Cap_members];
	$btn3_link=$path."membersarea.php";
	$btn4_caption=$lang[Cap_logout];
	$btn4_link=$path."index.php?option=logout";
} else {
	$btn2_link=$path_ssl."login.php?redirect=sell";
	$btn3_caption=$lang['Cap_register'];
	$btn3_link=$path_ssl."register.php";
	$btn4_caption=$lang['Cap_login'];
	$btn4_link=$path_ssl."login.php";
}
include ("themes/".$setts['default_theme']."/title.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>
<? 
if (@eregi("auctiondetails.php",$_SERVER['PHP_SELF'])) echo getSqlField("SELECT itemname FROM probid_auctions WHERE id='".$_GET['id']."'","itemname")." - ";
if (@eregi("wanted.details.php",$_SERVER['PHP_SELF'])) echo getSqlField("SELECT itemname FROM probid_wanted_ads WHERE id='".$_GET['id']."'","itemname")." - ";
if (@eregi("displaynews.php",$_SERVER['PHP_SELF'])) echo $lang['hotnews']." - ".getSqlField("SELECT title FROM probid_news WHERE id='".$_GET['id']."'","title")." - ";
if (@eregi("stores.php",$_SERVER['PHP_SELF'])) echo $lang['SEO_shops']." - "; 
dp("userid1<");
if (@eregi("shop.php",$_SERVER['PHP_SELF'])) echo getSqlField("SELECT store_name FROM probid_users WHERE id='".$_REQUEST['userid']."'",store_name)." - ";
dp("userid1>");
if (@eregi("viewfeedback.php",$_SERVER['PHP_SELF'])) echo $lang[recent_ratings]." ".getSqlField("SELECT username FROM probid_users WHERE id='".$_REQUEST['owner']."'",username)." - ";
if (@eregi("wanted.categories.php",$_SERVER['PHP_SELF'])) echo $lang[open_wanted]." - ";
if (@eregi("prefilledfields.php",$_SERVER['PHP_SELF'])) echo $lang[prefilledfields]." - ";
if (@eregi("help.php",$_SERVER['PHP_SELF'])) echo $lang[help]." - ";
if (@eregi("sitefees.php",$_SERVER['PHP_SELF'])) echo $lang[Site_fees]." - ";
if (@eregi("register.php",$_SERVER['PHP_SELF'])) echo $lang[registerforacc]." - ";
if (@eregi("team.php",$_SERVER['PHP_SELF'])) echo $lang[team]." - ";
if (@eregi("write.php",$_SERVER['PHP_SELF'])) echo $lang[sendmessage]." - ";
if (@eregi("terms.php",$_SERVER['PHP_SELF'])) echo $lang[terms_cond]." - ";
if (@eregi("pp.php",$_SERVER['PHP_SELF'])) echo $lang[Privacy_Policy]." - ";
if (@eregi("faq.php",$_SERVER['PHP_SELF'])) echo $lang[faq]." - ";
if (@eregi("membersarea.php",$_SERVER['PHP_SELF'])) echo $lang[memarea_title]." - ";
if (@eregi("w-contactinfo.php",$_SERVER['PHP_SELF'])) echo $lang[sellingtotals]." - ";
if (@eregi("abusereport.php",$_SERVER['PHP_SELF'])) echo $lang[abuse]." - ";
if (@eregi("editauction.php",$_SERVER['PHP_SELF'])) echo $lang[modifyauc]." - ";
if (@eregi("sellitem.php",$_SERVER['PHP_SELF'])) echo $lang[selling]." - ";
if (@eregi("search.php",$_SERVER['PHP_SELF'])) echo $lang[search]." - ";
if (@eregi("otheritems.php",$_SERVER['PHP_SELF'])) echo $lang[otherselleritems]." - ";
if (@eregi("buynow.php",$_SERVER['PHP_SELF'])) echo $lang[buynow]." - ";
if (@eregi("auctionfriend.php",$_SERVER['PHP_SELF'])) echo $lang[sendtofriend]." - ";
if (@eregi("itemwatch.php",$_SERVER['PHP_SELF'])) echo $lang[watchthisitem]." - ";
if (@eregi("auctionwatch.php",$_SERVER['PHP_SELF'])) echo $lang[auctionwatch]." - ";
if (@eregi("leavefeedback.php",$_SERVER['PHP_SELF'])) echo $lang[feedback_leave]." - ";
if (@eregi("mailprefs.php",$_SERVER['PHP_SELF'])) echo $lang[mailprefs]." - ";

else if (@eregi("categories.php",$_SERVER['PHP_SELF'])) { 
	$cat_title = getSqlField("SELECT name FROM probid_categories WHERE id='".$_REQUEST['parent']."'","name");
	if ($cat_title!=""&&$cat_title!="n/a") echo $cat_title." - ";
}
echo $setts['sitename'];
?>
</title>
<?
if (@eregi("categories.php",$_SERVER['PHP_SELF'])) {
	$meta_cat = getMainCat($_REQUEST['parent']);
	$meta_details = getSqlRow("SELECT meta_description, meta_keywords FROM probid_categories WHERE id='".$meta_cat."'");
}	
?>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lang[codepage];?>">
<? 
if ($meta_details['meta_description']!=""||$meta_details['meta_keywords']!="") {
	echo "<meta name=\"description\" content=\"".$meta_details['meta_description']."\"> \n";
	echo "<meta name=\"keywords\" content=\"".$meta_details['meta_keywords']."\"> \n";
} else {
	echo addSpecialChars(getSqlField ("SELECT metatags FROM probid_gen_setts","metatags"));
}

if (@eregi("shop", $_SERVER['PHP_SELF'])) {
	$store_metatags = getSqlField("SELECT store_metatags FROM probid_users WHERE id='".$_REQUEST['userid']."'", "store_metatags");
	if (!empty($store_metatags)) echo "<meta name=\"keywords\" content=\"".$store_metatags."\"> \n";
}
?>
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--

a.ln:link {
	background-image:  url(themes/v52/img/sidem.gif);
}
a.ln:visited {
	background-image:  url(themes/v52/img/sidem.gif);
}
a.ln:hover {
	background-image:  url(themes/v52/img/sidemo.gif);
}
-->
</style>
<script language="javascript" src="themes/<?=$setts['default_theme'];?>/main.js" type="text/javascript"></script>
<script language=JavaScript src='scripts/innovaeditor.js'></script>
<script type="text/javascript">
<? $diff=getSqlField("SELECT * FROM probid_timesettings WHERE active='selected'","value"); ?>
var currenttime = '<? print date("F d, Y H:i", time()+$diff*3600)?>';
var serverdate=new Date(currenttime);

function padlength(what){
	var output=(what.toString().length==1)? "0"+what : what;
	return output;
}

function displaytime(){
	serverdate.setSeconds(serverdate.getSeconds()+1)
	var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes());
	document.getElementById("servertime").innerHTML=timestring;
}

window.onload=function(){
	setInterval("displaytime()", 1000);
}

</script>
</head>
<body bgcolor="#ffffff" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<table width="960" border="0" align="center" cellpadding="0" cellspacing="2" style="border-bottom: 1px solid #999999;">
<tr>
   <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="9" height="1"></td>
   <td width="100%"><table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#ffffff">
         <tr height="70">
            <td><a href="http://www.sbe.ee/index.php"><img src="images/probidlogo.gif?<?=rand(2,9999);?>" alt="Sell, Buy, Enjoy - SBE.EE" border=0></a></td>
            <td width=450 align="left" valign="middle"><font color="#0C6CBB" size="+1" face="Georgia">&nbsp;</font></td>
            <td width="100%" align="right"><? 
					if ($setts['is_ssl']==1&&(@eregi("register.php",$_SERVER['PHP_SELF'])||@eregi("login.php",$_SERVER['PHP_SELF']))) {
						## do nothing, but dont display the banners.
					} else {
						## banner ads management
						$nbAds = 0;
						if (@eregi("categories.php",$_SERVER['PHP_SELF'])) {
							$catName = getSqlField("SELECT name FROM probid_categories WHERE id='".$_REQUEST['parent']."'","name");
							$getAdvert = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE 
							cat_filter LIKE '%".$catName."%' AND (views_p=0 OR views_p>=views) AND (clicks_p=0 OR clicks_p>=clicks) ORDER BY RAND() LIMIT 0,1");
							$nbAds = mysqli_num_rows($getAdvert);
						}
						if (@eregi("auctiondetails.php",$_SERVER['PHP_SELF'])) {
							$advertId=0;
							$allAdverts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE 
							(views_p=0 OR views_p>=views) AND (clicks_p=0 OR clicks_p>=clicks) AND keyword_filter!='' ORDER BY RAND()");
							while ($advertsQuery = mysqli_fetch_array($allAdverts)) {
								$isAdvert = getSqlNumber("SELECT id FROM probid_auctions WHERE keywords LIKE '%".$advertsQuery['keyword_filter']."%' AND id='".$_GET['id']."'");
								if ($isAdvert>0) $advertId = $advertsQuery['id'];
							}
							$getAdvert = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE id='".$advertId."'");
							$nbAds = mysqli_num_rows($getAdvert);
						}
						// if we are not on one of the pages, or there are no ads with filter, display an ad with no filter
						if ($nbAds==0) {
							$getAdvert=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_adverts WHERE 
							cat_filter='' AND keyword_filter='' AND (views_p=0 OR views_p>=views) AND (clicks_p=0 OR clicks_p>=clicks)  ORDER BY RAND() LIMIT 0,1");
						}
						while ($bannerDetails = mysqli_fetch_array($getAdvert)) {
							$addView = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_adverts SET views=views+1 WHERE id='".$bannerDetails['id']."'");
							if ($bannerDetails['type']=="custom") {
								echo "<a href=\"click.php?refid=".$bannerDetails['id']."\" target=\"new\"><img src=\"".$bannerDetails['imgpath']."\" alt=\"".$bannerDetails['alttext']."\" border=\"0\"><br>".$bannerDetails['textunder']."</a>";
								// echo "<a href=\"".$bannerDetails['url']."\"><img src=\"".$bannerDetails['imgpath']."\" alt=\"".$bannerDetails['alttext']."\" border=\"0\"><br>".$bannerDetails['textunder']."</a>";
							} else 	if ($bannerDetails['type']=="code") { 
								echo addSpecialChars($bannerDetails['banner_code']);
							}
						}
						((mysqli_free_result($getAdvert) || (is_object($getAdvert) && (get_class($getAdvert) == "mysqli_result"))) ? true : false); 
					} ?>
            </td>
         </tr>
      </table>
      
      <? ## calculate width
		$headerCells = 6;
		if ($showSell == "Y") $headerCells ++;
		if ($setts['enable_wantedads'] == "Y") $headerCells ++;
		if ($setts['stores_enabled'] == "Y") $headerCells++;
		if ($layout['is_about'] == "Y") $headerCells ++;
		if ($layout['is_contact'] == "Y") $headerCells ++;
		$headBtnWidth = 100/$headerCells."%";
		?>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr height="31" bgcolor="#2e5b9a">
        
        <? if (@eregi("index.php",$_SERVER['PHP_SELF'])) { ?>
        <td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
	<td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>index.php">&nbsp;
               <?=$lang[Cap_home];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else {?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
	<td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>index.php">&nbsp;
               <?=$lang[Cap_home];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } 
	
	if (@eregi("displaynews.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>displaynews.php?option=all">&nbsp;
               <?=$lang[hotnews];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>displaynews.php?option=all">&nbsp;
               <?=$lang[hotnews];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } 
	
	if ($showSell=="Y") { 
	             if (@eregi("sellitem.php",$_SERVER['PHP_SELF'])) { ?> 
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>                
	<td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn2_link;?>">&nbsp;
               <?=$lang[Cap_sell];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
    <? } else { ?>
    <td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>                
	<td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn2_link;?>">&nbsp;
               <?=$lang[Cap_sell];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	            <? } }
	
	if (@eregi("membersarea.php",$_SERVER['PHP_SELF'])||@eregi("register.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>                
	<td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn3_link;?>">&nbsp;
               <?=$btn3_caption;?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>                
	<td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn3_link;?>">&nbsp;
               <?=$btn3_caption;?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? }
	
	if (@eregi("login.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn4_link;?>">&nbsp;
               <?=$btn4_caption;?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?> 
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$btn4_link;?>">&nbsp;
               <?=$btn4_caption;?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? }
	
    if ($setts['stores_enabled']=="Y") { 
    	if (@eregi("stores.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>stores.php">&nbsp;
               <?=$lang[Cap_stores];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>stores.php">&nbsp;
               <?=$lang[Cap_stores];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } } 
    
    if ($setts['enable_wantedads']=="Y") { 
		if (@eregi("wanted.categories.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>wanted.categories.php">&nbsp;
               <?=$lang[Cap_wantedads];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>	
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>wanted.categories.php">&nbsp;
               <?=$lang[Cap_wantedads];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
    <? } } 
    
    if (@eregi("help.php",$_SERVER['PHP_SELF'])) { ?>
 	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>help.php">&nbsp;
               <?=$lang[Cap_help];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>help.php">&nbsp;
               <?=$lang[Cap_help];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? }
	
	if (@eregi("sitefees.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>sitefees.php">&nbsp;
               <?=$lang[Cap_fees];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>sitefees.php">&nbsp;
               <?=$lang[Cap_fees];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? }
	
	if (@eregi("team.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>team.php">&nbsp;
               <?=$lang[team];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>team.php">&nbsp;
               <?=$lang[team];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } ?>
    
    <?
    ## About us & Contact page remove - ALEKSEI
    
    /*
    if ($layout['is_about']=="Y") { 
		if (@eregi("aboutus.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>aboutus.php">&nbsp;
               <?=$lang[Cap_about];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>aboutus.php">&nbsp;
               <?=$lang[Cap_about];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_e.gif" width="8" height="31"></td>
	<? } } 
	
    if ($layout['is_contact']=="Y") { 
		if (@eregi("contactus.php",$_SERVER['PHP_SELF'])) { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenuactive" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>contactus.php">&nbsp;
               <?=$lang[Cap_contact];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_s_rev.gif" width="6" height="31"></td>
	<? } else { ?>
	<td width="6"><img src="themes/<?=$setts['default_theme'];?>/img/db_s.gif" width="6" height="31"></td>    
            <td nowrap class="mainmenu" width="<?=$headBtnWidth;?>" align="center"><a href="<?=$path;?>contactus.php">&nbsp;
               <?=$lang[Cap_contact];?>
               </a>&nbsp;</td>
	<td width="8"><img src="themes/<?=$setts['default_theme'];?>/img/db_s_rev.gif" width="6" height="31"></td>
	<? } } ?>*/
	
	## End of remove 
	?>
    
         </tr>
      </table>
      
      <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="2"></div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#336699">
         <tr>
            <td><table width="100%" height="29" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                     <td width="194" nowrap align="center" class="search">&nbsp;&nbsp;&nbsp;
                        <?php 
								$diff=getSqlField("SELECT * FROM probid_timesettings WHERE active='selected'","value");
								echo date(substr($setts['date_format'],0,7),time()+($diff*3600)) ?>
                        <span id="servertime"></span>&nbsp;&nbsp;&nbsp;</td>
			<form action="auctionsearch.php" method="post">
                        <td align="center" width="100%" class="search" nowrap>&nbsp;&nbsp;&nbsp;<a href="<?=$path;?>search.php">
                           <?=$lang[Cap_search];?>
                           </a>&nbsp;
                        <INPUT size=25 name="basicsearch">
                           &nbsp;
                        <input name="searchok" type="submit" value="<?=$lang[search]?>">
                           &nbsp;&nbsp;&nbsp;</td>
                     </form>
                     <? /*<td class="search" nowrap>&nbsp;&nbsp;&nbsp;
                        <?=$lang[browse];?>
                        &nbsp;&nbsp;</td>
                     <form name="catbrowse" method="post" action="<?=$path;?>categories.php">
                        <input type="hidden" name="show" value="subcats">
                        <? $getCategories = mysql_query("SELECT id,name FROM probid_categories WHERE parent=0 AND hidden=0 AND userid=0 ORDER BY theorder,name");?>
                        <td width="100%" class="search"><select name="parent" onChange="javascript:catbrowse.submit()">
                              <option value="" selected>
                              <?=$lang[choosecat];?>
                              </option>
                              <? while($category=mysql_fetch_array($getCategories)) { ?>
                              <option value="<?=$category['id'];?>" <? echo (($category['id']==$_REQUEST['parent'])?"selected":"");?>>
                              <?=$c_lang[$category['id']];?>
                              </option>
                              <? } ?>
                              <option value="">------------------------</option>
                              <option value="0">
                              <?=$lang[allcats];?>
                              </option>
                           </select>
                        </td>
                     </form> */ ?>
                     
                    <?/*
                     if ($setts['is_ssl']==1) {
                     	if (@eregi("register.php",$_SERVER['PHP_SELF'])) {
                     		 echo "<td>&nbsp;</td>"; }
                     		else { 
                     			if (@eregi("login.php",$_SERVER['PHP_SELF'])) {
                     			echo "<td>&nbsp;</td>"; }
                     			}*/
                     if ($setts['is_ssl']==1&&(@eregi("register.php",$_SERVER['PHP_SELF'])||@eregi("login.php",$_SERVER['PHP_SELF']))) {
                     
                     ## if SSL enabled, don't display language flags - ALEKSEI
                     
                     	echo "<td>&nbsp;</td>";
                     }
                     else {
                     ?>
                     <td width="100%" nowrap bgcolor="#2F99E3" style="border-left: 1px solid #dddddd;"><!-- ADD THE FOLLOWING CODE WHEREEVER YOU WANT YOUR LANGUAGE OPTIONS TO SHOW-->
                     <div align="center"> &nbsp;&nbsp;
                           <?
									if ($setts['user_lang']==1) {
										include ("config/lang/list2.php");
										$langlist = explode(" ", $langlist);
										$sizeofarray = count($langlist)-1; 
										for ($z=0; $z < $sizeofarray; $z++) {
											echo "<a href=\"lang.php?lang=".$langlist[$z]."\">";
											echo "<img src=\"themes/".$setts['default_theme']."/img/".$langlist[$z].".gif\" border=\"0\" alt=\"$langlist[$z]\"></a>&nbsp;&nbsp;";
										}
									} ?>
                        </div>
                        <!-- END ADDITION -->
                     </td>
                    <? } ?>
                  </tr>
               </table></td>
         </tr>
      </table>
      <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="2"></div>
      <table width="100%" border="0" cellspacing="1" cellpadding="5" bgcolor="#ffffff">
      <tr valign="top">
         <td width="180" bgcolor="#FFFFFF">
				<script language="javascript">
					var ie4 = false; 
					if(document.all) { ie4 = true; }

					function getObject(id) { if (ie4) { return document.all[id]; } else { return document.getElementById(id); } }
					function toggle(link, divId) { 
						var lText = link.innerHTML; 
						var d = getObject(divId);
						if (lText == '+') { link.innerHTML = '&#8211;'; d.style.display = 'block'; }
						else { link.innerHTML = '+'; d.style.display = 'none'; } 
					}
				</script>
            <?
				if ($_SESSION['membersarea']=="Active") { 
					$getAn=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,date,title FROM probid_announcements WHERE active=1 AND lang='".$_SESSION['sess_lang']."' ORDER BY date DESC");
					$nbAn=mysqli_num_rows($getAn);
					if ($nbAn>0) { 
						header6("$lang[member_announcements2] [<a title=\"show/hide\" class=\"hidelayer\" id=\"exp1102170555_link\" href=\"javascript: void(0);\" onclick=\"toggle(this, 'exp1102170555');\">&#8211;</a>]"); ?>
            <div id="exp1102170555">
               <table width="100%" border="0" cellspacing="0" cellpadding="3">
                  <tr>
                     <td class="c4"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                     <td class="c3"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  </tr>
                  <? while ($ans=mysqli_fetch_array($getAn)) { ?>
                  <tr>
                     <td class="c3"><img src="themes/<?=$setts['default_theme'];?>/img/arrow.gif" width="8" height="8" hspace="4"></td>
                     <td width="100%" class="c2 smallfont"><b><? echo displaydatetime($ans['date'],substr($setts['date_format'],0,7));?></b></td>
                  </tr>
                  <tr class="contentfont">
                     <td class="c4"></td>
                     <td class="c3"><a href="<?=$path;?>viewannouncement.php?id=<?=$ans['id'];?>">
                        <?=$ans['title'];?>
                        </a> </td>
                  </tr>
                  <? } ?>
               </table>
            </div>
            <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
            <? } } ?>
            <div id="exp1102170142">
               <? if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { ?>
	            <? /*header5("$lang[memarea_title]");*/
			echo "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center' bgcolor='0C6CBB'><b>".$lang[memarea_title]."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";?>
               <table border="0" cellpadding="1" cellspacing="1" width="100%" class="c4">
                  <tr>
                     <td align="center" height="35"><b style="font-size: 10px;">
                        <?=$lang[welcome];?>,
                        <br>
                        <? echo $_SESSION['membername']; ?></b> </td>
                  </tr>
                  <tr>
                     <td class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  </tr>
               </table>
              
               <table width="100%" cellpadding="1" cellspacing="1" border="0" class="c4" >
                     <tr class="c2">
                        <td nowrap>&nbsp;<b>
                           <?=$lang[feedback_rating];?></b>:</td>
                        <td width="100%"><? echo calcFeedback($_SESSION['memberid']);?></td>
                     </tr>
                     <tr class="c2">
                        <td colspan="2" align="center"><a href="viewfeedback.php?owner=<?=$_SESSION['memberid'];?>">
                           <?=$lang[view_fb];?>
                           </a></td>
                     </tr>
                     </table>
                     
                     <table width="100%" cellpadding="1" cellspacing="1" border="0" class="c4" >
                     	<tr>
                     	<td class="c5"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
                  		</tr>
                  	</table>
            
               
               <? if ($showSell=="Y") { 
               
                define("CALC_NOT_READED",1);
                include("m_actions.php");
               
               ?>
               <table border="0" cellpadding="3" cellspacing="2" width="100%" class="c4">
                  <? if ($_SESSION['accsusp']!=2) { ?>
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=bidding"><?=$lang[bidding_img]?></a></td></tr>
                  
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=selling"><?=$lang[selling_img]?></a></td></tr>
                  
                  <tr align="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=feedback"><?=$lang[feedback_img]?></a></td></tr>
                     
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="abusereport.php"><?=$lang[abuse]?></a></td></tr>
                  
                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=wanted"><?=$lang[wanted_img]?></a></td></tr>
                     <? } ?>
                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=messages"><?=$lang[message1]?>(<?=$not_readed_count?>)<B><FONT COLOR = RED>Beta</FONT></B></a></td></tr>
                     <? } ?>
                     
                     <?
                     if ($_SERVER['REMOTE_ADDR']=="85.29.234.78") {
                     ?>
                     
                  <tr valign="center" bgcolor="white" height="21">
                     <? if ($setts['enable_wantedads']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=money">Money <B><FONT COLOR = RED>Development</FONT></B></a></td>
                  </tr>
                     <? } }?> 
                  <tr valign="center" bgcolor="white" height="21">   
                     <? if ($setts['stores_enabled']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=store"><?=$lang[store_img]?></a></td></tr>
                     <? } ?>
                     
                 <tr valign="center" bgcolor="white" height="21">   
                     <? if ($setts['stores_enabled']=="Y") { ?>
                     <td align="center" class="membutt"><a href="membersarea.php?page=block_users"><?=$lang[blockusers]?></a></td></tr>
                     <? } ?>
                  <?/*<tr valign="center" bgcolor="white" height="21">   
                     <? if ($setts['stores_enabled']=="Y") { ?>
                     <td align="center" class="membutt"><a href="history.php"><?=$lang[acc_history]?></a></td></tr>
                     <? } ?>*/?>
                     
                  <? } ?>
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="membersarea.php?page=preferences"><?=$lang[details_img]?></a></td></tr>
                     
                  <tr valign="center" bgcolor="white" height="21">
                     <td align="center" class="membutt"><a href="index.php?option=logout"><?=$lang[logout]?></a></td></tr>
                  </tr>
               </table>
               <? } else { ?>
               <table border="0" cellpadding="3" cellspacing="2" width="100%" bgcolor="#e9e9eb">
                  <? if ($_SESSION['accsusp']!=2) { ?>
                  <tr align="center" bgcolor="white">
                     <td width="50%"><a class="membutt" href="membersarea.php?page=bidding"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_bidding.gif" width="40" height="40" border="0"><br>
                        <?=$lang[bidding_img]?>
                        </a></td>
                     <td width="50%"><a class="membutt" href="membersarea.php?page=feedback"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_feedback.gif" width="40" height="40" border="0"><br>
                        <?=$lang[feedback_img]?>
                        </a></td>
                  </tr>
                  <? } ?>
                  <tr align="center" bgcolor="white">
                     <td><a class="membutt" href="membersarea.php?page=preferences"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_details.gif" width="40" height="40" border="0"><br>
                        <?=$lang[details_img]?>
                        </a></td>
                     <td><a class="membutt" href="index.php?option=logout"><img src="themes/<?=$setts['default_theme'];?>/img/system/ma_logout.gif" width="40" height="40" border="0"><br>
                        <?=$lang[logout]?>
                        </a></td>
                  </tr>
               </table>
               <? } ?>
               <? } else { ?>
               <? if ($_SESSION['membersarea']!="Active"&&$layout['d_login_box']==1&&$setts['is_ssl']!=1) { ?>
	            <? /*header5("$lang[memarea_title]");*/
			echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang[memarea_title]."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";?>
               <table border="0" cellpadding="2" cellspacing="2" width="100%" class="c4">
                  <form action="<?=$path_ssl;?>login.php" method="post" name="loginbox">
                     <input type="hidden" name="islogin" value="yes">
                     <input type="hidden" name="loginok" value="login">
                     <tr class="c2">
                        <td align="right" class="user"><?=$lang[username];?>
                           &nbsp;</td>
                        <td nowrap class="user"><input name="username" type="text" size="10">
                        </td>
                     </tr>
                     <tr class="c2">
                        <td align="right" nowrap class="user"><?=$lang[pass];?>
                           &nbsp;</td>
                        <td nowrap class="user"><input name="password" type="password" size="10"></td>
                     </tr>
                     <tr >
                        <td colspan="2" align="center"><input name="loginok" id="loginok" type="submit" value="LOGIN"></td>
                     </tr>
                  </form>
               </table>
               <? } else if ($_SESSION['membersarea']!="Active"&&$layout['d_login_box']==1&&$setts['is_ssl']==1) {
		  				echo "<p align=\"center\" class=\"contentfont\">[ <a href=\"".$path_ssl."login.php\"><strong>".$lang[login_secure]."</strong></a> ]</p>";
		  	 		}
		  		} ?>
            </div>
            <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
            <noscript>
            JS not supported
            </noscript>
            <!-- flooble Expandable Content box end  -->
            <? /*header5("$lang[Cap_categories]");*/
		echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang[Cap_categories]."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";?>
            <div id="exp1102170166">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <? 
						$getCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,name,items_counter, hover_title FROM probid_categories WHERE parent=0 AND hidden=0 AND userid=0 ORDER BY theorder,name");
						while ($catBrowse=mysqli_fetch_array($getCats)) { 
							$nbSubCats = getSqlNumber("SELECT id FROM probid_categories WHERE parent='".$catBrowse['id']."'");
							
							if ($nbSubCats>0) $catLink=$path.processLink('categories', array('category' => $c_lang[$catBrowse['id']], 'parent' => $catBrowse['id'], 'show' => 'subcats'));
							else $catLink=$path.processLink('categories', array('category' => $c_lang[$catBrowse['id']], 'parent' => $catBrowse['id'])); ?>
                  <tr>
                     <td><a class="ln" href="<?=$catLink;?>" <? echo ($catBrowse['hover_title']!="") ? "title=\"".$catBrowse['hover_title']."\"":""; ?>> <? echo $c_lang[$catBrowse['id']];?> <? if (@eregi('Y', $setts['enable_cat_counters'])) echo ($catBrowse['items_counter']!=0) ? "(<strong>".$catBrowse['items_counter']."</strong>)":"(".$catBrowse['items_counter'].")";?></a> </td>
                  </tr>
                  <? } ?>
               </table>
               <p>&nbsp;</p>
                
  <?/*
  <table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b><?=$lang[reklaam]?></b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table><br> */?>

   <table width="100%" border="0" cellspacing="0" cellpadding="5">
   	<tr><td width="100%" align=center>
      &nbsp;
    </td></tr>
   </table> <br>
            </div>
            <noscript>
            JS not supported
            </noscript>
            <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="180" height="1"></div></td>
         <td width="100%">
   
