<?
## v5.24 -> apr. 06, 2006
## Authorize.net ADDON
if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {

	if ( !defined('INCLUDED') ) { die("Access Denied"); }

	include("formchecker.php");
	if ($_REQUEST['action']=="updateinfo") {

		$payment_update_string = '';
		$payment_update_string .= "paypalemail='".$_POST['paypalemail']."',";
		$payment_update_string .= "worldpayid='".$_POST['worldpayid']."',";
		$payment_update_string .= "ikoboid='".$_POST['ikoboid']."',ikoboipn='".$_POST['ikoboipn']."',";
		$payment_update_string .= "checkoutid='".$_POST['checkoutid']."',";
		$payment_update_string .= "protxname='".$_POST['protxname']."',protxpassword='".$_POST['protxpassword']."',";
		$payment_update_string .= "authnetid='".$_POST['authnetid']."',authnettranskey='".$_POST['authnettranskey']."',";
		$payment_update_string .= "nochexemail='".$_POST['nochexemail']."',";
		$payment_update_string = rtrim($payment_update_string,',');

		$vat_uid = $_POST['vat_uid_number'];

		$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
		name='".remSpecialChars($_POST['name'])."', address='".remSpecialChars($_POST['address'])."', city='".remSpecialChars($_POST['city'])."',
		state='".remSpecialChars($_POST['state'])."', country='".remSpecialChars($_POST['country'])."', zip='".remSpecialChars($_POST['zip'])."',
		phone='".remSpecialChars($_POST['phone'])."', email='".$_POST['email']."', newsletter='".$_POST['subsnl']."',
		apply_vat_exempt='".$_POST['apply_vat_exempt']."', vat_uid_number='".$vat_uid."', ".$payment_update_string."
		WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

		$_SESSION['membername'] = $_POST['name'];

		if ($_POST['password']!="") {
			$updatePassword = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
			password='".md5($_POST['password'])."' WHERE id='".$_SESSION['memberid']."'");
		}

		### VAT exemption email
		$recipientId = $_SESSION['memberid'];
		if ($_POST['apply_vat_exempt']=="Y"&&$_POST['apply_vat_exempt_old']=="N")
			include ("mails/register_apply_vat_exempt.php");

		echo "<br><p class=contentfont align=center>$lang[inf_updated]</p>";
		$link = "membersarea.php";
		echo "<script>window.setTimeout('changeurl();',500); function changeurl(){window.location='$link'}</script>";
  	} else {
		$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");

		$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
    	if ($setts['account_mode_personal']==1) {
    		$account_mode_local = ($tmp) ? 2 : 1;
  		} else $account_mode_local = $setts['account_mode'];

		if ($account_mode_local==2) {
			if ($user['balance']<=0) {
				$thevar = $lang[credit];
				$balance = abs($user['balance']);
			} else {
				$thevar = $lang[debit];
				$balance = $user['balance'];
			} ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
   <tr>
      <td class="contentfont"><b>
         <?=$lang['goto']?>
         : </b><a href="http://test.sbe.ee/membersarea.php?page=money">Balance</a> | 
	       <a href="addmoney.php">Add money</a> | 
               <a href="withdrawmoney.php">Withdraw money</a> |	
               <a href="transaction.php">View transactions</a> |	
               <a href="Bank details.php">Bank details</a> |	
         </a></td>
   </tr>
</table>
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr class="c1">
      <td colspan=3><b>
         <?=$lang[accmanage]?>
         </b></td>
   </tr>
   <tr valign="top" class="c3">
      <td align="center" colspan="3"><strong>
         <?=$lang[accbalance]?>
         :</strong> <? echo (($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar;?></td>
   </tr>
   <? if ($thevar==$lang[debit]&&$balance>=$setts['min_invoice_value']) { ?>
   <tr class="c2">
      <td align="center" class="contentfont" colspan="3"><?
			$amount = $balance;
			$returnUrl=$path."paymentdone.php";
			$failureUrl=$path."paymentfailed.php";
			if ($amount==0) $setts['payment_gateway']="none";

			$paymentAmount=number_format($amount,2,'.','');

			### new function that displays the payment message and amount
			displayPaymentMessage($paymentAmount);

			### new procedure to list all active payment gateways
			if ($setts['payment_gateway']=="none") {
				echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
				counterAddUser($_SESSION['memberid']);
				$activateUser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
				active = '1',payment_status='confirmed' WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			} else {
				$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
				while ($selectedPGs = mysqli_fetch_array($getPGs)) {
					// Pangalink
					include 'pangalink/invoice.php';

					if ($selectedPGs['name']=="Paypal") {
						$notifyUrl=$path."paymentprocess.php?table=4";
						paypalForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Nochex") {
						$notifyUrl=$path."nochexprocess.php?table=4";
						nochexForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="2Checkout") {
						$notifyUrl=$path."checkoutprocess.php?table=4";
						checkoutForm($_SESSION['memberid'],$setts['checkoutid'],$paymentAmount,4);
					}
					if ($selectedPGs['name']=="Worldpay") {
						$notifyUrl=$path."worldpayprocess.php?table=4";
						worldpayForm($_SESSION['memberid'],$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Ikobo") {
						$notifyUrl=$path."ikoboprocess.php?table=4";
						ikoboForm($_SESSION['memberid'],$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Protx") {
						$notifyUrl=$path."protxprocess.php?table=4";
						protxForm($_SESSION['memberid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
					if ($selectedPGs['name']=="Authorize.Net") {
						$notifyUrl=$path."authorize.net.process.php?table=4";
						authorizeNetForm($_SESSION['memberid'],$paymentAmount,4);
					}
					if ($selectedPGs['name']=="Moneybookers") {
						$notifyUrl=$path."moneybookers.process.php?table=4";
						moneybookersForm($_SESSION['memberid'],$paymentAmount,4);
					}
					if ($selectedPGs['name']=="Test Mode") {
						$notifyUrl=$path."paymentsimulator.php?table=4";
						testmodeForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
					}
				}
			}
			?>
      </td>
   </tr>
</table>
<? } ?>
<? } ?>

<br>
<?
		}
} else { echo "$lang[err_relogin]"; } ?>
