<?
## v5.22 -> oct. 11, 2005
session_start();
if ($_SESSION['memberid']<=0) {
	echo "<script>document.location.href='login.php?redirect=invoice&id=".$_GET['id']."'</script>";
} else {

include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php"); 

$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'");
$balance=$user['balance'];
	
if ($balance<=0) {
	$thevar = $lang[credit];
	$balance = abs($balance);
} else {
	$thevar = $lang[debit];
	$amount = $balance;
}

header5($lang[invoicetitle]);
?>
<br>
 <table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" class="border"> 
  <tr> 
     <td class="c1"><?=$lang[memarea_hello]?>,
      <?=$user['name'];?></td> 
   </tr> 
  <? if ($thevar==$lang[debit]) { ?> 
  <tr> 
     <td align="center" class="contentfont"><?
	$amount = $balance;
	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	if ($amount==0) $setts['payment_gateway']="none";
	
	$paymentAmount=number_format($amount,2,'.','');
	### new function that displays the payment message and amount
	displayPaymentMessage($paymentAmount);
	
	### new procedure to list all active payment gateways
	if ($setts['payment_gateway']=="none") {
		echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
		$activateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		active = '1',payment_status='confirmed' WHERE id='".$userid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	} else {
		$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
		while ($selectedPGs = mysqli_fetch_array($getPGs)) {
			if ($selectedPGs['name']=="Paypal") {
				$notifyUrl=$path."paymentprocess.php?table=4";
				paypalForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
			}	
			if ($selectedPGs['name']=="Nochex") {
				$notifyUrl=$path."nochexprocess.php?table=4";
				nochexForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,4);
			}
			if ($selectedPGs['name']=="2Checkout") {
				$notifyUrl=$path."checkoutprocess.php?table=4";
				checkoutForm($_SESSION['memberid'],$setts['checkoutid'],$paymentAmount,4);
			}
			if ($selectedPGs['name']=="Worldpay") {
				$notifyUrl=$path."worldpayprocess.php?table=4";
				worldpayForm($_SESSION['memberid'],$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
			}
			if ($selectedPGs['name']=="Ikobo") {
			 	$notifyUrl=$path."ikoboprocess.php?table=4";
				ikoboForm($_SESSION['memberid'],$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,4);
			}
			if ($selectedPGs['name']=="Protx") {
				$notifyUrl=$path."protxprocess.php?table=4";
				protxForm($_SESSION['memberid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
			}
			if ($selectedPGs['name']=="Authorize.Net") {
				$notifyUrl=$path."authorize.net.process.php?table=4";
				authorizeNetForm($_SESSION['memberid'],$paymentAmount,4);
			}
			if ($selectedPGs['name']=="Moneybookers") {
				$notifyUrl=$path."moneybookers.process.php?table=4";
				moneybookersForm($_SESSION['memberid'],$paymentAmount,4);
			}
			if ($selectedPGs['name']=="Test Mode") {
				$notifyUrl=$path."paymentsimulator.php?table=4";
				testmodeForm($_SESSION['memberid'],$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,4);
			}
		}
	}		
	?> </td> 
   </tr> 
  <? } ?> 
</table> 
<? 	include ("themes/$setts[default_theme]/footer.php"); 
} ?> 
