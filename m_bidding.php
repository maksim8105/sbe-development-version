<?
## v5.25 -> jun. 26, 2006

if ($_SESSION['membersarea']=="Active") {

	if ( !defined('INCLUDED') ) { die("Access Denied"); }

	if ($_GET['option']=="delwon") {
		$delwonitem=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_winners WHERE id='".$_GET['id']."' AND buyerid='".$_SESSION['memberid']."'");
	}

	if ($_GET['option']=="deloffer") {
		$deloffer=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_offers WHERE id='".$_GET['id']."' AND buyerid='".$_SESSION['memberid']."'");
	}

	if ($_GET['option']=="hidebid") {
		$hideBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET deleted='1' WHERE auctionid='".$_GET['auctionid']."' AND bidderid='".$_SESSION['memberid']."'");

	}

	if (isset($_POST['confirmok'])) {
		if ($_POST['option']=="delbid") {
			retractBid ($_POST['auctionid'], $_SESSION['memberid']);
		}
		if ($_POST['option']=="retractbid"&&underTime($_REQUEST['auctionid'])) {
			retractBid ($_POST['auctionid'], $_SESSION['memberid']);
		}
	}

	if ($_GET['option']=="delete_favstore") {
		$delFavStore = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_favourite_stores WHERE id='".$_GET['id']."' AND userid='".$_SESSION['memberid']."'");
	}


$nbWonItems=getSqlNumber("SELECT w.id FROM probid_winners w, probid_auctions a WHERE w.buyerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.b_deleted!=1");

$userDetails = getSqlRow("SELECT `authenticated`, `isikukood`, `realname` FROM probid_users WHERE id='".$_SESSION['memberid']."'");

if ( 'Y' == strtoupper( $userDetails['authenticated'] ) )
{
	// Я авторизованный пользователь
}
else
{
	// Я неавторизованный пользователь
	include 'pangalink/authenticate.php';
}

?>
<table border="0" cellspacing="1" cellpadding="4" class="buying">
   <tr>
      <td width="100" class="buyingtitle"><b><?=$lang[buyingtotals]?>:</b></td>
      <td class="<? echo ($nbWonItems>0)? "contentfont" : "smallfont";?> buyingtd"><?=$lang[won];?>
         <?=$lang[nbaucts];?>
         : <b>
         <?=getSqlNumber("SELECT w.id FROM probid_winners w, probid_auctions a WHERE w.buyerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.b_deleted!=1"); ?>
         </b> <? echo ($nbWonItems>0)? "/ <a href=\"s-contactinfo.php\"><b>".$lang[itemsdetails]."</b></a>" : ""; ?> </td>
      <td class="buyingtd"><?=$lang[biddingon];?>
         : <b>
         <?=getSqlNumber("SELECT b.id FROM probid_bids b, probid_auctions a WHERE b.bidderid='".$_SESSION['memberid']."' AND b.invalid=0 and a.closed!=1 and a.id=b.auctionid AND a.deleted!=1 AND b.deleted!=1"); ?>
         </b> </td>
      <td class="buyingtd"><?=$lang[winning];?>
         : <b>
         <?=getSqlNumber("SELECT DISTINCT b.auctionid FROM probid_bids b, probid_auctions a WHERE bidderid='".$_SESSION['memberid']."' AND a.active=1 AND a.closed=0 AND a.deleted!=1 AND b.invalid=0 AND b.out=0 AND b.auctionid=a.id"); ?>
         </b> </td>
   </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
   <tr>
      <td class="contentfont">
        <a href="auctionwatch.php">
         <?=$lang[auctionwatch]?>
         </a> | <a href="mbitemwatch.php">
         <?=$lang[itemwatch]?>
         </a> | <a href="abusereport.php">
         <?=$lang[abuse]?>
         </a> 
		 | <a href="m_blockusers.php">
         <?=$lang[blockusers]?>
         </a> 
		 </td>
   </tr>
</table>
<?
$limit = 20;

$orderField = "a.enddate";
$orderType = "DESC";

if ($_GET['srctype']=="biddingon") {
	if ($_REQUEST['orderField']=="") $orderField = "a.enddate";
	else $orderField=$_REQUEST['orderField'];
	if ($_REQUEST['orderType']=="") {
		$orderType = "ASC";
		$newOrder="DESC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}
	$start = $_GET['start'];
} else {
	$start = 0;
}

$additionalVars = "&page=bidding&srctype=biddingon";

$nbBids = getSqlNumber("SELECT b.id FROM probid_bids b, probid_auctions a WHERE
b.bidderid='".$_SESSION['memberid']."' AND b.invalid=0 and a.closed!=1 and a.id=b.auctionid AND a.deleted!=1 AND b.deleted!=1");

$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT b.* FROM probid_bids b, probid_auctions a WHERE
b.bidderid='".$_SESSION['memberid']."' AND b.invalid=0 and a.closed!=1 and a.id=b.auctionid AND a.deleted!=1 AND b.deleted!=1
ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit);
?>
<!-- Bidding on table -->
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&raquo;&nbsp;
                  <?=$lang[biddingon]?>
                  :</b> (
                  <?=$nbBids;?>
                  <?=$lang[items]?>
                  )</td>
            </tr>
         </table>
         <? if ($nbBids>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr class="smallfont">
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_id]?>
                  </a></td>
               <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_name]?>
                  </a></td>
               <td width="90" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=b.bidamount&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[bidamount]?>
                  </a></td>
               <td width="90" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=b.proxy&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[proxybid];?>
                  </a></td>
               <td width="150"class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=b.date&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[date]?>
                  </a></td>
               <td width="125" class="boldgrey" align="center"><?=$lang[status]?></td>
               <? if ($setts['enable_bid_retraction']=="Y") { ?>
               <td width="150" class="boldgrey" align="center"><?=$lang[options]?></td>
               <? } ?>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <? if ($setts['enable_bid_retraction']=="Y") { ?>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <? } ?>
            </tr>
            <?
				while ($bids=mysqli_fetch_array($getBids)) {
					$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$bids['auctionid']."'"); ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $bids['auctionid']));?>"> #
                  <?=$auction['id'];?>
                  </a></td>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $bids['auctionid']));?>">
                  <?=$auction['itemname'];?>
                  </a></td>
               <td align="center" class="smallfont"><? echo displayAmount($bids['bidamount'],$auction['currency']);?></td>
               <td align="center" class="smallfont"><? echo displayAmount($bids['proxy'],$auction['currency']);?></td>
               <td align="center" class="smallfont"><? echo format_date($bids['date'],1);?></td>
               <td align="center" class="smallfont"><? echo (($bids['out']==0)?"$lang[active]":"$lang[outbid]");?> </td>
               <? if ($setts['enable_bid_retraction']=="Y") { ?>
               <td align="center" class="smallfont"><?
						if ($setts['bidretraction']=="Y"&&underTime($bids['auctionid'])) {
							if ($bids['out']!=0) {
								echo "[ <a href=\"retractconfirm.php?page=bidding&option=delbid&auctionid=".$bids['auctionid']."\">".$lang[retract]."</a> ]";
								echo "[ <a href=\"membersarea.php?page=bidding&option=hidebid&auctionid=".$bids['auctionid']."\">".$lang[delete]."</a> ]";
							} else echo "[ <a href=\"retractconfirm.php?page=bidding&option=retractbid&auctionid=".$bids['auctionid']."&bidid=".$bids['id']."\">".$lang[retract]."</a> ]";
						} ?></td>
               <? } ?>
            </tr>
            <? } ?>
         </table>
         <?
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\" >\n";
			paginate($start,$limit,$nbBids,"membersarea.php",$additionalVars);
			echo "</td></tr></table>";
			} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<!-- EOF Bidding on table -->
<br />
<?
if ($setts['buyout_process']==1) {
	$limit = 20;

	$orderField = "a.enddate";
	$orderType = "DESC";

	if ($_GET['srctype']=="offering") {
		if ($_REQUEST['orderField']=="") $orderField = "a.enddate";
		else $orderField=$_REQUEST['orderField'];
		if ($_REQUEST['orderType']=="") {
			$orderType = "ASC";
			$newOrder="DESC";
		} else {
			$orderType=$_REQUEST['orderType'];
			$newOrder=($orderType=="ASC")?"DESC":"ASC";
		}
		$start = $_GET['start'];
	} else {
		$start = 0;
	}

	$additionalVars = "&page=bidding&srctype=offering";

	$nbOffers = getSqlNumber("SELECT o.id FROM probid_auction_offers o, probid_auctions a WHERE
	o.buyerid='".$_SESSION['memberid']."' AND a.closed!=1 AND a.id=o.auctionid AND a.deleted!=1");

	$getOffers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT o.* FROM probid_auction_offers o, probid_auctions a WHERE
	o.buyerid='".$_SESSION['memberid']."' AND a.closed!=1 AND a.id=o.auctionid AND a.deleted!=1
	ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	?>
<!-- Active offers table -->
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&raquo;&nbsp;
                  <?=$lang[offeringon]?>
                  :</b> (
                  <?=$nbOffers;?>
                  <?=$lang[offers]?>
                  )</td>
            </tr>
         </table>
         <? if ($nbOffers>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr class="smallfont">
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_id]?>
                  </a></td>
               <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_name]?>
                  </a></td>
               <td width="90" class="boldgrey" align="center"><a href="membersarea.php?start=<?=$start;?>&orderField=b.bidamount&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[offeramount]?>
                  </a></td>
               <td width="125" class="boldgrey" align="center"><?=$lang[accepted]?></td>
               <td width="150" class="boldgrey" align="center"><?=$lang[options]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <?
				while ($offers=mysqli_fetch_array($getOffers)) {
					$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$offers['auctionid']."'"); ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $offers['auctionid']));?>"> #
                  <?=$auction['id'];?>
                  </a></td>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $offers['auctionid']));?>">
                  <?=$auction['itemname'];?>
                  </a></td>
               <td align="center" class="smallfont"><? echo displayAmount($offers['amount'],$auction['currency']);?></td>
               <td align="center" class="smallfont"><font color="<? echo (@eregi('yes',$offers['accepted'])) ? "green" : "red"; ?>">
                  <?=$offers['accepted'];?>
                  </font> </td>
               <td align="center" class="smallfont"><?
						echo "[ <a href=\"membersarea.php?page=bidding&option=deloffer&id=".$offers['id']."\">".$lang[remove]."</a> ]";
						?></td>
            </tr>
            <? } ?>
         </table>
         <?
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\" >\n";
			paginate($start,$limit,$nbOffers,"membersarea.php",$additionalVars);
			echo "</td></tr></table>";
			} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<!-- EOF Active offers table -->
<br>
<?
}	## end active offers table

$orderField = "a.enddate";
$orderType = "DESC";

if ($_GET['srctype']=="wonitems") {
	if ($_REQUEST['orderField']=="") $orderField = "a.enddate";
	else $orderField=$_REQUEST['orderField'];
	if ($_REQUEST['orderType']=="") {
		$orderType = "ASC";
		$newOrder="DESC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}
	$start = $_GET['start'];
} else {
	$start = 0;
}

$additionalVars = "&page=bidding&srctype=wonitems";

$nbWonItems=getSqlNumber("SELECT w.id FROM probid_winners w, probid_auctions a WHERE w.buyerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.b_deleted!=1");

$getWonItems = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.* FROM probid_winners w, probid_auctions a WHERE w.buyerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.b_deleted!=1 ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit);
?>
<!-- Start Items Won table -->
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&raquo;&nbsp;
                  <?=$lang[wonitems]?>
                  :</b> (
                  <?="$nbWonItems $lang[items]"?>
                  ) <? echo ($nbWonItems>0)? "/ <a href=\"s-contactinfo.php\"><b>".$lang[itemsdetails]."</b></a>" : ""; ?> </td>
            </tr>
         </table>
         <? if ($nbWonItems>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr class="smallfont">
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_id]?>
                  </a></td>
               <td width="50%" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[item]?>
                  </a></td>
               <td width="20%" align="center"  class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.maxbid&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[bid]?>
                  </a></td>
               <td align="center"  class="boldgrey"><?=$lang[quant]?></td>
               <td align="center"  class="boldgrey"><?=$lang[options]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <?
				while ($wonItems=mysqli_fetch_array($getWonItems)) {
					$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$wonItems['auctionid']."'"); ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id'], 'winnerid' => $wonItems['id']));?>"> <? echo ($auction['itemname']=="")?$lang[na]:"#".$auction['id'];?> </a></td>
               <td class="smallfont"><strong> <a href="<?=processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $wonItems['auctionid'], 'winnerid' => $wonItems['id']));?>"> <? echo ($auction['itemname']=="")?$lang[auctiondeleted]:$auction['itemname'];?> </a> </strong></td>
               <td align="center" class="smallfont"><? echo displayAmount($wonItems['amount'],$auction['currency']);?></td>
               <td align="center" class="smallfont"><?=$lang[requested]?>
                  : <? echo (($auction['auctiontype']=="dutch")?"".$wonItems['quant_req']."":"1");?><br>
                  <?=$lang[offered]?>
                  : <? echo (($auction['auctiontype']=="dutch")?"".$wonItems['quant_offered']."":"1");?> </td>
               <td align="center" class="smallfont"><?
						if ($auction['acceptdirectpayment']&&($wonItems['directpayment_paid']==0&&$wonItems['flag_paid']==0)&&$wonItems['payment_status']=="confirmed") {
							echo "<a href=\"".processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id'], 'winnerid' => $wonItems['id']))."\">".$lang[m_dir_pay]."</a>";
						}
						if ($auction['itemname']=="") {
							echo "<a href=membersarea.php?page=bidding&option=delwon&id=".$wonItems['id'].">".$lang[delete]."</a>";
						} ?>
               </td>
            </tr>
            <? } ?>
         </table>
         <?
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\">\n";
			paginate($start,$limit,$nbWonItems,"membersarea.php",$additionalVars);
			echo "</td></tr></table>";
			} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<!-- EOF Items Won table -->
<br>
<?
$orderField = "a.enddate";
$orderType = "DESC";

if ($_GET['srctype']=="watcheditems") {
	if ($_REQUEST['orderField']=="") $orderField = "a.enddate";
	else $orderField=$_REQUEST['orderField'];
	if ($_REQUEST['orderType']=="") {
		$orderType = "ASC";
		$newOrder="DESC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}
	$start = $_GET['start'];
} else {
	$start = 0;
}

$additionalVars = "&page=bidding&srctype=watcheditems";

$nbWatchedItems=getSqlNumber("SELECT w.id FROM probid_auction_watch w, probid_auctions a
WHERE w.userid='".$_SESSION['memberid']."' AND w.auctionid=a.id");

$getWatchedItems=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.*, a.itemname, a.maxbid, a.currency
FROM probid_auction_watch w, probid_auctions a
WHERE w.userid='".$_SESSION['memberid']."' AND w.auctionid=a.id
ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
?>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&raquo;&nbsp;
                  <?=$lang[watchitems]?>
                  :</b> (
                  <?=$nbWatchedItems;?>
                  <?=$lang[items]?>
                  ) <? echo ($nbWatchedItems>0)? " / <a href=\"mbitemwatch.php\"><b>".$lang[watchitemsdetails]."</b></a> " : ""; ?> </td>
            </tr>
         </table>
         <? if ($nbWatchedItems>0) {
			if ($setts['date_format_type']=="USA") $format="m/d/Y";
			else $format="d/m/Y";?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr class="smallfont">
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[auc_id]?>
                  </a></td>
               <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[itemname]?>
                  </a></td>
               <td width="100" align="center" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.maxbid&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[max_bid]?>
                  </a></td>
               <td width="100" align="center" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=a.enddate&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[ends]?>
                  </a></td>
               <td width="100" align="center" class="boldgrey"><?=$lang[options]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <? while ($watchedItems=mysqli_fetch_array($getWatchedItems)) { ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $watchedItems['itemname'], 'id' => $watchedItems['auctionid']));?>"> #
                  <?=$watchedItems['auctionid'];?>
                  </a></td>
               <td class="smallfont"><a href="<?=processLink('auctiondetails', array('itemname' => $watchedItems['itemname'], 'id' => $watchedItems['auctionid']));?>">
                  <?=$watchedItems['itemname'];?>
                  </a></td>
               <td align="center" class="smallfont"><? echo displayAmount($watchedItems['amount'],$watchedItems['currency']);?></td>
               <td align="center" class="smallfont"><? echo convertdate(getSqlField("SELECT enddate FROM probid_auctions WHERE id='".$watchedItems['auctionid']."'","enddate"),$format);?></td>
               <td align="center" class="smallfont"><a href="mbitemwatch.php?option=delete&aid=<?=$watchedItems['id'];?>">
                  <?=$lang[delete]?>
                  </a></td>
            </tr>
            <? } ?>
         </table>
         <?
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\">\n";
			paginate($start,$limit,$nbWatchedItems,"membersarea.php",$additionalVars);
			echo "</td></tr></table>";

		} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<br>
<?
$orderField = "f.id";
$orderType = "DESC";

if ($_GET['srctype']=="favstores") {
	if ($_REQUEST['orderField']=="") $orderField = "f.id";
	else $orderField=$_REQUEST['orderField'];
	if ($_REQUEST['orderType']=="") {
		$orderType = "ASC";
		$newOrder="DESC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}
	$start = $_GET['start'];
} else {
	$start = 0;
}

$additionalVars = "&page=bidding&srctype=favstores";

$nbFavStores=getSqlNumber("SELECT f.id FROM probid_favourite_stores f, probid_users u
WHERE f.userid='".$_SESSION['memberid']."' AND u.id=f.userid");

$getFavStores=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT f.id AS f_id, u.* FROM probid_favourite_stores f, probid_users u
WHERE f.userid='".$_SESSION['memberid']."' AND u.id=f.storeid
ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
?>
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="border">
   <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr height="21">
               <td class="c7 smallfont"><b>&nbsp;&nbsp;&raquo;&nbsp;
                  <?=$lang[favstores]?>
                  :</b> (
                  <?=$nbFavStores;?>
                  <?=$lang[stores]?>
                  ) </td>
            </tr>
         </table>
         <? if ($nbFavStores>0) { ?>
         <table width="100%" border="0" cellpadding="3" cellspacing="1">
            <tr class="smallfont">
               <td width="60" class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=u.id&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[store_id]?>
                  </a></td>
               <td class="boldgrey"><a href="membersarea.php?start=<?=$start;?>&orderField=u.store_name&orderType=<?=$newOrder.$additionalVars;?>">
                  <?=$lang[store_name]?>
                  /
                  <?=$lang[owner_usern];?>
                  </a></td>
               <td width="100" align="center" class="boldgrey"><?=$lang[options]?></td>
            </tr>
            <tr class="c5">
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
               <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
            </tr>
            <? while ($favStores=mysqli_fetch_array($getFavStores)) { ?>
            <tr>
               <td class="smallfont"><a href="<?=processLink('shop', array('store' => $favStores['store_name'], 'userid' => $favStores['id'])); ?>"> #
                  <?=$favStores['id'];?>
                  </a></td>
               <td class="smallfont"><?=$favStores['store_name'];?>
                  /
                  <?=$favStores['username'];?></td>
               <td align="center" class="smallfont"><a href="membersarea.php?start=<?=$start;?>&option=delete_favstore&id=<?=$favStores['f_id'].$additionalVars;?>">
                  <?=$lang[delete]?>
                  </a></td>
            </tr>
            <? } ?>
         </table>
         <?
			echo "<table width=\"100%\" cellpadding=\"3\" cellspacing=\"1\"><tr><td align=\"center\" class=\"contentfont c4\">\n";
			paginate($start,$limit,$nbWatchedItems,"membersarea.php",$additionalVars);
			echo "</td></tr></table>";

		} else { echo "<div class=contentfont align=center style='padding:5px;'>$lang[noitems]</div>"; } ?>
      </td>
   </tr>
</table>
<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?>
