<?
## v5.25 -> jul. 03, 2006
## Authorize.net ADDON

session_start();
include_once ("config/config.php");

## overwrite $setts['payment gateway']
$setts['payment_gateway']="Authorize.NET";

function setsrc($a) {
  	$d = explode("/", $_SERVER["PHP_SELF"]);
  	$e = "/";
  	for ($i = 0; isset($d[$i+1]); $i++) {
   	 	$e .= $d[$i] . "/";
  	}
  	$a =  str_replace("src=\"", "src=\"http://" . $_SERVER["HTTP_HOST"] . $e, $a);
  	$a =  str_replace("background=\"", "background=\"http://" . $_SERVER["HTTP_HOST"] . $e, $a);
  	$a = str_replace("href=\"t", "href=\"http://" . $_SERVER["HTTP_HOST"] . $e . "t", $a);
  	return str_replace("href=\"i", "href=\"http://" . $_SERVER["HTTP_HOST"] . $e . "i", $a);
}

ob_start(setsrc);
include ("themes/".$setts['default_theme']."/header.php");
ob_end_flush();

$value = explode("TBL", $_REQUEST['ProBidID']);
$table = $value[1];
$custom = $value[0];
$txnid = $_REQUEST['x_trans_id'];
$payment_gross = $_REQUEST['x_amount'];
$currentTime=time();

if ($_REQUEST['x_response_code'] == 1) {

	$isTxnId = FALSE;
	$getTxn = getSqlNumber("SELECT * FROM probid_txn_ids WHERE txnid='".$txnid."' AND processor='".$setts['payment_gateway']."'");
	if ($getTxn) $isTxnId = TRUE;
				
	## only do all this if there is no txn yet.
	if (!$isTxnId) { 
		$addTxn = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_txn_ids (txnid, processor) VALUES ('".$txnid."','".$setts['payment_gateway']."')");

		if ($table == 4) {
			$_SESSION[accsusp] = 0;
			$current_balance = getSqlField("SELECT balance FROM probid_users WHERE id='$custom'","balance");
			$invoice_balance = (-1) * $current_balance;
			## if user is suspended, activate the counters
			$userDCat = getSqlRow("SELECT id, active FROM probid_users WHERE id='".$custom."'");
			if ($userDCat['active'] == 0) counterAddUser($userDCat['id']);
	
			$query1 = "UPDATE probid_users SET active='1', payment_status='confirmed', balance='0' WHERE id='$custom'";
			$query2 = "UPDATE probid_auctions SET active='1' WHERE ownerid='$custom'";
			$timenow = time();
			$query3 = "INSERT INTO probid_invoices (userid,feename,feevalue,feedate,balance) VALUES('$custom','$lang[payment_fee]','$invoice_balance','$timenow','0')";
			mysqli_query($GLOBALS["___mysqli_ston"], $query1) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			mysqli_query($GLOBALS["___mysqli_ston"], $query2) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			mysqli_query($GLOBALS["___mysqli_ston"], $query3) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$updateWantedAd = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
			active='1' WHERE ownerid='".$custom."'");
		} else {
			if ($table == 1) $table_to_modify="probid_users";
			if ($table == 2) $table_to_modify="probid_auctions";
			if ($table == 3) $table_to_modify="probid_winners";
			if ($table == 7) $table_to_modify="probid_wanted_ads";
			
			if ($table == 100) {
				//$pDetails = explode("_",$custom);
				//mysql_query("UPDATE probid_auctions SET	paidwithdirectpayment='1' WHERE id='".$pDetails[0]."'");
				/*
				mysql_query("UPDATE probid_winners SET flag_paid=1, directpayment_paid=1
				WHERE  buyerid='".$pDetails[1]."' AND auctionid='".$pDetails[0]."'");
				*/
				mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid=1, directpayment_paid=1 WHERE  id='".$custom."'");
			} else if ($table == 3) { 
				$query1 = "UPDATE $table_to_modify SET active = '1',payment_status='confirmed',
				amountpaid='".$payment_gross."', txnid='$txnid',processor='".$setts['payment_gateway']."' WHERE auctionid='$custom'";
			} else if ($_REQUEST['table']==8) {
				$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET
				store_active='1', store_lastpayment='".$currentTime."' WHERE id='".$custom."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				## now we submit the accounting details
				$addAccounting = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_stores_accounting
				(userid, amountpaid, paymentdate, processor) VALUES
				('".$custom."', '".$payment_gross."', '".$currentTime."', '".$setts['payment_gateway']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			} else { 
				$query1 = "UPDATE $table_to_modify SET active='1', payment_status='confirmed',
				amountpaid='".$payment_gross."',paymentdate='".$currentTime."',
				processor='".$setts['payment_gateway']."' WHERE id='$custom'";
				## if we activate the users table, count all auctions
				if ($table==1) counterAddUser($custom);
				## if we activate the auctions table, count the auction
				if ($table==2) {
					$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$custom."'");
					if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
						addcatcount ($auctCat['category']);
						addcatcount ($auctCat['addlcategory']);
					}
				}
				if ($table==7) {
					$wantedCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_wanted_ads WHERE id='".$custom."'");
					if ($wantedCat['closed']==0&&$wantedCat['active']==1&&$wantedCat['deleted']!=1) {
						addwantedcount ($wantedCat['category']);
						addwantedcount ($wantedCat['addlcategory']);
					}
				}
			}
			mysqli_query($GLOBALS["___mysqli_ston"], $query1) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
  	$msg = $lang[payment_done];
} else {
  	$msg = "<strong>Error #" . $_REQUEST['x_response_reason_code'] . ": " . $_REQUEST['x_response_reason_text'] . "</strong><p>" . $lang[payment_fail];
}

$d = explode("/", $_SERVER["PHP_SELF"]);
$e = "/";
for ($i = 0; isset($d[$i+1]); $i++) {
  	$e .= $d[$i] . "/";
}
$msg .= "<p><a href=\"http://" . $_SERVER["HTTP_HOST"] . $e . "membersarea.php?page=preferences\">Click Here to Return to the Members Area</a>";

?>
 <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
  <tr> 
     <td class="contentfont"> <table width="90%" border="0" align="center"> 
         <tr> 
          <td align="center" class="contentfont"><br> 
             <br> 
            	<?=$msg?> 
             <br> 
             <br></td> 
        </tr> 
       </table></td> 
   </tr> 
</table> 
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>