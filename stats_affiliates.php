<?php
## v5.24 -> may. 08, 2006
session_start();
include_once ("config/config.php");
include ("themes/".$setts['default_theme']."/header.php");

function Vanus($isikukood)
{
$Birth_year=1900+substr($isikukood,1,2);
$Birth_month=0+substr($isikukood,3,2);
$Birth_day=0+substr($isikukood,3,2);

//Теперь вычислим метку Unix для указанной даты
$birthdate_unix = mktime(0, 0, 0, $Birth_month, $Birth_day, $Birth_year);

//Вычислим метку unix для текущего момента
$current_unix = time();

//Просчитаем разность меток
$period_unix=$current_unix - $birthdate_unix;

// Получаем искомый возраст

// Возраст измеряемый годами
return floor($period_unix / (365*24*60*60));
}
//Updating ages man
 $auth_man=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '3%' and country='Estonia'");
   while ($mout = mysqli_fetch_array($auth_man))
   {
   $res=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET age='".Vanus($mout['isikukood'])."'  WHERE id='".$mout['id']."'");
   };
//Updating ages woman
 $auth_woman=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE active=1 and authenticated='Y' and isikukood like '4%' and country='Estonia'");
   while ($wout = mysqli_fetch_array($auth_woman)){
   $res=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET age='".Vanus($wout['isikukood'])."'  WHERE id='".$wout['id']."'");
}
;

if ($setts['h_counter']==1) {
   	 	$nb_dig_ag_users=getSqlField("SELECT COUNT(DISTINCT refid) AS 'Total' FROM probid_users_friends_invites","Total");
   	 	$nb_dig_ag_invites=getSqlField("SELECT COUNT(*) AS 'Total' FROM probid_users_friends_invites","Total");
   	 	$nb_dig_ag_registered=getSqlField("SELECT COUNT(*) AS 'Total' FROM probid_users_friends_invites WHERE status_reg=1","Total");
   	 	$nb_dig_ag_auth=getSqlField("SELECT COUNT(*) AS 'Total' FROM probid_users_friends_invites WHERE status_auth=1","Total");
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbAuthUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1 and authenticated='Y'");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		$nbFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1","Total");
		$nbRegisteredUsers24h=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-86400));
		$nbRegisteredUsers7d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-604800));
		$nbRegisteredUsers30d=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-2592000));
		$nbRegisteredUsers1y=getSqlNumber("SELECT id FROM probid_users WHERE regdate<".time()." AND regdate>".(time()-31536000));
		$nbopenauctions24h=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbopenauctions7d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbopenauctions30d=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbopenauctions1y=getSqlNumber("SELECT id FROM probid_auctions WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbtotalauctions=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_auctions","Total1");
		$nbauctionsAmount24h=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'","Total1");
		$nbauctionsAmount7d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'","Total1");
		$nbauctionsAmount30d=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'","Total1");
		$nbauctionsAmount1y=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1 AND startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'","Total1");
		$nbauctionsFullAmount=getSqlField("SELECT SUM(bidstart*quantity) AS 'Total1' FROM probid_auctions","Total1");
		$nbsolds24h=getSqlField("SELECT SUM(amount) AS 'Total3' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-86400),"Total3");
		$nbsolds7d=getSqlField("SELECT SUM(amount) AS 'Total4' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-604800),"Total4");
		$nbsolds30d=getSqlField("SELECT SUM(amount) AS 'Total5' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-2592000),"Total5");
		$nbsolds1y=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners WHERE purchase_date<".time()." AND purchase_date>".(time()-31536000),"Total6");
		$nbsoldsTotal=getSqlField("SELECT SUM(amount) AS 'Total6' FROM probid_winners","Total6");
		$nbWantAds24h=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-86400))."'");
		$nbWantAds7d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-604800))."'");
		$nbWantAds30d=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-2592000))."'");
		$nbWantAds1y=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE startdate<'".date('Y-m-d H:i:s',time())."' AND startdate>'".date('Y-m-d H:i:s',(time()-31536000))."'");
		$nbWantAdsTotal=getSqlField("SELECT MAX(id) AS 'Total1' FROM probid_wanted_ads","Total1");
		if ($nbsolds24h>0) $effect24h=($nbsolds24h/$nbauctionsAmount24h)*100; else $effect24h=0;
		if ($nbsolds7d>0) $effect7d=($nbsolds7d/$nbauctionsAmount7d)*100; else $effect7d=0;
		if ($nbsolds30d>0) $effect30d=($nbsolds30d/$nbauctionsAmount30d)*100; else $effect30d=0;
		if ($nbsolds1y>0) $effect1y=($nbsolds1y/$nbauctionsAmount1y)*100; else $effect1y=0;
		if ($nbsoldsTotal>0) $effecttotal=($nbsoldsTotal/$nbauctionsFullAmount)*100; else $effecttotal=0;
	
		
		



include "stat_menu.php";
echo "На этой странице вы можете в реальном времени отслеживать как наши пользователи воспринимают рекламные кампании.";


echo "<div align='center'><h3>Пригласи друга</h3></div>
				<table align='center'>
				<tr class='c3'><td align='right'><b>Приняло участие</b></td><td align='right'><b>".$nb_dig_ag_users."</b></td></tr>
				<tr class='c3'><td align='right'><b>Отправлено приглашений</b></td><td align='right'><b>".$nb_dig_ag_invites."</b></td></tr>
				<tr class='c3'><td align='right'><b>Зарегистрировалось в рамках кампании</b></td><td align='right'><b>".$nb_dig_ag_registered."</b></td></tr>
				<tr class='c3'><td align='right'><b>Авторизировалось в рамках кампании</b></td><td align='right'><b>".$nb_dig_ag_auth."</b></td></tr>
				<tr class='c3'><td align='right'><b>Пользователям выплачено</b></td><td align='right'><b>".($nb_dig_ag_auth*2+$nb_dig_ag_registered)." крон</b></td></tr>";

	}
include ("themes/".$setts['default_theme']."/footer.php"); ?>