<?
## v5.25 -> jun. 15, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }
$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");

function closingdate ($thedate,$offset) {
	global $diff;

	$splitted = explode (" ",$thedate);
	$ourdate = explode ("-",$splitted[0]);
	list($ryear, $rmonth, $rday) = $ourdate;
	$ourtime = explode (":",$splitted[1]);
	list($rhour,$rmin,$rsec) = $ourtime;

	$date = strftime("%Y-%m-%d %H:%M:%S", 
  	mktime($rhour,$rmin,$rsec,$rmonth,$rday+$offset,$ryear)); 
	return $date;
}

function convertdate ($thedate,$format) {
	global $diff;

	$splitted = explode (" ",$thedate);
	$ourdate = explode ("-",$splitted[0]);
	list($ryear, $rmonth, $rday) = $ourdate;
	$ourtime = explode (":",$splitted[1]);
	list($rhour,$rmin,$rsec) = $ourtime;

	$date = date($format, 
  	mktime($rhour+$diff,$rmin,$rsec,$rmonth,$rday,$ryear)); 
	return $date;
}

function displaydatetime($thedate,$format) {
	global $diff;

	$splitted = explode (" ",$thedate);
	$ourdate = explode ("-",$splitted[0]);
	list($ryear, $rmonth, $rday) = $ourdate;
	$ourtime = explode (":",$splitted[1]);
	list($rhour,$rmin,$rsec) = $ourtime;

	$date = date($format, 
  	mktime($rhour+$diff,$rmin,$rsec,$rmonth,$rday,$ryear)); 
	return $date;
}

function daysleft($date,$format) {

### this function will return the number of seconds between 2 dates!!
	$today=date("Y-m-d H:i:s");
	$split_datenow=explode (" ",$today);
	$date_now=explode ("-",$split_datenow[0]);
	list ($nyear,$nmonth,$nday)=$date_now;
	$time_now=explode (":",$split_datenow[1]);
	list ($nhour,$nmin,$nsec)=$time_now;
	$datenow=mktime($nhour,$nmin,$nsec,$nmonth,$nday,$nyear);
	
	$arrDateTime = explode(" ", $date);
	$arrDate = explode("-", $arrDateTime[0]);
	$arrTime = explode(":", $arrDateTime[1]);
	$daysleft=mktime((int)$arrTime[0],(int)$arrTime[1],(int)$arrTime[2],(int)$arrDate[1],(int)$arrDate[2],(int)$arrDate[0]);

	$daycount=intval($daysleft-$datenow); 
	
	
	return $daycount;
}

function timeleft($date,$format) {
    global $lang;
	## revamped function -> v5.20
	$currentTime = time();
		
    $arrDateTime = explode(" ", $date);
    $arrDate = explode("-", $arrDateTime[0]);
    $arrTime = explode(":", $arrDateTime[1]);

	$endDate = mktime($arrTime[0],$arrTime[1],$arrTime[2],$arrDate[1],$arrDate[2],$arrDate[0]);
	
	$secondsLeft = $endDate - $currentTime;

	$day = 24*60*60;
	$hour = 60*60;
	$minute = 60;
	
	$daysLeft = floor($secondsLeft/$day);
	$hours = $secondsLeft - ($daysLeft * $day);
	$hoursLeft = floor($hours/$hour);
	$minutes = $hours - ($hoursLeft * $hour);
	$minutesLeft = floor($minutes/$minute);
	

	$timecount = (($daysLeft>0) ? $daysLeft." ".(($daysLeft==1) ? $lang['day'] : $lang['days']).", " : "").(($hoursLeft>0||$daysLeft>0) ? $hoursLeft.$lang['h']." " : "").$minutesLeft.$lang['m'];
	
	return $timecount;
}

function convert_date($date) {
// ###############################
// this function converts a UNIX timestamp into a date
	$result=date("M. j, Y",$date);
	return $result;
}

?>