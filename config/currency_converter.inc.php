<?php
## v5.10 -> feb. 21, 2005
/*
#///////////////////////////////////////////////////////
#//  MOD NAME: CURRENCY CONVERTER FOR PPB AUCTIONS
#//  v.1.0.0
#//  COPYRIGHT 2005 
#//  Henk C. Huitema & Kevin Thomas 
#//  ALL RIGHTS RESERVED
#///////////////////////////////////////////////////////
*/

Function ConvertCurrency($FROM,$INTO,$AMOUNT)
{
	$query = "SELECT * FROM probid_rates WHERE symbol='$FROM'";
	$R 		= mysqli_query($GLOBALS["___mysqli_ston"], $query);
	if(!$R)
	{
		print "Error: $query<BR>".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
		exit;
	}
	$F_RATE	= mysqli_fetch_array($R);

	$query = "SELECT * FROM probid_rates WHERE symbol='$INTO'";
	$R_ 	= mysqli_query($GLOBALS["___mysqli_ston"], $query);
	if(!$R_)
	{
		print "Error: $query<BR>".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
		exit;
	}
	$I_RATE	= mysqli_fetch_array($R_);

	$VAL 	= doubleval($AMOUNT);
	return $VAL/$F_RATE[rate]*$I_RATE[rate];
}

?>