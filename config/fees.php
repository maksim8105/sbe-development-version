<?
## v5.25 -> jun. 08, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }
#######################################################
### first we have the payment gateway forms functions

function paypalForm($transactionId,$paypalEmail,$paymentAmount,$currency,$returnUrl,$failureUrl,$notifyUrl,$mTable,$paymentDescription=NULL,$directPayment=FALSE,$postUrl='https://www.paypal.com/cgi-bin/webscr') {
	global $lang, $setts;

	$paymentDesc = ($paymentDescription==NULL) ? $lang[paypalpayment] : $paymentDescription;	
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "   	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/paypal_logo.gif\"></td>\n";
	### the description will go below
	echo "		<form action=\"".$postUrl."\" method=\"post\" id=\"payform\">				\n";
   ## echo "	<td class=\"paytable2\" width=\"100%\">$lang[paypal_description_new]".(($directPayment)? "<br>".displayAmount($paymentAmount,$currency) :"");
	echo "		<td class=\"paytable2\" width=\"100%\">".$lang[paypal_description_new]."<br>".displayAmount($paymentAmount,$currency);
	## this setting is for the direct payment feature
	if ($directPayment) {
		echo "<br><strong>$lang[add_insurance]:</strong> $currency <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
	}
	echo "  </td>\n";
	### the payment gateway from will go below
   echo "	<td class=\"paytable3\">								\n".
	     "		 <input type=\"hidden\" name=\"cmd\" value=\"_xclick\">								\n".
	     "		 <input type=\"hidden\" name=\"bn\" value=\"wa_dw_2.0.4\">							\n".
		  "	 	 <input type=\"hidden\" name=\"business\" value=\"".$paypalEmail."\">			\n".
		  "		 <input type=\"hidden\" name=\"receiver_email\" value=\"".$paypalEmail."\">	\n".
		  "		 <input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">			\n".
    	  "		 <input type=\"hidden\" name=\"currency_code\" value=\"".$currency."\">			\n".
	     "		 <input type=\"hidden\" name=\"return\" value=\"".$returnUrl."\">					\n".
	     "		 <input type=\"hidden\" name=\"cancel_return\" value=\"".$failureUrl."\">		\n".
		  "	  	 <input type=\"hidden\" name=\"item_name\" value=\"".$paymentDesc."\">\n".
		  "		 <input type=\"hidden\" name=\"undefined_quantity\" value=\"0\">					\n".
    	  "		 <input type=\"hidden\" name=\"no_shipping\" value=\"1\">							\n".
		  "		 <input type=\"hidden\" name=\"no_note\" value=\"1\">									\n".
		  "	 	 <input type=\"hidden\" name=\"custom\" value=\"".$transactionId."TBL".$mTable."\">\n".
		  " 		 <input type=\"hidden\" name=\"notify_url\" value=\"".$notifyUrl."\">			\n".
	     " 		 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\">\n".
		  "		</td></form>\n".
		  "	</tr>\n".
		  "</table>";
}

function nochexForm($transactionId,$paypalEmail,$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,$mTable,$directPayment=FALSE,$postUrl='https://www.nochex.com/nochex.dll/checkout') {
	global $lang, $setts;
	### this will be the new payment form design

	$transaction_code = md5(uniqid(mt_rand(), true));

	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/nochex_logo.gif\"></td>\n";
	### the description will go below
	echo "		<form action=\"".$postUrl."\" method=\"post\" id=\"payform\"> 		\n";
   echo "		<td class=\"paytable2\" width=\"100%\">".$lang[nochex_description_new]."<br>".displayAmount($paymentAmount,$currency);
	if ($directPayment) {
			echo "<br><strong>$lang[add_insurance]:</strong> $currency <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
	}
	echo "  </td>\n";
	### the payment gateway from will go below
   echo "		<td class=\"paytable3\">								\n".
		  "		 <input type=\"hidden\" name=\"returnurl\" value=\"".$returnUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"cancelurl\" value=\"".$failureUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"email\" value=\"".$paypalEmail."\">					\n".
		  "		 <input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">				\n".
		  "		 <input type=\"hidden\" name=\"responderurl\" value=\"".$notifyUrl."\">				\n".
		  " 		 <input type=\"hidden\" name=\"ordernumber\" value=\"".$transactionId."TBL".$mTable."TBL".$transaction_code."\">\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

function checkoutForm($transactionId,$checkoutId,$paymentAmount,$orderNumber,$directPayment=FALSE,$postUrl='https://www2.2checkout.com/2co/buyer/purchase') {
	global $lang, $setts;
	## v1 LINK
	$coLink_v1 = "https://www.2checkout.com/cgi-bin/sbuyers/cartpurchase.2c";
	## v2 LINK
	$coLink_v2 = "https://www2.2checkout.com/2co/buyer/purchase";
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/checkout_logo.gif\"></td>\n";
	### the description will go below
   echo "		<td class=\"paytable2\" width=\"100%\">".$lang[checkout_description_new]."<br>".displayAmount($paymentAmount,'USD');
	if ($directPayment) {
		echo "<form action=\"".$postUrl."\" method=POST id=\"payform\">				\n";
		echo "<br><strong>".$lang[add_insurance].":</strong> USD <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
      echo "<input type=\"hidden\" name=\"sid\" value=\"".$checkoutId."\">        \n";
      echo "<input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">  \n";
	   echo "</td>\n";
      echo "<td class=\"paytable3\">								\n";
	   echo "<input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	   echo "</td></form>\n";
	} else {
      echo "      </td>\n";
	### the payment gateway from will go below
      echo "		<td class=\"paytable3\">								\n";
	   echo "  	 		<a id=\"paylink\" href=\"".$postUrl."?sid=".$checkoutId."&total=".$paymentAmount."&cart_order_id=".$transactionId."&order_number=".$orderNumber."\"><img src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"></a>\n";
		echo "		</td>\n";
    }
  	echo "	</tr>\n";
	echo "</table>";
}

function worldpayForm($transactionId,$worldpayId,$paymentAmount,$currency,$returnUrl,$failureUrl,$notifyUrl,$mTable,$directPayment=FALSE,$postUrl='https://select.worldpay.com/wcc/purchase') {
	global $lang, $setts;
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo " 		<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/worldpay_logo.gif\"></td>\n";
	### the description will go below
	echo "		<form action=\"".$postUrl."\" method=POST id=\"payform\">				\n";
   echo "		<td class=\"paytable2\" width=\"100%\">".$lang[worldpay_description_new]."<br>".displayAmount($paymentAmount,$currency);
	if ($directPayment) {
		echo "<br><strong>$lang[add_insurance]:</strong> $currency <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
	}
	echo "  </td>\n";
	### the payment gateway from will go below
   echo "		<td class=\"paytable3\">								\n".
	 	  "	 	 <input type=hidden name=\"instId\" value=\"".$worldpayId."\">						\n". 
		  "	 	 <input type=hidden name=\"cartId\" value=\"".$transactionId."\"> 					\n".
		  "	 	 <input type=hidden name=\"amount\" value=\"".$paymentAmount."\"> 					\n".
		  "	 	 <input type=hidden name=\"currency\" value=\"".$currency."\"> 						\n".
		  "	  	 <input type=hidden name=\"desc\" value=\"".$lang[serv_act_fee]."\"> 				\n".
		  "	 	 <input type=hidden name=MC_callback value=\"".$notifyUrl."\">						\n".
		  "	 	 <input type=hidden name=M_table value=\"".$mTable."\">								\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

function ikoboForm($transactionId,$ikoboMbid,$ikoboIpnPass,$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,$mTable,$directPayment=FALSE,$postUrl='https://www.ikobo.com/store/index.php') {
	global $lang, $setts;
  	$orderid = $transactionId . "-" . $mTable . "-" . md5(rand());
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo " 		<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/ikobo_logo.gif\"></td>\n";
	### the description will go below
  	echo "		<FORM METHOD=\"POST\" ACTION=\"".$postUrl."\" id=\"payform\">			\n";
   echo "		<td class=\"paytable2\" width=\"100%\">".$lang[ikobo_description_new]."<br>".displayAmount($paymentAmount,'USD')."\n";
	if ($directPayment) {
		echo "<br><strong>".$lang[add_insurance].":</strong> USD <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
	}
	echo "  </td>\n";
	### the payment gateway from will go below
   echo "		<td class=\"paytable3\">								\n".
  		  "		 <input type=\"hidden\" name=\"cmd\" value=\"cart\">								\n".
  		  "		 <input type=\"hidden\" name=\"item_id\" value=\"".$orderid."\">					\n".
	  	  "		 <input type=\"hidden\" name=\"item\" value=\"".$lang[serv_act_fee]."\">			\n".
  		  "		 <input type=\"hidden\" name=\"price\" value=\"".$paymentAmount."\">				\n".
  		  "		 <input type=\"hidden\" name=\"poid\" value=\"".$ikoboMbid."\">						\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

function protxForm($transactionId,$paymentAmount,$currency,$returnUrl,$failureUrl,$notifyUrl,$mTable,$directPayment=FALSE,$postUrl='https://ukvps.protx.com/vps2form/submit.asp') {
	global $lang, $setts;

	$transaction_code = md5(uniqid(mt_rand(), true));
	if ($mTable == 1 || $mTable == 4) $userId = $transactionId;
	else if ($mTable == 2 || $mTable == 3) $userId = getSqlField("SELECT ownerid FROM probid_auctions WHERE id='".$transactionId."'","ownerid");

   if($mTable == 100){
   	$userId = getSqlField("SELECT sellerid FROM probid_winners WHERE id='".$transactionId."'",'sellerid');
   }

	$userDetails = getSqlRow ("SELECT email, address, zip, protxname, protxpassword FROM probid_users WHERE id='".$userId."'");

   if($mTable == 100){
   	$vendorname = $userDetails['protxname'];
	   $password = $userDetails['protxpassword'];
   } else {
      $vendorname = $setts['protxname'];
	   $password = $setts['protxpass'];
   }
	
	$string  = "VendorTxCode=" . $transactionId . '-' . $mTable . '-' . md5(uniqid(rand(2,10000))). "&";
	$string .= "Amount=" . $paymentAmount . "&";
	$string .= "Currency=" . $currency . "&";
	$string .= "Description=Service Activation Fee&";
	$string .= "CustomerEmail=".$userDetails['email']."&";	
	$string .= "SuccessURL=".$notifyUrl."&";
	$string .= "FailureURL=".$notifyUrl."&";
	$string .= "BillingAddress=".$userDetails['address']."&";
	$string .= "BillingPostCode=".$userDetails['zip']."";
	$key_values = array();

	for($i = 0; $i < strlen($password); $i++) $key_values[$i] = ord(substr($password, $i, 1));
	$secret_string = '';
	for($i = 0; $i < strlen($string); $i++) $secret_string .= chr(ord(substr($string, $i, 1)) ^ ($key_values[$i % strlen($password)]));
	$secret_string = base64_encode($secret_string);

	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo " 		<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/protx_logo.gif\"></td>\n";
	echo "		<form method=\"post\" action=\"".$postUrl."\" name=\"payform\" id=\"payform\">\n";
	### the description will go below
   echo "		<td class=\"paytable2\" width=\"100%\">".$lang[protx_description_new]."<br>".displayAmount($paymentAmount,$currency)."\n";
	if ($directPayment) {
		echo "<br><strong>".$lang[add_insurance].":</strong> ".$currency." <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
		echo "<input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">";
	}
	echo "  </td>\n";
	### the payment gateway from will go below
   echo "		<td class=\"paytable3\">					\n".
		  "		 <input type=\"hidden\" name=\"VPSProtocol\" value=\"2.21\">			\n".
		  "		 <input type=\"hidden\" name=\"TxType\" value=\"PAYMENT\">				\n".
		  "		 <input type=\"hidden\" name=\"Vendor\" value=\"".$vendorname."\">\n".
		  "		 <input type=\"hidden\" name=\"Crypt\" value=\"".$secret_string."\">	\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

function testmodeForm($transactionId,$paypalEmail,$paymentAmount,$currency,$returnUrl,$failureUrl,$notifyUrl,$mTable) {
	global $lang, $setts;
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo " 		<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/phpprosim_logo.gif\"></td>\n";
	### the description will go below
   echo "		<td class=\"paytable2\" width=\"100%\">$lang[testmode_description_new]</td>\n";
	### the payment gateway from will go below
	echo "		<form action=\"paymentsimulator.php\" method=\"post\">							\n";
	echo "		<td class=\"paytable3\">							\n".
	     "		 <input type=\"hidden\" name=\"table\" value=\"".$mTable."\">					\n".
		  "		 <input type=\"hidden\" name=\"business\" value=\"".$paypalEmail."\">			\n".
		  "		 <input type=\"hidden\" name=\"receiver_email\" value=\"".$paypalEmail."\">		\n".
		  "		 <input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">			\n".
    	  "		 <input type=\"hidden\" name=\"currency_code\" value=\"".$currency."\">			\n".
	     "		 <input type=\"hidden\" name=\"return\" value=\"".$returnUrl."\">				\n".
	     "		 <input type=\"hidden\" name=\"cancel_return\" value=\"".$failureUrl."\">		\n".
		  "		 <input type=\"hidden\" name=\"undefined_quantity\" value=\"0\">				\n".
    	  "		 <input type=\"hidden\" name=\"no_shipping\" value=\"1\">						\n".
		  "		 <input type=\"hidden\" name=\"no_note\" value=\"1\">							\n".
		  "		 <input type=\"hidden\" name=\"custom\" value=\"".$transactionId."\">			\n".
		  "		 <input type=\"hidden\" name=\"notify_url\" value=\"".$notifyUrl."\">			\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

## BEGIN of Authorize.Net related functions
function hmac ($key, $data) {
   // RFC 2104 HMAC implementation for php.
   // Creates an md5 HMAC.
   // Eliminates the need to install mhash to compute a HMAC
   // Hacked by Lance Rushing

   $b = 64; // byte length for md5
   if (strlen($key) > $b) {
       $key = pack("H*",md5($key));
   }
   $key  = str_pad($key, $b, chr(0x00));
   $ipad = str_pad('', $b, chr(0x36));
   $opad = str_pad('', $b, chr(0x5c));
   $k_ipad = $key ^ $ipad ;
   $k_opad = $key ^ $opad;

   return md5($k_opad  . pack("H*",md5($k_ipad . $data)));
}

// Inserts the hidden variables in the HTML FORM required for SIM
// Invokes hmac function to calculate fingerprint.
function InsertFP ($loginid, $x_tran_key, $amount, $sequence, $currency = "") {
	$tstamp = time();
	$fingerprint = hmac ($x_tran_key, $loginid . "^" . $sequence . "^" . $tstamp . "^" . $amount . "^");
	echo ('<input type="hidden" name="x_fp_sequence" value="' . $sequence . '">');
	echo ('<input type="hidden" name="x_fp_timestamp" value="' . $tstamp . '">');
	echo ('<input type="hidden" name="x_fp_hash" value="' . $fingerprint . '">');
	return (0);
}

function authorizeNetForm($transactionId, $paymentAmount, $mTable,$directPayment=FALSE,$postUrl='https://secure.authorize.net/gateway/transact.dll') {
	global $lang, $setts;

	$TESTMODE = 0;
	## UnComment this line out to enter Testing Mode!
	## $TESTMODE = 1;

	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo " 		<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/authorize_logo.gif\"></td>\n";
	if ($TESTMODE) { 
		echo "<form action=\"https://certification.authorize.net/gateway/transact.dll\" method=\"POST\">";
	} else { 
		echo "<form action=\"".$postUrl."\" method=\"POST\" id=\"payform\">";
	}
	### the description will go below
   echo "		<td class=\"paytable2\" width=\"100%\">".$lang[authorize_net_description_new]."<br>".displayAmount($paymentAmount,'USD')."\n";;
	if ($directPayment) {
		echo "<br><strong>".$lang[add_insurance].":</strong> USD <input type=\"text\" name=\"insurance\" value=\"\" size=\"7\">\n";
	} 
	echo "  		</td>\n";
	### the payment gateway from will go below
	echo "		<td class=\"paytable3\">							\n";
	
   if($mTable == 100){
      $userId = getSqlField("SELECT sellerid FROM probid_winners WHERE id='".$transactionId."'",'sellerid');
		$userDetails = getSqlRow ("SELECT authnetid, authnettranskey FROM probid_users WHERE id='".$userId."'");
      $authid = $userDetails['authnetid'];
      $authkey = $userDetails['authnettranskey'];
   }else{
      $authid = $setts['authnetid'];
      $authkey = $setts['authnettranskey'];
   }

	$amount = $paymentAmount;
	if (substr($amount, 0,1) == "$") {
		$amount = substr($amount,1);
	}
	srand(time());
	$sequence = rand(1, 1000);
	$ret = InsertFP($authid, $authkey, $amount, $sequence);
	echo "<input type=\"hidden\" name=\"x_description\" value=\"".$lang[serv_act_fee]."\">\n";
	echo "<input type=\"hidden\" name=\"x_login\" value=\"" . $authid . "\">\n";
	echo "<input type=\"hidden\" name=\"x_amount\" value=\"" . $amount . "\">\n";
	echo "<input type=\"hidden\" name=\"x_show_form\" value=\"PAYMENT_FORM\">\n";
	echo "<input type=\"hidden\" name=\"x_relay_response\" value=\"TRUE\">\n";
	echo "<input type=\"hidden\" name=\"ProBidID\" value=\"".$transactionId."TBL".$mTable."\">\n";
	if ($TESTMODE) { 
		echo "<input type=\"hidden\" name=\"x_test_request\" value=\"TRUE\">";
	}
	echo "		<input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

## END of Authorize.Net related functions 

### moneybookers form
function moneybookersForm($transactionId,$paymentAmount,$mTable) {
	global $lang, $setts, $path;
		
	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	$notifyUrl=$path."moneybookers.process.php";
	
	$mb_trans_id = md5(uniqid(rand()));
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/mb_logo.gif\"></td>\n";
	### the description will go below
    echo "		<td class=\"paytable2\" width=\"100%\">$lang[mb_description_new]</td>\n";
	### the payment gateway from will go below

	echo "		<form action=\"https://www.moneybookers.com/app/payment.pl\" method=\"post\"> 		\n";
   echo "		<td class=\"paytable3\">								\n".
	 	  "		 <input type=\"hidden\" name=\"pay_to_email\" value=\"".$setts['moneybookersemail']."\"> 	\n".
		  "  	 	 <input type=\"hidden\" name=\"transaction_id\" value=\"".$mb_trans_id."\">			\n".
		  "		 <input type=\"hidden\" name=\"return_url\" value=\"".$returnUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"cancel_url\" value=\"".$failureUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"status_url\" value=\"".$notifyUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"language\" value=\"EN\">								\n".
		  "		 <input type=\"hidden\" name=\"merchant_fields\" value=\"transactionid\">			\n".
		  "		 <input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">				\n".
		  "		 <input type=\"hidden\" name=\"currency\" value=\"".$setts['currency']."\">			\n".
		  " 		 <input type=\"hidden\" name=\"transactionid\" value=\"".$transactionId."TBL".$mTable."\">\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

### HANSAPANK test function
function HansapankForm($transactionId,$paymentAmount,$mTable) {
	global $lang, $setts, $path;
		
	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	$notifyUrl="http://www.hanza.net";
	
	$HP_trans_id = md5(uniqid(rand()));
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
#	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/hansapank_logo.gif\"></td>\n";
	echo "    	<td width=\"30%\" class=\"paytable1\">$lang[HP_client]</td>\n";
	### the description will go below
    echo "		<td class=\"paytable2\" width=\"70%\">$lang[HP_description_new]</td>\n";
	### the payment gateway from will go below

	echo "		<form action=\"https://www.hanza.net\" method=\"post\"> 		\n";
   echo "		<td class=\"paytable3\">								\n".
	## 	  "		 <input type=\"hidden\" name=\"pay_to_email\" value=\"".$setts['moneybookersemail']."\"> 	\n".
		  "  	 	 <input type=\"hidden\" name=\"transaction_id\" value=\"".$HP_trans_id."\">			\n".
		  "		 <input type=\"hidden\" name=\"return_url\" value=\"".$returnUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"cancel_url\" value=\"".$failureUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"status_url\" value=\"".$notifyUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"language\" value=\"EN\">								\n".
		  "		 <input type=\"hidden\" name=\"merchant_fields\" value=\"transactionid\">			\n".
		  "		 <input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">				\n".
		  "		 <input type=\"hidden\" name=\"currency\" value=\"".$setts['currency']."\">			\n".
		  " 		 <input type=\"hidden\" name=\"transactionid\" value=\"".$transactionId."TBL".$mTable."\">\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/hansapank_logo.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

### SEB test function
function SEBForm($transactionId,$paymentAmount,$mTable) {
	global $lang, $setts, $path;
		
	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	$notifyUrl="http://www.seb.ee";
	
	$HP_trans_id = md5(uniqid(rand()));
	### this will be the new payment form design
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
 	echo "	<tr>\n";
	### the PG image will go below
#	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/seb_logo.gif\"></td>\n";
	echo "    	<td width=\"30%\" class=\"paytable1\">$lang[SEB_client]</td>\n";
	### the description will go below
    echo "		<td class=\"paytable2\" width=\"70%\">$lang[SEB_description_new]</td>\n";
	### the payment gateway from will go below

	echo "		<form action=\"https://www.seb.ee\" method=\"post\"> 		\n";
    echo "		<td class=\"paytable3\">								\n".
	## 	  "		 <input type=\"hidden\" name=\"pay_to_email\" value=\"".$setts['moneybookersemail']."\"> 	\n".
		  "  	 	 <input type=\"hidden\" name=\"transaction_id\" value=\"".$HP_trans_id."\">			\n".
		  "		 <input type=\"hidden\" name=\"return_url\" value=\"".$returnUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"cancel_url\" value=\"".$failureUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"status_url\" value=\"".$notifyUrl."\">				\n".
		  "		 <input type=\"hidden\" name=\"language\" value=\"EN\">								\n".
		  "		 <input type=\"hidden\" name=\"merchant_fields\" value=\"transactionid\">			\n".
		  "		 <input type=\"hidden\" name=\"amount\" value=\"".$paymentAmount."\">				\n".
		  "		 <input type=\"hidden\" name=\"currency\" value=\"".$setts['currency']."\">			\n".
		  " 		 <input type=\"hidden\" name=\"transactionid\" value=\"".$transactionId."TBL".$mTable."\">\n".
	     "	  	 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/seb_logo.gif\" border=\"0\"> \n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
}

### this is the function that shows the text with the payment processors and the amount owed.
function displayPaymentMessage($paymentAmount) {
	global $lang, $setts;
	echo "<table width=100% cellpadding=4 cellspacing=4 class=\"paymenttable\">\n";
	echo "<tr>";
	echo "<td align=center>$lang[supported_gateways]<br>";
	$getActivePGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT name FROM probid_payment_gateways WHERE value='checked'");
	while($activePG = mysqli_fetch_array($getActivePGs)) {
		echo "[ <font class=\"payactive\">".$activePG['name']."</font> ] ";
	}
	echo "<br>$lang[act_account]<br><br>";
	echo "$lang[pay_fee_of] ".displayAmount($paymentAmount);
	echo "</td></tr></table><br>";
}

### this is the function for the signup fee
### new and updated function with the revamp!!! All the functions are updated
function signupFee($amount,$currency,$userid) {
	global $path, $setts, $lang, $fee;

	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userid."'","payment_mode");
    if ($setts['account_mode_personal']==1) {
    	$account_mode_local = ($tmp) ? 2 : 1;
  	} else $account_mode_local = $setts['account_mode'];

	if ($fee['is_signup_fee']=="Y"&&$fee['val_signup_fee']>0) {
	## Live payment mode
		$returnUrl=$path."paymentdone.php";
		$failureUrl=$path."paymentfailed.php";
		if ($amount==0) $setts['payment_gateway']="none";
		
		$amount = applyVat($amount, $userid);
		
		$paymentAmount=number_format($amount,2,'.','');
		
		### new function that displays the payment message and amount
		displayPaymentMessage($paymentAmount);
	
		### new procedure to list all active payment gateways
		if ($setts['payment_gateway']=="none") {
			echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
			$activateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			active = '1',payment_status='confirmed' WHERE id='".$userid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
			while ($selectedPGs = mysqli_fetch_array($getPGs)) {
				if ($selectedPGs['name']=="Paypal") {
					$pmDesc = $setts['sitename'].' - '.$lang[user_sign_fee];
					$notifyUrl=$path."paymentprocess.php?table=1";
					paypalForm($userid,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,1,$pmDesc);
				}	
				if ($selectedPGs['name']=="Nochex") {
					$notifyUrl=$path."nochexprocess.php?table=1";
					nochexForm($userid,$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,1);
				}
				if ($selectedPGs['name']=="2Checkout") {
					$notifyUrl=$path."checkoutprocess.php?table=1";
					checkoutForm($userid,$setts['checkoutid'],$paymentAmount,1);
				}
				if ($selectedPGs['name']=="Worldpay") {
					$notifyUrl=$path."worldpayprocess.php?table=1";
					worldpayForm($userid,$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,1);
				}
				if ($selectedPGs['name']=="Ikobo") {
				 	$notifyUrl=$path."ikoboprocess.php?table=1";
					ikoboForm($userid,$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,1);
				}
				if ($selectedPGs['name']=="Protx") {
					$notifyUrl=$path."protxprocess.php?table=1";
					protxForm($userid,$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,1);
				}
				if ($selectedPGs['name']=="Authorize.Net") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					authorizeNetForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="Moneybookers") {
					$notifyUrl=$path."moneybookers.process.php?table=1";
					moneybookersForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="Test Mode") {
					$notifyUrl=$path."paymentsimulator.php?table=1";
					testmodeForm($userid,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,1);
				}
				if ($selectedPGs['name']=="Hansapank") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					HansapankForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="SEB") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					authorizeNetForm($userid,$paymentAmount,1);
				}
			}
		}		
	} else {
		if ($account_mode_local!=2) echo "<p class=contentfont>$lang[reg_emailsent]</p>";
	}
		
	if ($account_mode_local==2) {
		// account mode thing

		$balance=0;
		//$balance+=$amount;
		$balance-=$setts['init_credit'];
		$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		balance=".$balance." WHERE id='".$userid."'");
		if ($setts['enable_ra']=="Y"&&$fee['is_signup_fee']!="Y") echo "<p align=center class=contentfont>".$lang[reg_emailsent_approval]."<br><br>";
		else if ($setts['enable_ra']=="Y"&&$fee['is_signup_fee']=="Y"&&$fee['val_signup_fee']>0) echo "<p align=center class=contentfont>".$lang[acct_needs_payment];
		else echo "<p align=center class=contentfont>".$lang[accountcreated]."<br><br>";
		// insert into invoices table
		$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$userid."'","balance");
		$newBalance = $currentBalance;
		$currentTime = time();
		$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
		(userid,feename,feevalue,feedate,balance) VALUES 
		('".$userid."','".remSpecialChars($lang[user_sign_fee])."','".$amount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}	
}

### this is the function for the auction setup fee
#####################################################
## Revamped Function -> v5.24								##
#####################################################
function setupFee($startPrice,$currency,$auctionId,$isFee, $editAuction=FALSE,$showOnly=FALSE,$voucherCode="",$providedCat=0, $displayMsgs = TRUE, $isRelist = FALSE) {
	global $path, $setts, $lang, $fee;
##$isHp,$isCat,$isBold,$isHl,$isReserve,$picCount,$isSecondcat,$isBin,

	## v5.22 addon -> if the currency is different from the default one, then apply the converter.
	$exchange_rate = 1;
	if (trim($setts['currency'])!=trim($currency)) {
		$converter = getSqlField("SELECT converter FROM probid_currencies WHERE symbol = '".trim($currency)."'","converter");
		$converter = ($converter == 0) ? 1 : $converter;
		$exchange_rate = 1/$converter;
	}
	if($exchange_rate<=0) $exchange_rate = 1;

	##feeCategory -> its selected either from the providedCat field (sellitem-step12) or from the auction's main category
	if ($providedCat == 0) $category_id = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auctionId."'","category");
	else $category_id = $providedCat;
	
	$category_id = getMainCat($category_id);
		
	$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
	$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
	
	$relistReduction = ($isRelist) ? $fee['relist_fee_reduction'] : 0;
	
	$actItemA = getSqlField("SELECT active FROM probid_auctions WHERE id='".$auctionId."'","active");
	$userid = getSqlField("SELECT * FROM probid_auctions WHERE id='".$auctionId."'","ownerid");
	if (!$auctionId) $userid = $_SESSION['memberid'];
	#### check if the user is a preferred seller
	$prefSeller = "N";
	if (@eregi('Y',$setts['pref_sellers'])) {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$userid."'","preferred_seller");
	}
	
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userid."'","payment_mode");
    if ($setts['account_mode_personal']==1) {
    	$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	#### calculate the amount that has to be paid for this auction's setup
	$amount=0;
	$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$userid."'","balance");
	$newBalance = $currentBalance;
	if (@eregi('Y', $fee['is_setup_fee']) && !$editAuction) {
		
		$listingFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE fee_from<=".$startPrice." AND fee_to>".$startPrice." AND fee_type='setup' AND category_id='".$category_id."'");
		if (@eregi('percent', $listingFee['calc_type'])) {
			$amount+=applyVat(($listingFee['fee_amount']/100)*$startPrice*$exchange_rate,$userid);
			$invoiceAmount=applyVat(($listingFee['fee_amount']/100)*$startPrice*$exchange_rate,$userid);
		} else {
			$amount+=applyVat($listingFee['fee_amount'],$userid);
			$invoiceAmount=applyVat($listingFee['fee_amount'],$userid);
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"setup");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);
			
			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance) VALUES 
			('".$userid."','".$auctionId."','".remSpecialChars($lang[auc_setup_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	if (@eregi('Y', $fee['is_pic_fee']) && $isFee['pic_count']>0) {
		$amount+=applyVat($fee['val_pic_fee'],$userid);
		$invoiceAmount=applyVat($fee['val_pic_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"pic");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)	VALUES 
			('".$userid."','".$auctionId."','".remSpecialChars($lang[add_pic_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	if (@eregi('Y', $fee['is_catfeat_fee']) && $fee['val_catfeat_fee']>0 && @eregi('Y', $isFee['catfeat'])) {
		$amount+=applyVat($fee['val_catfeat_fee'],$userid);
		$invoiceAmount=applyVat($fee['val_catfeat_fee'],$userid);

		### if we are editing the auction, see if there are 2 cats and multiply the amount
		$secCat = getSqlField("SELECT addlcategory FROM probid_auctions WHERE id='".$auctionId."'","addlcategory");
		$featSecondcat = "N";
		if ($editAuction&&$secCat>0) $featSecondcat = "Y";
		
		if (@eregi('Y', $isFee['secondcat']) || @eregi('Y', $featSecondcat)) {
			$amount = $amount+$invoiceAmount;
			$invoiceAmount = $invoiceAmount*2;
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"catfeat");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[cat_page_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if (@eregi('Y', $fee['is_bolditem_fee']) && $fee['val_bolditem_fee']>0 && @eregi('Y', $isFee['bold'])) {
		$amount+=applyVat($fee['val_bolditem_fee'],$userid);
		$invoiceAmount=applyVat($fee['val_bolditem_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"bold");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[bold_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if (@eregi('Y', $fee['is_hlitem_fee']) && $fee['val_hlitem_fee']>0 && @eregi('Y', $isFee['hl'])) {
		$amount+=applyVat($fee['val_hlitem_fee'],$userid);
		$invoiceAmount=applyVat($fee['val_hlitem_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"hl");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[highl_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if (@eregi('Y', $fee['is_hpfeat_fee']) && $fee['val_hpfeat_fee']>0 && @eregi('Y', $isFee['hpfeat'])) {
		$amount+=applyVat($fee['val_hpfeat_fee'],$userid);
		$invoiceAmount=applyVat($fee['val_hpfeat_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"hpfeat");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[home_page_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if (@eregi('Y', $fee['is_rp_fee']) && $fee['val_rp_fee']>0 && @eregi('Y', $isFee['rp'])) {
		$amount+=applyVat($fee['val_rp_fee'] ,$userid);
		$invoiceAmount=applyVat($fee['val_rp_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"rp");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[res_price_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if ($fee['second_cat_fee']>0 && @eregi('Y', $isFee['secondcat'])) {
		$amount+=applyVat($fee['second_cat_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['second_cat_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"seccat");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[second_cat_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if ($fee['bin_fee']>0 && @eregi('Y', $isFee['bin'])) {
		$amount+=applyVat($fee['bin_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['bin_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"bn");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}
	
		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[bin_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	if ($fee['custom_st_fee']>0 && @eregi('Y', $isFee['customst'])) {
		$amount+=applyVat($fee['custom_st_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['custom_st_fee'],$userid);

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[custom_st_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	if ($fee['videofile_fee']>0 && @eregi('Y', $isFee['videofee'])) {
		$amount+=applyVat($fee['videofile_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['videofile_fee'],$userid);

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller, "", "", $relistReduction);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[videofile_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	if ($amount==0) $setts['payment_gateway']="none";

	### setts for the preferred sellers reduction
	$amount = calcReduction ($amount, $prefSeller, "", "", $relistReduction);

	if (!$showOnly) {
		if ($account_mode_local==1) {
			$paymentAmount=number_format($amount,2,'.','');
			### new function that displays the payment message and amount
			displayPaymentMessage($paymentAmount);
	
			### new procedure to list all active payment gateways
			if ($setts['payment_gateway']=="none") {
				if ($displayMsgs) echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
				$activateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
				active = '1',payment_status='confirmed' WHERE id='".$userid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				## if there are no fees to pay, add counter
				$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$auctionId."'");
				if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
					addcatcount ($auctCat['category'],$auctionId);
					addcatcount ($auctCat['addlcategory'],$auctionId);
				}
			} else {
				$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
				while ($selectedPGs = mysqli_fetch_array($getPGs)) {
					if ($selectedPGs['name']=="Paypal") {
						$pmDesc = $setts['sitename'].' - '.$lang[aucsetupfees];
						$notifyUrl=$path."paymentprocess.php?table=2";
						paypalForm($auctionId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2,$pmDesc);
					}	
					if ($selectedPGs['name']=="Nochex") {
						$notifyUrl=$path."nochexprocess.php?table=2";
						nochexForm($auctionId,$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="2Checkout") {
						$notifyUrl=$path."checkoutprocess.php?table=2";
						checkoutForm($auctionId,$setts['checkoutid'],$paymentAmount,2);
					}
					if ($selectedPGs['name']=="Worldpay") {
						$notifyUrl=$path."worldpayprocess.php?table=2";
						worldpayForm($auctionId,$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Ikobo") {
					 	$notifyUrl=$path."ikoboprocess.php?table=2";
						ikoboForm($auctionId,$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Protx") {
						$notifyUrl=$path."protxprocess.php?table=2";
						protxForm($auctionId,$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Authorize.Net") {
						$notifyUrl=$path."authorize.net.process.php?table=2";
						authorizeNetForm($auctionId,$paymentAmount,2);
					}
					if ($selectedPGs['name']=="Moneybookers") {
						$notifyUrl=$path."moneybookers.process.php?table=2";
						moneybookersForm($auctionId,$paymentAmount,2);
					}
					if ($selectedPGs['name']=="Test Mode") {
						$notifyUrl=$path."paymentsimulator.php?table=2";
						testmodeForm($auctionId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Hansapank") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					HansapankForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="SEB") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					authorizeNetForm($userid,$paymentAmount,1);
				}
				}
			}		
		} else if ($account_mode_local==2) {
			// account mode thing
			$userId = getSqlField("SELECT * FROM probid_auctions WHERE id='".$auctionId."'","ownerid");
			$balance = 0;
			$balance+=$amount;
			
			$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			balance=balance+".$balance." WHERE id='".$userId."'");
			$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
			active = '1',payment_status='confirmed' WHERE id='".$auctionId."'");
			## since site is in account mode, add counter
			$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$auctionId."'");
			if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
				addcatcount ($auctCat['category'],$auctionId);
				addcatcount ($auctCat['addlcategory'],$auctionId);
			}
			$t = displayAmount($amount,$setts['currency'], TRUE);
			
			if ($displayMsgs) echo "<p align=center class=contentfont>".$lang['aucsubmitted1']." #".$auctionId." ".$lang['auctionactivated2']." ".$t." ".$lang['actfeeof1']."<br><br></p>";
		}	
	}
	return $amount;
}

#### the function for the end of auction fee
function endauctionFee($finalBid,$currency,$auctionId, $payerId = 0) {
	global $path, $setts, $lang, $fee;

	$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auctionId."'","category");

	$category_id = getMainCat($itemCategory);

	$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
	$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

	## v5.22 addon -> if the currency is different from the default one, then apply the converter.
	$exchange_rate = 1;
	$auction_currency = getSqlField("SELECT currency FROM probid_auctions WHERE id='".$auctionId."'","currency");
	
	if (trim($setts['currency'])!=trim($auction_currency)) 
		$exchange_rate = 1/getSqlField("SELECT converter FROM probid_currencies WHERE symbol = '".trim($auction_currency)."'","converter");		
	if($exchange_rate<=0) $exchange_rate = 1;
	
	$userid = (!$payerId) ? getSqlField("SELECT * FROM probid_auctions WHERE id='".$auctionId."'","ownerid") : $payerId;

	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userid."'","payment_mode");
	if ($setts['account_mode_personal']==1) {
    	$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	#### check if the user is a preferred seller
	$prefSeller = "N";
	if ($setts['pref_sellers']=="Y") {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$userid."'","preferred_seller");
	}
	### we will check for what type of gateway the client has chosen

	$amount=0;
	$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$userid."'","balance");
	$newBalance = $currentBalance;
	if ($finalBid<0) $amount=$fee['val_swap_fee'];
	
	
	if ($fee['is_endauction_fee']=="Y"&&$finalBid>0) {
		$endauctionFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE 
		fee_from<=".$finalBid." AND fee_to>".$finalBid." AND fee_type='endauction' AND category_id='".$category_id."'");
		
		if ($endauctionFee['calc_type']=="percent") {
			$amount+=($endauctionFee['fee_amount']/100)*$finalBid*$exchange_rate;
			$invoiceAmount=($endauctionFee['fee_amount']/100)*$finalBid*$exchange_rate;
		} else {
			$amount+=$endauctionFee['fee_amount'];
			$invoiceAmount=$endauctionFee['fee_amount'];
		}
		
		### if there is a dutch auction, calculate the number of sold items times end of auction amount
		
		$auctionType = getSqlField("SELECT auctiontype FROM probid_auctions WHERE id='".$auctionId."'","auctiontype");
		if ($auctionType=="dutch") {
			$totalQuant = getSqlField("SELECT sum(quant_offered) AS total_quant FROM 
			probid_winners WHERE auctionid='".$auctionId."'","total_quant");
			$add_desc = " x ".$totalQuant;
		} else { 
			$totalQuant = 1;
			$add_desc = "";
		}
		$amount = $amount * $totalQuant;
		$invoiceAmount = $invoiceAmount * $totalQuant;
		
		if ($account_mode_local==2) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[end_auc_fee].$add_desc)."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}
	
	if ($finalFid<0&&$fee['val_swap_fee']>0) {
		if ($account_mode_local==2) {
			$newBalance += $amount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[swap_fee])."','".$amount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	if ($amount==0) $setts['payment_gateway']="none";

	## apply exchange rate 
	$amount = $amount * $exchange_rate;
	
	if ($account_mode_local==1) { 

		$paymentAmount=number_format($amount,2,'.','');
		### new function that displays the payment message and amount
		displayPaymentMessage($paymentAmount);
	
		### new procedure to list all active payment gateways
		if ($setts['payment_gateway']=="none") {
			echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
			$activateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			active = '1',payment_status='confirmed' WHERE id='".$userid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
			while ($selectedPGs = mysqli_fetch_array($getPGs)) {
				if ($selectedPGs['name']=="Paypal") {
					$pmDesc = $setts['sitename'].' - '.$lang[end_auc_fee];			
					$notifyUrl=$path."paymentprocess.php?table=3";
					paypalForm($auctionId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,3,$pmDesc);
				}	
				if ($selectedPGs['name']=="Nochex") {
					$notifyUrl=$path."nochexprocess.php?table=3";
					nochexForm($auctionId,$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,3);
				}
				if ($selectedPGs['name']=="2Checkout") {
					$notifyUrl=$path."checkoutprocess.php?table=3";
					checkoutForm($auctionId,$setts['checkoutid'],$paymentAmount,3);
				}
				if ($selectedPGs['name']=="Worldpay") {
					$notifyUrl=$path."worldpayprocess.php?table=3";
					worldpayForm($auctionId,$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,3);
				}
				if ($selectedPGs['name']=="Ikobo") {
				 	$notifyUrl=$path."ikoboprocess.php?table=3";
					ikoboForm($auctionId,$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,3);
				}
				if ($selectedPGs['name']=="Protx") {
					$notifyUrl=$path."protxprocess.php?table=3";
					protxForm($auctionId,$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,3);
				}
				if ($selectedPGs['name']=="Authorize.Net") {
					$notifyUrl=$path."authorize.net.process.php?table=3";
					authorizeNetForm($auctionId,$paymentAmount,3);
				}
				if ($selectedPGs['name']=="Moneybookers") {
					$notifyUrl=$path."moneybookers.process.php?table=3";
					moneybookersForm($auctionId,$paymentAmount,3);
				}
				if ($selectedPGs['name']=="Test Mode") {
					$notifyUrl=$path."paymentsimulator.php?table=3";
					testmodeForm($auctionId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,3);
				}
				if ($selectedPGs['name']=="Hansapank") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					HansapankForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="SEB") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					authorizeNetForm($userid,$paymentAmount,1);
				}
			}
		}		
	} else if ($account_mode_local==2) {
		// account mode thing
		$userId = getSqlField("SELECT * FROM probid_auctions WHERE id='".$auctionId."'","ownerid");
		$balance = 0;
		$balance+=$amount;
		$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		balance=balance+".$balance." WHERE id='".$userId."'");
		$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
		active = '1',payment_status='confirmed' WHERE id='".$auctionId."'");
		$updwinner = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET 
		active = '1',payment_status='confirmed',amountpaid='".$amount."' WHERE auctionid='".$auctionId."'");

		$t = $amount;
		if($t == '0,00' || $t == '0.00' || empty($t)){
    		$t = 'free';
		}
		
		echo "<p align=center class=contentfont>".$lang[actfeeof]." ".$setts['currency']." ".$t." ".$lang['actfeeof1']."<br><br></p>";
	}	
	/*
	$actItemB = getSqlField("SELECT active FROM probid_auctions WHERE id='".$auctionid."'","active");
	if ($actItemA==0&&$actItemB==1) counterAddItem($auctionid);
	if ($actItemA==1&&$actItemB==0) counterRemItem($auctionid);	
	*/
	return $amount;
}

### this is the function for the wanted ad fee
function wantedAdFee($wantedAdId) {
	global $path, $setts, $lang, $fee;

	$userid = getSqlField("SELECT * FROM probid_wanted_ads WHERE id='".$wantedAdId."'","ownerid");
	if (!$wantedAdId) $userid = $_SESSION['memberid'];
	#### check if the user is a preferred seller
	$prefSeller = "N";
	if ($setts['pref_sellers']=="Y") {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$userid."'","preferred_seller");
	}
	
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userid."'","payment_mode");
	if ($setts['account_mode_personal']==1) {
		$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
  
	#### calculate the amount that has to be paid for this auction's setup
	$amount=0;
	$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$userid."'","balance");
	$newBalance = $currentBalance;

	if ($fee['wantedad_fee']>0) {
		$amount+=applyVat($fee['wantedad_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['wantedad_fee'],$userid);

		if ($account_mode_local==2) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$wantedAdId."','".remSpecialChars($lang[wantedad_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	if ($amount==0) $setts['payment_gateway']="none";

	### setts for the preferred sellers reduction
	$amount = calcReduction ($amount, $prefSeller);

	if ($account_mode_local==1) {
		$paymentAmount=number_format($amount,2,'.','');
		### new function that displays the payment message and amount
		displayPaymentMessage($paymentAmount);

		### new procedure to list all active payment gateways
		if ($setts['payment_gateway']=="none") {
			echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
			$activateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			active = '1',payment_status='confirmed' WHERE id='".$userid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
			while ($selectedPGs = mysqli_fetch_array($getPGs)) {
				if ($selectedPGs['name']=="Paypal") {
					$pmDesc = $setts['sitename'].' - '.$lang[wantedad_fee];
					$notifyUrl=$path."paymentprocess.php?table=7";
					paypalForm($wantedAdId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,7,$pmDesc);
				}	
				if ($selectedPGs['name']=="Nochex") {
					$notifyUrl=$path."nochexprocess.php?table=7";
					nochexForm($wantedAdId,$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,7);
				}
				if ($selectedPGs['name']=="2Checkout") {
					$notifyUrl=$path."checkoutprocess.php?table=7";
					checkoutForm($wantedAdId,$setts['checkoutid'],$paymentAmount,7);
				}
				if ($selectedPGs['name']=="Worldpay") {
					$notifyUrl=$path."worldpayprocess.php?table=7";
					worldpayForm($wantedAdId,$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,7);
				}
				if ($selectedPGs['name']=="Ikobo") {
				 	$notifyUrl=$path."ikoboprocess.php?table=7";
					ikoboForm($wantedAdId,$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,7);
				}
				if ($selectedPGs['name']=="Protx") {
					$notifyUrl=$path."protxprocess.php?table=7";
					protxForm($wantedAdId,$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,7);
				}
				if ($selectedPGs['name']=="Authorize.Net") {
					$notifyUrl=$path."authorize.net.process.php?table=7";
					authorizeNetForm($wantedAdId,$paymentAmount,7);
				}
				if ($selectedPGs['name']=="Moneybookers") {
					$notifyUrl=$path."moneybookers.process.php?table=7";
					moneybookersForm($wantedAdId,$paymentAmount,7);
				}
				if ($selectedPGs['name']=="Test Mode") {
					$notifyUrl=$path."paymentsimulator.php?table=7";
					testmodeForm($wantedAdId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,7);
				}
				if ($selectedPGs['name']=="Hansapank") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					HansapankForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="SEB") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					authorizeNetForm($userid,$paymentAmount,1);
				}
			}
		}		
	} else if ($account_mode_local==2) {
		// account mode thing
		$userId = getSqlField("SELECT * FROM probid_wanted_ads WHERE id='".$wantedAdId."'","ownerid");
		$balance = 0;
		$balance+=$amount;

		$wantedCat = getSqlRow("SELECT category, addlcategory FROM probid_wanted_ads WHERE id='".$wantedAdId."'");
		addwantedcount ($wantedCat['category']);
		addwantedcount ($wantedCat['addlcategory']);
			
		$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		balance=balance+".$balance." WHERE id='".$userId."'");
		$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
		active = '1',payment_status='confirmed' WHERE id='".$wantedAdId."'");
		echo "<p align=center class=contentfont>".$lang[wantedadactivated]." ".displayAmount($amount,$currency, TRUE)." ".$lang[actfeeof1]."<br><br></p>";
	}	
	return $amount;
}

### this is the stores setup fee
function storeSetupFee($userId,$storeAccountType,$noOutput=FALSE) {
	global $path, $setts, $lang, $fee;

	#### check if the user is a preferred seller
	$prefSeller = "N";
	#### the preferred sellers feature doesnt apply here.
	/*
	if ($setts['pref_sellers']=="Y") {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$userId."'","preferred_seller");
	}
	*/
	
	$currentTime = time();

	$store = storeAccountType($storeAccountType);

	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userId."'","payment_mode");
	if ($setts['account_mode_personal']==1) {
		$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	#### calculate the amount that has to be paid for this auction's setup
	$amount=0;
	$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$userId."'","balance");
	$newBalance = $currentBalance;
	if ($fee['is_store_fee']=="Y") {
		$amount+=applyVat($store['fee_amount'],$userId) ;
		$invoiceAmount=applyVat($store['fee_amount'],$userId);

		if ($account_mode_local==2) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userId."','0','".remSpecialChars($lang[store_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	if ($amount==0) $setts['payment_gateway']="none";

	### setts for the preferred sellers reduction
	$amount = calcReduction ($amount, $prefSeller);

	if ($account_mode_local==1) {
		$paymentAmount=number_format($amount,2,'.','');
		### new function that displays the payment message and amount
		displayPaymentMessage($paymentAmount);
	
		### new procedure to list all active payment gateways
		if ($setts['payment_gateway']=="none") {
			echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
			$activateStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			store_active = '1',store_lastpayment='".$currentTime."' WHERE id='".$userId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			$getPGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways WHERE value='checked'");
			while ($selectedPGs = mysqli_fetch_array($getPGs)) {
				if ($selectedPGs['name']=="Paypal") {
					$pmDesc = $setts['sitename'].' - '.$lang[store_fee];
					$notifyUrl=$path."paymentprocess.php?table=8";
					paypalForm($userId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,8,$pmDesc);
				}	
				if ($selectedPGs['name']=="Nochex") {
					$notifyUrl=$path."nochexprocess.php?table=8";
					nochexForm($userId,$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,8);
				}
				if ($selectedPGs['name']=="2Checkout") {
					$notifyUrl=$path."checkoutprocess.php?table=8";
					checkoutForm($userId,$setts['checkoutid'],$paymentAmount,8);
				}
				if ($selectedPGs['name']=="Worldpay") {
					$notifyUrl=$path."worldpayprocess.php?table=8";
					worldpayForm($userId,$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,8);
				}
				if ($selectedPGs['name']=="Ikobo") {
				 	$notifyUrl=$path."ikoboprocess.php?table=8";
					ikoboForm($userId,$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,8);
				}
				if ($selectedPGs['name']=="Protx") {
					$notifyUrl=$path."protxprocess.php?table=8";
					protxForm($userId,$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,8);
				}
				if ($selectedPGs['name']=="Authorize.Net") {
					$notifyUrl=$path."authorize.net.process.php?table=8";
					authorizeNetForm($userId,$paymentAmount,8);
				}
				if ($selectedPGs['name']=="Moneybookers") {
					$notifyUrl=$path."moneybookers.process.php?table=8";
					moneybookersForm($userId,$paymentAmount,8);
				}
				if ($selectedPGs['name']=="Test Mode") {
					$notifyUrl=$path."paymentsimulator.php?table=8";
					testmodeForm($userId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,8);
				}
				if ($selectedPGs['name']=="Hansapank") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					HansapankForm($userid,$paymentAmount,1);
				}
				if ($selectedPGs['name']=="SEB") {
					$notifyUrl=$path."authorize.net.process.php?table=1";
					authorizeNetForm($userid,$paymentAmount,1);
				}
			}
		}		
	} else if ($account_mode_local==2) {
		// account mode thing
		$balance = 0;
		$balance+=$amount;
		
		$currentTime = time();
		
		$store_nextpayment = 0;
		if ($store['store_recurring']>0) $store_nextpayment = time() + ($store['store_recurring']*24*3600);
			
		$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		balance=balance+".$balance.", store_active='1', store_lastpayment='".$currentTime."', 
		store_account_type='".$storeAccountType."', store_nextpayment='".$store_nextpayment."' WHERE id='".$userId."'");
		if (!$noOutput) echo "<p align=center class=contentfont>".$lang[storeactivated]." ".displayAmount($amount,$currency, TRUE)." ".$lang[actfeeof1]."<br><br></p>";
	}	
	return $amount;
}
?>
