﻿<?php

//include('restrict.php');

// if($_SERVER['REMOTE_ADDR'] != '193.40.244.196' && $_SERVER['REMOTE_ADDR'] != '84.52.42.51' && $_SERVER['REMOTE_ADDR'] != '82.131.81.214' && $_SERVER['REMOTE_ADDR'] != '84.52.31.140' && $_SERVER['REMOTE_ADDR'] != '85.253.53.242')
// {
	// echo "Only some IPs are allowed.";
	// die();
// }

include('db_login.php'); //database login and password										
																							
$dbh = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname))							
	or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));															
																			
	
//if (file_exists("config/config.php")) $fileExtension = "";		
//else $fileExtension = "../";													
$fileExtension =  "../";
//debug:
//if(substr($_SERVER['SERVER_NAME'], 0, 3)=='loc') $fileExtension = $_SERVER['DOCUMENT_ROOT']."/SBE/";
if(substr($_SERVER['SERVER_NAME'], 0, 3)=='loc') $fileExtension = $_SERVER['DOCUMENT_ROOT']."/";
else $fileExtension = $_SERVER['DOCUMENT_ROOT']."/";
																						
/*		
mysql_query('set character utf8');         
mysql_query('set names utf8');											
		*/																	

	$domain_parts=explode('.', $_SERVER['SERVER_NAME'], 2);
	// echo $domain_parts[0];
	switch($domain_parts[0])
	{
		case 'www':
		case 'sbe':
			$domain_lang = 'est';
			$domain_top = 'sbe.ee';
			break;
		case 'dev':
		case 'test':
			$domain_lang = 'est';
			$domain_top = $_SERVER['SERVER_NAME'];
			break;
		default:
			$domain_lang = $domain_parts[0];
			$domain_top = $domain_parts[1];
	}

   $sLang = array("rus","est","lat","eng"); // List of languages
   $varLang["rus"] = "b";
   $varLang["eng"] = "a";
   $varLang["est"] = "english";
   $varLang["lat"] = "latvian";
   $varHost["rus"] = "http://rus.$domain_top/";
   $varHost["eng"] = "http://eng.$domain_top/";
   $varHost["est"] = "http://www.$domain_top/";
   $varHost["lat"] = "http://lat.$domain_top/";
   $varHost["b"] = "http://rus.$domain_top/";
   $varHost["a"] = "http://eng.$domain_top/";
   $varHost["english"] = "http://www.$domain_top/";
   $varHost["latvian"] = "http://lat.$domain_top/";
   
   if (!isset($_SESSION['sess_lang'])) 
   {
       
     if (in_array($domain_lang,$sLang))
     {
      include_once ("$fileExtension/config/lang/".$varLang[$domain_lang]."/site.lang");
      include_once ("$fileExtension/config/lang/".$varLang[$domain_lang]."/category.lang");
      @$_SESSION['sess_lang']="".$varLang[$domain_lang]."";
      @define("VAR_HOST",$varHost[$domain_lang]);
        
     } else {
       @include_once ("$fileExtension/config/lang/".$varLang["est"]."/site.lang");
       @include_once ("$fileExtension/config/lang/".$varLang["est"]."/category.lang");
       @$_SESSION['sess_lang']="".$varLang["est"]."";
       @define("VAR_HOST",$varHost["est"]);
     
     }		
   } else {
       // $x= "$fileExtension/config/lang/".$_SESSION['sess_lang']."/site.lang";
       // echo $x;
      include_once ("$fileExtension/config/lang/".$_SESSION['sess_lang']."/site.lang");
      include_once ("$fileExtension/config/lang/".$_SESSION['sess_lang']."/category.lang");
      @define("VAR_HOST","http://".$_SERVER["SERVER_NAME"]."/");   
     // echo VAR_HOST;   
   }
   
   if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) {
   
      $arrAllLang = array("lat","rus","est","eng");
      $l["rus"] = "b";
      $l["est"] = "english";
      $l["eng"] = "a";
      $l["lat"] = "latvian";
      if (in_array($_POST["lang"],$arrAllLang))
      {
         $_SESSION['sess_lang'] = $l[$_POST["lang"]];
          $_SESSION['in_lang']  = 1;
      @include_once ("$fileExtension/config/lang/".$_SESSION['sess_lang']."/site.lang");
      @include_once ("$fileExtension/config/lang/".$_SESSION['sess_lang']."/category.lang");
      @define("VAR_HOST","http://".$_SERVER["SERVER_NAME"]."/");   
      }
   }
																				
function cleanData() { 																
   foreach($_GET as $k => $v) { 												
   	if (is_array($_GET[$k])) { 												
			for ($i=0; $i<count($_GET[$k]); $i++) { 						
				if (!get_magic_quotes_gpc()) $_GET[$k][$i] = addslashes(htmlspecialchars($_GET[$k][$i]));	
				else $_GET[$k][$i] = htmlspecialchars($_GET[$k][$i]);	
			} 																				
		} else { 																		
			if (!get_magic_quotes_gpc()) $_GET[$k] = addslashes(htmlspecialchars($v)); 
			else $_GET[$k] = htmlspecialchars($v); 						
		} 																					
	} 																						
   foreach($_POST as $k => $v) { 											
   	if (is_array($_POST[$k])) { 											
			for ($i=0; $i<count($_POST[$k]); $i++) { 					
				if (!get_magic_quotes_gpc()) $_POST[$k][$i] = addslashes(htmlspecialchars($_POST[$k][$i])); 
				else $_POST[$k][$i] = htmlspecialchars($_POST[$k][$i]); 
			} 																				
		} else { 																		
			if (!get_magic_quotes_gpc()) $_POST[$k] = addslashes(htmlspecialchars($v)); 
			else $_POST[$k] = htmlspecialchars($v); 						
		} 																					
	} 																						
   foreach($_REQUEST as $k => $v) { 										
   	if (is_array($_REQUEST[$k])) { 										
			for ($i=0; $i<count($_REQUEST[$k]); $i++) { 				
				if (!get_magic_quotes_gpc()) $_REQUEST[$k][$i] = addslashes(htmlspecialchars($_REQUEST[$k][$i])); 
				else $_REQUEST[$k][$i] = htmlspecialchars($_REQUEST[$k][$i]); 
			}																				
		} else { 																		
			if (!get_magic_quotes_gpc()) $_REQUEST[$k] = addslashes(htmlspecialchars($v)); 
			else $_REQUEST[$k] = htmlspecialchars($v); 					
		} 																					
	} 																						
}																							
 																							
#### turn off magic_quotes_gpc 													
##ini_set('magic_quotes_gpc', 'Off'); 											
 																							
cleanData(); 				

  $arrLang = array(
  "0"=>array(
    "name"=>"Eesti",
    "value"=>"english"),
  "1"=>array(
    "name"=>"English",
    "value"=>"a"),  
  "2"=>array(
    "name"=>"Русский",
    "value"=>"b"),
  "3"=>array(
    "name"=>"Latviešu",
    "value"=>"latvian")
  );

  $arrDomLang = array(
  "0"=>array(
    "name"=>"Eesti",
    "value"=>"est"),
  "1"=>array(
    "name"=>"English",
    "value"=>"eng"),  
  "2"=>array(
    "name"=>"Русский",
    "value"=>"rus"),
  "3"=>array(
    "name"=>"Latviešu",
    "value"=>"lat")
  );  
  

function myLangChecks($checked,$type="checkbox") {

  global $arrLang;

  foreach ($arrLang as $row)
  {
    $i++;
    if (is_array($checked))
    {
      if (in_array($row["value"],$checked))
      {
          $mark = "checked";
      } else {
          $mark = "";
      } 
    }
    
  
    $check .= "<input ".$mark." type='".$type."' name='lang[]' value='".$row["value"]."'>".$row["name"];
  }
  return $check;
}


function myCurrency($selected) {
//dp("myCurrency<");
  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT symbol,id,caption FROM probid_currencies ORDER BY id ASC");
//dp(">");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $arrCur[] = $row; 
  }

  $select = "<select name='mycur'>";
  
  foreach ($arrCur as $row)
  {
    $select .= "<option ".($selected==$row["id"]?'selected':'')." value='".$row["id"]."'>".$row["symbol"]." - ".$row["caption"];
  }
  
  $select .= "</select>";
  
  return $select;


}


function myLang($selected) {

  global $arrLang;
  
  $select = "<select name='mylang'>";
  
  foreach ($arrLang as $row)
  {
    $select .= "<option ".($selected==$row["value"]?'selected':'')." value='".$row["value"]."'>".$row["name"];
  }
  
  $select .= "</select>";
  
  return $select;
}															
 																							
$doc_directory=$_SERVER['DOCUMENT_ROOT']; 	
//debug:
if(substr($_SERVER['SERVER_NAME'], 0, 3)=='loc') $doc_directory=$_SERVER['DOCUMENT_ROOT']."/SBE/";
else $doc_directory=$_SERVER['DOCUMENT_ROOT']."/";

 																							
define('INCLUDED', 1);																																						
																							
include_once ($fileExtension."config/settings.php");					
@include_once ($fileExtension."config/functions.php");					
include_once ($fileExtension."config/datecalc.php");					
include_once ($fileExtension."config/fees.php");						
include_once ($fileExtension."config/version.php");			

if (intval($_SESSION["memberid"])>0)
{
  if ($_SESSION['in_lang']) 
  {
      @include_once ("$fileExtension/config/lang/".$_SESSION['sess_lang']."/site.lang");
      @include_once ("$fileExtension/config/lang/".$_SESSION['sess_lang']."/category.lang");     
  } else {
      # GET user language interface
      $prefs = getSqlRow("SELECT mylang FROM probid_users  WHERE id=".$_SESSION["memberid"]."");
      $_SESSION['sess_lang'] = $prefs["mylang"];
      $_SESSION['mylang'] = $prefs["mylang"];
      @ include_once ("$fileExtension/config/lang/".$prefs["mylang"]."/site.lang");
      @ include_once ("$fileExtension/config/lang/".$prefs["mylang"]."/category.lang");
      $_SESSION['sess_lang'] = $prefs["mylang"];
      define("VAR_HOST",$varHost["est"]);
  }
} 
			
		
																							
if ($setts['is_ssl']==1) $path=$setts['ssl_address'];					
else $path=$setts['siteurl'];													
?>
