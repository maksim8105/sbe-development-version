<?
$currentVersion = "5.25";
#####################################################################################################
### v5.12 Changelog -> mar. 16, 2005
###
### [ FEATURES ]  
### - added counters for wanted ads
### - the activation fee can be paid on login too, not only on registration
### - added a new feature to enable/disable bid retraction
### - added new items flagging feature. Sellers can flag the items as paid/unpaid, and they can
###   also flag their delivery status.
### [ BUG FIXES ]  
### - fixed auto relisting issue
### - fixed auction start issue in cron
### - fixed fee calculation issues that occurred when using Swap or Buy Now
### - counter is now updated when an auction is ended with the Swap option
### - fixed second category selection issue that appeared when relisting items
### - category images can now be deleted from the admin area
### - fixed the send user email and send newsletter pages from the admin area
### - fixed invalid display of fees when the user was preferred seller with 100% reduction
### - fixed invalid fees calculation that appeared in some cases on live mode
### - fixed issue where the registration page was still available if the user was logged in
#####################################################################################################
### v5.11 Changelog -> mar. 09, 2005
###
### [ FEATURES ]  
### - if registration approval is enabled, and the user hasnt activated his account initially, 
###   he will have the option to come back and activate, by logging in with his chosen username 
###   and password. He will login and the payment activation page will appear so he can activate 
###   his account
### - added a delete option for bids. Outbid bids can now be retracted, but they can only be removed 
###   from the "Items I'm bidding on" list by clicking on the delete button
### - added counter to wanted ads
### [ BUG FIXES ]  
### - fixed stores ordering
### - fixed stores activation issue, and also the about me page activation issue
### - fixed the issues that appeared in the bulk upload page 
### - custom start times now work with time offset settings
### - resynced the categories counter. Please run initialize.counters.php file so both the auctions 
###   and wanted ads counters are refreshed.
### - fixed category display related issues (not being displayed alphabetically in the edit auction page)
### - the pictures in the view all pages are showing now
### - auctions cannot be listed twice in the same category anymore
### - a seller will have to pay the end of auction fee for all his sold items on a dutch auction, 
###   not just once like it worked until now
#####################################################################################################
### v5.10 Changelog -> feb. 25, 2005
###
### [ FEATURES ]  
### - custom listing fields
### - HTML editor for all the admin area content pages
### - HTML editor for the admin send newsletter and admin send user email pages
### - user Post Code and Country are automatically filled when creating a listing
### - authorize.NET payment gateway
### - admin can enable registration approval
### - the signup fee is now used as account activation fee 
### - option to block admin defined email domains
### - new stores page where all existing stores are listed
### - new store fee (one time or recurring)
### - new wanted ads feature 
### - new wanted ad setup fee
### - new admin section to edit currencies
### - added shipping details description box
### - added TAX / VAT feature that applies to all site fees. Users can be exempted from paying VAT if enabled by the admin
### - all admin passwords are now encrypted
### - new feedback system with positive/neutral/negative ratings
### - individual titles and meta descriptions for the categories and auction details pages
### - new option to add meta tags and custom images to main categories
### - new feature so users can edit the auction's description after bids have been made
### - SSL is now applied only to the registration and login pages, this way the "display secure and non-secure items" message is not displayed anymore
### - improved the item creation process
### - added a new odering/display feature in the admin area/users management page  
### - added the accounting feature
### - added multi-language categories
### - added new currency converter
### [ BUG FIXES ]  
### - fixed the category counters. Please run the initialize.counters.php file first to resync your counters 
### - added a new bulk file checker, this way only properly formatted bulk files are allowed to be uploaded
### - Javascript code instertion is not allowed in the item description box
### - ended auctions arent available when searching anymore
### - fixed issue where if the seller or buyer would delete the contact details, it would be phisically deleted, and the other party wasnt able to view the details anymore
### - fixed the dutch bidding process
### - fixed the bid retraction process
### - fixed auction ordering in the browse pages
### - auctions and bids cannot be withdrawed if the auction has less than 12 hours to run
### - fixed issue where a user was able to bid on a deleted auction, if that auction was still open
### - fixed bidding and buy now issues
 
#####################################################################################################
### v5.012 Changelog -> jan. 10, 2005
###
### - bidding security update 
### - bulk uploader security update
#####################################################################################################
### v5.011 Changelog -> dec. 22, 2004
###
### - fixed bidding blank page issue which appeared on some sites
### - fixed images doubling up issue when selling similar items that still appeared on some sites
### - fixed terms and conditions submission from the admin area
#####################################################################################################
### v5.01 Changelog -> dec. 15, 2004
###
### - fixed proxy bidding bid history display
### - fixed 100% fee reduction problem, where the seller was asked to pay an amount of "No Bids",  
###   and couldnt proceed in case the site was in live mode
### - new feature: an email is sent if an user goes over the maximum credit limit, available for 
###   account mode only
### - fixed FAQ section submission issue.  
### - new features: the FAQ and Help pages now support ordering for their topics  
### - fixed the display of paging at the bottom of some pages from the front end  
### - fixed dutch auctions bidding  
### - fixed the bulk uploader auction activation issue, where auctions were set from the bulk to 
###   the selling section as suspended if an auction setup fee was set  
### - new feature: when listing an auction, on the page where the fees that will be applied to 
###   the auctions are displayed, a total of the setup fee that will be paid is also shown.
### - if a language is removed, all the pages that are saved in the database are also deleted.  
### - fixed issue when on the sell similar feature, if the seller uploaded other images, the   
###   additional images were doubled up
#####################################################################################################
### v5.00 Changelog -> dec. 08, 2004
###
### - completely new site skin
### - improved relisting and deletion of closed auctions.
### - admin can now relist closed auctions
### - seller and admin can now retract user bids
### - completely customizable anti-sniping feature, the admin can now enable/disable it, and set
###   the amount of minutes the auction is prolonged with 
### - new private site feature. If enabled, only users allowed by the admin have selling capabilities
### - new multiple payment gateways option available. The admin can activate more than one 
###   payment gateway for the user to pay fees through
### - new preferred sellers feature: Add an admin option to allow them to pick users who can list for 
###   free or set a reduction rate off fees for them
### - new admin BCC features. If enabled, all emails sent through the site are also forwarded to the
###   admin
### - fixed problem where the proxy bidder was left as the high bidder if a bid the same as the proxy
###   bid was placed
### - if there are more high bidders, they are viewed on a popup window (dutch auction feature)
### - sellers cannot edit their items if the items have less than 12 hours until they end
### - news items can now be edited in the admin area
### - the insurance box accepts only amounts now
### - fixed the multiple bidding issue
### - new feature, auctions can now be searched by their number in the admin area
### - admin can view what auctions a user has bid on
### - fixed the bidding retraction/removal problems.
#####################################################################################################
### v4.07 Changelog -> nov. 09, 2004
###
### - new payment gateway added: Pro TX
### - fixed name problem when uploading payment methods images - appeared when using a different charset
###   than the european one
### - implemented mail sending through the UNIX Sendmail program, as an alternative for the PHP
###   mail() function
### - fixed parse errors that appeared in the ikoboprocess.php and nochexprocess.php files
### - fixed the displaying of feedbacks for users on the bid history page
### - fixed uploading of the site logo, this didn't work if the register globals was turned to off
### - revamped category counters functions
#####################################################################################################
### v4.06 Changelog -> oct. 27, 2004
###
### - fixed the category counter because it caused lagging when pages were loaded
### - fixed issue where in case of item swapping, the buyer and seller details were sent twice
### - fixed issue where the auction not sold emails were bounced back where there were no bids
### - the final bid amount now displays correctly on the winner and seller contact info pages
### - words longer than 20 characters are limited for the item title. This value can be changed though.
### - fixed bug where users weren't able to set buy now prices when creating dutch auctions
### - added new buy it now auction fee, when a user creates an auction with buy now option. This
###   works exactly like the reserve price fee.
### - swap auctions can now be ended even if there is no swap fee set. 
### - category listings show instead of the category browse page in case there are no subcategories,
###   even if the user chooses the "Browse" list menu from the site's header
### - the buy it now price can now be properly edited, when editing an auction from the admin area
### - fixed bug where when editing similar, the images weren't copied properly
### - fixed bug where the first additional picture was not copied when creating a new auction
### - fixed bug where auctions displayed twice in the category listings page, in case they appeared
###   in two categories and both categories were selected
### - added new feature to edit shipping options
### - fixed banner filters problem, banners now display properly if they are filtered by auction keywords
###   or categories
#####################################################################################################
### v4.05 Changelog -> oct. 19, 2004
###
### - relisting an auction now creates a completely new auction with the same properties as the 
###   original one, it doesnt only reopen the old auction
### - shipping conditions are now saved in the database when a new item is posted for sale
### - fixed bug where system emails couldnt be modified from the admin area
### - fixed bug where an automatic reserve price of 0 was set when relisting or selling a similar item
### - fixed bug where users had to reload pictures when using the sell similar feature
### - auctions cannot be deleted from the members area by link manipulation (previous security concern)
### - only valid float numbers are now accepted in the shipping costs field when posting an item for sale
### - fixed problem where on some cases the send auction to a friend email didnt work correctly
### - the 'go to larger picture' link now works on all browsers
#####################################################################################################
### v4.04 Changelog -> oct. 15, 2004
###
### - fixed item swapping bug, where auctions that were ended with the swap item option didnt close
### - swap items fee is now shown when creating an auction with the accept swap feature set to yes
### - fixed auction watch feature bug, users now receive emails when auctions that their keywords are
###   submitted
### - fixed second category message typo, this message didnt appear on the selling process, only the
###   fee appeared
### - buy it now price cannot be lower than the reserve price anymore
### - fixed item description and bid on this item links, that didnt work on some browsers
### - added a new feature in the admin area to spot and try to eliminate the categories that
###   cause conflicts, because of certain name similarities. You can access this new feature and see
###   if your site is ok from this point of view by selecting the edit categories page, and then clicking
###   on the view conflicting categories link. Conflicting categories will cause small tweaks in the 
###   way that auctions are counted by the item counter, and in the way auctions are displayed in the
###   category listings page. It is recommended that you do not have conflicting categories, but having
###   them wont cause any big problems
###   fixed edit auction page from the admin area, it didnt select the category correctly
### - we have turned of the site statistics because they do high database load which can cause 
###   the server to slow down. We recommend you use awstats or any other stats tool that is provided
###   with your control panel instead. Also we recommend you empty the probid_stats table, you can do
###   that from the Stats page in the admin area.
### - fixed bug where the minimum bid wasnt displayed correctly in the bid box for dutch auctions
### - fixed bug where links werent parsed correctly on the description fields of the items
### - categories on the categories browse and listings pages are now ordered following the custom
###   ordering set up in the admin area
#####################################################################################################
### v4.03 Changelog -> oct. 13, 2004
###
### - new sortable column in the users management table in the admin area, now users can be sorted by
###   amounts owed 
### - admin users can now be properly deleted, only one admin user cannot be deleted
### - the site logo and the payment options logos can now be properly uploaded
### - fixed category deletion bug in the edit F-A-Q database bug
### - new feature for the lost password page, making it now more secure. Users now have to submit
###   both their username and email to be able to reset their password
### - fixed view newsletter subscribers page
### - fixed bug where auctions that were relisted after that item was already sold before, it displayed
###   the high bidder improperly
### - added new feature, now users can relist a similar auction instead of relisting the same auction
### - fixed bug where none of the emails didnt get sent by the cron job, when running it from the 
###   cron tab in the site's control panel
### - fixed bug where users couldn't register if they entered a birth year under 1970
### - fixed bug where members could list dutch auctions with a quantity of 1. Now at least
###   two items must be listed on a dutch auction
#####################################################################################################
### v4.02 Changelog -> oct. 8, 2004
###
### - fixed "item swapped" message that appeared on some sites
### - fixed swap option problem, where sellers could choose to accept swaps when listing even 
###   if the swap option was disabled from the admin area
### - fixed payment problem, where user was prompted for a payment when selecting to list
###   an item in a second category even if there was no fee for listing in a second category
### - fixed dutch auction listing problem
### - fixed the getItems problem, where on some categories it didnt count the auctions correctly
### - image oversizing problem fixed
### - fixed advanced search paging function, if a user would select a certain country it didnt create
###   the paging correctly
### - fixed the auction browsing pages, where only one page was possible to view
### - fixed about, terms, privacy policy and enable login box pages in the admin area
### - fixed text display problem in the popups from the edit auction and bulk pages from the members area
### - fixed setup and end of auction fees insertion problems that appeared in the admin area
### - fixed logging out from the admin area problem
#####################################################################################################
### v4.01 Changelog -> oct. 7, 2004
###
### - admin users can now be added, edited and deleted
### - logout from the admin area is now possible
### - when going through the installation process, the new admin user is successfully 
###   created, any old ones are deleted though
### - when logging to send an auction to a friend, the user gets redirected correctly
### - the subject, date and message formatting are now parsed correctly in the emails 
###   that are sent by the software
### - winner contact information is now displayed correctly
### - users and auctions can now be successfully deleted
### - swapping feature now works correctly
#####################################################################################################
### v4.00 Changelog -> oct. 6, 2004
###
### - complete code revamp to be fully compatible with PHP Super Globals, so now the software
###   works with the 'register_globals = off' php setting, offering improved security
### - overall tidying up of the code
### - some files were compacted, resulting a single file from more that previously existed
### - new picture resizing function, applies to all pictures throughout the site, GD library
###   needs to be installed. GD 2.0 or higher recommended.
### - improved html mail function, with complete headers, this way avoiding emails being treated 
###   as spam by some more restrictive servers
### - multiple fees tiers for auction setup fee and for end of auction fee
### - new payment gateways implemented: Nochex, Ikobo
### - IP Logging feature for site users
### - custom banners now count the clicks (reported bug)
### - option to hide certain categories from being displayed in the categories drop down that is
###   displayed on all the pages
### - the starting bid is now displayed on the home page auctions
### - better unique id system, all users and auctions now start at 100001
### - the fees can be viewed on a fees page
### - auctions browsing is now sortable by different criteria
### - sellers can set the start time of auctions, rather than start when listed
### - buy now auctions can be created (by setting the start price equal to the buy now price - no
###   bidding will then be allowed on that item)
### - sellers can change the end date
### - sellers can change the reserve price
### - new admin accounting section, that lists all pending payments, and done payments, as well as
###   a total of payments pending and made by the site's users.
#####################################################################################################
?>