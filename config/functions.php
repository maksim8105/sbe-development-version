<?
## v5.25 -> jun. 07, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

### newly implemented functions

function strToForm($str) {
    return htmlspecialchars(stripslashes($str));
}

function getSqlRow($query) {
	$result = mysqli_query($GLOBALS["___mysqli_ston"], $query) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	$row = mysqli_fetch_array($result);
	((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	return $row;
}

function getSqlNumber($sqlQuery) {
	$query=@mysqli_query($GLOBALS["___mysqli_ston"], $sqlQuery);
	$result=@mysqli_num_rows($query);
	@((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

function getSqlField($sqlQuery,$field) {
	$isQuery = getSqlNumber($sqlQuery);
	$query = @mysqli_query($GLOBALS["___mysqli_ston"], $sqlQuery) or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))."<br>INVALID QUERY: ".$sqlQuery);
	if ($isQuery>0) {
		$result=@mysqli_fetch_assoc($query)[$field];
	} else $result="n/a";
	@((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

function titleResize($titleText) {
	$maxLength = 55; ## the maximum length of a word is 20 characters; increased from 15 to 55 - ALEKSEI HODUNKOV
	$newText = "";
	$titleText = addSpecialChars($titleText);
	$titleWords = explode(" ",$titleText);
	for ($i=0; $i<count($titleWords); $i++) {
		if (strlen($titleWords[$i])>$maxLength) $newText .= substr($titleWords[$i],0,$maxLength-3)."... ";
		else $newText .= $titleWords[$i]." ";
	}
	return $newText;
}

function deleteFile($filepath,$filename) {
	global $path;
	$success = FALSE;
	if (file_exists($filepath.$filename)&&$filename!=""&&$filename!="n/a") {
		unlink ($filepath.$filename);
		$success = TRUE;
	}
	return $success;	
}

function uploadFile($file,$fileName,$destDir,$overwrite=FALSE, $adminPath = "", $showMsg = TRUE) {
	$success = FALSE;
	if ($file!=""&&$file!="none"&&$file!=$destDir.$fileName) {
		if(file_exists($destDir.$fileName)) { 
			## also delete all appearances in the cache folder of cached versions of the file that is being deleted
			$filename1 = @eregi_replace("\/","",$fileName);
			$filename2 = @eregi_replace("\.","",$filename1);
			$cached_filename = @eregi_replace("\:","",$filename2);
		
			$rep=@opendir($adminPath.'cache/');
			while ($cacheFile = @readdir($rep)){
				if (@eregi($cached_filename, $cacheFile)) @unlink ($adminPath.'cache/'.$cacheFile); 
			}
			@closedir($rep);
			@clearstatcache();		
		}
		$success = TRUE;
		$copyPath = $destDir.$fileName;
		if (!@copy($file, $copyPath)) {
    		if ($showMsg) print ("Error: Failed to copy $file...<br>");
			$success = FALSE;
		}
		##unlink($destDir.$fileName);
	} else if ($file==$destDir.$fileName) $success = TRUE;
	return $success;
}

function remSpecialChars($string) {
	$string = stripslashes($string);
	$string = @eregi_replace("'","&#039;",$string);
	$string = @eregi_replace('"','&quot;',$string);
	return $string;
}

function addSpecialChars($string, $noQuotes = FALSE) {
	$string = @eregi_replace("&amp;","&",$string);
	if (!$noQuotes) $string = @eregi_replace("&#039;","'",$string);
	$string = @eregi_replace('&quot;','"',$string);
	$string = @eregi_replace('&lt;','<',$string);
	$string = @eregi_replace('&gt;','>',$string);
	$string = @eregi_replace('&nbsp;',' ',$string);
	return $string;
}

function paginate($start,$limit,$total,$filePath,$otherParams) {
	global $lang;

	$allPages = ceil($total/$limit);

	$currentPage = floor($start/$limit) + 1;

	$pagination = "";
	if ($allPages>10) {
		$maxPages = ($allPages>9) ? 9 : $allPages;

		if ($allPages>9) {
			if ($currentPage>=1&&$currentPage<=$allPages) {
				$pagination .= ($currentPage>4) ? " ... " : " ";

				$minPages = ($currentPage>4) ? $currentPage : 5;
				$maxPages = ($currentPage<$allPages-4) ? $currentPage : $allPages - 4;

				for($i=$minPages-4; $i<$maxPages+5; $i++) {
					$pagination .= ($i == $currentPage) ? "[".$i."] " : "<a href=\"".$filePath."?start=".(($i-1)*$limit).$otherParams."\">".$i."</a> ";
				}
				$pagination .= ($currentPage<$allPages-4) ? " ... " : " ";
			} else {
				$pagination .= " ... ";
			}
		}
	} else {
		for($i=1; $i<$allPages+1; $i++) {
			$pagination .= ($i==$currentPage) ? "[".$i."] " : "<a href=\"".$filePath."?start=".(($i-1)*$limit).$otherParams."\">".$i."</a> ";
		}
	}

	if ($currentPage>1) $pagination = "[<a href=\"".$filePath."?start=0".$otherParams."\">&lt;&lt;</a>] [<a href=\"".$filePath."?start=".(($currentPage-2)*$limit).$otherParams."\">&lt;</a>] ".$pagination;
	if ($currentPage<$allPages) $pagination .= " [<a href=\"".$filePath."?start=".($currentPage*$limit).$otherParams."\">&gt;</a>] [<a href=\"".$filePath."?start=".(($allPages-1)*$limit).$otherParams."\">&gt;&gt;</a>]";

	echo $pagination;
}

function deleteAuction ($auctionid,$deletionType=FALSE,$countAuctions=TRUE) {
	global $timeNowMysql;
	// KEV START CHANGES - changed the following function, as it stood, if the user closed his/her auction and the admin deleted it, 2 counts were taken off
	$item = getSqlRow("SELECT active,id,category,addlcategory,deleted,closed, listin, ownerid FROM probid_auctions WHERE id='".$auctionid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));	
	if (!$deletionType) {
		$markDeleted = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET deleted=1 WHERE id='".$auctionid."' AND ownerid='".$_SESSION['memberid']."'");
		if ($item['active']==1&&$item['closed']==0&&$item['listin']!="store"&&$item['ownerid']==$_SESSION['memberid']) {
			$closeAuction=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
			closed=1, enddate = '".$timeNowMysql."' WHERE id='".$item['id']."' AND ownerid='".$_SESSION['memberid']."'");
			delcatcount ($item['category'],$auctionid);
			delcatcount ($item['addlcategory'],$auctionid);
		}
	} else if ($deletionType&&$_SESSION['adminarea']=="Active") {
		$getAuction = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_auctions WHERE id='".$auctionid."'");
		$counter=0;
		## first unlink images
		while ($auctionsArray=mysqli_fetch_array($getAuction)) {
			## unlink main images
			## Images remove fix - ALEKSEI HODUNKOV
			/*$imageName = getSqlField("SELECT picpath FROM probid_auctions WHERE id='".$auctionsArray['id']."'","picpath");
			deleteFile("../",$imageName);
			## unlink additional images
			$getAdditionalImages = mysql_query("SELECT * FROM probid_auction_images 
			WHERE auctionid='".$auctionsArray['id']."'");
			while($adImage = mysql_fetch_array($getAdditionalImages)) {
				deleteFile("../",$adImage['name']);
		
			}
		## Images remove end of fix - ALEKSEI HODUNKOV
		*/}
		if ($item['closed']==0&&$item['active']==1&&$item['deleted']!=1&&$countAuctions) {
			delcatcount ($item['category']);
			delcatcount ($item['addlcategory']);
		}
		## now delete all fields that are linked to this auction
		$delAuction = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auctions WHERE id='".$auctionid."'");
		## Images remove fix - ALEKSEI HODUNKOV
		## $delAuctionImages = mysql_query("DELETE FROM probid_auction_images WHERE auctionid='".$auctionid."'");
		## Images remove end of fix - ALEKSEI HODUNKOV
		$delProxy = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_proxybid WHERE auctionid='".$auctionid."'");
		$delBids = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bids WHERE auctionid='".$auctionid."'");
		$delBidsHistory = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bids_history WHERE auctionid='".$auctionid."'");
		##$delWinner = mysql_query("DELETE FROM probid_winners WHERE auctionid='".$auctionid."'");
		##$delFeedback = mysql_query("DELETE FROM probid_feedbacks WHERE auctionid='".$auctionid."' AND submitted!=1");
	}
}

function setStyle($auctionId) { 
	$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
	
	if ($auction['bolditem']=="Y"&&$auction['hlitem']=="Y") $style="hlbolditem";
	else if ($auction['bolditem']=="Y"&&$auction['hlitem']!="Y") $style="bolditem";						
	else if ($auction['bolditem']!="Y"&&$auction['hlitem']=="Y") $style="hlitem";
	else if ($auction['bolditem']!="Y"&&$auction['hlitem']!="Y") $style="item";
	return $style;
}

function showBuyNow($isRp,$reservePrice,$maxBid,$nrBids) {
	global $setts;
	if ($setts['alwaysshowbuynow']) return TRUE;
	$showBn = FALSE;
	if ($nrBids <= 0) {
		$showBn = TRUE;
	} else {
		## nrbids > 0
		if ($isRp == "Y") {
			if ($maxBid < $reservePrice) $showBn = TRUE;
			else $showBn = FALSE;
		} else {
			$showBn = FALSE;
		}	
	}
	return $showBn;
}

function itemPics($auctionId) {
		global $lang, $setts;
		$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
		$current_language=get_current_lang();
	/*	if ($auction['picpath']!="") 
			$result="<img src=\"themes/".$setts[default_theme]."/img/system/camera1.gif\" border=\"0\" align=\"absmiddle\" alt=\"".$lang['picture']."\"> "; */
		$showBn = showBuyNow($auction['rp'],$auction['rpvalue'],$auction['maxbid'],$auction['nrbids']);
		if ($auction['bn']=="Y"&&$showBn&&$auction['closed']==0&&$setts['buyout_process']==0) 
			$result.="<a href=\"buynow.php?id=".$auctionId."\"><img src=\"themes/".$setts[default_theme]."/img/system/buyitnow_$current_language.png\" border=\"0\" align=absmiddle class=\"buynow_img\" alt=\"".$lang['buynow']."\"></a>";
		if ($auction['bn']=="Y"&&$setts['buyout_process']==1) 
			$result.="<a href=\"makeoffer.php?auctionid=".$auctionId."&noerror=1\"><img src=\"themes/".$setts[default_theme]."/img/system/makeoffer25.gif\" border=\"0\" align=absmiddle alt=\"".$lang['makeoffer']."\"></a>" ;
		return $result;
}

function displayAmount($amount,$currency=NULL,$invoicelook=NULL) {
	global $setts, $lang;
	settype($amount, 'double');
	if ($invoicelook==NULL) {
		if ($amount<0) return $lang['item_swapped'];
	}
	if ($currency==NULL) $currency = $setts['currency'];
	if ($amount==99999999999) return "Above";

	if($setts['m_format'] == 1){
        $formattedAmount = number_format($amount,$setts['digits'],'.',',');	
	}else{
        $formattedAmount = number_format($amount,$setts['digits'],',','.');	
	} 
	
	if ($setts['position']==1) $result = $currency." ".$formattedAmount;
	else $result = $formattedAmount." ".$currency;
	
	if ( $invoicelook == NULL && $amount = 0){
	       $result=$lang['nobids'];	
    }
	
	return $result;
}

function getFeedback($userid,$admin=FALSE) {
	global $setts;
	
	$query=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks WHERE userid='".$userid."' AND submitted=1");
	$rating=0;

	$subdir = ($admin) ? "../" : "";

	while ($row=mysqli_fetch_array($query)) {
		if ($row['rate']>=4) $rating++;
		else if ($row['rate']<=2) $rating--;
	}
	if ($rating<1) $result=" ($rating) ";
	else if ($rating>=1&&$rating<10) $result=" ($rating) <img align=absmiddle src=\"".$subdir."themes/".$setts[default_theme]."/img/system/yellow_star.gif\" border=\"0\">";
	else if ($rating>=10&&$rating<50) $result=" ($rating) <img align=absmiddle src=\"".$subdir."themes/".$setts[default_theme]."/img/system/green_star.gif\" border=\"0\">";
	else if ($rating>=50&&$rating<100) $result="( $rating) <img align=absmiddle src=\"".$subdir."themes/".$setts[default_theme]."/img/system/blue_star.gif\" border=\"0\">";
	else if ($rating>=100&&$rating<200) $result=" ($rating) <img align=absmiddle src=\"".$subdir."themes/".$setts[default_theme]."/img/system/red_star.gif\" border=\"0\">";
	else if ($rating>=200) $result=" ($rating) <img align=absmiddle src=\"".$subdir."themes/".$setts[default_theme]."/img/system/gold_star.gif\" border=\"0\">";

	if (getSqlField("SELECT * FROM probid_users WHERE id='".$userid."'","aboutpage_type")==1) 
		$result.=" <a href=\"".$subdir."aboutme.php?userid=".$userid."\"><img src=\"".$subdir."themes/".$setts[default_theme]."/img/system/about_me.gif\" border=\"0\" align=absmiddle></a>";
	/*if (getSqlField("SELECT * FROM probid_users WHERE id='".$userid."'","aboutpage_type")==2) 
		$result.=" <a href=\"".$subdir.processLink('shop', array('userid' => $userid))."\"><img src=\"".$subdir."themes/".$setts[default_theme]."/img/system/25store.gif\" border=\"0\" align=absmiddle></a>";*/
	((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

function setMinBid($value,$autoIncrement,$incrementValue,$auctionType) {
	if ($autoIncrement==0) {
		$getIncrements=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bid_increments");
		while ($increment = mysqli_fetch_array($getIncrements)) {
			if ($value>=$increment['bfrom']&&$value<=$increment['bto']) {
				$minimumBid=$value+$increment['increment'];
			}
		}
	} else {
		$minimumBid=$value+$incrementValue;
	}
	return $minimumBid;
}

function deleteConfirm($oklink,$cancellink) {
	global $lang;
	$output = "<table width=\"70%\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"2\" class=\"border\">\n"; 
  	$output.= "	<tr class=\"c2\">																	\n"; 
    $output.= "		<td colspan=\"2\" align=\"center\" class=\"contentfont\">".$lang[areyousure]."</td>\n"; 
  	$output.= "	</tr>																				\n"; 
  	$output.= "	<tr class=\"c3\">																	\n"; 
    $output.= "		<td width=\"50%\" align=\"center\" class=\"submenu\"><a href=\"".$oklink."&confirmed=yes\"><strong>".$lang[yes]."</strong></a></td>\n"; 
    $output.= "		<td align=\"center\" class=\"submenu\"><a href=\"".$cancellink."\"><strong>".$lang[no]."</strong></a></td>\n";
  	$output.= "	</tr>																				\n"; 
	$output.= "</table>"; 
	return $output;
}

function hiddenItem($categoryId) { 
}

### here go the old functions
### this function will calculate the minimum bid (will apply the automatic increment

function checkbanned($bannedadr,$type) {
	$query=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_banned WHERE bannedadr='".$bannedadr."' AND type='".$type."'");
	$isbanned=mysqli_num_rows($query);
	if ($isbanned>0) return true;
	else return false;
}

$htmlfont = "<font face=\"Verdana, Arial, Helvetica\" size=\"2\">";
function htmlmail($to, $subject, $msg, $sender, $hmsg = "", $sendMail=TRUE) {
	global $siteurl, $setts, $htmlfont, $currentVersion;

	## set date
	$tz = date("Z");
    $tzs = ($tz < 0) ? "-" : "+";
    $tz = abs($tz);
    $tz = ($tz/3600)*100 + ($tz%3600)/60;
    $sendDate = sprintf("%s %s%04d", date("D, j M Y H:i:s"), $tzs, $tz);
	
	$uniq_id = md5(uniqid(time()));

	## create the message body
	if ($hmsg == "") $hmsg = $msg;
	$htmlmsg .= "<!--\n$msg\n-->\n";
	$htmlmsg .= "<html><body><p>$htmlfont" . $hmsg . "</body></html>";

	if ($setts['mailer']=="sendmail"&&$sendMail) {
		## send through the UNIX Sendmail function
		$Sendmail = $setts['sendmail_path'];
	
		## create header
		$header = "";
		$header .= "Date: ".$sendDate."\n";
		$header .= "Return-Path: ".$sender."\n";
		$header .= "To: ".$to."\n"; 
		$header .= "From: ".$sender." <".$sender.">\n"; 

		## admin BCC
		if ($setts['bcc']=="Y") $header .= "Bcc: ".$setts['adminemail']."\n";

		$header .= "Reply-to: ".$sender."\n";
		$header .= "Subject: ".$subject."\n";
		$header .= sprintf("Message-ID: <%s@%s>%s", $uniq_id, $_SERVER['SERVER_NAME'], "\n");
		$header .= "X-Priority: 3\n";
		$header .= "X-Mailer: PHPProBid/Sendmail [version " . $currentVersion . "]\n";
		$header .= "MIME-Version: 1.0\n";
		$header .= "Content-Transfer-Encoding: 8bit\n";
		$header .= sprintf("Content-Type: %s; charset=\"%s\"","text/html","UTF-8");
		$header .= "\n\n";

		if ($sender != "")
			$sendmail = sprintf("%s -oi -f %s -t", $Sendmail, $sender);
		else
			$sendmail = sprintf("%s -oi -t", $Sendmail);

		if(!@$mail = popen($sendmail, "w")) {
			echo "Could not execute: " . $Sendmail;
		}
		
		fputs($mail, $header);
		fputs($mail, $htmlmsg);
        
		$result = pclose($mail) >> 8 & 0xFF;
		if($result != 0) {
			echo "Could not execute: " . $Sendmail;
		}
	} else if ($setts['mailer']=="mail"&&$sendMail) {
		## send through the PHP mail() function
		## create header
		$boundary[1] = "b1_" . $uniq_id;
		$boundary[2] = "b2_" . $uniq_id;
	
		$header = "";
		$header .= "Date: ".$sendDate."\n";
		$header .= "Return-Path: ".$sender."\n";
		$header .= "From: ".$sender." <".$sender.">\n";

		## admin BCC
		if ($setts['bcc']=="Y") $header .= "Bcc: ".$setts['adminemail']."\n";

		$header .= "Reply-to: ".$sender."\n";
		$header .= sprintf("Message-ID: <%s@%s>%s", $uniq_id, $_SERVER['SERVER_NAME'], "\n");
		$header .= "X-Priority: 3\n";
		$header .= "X-Mailer: PHPProBid [version " . $currentVersion . "]\n";
		$header .= "MIME-Version: 1.0\n";
		$header .= "Content-Transfer-Encoding: 8bit\n";
		$header .= sprintf("Content-Type: %s; charset=\"%s\"","text/html","UTF-8");
	
		$params = sprintf("-oi -f %s",$sender);
		if (strlen(ini_get("safe_mode"))<1) {
			$old_from = ini_get("sendmail_from");
			ini_set("sendmail_from",$sender);
			$result = @mail($to, $subject, $htmlmsg, $header, $params);
		} else {
			$result = @mail($to, $subject, $htmlmsg, $header);
		}
		if (isset($old_from)) ini_set("sendmail_from",$old_from);
		if (!$result) echo "Mail Sending Failed..";
	}
}

### IP Logging addon, created by Kevin

if ($_SESSION['memberid'] > 0) {
	$set = 0;
	mysqli_query($GLOBALS["___mysqli_ston"], "CREATE TABLE IF NOT EXISTS `probid_iphistory` (
	`memberid` INT NOT NULL, 
	`time1` INT NOT NULL, 
	`time2` INT NOT NULL, 
	`ip` VARCHAR(20) NOT NULL)");
	$q = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT time1, time2, ip FROM `probid_iphistory` WHERE 
	memberid='".$_SESSION['memberid']."' ORDER by time1 DESC LIMIT 1");
	if (mysqli_num_rows($q) > 0) {
		if ($r = mysqli_fetch_row($q)) {
			if ($r[2] == $_SERVER['REMOTE_ADDR']) {
				mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE `probid_iphistory` SET time2='".time()."' WHERE 
				time1='".$r[0]."' AND ip='".$r[2]."'");
				$set = 1;
			}
		}
	}
	if (!$set) {
		mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO `probid_iphistory` VALUES 
		('".$_SESSION['memberid']."','".time()."','0','".$_SERVER['REMOTE_ADDR']."')");
	}
}

function countCategories() {
}

## Added these 2 new functions, these replace counterAddItem and counterRemItem
## They work on passing the category rather than the item number

function addcatcount ($catid, $auctionid = 0) //adds a category count to each relevant category
{
	$cnt = 0;

	$canAdd = TRUE;
	if (getSqlField("SELECT listin FROM probid_auctions WHERE id='".$auctionid."'","listin") == "store") $canAdd = FALSE;

	if($catid > 0 && $canAdd) {
		$croot = $catid;
		while ($croot>0) {
			$crw = getSqlRow("SELECT parent FROM probid_categories WHERE id='$croot'");		
			$addcount[$cnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET items_counter=items_counter+1 WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$croot = $crw['parent'];
		}
	} 
}

function delcatcount ($catid, $auctionid = 0) //removes a category count to each relevant category
{
	$cnt = 0;

	$canDel = TRUE;
	if (getSqlField("SELECT listin FROM probid_auctions WHERE id='".$auctionid."'","listin") == "store") $canDel = FALSE;

	if($catid > 0 && $canDel) {
		$croot = $catid;
		while ($croot>0) {
			$crw = getSqlRow("SELECT parent FROM probid_categories WHERE id='$croot'");		
			$delcount[$cnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET items_counter=items_counter-1 WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$croot = $crw['parent'];
		}
	} 
}
function counterAddItem($item_id) {
}

function counterRemItem($item_id) { 
}

### added function to add an item to the category counter - if we want to activate all auctions for a user
### added the call to take only the category and addlcategory fields and also items that are not deleted!!!!
function counterAddUser($user_id) {
	$get_auctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,category,addlcategory FROM probid_auctions WHERE ownerid='$user_id' AND active=0 AND closed=0 AND deleted!=1");
	while ($auction = mysqli_fetch_array($get_auctions)) {
		addcatcount ($auction['category'], $auction['id']);
		addcatcount ($auction['addlcategory'], $auction['id']);
	}
	$get_wanted_ads = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT category,addlcategory FROM probid_wanted_ads WHERE ownerid='$user_id' AND active=0 AND closed=0 AND deleted!=1");
	while ($wantedAd = mysqli_fetch_array($get_wanted_ads)) {
		addwantedcount ($wantedAd['category']);
		addwantedcount ($wantedAd['addlcategory']);
	}
}

### added function to substract an item from the category counter - if we want to suspend all auctions for a user
### added the call to take only the category and addlcategory fields and also items that are not deleted!!!!
function counterRemUser($user_id) { 
	$get_auctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,category,addlcategory FROM probid_auctions WHERE ownerid='$user_id' AND active=1 AND closed=0 AND deleted!=1");
	while ($auction = mysqli_fetch_array($get_auctions)) {
		delcatcount ($auction['category'],$auction['id']);
		delcatcount ($auction['addlcategory'],$auction['id']);
	}
	$get_wanted_ads = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT category,addlcategory FROM probid_wanted_ads WHERE ownerid='$user_id' AND active=1 AND closed=0 AND deleted!=1");
	while ($wantedAd = mysqli_fetch_array($get_wanted_ads)) {
		delwantedcount ($wantedAd['category']);
		delwantedcount ($wantedAd['addlcategory']);
	}
}

function calcReduction ($amount, $prefSeller,$voucherCode="",$feeType="", $relistReduction = 0) {
	global $setts;
	
	$voucher = checkSetupVoucher(trim($voucherCode),trim($feeType));

	$reduction = ($prefSeller=="Y") ? ($setts['pref_sellers_reduction']/100) : 0;
	$result = $amount - ($amount * $reduction);
	
	if ($voucher['valid']) {
		$result = $result - ($result * ($voucher['reduction']/100));
	}
	
	$result = $result - ($result * ($relistReduction/100));
	
	return $result;
}

function calcHighBid($auctionid) {
	### this function is used when bids are retracted.
	### it will determine the minimum high bid necessary to remain
	### the reserve price will also be taken in consideration
	### if the bids are over the reserve, then the minimum bid will be the reserve.
	$getHighBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE auctionid='".$auctionid."' ORDER BY bidamount ASC");
	// maybe there are no other bids, then skip this, instead just modify probid_auctions
	$isBids = mysqli_num_rows($getHighBids);
	if ($isBids>0) {
		### if there is more than one bid we will save all bids so we can make the necessary operations
		$cnt = 0;
		while ($bids = mysqli_fetch_array($getHighBids)) {
			$bidId[$cnt] = $bids['id'];
			$bidderId[$cnt] = $bids['bidderid'];
			$bidAmount[$cnt] = $bids['bidamount'];
			$bidProxy[$cnt] = $bids['proxy'];
			$bidQuant[$cnt] = $bids['quantity'];
			$cnt++;
		}
		### now we make the necessary calculations
		$auction = getSqlRow("SELECT rp, rpvalue FROM probid_auctions WHERE id='".$auctionid."'");
		$reserve = ($auction['rp']=="Y") ? $auction['rpvalue'] : 0 ;
		## if the maximum bid is under the reserve, then we wont need the reserve, so we'll set it to 0.
		$reserve = ($bidAmount[$cnt]<$reserve) ? 0 : $reserve;
		$cnt--;
		$deletedBids = 0;
		$highBidId = 0;
		while ($cnt>0) {
			if ($bidderId[$cnt] == $bidderId[$cnt-1] && $bidAmount[$cnt-1]>=$reserve) {
				## delete the bid
				$isDel = getSqlNumber("SELECT id FROM probid_bids WHERE id = '".$bidId[$cnt]."' AND quantity='".$bidQuant[$cnt]."' AND proxy='".$bidProxy[$cnt]."'");
				mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bids WHERE id = '".$bidId[$cnt]."' AND quantity='".$bidQuant[$cnt]."' AND proxy='".$bidProxy[$cnt]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
				## only a temporary solution
				if ($isDel) {
					mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bids_history WHERE bidid = '".$bidId[$cnt]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					$highBidId = $bidId[$cnt-1];
					$highBidAmount = $bidAmount[$cnt-1];
					$deletedBids++;
				}
			}
			$cnt--;
		}
		if ($highBidId > 0) {
			## set high bid and all. Proxy will remain the same though
			$setHighBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET out=0, invalid=0 WHERE id='".$highBidId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
			maxbid = '".$highBidAmount."', nrbids = nrbids-".$deletedBids." WHERE id='".$auctionid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
		### if all bids are out, select the highest bid as active.
		$nbActiveBids = getSqlNumber("SELECT id FROM probid_bids WHERE out=0 AND invalid=0 AND  auctionid='".$auctionid."'");
		if ($nbActiveBids==0) $highestBid = getSqlField("SELECT id FROM probid_bids WHERE auctionid='".$auctionid."' ORDER BY bidamount DESC, id DESC LIMIT 0,1","id");
		$activateBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET out=0, invalid=0 WHERE id='".$highestBid."'");
		### if there is only one bid left, then set it to be equal with the start price;
		$nbBids = getSqlNumber("SELECT * FROM probid_bids WHERE auctionid='".$auctionid."'");
		if ($nbBids == 1&&$reserve==0) {
			## update bids, bids history and auctions tables.
			$auction = getSqlRow("SELECT rp, rpvalue FROM probid_auctions WHERE id='".$auctionid."'");
			$reserve = ($auction['rp']=="Y") ? $auction['rpvalue'] : 0 ;
			$bidStart = getSqlField("SELECT bidstart FROM probid_auctions WHERE id='".$auctionid."'","bidstart");
			$bidId = getSqlField("SELECT id FROM probid_bids WHERE auctionid='".$auctionid."'","id");
			$bidProxy = getSqlField("SELECT proxy FROM probid_bids WHERE id='".$bidId."'","proxy");
			
			$bidToSet = ($reserve>0&&$bidProxy>=$reserve) ? $reserve : $bidStart;
			$updateBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET bidamount='".$bidToSet."' WHERE auctionid='".$auctionid."'");
			$updateBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids_history SET amount='".$bidToSet."' WHERE auctionid='".$auctionid."'");
			$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET maxbid='".$bidToSet."' WHERE id='".$auctionid."'");
		}
		
		### last thing, we will select the active bid and set the proxy bid as equal with the proxy from the bids table, if > 0
		$highestBid = getSqlRow("SELECT auctionid,bidderid,bidamount,proxy FROM probid_bids WHERE 
		out=0 AND invalid=0 AND auctionid='".$auctionid."' ORDER BY bidamount DESC, proxy DESC LIMIT 0,1");
		
		if ($highestBid['proxy']>0) {
			$isProxy = getSqlNumber("SELECT id FROM probid_proxybid WHERE auctionid='".$auctionid."'");
			if ($isProxy>0) {
				$updateProxy = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_proxybid SET 
				bidderid='".$highestBid['bidderid']."',bidamount='".$highestBid['proxy']."' WHERE auctionid='".$auctionid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			} else {
				$insertProxy = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_proxybid 
				(auctionid, bidderid, bidamount) VALUES
				('".$auctionid."','".$highestBid['bidderid']."','".$highestBid['proxy']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
		}
		## if auction is dutch then activate all bids up to the available quantity
		$aDets = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$auctionid."'");
		if ($aDets['auctiontype']=="dutch") {			
			### activate all bids up to the available quantity
			$outbid_bids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET out=1 WHERE auctionid='".$aDets['id']."' AND out=0 AND invalid=0");
			
			$o_cnt=0;
			$getAllOutBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE auctionid='".$aDets['id']."' AND deleted!='1' ORDER BY id DESC");
			$nbOutBids = mysqli_num_rows($getAllOutBids);
			while ($allOutBids = mysqli_fetch_array($getAllOutBids)) {
				$outbid_id[$o_cnt] = $allOutBids['id']; 
				$outbid_quantity[$o_cnt] = $allOutBids['quantity'];
				$o_cnt++; 
			}
							
			$rem_qnt = $aDets['quantity'];
			for ($i=0; $i<$o_cnt; $i++) {
				if ($rem_qnt>0) $act_Bid[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET out='0' WHERE id='".$outbid_id[$i]."'");
				$rem_qnt-=$outbid_quantity[$i];
				//echo "ID: ".$outbid_id[$i].", RQ: ".$rem_qnt."<br>";
			}
				
			### now equalize all active bids to the lowest active bid.
			$lowestActiveBid = getSqlField("SELECT proxy FROM probid_bids WHERE auctionid='".$auctionid."' AND invalid='0' AND out='0' AND deleted!='1' ORDER BY proxy ASC LIMIT 0,1","proxy");
			
			### if only one user is bidding then the lowest bid will be the bid start
			$nbBidders = getSqlNumber("SELECT id FROM probid_bids WHERE auctionid='".$auctionid."' AND invalid='0' AND out='0' AND deleted!='1' GROUP BY bidderid");
			if ($nbBidders==1) $lowestActiveBid = getSqlField("SELECT bidstart FROM probid_auctions WHERE id='".$auctionid."'","bidstart");
									
			$equalizeActiveBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET bidamount='".$lowestActiveBid."' WHERE auctionid='".$auctionid."' AND invalid='0' AND bidamount>'".$lowestActiveBid."' AND deleted!='1'");
			$getAB = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, bidamount FROM probid_bids WHERE  auctionid='".$auctionid."'");
			while ($updHist = mysqli_fetch_array($getAB)) {
				$updBH[$ucnt] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids_history SET amount='".$updHist['bidamount']."' WHERE bidid='".$updHist['id']."'");
			}
			$updateMaxBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET maxbid='".$lowestActiveBid."' WHERE id='".$auctionid."'");						
		}		
	} else {
		$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET maxbid='0' WHERE id='".$auctionid."'");
	}	

	$nb_bids = getSqlNumber("SELECT id FROM probid_bids WHERE auctionid='".$auctionid."'");
	$updnbids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET nrbids=".$nb_bids." WHERE id='".$auctionid."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));	
}

## same as in cron, this is included because it might not do the thing if cron is run from probid
## added in v5.22 -> only suspend the users if the option in admin to do so is enabled
if (($setts['account_mode']==2||$setts['account_mode_personal']==1)&&$setts['suspend_over_bal_users']==1) {
	## now we will invalidate accounts for which the balance is over max_limit
	if ($setts['account_mode_personal']==1) 
		$getaccs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_users WHERE balance>allowed_credit AND active='1'");
	else if ($setts['account_mode']==2)
		$getaccs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_users WHERE balance>'".$setts['max_credit']."' AND active='1'");

	while ($inrow = mysqli_fetch_array($getaccs)) {
		counterRemUser($inrow['id']);

		$inactauct = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET active='0' WHERE ownerid='$inrow[id]'");
		$inactuser = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET active='0' WHERE id='$inrow[id]'");
		### v5.01 -> send email to the user who's account gets suspended
		$userId = $inrow['id'];
		$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$userId."'");

		$plainMessage = "Dear ".$user['name'].",												\n".
						"																		\n".
						"Your account on ".$setts['sitename']." was suspended, because your		\n".
						"account has gone over the maximum debit limit permitted by our site.	\n".
						"																		\n".
						"In order to activate your account, please log in to our site at:		\n".
						"																		\n".
						"".$setts['siteurl']."login.php											\n".
						"																		\n".
						"and clear your account balance.										\n".
						"																		\n".
						"Warm regards,															\n".
						"The ".$setts['sitename']." Staff";

		$htmlMessage = 	"Dear ".$user['name'].",												<br>".
						"																		<br>".
						"Your account on ".$setts['sitename']." was suspended, because your		<br>".
						"account has gone over the maximum debit limit permitted by our site.	<br>".
						"																		<br>".
						"In order to activate your account, please click on the link below:		<br>".
						"																		<br>".
						"<a href=\"".$setts['siteurl']."login.php\">Click for the login page</a><br>".
						"																		<br>".
						"and clear your account balance.										<br>".
						"																		<br>".
						"Warm regards,															<br>".
						"The ".$setts['sitename']." Staff";
		htmlmail($user['email'],$setts['sitename']." - Account Suspended",$plainMessage,$setts['adminemail'],$htmlMessage);
		// if the user is logged in and tries to post an auction and his account is maxxed, then the
		// session variable should change so the user cannot post auctions anymore
		if ($_SESSION['membersarea']=="Active") {
			$_SESSION['accsusp'] = 2;
			$_SESSION['membersarea']="";
		}
	}
}

## v5.01 addition - function which checks if user has free fees (100% fee reduction)
function freeFees($userId) {
	global $setts;
	
	$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
	WHERE id='".$userId."'","preferred_seller");
	
	if ($setts['pref_sellers']=="Y"&&$setts['pref_sellers_reduction']>=100&&$prefSeller=="Y") { 
		$freeFee=TRUE;
	} else {
		$freeFee=FALSE;
	}
	
	return $freeFee;
}

function showFeedback($rate) {
	global $lang;
##	if ($rate<3) echo "<span style=\"color:#FF0000; \">".$lang[negative]."</span>";
##	else if ($rate==3) echo "<span style=\"color:#666666; \">".$lang[neutral]."</span>";
##	else if ($rate>3) echo "<span style=\"color:#009933; \">".$lang[positive]."</span>";
## v5.22 -> ticks
	if ($rate<=1) echo "<img src=\"images/1stars.gif\" border=\"0\">";
	else if ($rate==2) echo "<img src=\"images/2stars.gif\" border=\"0\">";
	else if ($rate==3) echo "<img src=\"images/3stars.gif\" border=\"0\">";
	else if ($rate==4) echo "<img src=\"images/4stars.gif\" border=\"0\">";
	else if ($rate>=5) echo "<img src=\"images/5stars.gif\" border=\"0\">";
}

function calcFeedback($userid, $feedbackType='') {
	global $lang;
	$addQuery = '';
	if ($feedbackType!='') $addQuery = " AND type='".$feedbackType."'";
	$getFeedbacks = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks WHERE
	userid='".$userid."' AND submitted=1".$addQuery);
	while ($fb = mysqli_fetch_array($getFeedbacks)) {
		if ($fb['rate']>3) $fbPositive++;
		else if ($fb['rate']<3) $fbNegative++;
	}
	$fbTotal = $fbPositive + $fbNegative;
	if ($fbTotal==0) $fbTotal=1;

	## as requested, it returns only positive feedback.
	$toReturn = number_format(($fbPositive*100/$fbTotal),2,".",",")."%";
	return $toReturn;
	## return ($fbPositive*100/$fbTotal)."% ".$lang[positive]." | ".($fbNegative*100/$fbTotal)."% ".$lang[negative];
}

function getMainCat($parent) {
	if($parent > 0) {
		$cat = $parent;
		$cntr = 0;
		while ($cat>0) {
			$category = $cat;
			$cat = getSqlField("SELECT parent FROM probid_categories WHERE id='".$cat."'","parent");
		}
	} 
	return $category;
}

### function which checks if under a number of hours are left until the auction ends, returns TRUE if over, FALSE if under
function underTime($auctionId) {
	global $setts;
	$auction = getSqlRow("SELECT enddate FROM probid_auctions WHERE id='".$auctionId."'");
	$minHours = 12; ## set to 12 hours.
	$timeLeft = daysleft($auction['enddate'],$setts['date_format']);
	$isTime = $timeLeft - ($minHours * 60 * 60);
	if ($isTime<=0) return FALSE;
	else return TRUE;
}

### function that applies Tax/VAT on an amount

function applyVat($amount, $userid) {
	global $setts;
	$vatExempted = "Y";
	if ($setts['vat_rate']>0) $vatExempted = getSqlField("SELECT vat_exempted FROM probid_users
	WHERE id='".$userid."'","vat_exempted");

	if ($vatExempted=="Y") $amountResulted = $amount;
	else $amountResulted = $amount + $amount * ($setts['vat_rate']/100);
	
	return $amountResulted;
}

## custom fields creation function
function createField($boxId, $selectedValue, $table = 'probid_fields') {
	$getValues = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM $table WHERE boxid='".$boxId."' ORDER BY boxorder ASC");
	$boxtype = getSqlField("SELECT boxtype FROM $table WHERE  boxid='".$boxId."' LIMIT 0,1","boxtype");
	$htmlOutput = "";
	if ($boxtype == "text") {
		$htmlOutput = "<input type=\"text\" name=\"box".$boxId."\" value=\"".$selectedValue."\">";
	} else if ($boxtype == "textarea") {
		$htmlOutput = "<textarea name=\"box".$boxId."\" cols=\"40\" rows=\"5\">".$selectedValue."</textarea>";
	} else if ($boxtype == "list") {
		$htmlOutput = "<select name=\"box".$boxId."\">\n";
		while ($fields = mysqli_fetch_array($getValues)) {
			$htmlOutput .= "<option value=\"".$fields['boxvalue']."\" ".(($fields['boxvalue']==$selectedValue)?"selected":"").">".$fields['boxcaption']."</option>";
		}
		$htmlOutput .= "</select>";
	} else if ($boxtype == "radio") {
		while ($fields = mysqli_fetch_array($getValues)) {
			$htmlOutput .= "<input type=\"radio\" name=\"box".$boxId."\" value=\"".$fields['boxvalue']."\" ".(($fields['boxvalue']==$selectedValue)?"checked":"")."> ".$fields['boxcaption'];
		}
	} else if ($boxtype == "checkbox") {
		while ($fields = mysqli_fetch_array($getValues)) {
			if ($selectedValue == "") $selectedValue = array();
			$htmlOutput .= "<input type=\"checkbox\" name=\"box".$boxId."[]\" value=\"".$fields['boxvalue']."\" ".((in_array($fields['boxvalue'],$selectedValue))?"checked":"")."> ".$fields['boxcaption'];
		}
	}		
	return $htmlOutput;
}

### if registration approval is enabled and activation fee is set, set the mailactivated 
### setting to 1 for the active accounts

if ($setts['enable_ra']=="Y"&&$fee['is_signup_fee']=="Y"&&$fee['val_signup_fee']>0)
	$fullyActivate = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET mailactivated = 1 WHERE
	active = 1 AND payment_status = 'confirmed' AND mailactivated = '0'");
	
### the new bid retraction function
function retractBid($auctionid, $bidderid) {
	global $lang, $setts;
	if ($setts['enable_bid_retraction']=="Y") {
		### first we will remove the bid remains from the necessary tables
		$nbBids = getSqlNumber("SELECT * FROM probid_bids WHERE 
		auctionid='".$auctionid."' AND bidderid='".$bidderid."'");
		$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE 
		auctionid='".$auctionid."' AND bidderid='".$bidderid."'");
		while ($bids = mysqli_fetch_array($getBids)) {
			$delBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bids_history WHERE 
			bidid='".$bids['id']."' AND auctionid='".$auctionid."'");
		}
		$delBid = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bids WHERE 
		auctionid='".$auctionid."' AND bidderid='".$bidderid."'");
		$delProxyBid = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_proxybid WHERE 
		auctionid='".$auctionid."' AND bidderid='".$bidderid."'");
	
		$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
		nrbids=nrbids-".$nbBids." WHERE id='".$auctionid."'");
		## we also need to set the minimum possible bid for the remaining bidders.
		
		calcHighBid($auctionid);
	} else {
		echo "<p class=contentfont>$lang[operation_impossible]</p>";
	}
}

## added v5.10 - if a payment is made, activate the account
if ($_SESSION['memberid']>0&&$_SESSION['accsusp']==0) $_SESSION['membersarea']="Active";

## these are the counter functions for the wanted ads.

function addwantedcount ($catid) //adds a category count to each relevant category
{
	$cnt = 0;
	if($catid > 0) {
		$croot = $catid;
		while ($croot>0) {
			$crw = getSqlRow("SELECT parent FROM probid_categories WHERE id='$croot'");		
			$addcount[$cnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET wanted_counter=wanted_counter+1 WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$croot = $crw['parent'];
		}
	} 
}

function delwantedcount ($catid) //removes a category count to each relevant category
{
	$cnt = 0;
	if($catid > 0) {
		$croot = $catid;
		while ($croot>0) {
			$crw = getSqlRow("SELECT parent FROM probid_categories WHERE id='$croot'");		
			$delcount[$cnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET wanted_counter=wanted_counter-1 WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			$croot = $crw['parent'];
		}
	} 
}

function showPaidFlag($flag) {
	global $lang;
	switch ($flag) {
		case 0: 
			$output = "<span class=redfont><strong>".$lang[flag_paid_unpaid]."</strong></span>";
			break;
		case 1: 
			$output = "<span class=greenfont><strong>".$lang[flag_paid_paid]."</strong></span>";
			break;		
	}
	return $output;
}

function showStatusFlag($flag) {
	global $lang;
	return $lang['flag_status_'.$flag];
}

function auctionListedIn($auctionId,$alwaysShow=FALSE) {
	global $lang;
	$auction = getSqlRow("SELECT ownerid, listin FROM probid_auctions WHERE id='".$auctionId."'");
	$storeEnabled = getSqlField("SELECT aboutpage_type FROM probid_users WHERE id='".$auction['ownerid']."'","aboutpage_type");
	$listed_in = "";
	if ($storeEnabled==2||$alwaysShow) {
		if ($auction['listin']=="auction") $listed_in = "[ ".$lang[listed_on_site]." ]";
		else if ($auction['listin']=="store") $listed_in = "[ ".$lang[listed_in_store]." ]";
		else if ($auction['listin']=="both") $listed_in = "[ ".$lang[listed_in_both]." ]";
	}
	
	return $listed_in;
}

function storeAccountType($storeAccId) {
	global $lang, $setts, $fee;
	$result = array();
	if (!$storeAccId) { 
		$result['store_nb_items'] = 0;
		$result['fee_amount'] = 0;
		$result['store_recurring'] = 0;
		## the unlimited type will only be valid if the store fees are disabled.
		if ($fee['is_store_fee'] == "N"||getSqlNumber("SELECT id FROM probid_fees_tiers WHERE fee_type='store'")==0) $result['store_valid'] = 1;
		else $result['store_valid'] = 0;
	} else {
		$getStore = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE id='".$storeAccId."'");
		$isStore = mysqli_num_rows($getStore);
		if ($isStore) {
			$storeValues = mysqli_fetch_array($getStore);
			$result['store_name'] = $storeValues['store_name'];
			$result['store_nb_items'] = $storeValues['store_nb_items'];
			$result['fee_amount'] = $storeValues['fee_amount'];
			$result['store_recurring'] = $storeValues['store_recurring'];
			$result['store_valid'] = 1;
		} else {
			$result['store_valid'] = 0;
		}
	} 
	return $result;
}

function checkSignupVoucher($voucher_code) {
	$valid = false;
	$currentTime = time();
	$getVoucher = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_vouchers WHERE voucher_code='".$voucher_code."' AND 
	regdate<='".$currentTime."' AND (expdate='0' OR expdate>='".$currentTime."') AND (usesleft>0 OR nbuses=0) AND active='1'");
	$isVoucher = mysqli_num_rows($getVoucher);
	$voucher = mysqli_fetch_array($getVoucher);
	if ($isVoucher>0) {
		if ($voucher['nbuses']>0) $updateVoucher=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_vouchers SET usesleft=usesleft-1 WHERE id='".$voucher['id']."'");
		$valid = true;
	}
	return $valid;
}

function checkSetupVoucher($voucher_code,$fee_type="") {
	
	$result = array();
	$result['valid'] = false;
	$result['reduction'] = 0;
	$result['nbuses'] = 0;
	$result['id'] = 0;
	
	$addQuery = "";
	if ($fee_type!="") $addQuery = " AND (assignto='' OR assignto=';all;' OR assignto LIKE '%".$fee_type."%')";
	$currentTime = time();
	$getVoucher = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,nbuses,reduction FROM probid_vouchers WHERE voucher_code='".$voucher_code."' AND 
	regdate<='".$currentTime."' AND (expdate='0' OR expdate>='".$currentTime."') AND (usesleft>0 OR nbuses=0) AND
	(assigned_users='' OR assigned_users LIKE '%".$_SESSION['memberusern']."%') ".$addQuery." AND active='1'");
		
	$isVoucher = mysqli_num_rows($getVoucher);
	$voucher = mysqli_fetch_array($getVoucher);
	if ($isVoucher>0) {
		$result['valid'] = true;
		$result['reduction'] = $voucher['reduction'];
		$result['nbuses'] = $voucher['nbuses'];
		$result['id'] = $voucher['id'];
	}
	
	return $result;
}

function getFileExtension($input_file) {
	$file_array = explode(".",$input_file);
	
	$nb_array = count($file_array);
	$ext_cnt = count($file_array) - 1;
	
	$extension = ($nb_array<=1) ? "" : $file_array[$ext_cnt];
	$extension = strtolower($extension);
	
	if ($extension!="gif"&&$extension!="jpg"&&$extension!="jpeg"&&$extension!="png"&&$extension!="bmp"&&$extension!="avi"&&$extension!="mpg"&&$extension!="mpeg"&&$extension!="mov") $extension="img";
	
	return $extension;
}

function fileUploadError($uploadError) { 
	global $lang;

	switch ($uploadError) {
		case 1:
			$output = $lang[fileuplerr1];
			break;
		case 2:
			$output = $lang[fileuplerr2];
			break;
		case 3:
			$output = $lang[fileuplerr3];
			break;
		case 4:
			$output = $lang[fileuplerr4];
			break;
		case 5:
			$output = $lang[fileuplerr5];
			break;
		default:
			$output = $lang[fileuplerr6];
	}
	
	return $output;
}
### added in v5.20 - check if the user was activated or inactivated during his session
if ($_SESSION['memberid']>0) {
	$userDetails = getSqlRow("SELECT id, payment_mode, active, is_seller FROM probid_users WHERE id='".$_SESSION['memberid']."'");
	
	if ($setts['account_mode_personal']==1) {
	  	$account_mode_local = ($userDetails['payment_mode']==1) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	if ($userDetails['active']==1) {
		$_SESSION['membersarea']="Active";
		$_SESSION['accsusp']=0;
	} else if ($userDetails['active']!=1&&$account_mode_local==2) {
		$_SESSION['membersarea']="";
		if ($_SESSION['accsusp']!=2) {
			$_SESSION['accsusp']=2;
		}
	} else if ($userDetails['active']!=1&&$account_mode_local!=2) {
		$_SESSION['membersarea']="";
		$_SESSION['accsusp']=1;
	}
		
	if ($setts['private_site']=="Y") $_SESSION['is_seller']=$userDetails['is_seller'];
	else $_SESSION['is_seller']="Y";
}

if ($_SESSION['auctionid_refresh']>0) {
	$auct_submitted = getSqlNumber("SELECT id FROM probid_auctions WHERE id='".$_SESSION['auctionid_refresh']."'");
	if ($auct_submitted && $_REQUEST['step']!="step3") $_SESSION['auctionid_refresh'] = 0;
}

if ($_SESSION['buynow_refresh']&&!@eregi("buynow.php",$_SERVER['PHP_SELF'])) $_SESSION['buynow_refresh']=FALSE;

## start procedure to suspend auctions that are awaiting approval
## this is a workaround to also work with auction counters, but also avoid server load (limit 20 auctions/load)
(string)$addApprovalQuery = NULL;
(int)$countLimit = 20;

/*
if (@eregi('N', $setts['enable_auctions_approval'])) {
	$addApprovalQuery = " AND category IN (".$approvedCats.") ";
	$updateApprovalAuctsByUser = mysql_query("UPDATE probid_auctions a, probid_users u SET a.active=0 WHERE a.approved=0 AND 
	a.active=1 AND u.id=a.ownerid AND u.auction_approval='Y' AND (a.closed=0 OR a.enddate>'".$timeNowMysql."')");
}
$updateApprovalAucts = mysql_query("UPDATE probid_auctions SET active=0 WHERE approved=0 AND active=1 AND (closed=0 OR enddate>'".$timeNowMysql."') ".$addApprovalQuery);
*/

if (@eregi('N', $setts['enable_auctions_approval'])) {
	$approvedCats = (!empty($setts['approval_categories'])) ? substr($setts['approval_categories'],0,-1) : -1;
	
	$addAppQuery = " AND (a.category IN (".$approvedCats.") OR a.addlcategory IN (".$approvedCats.") OR u.auction_approval='Y')";
	##$updateApprovalAuctsByUser = mysql_query("UPDATE probid_auctions a, probid_users u SET a.active=0 WHERE a.approved=0 AND 
	##a.active=1 AND u.id=a.ownerid AND u.auction_approval='Y' AND (a.closed=0 OR a.enddate>'".$timeNowMysql."')");
}

$approvalQuery = "SELECT a.id, a.category, a.addlcategory, a.ownerid FROM probid_auctions a, probid_users u WHERE a.approved=0 AND a.active=1 AND u.id=a.ownerid AND 
a.count_in_progress=0 AND (a.closed=0 OR a.enddate>'".$timeNowMysql."') ".$addAppQuery." LIMIT 0,".$countLimit;

$getApprAucts = mysqli_query($GLOBALS["___mysqli_ston"], $approvalQuery) or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
while ($apprAuct = mysqli_fetch_array($getApprAucts)) {
	$query_a = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET active=0, count_in_progress=1 WHERE id='".$apprAuct['id']."'");
	$aTemp = getSqlRow("SELECT category, addlcategory FROM probid_auctions WHERE id='".$apprAuct['id']."'");
	delcatcount($aTemp['category'], $apprAuct['id']);
	delcatcount($aTemp['addlcategory'], $apprAuct['id']);	
	$query_b = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET count_in_progress=0 WHERE id='".$apprAuct['id']."'");
	
	## notification emails
	$userId = $apprAuct['ownerid'];
	$auctionId = $apprAuct['id'];
	include('mails/auctionapprovaluser.php');
	include('mails/auctionapprovaladmin.php');
}

## end procedure to suspend auctions that are awaiting approval

function auctionApproval($auctionId, $userId) {
	global $setts, $timeNowMysql;

	$auction = getSqlRow("SELECT category, addlcategory, approved, active, closed, enddate FROM probid_auctions WHERE id='".$auctionId."' AND ownerid='".$userId."'");
	$user = getSqlRow("SELECT auction_approval FROM probid_users WHERE id='".$userId."'");
	
	$approvedCats = ','.$setts['approval_categories'].',';

	$isApprovalUser = getSqlField("SELECT auction_approval FROM probid_users WHERE id='".$userId."'", "auction_approval");
	$isApprovalCats = (@eregi($auction['category'], $approvalCats) || @eregi($auction['addlcategory'], $approvalCats)) ? 'Y' : 'N';
	
	$isApproval = (@eregi('Y', $isApprovalUser) || @eregi('Y', $isApprovalCats) || @eregi('Y', $setts['enable_auctions_approval'])) ? 'Y' : 'N';

	if (@eregi('Y', $isApproval)) {
		if ($auction['approved']==0&&$auction['enddate']>$timeNowMysql&&$auction['active']==1) {
			$inactivateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET active=0 WHERE id='".$auction['id']."'");
			if ($auction['closed']==0) { 
				delcatcount($auction['category'], $auction['id']);
				delcatcount($auction['addlcategory'], $auction['id']);
			}
			## notification emails
			include('mails/auctionapprovaluser.php');
			include('mails/auctionapprovaladmin.php');
		}
	} 
}
## end procedure to suspend auctions that are awaiting approval

function offerRange($offer_range_min, $offer_range_max, $currency) {
	global $lang;
	
	if (!$offer_range_min) $offer_range_min = 0;
	if (!$offer_range_max) $offer_range_max = 0;
	
	$output = $lang[from]." ".displayAmount($offer_range_min, $currency, "YES");
	if ($offer_range_max) $output .= " ".$lang[to]." ".displayAmount($offer_range_max, $currency, "YES");
	
	return $output;
}

$currentTimeOffset = time() + getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value") * 3600;	

function sanitizeVar($value) {
	$value = @ereg_replace("[^A-Za-z0-9 ]", "", $value);

	$value = @eregi_replace('amp','and',$value);
	$value = @eregi_replace('quot','',$value);
	$value = @eregi_replace('039','',$value);
	$value = @eregi_replace(' ','-',$value);

	return $value;
}

function processLink($baseUrl, $varArray = NULL) { 
	global $setts;
	
	## Y /1,auctiondetails
	## N /auctiondetails.php?id=1
	
	(string) $output = NULL;
	
	if (@eregi('Y', $setts['is_mod_rewrite'])) {
		##$output = 'redirect_process.php/';
		while(list($key, $value) = each($varArray)) { 
			$sanitizedValue = sanitizeVar($value);
			$output .= $sanitizedValue.','.$key.',';
			##$output .= $value.',';
		}
		$output .= $baseUrl;
	} else {
		$output = $baseUrl.'.php';
		if ($varArray) {
			$output .= '?';
			while(list($key, $value) = each($varArray)) { 
				$sanitizedValue = sanitizeVar($value);
				$output .= $key.'='.$sanitizedValue.'&';		
			}
		}
		$output = substr($output,0,-1);
	}
	
	return $output;
}

##if (@eregi('Y', $setts['is_mod_rewrite']) && @eregi('rewrite', $_REQUEST['rewrite_params'])) {
if (@eregi('Y', $setts['is_mod_rewrite'])) {
	$valsArray = explode(',', $_REQUEST['rewrite_params']);
	$valsCnt = 0;
	while ($valsCnt < count($valsArray)) {
		$_REQUEST[$valsArray[$valsCnt+1]] = $valsArray[$valsCnt];
		$_GET[$valsArray[$valsCnt+1]] = $valsArray[$valsCnt];
		$_POST[$valsArray[$valsCnt+1]] = $valsArray[$valsCnt];
		$valsCnt+=2;	
	}
}

function displayNavButtons($btn = 'next') {
	global $lang, $setts;
	
	$position = getSqlField("SELECT sell_nav_position FROM probid_gen_setts","sell_nav_position");
	
	##$position = 1; ## means that previous is first, next second (new position)
	##$position = 2; ## old <=v5.24 position - next first, previous second
	
	$nextstep = '<input name="nextstep" type="submit" id="nextstep" value="'.(($btn=='next') ? $lang[nextstep] : $lang[submit]).'"> ';
	$prevstep = '<input name="prevstep" type="submit" id="prevstep" value="'.$lang[prevstep].'"> ';
	
	$output = ($position == 1) ? $prevstep.' &nbsp; '.$nextstep : $nextstep.' &nbsp; '.$prevstep;
	
	return $output;
}
## quick fix for auctions that have quantity=0 as won items
$fixQuantity = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET quant_req='1', quant_offered='1' WHERE 
quant_req='0' AND quant_offered='0' AND auctiontype!='dutch'");


//Voldemar, 14 sep 2012 - moved the function here
function get_current_lang() #TODO: rewrite this function normally
{
	global $lang;
	if ($lang[active]=="aktiivne")
	{
		return "EST";
	}
	elseif($lang[active]=="aktīvi")
	{
		return "LAT";
	}
	elseif ($lang[active]=="active")
	{
		return "ENG";
	}
	else //"активно"
	{
		return "RUS";
	}	
	
}

function show_error_message($code)
{
	global $lang;
	
	if($code==404)
	{
		$error_code=$lang['error_404'];
		$error_details=$lang['error_404_details'];
	}
	elseif($code==403)
	{
		$error_code=$lang['error_403'];
		$error_details=$lang['error_403_details'];
	}
	elseif($code==401)
	{
		$error_code=$lang['error_401'];
		$error_details=$lang['error_401_details'];
	}
	else
	{
		$error_code=$lang['error_400'];
		$error_details=$lang['error_400_details'];
	}
	echo '<div class="errormessage" style=""><h1>',$error_code,'</h1>',$error_details,'</div>';
	//echo "lol2";
//	echo $lang['buynow'];
}

?>