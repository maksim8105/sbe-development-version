<? 
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($layout['nb_feat_hp']>0) {
	header1("$lang[Featured_Auctions] [ <span class=\"sell\"><a href=\"auctions.show.php?option=featured\">$lang[view_all]</a></span> ]"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="4">
   <? 
	$numb=0;
	for ($i=0;$i<$j;$i++) { ?>
   <tr>
      <? for ($k=0;$k<$layout['nb_feat_hp'];$k++) { 
			$w=100/$layout['nb_feat_hp'];
			$width=$w."%"; ?>
      <td width="<?=$width;?>" align="center" valign="top"  class="smallfont"><? if ($name[$numb]!="") { ?>
         <table width="100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
               <td colspan="2" align="center"><a href="<?=$site_url.processLink('auctiondetails', array('itemname' => $name[$numb], 'id' => $id[$numb]));?>"><img align=center src="<? echo (($pic[$numb]!="")?"makethumb.php?pic=".$pic[$numb]."&w=".$layout['w_feat_hp']."&sq=Y":"themes/".$setts['default_theme']."/img/system/noimg.gif");?>" width="100" height="100" border="0" alt="<?=$name[$numb];?>"></a></td>
            </tr>
            <tr>
               <td colspan="2" align=center class="smallfont"><a href="<?=$site_url.processLink('auctiondetails', array('itemname' => $name[$numb], 'id' => $id[$numb]));?>"> <? echo titleResize($name[$numb]);?></a></td>
            </tr>
            <tr>
               <td class="smallfont" align="center"><b><? 
               /*echo displayAmount(getSqlField("SELECT * FROM probid_auctions WHERE id='".$id[$numb]."'","maxbid"),$currency[$numb]);*/?>
               <? $pakkumised = getSqlRow("SELECT `bidstart`, `maxbid`, `nrbids` FROM probid_auctions WHERE id='".$id[$numb]."'");
               if ($pakkumised['nrbids'] == 0) { echo displayAmount($pakkumised['bidstart'],$currency[$numb]); 
               					}
               			else { 
               					echo displayAmount($pakkumised['maxbid'],$currency[$numb]); };
               ?>
               </b></td>
            </tr>
          </table>
         <? $numb++; } ?></td>
      <? } ?>
   </tr>
   <? } ?>
</table>
<div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
<? } 
if ($layout['nb_last_auct']>0) {
	header2("$lang[Recently_Listed_Auction] [ <span class=\"sell\"><a href=\"auctions.show.php?option=recent\">$lang[view_all]</a></span> ]");	?>
<table width="100%" border="0" cellpadding="3" cellspacing="1">
   <tr class="c4" height="15">
      <td></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b>
         <?=$lang['start_date'];?>
         </b></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b>
         <?=$lang['startbid'];?>
         </b></td>
      <td class="smallfont" width="100%">&nbsp;<b>
         <?=$lang['auction_title'];?>
         <b></td>
      <td class="smallfont" nowrap>&nbsp;</td>
   </tr>
   <? $getOpenAuctions=mysql_query("SELECT id, startdate, bidstart, currency, itemname FROM probid_auctions WHERE 
	closed=0 AND active=1 AND deleted!=1 AND listin!='store' ORDER BY startdate DESC LIMIT ".$layout['nb_last_auct']."");
	while ($openAuctions = mysql_fetch_array($getOpenAuctions)) { ?>
   <tr height="15">
      <td width="11"><img src="themes/<?=$setts['default_theme'];?>/img/arr_it.gif" width="11" height="11" hspace="4"></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b><? echo displaydatetime($openAuctions['startdate'],$setts['date_format']);?></b></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<? echo displayAmount($openAuctions['bidstart'],$openAuctions['currency']);?></td>
      <td width="100%" class="<? echo setStyle($openAuctions['id']);?>"><a href="<?=$path.processLink('auctiondetails', array('itemname' => $openAuctions['itemname'], 'id' => $openAuctions['id']));?>"> <? echo titleResize($openAuctions['itemname']);?> </a></td>
      <td nowrap><? echo itemPics($openAuctions['id']);?></td>
   </tr>
   <? } ?>
</table>
<br>
<? } 
if ($layout['nb_hot_auct']>0) { 
	header3("$lang[Recently_Bid_On] [ <span class=\"sell\"><a href=\"auctions.show.php?option=highbid\">$lang[view_all]</a></span> ]"); ?>
<table border="0" cellpadding="3" cellspacing="1" width="100%">
   <tr class="c4" height="15">
      <td></td>
      <td class="smallfont">&nbsp;<b>
         <?=$lang['max_bid'];?>
         </b></td>
      <td class="smallfont">&nbsp;<b>
         <?=$lang['auction_title'];?>
         <b></td>
      <td class="smallfont">&nbsp;</td>
   </tr>
   <? 
	$getRecentAuctions=mysql_query("SELECT id, maxbid, currency, itemname FROM probid_auctions WHERE   
	closed=0 AND active=1 AND deleted!=1 AND nrbids!=0 AND listin!='store' ORDER BY maxbid DESC LIMIT ".$layout['nb_hot_auct']."");
	while ($recentAuctions=mysql_fetch_array($getRecentAuctions)) { ?>
   <tr height="15">
      <td width="11"><img src="themes/<?=$setts['default_theme'];?>/img/arr_it.gif" width="11" height="11" hspace="4"></td>
      <td class="smallfont" nowrap>&nbsp;<b> <? echo displayAmount($recentAuctions['maxbid'],$recentAuctions['currency']);?></b></td>
      <td width="100%" class="<? echo setStyle($recentAuctions['id']);?>"><a href="<?=$path.processLink('auctiondetails', array('itemname' => $recentAuctions['itemname'], 'id' => $recentAuctions['id']));?>"> <? echo titleResize($recentAuctions['itemname']);?> </a> </td>
      <td nowrap><? echo itemPics($recentAuctions['id']);?></td>
   </tr>
   <? } ?>
</table>
<br>
<? } 
if ($layout['nb_end_auct']>0) { 
	header4("$lang[Ending_Soon] [ <span class=\"sell\"><a href=\"auctions.show.php?option=ending\">$lang[view_all]</a></span> ]"); ?>
<table width="100%" border="0" cellpadding="3" cellspacing="1">
   <tr class="c4" height="15">
      <td></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b>
         <?=$lang['timeleft'];?>
         </b></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b>
         <?=$lang['currently'];?>
         </b></td>
      <td class="smallfont" width="100%">&nbsp;<b>
         <?=$lang['auction_title'];?>
         </b></td>
      <td class="smallfont" nowrap>&nbsp;</td>
   </tr>
   <? 
	$getClosingAuctions=mysql_query("SELECT id, enddate, itemname, bidstart, currency, maxbid FROM probid_auctions WHERE 
	active=1 AND closed=0 AND deleted!=1 AND listin!='store' ORDER BY enddate ASC LIMIT ".$layout['nb_end_auct']."");
	while ($closingAuctions=mysql_fetch_array($getClosingAuctions)) { ?>
   <tr height="15">
      <td width="11"><img src="themes/<?=$setts['default_theme'];?>/img/arr_it.gif" width="11" height="11" hspace="4"></td>
      <td class="smallfont" nowrap>&nbsp;<b>
         <? 
		$daysleft=timeleft($closingAuctions['enddate'],$setts['date_format']);
		echo (($daysleft>=0)?"$daysleft":"bidding closed");?>
         </b></td>
      <td nowrap class="smallfont">&nbsp;<? echo displayAmount((($closingAuctions['maxbid']>0)?$closingAuctions['maxbid']:$closingAuctions['bidstart']),$closingAuctions['currency']);?></td>
      <td width="100%" class="<? echo setStyle($closingAuctions['id']);?>">&nbsp;<a href="<?=$path.processLink('auctiondetails', array('itemname' => $closingAuctions['itemname'], 'id' => $closingAuctions['id']));?>"><? echo titleResize($closingAuctions['itemname']);?></a></td>
      <td nowrap><? echo itemPics($closingAuctions['id']);?></td>
   </tr>
   <? } ?>
</table>
<br>
<? } 
if ($layout['nb_want_ads']>0) {
	header2("$lang[Recently_Listed_Wantads] [ <span class=\"sell\"><a href=\"wanted.categories.php\">$lang[view_all]</a></span> ]");	?>
<table width="100%" border="0" cellpadding="3" cellspacing="1">
   <tr class="c4" height="15">
      <td></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b>
         <?=$lang['start_date'];?>
         </b></td>
      <td class="smallfont" width="100%">&nbsp;<b>
         <?=$lang['wanted_ad_title'];?>
         <b></td>
   </tr>
   <? $getOpenWantAds=mysql_query("SELECT id, startdate, itemname FROM probid_wanted_ads WHERE 
	closed=0 AND active=1 AND deleted!=1 ORDER BY startdate DESC LIMIT ".$layout['nb_want_ads']."");
	while ($openWa = mysql_fetch_array($getOpenWantAds)) { ?>
   <tr height="15">
      <td width="11"><img src="themes/<?=$setts['default_theme'];?>/img/arr_it.gif" width="11" height="11" hspace="4"></td>
      <td class="smallfont" nowrap="nowrap">&nbsp;<b><? echo displaydatetime($openWa['startdate'],$setts['date_format']);?></b></td>
      <td width="100%" class="<? echo setStyle($openWa['id']);?>"><a href="<?=$path;?>wanted.details.php?id=<?=$openWa['id'];?>"> <? echo titleResize($openWa['itemname']);?> </a></td>
   </tr>
   <? } ?>
</table>
<? } ?>
</td>
<td><? if ($_SESSION['membersarea']<>"Active") { ?>
   <div><a href="<?=$path_ssl;?>register.php"><img src="themes/<?=$setts['default_theme'];?>/img/tips.jpg" width="171" height="114" border="0" alt="Register"></a></div>
   <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="5"></div>
   <? } ?>
   <? 
    if ($setts['h_counter']==1) {
   	 	$nbUsers=getSqlNumber("SELECT id FROM probid_users WHERE active=1");
   	 	$nbLiveAuctions=getSqlNumber("SELECT id FROM probid_auctions WHERE active=1 AND closed=0 AND deleted!=1");
		$nbLiveWantAds=getSqlNumber("SELECT id FROM probid_wanted_ads WHERE active=1 AND closed=0 AND deleted!=1");
		/*header5("$lang[Site_status]");*/
	echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang['Site_status']."</b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table>";
    	echo "
			<table width='100%' border='0' cellpadding='2' cellspacing='1' class='contentfont'>
       		<tr> 
          		<td width='20%' align='center'><b>".$nbLiveAuctions."</b></td>
          		<td width='80%'>&nbsp;<font style='font-size: 10px;'>$lang[live_auctions]</font></td>
        	</tr>";
		if ($setts['enable_wantedads']=="Y") {	
        echo "<tr> 
          		<td width='20%' align='center'><b>".$nbLiveWantAds."</b></td>
          		<td width='80%'>&nbsp;<font style='font-size: 10px;'>$lang[live_wantads]</font></td>
        	</tr>";
		}
        echo "</table><div><img src='themes/".$setts['default_theme']."/img/pixel.gif' width='1' height='5'></div>"; }?>
   <? 
	$getNews=mysql_query("SELECT id,date,title FROM probid_news WHERE active=1 AND lang='".$_SESSION['sess_lang']."' ORDER BY date DESC LIMIT 0,".$layout['d_news_nb']."");
	$nbNews=mysql_num_rows($getNews);
	$allNews=getSqlNumber("SELECT id FROM probid_news WHERE active=1");
	if ($layout['d_news_box']==1&&$nbNews>0) { 
	  	/*header6("$lang[site_news]"); */
echo "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='FF6600'>
		<tr height='21'><td width='6'><img src='themes/v52/img/news_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b>".$lang['site_news']."</b></td>
		<td width='6'><img src='themes/v52/img/news_r.gif' width='6' height='21' border='0'></td></tr>
		</table>"; ?>
   <table width="100%" border="0" cellspacing="0" cellpadding="3">
      <tr>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      </tr>
      <? while ($newsDetails=mysql_fetch_array($getNews)) { ?>
      <tr>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/arrow.gif" width="8" height="8" hspace="4"></td>
         <td width="100%" class="smallfont"><b><? echo displaydatetime($newsDetails['date'],substr($setts['date_format'],0,7));?></b></td>
      </tr>
      <tr class="contentfont">
         <td></td>
         <td width="100%"><a href="<?=$path;?>displaynews.php?id=<?=$newsDetails['id'];?>">
            <?=$newsDetails['title'];?>
            </a> <br><br></td>
      </tr>
      <? } ?>
      <? if ($allNews>$layout['d_news_box']) { ?>
      <tr>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr class="contentfont">
         <td></td>
         <td width="100%" align="right"><a href="<?=$path;?>displaynews.php?option=all">
            <?=$lang['more'];?>
            </a> </td>
      </tr>
      <? } ?>
   </table>
     <? } ?>
  <p>&nbsp;</p>
  
	<?/*<table width="100%" border="0" cellspacing="0" cellpadding="3">
   	<tr><td width="100%" align=center><script language="Javascript" src="https://seal.godaddy.com/getSeal?sealID=19890091740293daf35127115fd5dde2617f9700376445870400848784"></script>
   	</td></tr>
   	</table><p>&nbsp;</p><br>*/?>
   	<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='0C6CBB'>
		<tr height='21'><td width='6'><img src='themes/v52/img/cat_l.gif' width='6' height='21' border='0'></td>
		<td class='mainmenu' width='100%' align='center'><b><?=$lang['reklaam']?></b></td>
		<td width='6'><img src='themes/v52/img/cat_r.gif' width='6' height='21' border='0'></td></tr>
		</table><br>
   <table width="100%" border="0" cellspacing="0" cellpadding="3">
   	<tr><td width="100%" align=center><a href="http://www.uhkekingitus.ee" target="_blank"><img src="banners/uhkekingitus.gif" width=120 height=60 alt="Uhke Kingitus" border=0></a>
   	</td></tr>
   </table><br>
   <table width="100%" border="0" cellspacing="0" cellpadding="3"
   <tr><td width="100%" align="center">
   	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" 
 			width="120" height="60" id="Navely">
			<param name="movie" value="banners/navely.swf" />
 			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<embed src="banners/navely.swf" quality="high" bgcolor="#ffffff" width="120" height="60" name="Navely" align="" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"> 
			</embed> 
	</object> 
   </td></tr>
   </table>
   <br>
   <table width="100%" border="0" cellspacing="0" cellpadding="3"
   <tr><td width="100%" align="center">
   	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" 
 			width="120" height="60" id="Engenie">
			<param name="movie" value="banners/engenie.swf" />
 			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<embed src="banners/engenie.swf" quality="high" bgcolor="#ffffff" width="120" height="60" name="Engenie" align="" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"> 
			</embed> 
	</object> 
   </td></tr>
   </table>
   <br>
   <table width="100%" border="0" cellspacing="0" cellpadding="3">
   	<tr><td width="100%" align=center><a href="http://www.sbe.ee/custompage.php?id=3"><img src="banners/reklaam120x60.gif" width=120 height=60 alt="Reklaam" border=0></a>
   	</td></tr>
   </table><br>
   <table width="100%" border="0" cellspacing="0" cellpadding="3">
   	<tr><td width="100%" align=center><a href="http://b2bestonia.com/directory/mk_-_partners.html" target="_blank"><img src="banners/b2bestonia.gif" width=120 height=60 border=0 alt="B2BEstonia"></a>
   	</td></tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="3">
   	<tr><td width="100%" align=center><p>&nbsp;</p><p><a href="http://www.hisunglasses.com/oakley-sunglasses-c-23_86.html" target="_blank" style="text-decoration=none"><font size="+1" color="#333333">Oakley Sunglasses</font></a></p>
   	</td></tr>
   </table>
   <div><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="171" height="1"></div>
