<?
## v5.25 -> jun. 26, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php"); 

$auction=getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['auctionid']."' AND ownerid='".$_SESSION['memberid']."' AND closed=0 AND active=1 AND bn='Y'");
$isAuction=getSqlNumber("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['auctionid']."' AND ownerid='".$_SESSION['memberid']."' AND closed=0 AND active=1 AND bn='Y'");

header4($auction['itemname']." - ".$lang[processoffers]);

if ($isAuction && $setts['buyout_process']==1) { ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td class="contentfont"><?
			if (@eregi('accept_offer', $_REQUEST['option'])) {
				$acceptOffer = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auction_offers SET accepted='yes' WHERE id='".$_GET['id']."' AND sellerid='".$_SESSION['memberid']."'");
				$offer = getSqlRow("SELECT * FROM probid_auction_offers WHERE id='".$_REQUEST['id']."' AND auctionid='".$auction['id']."' AND sellerid='".$_SESSION['memberid']."'");
				$auction=getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['auctionid']."' AND ownerid='".$_SESSION['memberid']."' AND closed=0 AND active=1");
				if ($auction['closed']==1 || $auction['active']==0) {
					echo $lang[auctoffererror1];
				} else if ($offer['quantity']>$auction['quantity']) {
					echo $lang[auctoffererror2];
				} else {
					$prefSeller = "N";
					if ($setts['pref_sellers']=="Y") {
						$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
						WHERE id='".$_SESSION['memberid']."'","preferred_seller");
					}
		
					if ($offer['quantity']==$auction['quantity']) {
						$closeAuction=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET closed=1,swapped=1,enddate='".$timeNowMysql."' WHERE id='".$offer['auctionid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
						delcatcount(getSqlField("SELECT category FROM probid_auctions WHERE id='".$offer['auctionid']."'","category"),$offer['auctionid']);
						delcatcount(getSqlField("SELECT addlcategory FROM probid_auctions WHERE id='".$offer['auctionid']."'","addlcategory"),$offer['auctionid']);
					}
					if ($auction['auctiontype']=="dutch") $subtractItem=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET quantity=quantity-'".$offer['quantity']."' WHERE id='".$offer['auctionid']."'");
			
					if ($fee['is_endauction_fee']=="Y"&&!freeFees($_SESSION['memberid'])) {
						// if account mode is on, then add the end auction fee to the seller's balance, and activate the details of the winner
						$amount = $offer['amount'];
						
						$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$_SESSION['memberid']."'","payment_mode");
						if ($setts['account_mode_personal']==1) {
							$account_mode_local = ($tmp) ? 2 : 1;
						} else $account_mode_local = $setts['account_mode'];
						
						if ($account_mode_local==2) {
							$newBalance=0;
							$endauctionFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE 
							fee_from<=".$amount." AND fee_to>".$amount." AND fee_type='endauction'");
			
							if ($endauctionFee['calc_type']=="percent") {
								$newBalance+=($endauctionFee['fee_amount']/100)*$amount;
							} else {
								$newBalance+=$endauctionFee['fee_amount'];
							}
	
							### if there is a dutch auction, calculate the number of sold items times end of auction amount
							
							$auctionType = getSqlField("SELECT auctiontype FROM probid_auctions WHERE id='".$auction['id']."'","auctiontype");
							if ($auctionType=="dutch") {
								$totalQuant = getSqlField("SELECT sum(quant_offered) AS total_quant FROM 
								probid_winners WHERE auctionid='".$auction['id']."'","total_quant");
								$add_desc = " x ".$totalQuant;
							} else { 
								$totalQuant = 1;
								$add_desc = "";
							}
							##$pAmount = $amount * $totalQuant;
							$invoiceAmount = $invoiceAmount * $totalQuant;
												
							$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);
							
							$payment_status="confirmed";
							$active=1;
							$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$_SESSION['memberid']."'","balance");
							$updateSellerBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
							balance=balance+".$newBalance." WHERE id='".$_SESSION['memberid']."'");
							// add end of auction fee on the invoices table 
							$currentTime = time();
							$currentBalance += $newBalance;
							$insinv = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
							(userid,auctionid,feename,feevalue,feedate,balance)	VALUES 
							('".$_SESSION['memberid']."','".$auction['id']."','End of Auction Fee.$add_desc','".$newBalance."','".$currentTime."','".$currentBalance."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					
						} else {
							$payment_status="unconfirmed";
							$active=0;
						}
					} else {
						$payment_status="confirmed";
						$active=1;
					}
				
					$saveWinner=mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_winners 
					(sellerid,buyerid,amount,auctionid,auctiontype,quant_req,quant_offered,
					active,payment_status,purchase_date) VALUES 
					('".$offer['sellerid']."','".$offer['buyerid']."','".$amount."','".$offer['auctionid']."',
					'".$auction['auctiontype']."','".$offer['quantity']."','".$offer['quantity']."',
					'".$active."','".$payment_status."','".time()."')");
					
					$winnerInsertId = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
					
					$addSale=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET items_sold=items_sold+1 WHERE id='".$_SESSION['memberid']."'");
					$addPurchase=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET items_bought=items_bought+1 WHERE id='".$offer['buyerid']."'");
						
					$sellerUsername = getSqlField("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'","username");
					$buyerUsername = getSqlField("SELECT * FROM probid_users WHERE id='".$offer['buyerid']."'","username");
					$prepareFeedbackForSeller = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
					(userid,usernick,fromid,submitted,auctionid,type) VALUES 
					('".$_SESSION['memberid']."','".$sellerUsername."','".$offer['buyerid']."',0,
					'".$offer['auctionid']."','sale')");
					$prepareFeedbackForBuyer = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_feedbacks 
					(userid,usernick,fromid,submitted,auctionid,type) VALUES 
					('".$offer['buyerid']."','".$buyerUsername."','".$_SESSION['memberid']."',0,
					'".$offer['auctionid']."','purchase')");
		
					echo "<br><div align=center>$lang[offer_accepted]</div>";
							
					$sellerId = $_SESSION['memberid'];
					$buyerId = $offer['buyerid'];
					$auctionId = $_REQUEST['auctionid'];
					$winnerId = $winnerInsertId;
						
					include ("mails/notifysellerofferaccepted.php");
					include ("mails/notifybuyerofferaccepted.php");
						
					if ($account_mode_local==2) {
						mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET emailsent=1 WHERE id='".$winnerInsertId."'");				
						include ("mails/buyerdetails.php");		
						include ("mails/sellerdetails.php");		
					}
				}
			} else { 
				$auction=getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['auctionid']."' AND ownerid='".$_SESSION['memberid']."' AND closed=0 AND active=1");
				$binAuction=FALSE;
				if ($auction['bidstart']==$auction['bnvalue']&&$auction['bn']=="Y") $binAuction=TRUE;	
				### We decide if the auction is about to start, if it didnt, then only the owner can see it.
				$notStarted=FALSE;
				if (strtotime($auction['startdate'])>time()) $notStarted=TRUE;
				if ($auction['ownerid']==$_SESSION['memberid']) $notStarted=FALSE; ?>
         <br />
         <table width="100%" border="0" cellspacing="1" cellpadding="3" class="border">
            <tr class="c1">
               <td width="35%" align="center"><?=$lang[auctiondetails];?></td>
               <td align="center"><?=$lang[offers];?></td>
            </tr>
            <tr>
               <td valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="3" class="border">
                     <tr class="c5">
                        <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                        <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                     </tr>
                     <? if (!$binAuction) { ?>
                     <tr valign="top" class="c3">
                        <td nowrap="nowrap"><b>
                           <?=$lang[currbid]?>
                           :</b></td>
                        <td><b><? echo displayAmount($auction['maxbid'],$auction['currency']);?></b></td>
                     </tr>
                     <tr valign="top" class="c2">
                        <td nowrap="nowrap"><b>
                           <?=$lang[startbid]?>
                           :</b></td>
                        <td><b><font class="redfont"><? echo displayAmount($auction['bidstart'],$auction['currency']);?></font></b></td>
                     </tr>
                     <? } ?>
                     <tr valign="top" class="c2">
                        <td><b>
                           <?=$lang[quant]?>
                           :</b></td>
                        <td><b>
                           <?=$auction['quantity'];?>
                           </b></td>
                     </tr>
                     <? if (!$binAuction) { ?>
                     <tr valign="top" class="c3">
                        <td nowrap="nowrap"><b>
                           <?=$lang[num_bids]?>
                           :</b></td>
                        <td class="contentfont"><?=$auction['nrbids'];?></td>
                     </tr>
                     <? } ?>
                     <tr valign="top" class="c2">
                        <td><b>
                           <?=$lang[timeleft]?>
                           :</b></td>
                        <td><? 
									$daysLeft = daysleft($auction['enddate'],$setts['date_format']);
									$timeLeft = timeleft($auction['enddate'],$setts['date_format']);
									echo ($daysLeft>0) ? $timeLeft : $lang[bidclosed]; ?>
                        </td>
                     </tr>
                     <tr valign="top" class="c3">
                        <td><b>
                           <?=$lang[started]?>
                           :</b></td>
                        <td><? echo displaydatetime($auction['startdate'],$setts['date_format']);?></td>
                     </tr>
                     <tr valign="top" class="c2">
                        <td><b>
                           <?=$lang[ends]?>
                           :</b></td>
                        <td><? echo displaydatetime($auction['enddate'],$setts['date_format']);?></td>
                     </tr>
                     <tr class="c3">
                        <td><b>
                           <?=$lang[status]?>
                           :</b></td>
                        <td><? echo ($auction['closed']==0)?"<font class='greenfont'><b>".$lang[open]."</b></font>":"<font class='redfont'><b>".$lang[closed]."</b></font>";?></td>
                     </tr>
                     <tr class="c5">
                        <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                     </tr>
                  </table></td>
               <td valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="3" class="border">
                     <tr class="smallfont">
                        <td><strong>Username</strong></td>
                        <? if ($auction['auctiontype']=="dutch") { ?>
                        <td align="center" width="80"><strong>Quantity</strong></td>
                        <? } ?>
                        <td align="center" width="80"><strong>Amount</strong>
									<? if ($auction['auctiontype']=="dutch") echo '<br />(per item)'; ?></td>
                        <td align="center" width="100"><strong>Accepted</strong></td>
                     </tr>
                     <tr class="c5">
                        <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                        <? if ($auction['auctiontype']=="dutch") { ?>
                        <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                        <? } ?>
                        <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                        <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1" /></td>
                     </tr>
							<? $getOffers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT o.*, u.username FROM probid_auction_offers o, probid_users u WHERE o.auctionid='".$_GET['auctionid']."' AND o.sellerid='".$_SESSION['memberid']."' AND o.buyerid=u.id") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
							$currentTime = time();
							while ($actOffer = mysqli_fetch_array($getOffers)) { ?>
                     <tr class="contentfont <? echo (($count++)%2==0) ? "c2":"c3"; ?>">
                        <td><?=$actOffer['username'];?></td>
                        <? if ($auction['auctiontype']=="dutch") { ?>
                        <td align="center"><?=$actOffer['quantity'];?></td>
								<? } ?>
                        <td align="center"><?=displayAmount($actOffer['amount'], $auction['currency']); ?></td>
                        <td align="center"><font color="<? echo (@eregi('yes',$actOffer['accepted'])) ? "green" : "red"; ?>"><?=$actOffer['accepted'];?></font> <? echo (@eregi('no',$actOffer['accepted']) && $actOffer['expires']>=0 && $auction['closed']==0 && $auction['active']==1) ? '[ <a href="auctionoffers.php?option=accept_offer&auctionid='.$_GET['auctionid'].'&id='.$actOffer['id'].'">accept</a> ]' : ''; ?></td>
                     </tr>
							<? } ?>
                  </table></td>
            </tr>
         </table>
         <? } ?>
      </td>
   </tr>
</table>
<? 	
} else { 
	echo "<p align=center><strong>$lang[offerpagecantbecreated]</strong></p>"; 
}

include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
