<? 
## v5.25 -> jul. 06, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($_SESSION['membersarea']=="Active") { 
?>

 <table width="100%" border="0" cellpadding="2" cellspacing="2"> 
  <tr class="contentfont" align="center"> 
     <td class="c4" width="20%"><?=$lang[sellstep1];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep3];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep4];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep5];?></td> 
     <td class="c1" width="20%"><?=$lang[sellstep6];?></td> 
   </tr> 
</table>
<br>
<input type="hidden" name="step" value="step3">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr>
      <td colspan="2" class="c1"><b><? echo titleResize($_REQUEST['name']);?></b></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr class="errormessage border">
      <td class="contentfont" colspan="2"><?
			if($_REQUEST['category'] > 0) {
				$croot = $_REQUEST['category'];
				$nav = "";
				$cntr = 0;
				while ($croot>0) {
					$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
							
					$crw = mysqli_fetch_array($sbcts);
					if($cntr == 0) {
						$nav = $c_lang[$crw['id']];
						$plain_email_link = $c_lang[$crw['id']];
					} else {
						if($parent != $croot) {
							$nav = $c_lang[$crw['id']]." > $nav";
							$plain_email_link = $c_lang[$crw['id']]." > ".$plain_email_link;
						}
					}
					$cntr++;
					$croot = $crw['parent'];
				}
				echo "$nav";
				echo "<input type=\"hidden\" name=\"emaillink\" value=\"".$plain_email_link."\">";
			}
			
			if($_REQUEST['addlcategory'] > 0) {
				$croot = $_REQUEST['addlcategory'];
				$nav = "";
				$cntr = 0;
				while ($croot>0) {
					$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE id='$croot'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
							
					$crw = mysqli_fetch_array($sbcts);
					if($cntr == 0) {
						$nav = $c_lang[$crw['id']];
						$plain_email_link = $c_lang[$crw['id']];
					} else {
						if($parent != $croot) {
							$nav = $c_lang[$crw['id']]." > $nav";
							$plain_email_link = $c_lang[$crw['id']]." > ".$plain_email_link;
						}
					}
					$cntr++;
					$croot = $crw['parent'];
				}
				echo "<br>$nav";
			} ?></td>
   </tr>
   <tr>
      <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr class="c3">
               <td class="contentfont"><b>
                  <?=$lang[quant]?>
                  :</b></td>
               <td class="contentfont"><b>
                  <?=$_REQUEST['quantity'];?>
                  </b></td>
            </tr>
            <tr class="c2">
               <td class="contentfont"><b>
                  <?=$lang[startbid]?>
                  :</b></td>
               <td class="contentfont"><b><? echo displayAmount($_REQUEST['startprice'],$_REQUEST['currency']);?></b> </td>
            </tr>
            <tr class="c3">
               <td class="contentfont"><b>
                  <?=$lang[duration]?>
                  :</b></td>
               <td class="contentfont"><?=$_REQUEST['duration'];?>
                  <?=$lang[days];?></td>
            </tr>
            <?
				if ($_REQUEST['starttime']=="NOW") {
					$startDate = date( "Y-m-d H:i:s", time() );
					$closed = 0;
				} else {
					$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");
					$startDate = date("Y-m-d H:i:s", 
					mktime($_REQUEST['shour']-$diff,$_REQUEST['sminute'],0,$_REQUEST['dmonth'],$_REQUEST['ddate'],$_REQUEST['dyear'])); 
					$closed = 1;
				}
		
				if ($_REQUEST['endtime']=="duration") {
					$closingdate = closingdate($startDate,$_REQUEST['duration']);
				} else {
					$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");
					$closingdate = date("Y-m-d H:i:s", 
					mktime($_REQUEST['eshour']-$diff,$_REQUEST['esminute'],0,$_REQUEST['edmonth'],$_REQUEST['eddate'],$_REQUEST['edyear'])); 
				}
				?>
            <tr class="c2">
               <td class="contentfont"><b>
                  <?=$lang[starts]?>
                  :</b></td>
               <td class="contentfont"><? echo displaydatetime($startDate,$setts['date_format']);?></td>
            </tr>
            <tr class="c3">
               <td><?=$lang[ends]?></td>
               <td><? echo displaydatetime($closingdate,$setts['date_format']);?></td>
            </tr>
            <tr class="c2">
               <td class="contentfont"><b>
                  <?=$lang[zip]?>
                  :</b></td>
               <td class="contentfont"><?=$_REQUEST['zip'];?>
               </td>
            </tr>
            <tr class="c3">
               <td class="contentfont"><b>
                  <?=$lang[country]?>
                  :</b></td>
               <td class="contentfont"><?=$_REQUEST['country'];?>
               </td>
            </tr>
				<? if ($_REQUEST['buynow']=="Y") { ?>
				<? if ($setts['buyout_process']==0) { ?>
            <tr class="c2">
               <td class="contentfont" colspan="2"><? 
						$bnvalue=displayAmount($_REQUEST['bnprice'],$_REQUEST['currency']);
						echo "<b>".(($_REQUEST['buynow']=="Y")?"<img src=themes/".$setts['default_theme']."/img/system/buyitnow.gif border=0><br> $lang[bynow_for] ".$bnvalue."":"")."</b>";
		  				?></td>
            </tr>
				<? } else if ($setts['buyout_process']==1) { ?>
            <tr class="c2">
               <td class="contentfont"><b>
                  <?=$lang[makeoffer]?>
                  :</b></td>
               <td class="contentfont">[ <?=$lang[enabled];?> ] <?=$lang[offerrange];?> : <?=offerRange($_REQUEST['offer_range_min'], $_REQUEST['offer_range_max'], $_REQUEST['currency']);?>
               </td>
            </tr>
				<? } ?>
				<? } ?>
            <tr class="c2">
               <td class="contentfont" nowrap="nowrap"><? if ($_REQUEST['respr']=="Y") { echo "<b>$lang[resprice]:</b>"; } ?>
               </td>
               <td><? if ($_REQUEST['respr']=="Y") { echo displayAmount($_REQUEST['resprice'],$_REQUEST['currency']); } ?></td>
            </tr>
         </table></td>
      <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr valign="top" class="c3">
               <td class="contentfont"><b>
                  <?=ucwords($lang[payment])?>
                  :</b></td>
               <td class="c4"><?
						$pm_methods=explode("<br>",$_REQUEST['pm']);
						$nb_pm_methods=count($pm_methods);
						?>
                  <table border="0" cellspacing="2" cellpadding="2">
                     <? for ($i=0;$i<$nb_pm_methods;$i+=2) { 
								$j=$i+1; ?>
                     <tr class="contentfont ">
                        <td width="120" class="c3"><b>
                           <?=$pm_methods[$i];?>
                           </b></font></td>
                        <td width="120" class="c3"><b>
                           <?=$pm_methods[$j];?>
                           </b></font></td>
                     </tr>
                     <? } ?>
                  </table></td>
            </tr>
            <tr valign="top" class="c2">
               <td class="contentfont"><b>
                  <?=ucwords($lang[shipping])?>
                  :</b></td>
               <td class="contentfont"><?
						if ($_REQUEST['shipcond']=="BP") echo "$lang[buyerpaysshipment]";
						else echo "$lang[sellerpaysshipment]";
						if ($_REQUEST['shipint']=="Y") echo "<br>$lang[sellershipinternat]"; ?>
               </td>
            </tr>
            <? if ($setts['shipping_costs']==1) { ?>
            <tr valign="top" class="c2">
               <td class="contentfont"><b>
                  <?=$lang[postagecosts]?>
                  :</b></td>
               <td class="contentfont"><?=displayAmount($_REQUEST['postage_costs'],$_REQUEST['currency'],"YES");?></td>
            </tr>
            <tr valign="top" class="c2">
               <td class="contentfont"><b>
                  <?=$lang[insurance]?>
                  :</b></td>
               <td class="contentfont"><?=displayAmount($_REQUEST['insurance'],$_REQUEST['currency'],"YES");?>
               </td>
            </tr>
            <tr valign="top" class="c2">
               <td class="contentfont"><b>
                  <?=$lang[servicetype]?>
                  :</b></td>
               <td class="contentfont"><?=$_REQUEST['type_service'];?>
               </td>
            </tr>
            <? if ($_REQUEST['shipping_details']!="") { ?>
            <tr valign="top" class="c2">
               <td class="contentfont"><b>
                  <?=$lang[shipping_details]?>
                  :</b></td>
               <td class="contentfont"><?=remSpecialChars($_REQUEST['shipping_details']);?>
               </td>
            </tr>
            <? } ?>
            <? } ?>
            <tr valign="top" class="c3">
               <td class="contentfont"><b>
                  <?=$lang[featuring]?>
                  :</b></td>
               <td class="contentfont"><? 
						if ($_REQUEST['hpfeat']=="Y") echo "<li>$lang[sell_homepage]</li>";
						if ($_REQUEST['catfeat']=="Y") echo "<li>$lang[sell_category]</li>";
						if ($_REQUEST['bolditem']=="Y") echo "<li>$lang[sell_bold]</li>";
						if ($_REQUEST['hlitem']=="Y") echo "<li>$lang[sell_highlighted]</li>";
						if ($_REQUEST['hlitem']!="Y"&&$_REQUEST['bolditem']!="Y"&&$_REQUEST['catfeat']!="Y"&&$_REQUEST['hpfeat']!="Y") echo $lang[none]; ?>
               </td>
            </tr>
         </table></td>
   </tr>
   <tr>
      <td colspan="2" class="c1"><b>
         <?=strtoupper($lang[descr])?>
         </b></td>
   </tr>
   <tr class="c5">
      <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <tr>
      <td colspan="2"><? echo addSpecialChars($_REQUEST['description']);?> </td>
   </tr>
   <tr>
      <td align="center" class="contentfont" colspan="2"><? 
			if ($_REQUEST['mainpic']!="") echo "<img src=\"makethumb.php?pic=".$_REQUEST['mainpic']."&w=400\"><br>";  
				for ($i=0;$i<$_REQUEST['cnt1'];$i++) { 
					echo "<img src=\"makethumb.php?pic=".$_REQUEST['the_pic'][$i]."&w=100\">&nbsp;";
				} 
			?></td>
   </tr>
   <? if (!empty($_REQUEST['videofile_path'])) { ?>
   <tr>
      <td colspan="2" class="c1"><b>
         <?=strtoupper($lang[movie])?>
         </b></td>
   </tr>
   <tr class="c5">
      <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <?
	$video_name_raw = $_REQUEST['videofile_path'];
	$video_name = explode(".",$video_name_raw);
	$video_file_cnt = count($video_name) - 1;
	$video_extension = $video_name[$video_file_cnt];
	##$base_path = substr($_SERVER['SCRIPT_FILENAME'],0,count($_SERVER['SCRIPT_FILENAME'])-19);

	if (trim($video_extension)=="mov") {
		## run quicktime
		$display_video = '<OBJECT id="QTPlay" 
			CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" 
			CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab" 
			align="baseline" border="0"
			standby="Loading Video Player components..." 
			type="application/x-oleobject" WIDTH="480" HEIGHT="360" > 
        	<PARAM NAME="src" VALUE="http://movies.apple.com/movies/qt_posters/qtstart5a_480x228.mov"> 
	        <PARAM NAME="controller" VALUE="false"> 
	        <PARAM NAME="target" VALUE="myself"> 
	        <PARAM NAME="href" VALUE="'.$video_name_raw.'"> 
	        <PARAM NAME="pluginspage" VALUE="http://www.apple.com/quicktime/download/"> 
		</OBJECT> ';
	} else {
		## run windows media player
		$display_video = '<object data="'.$video_name_raw.'" type="video/x-ms-wmv" 
       width="320" height="320">
       <param name="src" value="'.$video_name_raw.'">
  			<param name="ShowControls" value="1"> 
			<param name="ShowPositionControls" value="0"> 
			<param name="ShowAudioControls" value="1"> 
			<param name="ShowTracker" value="1"> 
			<param name="ShowDisplay" value="0"> 
			<param name="ShowStatusBar" value="1"> 
			<param name="AutoSize" value="0"> 
			<param name="ShowGotoBar" value="0"> 
			<param name="ShowCaptioning" value="0"> 
			<param name="AutoStart" value="0"> 
			<param name="AnimationAtStart" value="false"> 
			<param name="TransparentAtStart" value="false"> 
			<param name="AllowScan" value="1"> 
			<param name="EnableContextMenu" value="1"> 
			<param name="ClickToPlay" value="0"> 
			<param name="InvokeURLs" value="1"> 
			<param name="DefaultFrame" value="datawindow"> 
      </object>';
	} ?>
   <tr>
      <td align="center" class="contentfont" colspan="2"><? 
			echo $display_video."<br><br>".$lang[clicktoplayvideo];	?></td>
   </tr>
   <? } ?>
   <?
	$mainCat_primary = getMainCat($_REQUEST['category']);
	$mainCat_secondary = getMainCat($_REQUEST['addlcategory']);
	  
	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype FROM probid_fields 
	WHERE (categoryid='".$mainCat_primary."' OR categoryid='".$mainCat_secondary."' OR categoryid='0') 
	ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
	$isFields = mysqli_num_rows($getFields);
	if ($isFields) { ?>
   <tr class="c1">
      <td colspan="2"><?=strtoupper($lang[addfields])?></td>
   </tr>
   <tr class="c5">
      <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? while ($fields=mysqli_fetch_array($getFields)) { $toDisp = ""; ?>
   <tr class="<? echo (($count++)%2==0)?"c3":"c2"; ?>">
      <td align="right" width="50%"><strong>
         <?=$fields['boxname'];?>
         </strong></td>
      <td><?
			if ($fields['boxtype']=="checkbox") {
				for ($i=0; $i<count($_REQUEST['box'.$fields['boxid']]); $i++) { 
					$toDisp .= getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$_REQUEST['box'.$fields['boxid']][$i]."'","boxcaption")."&nbsp; &nbsp;";
				}	
			} else if ($fields['boxtype']=="radio") {
				$toDisp = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$_REQUEST['box'.$fields['boxid']]."'","boxcaption")." ";
			} else if ($fields['boxtype']=="list") {
				$toDisp = getSqlField("SELECT boxcaption FROM probid_fields WHERE boxid='".$fields['boxid']."' AND boxvalue='".$_REQUEST['box'.$fields['boxid']]."'","boxcaption")." ";
			} else {
				$toDisp .= remSpecialChars($_REQUEST['box'.$fields['boxid']]);
			}
			echo $toDisp;
			?></td>
   </tr>
   <? } 
	} ?>
	<? if (count($_REQUEST['dirpay'])) { ?>
	<!-- Header Direct Payment -->
   <tr>
      <td class="c1" colspan="2"><?=strtoupper($lang[m_dir_pay_block]);?></td>
   </tr>
   <tr class="c5">
      <td colspan="2"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
	<tr valign="top">
		<td class="c2" colspan="2"><?
			foreach($_REQUEST['dirpay'] as $v)	{
				if ($v) echo '<img src="themes/'.$setts[default_theme].'/img/system/pay_system_'.$v.'.gif">';
			} ?>
		</td>
	</tr>
	<? } ?>
   <tr>
      <td class="errormessage" align="center" colspan="2"><?=$lang[dontrefrest_note];?></td>
   </tr>
   <tr class="c4">
      <td class="contentfont" align="center" colspan="2"><? echo displayNavButtons('submit'); ?></td>
   </tr>
</table>
<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?>