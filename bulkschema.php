<? 
## v4.03 -> oct. 12, 2004
include_once ("config/config.php");
include_once ("config/lang/$setts[default_lang]/site.lang");
?>
<HTML>
<HEAD>
<TITLE>
<?=$lang[bulkschema]?>
</TITLE>
<link href="style.css" rel="stylesheet" type="text/css">
</HEAD>
<script language="JavaScript">
function Openimage2(url) {
        openimage2 = open(url,"2openimage","width=600,height=600,resizable=0,locationbar=0,menubar=0,scrollbars=1,status=1,toolbar=0,left=180,top=20");
}
</script>
<BODY BGCOLOR="white"> 
<? include ("style.php"); ?> 
<TABLE WIDTH="100%" CELLSPACING="0" BORDER="0" CELLPADDING="4" class="contentfont"> 
  <TR BGCOLOR="#eeeeee"> 
    <TD WIDTH="100%"><B> 
      <?=$lang[bulkuploadschema]?> 
      </B></TD> 
  </TR> 
  <TR> 
    <TD WIDTH="100%"><?=$lang[bulkuploadschematext]?></TD> 
  </TR> 
  <TR ALIGN="center"> 
    <TD WIDTH="100%"> <TABLE WIDTH="100%" CELLSPACING="1" BORDER="0" CELLPADDING="2" class="contentfont"> 
        <TR BGCOLOR="#dddddd"> 
          <TD WIDTH="30%"><B> 
            <?=$lang[field]?> 
            </B></TD> 
          <TD WIDTH="70%"><B> 
            <?=$lang[value]?> 
            </B></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD WIDTH="30%" BGCOLOR="#eeeeee"><?=$lang[itemname]?></TD> 
          <TD WIDTH="70%" BGCOLOR="#eeeeee"><?=$lang[itemnametext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD WIDTH="30%" BGCOLOR="#eeeeee"><?=$lang[itemdescr]?></TD> 
          <TD WIDTH="70%" BGCOLOR="#eeeeee"><?=$lang[itemdescrtext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[quant]?></TD> 
          <TD BGCOLOR="#eeeeee"><?=$lang[quanttext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD WIDTH="30%" BGCOLOR="#eeeeee"><?=$lang[auctype]?></TD> 
          <TD WIDTH="70%" BGCOLOR="#eeeeee"><?=$lang[auctypetext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD WIDTH="30%" BGCOLOR="#eeeeee"><?=$lang[bidstartprice]?></TD> 
          <TD WIDTH="70%" BGCOLOR="#eeeeee"><?=$lang[bidstartpricetext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[duration]?></TD> 
          <TD BGCOLOR="#eeeeee"> <?
		  	$getAuctionDurations = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations");
		  	while ($auctionDuration = mysqli_fetch_array($getAuctionDurations)) {
		  		echo "<i>$auctionDuration[days]</i> $lang[means] <b>".$auctionDuration['description']."</b><br>";
		  	} ?> </TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[country]?></TD> 
          <TD BGCOLOR="#eeeeee"><?=$lang[countrytext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[zip]?></TD> 
          <TD BGCOLOR="#eeeeee"><?=$lang[ziptext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[shippingcond]?></TD> 
          <TD BGCOLOR="#eeeeee"><?=$lang[shippingcondtext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD WIDTH="30%" BGCOLOR="#eeeeee"><?=$lang[cat]?></TD> 
          <TD WIDTH="70%" BGCOLOR="#eeeeee"><?=$lang[cattext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[resprice]?> </TD> 
          <TD BGCOLOR="#eeeeee"><?=$lang[respricetext]?></TD> 
        </TR> 
        <TR VALIGN="top"> 
          <TD BGCOLOR="#eeeeee"><?=$lang[buynowprice]?> </TD> 
          <TD BGCOLOR="#eeeeee"><?=$lang[buynowpricetext]?></TD> 
        </TR> 
      </TABLE></TD> 
  </TR> 
  <TR ALIGN="center"> 
    <TD WIDTH="100%"><BR> 
      <BR> 
      <A HREF="Javascript:window.close()"> 
      <?=$lang[close]?> 
      </A></TD> 
  </TR> 
</TABLE> 
</BODY>
</HTML>
