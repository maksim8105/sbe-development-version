﻿<? 
## v5.25 -> jun. 08, 2006
if ($_SESSION['membersarea']=="Active"&&$_SESSION['is_seller']=="Y"&&$setts['stores_enabled']=="Y") {

## ADD AUTHENTICATION - ALEKSEI

$userDetails = getSqlRow("SELECT `authenticated`, `isikukood`, `realname` FROM probid_users WHERE id='".$_SESSION['memberid']."'");
if ( 'Y' == strtoupper( $userDetails['authenticated'] ) )
{
	// Я авторизованный пользователь


if ( !defined('INCLUDED') ) { die("Access Denied"); }

$noDisplay = FALSE;

$prefSeller = "N";
if ($setts['pref_sellers']=="Y") {
	$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
	WHERE id='".$_SESSION['memberid']."'","preferred_seller");
	$reduction = (100-$setts['pref_sellers_reduction'])/100;
	$reduction = ($prefSeller=="Y") ? $reduction : 1;
} else $reduction = 1;

$vatExempted = "Y";
if ($setts['vat_rate']>0) $vatExempted = getSqlField("SELECT vat_exempted FROM probid_users
WHERE id='".$_SESSION['memberid']."'","vat_exempted");

include_once("formchecker.php"); 

if ($action=="store_save") {
	$updateAboutSetts = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	isaboutme='".$_POST['is_aboutme']."',aboutpage_type='".$_POST['aboutpage_type']."',store_template=0  ,
	shop_mainpage='".$_POST['shop_mainpage']."',shop_mainpage_preview='',
	aboutmepage='".remSpecialChars($_POST['content'])."',
	store_name='".remSpecialChars($_POST['store_name'])."', 
	allow_stat='".$_POST['allowstats']."', 
	store_metatags='".remSpecialChars($_POST['store_metatags'])."'    
	WHERE id='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

	$fileExtension = getFileExtension($_FILES['logo']['name']);
	$tempNumber = md5(uniqid(rand(2, 999999999)));
	$logoName = @eregi_replace(" ","",$_SESSION['memberusern'])."_logo".$tempNumber.".".$fileExtension;
	## unlink cache if exists
	
	$isUpload = uploadFile($_FILES['logo']['tmp_name'],$logoName,"uplimg/",TRUE);
	if ($isUpload) {
		$updlogo = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		shop_logo='uplimg/".$logoName."' WHERE id='".$_SESSION['memberid']."'");
	}

	### if there is no shop activation fee, then activate the store. otherwise the activate store button will have
	### to be clicked
	$currentSubscription = getSqlField("SELECT store_account_type FROM probid_users WHERE id='".$_SESSION['memberid']."'","store_account_type");
	
	$recurring = getSqlField("SELECT store_recurring FROM probid_fees_tiers WHERE id='".$_POST['store_account_type']."'","store_recurring");
	$store_nextpayment = 0;
	if ($recurring>0) $store_nextpayment = time() + ($recurring*24*3600);
	
	if ($fee['is_store_fee']=="Y"&&$_POST['store_account_type']!=$currentSubscription&&!freeFees($_SESSION['memberid'])&&$_POST['aboutpage_type']==2) {
		$currentTime = time();
		$setupStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active=0, 
		store_lastpayment='".$currentTime."', store_account_type='".$_POST['store_account_type']."', store_nextpayment='".$store_nextpayment."' WHERE id = '".$_SESSION['memberid']."'");
		storeSetupFee($_SESSION['memberid'],$_POST['store_account_type']);
		$noDisplay = TRUE;
	} else if ($fee['is_store_fee']=="Y"&&$_POST['store_account_type']!=$currentSubscription&&$isActive==0&&freeFees($_SESSION['memberid'])&&$_POST['aboutpage_type']==2) {
		echo "<br><br><span class=contentfont>".$lang[updatesuccessful].".</span><br>&nbsp;";
		$currentTime = time();
		$activateStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active=1, 
		store_lastpayment='".$currentTime."', store_account_type='".$_POST['store_account_type']."', store_nextpayment='".$store_nextpayment."' WHERE id = '".$_SESSION['memberid']."'");
	} else if ($_POST['aboutpage_type']==2&&$_POST['store_account_type']!=$currentSubscription) {
		$currentTime = time();
		$activateStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active=1, 
		store_lastpayment='".$currentTime."', store_account_type='".$_POST['store_account_type']."', store_nextpayment='".$store_nextpayment."' WHERE id = '".$_SESSION['memberid']."'");
		echo "<br><br><span class=contentfont>".$lang[updatesuccessful].".</span><br>&nbsp;";
	}
}

if ($_GET['option']=="activate_store") {
	$isActive = getSqlField("SELECT store_active FROM probid_users WHERE id='".$_SESSION['memberid']."'","store_active");
	
	$recurring = getSqlField("SELECT store_recurring FROM probid_fees_tiers WHERE id='".$_POST['store_account_type']."'","store_recurring");
	$store_nextpayment = 0;
	if ($recurring>0) $store_nextpayment = time() + ($recurring*24*3600);

	$currentTime = time();
	$setupStore = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active=0, 
	store_lastpayment='".$currentTime."', store_account_type='".$_POST['store_account_type']."', store_nextpayment='".$store_nextpayment."' WHERE id = '".$_SESSION['memberid']."'");

	storeSetupFee($_SESSION['memberid'],$_GET['store_account_type']);
	$noDisplay = TRUE;	
}

if (isset($_POST['createok'])) {
	$result = mkdir ("shops/".$_SESSION['memberusern'],0755);
	### create index page
	$fp = fopen("shops/".$_SESSION['memberusern']."/index.php","w");
	$content = "<?																						\n";
	$content.= "header (\"Location: ".$setts['siteurl']."aboutme.php?userid=".$_SESSION['memberid']."\")\n";
	$content.= "?>"; 
	fputs($fp,$content); 
	fclose($fp); 
} 

if (isset($_POST['removeok'])) { 
	unlink ("shops/".$_SESSION['memberusern']."/index.php"); 
	$result = rmdir ("shops/".$_SESSION['memberusern']); 
} 

$shopDetails=getSqlRow("SELECT * FROM probid_users WHERE id='".$_SESSION['memberid']."'"); 

if ($_REQUEST['store_template']) $ostoretemplate=$_REQUEST['store_template'];
else $ostoretemplate=$shopDetails['store_template'];
		
if (!$noDisplay) {
	$shopEdit['store_name'] = (trim($_POST['store_name']) != '') ? $_POST['store_name'] : $shopDetails['store_name'];
	$shopEdit['content'] = (trim($_POST['content']) != '') ? $_POST['content'] : $shopDetails['aboutmepage'];
	$shopEdit['metatags'] = (trim($_POST['store_metatags']) != '') ? $_POST['store_metatags'] : $shopDetails['store_metatags'];
	
?>
<form action="membersarea.php?page=store" method="post" enctype="multipart/form-data">
<input type="hidden" name="oldlogo" value="<?=$shopDetails['shop_logo'];?>"> 
 <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border">
     <? $storeInfo = storeAccountType($shopDetails['store_account_type']); ?>
      <tr class="c2">
         <td colspan="2" class="contentfont"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="errormessage">
               <tr>
                  <td class="contentfont"><a href="membersarea.php?page=store">
                     <?=$lang[storemainsetts]?></a> | <a href="membersarea.php?page=store_pages">
                     <?=$lang[storepages]?></a> | <a href="membersarea.php?page=cats_management">
                     <?=$lang[catsmanagement]?></a></td>
               </tr>
            </table>
            <? $articlesInStore = getSqlNumber("SELECT id FROM probid_auctions WHERE listin!='auction' AND active='1' AND deleted!='1' AND closed='0' AND ownerid='".$_SESSION['memberid']."'"); ?>
            <table border="0" cellspacing="2" cellpadding="3" width="100%" class="border c4">
               <tr>
                  <td colspan="3" class="c7"><strong>&raquo;&nbsp;
                     <?=$lang[subscription_details]?>
                     </strong></td>
               </tr>
               <tr>
                  <td colspan="3" class="c5"></td>
               </tr>
               <? $timeOffset = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value") * 3600; ?>
               <tr class="c2">
                  <td width="33%"><strong>
                     <?=$lang[store_status]?>
                     </strong>: <? echo ($shopDetails['aboutpage_type']==2&&$shopDetails['store_active']==1) ? "<img src='themes/".$setts['default_theme']."/img/system/active.gif' hspace='4'><span class=greenfont>$lang[active]</span>" : "<img src='themes/".$setts['default_theme']."/img/system/inactive.gif' hspace='4'><span class=redfont>".$lang[inactive]."</span>"; ?></td>
                  <td width="33%"><strong>
                     <?=$lang[last_payment];?>
                     </strong>: <? echo ($shopDetails['store_lastpayment']>0) ? date($setts['date_format'],($shopDetails['store_lastpayment']+$timeOffset)) : $lang[na]; ?></td>
                  <td width="33%"><strong>
                     <?=$lang[next_payment];?>
                     </strong>: <? echo ($shopDetails['store_nextpayment']>0) ? date($setts['date_format'],($shopDetails['store_nextpayment']+$timeOffset)) : $lang[na]; ?></td>
               </tr>
               <? if ($shopDetails['aboutpage_type']==2&&$shopDetails['store_active']==1) { ?>
               <tr class="c3">
                  <td><strong>
                     <?=$lang[store_acc_type]?>
                     </strong>:
                     <? 
							if ($storeInfo['store_nb_items']==0&&$storeInfo['store_valid']==1) echo $lang[unlimited]; 
							else if ($storeInfo['store_nb_items']==0&&$storeInfo['store_valid']==0) echo $lang[invalid_acc_type]; 
							else {
								echo "<strong>".$storeInfo['store_name']."</strong>, ".$storeInfo['store_nb_items']." $lang[items]<br>
								$lang[price]: ".displayAmount($storeInfo['fee_amount'],$setts['currency'],'YES').", ".(($storeInfo['store_recurring']>0)? "$lang[recurring_every] ".$storeInfo['store_recurring']." $lang[days]":"$lang[one_time_fee]");
							}
							?></td>
                  <td><strong>
                     <?=$lang[total_articles_in_store];?>
                     </strong>:
                     <?
							if ($storeInfo['store_nb_items']==0&&$storeInfo['store_valid']==1) echo $lang[unlimited]; 
							else if ($storeInfo['store_nb_items']==0&&$storeInfo['store_valid']==0) echo $lang[na]; 
							else echo $articlesInStore; ?></td>
                  <td><strong>
                     <?=$lang[remaining_articles_in_store];?>
                     </strong>:
                     <?
							if ($storeInfo['store_nb_items']==0&&$storeInfo['store_valid']==1) echo $lang[unlimited]; 
							else if ($storeInfo['store_nb_items']==0&&$storeInfo['store_valid']==0) echo $lang[na]; 
							else echo ($storeInfo['store_nb_items'] - $articlesInStore); ?></td>
               </tr>
               <? } ?>
            </table></td>
      </tr>
      <?
		$nbShopItems = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."' AND active=1 AND deleted!=1 AND listin!='auction'");
		?>
      <tr class="c3">
         <td width="150" align="right" class="contentfont"><b>
            <?=$lang[enable_store]?>
            </b></td>
         <td class="contentfont"><input type="radio" name="aboutpage_type" value="2" <? echo (($shopDetails['aboutpage_type']==2)?"checked":"");?>>
            <?=$lang[yes]?>
            <input type="radio" name="aboutpage_type" value="" <? echo (($shopDetails['aboutpage_type']!=2)?"checked":"");?> <? echo ($nbShopItems) ? 'onclick="return alert(\'Warning, you still have items in your store!\');"' : ''; ?>>
            <?=$lang[no]?></td>
      </tr>
      <tr class="c3">
         <td width="150" align="right" class="contentfont"><b>
            Teen e-poe statistikat avalikuks
            </b></td>
         <td class="contentfont"><input type="radio" name="allowstats" value="1" <? echo (($shopDetails['aboutpage_type']==2)?"checked":"");?>>
            <?=$lang[yes]?>
            <input type="radio" name="allowstats" value="" <? echo (($shopDetails['aboutpage_type']!=2)?"checked":"");?> <? echo ($nbShopItems) ? 'onclick="return alert(\'Warning, you still have items in your store!\');"' : ''; ?>>
            <?=$lang[no]?></td>
      </tr>
      <tr class="c2">
         <td width="150"></td>
         <td class="contentfont"><?=$lang[store_desc_msg]?></td>
      </tr>
      <?
		$nbStoreAccTypes = getSqlNumber("SELECT id FROM probid_fees_tiers WHERE fee_type='store'"); 
		if ($fee['is_store_fee'] == "Y" && $nbStoreAccTypes > 0) { ?>
      <tr class="c3">
         <td width="150" align="right" class="contentfont" valign="top"><b>
            <?=$lang[choose_account_type]?>
            </b></td>
         <td class="contentfont"><? 
				$getStoreTypes = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='store'"); 
				while ($storeTypes = mysqli_fetch_array($getStoreTypes)) { ?>
            <input type="radio" name="store_account_type" value="<?=$storeTypes['id'];?>" <? echo (($shopDetails['store_account_type']==$storeTypes['id'])?"checked":"");?>>
            <? 
			echo "<strong>".$storeTypes['store_name']."</strong>, ".$storeTypes['store_nb_items']." $lang[items]; $lang[price]: ".displayAmount($storeTypes['fee_amount'],$setts['currency'],'YES').", ".(($storeTypes['store_recurring']>0)? "$lang[recurring_every] ".$storeTypes['store_recurring']." $lang[days]":"$lang[one_time_fee]");
			echo "<br>";
		} ?></td>
      </tr>
      <? } ?>
      <? if (!@eregi('Y', $setts['is_mod_rewrite'])) { ?>
      <tr class="c3">
         <td align="right" class="contentfont"><b>
            <?=$lang[store_url]?>
            </b></td>
         <td class="contentfont"><?
				if (ini_get('safe_mode')) {
					echo "$lang[shop_safemodewarning]".
					$setts['siteurl']."aboutme.php?userid=".$_SESSION['memberid'];
				} else {
					if (!is_dir("shops/".$_SESSION['memberusern']))
							echo "<strong>".$setts['siteurl']."shops/".$_SESSION['memberusern']."</strong> $lang[notexist] &nbsp;
						<input type=\"submit\" name=\"createok\" value=\"$lang[create] $lang[store_url] \">";
						else 
							echo "<strong>".$setts[siteurl]."shops/".$_SESSION['memberusern']."</strong> exists &nbsp;
						<input type=\"submit\" name=\"removeok\" value=\"$lang[remove] $lang[store_url] \">";
				}
				?></td>
      </tr>
      <? } ?>
      <tr class="c2">
         <td align="right" class="contentfont"><b>
            <?=$lang[store_name]?>
            </b></td>
         <td class="contentfont"><input name="store_name" type="text" class="contentfont" id="store_name" value="<?=$shopEdit['store_name'];?>" size="50" maxlength="50"></td>
      </tr>
      <tr class="c3">
         <td align="right" valign="top" class="contentfont"><b>
            <?=$lang[store_desc]?>
            </b></td>
         <td class="contentfont"><textarea name="content" cols="45" rows="10" id="content"><? echo addSpecialChars($shopEdit['content']);?></textarea>
            <script> 
					var oEdit1 = new InnovaEditor("oEdit1");
					oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit1.height=300;
					oEdit1.REPLACE("content");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top" class="contentfont"><?=$lang[store_metatags]?>
         </td>
         <td class="contentfont"><textarea name="store_metatags" cols="45" rows="6" id="store_metatags"><? echo addSpecialChars($shopEdit['metatags']);?></textarea>
            <br />
            <?=$lang[storemetatagsnote];?></td>
      </tr>
      <tr class="c3">
         <td align="right" class="contentfont"><b>
            <?=$lang[upload_store_logo]?>
            </b></td>
         <td class="contentfont"><input name="logo" type="file" id="logo" size="40"></td>
      </tr>
      <? if ($shopDetails['shop_logo']!="") { ?>
      <tr class="c3">
         <td align="right" valign="top" class="contentfont"><b>
            <?=$lang[current_store_logo];?>
            </b></td>
         <td class="contentfont"><img src="<?=$shopDetails['shop_logo'];?>?<? echo rand(1,99999);?>" border="0"></td>
      </tr>
      <? } ?>
      
      <tr class="c4">
         <td colspan="2" align="center" class="contentfont"><input type="submit" name="storesaveok" value="<?=$lang[savesetts]?>"></td>
      </tr>
  </table>
</form>
<br>
<? 
	} ## end of noDisplay check

	## AUTHENTICATION CONTINUE - ALEKSEI	
	}
else
{
	// Я неавторизованный пользователь
	include 'pangalink/authenticate.php';
} 
 	## END OF AUTHENTICATION - ALEKSEI
 	
} else if ($_SESSION['is_seller']!="Y") {
	echo $lang[seller_error]; 
} else { 
	echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; 
} ?>
