<?
## v5.22 -> oct. 06, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

if ($_SESSION['adminlevel'] >1) {
include_once ('../config/config.php');
include_once ('header.php');
echo '<table width="100%" bgcolor="red"><tr><td align="center"><font size="+2" color="#ffffff">Sorry, You have not a high enough admin priviledge level to access this page!!</font>';
echo '<br><br><font color="#FFFFFF">redirecting...<br><br>Please click <a href=index.php>here</a><br>if the page does not refresh automatically<br><br></font>';
echo '<script>window.setTimeout("changeurl();",700); function changeurl(){window.location="index.php"}</script></td></tr></table>';
include_once ('footer.php');
exit;
}

include ("../config/config.php");

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[USER_ADMIN];?>&nbsp;&nbsp;</td>
    <td><img src="images/end_part.gif"></td>
  </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
  <form action="adminmanagement.php" method="post">
    <input type="hidden" name="id" value="<?=$_GET['id'];?>">
    <tr class="c3">
      <td align="center" colspan="2"><b>
        <?=$a_lang[ADMINEDIT_TITLE];?>
        </b></td>
    </tr>
    <? $row = getSqlRow("SELECT * FROM probid_admins WHERE id='".$_GET['id']."'"); ?>
    <tr class="c1">
      <td width="50%" align="right"><?=$a_lang[USERNAME];?>
        : </td>
      <td><input name="username" type="text" id="username" value="<?=$row['username'];?>" size="15"></td>
    </tr>
    <tr class="c2">
      <td align="right"><?=$a_lang[CREATED];?>
        :</td>
      <td><? echo date("M j,Y",strtotime($row['created']));?></td>
    </tr>
    <tr class="c1">
      <td align="right"><?=$a_lang[LASTLOGIN];?>
        :</td>
      <td><? if ($row['lastlogin']>0) { echo date("M j,Y h:i:s a",strtotime($row['lastlogin'])); } else { echo "n/a"; } ?>
      </td>
    </tr>
    <tr class="c2">
      <td align="right"><?=$a_lang[ADMINEDIT6];?>
        :</td>
      <td><input name="oldpass" type="password" id="oldpass"></td>
    </tr>
    <tr class="c1">
      <td align="right"><?=$a_lang[ADMINEDIT7];?>
        :</td>
      <td><input name="newpass" type="password" id="newpass"></td>
    </tr>
    <tr class="c2">
      <td align="right"><?=$a_lang[ADMINEDIT8];?>
        :</td>
      <td><input name="newpassconfirm" type="password" id="newpassconfirm"></td>
    </tr>
    <tr class="c1">
      <td align="right"><?=$a_lang[ADMIN_LVL];?></td>
      <td><select name="level"><? 
	  	for ($i=1; $i < 3; $i++) { 
			echo "<option value=\"$i\"".(($row['level']==$i) ? " selected=\"selected\"" : "").">".$i."</option>\n";
		} ?></select><br /><?=$a_lang[ADMIN_LVLS_EXPL];?>
      </td>
    </tr>
    <tr class="c3">
      <td align="center" colspan="2"><input type="submit" name="changepassok" value="<?=$a_lang[BUTT_UPDATE];?>">
      </td>
    </tr>
  </form>
</table>
</td>
<? 	include ("footer.php"); 
} ?>
