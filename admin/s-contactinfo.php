<?
## v5.25 -> jun. 26, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

      function hashid($id) {
        $hash = md5($id*date("dmY"));
        $hash = strtoupper(substr($hash,0,5));
        return $hash;
      }


if ($_GET['option']=="delete") {
	$deleteWonItem=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET b_deleted=1 WHERE 
	id='".$_GET['id']."' AND  buyerid='".$_SESSION['memberid']."'");
	##$deleteWonItem=mysql_query("DELETE FROM probid_winners WHERE 
	##id='".$_GET['id']."' AND  buyerid='".$_SESSION['memberid']."'");
}

include ("themes/".$setts['default_theme']."/header.php");

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");

include("membersmenu.php"); ?>
<? $wonItems=getSqlRow("SELECT count(w.id) AS nb_auctions, sum(w.quant_offered) AS nb_quant, sum(w.amount * w.quant_offered) AS nb_amount FROM 
probid_winners w, probid_auctions a WHERE w.buyerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.b_deleted!=1"); 

$start = (empty($_GET['start'])) ? 0 : $_GET['start'];
$limit = 10;

$orderField = (empty($_REQUEST['orderField'])) ? "a.id" : $_REQUEST['orderField'];
if (empty($_REQUEST['orderType'])) {
	$orderType = "DESC";
	$newOrder="ASC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}

$additionalVars = "&purchase_date=".$_REQUEST['purchase_date'];

?>

<table border="0" cellspacing="1" cellpadding="4" class="buying">
   <tr>
      <td width="100" class="buyingtitle"><b><?=$lang[buyingtotals]?>:</b></td>
      <td class="buyingtd"><?=$lang[auctions];?>: <b><?=$wonItems['nb_auctions']; ?></b> </td>
      <td class="buyingtd"><?=$lang[quantity];?>: <b><?=$wonItems['nb_quant']; ?></b> </td>
      <td class="buyingtd"><?=$lang[amount];?>: <b><?=displayAmount($wonItems['nb_amount'], $setts['currency'], 'YES'); ?></b> </td>
   </tr>
</table>
<br />
<? 
$o86400 = ($_GET['purchase_date']==86400) ? "selected" : "";
$o604800 = ($_GET['purchase_date']==604800) ? "selected" : "";
$o2592000 = ($_GET['purchase_date']==2592000) ? "selected" : "";
$o31536000 = ($_GET['purchase_date']==31536000) ? "selected" : "";

$htmlPart = <<<BLOCK
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><form action="s-contactinfo.php?start={$start}&orderField={$orderField}&orderType={$orderType}" method="get">
		<td class="cathead"><b>{$lang[wonitems]}</b> ( {$wonItems['nb_auctions']} {$lang[items]} )</td>
		<td align="right" nowrap="nowrap" class="cathead">{$lang[viewitemspurchased]}
      	<select name="purchase_date" onChange="form.submit(this);" style="font-size: 10px; height: 12px;">
         	<option value="0" selected="selected">{$lang[all]}</option>
				<option value="86400" {$o86400}>{$lang[lastday]}</option>
				<option value="604800" {$o604800}>{$lang[lastweek]}</option>
				<option value="2592000" {$o2592000}>{$lang[lastmonth]}</option>
				<option value="31536000" {$o31536000}>{$lang[lastyear]}</option>
			</select> &nbsp; &nbsp; &nbsp;</td>
		</form>
      <td align="right" class="cathead" nowrap="nowrap">[ <a href="javascript:popUp('contact.print.php?option=buyer');">{$lang[printthis]}</a> ]</td>
	 </tr>
</table>
BLOCK;

headercat($htmlPart);

?>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="border">
   <tr>
      <td class="boldgrey"><a href="s-contactinfo.php?start=<?=$start;?>&orderField=w.id&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[item_id]?>
         </a>
         <? if($_GET['orderField'] == 'w.id'):?>
         <img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
         <? endif;?>
         / <a href="s-contactinfo.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[item_name];?>
         </a>
         <? if($_GET['orderField'] == 'a.itemname'):?>
         <img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
         <? endif;?>
      </td>
      <td align="center" class="boldgrey"><a href="s-contactinfo.php?start=<?=$start;?>&orderField=w.amount&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[bid]?>
         </a>
         <? if($_GET['orderField'] == 'w.amount'):?>
         <img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
         <? endif;?>
      </td>
      <td align="center" class="boldgrey"><?=$lang[quant]?></td>
      <td class="boldgrey"><?=$lang[contact_info]?></td>
      <td align="center" class="boldgrey"><a href="s-contactinfo.php?start=<?=$start;?>&orderField=w.purchase_date&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[purchase_date];?>
         </a>
         <? if($_GET['orderField'] == 'w.purchase_date'):?>
         <img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
         <? endif;?>
         / <a href="s-contactinfo.php?start=<?=$start;?>&orderField=w.flag_paid&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[status];?>
         </a>
         <? if($_GET['orderField'] == 'w.flag_paid'):?>
         <img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
         <? endif;?>
      </td>
      <td align="center" class="boldgrey"><?=$lang[option]?></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td align="center"><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? 	
	$addQuery = ($_REQUEST['purchase_date']>0) ? " AND w.purchase_date>=(".time()."-".$_REQUEST['purchase_date'].") " : "";
	$nbSellers = getSqlNumber("SELECT w.* FROM probid_winners w WHERE w.buyerid='".$_SESSION['memberid']."' AND w.b_deleted!=1".$addQuery);
	
	$getSellers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.* FROM probid_winners w, probid_auctions a WHERE w.buyerid='".$_SESSION['memberid']."' 
	AND w.b_deleted!=1 AND w.auctionid=a.id ".$addQuery."
	ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	while ($sellerDetails = mysqli_fetch_array($getSellers)) { 
		$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$sellerDetails['auctionid']."'");
		$seller = getSqlRow("SELECT * FROM probid_users WHERE id='".$sellerDetails['sellerid']."'");

		$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auction['id']."'","category");

		$category_id = getMainCat($itemCategory);

		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

		if ($sellerDetails['payment_status']=="confirmed"||($fee['is_endauction_fee']=="N")) { ?>
   <tr class="<? echo (($count++)%2==0)?"c2":"c3";?>" valign="top">
      <td class="smallfont"><strong> <? echo ($auction['itemname']=="")?$lang[auctiondeleted]:("#".$auction['id']." / ".$auction['itemname']);?> </strong> </td>
      <td align="center" class="smallfont"><? echo displayAmount($sellerDetails['amount'],$auction['currency']); ?></td>
      <td class="smallfont"><?=$lang[requested]?>
         : <? echo (($auction['auctiontype']=="dutch")?"".$sellerDetails['quant_req']."":"1");?><br>
         <?=$lang[offered]?>
         : <? echo (($auction['auctiontype']=="dutch")?"".$sellerDetails['quant_offered']."":"1");?> </td>
      <td class="smallfont"><table width="100%" border="0" cellpadding="2" cellspacing="1" class="c4">
            <tr valign="top" class="c2">
               <td class="smallfont"><b>
                  <?=$lang[username]?>
                  </b></td>
               <td class="smallfont"><? echo ($seller['username']=="")?$lang[userdeleted]:$seller['username'];?></td>
            </tr>
            <tr valign="top" class="c3">
               <td class="smallfont"><b>
                  <?=$lang[fullname]?>
                  </b></td>
               <td class="smallfont"><? echo ($seller['name']=="")?$lang[na]:$seller['name'];?></td>
            </tr>
         </table></td>
      <td align="center" class="smallfont"><? echo ($sellerDetails['purchase_date']>0) ? date($setts['date_format'],$sellerDetails['purchase_date']+$diff) : $lang[na]; ?><br />
         <? echo showPaidFlag($sellerDetails['flag_paid']); ?><br>
         <? echo showStatusFlag($sellerDetails['flag_status']); ?>
         <?
			if ($auction['acceptdirectpayment']) {
				if ( ($sellerDetails['directpayment_paid']==0) && ($sellerDetails['flag_paid']==0) ) {
					echo "<br><br><a href=\"".processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id'], 'winnerid' => $sellerDetails['id']))."\">".$lang[m_dir_pay]."</a>";
				} else {
					echo "<br><br><center><b>" . $lang[paiddirectpayment] . "</b></center>";
				}
			} ?></td>
      <td class="smallfont" style="line-height: 130%;" align="center"><? 
			echo '<b class="greenfont">&nbsp;&#8226;&nbsp;<a href="msgboard.php?userid='.$sellerDetails['sellerid'].'&winnerid='.$sellerDetails['id'].'"><span class="greenfont">'.$lang[m_view_messages].'</span></a></b> <br>';
      echo '<b class=""><a href="write.php?to=seller&amp;toid='.$sellerDetails['sellerid'].'&amp;tosid='.md5($sellerDetails['sellerid']).'&amp;aucid='.$sellerDetails['auctionid'].'">Send Message</a></b><br>';
			$isFeedback = getSqlNumber("SELECT * FROM probid_feedbacks WHERE fromid='".$_SESSION['memberid']."' AND userid='".$sellerDetails['sellerid']."' AND auctionid='".$sellerDetails['auctionid']."' AND submitted=0");
			if ($isFeedback>0) echo '&nbsp;&#8226;&nbsp;<a href="leavefeedback.php?to='.$sellerDetails['sellerid'].'&from='.$_SESSION['memberid'].'&auctionid='.$sellerDetails['auctionid'].'">'.ucwords($lang[feedback_leave]).'</a> <br>';
	
		##	if($sellerDetails['invoice_sent']) echo '&nbsp;&#8226;&nbsp;<a href="showinvoice.php?winnerid='.$sellerDetails['id'].'" target="_blank">'.$lang[m_show_invoice].'</a> <br>';

			echo '<script type="text/javascript"> ';
			echo '	function openPopUp(url) { ';
			echo '		var bank = window.open(url,\'bankdetails\',\'width=300, height=300\'); ';
			echo '		bank.focus(); ';
			echo '	} ';
			echo '</script> ';
	

      
      if ($sellerDetails['flag_paid']==0) echo '&nbsp;&#8226;&nbsp;<a href="payitem.php?id='.$sellerDetails['auctionid'].'&amp;hash='.hashid($sellerDetails['auctionid']).'"><b>Maksan SBE e-konto kaudu</b></a>';
			
          
		//	echo '&nbsp;&#8226;&nbsp;<a href="javascript:void(0);" onclick="openPopUp(\'showbankdetails.php?auctionid='.$sellerDetails['auctionid'].'&winnerid='.$sellerDetails['id'].'\');">'.$lang[m_bank_details].'</a><br> '; 
     		echo '<b class="redfont">&nbsp;&#8226;&nbsp;<a href="s-contactinfo.php?option=delete&id='.$sellerDetails['id'].'"><span class="redfont">'.$lang[delete].'</span></a></b>'; ?>
      </td>
   </tr>
   <? } else { ?>
   <tr valign="top">
      <td colspan="6" class="smallfont"><?
			echo $auction['itemname'].'<br>'.$lang[err_endofauc_fee].'<br>';

			if (@eregi('b', $fee['endauction_fee_applies']))
				echo '<a href="payfee.php?option=pay_endauction_fee&auctionid='.$sellerDetails['auctionid'].'&finalbid='.$sellerDetails['amount'].'">'.$lang[payfee].'</a>'; 
			?></td>
   </tr>
   <? }
	} ?>
   <tr>
      <td align="center" class="contentfont c4" colspan="6"><? paginate($start,$limit,$nbSellers,"s-contactinfo.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); ?></td>
   </tr>
</table>
<?	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
