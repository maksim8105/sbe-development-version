<?
## v5.23 -> dec. 14, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");
include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1;  

if (isset($_POST['savesettsok'])) {
	$fid = $_POST['fid'];
	$faq = $_POST['faq'];
	$delete = $_POST['delete'];
	$lang = $_POST['lang'];
	
	for ($i=0;$i<count($_POST['faq']);$i++) {
		$updateCountries = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_faq_categories SET 
		name='".remSpecialChars($faq[$i])."', lang='".$lang[$i]."', theorder = '".$_POST['theorder'][$i]."'  
		WHERE id='".$fid[$i]."'") or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	
	if ($_POST['addfaq']!="") {
		$insertFaqCat = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_faq_categories (name,lang) 
		VALUES ('".remSpecialChars($_POST['addfaq'])."','".$_POST['addlang']."')");
	}
	
	if (count($_POST['delete'])>0) {
		for ($i=0;$i<count($_POST['delete']);$i++) {
			$delCategories[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_faq_categories WHERE id='".$_POST['delete'][$i]."'");
			$delQuestions[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_faq_questions WHERE sectionid='".$_POST['delete'][$i]."'");
		}
	}
	$savedSettings="yes";
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_FAQ];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[FAQ_SEC_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
  <form action="faqsection.php" method="post"> 
    <tr class="c3"> 
      <td colspan="5" align="center"><b> 
        <?=$a_lang[EDIT_FAQ_SECTIONS];?> 
        </b></td> 
    </tr> 
    <tr class="c4"> 
      <td><?=$a_lang[FAQ_CAT_NAME];?></td> 
	  <td align="center"><?=$a_lang[LANG];?></td>
      <td align="center"><?=$a_lang[OPTIONS];?></td> 
      <td width="80" align="center"><?=$a_lang[ORDER_LIST];?></td>
      <td width="80" align="center"><?=$a_lang[DELETE];?></td> 
    </tr> 
    <? 
	$getFaqCategories = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_faq_categories ORDER BY lang ASC, theorder ASC"); 
	while ($faqCat = mysqli_fetch_array($getFaqCategories)) { ?> 
    <input type="hidden" name="fid[]" value="<?=$faqCat['id'];?>"> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td><input name="faq[]" type="text" value="<?=$faqCat['name'];?>" size="30"></td> 
	  <td><? 
	  	echo  "<select name=\"lang[]\">";
		for ($z=0; $z < $sizeofarray; $z++) {
			echo "<option value=\"$langlist[$z]\"";
			if($faqCat['lang']==$langlist[$z]) {
				echo " selected=\"selected\"";
			}
			echo ">$langlist[$z]\n";
		} ?></td>
      <td align="center"><a href="editquestions.php?section=<?=$faqCat['id'];?>"> 
        <?=$a_lang[EDIT_FAQ_QWECAT];?> 
        </a></td> 
      <td align="center"><input type="text" name="theorder[]" value="<?=$faqCat['theorder'];?>" size="7"></td>
      <td align="center"><input type="checkbox" name="delete[]" value="<?=$faqCat['id'];?>"></td> 
    </tr> 
    <? } ?> 
    <tr> 
      <td colspan="5" class="c1"><b> 
        <?=$a_lang[FAQ_ADD_SEC];?> 
        </b> 
        <input name="addfaq" type="text" id="addfaq" size="40"> <? 
	  	echo  "<select name=\"addlang\">";
		for ($z=0; $z < $sizeofarray; $z++) {
			echo "<option value=\"$langlist[$z]\"";
			if($setts['default_lang']==$langlist[$z]) {
				echo " selected=\"selected\"";
			}
			echo ">$langlist[$z]\n";
		} ?></td> 
    </tr> 
    <tr class="c3"> 
      <td colspan="5" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
