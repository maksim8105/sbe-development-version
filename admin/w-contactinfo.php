<?
## v5.25 -> jun. 26, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

if ($_GET['option']=="delete") {
	$deleteWonItem=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET s_deleted=1 WHERE 
	id='".$_GET['id']."' AND sellerid='".$_SESSION['memberid']."'");
	##$deleteWonItem=mysql_query("DELETE FROM probid_winners WHERE 
	##id='".$_GET['id']."' AND  sellerid='".$_SESSION['memberid']."'");
}

if ($_GET['option']=="claimback") {
##send email to admin with the details of the payment to retrieve the end of auction fee
	$auctionId = $_GET['auctid'];
	$sellerId = $_SESSION['memberid'];
	$buyerId = $_GET['buyerid'];
	$txnid = $_GET['txnid'];
	$amount = $_GET['amount'];
	include("mails/claimback.php");
	$mailsent="yes";
}

if (isset($_POST['updflagok'])) {
	$updFlag = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_winners SET flag_paid='".$_POST['flag_paid']."',
	flag_status='".$_POST['flag_status']."' WHERE id='".$_POST['win_id']."' AND
	sellerid='".$_SESSION['memberid']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
}
include ("themes/".$setts['default_theme']."/header.php");

if ($mailsent=="yes") {
	echo "<div class=contentfont align=center>$lang[claimsent]</div>";
}

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");

include("membersmenu.php"); 

$soldItems=getSqlRow("SELECT count(w.id) AS nb_auctions, sum(w.quant_offered) AS nb_quant, sum(w.amount * w.quant_offered) AS nb_amount FROM 
probid_winners w, probid_auctions a WHERE w.sellerid='".$_SESSION['memberid']."' AND w.auctionid=a.id AND w.s_deleted!=1"); 

$start = (empty($_GET['start'])) ? 0 : $_GET['start'];
$limit = 10;

$orderField = (empty($_REQUEST['orderField'])) ? "a.id" : $_REQUEST['orderField'];
if (empty($_REQUEST['orderType'])) {
	$orderType = "DESC";
	$newOrder="ASC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}

$additionalVars = "&purchase_date=".$_REQUEST['purchase_date'];

?>

<table border="0" cellspacing="1" cellpadding="4" class="selling">
   <tr>
      <td width="100" class="sellingtitle"><b><?=$lang[sellingtotals]?>:</b></td>
      <td class="sellingtd"><?=$lang[auctions];?>: <b><?=$soldItems['nb_auctions']; ?></b> </td>
      <td class="sellingtd"><?=$lang[quantity];?>: <b><?=$soldItems['nb_quant']; ?></b> </td>
      <td class="sellingtd"><?=$lang[amount];?>: <b><?=displayAmount($soldItems['nb_amount'], $setts['currency'], 'YES'); ?></b> </td>
   </tr>
</table>
<br />
<?
$o86400 = ($_GET['purchase_date']==86400) ? "selected" : "";
$o604800 = ($_GET['purchase_date']==604800) ? "selected" : "";
$o2592000 = ($_GET['purchase_date']==2592000) ? "selected" : "";
$o31536000 = ($_GET['purchase_date']==31536000) ? "selected" : "";

$htmlPart = <<<BLOCK
<table width="100%" border="0" cellspacing="1" cellpadding="1">
	<tr>
   	<td width="50%" class="cathead"><b>{$lang[solditems]}</b> ({$soldItems['nb_auctions']} {$lang[items]})</td>
		<form action="w-contactinfo.php?start={$start}&orderField={$orderField}&orderType={$orderType}" method="get">
      <td width="50%" align="right" nowrap="nowrap" class="cathead">{$lang[viewitemssold]}
			<select name="purchase_date" onchange="form.submit(this);" style="font-size: 10px; height: 16px;">
         	<option value="0" selected="selected">{$lang[all]}</option>
				<option value="86400" {$o86400}>{$lang[lastday]}</option>
				<option value="604800" {$o604800}>{$lang[lastweek]}</option>
				<option value="2592000" {$o2592000}>{$lang[lastmonth]}</option>
				<option value="31536000" {$o31536000}>{$lang[lastyear]}</option>
			</select> &nbsp; &nbsp; &nbsp;</td>
		</form>
	</tr>
</table>
BLOCK;
headercat($htmlPart);
?>
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="border">
   <tr>
      <td class="boldgrey"><a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.id&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[item_id]?></a>
<? if($_GET['orderField'] == 'w.id'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
 / <a href="w-contactinfo.php?start=<?=$start;?>&orderField=a.itemname&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[item_name];?>
         </a>
<? if($_GET['orderField'] == 'a.itemname'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
</td>
      <td align="center" class="boldgrey"><a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.amount&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[bid]?>
         </a>
<? if($_GET['orderField'] == 'w.amount'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
</td>
      <td align="center" class="boldgrey"><?=$lang[quant]?></td>
      <td class="boldgrey"><?=$lang[contact_info]?></td>
      <td align="center" class="boldgrey"><a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.purchase_date&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[purchase_date];?>
         </a>
<? if($_GET['orderField'] == 'w.purchase_date'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?>
 / <a href="w-contactinfo.php?start=<?=$start;?>&orderField=w.flag_paid&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[status];?>
         </a>
<? if($_GET['orderField'] == 'w.flag_paid'):?>
<img src="themes/<?=$setts['default_theme'];?>/img/system/<?=$_GET['orderType'];?>.gif">
<? endif;?></td>
      <td align="center" class="boldgrey"><?=$lang[option]?></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <? 
	$addQuery = ($_REQUEST['purchase_date']>0) ? " AND w.purchase_date>=(".time()."-".$_REQUEST['purchase_date'].") " : "";
	$nbBuyers = getSqlNumber("SELECT w.* FROM probid_winners w WHERE w.sellerid='".$_SESSION['memberid']."' AND w.s_deleted!=1".$addQuery);
	
	$getBuyers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.* FROM probid_winners w, probid_auctions a WHERE w.sellerid='".$_SESSION['memberid']."' 
	AND w.s_deleted!=1 AND w.auctionid=a.id ".$addQuery."
	ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	
	while ($buyerDetails = mysqli_fetch_array($getBuyers)) { 
		$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$buyerDetails['auctionid']."'");
		$buyer = getSqlRow("SELECT * FROM probid_users WHERE id='".$buyerDetails['buyerid']."'");

		$itemCategory = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auction['id']."'","category");

		$category_id = getMainCat($itemCategory);

		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

		if ($buyerDetails['payment_status']=="confirmed"||($fee['is_endauction_fee']=="N")) { ?>
   <tr class="<? echo (($count++)%2==0)?"c2":"c3";?>" valign="top">
      <td class="smallfont"><strong> <? echo ($auction['itemname']=="")?$lang[auctiondeleted]:("#".$auction['id']." / ".$auction['itemname']);?> </strong> </td>
      <td align="center" class="smallfont"><? echo displayAmount($buyerDetails['amount'],$auction['currency']); ?></td>
      <td class="smallfont"><?=$lang[requested]?>
         : <? echo (($auction['auctiontype']=="dutch")?"".$buyerDetails['quant_req']."":"1");?><br>
         <?=$lang[offered]?>
         : <? echo (($auction['auctiontype']=="dutch")?"".$buyerDetails['quant_offered']."":"1");?> </td>
      <td class="smallfont"><table width="100%" border="0" cellpadding="2" cellspacing="1" class="c4">
            <tr valign="top" class="c2">
               <td class="smallfont"><b>
                  <?=$lang[username]?>
                  </b></td>
               <td class="smallfont"><? echo ($buyer['username']=="")?$lang[userdeleted]:$buyer['username'];?></td>
            </tr>
            
         </table></td>
      <td align="center"><? echo ($buyerDetails['purchase_date']>0) ? date($setts['date_format'],$buyerDetails['purchase_date']+$diff) : $lang[na]; ?><br />
         <table width="100%" border="0" cellpadding="2" cellspacing="1" class="border c4">
            <form action="w-contactinfo.php" method="post">
               <input type="hidden" name="win_id" value="<?=$buyerDetails['id'];?>">
               <tr>
                  <td align="center"><select name="flag_paid" style="font-size:10px; width: 100px;">
                        <option value="0" <? echo ($buyerDetails['flag_paid']==0) ? "selected":""; ?>>
                        <?=$lang[flag_paid_unpaid]; ?>
                        </option>
                        <option value="1" <? echo ($buyerDetails['flag_paid']==1) ? "selected":""; ?>>
                        <?=$lang[flag_paid_paid]; ?>
                        </option>
                     </select></td>
               </tr>
               <tr>
                  <td align="center"><select name="flag_status" style="font-size:10px; width: 100px;">
                        <option value="0" <? echo ($buyerDetails['flag_status']==0) ? "selected":""; ?>>
                        <?=$lang[flag_status_0]; ?>
                        </option>
                        <option value="1" <? echo ($buyerDetails['flag_status']==1) ? "selected":""; ?>>
                        <?=$lang[flag_status_1]; ?>
                        </option>
                        <option value="2" <? echo ($buyerDetails['flag_status']==2) ? "selected":""; ?>>
                        <?=$lang[flag_status_2]; ?>
                        </option>
                        <option value="3" <? echo ($buyerDetails['flag_status']==3) ? "selected":""; ?>>
                        <?=$lang[flag_status_3]; ?>
                        </option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <td align="center"><input type="submit" name="updflagok" value="<?=$lang[go];?>" style="font-size:10px; width: 100px;"></td>
               </tr>
            </form>
         </table></td>
      <td class="smallfont" style="line-height: 130%;"><?
      
			echo '<b class="greenfont">&nbsp;&#8226;&nbsp;<a href="msgboard.php?winnerid='.$buyerDetails['id'].'"><span class="greenfont">'.$lang[m_view_messages].'</span></a></b><br>';
      echo '<b class=""><a href="write.php?toid='.$buyerDetails['buyerid'].'&amp;tosid='.md5($buyerDetails['buyerid']).'&amp;aucid='.$buyerDetails['auctionid'].'">Send Message</a></b><br>';
			$isFeedback = getSqlNumber("SELECT * FROM probid_feedbacks WHERE fromid='".$_SESSION['memberid']."' AND userid='".$buyerDetails['buyerid']."' AND auctionid='".$buyerDetails['auctionid']."' AND submitted=0");
			if ($isFeedback>0) echo '&nbsp;&#8226;&nbsp;<a href="leavefeedback.php?to='.$buyerDetails['buyerid'].'&from='.$_SESSION['memberid'].'&auctionid='.$buyerDetails['auctionid'].'">'.ucwords($lang[feedback_leave]).'</a>';

		##	if(!$buyerDetails['invoice_sent']) echo '<br>&nbsp;&#8226;&nbsp;<a href="sendinvoice.php?winnerid='.$buyerDetails['id'].'">'.$lang[m_send_invoice].'</a>';
        ## 	else echo'<br>&nbsp;&#8226;&nbsp;<a href="showinvoice.php?winnerid='.$buyerDetails['id'].'" target="_blank">'.$lang[m_show_invoice].'</a>';

			echo '<script type="text/javascript"> ';
			echo '	function openPopUp(url) { ';
			echo '		var bank = window.open(url,\'bankdetails\',\'width=300, height=300\'); ';
			echo '		bank.focus(); ';
			echo '	} ';
        	echo '</script> ';

			echo '<br>&nbsp;&#8226;&nbsp;<a href="javascript:void(0);" onclick="openPopUp(\'bankdetails.php?winnerid='.$buyerDetails['id'].'&auctionid='.$buyerDetails['auctionid'].'\');">'.$lang[m_send_bank_details].'</a> <br>'; 
			echo '<b class="redfont">&nbsp;&#8226;&nbsp;<a href="w-contactinfo.php?option=delete&id='.$buyerDetails['id'].'"><span class="redfont">'.$lang[delete].'</span></a></b>';
			?></td>
   </tr>
   <? if ($buyerDetails['amountpaid']>0) { ?>
   <tr valign="top" class="c4">
      <td class="contentfont"><? 
				echo '<b>#'.$buyerDetails['auctionid'].'</b> </td><td colspan="3" class="smallfont"><a href="w-contactinfo.php?option=claimback&auctid='.$buyerDetails['auctionid'].'&buyerid='.$buyerDetails['buyerid'].'&amount='.$buyerDetails['endamount'].'&txnid='.$buyerDetails['txnid'].'">'.$lang[claimbackendauc].'</a>';
			 ?>
      </td>
   </tr>
   <? } } else { ?>
   <tr valign="top" class="c4">
      <td class="contentfont"><?
			echo '<b>#'.$buyerDetails['auctionid'].'</b></td><td colspan="3" class="smallfont">'.$lang[err_endofauc_fee].'<br>';

			if (@eregi('s', $fee['endauction_fee_applies'])) 
				echo '<a href="payfee.php?option=pay_endauction_fee&auctionid='.$buyerDetails['auctionid'].'&finalbid='.$buyerDetails['amount'].'">'.$lang[payfee].'</a>'; 
			?></td>
   </tr>
   <? }
	} ?>
   <tr>
      <td align="center" class="contentfont c4" colspan="6"><? paginate($start,$limit,$nbBuyers,"w-contactinfo.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); ?></td>
   </tr>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
