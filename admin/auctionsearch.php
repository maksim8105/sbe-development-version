<?
## v5.25 -> jun. 27, 2006
session_start();

$params="?posted=1";
foreach($_REQUEST as $key=>$value){
	$$key=$value;
	$params.=($key=="PHPSESSID")?"":"&".$key."=".$value; // block session id
}
if (!$posted) {
	$redirect=$_SERVER["PHP_SELF"].$params;
	header("Location: $redirect"); /* Redirect browser */
    return;
}

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");
header5("$lang[aucsearch]");
	
if ($_REQUEST['orderField']=="") $orderField = "enddate";
else $orderField=$_REQUEST['orderField'];
if ($_REQUEST['orderType']=="") {
	$orderType = "ASC";
	$newOrder="DESC";
} else {
	$orderType=$_REQUEST['orderType'];
	$newOrder=($orderType=="ASC")?"DESC":"ASC";
}
	
if (isset($_REQUEST['itemsearchok'])||$_GET['advsrc']=="itemsearch") {
	$advsrc = "itemsearch";
	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_find ('query') VALUES ('Evgen')");
	$query = "SELECT * FROM probid_auctions WHERE keywords LIKE '%".$_REQUEST['keywords']."%' AND deleted!=1 AND active=1 ";
	if ($_REQUEST['category']!=0) $query .= " AND category='".$_REQUEST['category']."'";
	if ($_REQUEST['bnonly']=="yes") $query .= " AND bn='Y'";
	if ($_REQUEST['rponly']=="yes") $query .= " AND rp='Y'";
	if ($_REQUEST['dutchonly']=="yes") $query .= " AND auctiontype='dutch'";
	if ($_REQUEST['results']==2) $query .= " AND closed=0";
	if ($_REQUEST['results']==3) $query .= " AND closed=1";
	if ($_REQUEST['country']!="all") $query .= " AND country='".$_REQUEST['country']."'";
	
	if ($_REQUEST['listin']=="auction") $query .= " AND listin!='store'";
	else if ($_REQUEST['listin']=="store") $query .= " AND listin!='auction'";
	
	if (trim($_REQUEST['zip'])!="") $query .= " AND zip LIKE '%".$_REQUEST['zip']."%'";
	if (!$_REQUEST['orderField']) {
		if ($_REQUEST['sortby']==1) {
			$orderField = "enddate";
			$orderType = "ASC";
		}
		if ($_REQUEST['sortby']==2) {
			$orderField = "startdate";
			$orderType = "DESC";
		}
		if ($_REQUEST['sortby']==3) {
			$orderField = "bidstart";
			$orderType = "ASC";
		}
	}
} else if (isset($_REQUEST['usersearchok'])||$_GET['advsrc']=="usersearch") {
	$advsrc = "usersearch";
	$query = "SELECT DISTINCT auct.id, auct.picpath, auct.itemname, auct.maxbid, auct.nrbids, auct.currency, auct.enddate,
	auct.bidstart, auct.bn, auct.bnvalue FROM 
	probid_users AS users, probid_auctions AS auct WHERE 
	(users.username LIKE '%".$_REQUEST['keywords']."%') 
	AND users.id=auct.ownerid AND auct.deleted!=1 AND auct.active=1";

	if ($_REQUEST['results']==2) $query .= " AND auct.closed=0";
	if ($_REQUEST['results']==3) $query .= " AND auct.closed=1";
	
	if (!$_REQUEST['orderField']) {
		if ($_REQUEST['sortby']==1) {
			$orderField = "auct.enddate";
			$orderType = "ASC";
		}
		if ($_REQUEST['sortby']==2) {
			$orderField = "auct.startdate";
			$orderType = "DESC";
		}
		if ($_REQUEST['sortby']==3) {
			$orderField = "auct.bidstart";
			$orderType = "ASC";
		}
	}
} else if (isset($_REQUEST['buyersearchok'])||$_GET['advsrc']=="buyersearch") {
	$advsrc = "buyersearch";
	$query = "SELECT DISTINCT auct.id, auct.picpath, auct.itemname, auct.maxbid, auct.nrbids, auct.currency, auct.enddate,
	auct.bidstart, auct.bn, auct.bnvalue FROM 
	probid_users AS users, probid_auctions AS auct, probid_bids AS bid WHERE 
	(users.username LIKE '%".$_REQUEST['username']."%') 
	AND users.id=bid.bidderid AND auct.id=bid.auctionid AND auct.deleted!=1 AND auct.closed=0 AND auct.active=1 ";
} else {
	$query = "SELECT * FROM probid_auctions 
	WHERE keywords LIKE '%".$_REQUEST['basicsearch']."%' AND active=1 AND closed=0 AND deleted!=1";
}

$additionalVars = "&basicsearch=".$_REQUEST['basicsearch']."&advsrc=".$advsrc.
"&results=".$_REQUEST['results']."&sortby=".$_REQUEST['sortby'].
"&keywords=".$_REQUEST['keywords']."&country=".$_REQUEST['country'].
"&bnonly=".$_REQUEST['bnonly']."&rponly=".$_REQUEST['rponly']."&dutchonly=".$_REQUEST['dutchonly']."&username=".$_REQUEST['username']."&listin=".$_REQUEST['listin'];
if ($_GET['start'] == "") $start = 0;
else $start = $_GET['start'];
$limit = 20;
	
$totalResults = getSqlNumber($query." ORDER BY ".$orderField." ".$orderType.""); 
$resultsQuery = mysqli_query($GLOBALS["___mysqli_ston"], $query." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");
?>

<br>
<table width="100%" border="0" cellspacing="1" cellpadding="3">
   <tr class="c1">
      <td width="50" align="center" class="submenu"><?=$lang[picture]?></td>
      <td class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[auc_name]?>
         </a></td>
      <td width="80" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=bidstart&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[startbid]?>
         </a></td>
      <td width="80" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=maxbid&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[lastbid]?>
         </a></td>
		<? if ($setts['buyout_process']==0) { ?>
      <td width="80" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=bnvalue&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[buyout]?>
         </a></td>
		<? } ?>
      <td width="70" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=nrbids&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[bids]?>
         </a></td>
      <td width="120" align="center" class="submenu"><a href="auctionsearch.php?start=<?=$_REQUEST['start'];?>&orderField=enddate&orderType=<?=$newOrder.$additionalVars;?>">
         <?=$lang[endsin]?>
         </a></td>
   </tr>
   <tr class="c5">
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
		<? if ($setts['buyout_process']==0) { ?>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
		<? } ?>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
   </tr>
   <?
	if ($totalResults==0) {
		echo "<script>document.location.href='search.php?search=empty'</script>";	
	} else { 
		$i=0;
		while ($searchDetails = mysqli_fetch_array($resultsQuery)) { ?>
   <tr height="25" class="<? echo (($count++)%2==0) ? "c2":"c3"; ?>">
      <td align="center"><? 
			echo "<a href=\"".processLink('auctiondetails', array('itemname' => $searchDetails['itemname'], 'id' => $searchDetails['id']))."\">";
			echo "<img src=\"$setts[siteurl]/makethumb.php?pic=".((!empty($searchDetails['picpath'])) ? $searchDetails['picpath'] : "themes/".$setts['default_theme']."/img/system/noimg.gif")."&w=100&sq=Y&b=Y\" border=\"0\">";
			echo "</a>"; ?>
      </td>
      <td><span class="<? echo setStyle($searchDetails['id']);?>"><a href="<?=processLink('auctiondetails', array('itemname' => $searchDetails['itemname'], 'id' => $searchDetails['id'])); ?>">
         <?=$searchDetails['itemname'];?>
         </a> <? echo itemPics($searchDetails['id']);?></span> </td>
      <td align="center"><? echo displayAmount($searchDetails['bidstart'],$searchDetails['currency']);?></td>
      <td align="center"><? echo displayAmount($searchDetails['maxbid'],$searchDetails['currency']);?></td>
		<? if ($setts['buyout_process']==0) { ?>
      <td align="center"><? echo (@eregi('Y',$searchDetails['bn'])) ? displayAmount($searchDetails['bnvalue'],$searchDetails['currency'],'YES') : $lang[na];?></td>
		<? } ?>
      <td align="center"><?=$searchDetails['nrbids'];?>
      </td>
      <td align="center"><? 
			$daysLeft = daysleft($searchDetails['enddate'],$setts['date_format']);
			$timeLeft = timeleft($searchDetails['enddate'],$setts['date_format']);
			echo ($daysLeft>0) ? $timeLeft : $lang[bidclosed]; ?></td>
   </tr>
   <? } ?>
   <tr class="c4">
      <td colspan=7 class=contentfont align=center><? 
	paginate($start,$limit,$totalResults,"auctionsearch.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); 
	?></td>
   </tr>
   <? } ?>
</table>
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>
