<?
## v5.22 -> sep. 22, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	$days = $_POST['days'];
	$description = $_POST['description'];
	$delete = $_POST['delete'];
	
	$deleteAuctionDurations = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_durations");
	
	for ($i=0;$i<count($days);$i++) {
		$insertAuctionDuration[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_durations 
		(days,description) VALUES 
		('".$days[$i]."','".remSpecialChars($description[$i])."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	
	if ($_POST['newday']!=""&&$_POST['newdescription']!="") {
		$insertNewAuctionDuration = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_durations 
		(days,description) VALUES 
		('".$_POST['newday']."','".remSpecialChars($_POST['newdescription'])."')");
	}
	if (count($delete)>0) {
		for ($i=0;$i<count($delete);$i++) {
			$deleteAuctionDuration[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_durations 
			WHERE days='".$delete[$i]."'");
		}
	}
	$savedSettings="yes";
	$arrangeAuctionDurations=mysqli_query($GLOBALS["___mysqli_ston"], "ALTER TABLE probid_auction_durations ORDER BY days");

}
include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_tables.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[EDIT_TABLES]; echo " / "; echo $a_lang[EDIT_A_DURATIONS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[AUCTION_DURATION_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post"> 
    <tr class="c3"> 
      <td colspan="3" align="center"><b> 
        <?=$a_lang[AUCTION_DURATION_SETTINGS];?> 
        </b></td> 
    </tr> 
    <tr class="c4"> 
      <td width="80"><?=$a_lang[DAYS];?></td> 
      <td><?=$a_lang[DESCRIPTION];?></td> 
      <td width="80" align="center"><?=$a_lang[DELETE];?></td> 
    </tr> 
    <? $getad=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_durations"); 
	while ($row=mysqli_fetch_array($getad)) { ?> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td><input name="days[]" type="text" id="days[]" value="<?=$row['days'];?>" size="8" maxlength="4"></td> 
      <td><input name="description[]" type="text" id="description[]" value="<?=$row['description'];?>"></td> 
      <td align="center"><input type="checkbox" name="delete[]" value="<?=$row['days'];?>"></td> 
    </tr> 
    <? } ?> 
    <tr class="c4"> 
      <td><?=$a_lang[ADD];?></td> 
      <td>&nbsp;</td> 
      <td>&nbsp;</td> 
    </tr> 
    <tr class="c1"> 
      <td><input name="newday" type="text" id="newday" size="8" maxlength="4"></td> 
      <td><input name="newdescription" type="text" id="newdescription"></td> 
      <td>&nbsp;</td> 
    </tr> 
    <tr class="c3"> 
      <td colspan="3" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
