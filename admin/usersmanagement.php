<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if (isset($_GET['usertype'])) {
    $_SESSION['usertype'] = $_GET['usertype'];
} else {
    if(count($_GET)==0) $_SESSION['usertype']='all';
}

if ($_GET['option']=="invoice") {
	$recipientId = $_GET['id'];
	include_once("../mails/invoiceuser.php");
}

if ($_GET['option']=="pref_sell") {
	$chgUserType = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	preferred_seller='".$_GET['value']."' WHERE id='".$_GET['id']."'");
}

if ($_GET['option']=="vat_exempt") {
	$chgUserType = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	vat_exempted='".$_GET['value']."' WHERE id='".$_GET['id']."'");
}

if ($_GET['option']=="auction_approval") {
	$chgUserType = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	auction_approval='".$_GET['value']."' WHERE id='".$_GET['id']."'");
}

if ($_GET['option']=="sell") {
	$chgUserType = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	is_seller='".$_GET['value']."' WHERE id='".$_GET['id']."'");
}

if ($_GET['option']=="chstatus") {
	if ($_GET['value']==1) counterAddUser($_GET['id']);
	if ($_GET['value']==0) counterRemUser($_GET['id']);

	$chgUserStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	active='".$_GET['value']."' WHERE id='".$_GET['id']."'");
	
	$chgAuctionStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
	active='".$_GET['value']."' WHERE ownerid='".$_GET['id']."'");

	$chgWantedStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_wanted_ads SET 
	active='".$_GET['value']."' WHERE ownerid='".$_GET['id']."'");
	
	## if it's the registration approval, change the setting and send the confirmation email
	$initApproval = getSqlField("SELECT mailactivated FROM probid_users WHERE id='".$_GET['id']."'","mailactivated");
	if ($setts['enable_ra']&&$initApproval==0&&$_GET['value']==1) {
		$approveAcct = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET mailactivated=1 WHERE id='".$_GET['id']."'");
		## notify user
		$recipientId=$_GET['id'];
		include ("../mails/register_approval_approved.php");
	}
}


if ($_GET['option'] == 'chpayg') {
  	$changePaymentMode = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	payment_mode='".$_GET['value']."' WHERE id='".intval($_GET['id'])."'");
}

if ($_GET['option']=="delete"&&$_GET['confirmed']=="yes") {
	##counterRemUser($_GET['id']);
	$delUser=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_users WHERE id='".$_GET['id']."'");
	$delKWatch = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_keywords_watch WHERE bidderid='".$_GET['id']."'");
	
	$getWanted = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_wanted_ads WHERE ownerid='".$_GET['id']."' AND active=1 AND closed=0 AND deleted!=1");
	while ($wantedArray=mysqli_fetch_array($getWanted)) {
		delwantedcount ($wantedArray['category']);
		delwantedcount ($wantedArray['addlcategory']);
	}
	$delWanted = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_wanted_ads WHERE ownerid='".$_GET['id']."'");
	
	$getAuction = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_auctions WHERE ownerid='".$_GET['id']."'");
	$counter=0;
	while ($auctionsArray=mysqli_fetch_array($getAuction)) {
		deleteAuction($auctionsArray['id'],TRUE);
	}
}

include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");

if ($_REQUEST['option']=="delete"&&$_GET['confirmed']!="yes") {
	echo deleteConfirm("usersmanagement.php?option=delete&id=".$_REQUEST['id']."&start=".$_REQUEST['start']."&keywords=".$_REQUEST['keywords'],"usersmanagement.php?start=".$_REQUEST['start']."&keywords=".$_REQUEST['keywords']);
} else { ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[USER_MANAGE_LIST];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table border="0" cellpadding="4" cellspacing="2" class="border" align="center">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <tr class="c3">
         <td colspan="3"><?=$a_lang[USER_SEARCH_TITLE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[BY_NAME_USERNAME];?>
            :</td>
         <td colspan="2"><input name="keywords_name" type="text" id="keywords_name"></td>
      <tr class="c2">
         <td>ISIKIUKOOD
            :</td>
         <td colspan="2"><input name="keywords_isikukood" type="text" id="keywords_isikukood"></td>
      <tr class="c1">
         <td>REAL NAME FROM BANK
            :</td>
         <td colspan="2"><input name="keywords_realname" type="text" id="keywords_realname"></td>
      <tr class="c2">
         <td><?=$a_lang[BY_EMAIL];?>
            :</td>
         <td><input name="keywords_email" type="text" id="keywords_email"></td>
         <td><input name="usersearchok" type="submit" id="usersearchok" value="<?=$a_lang[BUTT_SEARCH];?>"></td>
      </tr>
   </form>
</table>
<br>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr>
      <td align="center" class="c3" colspan="5"><b>
         <?=$a_lang[USER_MANAGE_TITLE];?>
         </b></td>
   </tr>
   <? 
  	## if a search is made, create the WHERE clause
	if ($_REQUEST['keywords_name']!="") {
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." (username LIKE '%".$_REQUEST['keywords_name']."%' OR name LIKE '%".$_REQUEST['keywords_name']."%')";
	}
	if ($_REQUEST['keywords_isikukood']!="") {
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." isikukood LIKE '%".$_REQUEST['keywords_isikukood']."%'";
	}
	if ($_REQUEST['keywords_realname']!="") {
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." realname LIKE '%".$_REQUEST['keywords_realname']."%'";
	}
	if ($_REQUEST['keywords_email']!="") {
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." email LIKE '%".$_REQUEST['keywords_email']."'";
	}
	
	if ($_REQUEST['showonly']=="authenticated")
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." authenticated='Y'";
	else if ($_REQUEST['showonly']=="preferred")
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." preferred_seller='Y'";
	else if ($_REQUEST['showonly']=="active") 
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." active='1'";
	else if ($_REQUEST['showonly']=="suspended") 
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." active='0'";
	else if ($_REQUEST['showonly']=="tax_exempt") 
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." vat_exempted='Y'";
	else if ($_REQUEST['showonly']=="tax_unexempt") 
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." vat_exempted='N'";
	else if ($_REQUEST['showonly']=="reg_incompl") 
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." active='0' AND mailactivated='0'";
		
    if($_SESSION['usertype']=='individual'){
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." companyname IS NULL";
    }elseif($_SESSION['usertype']=='business'){
		$searchPattern .= (($searchPattern == "") ? " WHERE" : " AND")." companyname IS NOT NULL";
    }
 		
	if ($_GET['start'] == "") $start = 0;
	else $start = $_GET['start'];
	$limit = 20;
	if (!$_REQUEST['orderField']) $orderField = "id";
	else $orderField=$_REQUEST['orderField'];
	if (!$_REQUEST['orderType']) {
		$orderType = "DESC";
		$newOrder="ASC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}

	$additionalVars = "&keywords_name=".$_REQUEST['keywords_name']."&keywords_email=".$_REQUEST['keywords_email'];
	
	$totalUsers = getSqlNumber("SELECT * FROM probid_users 
	".$searchPattern." ORDER BY ".$orderField." ".$orderType.""); 
	$usersQuery = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users 
	".$searchPattern." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");
	?>
   <tr>
      <td align="center" colspan="5">[ <a href="registerclient.php">
         <?=$a_lang[REG_NEW_CLIENT];?>
         </a> ]</td>
   </tr>
   <tr>
      <td align="center" valign="bottom" colspan="5"><?=$a_lang[YOUR_QWERY_MATCHED];?>
         <?=$totalUsers;?>
         <?=$a_lang[RESULTS];?>
         .</td>
   </tr>
   <tr>
      <td height="30" align="center" valign="bottom" colspan="5"><? 
    echo "<strong>".$a_lang[USER_TYPE]."</strong>: ".
		(($_REQUEST['usertype']=="all")||($_SESSION['usertype']=="all") ? "[ ":"<a href=\"usersmanagement.php?usertype=all&showonly=".$_REQUEST['showonly']."&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[SHOW_ALL].(($_REQUEST['usertype']=="all")||($_SESSION['usertype']=="all") ? " ]":"</a>")." | ".
		(($_REQUEST['usertype']=="individual")||($_SESSION['usertype']=="individual") ? "[ ":"<a href=\"usersmanagement.php?usertype=individual&showonly=".$_REQUEST['showonly']."&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[USER_INDIVUDUAL].(($_REQUEST['usertype']=="individual")||($_SESSION['usertype']=="individual") ? " ]":"</a>")." | ".
   	(($_REQUEST['usertype']=="business")||($_SESSION['usertype']=="business") ? "[ ":"<a href=\"usersmanagement.php?usertype=business&showonly=".$_REQUEST['showonly']."&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[USER_BUSINESS].(($_REQUEST['usertype']=="business")||($_SESSION['usertype']=="business") ? " ]":"</a>")
      ."<br>";
	echo "<strong>".$a_lang[SHOW_ONLY]."</strong>: ".
		(($_REQUEST['showonly']=="authenticated") ? "[ ":"<a href=\"usersmanagement.php?showonly=authenticated&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">")."Authenticated".(($_REQUEST['showonly']=="authenticated") ? " ]":"</a>")." | ".
		(($_REQUEST['showonly']=="preferred") ? "[ ":"<a href=\"usersmanagement.php?showonly=preferred&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[PREF_SELLERS].(($_REQUEST['showonly']=="preferred") ? " ]":"</a>")." | ".
		(($_REQUEST['showonly']=="active") ? "[ ":"<a href=\"usersmanagement.php?showonly=active&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[ACTIVE_USERS].(($_REQUEST['showonly']=="active") ? " ]":"</a>")." | ".
		(($_REQUEST['showonly']=="suspended") ? "[ ":"<a href=\"usersmanagement.php?showonly=suspended&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[SUSPENDED_USERS].(($_REQUEST['showonly']=="suspended") ? " ]":"</a>")." | ".
		(($setts['auto_vat_exempt']=="Y") ? 
		(($_REQUEST['showonly']=="tax_exempt") ? "[ ":"<a href=\"usersmanagement.php?showonly=tax_exempt&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[TAX_EXTEMPTED].(($_REQUEST['showonly']=="tax_exempt") ? " ]":"</a>")." | ".
		(($_REQUEST['showonly']=="tax_unexempt") ? "[ ":"<a href=\"usersmanagement.php?showonly=tax_unexempt&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[NOT_TAX_EXTEMPTED].(($_REQUEST['showonly']=="tax_unexempt") ? " ]":"</a>")." | "
		: "").
		(($_REQUEST['showonly']=="reg_incompl") ? "[ ":"<a href=\"usersmanagement.php?showonly=reg_incompl&start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[REG_NOT_COMPLETE].(($_REQUEST['showonly']=="reg_incompl") ? " ]":"</a>")." | ".
		(($_REQUEST['showonly']=="") ? "[ ":"<a href=\"usersmanagement.php?start=0&orderField=".$_REQUEST['orderField']."&orderType=".$_REQUEST['orderType'].$additionalVars."\">").$a_lang[SHOW_ALL].(($_REQUEST['showonly']=="") ? " ]":"</a>");?></td>
   </tr>
   <tr class="c4">
      <td width="85"><a href="usersmanagement.php?start=<?=$_REQUEST['start'];?>&orderField=username&orderType=<?=$newOrder.$additionalVars;?>&showonly=<?=$_REQUEST['showonly'];?>"><font color="#EEEE00">
         <?=$a_lang[USERNAME];?>
         </font></a></td>
      <td><a href="usersmanagement.php?start=<?=$_REQUEST['start'];?>&orderField=name&orderType=<?=$newOrder.$additionalVars;?>&showonly=<?=$_REQUEST['showonly'];?>"><font color="#EEEE00">
         <?=$a_lang[USER_DETAILS];?>
         </font></a></td>
      <td width="105" align="center"><a href="usersmanagement.php?start=<?=$_REQUEST['start'];?>&orderField=balance&orderType=<?=$newOrder.$additionalVars;?>&showonly=<?=$_REQUEST['showonly'];?>"><font color="#EEEE00">
         <?=$a_lang[ACCOUNT_BALANCE];?>
         </font></a></td>
      <td width="45" align="center"><a href="usersmanagement.php?start=<?=$_REQUEST['start'];?>&orderField=active&orderType=<?=$newOrder.$additionalVars;?>&showonly=<?=$_REQUEST['showonly'];?>"><font color="#EEEE00">
         <?=$a_lang[STATUS];?>
         </font></a></td>
      <td align="center" width="105"><?=$a_lang[OPTIONS];?></td>
   </tr>
   <? 
	while ($usersArray=mysqli_fetch_array($usersQuery)) { 
		$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$usersArray['id']."'","payment_mode");
		if ($setts['account_mode_personal']==1) {
			$account_mode_local = ($tmp) ? 2 : 1;
		} else $account_mode_local = $setts['account_mode']; ?>
  	   <tr class="<? echo (($count++)%2==0)?"c1":"c2";?>">
      <td><?=$usersArray['username'];?>
         <? echo getFeedback($usersArray['id'],TRUE);?> <br>
         [ <a href="userdetails.php?id=<?=$usersArray['id'];?>">
         <?=$a_lang[USER_MANAGE_DETAILS];?>
         </a> ]<br>
         [ <a href="userbids.php?id=<?=$usersArray['id'];?>">
         <?=$a_lang[VIEW_BIDS];?>
         </a> ]<br>
         [ <a href="auctionsmanagement.php?page=open&ownerid=<?=$usersArray['id'];?>">
         <?=$a_lang[VIEW_AUCT_FOR_SALE];?>
         </a> ]<br>
         [ <a href="edituser.php?id=<?=$usersArray['id'];?>">
         <?=$a_lang[CHANGE_DETS];?>
         </a> ]</td>
      <td><?
			echo "<b>$a_lang[NAME] :</b> ".$usersArray['name']."<br>"
				."<b>Real Name :</b> ".$usersArray['realname']."<br>"
				."<b>Isikukood :</b> ".$usersArray['isikukood']."<br>"   
				."<b>$a_lang[COUNTRY] :</b> ".$usersArray['country']."<br>" 
				."<b>$a_lang[EMAIL] :</b> ".$usersArray['email']."<br>" 
				."<b>User IP :</b> ".$usersArray['reg_ip'];
			if ($usersArray['referredby']!="") echo "<br><b>$a_lang[REFERRED] :</b> ".$usersArray['referredby'];	
			if ($usersArray['balance']<=0) {
				$thevar = "Credit";
				$balance = abs($usersArray['balance']);
			} else {
				$thevar = "Debit";
				$balance = $usersArray['balance'];
			}
			if ($setts['private_site']=="Y") {
				echo "<br><strong>".$a_lang[ENABLED_SELLER]."</strong> : <br>".
				(($usersArray['is_seller']=="N") ? 
				"[ $a_lang[NO] ] -> [ <a href=\"usersmanagement.php?option=sell&value=Y&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[ENABLE]</a> ]" :
				"[ <font color=\"#FF0000\">$a_lang[YES]</font> ] -> [ <a href=\"usersmanagement.php?option=sell&value=N&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[DISABLE]</a> ]");
			}
			if ($setts['pref_sellers']=="Y") {
				echo "<br><strong>".$a_lang[PREF_SELLER]."</strong> : <br>".
				(($usersArray['preferred_seller']=="N") ? 
				"[ $a_lang[NO] ] -> [ <a href=\"usersmanagement.php?option=pref_sell&value=Y&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[ENABLE]</a> ]" :
				"[ <font color=\"#FF0000\">$a_lang[YES]</font> ] -> [ <a href=\"usersmanagement.php?option=pref_sell&value=N&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[DISABLE]</a> ]");
			}
			if ($setts['vat_rate']>0) {
				if ($usersArray['applied_vat_exempt']=="Y") echo "<br><strong>".$a_lang[APPLIED_VAT_EXEMPT]."</strong>";
				if ($usersArray['vat_uid_number']!="") echo "<br><strong>".$a_lang[UID_NUMBER]."</strong> : ".$usersArray['vat_uid_number'];
				echo "<br><strong>".$a_lang[EXEMPT_FROM_VAT]."</strong> : <br>".
				(($usersArray['vat_exempted']=="N") ? 
				"[ $a_lang[NO] ] -> [ <a href=\"usersmanagement.php?option=vat_exempt&value=Y&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[ENABLE]</a> ]" :
				"[ <font color=\"#FF0000\">$a_lang[YES]</font> ] -> [ <a href=\"usersmanagement.php?option=vat_exempt&value=N&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[DISABLE]</a> ]");
			}
			if (@eregi('N', $setts['enable_auctions_approval'])) {
				echo "<br><strong>".$a_lang[REQ_AUCT_APPROV]."</strong> : <br>".
				(($usersArray['auction_approval']=="N") ? 
				"[ $a_lang[NO] ] -> [ <a href=\"usersmanagement.php?option=auction_approval&value=Y&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[ENABLE]</a> ]" :
				"[ <font color=\"#FF0000\">$a_lang[YES]</font> ] -> [ <a href=\"usersmanagement.php?option=auction_approval&value=N&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[DISABLE]</a> ]");
			}
			/*if ($setts[account_mode]==2) { 
				echo "<br><b>$a_lang[ACCOUNT_BALANCE]: </b>".(($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar."<br> <a href=\"userdetails.php?id=".$usersArray['id']."\"> --> $a_lang[EDIT_BALANCE]</a>"; 
				if ($usersArray['balance']>0) echo "<br><a href=\"usersmanagement.php?option=invoice&id=".$usersArray['id']."\">$a_lang[SEND_INVOICE]</a>";
			}*/ ?>
      </td>
      <td align="center"><?
			if ($account_mode_local==2) { 
				echo (($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar."<br> <a href=\"userdetails.php?id=".$usersArray['id']."\">$a_lang[EDIT_BALANCE]</a>"; 
				if ($usersArray['balance']>0) echo "<br><a href=\"usersmanagement.php?option=invoice&id=".$usersArray['id']."\">$a_lang[SEND_INVOICE]</a>";
			} else { echo "n/a"; } ?>
         <br>
         <a href="acc_user.php?id=<?=$usersArray['id'];?>">View History</a> <br>
         <? if ($setts['account_mode_personal']==1) { ?>
         <? if (!$usersArray['payment_mode']) { ?>
         <a href="usersmanagement.php?option=chpayg&id=<?=$usersArray['id'];?>&value=1">
         <?=$a_lang[ACCOUNT_PAY_ACC];?>
         </a>
         <? } else echo $a_lang[ACCOUNT_PAY_ACC]; ?>
         |
         <? if ($usersArray['payment_mode']) { ?>
         <a href="usersmanagement.php?option=chpayg&id=<?=$usersArray['id'];?>&value=0">
         <?=$a_lang[ACCOUNT_PAY_PAYG];?>
         </a>
         <? } else echo $a_lang[ACCOUNT_PAY_PAYG]; ?>
         <? } ?>
      </td>
      <td align="center"><? 
			if ($usersArray['active']==1) echo "$a_lang[ACTIVE]";
			else echo "$a_lang[INACTIVE]".(($setts['enable_ra']=="Y"&&$usersArray['mailactivated']==0)?"<br><b>".$a_lang[NOT_APPROVED]."</b>":""); ?>
      </td>
      <td align="center" nowrap><a href="senduseremail.php?username=<?=$usersArray['username'];?>&email=<?=$usersArray['email'];?>">
         <?=$a_lang[EMAIL_THIS_USER];?>
         </a><br>
         <? if ($usersArray['active']==1) echo "<a href=usersmanagement.php?option=chstatus&id=".$usersArray['id']."&value=0&showonly=".$_REQUEST['showonly'].">$a_lang[SUSPEND_ACCOUNT]</a>";
			else echo "<a href=usersmanagement.php?option=chstatus&id=".$usersArray['id']."&value=1&showonly=".$_REQUEST['showonly'].">".(($setts['enable_ra']=="Y"&&$usersArray['mailactivated']==0)?"<b>".$a_lang[APPROVE_ACCOUNT]."</b>":$a_lang[ACTIVATE_ACCOUNT])."</a>"; ?>
         <br>
         <a href="usersmanagement.php?option=delete&id=<?=$usersArray['id'];?>">
         <?=$a_lang[DELETE_ACCOUNT];?>
         </a></td>
   </tr>
   <? } ?>
</table>
<table width="100%"  border="0" cellspacing="4" cellpadding="4">
   <tr>
      <td align="center"><? paginate($start,$limit,$totalUsers,"usersmanagement.php",$additionalVars."&orderField=$orderField&orderType=$orderType&showonly=".$_REQUEST['showonly']); ?></td>
   </tr>
</table>
<? }
include ("footer.php"); 
} ?>
