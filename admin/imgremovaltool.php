<?
## v5.23 -> dec. 15, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include_once ('../config/lang/'.$_SESSION['sess_lang'].'/site.lang');

$deletion = FALSE;

function getmicrotime() 
{ 
   list($usec, $sec) = explode(" ", microtime()); 
   return ((float)$usec + (float)$sec); 
}


$dir = substr($_SERVER['SCRIPT_FILENAME'],0,-24);

if (isset($_GET['proceedaddimgok'])) {
	$filessize = 0;
	$exitloop = FALSE;
	$time_start = getmicrotime();
	$currentTime = time();
	## first we will delete all additional images for auctions that dont exist anymore
	$getAddImgs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT auctionid FROM probid_auction_images");
	while ($addImg = mysqli_fetch_array($getAddImgs)) {
		if (getSqlNumber("SELECT id FROM probid_auctions WHERE id='".$addImg['auctionid']."'")<=0)
			$img_aid = $addImg['auctionid']." ";
	}
	$img_aid = @eregi_replace(" ",",",trim(img_aid));
	
	$delAddImg[$add_img_cnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_images WHERE auctionid IN(".$img_aid.")");			
	$time_end = getmicrotime();
	$time_passed = $time_end - $time_start;
}


include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_auct.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[AUCT_MANAGE]; echo " / "; echo $a_lang[OLD_IMG_REMOVAL_TOOL];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <tr class="c3"> 
    <td align="center"><b> 
      <?=strtoupper($a_lang[OLD_IMG_REMOVAL_TOOL]);?> 
      </b></td> 
  </tr> 
</table> 

<?
function filter_unused( $all, $used, $prefix_all = "", $prefix_used = "" ) {
	$unused = array();

	// prefixes are not needed for sorting
	sort( $all );
	sort( $used );

	$a = 0;
	$u = 0;

	$maxa = sizeof($all)-1;
	$maxu = sizeof($used)-1;

	while( true ) {
		if( $a > $maxa ) {
			// done; rest of $used isn't in $all
			break;
		}
		if( $u > $maxu ) {
			// rest of $all is unused
			for( ; $a <= $maxa; $a++ ) {
				$unused[] = $all[$a];
			}
			break;
		}

		if( $prefix_all.$all[$a] > $prefix_used.$used[$u] ) {
			// $used[$u] isn't in $all?
			$u++;
			continue;
		}

		if( $prefix_all.$all[$a] == $prefix_used.$used[$u] ) {
			// $all[$a] is used
			$a++;
			$u++;
			continue;
		}

		$unused[] = $all[$a];

		$a++;
	}

	return $unused;
}


if (isset($_POST['proceedok'])) {
	
	echo '<br><table width="100%" border="0" cellspacing="2" cellpadding="4" class="border">'; 	
	
	$filessize = 0;
	$deletion=TRUE;
	$exitloop = FALSE;
	$time_start = getmicrotime();
	$currentTime = time();
		
	$rep=opendir($dir.'/uplimg/');
	$counter = 0;
	
	## - probid_auctions ('picpath' field)
	## - probid_users ('shop_logo' field)
	## - probid_auctions_images ('name' field)
	## gather all in an array
			
	$files = array();
	$filessize = 0;
	while (($file = readdir($rep))&&(!$exitloop)){
		if($file != '..' && $file !='.' && $file !='' && $file !='index.htm'){
			$files[] = $file;
			//echo $file."<br>";
			$filessize += filesize($dir.'/uplimg/'.$file);			
		}
	}
	
	$db_images = array();
	$getAuctionImg = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT picpath FROM probid_auctions WHERE picpath!=''");
	while ($auctionImg=mysqli_fetch_array($getAuctionImg)) $db_images[] = @eregi_replace("uplimg/","",$auctionImg['picpath']);
	$getAddAuctionImg = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT name FROM probid_auction_images WHERE name!=''");
	while ($addAuctionImg=mysqli_fetch_array($getAddAuctionImg)) $db_images[] = @eregi_replace("uplimg/","",$addAuctionImg['name']);
	$getShopImg = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT shop_logo FROM probid_users WHERE shop_logo!=''");
	while ($shopImg=mysqli_fetch_array($getShopImg)) $db_images[] = @eregi_replace("uplimg/","",$shopImg['shop_logo']);
	$getCatsImg = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT imagepath FROM probid_categories WHERE imagepath!=''");
	while ($catImg=mysqli_fetch_array($getCatsImg)) $db_images[] = @eregi_replace("uplimg/","",$catImg['imagepath']);
	
	//echo "DB IMAGES<br>";
	//for ($i=0; $i<count($db_images); $i++) echo $db_images[$i]."<br>";
	natcasesort($files);
	natcasesort($db_images);
	$obsolete_files = filter_unused($files, $db_images);
	//echo "DIFF<br>";
	//for ($i=0; $i<count($obsolete_files); $i++) echo $obsolete_files[$i]."<br>";
	
	$nb_obs = count($obsolete_files);

	$ending = ($nb_obs>100) ? 100 : $nb_obs;
	
	$exitloop = ($nb_obs>100) ? TRUE : FALSE;
	
	if ($nb_obs) echo "<tr class=\"c2\"><td>";

	for ($i=0; $i<$ending; $i++) {
		 
		$obsolete_file = trim($obsolete_files[$i]);
		//echo "O: ".$obsolete_file;
		if ($obsolete_file!="") { 
			echo "<strong>Processing</strong> ".$obsolete_file;
			$delfilessize += filesize($dir.'/uplimg/'.$obsolete_file);			
			@unlink($dir.'/uplimg/'.$obsolete_file);
			$counter++;
			echo " -> <font color=red><strong>DELETED</strong></font><br>";
		}
	}

	if ($nb_obs) echo "</td></tr>";
	
	echo "<tr class=\"c1\"><td>";
	echo "<strong>Total Files</strong>: ".count($files).". <strong>Total Size</strong>: ".number_format($filessize/1024,2,".",",")." kb.";
	echo "<br><strong>Total Images in Database</strong>: ".count($db_images);
	echo "<br><strong>Total Obsolete Files</strong>: ".count($obsolete_files);
	echo "<br><strong>Obsolete Files Erased in this Session</strong>: ".$counter."";
	closedir($rep);
	clearstatcache(); 
	
	$time_end = getmicrotime();
	$time_passed = $time_end - $time_start;
	echo '</td></tr></table>';
}
?>
<br> 
<? if ($deletion) { ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4" class="border"> 
  <tr class="c2"> 
    <td><b><?=$a_lang[CLEANUP_SUCCESS];?></b>. <strong><?=$counter;?></strong> <?=$a_lang[IMGS_HAVE_BEEN_DELETED_TOTALING];?> <strong><?=number_format($delfilessize/1024,2,".",",");?></strong> kb. <?=$a_lang[OPERATION_LASTED];?> 
	<strong><?=number_format($time_passed,4,".","");?></strong> <?=$a_lang[SECONDS];?> 
	<? if ($exitloop) echo "<br><strong>IMPORTANT:</strong> There are more than 100 files to be removed, please click proceed to resume the deletion. Deletion is capped at 100 files per click to avoid server stall."; ?></td> 
  </tr> 
</table> 
<br> 
<? } ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4" class="border"> 
  <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
  <tr class="c1"> 
    <td><?=$a_lang[IMG_REMOVAL_TOOL_DESC];?></td> 
  </tr> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
    <td colspan="4" align="center" class="c4"><input type="submit" name="proceedok" value="<?=$a_lang[PROCEED];?>"></td> 
  </tr> 
  </form>
</table> 
<br />
<? if (isset($_GET['proceedaddimgok'])) { ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4" class="border"> 
  <tr class="c2"> 
    <td><b><?=$a_lang[CLEANUP_SUCCESS];?></b>. <?=$a_lang[OPERATION_LASTED];?> 
	<strong><?=number_format($time_passed,4,".","");?></strong> <?=$a_lang[SECONDS];?> </td> 
  </tr> 
</table> 
<br> 
<? } ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4" class="border"> 
  <form action="<?=$_SERVER['PHP_SELF'];?>" method="get">
  <tr class="c1"> 
    <td><?=$a_lang[OBS_ADD_IMG_DESC];?></td> 
  </tr> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
    <td colspan="4" align="center" class="c4"><input type="submit" name="proceedaddimgok" value="<?=$a_lang[PROCEED];?>"></td> 
  </tr> 
  </form>
</table> 
<? 	include ("footer.php"); 
} ?>