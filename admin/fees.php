<?
## v5.22 -> sep. 22, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	$update1 = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_gen_setts SET 
	account_mode='".$_POST['setbutton']."', init_credit='".$_POST['value1']."', 
	max_credit='".$_POST['value2']."', account_mode_personal='".$_POST['account_mode_personal']."',
	suspend_over_bal_users='".$_POST['suspend_over_bal_users']."',
	min_invoice_value='".$_POST['min_invoice_value']."', init_acc_type='".$_POST['init_acc_type']."'");
	$savedSettings="yes";
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_fees.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[FEES]; echo " / "; echo $a_lang[FEES_MAIN];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[SETT_CHANGED]."</p>":""; ?> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form name="form" action="fees.php" method="post"> 
    <input type="hidden" name="savesettsok" value="yes"> 
    <tr class="c3"> 
      <td colspan="2" align="center"><b> 
        <?=$a_lang[FEES_MAIN_TITLE];?> 
        </b></td> 
    </tr> 
    <? $genSetts=getSqlRow("SELECT * FROM probid_gen_setts"); ?> 
    <tr> 
      <td colspan="2"><?=$a_lang[FEES_MAIN_MESSAGE];?></td> 
    </tr> 
    <tr class="c1"> 
      <td width="175"><?=$a_lang[FEES_MAIN_CHOOSE];?> 
        : </td> 
      <td> <input type="radio" name="setbutton" value="2" <? echo (($genSetts['account_mode']=="2")?"checked":"");?>> 
        <?=$a_lang[FEES_MAIN_AMODE];?> 
        <br> 
        <input type="radio" name="setbutton" value="1" <? echo (($genSetts['account_mode']=="1")?"checked":"");?>> 
        <?=$a_lang[FEES_MAIN_LMOME];?> </td> 
    </tr> 
    <tr class="c2"> 
      <td width="175"><?=$a_lang[FEES_MAIN_TYPE];?> 
        : </td> 
      <td> <input type="radio" name="account_mode_personal" value="0" <? echo (($genSetts['account_mode_personal']=="0")?"checked":"");?>> 
        <?=$a_lang[FEES_MAIN_GLOBAL];?> 
        <br> 
        <input type="radio" name="account_mode_personal" value="1" <? echo (($genSetts['account_mode_personal']=="1")?"checked":"");?>> 
        <?=$a_lang[FEES_MAIN_PERSONAL];?> </td> 
    </tr> 
    <tr class="c1"> 
      <td width="175"><?=$a_lang[INIT_ACC_TYPE];?> 
        : </td> 
      <td> <input type="radio" name="init_acc_type" value="0" <? echo (($genSetts['init_acc_type']=="0")?"checked":"");?>> 
        <?=$a_lang[FEES_MAIN_LMOME];?> 
        <br> 
        <input type="radio" name="init_acc_type" value="1" <? echo (($genSetts['init_acc_type']=="1")?"checked":"");?>> 
        <?=$a_lang[FEES_MAIN_AMODE];?> </td> 
    </tr> 
    <? if ($genSetts['account_mode']=="2") { ?> 
    <tr class="c4"> 
      <td colspan="2"><?=$a_lang[FEES_MAIN_MESS1];?></td> 
    </tr> 
    <tr class="c1"> 
      <td><?=$a_lang[FEES_MAIN_SCRED];?> 
        :</td> 
      <td><?=$genSetts['currency'];?> 
        <input name="value1" type="text" id="value1" value="<?=$genSetts['init_credit'];?>" size="20"></td> 
    </tr> 
    <tr class="c2"> 
      <td><?=$a_lang[FEES_MAIN_MCRED];?> 
        :</td> 
      <td><?=$genSetts['currency'];?> 
        <input name="value2" type="text" id="value2" value="<?=$genSetts['max_credit'];?>" size="20"> </td> 
    </tr> 
    <tr class="c1"> 
      <td><?=$a_lang[FEES_MIN_INVOICE];?> 
        :</td> 
      <td><?=$genSetts['currency'];?> 
        <input name="min_invoice_value" type="text" id="min_invoice_value" value="<?=$genSetts['min_invoice_value'];?>" size="20"> </td> 
    </tr> 
    <? 
	} else {
    	echo "<input name=\"value1\" type=\"hidden\" value=\"".$genSetts['init_credit']."\">"
             ."<input name=\"value2\" type=\"hidden\" value=\"".$genSetts['max_credit']."\">";
	} ?> 
    <tr class="c4"> 
      <td colspan="2"><?=$a_lang[SUSPEND_OVER_BAL_USRS];?></td> 
    </tr> 
    <tr class="c1"> 
      <td width="144"><?=$a_lang[SUSP_ACCTS];?> 
        : </td> 
      <td> <input type="radio" name="suspend_over_bal_users" value="1" <? echo (($genSetts['suspend_over_bal_users']=="1")?"checked":"");?>> 
        <?=$a_lang[YES];?> 
        <input type="radio" name="suspend_over_bal_users" value="0" <? echo (($genSetts['suspend_over_bal_users']=="0")?"checked":"");?>> 
        <?=$a_lang[NO];?><br /><?=$a_lang[NOT_SUSPEND_OVER_BAL_USRS];?></td> 
    </tr> 
    <tr class=""> 
      <td colspan="2" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?> 
