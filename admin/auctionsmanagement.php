<?
## v5.25 -> jun. 09, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

if ($_REQUEST['page']!="open"&&$_REQUEST['page']!="closed"&&$_REQUEST['page']!="suspended"&&$_REQUEST['page']!="unstarted"&&$_REQUEST['page']!="approval") {
	header ("Location: index.php");
}

include ("../config/config.php");

if ($_GET['option']=="relist") {
	$relistId = $_GET['id'];
	$today = date( "Y-m-d H:i:s", time() );
	$closingdate = closingdate($today,$_GET['duration']);
	$relistAuction = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auctions 
	(itemname, description, picpath, quantity, auctiontype, bidstart, rp, 
	rpvalue, bn, bnvalue, bi, bivalue, duration, country, zip, sc, scint, pm, 
	category, active, payment_status, startdate, enddate, closed, keywords, 
	nrbids, maxbid, clicks, ownerid, hpfeat, catfeat, bolditem, hlitem, private, 
	currency, swapped, postage_costs, insurance, type_service, isswap, acceptdirectpayment, 
	addlcategory, deleted, listin, shipping_details, bank_details, accept_payment_systems, apply_vat, 
	auto_relist, auto_relist_bids, videofile_path, offer_range_min, offer_range_max, auto_relist_nb) SELECT
	itemname, description, picpath, quantity, auctiontype, bidstart, rp, 
	rpvalue, bn, bnvalue, bi, bivalue, duration, country, zip, sc, scint, pm, 
	category, active, payment_status, startdate, enddate, closed, keywords, 
	nrbids, maxbid, clicks, ownerid, hpfeat, catfeat, bolditem, hlitem, private, 
	currency, swapped, postage_costs, insurance, type_service, isswap, acceptdirectpayment, 
	addlcategory, deleted, listin, shipping_details, bank_details, accept_payment_systems, apply_vat, 
	auto_relist, auto_relist_bids, videofile_path, offer_range_min, offer_range_max, auto_relist_nb 
	FROM probid_auctions b WHERE b.id='".$relistId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
				
	$newId = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
				
	$getCustomFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fields_data WHERE auctionid='".$relistId."'");
	while ($relistFld = mysqli_fetch_array($getCustomFields)) {
		$relistCustomFields[$relist_counter][$custom_fld++] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fields_data 
		(auctionid,ownerid,boxid,boxvalue,active) VALUES 
		('".$newId."','".$relistFld['ownerid']."','".$relistFld['boxid']."','".$relistFld['boxvalue']."','".$relistFld['active']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));			
	}
	
	$getImages = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$relistId."'");
	$addCnt=0;
	while ($relistAddImages=mysqli_fetch_array($getImages)) {
		$fileExtension = getFileExtension($relistAddImages['name']);
		$addImageName = "a".$newId."_addpic".$addCnt.".".$fileExtension;				
		$isUpload = uploadFile("../".$relistAddImages['name'],$addImageName,"../uplimg/",TRUE,'../');
		if ($isUpload) {
			$addImage[$addCnt] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_auction_images
			(name, auctionid) VALUES ('uplimg/".$addImageName."','".$newId."')");
			$addCnt++;
		}
	}
	$mainPicRelist = getSqlField("SELECT picpath FROM probid_auctions WHERE id='".$newId."'","picpath");
	if ($mainPicRelist != "") {
		$fileExtension = getFileExtension($mainPicRelist);
		$mainPicName = "a".$newId."_mainpic.".$fileExtension;
		$isUpload = uploadFile("../".$mainPicRelist,$mainPicName,"../uplimg/",TRUE,'../');
		if ($isUpload) {
			$updateAuctionPic = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
			picpath='uplimg/".$mainPicName."' WHERE id='".$newId."'");
		}
	}
	## video relist
	$videoRelist = getSqlField("SELECT videofile_path FROM probid_auctions WHERE id='".$newId."'","videofile_path");
	if (!empty($videoRelist)) {
		$fileExtension = getFileExtension($videoRelist);
		$videoName = "a".$newId."_video.".$fileExtension;
		$isUpload = uploadFile($videoRelist,$videoName,"uplimg/");
		if ($isUpload) {
			$updateAuctionVideo[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET
			videofile_path='uplimg/".$videoName."' WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
	}

	### check for additional images
		
	$nbAddImages = getSqlNumber("SELECT * FROM probid_auction_images WHERE auctionid='".$newId."'");
	
	$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$newId."'");
			
	$payment_status="confirmed";
	$active=1;
				
	addcatcount($auctionDetails['category'],$newId);
	addcatcount($auctionDetails['addlcategory'],$newId);

	$hlitem_chg = ($setts['hl_item']==1) ? $auctionDetails['hlitem'] : "N";
	$bolditem_chg = ($setts['bold_item']==1) ? $auctionDetails['bolditem'] : "N";
	$hpfeat_chg = ($setts['hp_feat']==1) ? $auctionDetails['hpfeat'] : "N";
	$catfeat_chg = ($setts['cat_feat']==1) ? $auctionDetails['catfeat'] : "N";

	## calculate quantity
	$qu_a = getSqlField("SELECT quantity FROM probid_auctions WHERE id='".$newId."'","quantity");
	$qu_b = getSqlField("SELECT sum(quant_offered) AS quant_o FROM probid_winners WHERE auctionid='".$relistId."'", "quant_o");
	$qu_total = $qu_a + $qu_b;
	$qu_total = ($auctionDetails['auctiontype']=="dutch") ? $qu_total : 1;

	## if item is listed in both, and storeactive=false, then only list in auction
	$listIn = $itemListedIn ;
	$relistAuctionDets_1[$relist_counter] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
	startdate='".$timeNowMysql."',enddate='".$closingdate."',duration='".$_POST['duration'][$relistId]."',
	closed=0,nrbids=0,maxbid=0, quantity='".$qu_total."', 
	hlitem='".$hlitem_chg."', bolditem='".$bolditem_chg."', hpfeat='".$hpfeat_chg."', catfeat='".$catfeat_chg."', 
	listin='".$listIn."', clicks=0 
	WHERE id='".$newId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	//echo "You have to pay the following fees: $topay";
			
	### check in auction watch table for keywords matching ours and send email to the user interested.
	$keywords = getSqlField("SELECT keywords FROM probid_auctions WHERE id='".$newId."'","keywords");
			
	$getKeywords = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_keywords_watch");
	while ($keywordsW=mysqli_fetch_array($getKeywords)) {
		if (@eregi($keywordsW['keyword'],$keywords)) {
			$userId = $keywordsW['bidderid'];
			$auctionId = $newId;
			$keyword = $keywordsW['keyword'];
			include ("../mails/keywordmatch.php");
		}
	}
	### send confirmation email to the seller
	$userId = $auctionDetails['ownerid'];
	$auctionId = $newId;
	include ("../mails/confirmtoseller.php");
	
	$auctionRelisted = TRUE;
}

if ($_GET['option']=="deleteall") {
	$getDeletedAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_auctions WHERE deleted=1");
	while ($deletedAuction = mysqli_fetch_array($getDeletedAuctions)) {
		deleteAuction($deletedAuction['id'],TRUE);
	}
}

## new activate and delete functions
if (isset($_GET['processauctions'])) {
	## delete checked auctions
	for ($i=0;$i<count($_GET['delete']);$i++) {
		deleteAuction($_GET['delete'][$i],TRUE);
	}
	## activate checked auctions
	for ($i=0;$i<count($_GET['activate']);$i++) {
		$changeStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET active='1' WHERE id='".$_GET['activate'][$i]."'");
		$auction = getSqlRow("SELECT id,category,addlcategory,deleted,closed FROM probid_auctions WHERE id='".$_GET['activate'][$i]."'");		
		if ($auction['active']==1&&$auction['deleted']!=1&&$auction['closed']==0) {
			addcatcount($auction['category'],$auction['id']);
			addcatcount($auction['addlcategory'],$auction['id']);
		}
	}
	## inactivate checked auctions
	for ($i=0;$i<count($_GET['inactivate']);$i++) {
		$changeStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET active='0' WHERE id='".$_GET['inactivate'][$i]."'");
		$auction = getSqlRow("SELECT id,category,addlcategory,deleted,closed FROM probid_auctions WHERE id='".$_GET['inactivate'][$i]."'");		
		if ($auction['active']==0&&$auction['deleted']!=1&&$auction['closed']==0) {
			delcatcount($auction['category'],$auction['id']);
			delcatcount($auction['addlcategory'],$auction['id']);
		}
	}
	## approve checked auctions
	for ($i=0;$i<count($_GET['approve']);$i++) {
		$pm_status = getSqlField("SELECT payment_status FROM probid_auctions WHERE id='".$_GET['approve'][$i]."'", "payment_status");
		if (@eregi("confirmed",$pm_status)) $addlQuery = ", active=1 ";
		$approveAuct[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET approved=1".$addlQuery." WHERE id='".$_GET['approve'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$auction = getSqlRow("SELECT id,category,addlcategory,deleted,closed FROM probid_auctions WHERE id='".$_GET['approve'][$i]."'");		
		if (@eregi("confirmed",$pm_status)&&$auction['deleted']!=1&&$auction['closed']==0) {
			addcatcount($auction['category'],$auction['id']);
			addcatcount($auction['addlcategory'],$auction['id']);
		}
		$auctionId = $auction['id'];
		include('../mails/approvalnotification.php');
	}
}

include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang"); ?>
<script language="Javascript">
<!--
function checkAll(field, array_len, check) {
	if (array_len == 1) { 
		field.checked = check;
	} else {
		for (i = 0; i < array_len; i++)
			field[i].checked = check ;
	}
}

function checkAll2(field_a, field_b, array_len, check) {
	if (check==true) check2=false;
	else check2=true;

	if (array_len == 1) {
		field_a.checked = check;
		field_b.checked = check2;
	} else {
		for (i = 0; i < array_len; i++)
			field_a[i].checked = check ;
		for (i = 0; i < array_len; i++)
			field_b[i].checked = check2 ;
	}
}

-->
</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_auct.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? 
			echo $a_lang[AUCT_MANAGE]." / "; 
			if ($_REQUEST['page']=="open") echo $a_lang[AUCT_ALL_ACTIVE];
			else if ($_REQUEST['page']=="closed") echo $a_lang[AUCT_CLOSED];
			else if ($_REQUEST['page']=="suspended") echo $a_lang[AUCT_SUSP];
			else if ($_REQUEST['page']=="unstarted") echo $a_lang[AUCT_UNSTARTED];
			else if ($_REQUEST['page']=="approval") echo $a_lang[AUCT_APPROVAL];
			?>
         &nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table border="0" cellpadding="4" cellspacing="2" class="border" align="center">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <input type="hidden" name="page" value="<?=$_REQUEST['page'];?>">
      <tr class="c3">
         <td colspan="3"><strong>
            <?=$a_lang[AUCT_SEARCH];?>
            </strong></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[ENTER_KEYWORDS_SEARCH];?>
            :</td>
         <td colspan="2"><input name="keywords" type="text" id="keywords" size="25"></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[ENTER_AID_SEARCH];?>
            :</td>
         <td><input name="src_aid" type="text" id="src_aid" size="15"></td>
         <td><input name="auctionsearchok" type="submit" id="auctionsearchok" value="<?=$a_lang[BUTT_SEARCH];?>"></td>
      </tr>
   </form>
</table>
<br>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[AUCT_TABLE_UPDATED]."</p>":""; ?> <? echo ($auctionRelisted=="yes")?"<p align=\"center\">".$a_lang[AUCTION_RELISTED]."</p>":""; ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr class="c3">
      <td align="center" colspan="5"><b>
         <?
			if ($_REQUEST['page']=="open") echo $a_lang[AUCT_ACTIVE_MANAGEMENT];
			else if ($_REQUEST['page']=="closed") echo $a_lang[AUCT_CLOSED_MANAGEMENT];
			else if ($_REQUEST['page']=="suspended") echo $a_lang[AUCT_SUSP_MANAGEMENT];
			else if ($_REQUEST['page']=="approval") echo $a_lang[AUCT_APPROVAL_MANAGEMENT];
			else if ($_REQUEST['page']=="unstarted") echo $a_lang[AUCT_UNSTARTED_MANAGEMENT];
			?>
         </b></td>
   </tr>
   <?  
	if (isset($_POST['auctionsearchok'])||$_REQUEST['keywords']!=""||$_REQUEST['src_aid']!=""||$_REQUEST['ownerid']>0) {
		if ($_REQUEST['keywords']!="") 
			$searchPattern .= "AND keywords LIKE '%".$_REQUEST['keywords']."%'";
		if ($_REQUEST['src_aid']!="") 
			$searchPattern .= " AND id LIKE '%".$_REQUEST['src_aid']."%'";
		if ($_REQUEST['ownerid']>0) 
			$searchPattern .= " AND ownerid='".$_REQUEST['ownerid']."'";
	}
	if ($_GET['start'] == "") $start = 0;
	else $start = $_GET['start'];
	$limit = 20;
	if (!$_REQUEST['orderField']) $orderField = "id";
	else $orderField=$_REQUEST['orderField'];
	if (!$_REQUEST['orderType']) {
		$orderType = "DESC";
		$newOrder="ASC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}
	$additionalVars = "&page=".$_REQUEST['page']."&keywords=".$_REQUEST['keywords']."&ownerid=".$_REQUEST['ownerid'];
	## depending on the auction select which are requested
	if ($_REQUEST['page']=="open") $whereClause = "WHERE closed=0 AND active=1";
	else if ($_REQUEST['page']=="closed") $whereClause = "WHERE active=1 AND closed=1 AND enddate<='".$timeNowMysql."'";
	else if ($_REQUEST['page']=="suspended") $whereClause = "WHERE active=0 AND approved=1";
	else if ($_REQUEST['page']=="unstarted") $whereClause = "WHERE active=1 AND closed=1 AND enddate>'".$timeNowMysql."'";
	else if ($_REQUEST['page']=="approval") $whereClause = "WHERE active=0 AND approved=0";

	$nbAuctions = getSqlNumber("SELECT * FROM probid_auctions ".$whereClause." ".$searchPattern."
	ORDER BY ".$orderField." ".$orderType."");
	$nbAuctionsMD = getSqlNumber("SELECT * FROM probid_auctions ".$whereClause." AND deleted!=0 ".$searchPattern."
	ORDER BY ".$orderField." ".$orderType."");
	 
	$getAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions ".$whereClause." ".$searchPattern."
	ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");
	?>
   <tr>
      <td height="30" align="center" colspan="5"><?=$a_lang[YOUR_QWERY_MATCHED];?>
         <?=$nbAuctions;?>
         <?=$a_lang[RESULTS];?>
         ,
         <?=$nbAuctionsMD;?>
         <?=$a_lang[MARKED_DELETED];?>
         .</td>
   </tr>
   <form action="auctionsmanagement.php" method="get" name="select_auctions">
      <input type="hidden" name="start" value="<?=$_REQUEST['start'];?>">
      <input type="hidden" name="orderField" value="<?=$orderField;?>">
      <input type="hidden" name="orderType" value="<?=$orderType;?>">
      <input type="hidden" name="page" value="<?=$_REQUEST['page']?>">
      <input type="hidden" name="keywords" value="<?=$_REQUEST['keywords'];?>">
      <tr class="c4">
         <td width="100"><a href="auctionsmanagement.php?start=<?=$_REQUEST['start'];?>&orderField=itemname&orderType=<?=$newOrder.$additionalVars;?>"><font color="#EEEE00">
            <?=$a_lang[ITEM_NAME];?>
            </font></a></td>
         <td ><?=$a_lang[AUCTION_DETAILS];?></td>
			<? if ($_REQUEST['page']=="approval") { ?>
         <td align="center" width="100"><?=$a_lang[APPROVE];?>
            <br>
            <? echo "[ <a href=\"javascript:void(0);\" onclick=\"checkAll(document.select_auctions['approve[]'], ".$nbAuctions.", true);\"><font color=\"#EEEE00\">".$a_lang[all]."</font></a> | 
				<a href=\"javascript:void(0);\" onclick=\"checkAll(document.select_auctions['approve[]'], ".$nbAuctions.", false);\"><font color=\"#EEEE00\">".$a_lang[none]."</font></a> ]"; ?></td>
         <? } else { ?>
			<td align="center" width="100"><?=$a_lang[STATUS];?>
            <br>
            <? echo "[ <a href=\"javascript:void(0);\" onclick=\"checkAll2(document.select_auctions['activate[]'], document.select_auctions['inactivate[]'], ".$nbAuctions.", true);\"><font color=\"#EEEE00\">".$a_lang[activate_all]."</font></a> ]<br> 
				[ <a href=\"javascript:void(0);\" onclick=\"checkAll2(document.select_auctions['activate[]'], document.select_auctions['inactivate[]'], ".$nbAuctions.", false);\"><font color=\"#EEEE00\">".$a_lang[inactivate_all]."</font></a> ]<br>
				[ <a href=\"auctionsmanagement.php?start=".$_REQUEST['start']."&orderField=".$orderField."&orderType=".$newOrder.$additionalVars."\"><font color=\"#EEEE00\">".$a_lang[default_all]."</font></a> ]"; ?></td>
			<? } ?>
         <td align="center" width="80"><?=$a_lang[DELETE];?>
            <br>
            <? echo "[ <a href=\"javascript:void(0);\" onclick=\"checkAll(document.select_auctions['delete[]'], ".$nbAuctions.", true);\"><font color=\"#EEEE00\">".$a_lang[all]."</font></a> | 
				<a href=\"javascript:void(0);\" onclick=\"checkAll(document.select_auctions['delete[]'], ".$nbAuctions.", false);\"><font color=\"#EEEE00\">".$a_lang[none]."</font></a> ]"; ?></td>
      </tr>
      <? 
		while ($auctionDetails = mysqli_fetch_array($getAuctions)) { 
			$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$auctionDetails['ownerid']."'");
		?>
      <tr class="<? echo (($count++)%2==0)?"c1":"c2";?>">
         <td><a href="../auctiondetails.php?id=<?=$auctionDetails['id'];?>" target="new">
            <?=$auctionDetails['itemname'];?>
            </a><br>
            [ <a href="editauction.php?id=<?=$auctionDetails['id'];?>">
            <?=$a_lang[EDIT];?>
            </a> ] <? echo ($auctionDetails['deleted']!=0)?"<br><strong>(".$a_lang[MARKED_DELETED].")</strong>":"";?>
            <? if ($_REQUEST['page']=="closed") {
					echo "<br>[ <a href=\"auctionsmanagement.php?page=".$_REQUEST['page']."&option=relist&id=".$auctionDetails['id']."&duration=".$auctionDetails['duration']."\">$a_lang[RELIST]</a> ]"; 

	            $getNumberOfMessages = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT COUNT(*) AS num FROM probid_chatrooms AS c, probid_winners AS w
					WHERE w.auctionid='".$auctionDetails['id']."' AND c.winnerid=w.id") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
					$res = mysqli_fetch_array($getNumberOfMessages);
            	if($res['num'] > 0) echo '<br>[ <a href="msgboardsmanagement.php?auctionid='.$auctionDetails['id'].'">'.$a_lang[m_view_messages].'</a> ]';
        		} ?>
         </td>
         <td><?=auctionListedIn($auctionDetails['id'],TRUE); ?>
            <table width="100%"  border="0" cellspacing="1" cellpadding="1">
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td nowrap><?=$a_lang[USER];?></td>
                  <td width="100%"><?=$userDetails['username'];?></td>
               </tr>
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td nowrap><?=$a_lang[DATE_STARTED];?></td>
                  <td><? echo convertdate($auctionDetails['startdate'],$setts['date_format']);?></td>
               </tr>
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td nowrap><?=$a_lang[END_DATE];?></td>
                  <td><? echo convertdate($auctionDetails['enddate'],$setts['date_format']);?></td>
               </tr>
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td nowrap><?=$a_lang[DURATION];?></td>
                  <td><?=$auctionDetails['duration'];?>
                     <?=$a_lang[DAYS];?></td>
               </tr>
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td nowrap><?=$a_lang[CATEGORY];?></td>
                  <td><? echo getSqlField("SELECT name FROM probid_categories WHERE id='".$auctionDetails['category']."'","name"); ?></td>
               </tr>
            </table></td>
         <? if ($_REQUEST['page']=="approval") { ?>
			<td align="center"><input name="approve[]" type="checkbox" id="approve[]" value="<?=$auctionDetails['id'];?>" class="checkapprove"></td>
			<? } else { ?>
         <td align="center"><table border="0" cellspacing="1" cellpadding="1">
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td><?=$a_lang[ACTIVE];?></td>
                  <td><input name="activate[]" type="checkbox" id="activate[]" value="<?=$auctionDetails['id'];?>" class="checkactivate" <? echo ($auctionDetails['active']==1) ? "checked":""; ?>></td>
               </tr>
               <tr class="<? echo (($count)%2==0)?"c1":"c2";?>">
                  <td><?=$a_lang[INACTIVE];?></td>
                  <td><input name="inactivate[]" type="checkbox" id="inactivate[]" value="<?=$auctionDetails['id'];?>" class="checkinactivate" <? echo ($auctionDetails['active']==0) ? "checked":""; ?>></td>
               </tr>
            </table></td>
			<? } ?>
         <td align="center"><input name="delete[]" type="checkbox" id="delete[]" value="<?=$auctionDetails['id'];?>" class="checkdelete"></td>
      </tr>
      <? } ?>
      <tr class="c4">
         <td colspan="5" align="center"><input name="processauctions" type="submit" value="<?=$a_lang[makechanges]?>"></td>
      </tr>
   </form>
</table>
<table width="100%"  border="0" cellspacing="4" cellpadding="4">
   <tr>
      <td align="center"><? paginate($start,$limit,$nbAuctions,"auctionsmanagement.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); ?></td>
   </tr>
   <tr>
      <td align="center"><a href="auctionsmanagement.php?page=<?=$_REQUEST['page'];?>&option=deleteall">
         <?=$a_lang[DELETE_ALL_MARKED_DELETED];?>
         </a> (<? echo getSqlNumber("SELECT id FROM probid_auctions WHERE deleted=1")." ".$a_lang[RESULTS];?>)</td>
   </tr>
</table>
<? include ("footer.php"); 
} ?>
