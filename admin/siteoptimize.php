<?
## v5.24 -> apr. 07, 2006
session_start();
if ($_SESSION['adminarea']!="Active") 
{
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");
include ("header.php");

function getLang($selected) {

  global $arrDomLang;
  
  $select = "<select name='mylang'>";
  
  foreach ($arrDomLang as $row)
  {
    $select .= "<option ".($selected==$row["value"]?"selected":"")." value='".$row["value"]."'>".$row["name"];
  }
  
  $select .= "</select>";
  
  return $select;
}			


if (!isset($_POST["mylang"])) $_POST["mylang"] = "est";
if (($_POST["act"]=="update")) $_POST["action"] = "m_update"; // Some fix for update :)

$mLang = $_POST["mylang"];

// Action

switch ($_POST["action"]) 
{
  case "m_delete":
    if (is_array($_POST["meta"]) AND sizeof($_POST["meta"])>0) 
    {
      foreach ($_POST["meta"] as $row) 
      {
        $counter++;
        $ids.=$row.($counter<count($_POST["meta"])?',':'');    
      }
         $query = "DELETE FROM probid_search_meta WHERE id IN(".$ids.")";
         mysqli_query($GLOBALS["___mysqli_ston"], $query);
    } 
  	break;
  case "m_update":
    $query = "UPDATE probid_search_meta SET filename='".$_POST["filename"]."', name='".$_POST["name"]."', words='".$_POST["words"]."' WHERE id=".$_POST["mid"]."";
    mysqli_query($GLOBALS["___mysqli_ston"], $query);
    break;
   case "m_insert":
     $query = "INSERT INTO probid_search_meta(filename,name,words,lang) VALUES('".$_POST["filename"]."','".$_POST["name"]."','".$_POST["words"]."','".$mLang."')";
    mysqli_query($GLOBALS["___mysqli_ston"], $query);
    break;

}


?>
<form name="langForm" method="post" action="siteoptimize.php">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;"> 
  <tr> 
    <td width="200">
        SELECT ROOT LANGUAGE
    </td>
    <td width="200">
        <?=getLang($_POST["mylang"])?>
    </td>
    <td>
        <input type="submit" value="OK">
    </td>
  </tr>
</table>
</form>

<form name="addCat" method="post" action="siteoptimize.php">
<table border="0" cellpadding="0" cellspacing="0"> 
<tr>
    <td colspan="2">Add </td>
</tr>
<tr>
    <td>Filename</td><td><input class="edit" type="text" size="20" name="filename"></td>
</tr>
<tr>
    <td>Name *</td><td><input class="edit" type="text" size="20" name="name"></td>
</tr>
<tr>
    <td colspan="2">Words</td>
</tr>
<tr>
    <td colspan="2"><textarea name="words" rows="10" cols="100"></textarea></td>
</tr>
<tr>
    <td><input type="submit" value="Add"><input type="hidden" name="action" value="m_insert"><input type="hidden" value="<?=$_POST["mylang"]?>" name="mylang"></td>
</tr>
 </table> 
</form>
<?

// list of search content
?>
<form name="dataForm" method="post" action="siteoptimize.php">
<table border="0" cellpadding="2" cellspacing="0" style="width:90%;"> 
<tr>
    <td colspan="5">TT</td>
</tr>
<tr>
    <td width="5%">#</td>
    <td width="10%">filename</td>
    <td width="10%">name</td>
    <td width="65%">words</td>
    <td width="10%"><input type="checkbox" onclick="javascript:checker()" title="Check all" name="master_box"/></td>
</tr>
<?

  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_search_meta WHERE lang='".$mLang."'");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $arrList[] = $row; 
  }
  
  if ((is_array($arrList)) && (sizeof($arrList)>0))
  {
    // list
      foreach ($arrList as $row) 
      {
              $counter++;
      if (($row["id"] == $_POST["meta"][0]) && ($_POST["action"]!=="delete"))  {
        ?><tr>
        <td valign="top"><?=$counter?></td>
        <td valign="top"><input class="edit" type="text" size="20" value="<?=$row["filename"]?>" name="filename"></td>
        <td valign="top"><input class="edit" type="text" size="20" value="<?=$row["name"]?>" name="name"></td>
        <td><textarea name="words" rows="10" cols="100"><?=$row["words"]?></textarea></td>
        <td><input type="submit" value="UPDATE" name="button"><input type="hidden" value="<?=$row["id"]?>" name="mid"><input type="hidden" value="update" name="act"></td>
        </tr><?        
      
      } else {     
        ?><tr>
        <td><?=$counter?></td>
        <td><?=$row["filename"]?></td>
        <td><?=($row["name"]=="description" ||$row["name"]=="author"?"<b>".$row["name"]."</b>":$row["name"])?></td>
        <td><?=$row["words"]?></td>
        <td><input type="checkbox" value="<?=$row["id"]?>" name="meta[]"/></td>
        </tr><?
      }
      }
  }

?>
</table>
<table style="width:90%;" cellpadding="0">
<tr>
    <td align="right">Action:
        <select name="action" class="action">
          <option value="edit">Edit</option>
          <option value="m_delete">Delete</option>
        </select>
        <input type="hidden" value="<?=$_POST['mylang']?>" name="mylang">
        <input class="buttons" value="OK" type="button" onclick="dataForm.submit();">
  </td>
</tr>
<tr>
    <td>* - name values: <b>description, keywords, author</b></td>
</tr>
</table>
</form>
<?

include ("footer.php"); 

} ?>

