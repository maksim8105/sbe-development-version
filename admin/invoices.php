<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");


include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");

function format_date($date, $mode, $delimiter = ' - ', $clock = 0) {
//2002-04-16 22:01:45
//0123456789012345678
 global $msg;
 global $month;
 global $month2;
 $year=substr($date, 0, 4);
 $month_val=substr($date, 5, 2);
 $month_num=intval($month_val);
 $day=substr($date, 8, 2);
 $hour=substr($date, 11, 2);
 $min=substr($date, 14, 2);
 $sec=substr($date, 17, 2);
 //10.03.2002 - 02:10
 if ($mode==1) {$output=$day.".".$month_val.".".$year." - ".$hour.":".$min;}
 //10.03.2002
 if ($mode==2) {$output=$day.".".$month_val.".".$year;}
 //12:15
 if ($mode==3) {$output=$hour.":".$min;}
 //20. october 2004
 if ($mode==4) {$output="$day. $month[$month_num] $year";}
 //octjabrja 8, 2004
 if ($mode==5) {$output="$month2[$month_num] $day, $year";}
 if ($mode==7) {$output=$day.'.'.$month_val.'.'.$year; $clock = 1;} //alfa.fi
 if ($mode==6) {
	# from dd.mm.yyyy - hh:mm
	# to yyyy-mm-dd hh:mm
	$d_length  = strlen($delimiter);
	$pos = strpos($date, $delimiter);
	if ($pos > 0) {
		$aeg_ee  = substr($date, 0, $pos);
		$time_ee = trim(substr($date, $pos + $d_length));
	} else {
		$aeg_ee  = $date;
	}
	$format = 'dd.mm.yyyy';
	$month  = substr($aeg_ee, strpos($format, 'mm'), 2);
	$day    = substr($aeg_ee, strpos($format, 'dd'), 2);
	if(strstr($format, 'yyyy')) {
		$year = substr($aeg_ee, strpos($format, 'yyyy'), 4);
	} else {
		$year = '20'.substr($aeg_ee, strpos($format, 'yy'), 2);
	}
	$output = $year.'-'.$month.'-'.$day.($time_ee ? " ".$time_ee : "");
 }
 if ($clock) $output.=" ".$msg[1000][35];
 return $output;
}

function usersInvoices($arg) {

    $arrSort = array("cdate","username","realname","amount","status","transfer");
    $arrAw = array("asc","desc");
    
    if (($_POST['sort'])!='') {
    
      if ((in_array($_POST["sort"],$arrSort)) && (in_array($_POST["arrow"],$arrAw)))
      {
        $order = " ORDER BY ".$arg['sort']." ".$arg['arrow']."";
      }
    
      
    } else {
        $order = " ORDER BY probid_users_invoices.id ASC";
    }
    
     if (($_POST['start_date']!='' AND $_POST['end_date']!='') AND (strtotime($_POST['start_date']) <= strtotime($_POST['end_date']))) 
     {
        $period = "AND probid_users_invoices.cdate BETWEEN '".$_POST['start_date']." 00:00:00' AND '".$_POST['end_date']." 23:59:59'";
     } else {
        $period = "AND (MONTH(CURDATE()) = MONTH(probid_users_invoices.cdate))";
     }     
     
     if (($_POST["username"])!="")
     {
       $username=" AND probid_users_invoices.username='".$_POST["username"]."' ";
     }
     
     $criterias = $period.$username;
     

     
    $result = @mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * 
            FROM probid_users_invoices
            WHERE amount LIKE '%%'  ".$criterias." ".$order."");

    while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $arrInvoices[] = $row; 
    }
return $arrInvoices;
}


function status_box($checked,$id) {

  global $msg;
  

  $arrStatus = array(0=>"awaiting",1=>"confirmed",2=>"declined");
  
  for ($i=0;$i<3;$i++) {
  
  
  
  $box.= "<input type='radio' name='status_".$id."'  value='".$i."' ".($checked==$i?'checked':'').">".$arrStatus[$i];
  
  }
  
  return $box;

}

switch ($_POST["action"])
{
  case "save":
    if (is_array($_POST["invoices"]))
    {
        foreach ($_POST["invoices"] as $inv_id)
        {
            if ($_POST["status_".$inv_id]==1)
            {
              // CONFIRMED
              
              #Check previous invoice status
              $invoice = getSqlRow("SELECT status FROM probid_users_invoices WHERE id=".$inv_id."");  
              if ($invoice["status"] == 0)  
              {
                #We can update invoice status
                mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users_invoices SET status=".$_POST["status_".$inv_id]." WHERE id=".$inv_id."");
                #User balance updated
                $invoice = getSqlRow("SELECT userid,amount FROM probid_users_invoices WHERE id=".$inv_id."");
                $current = getSqlRow("SELECT balance FROM probid_users WHERE id=".$invoice["userid"].""); 
                mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET w_amount='0',balance='".($current["balance"]+$invoice["amount"])."' WHERE id=".$invoice["userid"]."");
                #Transaction    
                $sqlTran = "INSERT INTO probid_users_transactions(userid,name,description,amount,balance,op,tdate) 
                            VALUES (".$invoice["userid"].",'SBE SÜSTEEM','Väljamakse SBE kontolt','".$invoice["amount"]."','".abs($current["balance"]+$invoice["amount"])."','out','".date("Y-m-d H:i:s")."')";
                mysqli_query($GLOBALS["___mysqli_ston"], "SET CHARACTER SET utf8");
                mysqli_query($GLOBALS["___mysqli_ston"], "SET NAMES utf8");
                mysqli_query($GLOBALS["___mysqli_ston"], $sqlTran);                  
              }                       
            } elseif($_POST["status_".$inv_id]==2) {
                // DECLINE
                mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users_invoices SET status=".$_POST["status_".$inv_id]." WHERE id=".$inv_id."");
                #GET userid
                $invoice = getSqlRow("SELECT userid FROM probid_users_invoices WHERE id=".$inv_id."");
                #REMOVE RESERVED AMOUNT
                mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET w_amount='0' WHERE id=".$invoice["userid"]."");
                
            } elseif($_POST["status_".$inv_id]==0) {
                mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users_invoices SET status=".$_POST["status_".$inv_id]." WHERE id=".$inv_id."");
            }
        }
    }
  
  	break;
  case "delete":
    if (is_array($_POST["invoices"]))
    {
        foreach ($_POST["invoices"] as $id)
        {
          mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_users_invoices WHERE id=".$id."");
        }      
    }
    break;

}

$results = usersInvoices($_POST);

?>
<form name="dataForm" method="post" action="invoices.php">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
<tr>
    <td>
        <table>
          <tr>
              <td>Period</td>
              <td>
                <table>
                <tr>
                    <td><input class="date" type="text"  name="start_date" value="<?=$_POST['start_date']?>"/></td>
                    <td><a href="#" onclick="displayDatePicker('start_date');"><img alt="" border="0" src="<?="../images/calendar.jpg"?>"></a></td>
                    <td> - </td>
                    <td><input class="date" type="text"  name="end_date" value="<?=$_POST['end_date']?>"/></td>
                    <td><a href="#" onclick="displayDatePicker('end_date');"><img alt="" border="0" src="<?="../images/calendar.jpg"?>"></a></td>
                </tr>
                </table>
              </td>
          </tr>
          <tr>
              <td>Username</td><td><input id="req_user" style="font-weight:bold;" type="text" value="<?=$_POST["username"]?>" name="username"></td>
          </tr>
          <tr>
              <td><input type="submit" value="Show"></td>
          </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table style="width:100%;">
        <tr>
        <td>#<a href="javascript:sortBy('id','asc');"><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('id','desc')"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Date<a href="javascript:sortBy('cdate','asc');"><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('cdate','desc')"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Realname<a href="javascript:sortBy('realname','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('realname','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Acc nr.<a href="javascript:sortBy('acc_nr','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('acc_nr','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Amount<a href="javascript:sortBy('amount','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('amount','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Transfer<a href="javascript:sortBy('transfer','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('transfer','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Status<a href="javascript:sortBy('status','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('status','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td align="center"><input type="checkbox" onclick="javascript:checker()" title="Check all" name="master_box"/></td>
        </tr>
        <?
        if ((is_array($results)) && (sizeof($results)>0)) 
        {
            foreach ($results as $t) 
            {
              if ($data['p']>1) 
              {
                $number=(20*($data['p']-1));
                $x++;
              } else {
                $number++;
              }
                ?><tr class="<?=(($number%2==0)?'c2':'c1')?>"><td><?=$number?></td><td width="120"><?=format_date($t["cdate"],1)?></td><td><?=utf8_encode($t["realname"])?>(<a href="#" onclick="javascript:filterBy('username','<?=$t["username"]?>')"><?=$t["username"]?></a>)</td><td><?=$t["acc_nr"]?></td><td style="color:red">-<?=displayAmount($t["amount"])?></td><td style="color:green"><b>+<?=displayAmount($t["transfer"])?></b></td><td><?=status_box($t["status"],$t["id"])?></td><td align="center" class="list"><input type="checkbox" value="<?=$t["id"]?>" name="invoices[]"/></td></tr><?
            }
        } else {
        
          ?><tr><td colspan="6" align="center"> Счетов не найдено </td></tr><?
        
        }
        ?>
    </td>
</tr>
<tr>
    <td>
    <input type="hidden" value="<?=$_POST["status"]?>"  name="status" id="req_status"/>
    <input type="hidden" value="<?=$_POST["arrow"]?>"  name="arrow" id="arrow"/>
    <input type="hidden" value="<?=$_POST["sort"]?>"  name="sort" id="sort"/>    
    </td>
</tr>
</table>
  <table style="width:100%;" cellpadding="0" align="center">
  <tr>
      <td align="right">Action:
          <select name="action">
            <option value="save">save</option>
            <option value="delete">delete</option>
          </select>
          <input class="buttons" value="OK" type="button" onclick="dataForm.submit();">
    </td>
  </tr>
  </table>
</form>
<?

include ("footer.php"); 
} ?>
