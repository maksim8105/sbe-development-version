<table width="100%" border="0" cellpadding="4" cellspacing="0"> 
  <tr class="header"> 
    <td><img src="images/folder.gif"></td> 
    <td width="100%"><a href="javascript: d.openAll();"> 
      <?=$a_lang[OPEN_ALL];?> 
      </a> | <a href="javascript: d.closeAll();"> 
      <?=$a_lang[CLOSE_ALL];?> 
      </a></td> 
  </tr> 
</table>
<br> 
<div class="dtree" style="margin-left: 2px;"> 
  <script type="text/javascript">
	
		<!--

		d = new dTree('d');

		d.add(0,-1,'<b><?=$a_lang[ADMIN_HOME];?></b>','index.php');
		d.add(100,0,'<?=$a_lang[SITE_SETUP];?>','sitesetup.php');
		d.add(101,0,'<?=$a_lang[INIT_COUNTERS];?>','../initialize.counters.php','_blank');
		
		d.add(2,0,'<?=$a_lang[GEN_SET];?>','');
		d.add(200,2,'<?=$a_lang[CLOSED_AUCT_DEL];?>','generalsettings.php?page=CAD');
		d.add(201,2,'<?=$a_lang[HOME_ITEMS];?>','generalsettings.php?page=HPFI');
		d.add(202,2,'<?=$a_lang[CAT_ITEMS];?>','generalsettings.php?page=CFI');
		d.add(203,2,'<?=$a_lang[LAST_CREATED_AUCT];?>','generalsettings.php?page=LCA');
		d.add(204,2,'<?=$a_lang[HOT_AUCT];?>','generalsettings.php?page=HA');
		d.add(205,2,'<?=$a_lang[END_SOON_AUCT];?>','generalsettings.php?page=ESA');
		d.add(206,2,'<?=$a_lang[ADD_PICT];?>','generalsettings.php?page=AAP');
		d.add(207,2,'<?=$a_lang[CURRENCY_SETT];?>','generalsettings.php?page=CS');
		d.add(208,2,'<?=$a_lang[TIME_SETT];?>','generalsettings.php?page=TDS');
		d.add(209,2,'<?=$a_lang[SSL_SUPPORT];?>','generalsettings.php?page=SSL');
		d.add(210,2,'<?=$a_lang[META_TAGS];?>','generalsettings.php?page=MT');
		d.add(211,2,'<?=$a_lang[CRONJOB_SETTINGS];?>','generalsettings.php?page=AUS'); 
		d.add(212,2,'<?=$a_lang[MIN_REG_AGE_SETTINGS];?>','generalsettings.php?page=MRA'); 
		d.add(213,2,'<?=$a_lang[RECENT_WANTADS];?>','generalsettings.php?page=RWA'); 
		d.add(214,2,'<?=$a_lang[MOVIE_SETTINGS];?>','generalsettings.php?page=MOV'); 
		d.add(215,2,'<?=$a_lang[BUYOUT_PROCEDURE];?>','generalsettings.php?page=BOP'); 
		d.add(216,2,'<?=$a_lang[SELL_NAV_POSITION];?>','generalsettings.php?page=SNP'); 
		d.add(217,2,'<?=$a_lang[MAX_AUTO_RELISTS_NB];?>','generalsettings.php?page=ARN'); 
		
		d.add(3,0,'<?=$a_lang[ENA_DIS];?>','');
		d.add(300,3,'<?=$a_lang[SHIP_COST];?>','enabledisable.php?page=ESC');
		d.add(301,3,'<?=$a_lang[LOGIN_BOX];?>','enabledisable.php?page=OLB');
		d.add(302,3,'<?=$a_lang[NEWS_BOX];?>','enabledisable.php?page=DNB');
		d.add(303,3,'<?=$a_lang[A_BUYNOW];?>','enabledisable.php?page=ABN');
		d.add(304,3,'<?=$a_lang[SHOW_ACCEPT];?>','enabledisable.php?page=SAT');
		d.add(305,3,'<?=$a_lang[SHOW_TC];?>','enabledisable.php?page=STCB');
		d.add(306,3,'<?=$a_lang[A_HOME_ITEMS];?>','enabledisable.php?page=AHPFI');
		d.add(307,3,'<?=$a_lang[A_CAT_ITEMS];?>','enabledisable.php?page=ACFI');
		d.add(308,3,'<?=$a_lang[A_HL_ITEMS];?>','enabledisable.php?page=AHI');
		d.add(309,3,'<?=$a_lang[A_BOLD_ITEMS];?>','enabledisable.php?page=ABI');
		d.add(310,3,'<?=$a_lang[A_SWAP];?>','enabledisable.php?page=AISF');
		d.add(311,3,'<?=$a_lang[A_COUNTER];?>','enabledisable.php?page=AC');
		d.add(312,3,'<?=$a_lang[A_SECOND_CAT];?>','enabledisable.php?page=ELSC');
		d.add(313,3,'<?=$a_lang[A_DIRECTPAYMENT];?>','enabledisable.php?page=EPDP');
		d.add(314,3,'<?=$a_lang[USER_LANG];?>','enabledisable.php?page=LANG');
		d.add(315,3,'<?=$a_lang[SNIP_FEAT];?>','enabledisable.php?page=SNIP');
		d.add(316,3,'<?=$a_lang[PRIV_SITE];?>','enabledisable.php?page=PRIV');
		d.add(317,3,'<?=$a_lang[PREF_SEL];?>','enabledisable.php?page=PREF_SEL');
		d.add(318,3,'<?=$a_lang[BCC];?>','enabledisable.php?page=BCC');
		d.add(319,3,'<?=$a_lang[EASQ];?>','enabledisable.php?page=EASQ');
		d.add(320,3,'<?=$a_lang[ERA];?>','enabledisable.php?page=ERA');
		d.add(321,3,'<?=$a_lang[EWNTA];?>','enabledisable.php?page=EWNTA');
		d.add(322,3,'<?=$a_lang[EBR];?>','enabledisable.php?page=EBR');
		d.add(323,3,'<?=$a_lang[EOISA];?>','enabledisable.php?page=EOISA');
		d.add(324,3,'<?=$a_lang[BLKLST];?>','enabledisable.php?page=BLKLST');
		d.add(325,3,'<?=$a_lang[ENABLE_CATCNT];?>','enabledisable.php?page=CATCNT');
		d.add(326,3,'<?=$a_lang[ENABLE_PHONEDISP];?>','enabledisable.php?page=PHONEDISP');
		d.add(327,3,'<?=$a_lang[ENABLE_AUCTIONS_APPROVAL];?>','auctionapproval.php');
		d.add(328,3,'<?=$a_lang[ENABLE_MOD_REWRITE];?>','enabledisable.php?page=MOD_REWRITE');

		d.add(4,0,'<?=$a_lang[EDIT_TABLES];?>','');
		d.add(400,4,'<?=$a_lang[EDIT_COUNTRIES];?>','table.countries.php');
		d.add(401,4,'<?=$a_lang[EDIT_A_DURATIONS];?>','table.auctionduration.php');
		d.add(402,4,'<?=$a_lang[EDIT_PAY_OPTIONS];?>','table.paymentoptions.php');
		d.add(403,4,'<?=$a_lang[EDIT_BID_INC];?>','table.bidincrements.php');
		d.add(404,4,'<?=$a_lang[EDIT_SHIP_OPT];?>','table.shippingoptions.php');
		d.add(405,4,'<?=$a_lang[EDIT_CURRENCIES];?>','table.currencies.php');
		d.add(406,4,'<?=$a_lang[EDIT_REFERRALS];?>','table.referrals.php');
		
		d.add(22,0,'<?=$a_lang[VAT_SETTINGS];?>','');
		d.add(2200,22,'<?=$a_lang[M_VAT];?>','enabledisable.php?page=USERVAT');
		d.add(2201,22,'<?=$a_lang[EDIT_VATSETTS];?>','table.vat.php');


		d.add(5,0,'<?=$a_lang[USER_MANAGE];?>','');
		d.add(500,5,'<?=$a_lang[USER_ADMIN];?>','adminmanagement.php');
		d.add(501,5,'<?=$a_lang[USER_MANAGE_LIST];?>','usersmanagement.php');
		d.add(507,5,'<?=$a_lang[LOGIN_AS_USER];?>','spoofer.php');
		d.add(502,5,'<?=$a_lang[ACTIVATION_EMAILS];?>','sendactivation.php');
		d.add(503,5,'<?=$a_lang[USER_NEWS];?>','sendnewsletter.php');
		d.add(504,5,'<?=$a_lang[USER_FEEDBACK];?>','editfeedbacks.php');
		d.add(507,5,'<?=$a_lang[MANAGE_CUST_REP_FLDS];?>','custrepfldmanagement.php');
		d.add(505,5,'<?=$a_lang[USER_ABUSE];?>','abusereports.php');
		d.add(506,5,'<?=$a_lang[USER_BAN];?>','banned.php');
		d.add(508,5,'<?=$a_lang[BLOCKED_USERS];?>','blockedusers.php');
		
		d.add(6,0,'<?=$a_lang[AUCT_MANAGE];?>','');
		d.add(600,6,'<?=$a_lang[AUCT_ALL_ACTIVE];?>','auctionsmanagement.php?page=open');
		d.add(607,6,'<?=$a_lang[AUCT_UNSTARTED];?>','auctionsmanagement.php?page=unstarted');
		d.add(608,6,'<?=$a_lang[AUCT_APPROVAL];?>','auctionsmanagement.php?page=approval');
		d.add(601,6,'<?=$a_lang[AUCT_SUSP];?>','auctionsmanagement.php?page=suspended');
		d.add(602,6,'<?=$a_lang[AUCT_CLOSED];?>','auctionsmanagement.php?page=closed');
		d.add(603,6,'<?=$a_lang[FIELDS_MANAG];?>','fieldsmanagement.php');
		d.add(604,6,'<?=$a_lang[WANTED_MANAG];?>','wantedmanagement.php');
		d.add(605,6,'<?=$a_lang[PUBLIC_QUESTIONS];?>','managepublicquestions.php');
		d.add(606,6,'<?=$a_lang[OLD_IMG_REMOVAL_TOOL];?>','imgremovaltool.php');
		d.add(609,6,'<?=$a_lang[WANTED_FIELDS_MANAG];?>','wantedfieldsmanagement.php');
		
		d.add(15,0,'<?=$a_lang[VOUCHERS];?>','');
		d.add(1500,15,'<?=$a_lang[MANAGE_VOUCHERS];?>','managevouchers.php');

		d.add(7,0,'<?=$a_lang[SITE_CONTENT];?>','');
		d.add(700,7,'<?=$a_lang[EDIT_HELP];?>','helptopics.php');
		d.add(701,7,'<?=$a_lang[ADD_NEWS];?>','newsmanagement.php');
		d.add(702,7,'<?=$a_lang[EDIT_FAQ];?>','faqsection.php');
		d.add(703,7,'<?=$a_lang[EDIT_ABOUT];?>','aboutpage.php');
		d.add(704,7,'<?=$a_lang[EDIT_CONTACT];?>','contactpage.php');
		d.add(705,7,'<?=$a_lang[EDIT_TC];?>','termspage.php');
		d.add(706,7,'<?=$a_lang[EDIT_PRIVACY];?>','pppage.php');
		d.add(707,7,'<?=$a_lang[EDIT_SYS_EMAIL];?>','editemails.php');
		d.add(708,7,'<?=$a_lang[EDIT_ERROR_PAGES];?>','errorpages.php');
		d.add(709,7,'<?=$a_lang[BANNER_AD_CONTROL];?>','advertsmanagement.php');
		d.add(710,7,'<?=$a_lang[EDIT_SITE_LANG];?>','editsitelang.php');
		d.add(711,7,'<?=$a_lang[EDIT_ANNOUNCEMENTS];?>','annmanagement.php');
		d.add(712,7,'<?=$a_lang[CUSTPGMANAG];?>','custompagesmanagement.php');
		
		d.add(8,0,'<?=$a_lang[FEES];?>','');
		d.add(800,8,'<?=$a_lang[FEES_MAIN];?>','fees.php');
		d.add(801,8,'<?=$a_lang[FEES_SETUP];?>','editfees.php');
		d.add(802,8,'<?=$a_lang[SETUP_PAY_SETT];?>','paymentgateway.php');
		d.add(803,8,'<?=$a_lang[ENABLE_VAT];?>','enabledisable.php?page=VAT');
		d.add(804,8,'<?=$a_lang[INT_CURRENCY_CONVERTER];?>','currencyconverter.php');

		d.add(14,0,'<?=$a_lang[STORES];?>','');
		d.add(1400,14,'<?=$a_lang[ENABLE_STORES];?>','stores.enable.php');
		d.add(1401,14,'<?=$a_lang[MANAGE_SUBSCRIPTION_TYPES];?>','stores.subscriptions.php');
		d.add(1402,14,'<?=$a_lang[STORES_MANAGEMENT];?>','stores.management.php');

		d.add(9,0,'<?=$a_lang[CATEGORIES];?>','');
		d.add(900,9,'<?=$a_lang[EDIT_CAT];?>','table.categories.php');
		d.add(901,9,'<?=$a_lang[h_cat_lang];?>','cat_lang.php');
		d.add(902,9,'<?=$a_lang[h_edit_cat_lang];?>','cat_lang_edit.php');
		d.add(903,9,'<?=$a_lang[h_multi_cat_active];?>','enabledisable.php?page=MLC');
		d.add(904,9,'<?=$a_lang[VIEW_S_CATS];?>','suggestcategory.php');

		d.add(10,0,'<?=$a_lang[FEES_ACCOUNTING];?>','');
		d.add(1000,10,'<?=$a_lang[ACC_VIEW];?>','acc.php');
		d.add(1001,10,'<?=$a_lang[ACC_DEBTORS];?>','acc_owe.php');
		
		d.add(11,0,'<?=$a_lang[TOOLS];?>','');
		d.add(1100,11,'<?=$a_lang[WORLD_FILTER];?>','wordfilter.php');
		d.add(1101,11,'<?=$a_lang[BLOCK_EMAILS];?>','blockemails.php');
		d.add(1102,11,'<?=$a_lang[CURR_CONV];?>','http://www.xe.com/ucc/');
		
		d.add(12,0,'<?=$a_lang[SUPPORT];?>','');
		d.add(1200,12,'<?=$a_lang[SUPPORT_DESK];?>','http://www.phpprobid.com/client/support/pdesk.cgi');
		d.add(1201,12,'<?=$a_lang[SUPPORT_MANUAL];?>','http://www.phpprobid.com/client/manuals/manual.pdf');
		
		document.write(d);

		//-->
	</script> 
  <!-- All items - 80 --> 
  <!-- Example 'My node', 'node.html', 'node title', 'mainframe', 'img/musicfolder.gif'); --> 
</div>
<br> 
<div><img src="images/pixel.gif" height="1" width="200"></div>
