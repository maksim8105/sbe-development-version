<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");

$phone="(".$dayphone1.") ".$dayphone2;
?>
<link href="../themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../themes/<?=$setts['default_theme'];?>/main.js" type="text/javascript"></script>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[REG_NEW_CLIENT];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
   <tr>
      <td><? include ("../formchecker.php"); ?>
      </td>
   </tr>
</table>
<? 
if ($action=="submit_user") { 
	$payment_status="confirmed";
	$active=1;
	$mailactivated=1;
	$bdate = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
	$currentTime = time();
			
	if ($setts['vat_rate']>0&&$setts['auto_vat_exempt']=="Y") $vat_exempted="Y";
	else $vat_exempted="N";

	$vat_uid = $_POST['vat_uid_number'];

   $companyname = ($_POST['accounttype']==1) ? "'".((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_POST['companyname']) : ((trigger_error("Error. Please contact administration.", E_USER_ERROR)) ? "" : ""))."'" : 'NULL';

	if (trim($_POST['name'])!=""&&trim($_POST['email'])!=""&&trim($_POST['username'])!=""&&trim($_POST['password'])!="") {
		$insertUser = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_users 
		(name,email,companyname,birthdate,address,city,state,country,zip,phone,username,
		password,active,payment_status,newsletter,regdate,mailactivated,lang,
		apply_vat_exempt,vat_uid_number,vat_exempted,birthdate_year,
		paypalemail,worldpayid,ikoboid,ikoboipn,checkoutid,protxname,protxpassword,
		authnetid,authnettranskey,reg_ip) VALUES
		('".remSpecialChars($_POST['name'])."','".$_POST['email']."',".$companyname.",'".$bdate."','".remSpecialChars($_POST['address'])."',
		'".remSpecialChars($_POST['city'])."','".remSpecialChars($_POST['state'])."','".remSpecialChars($_POST['country'])."','".remSpecialChars($_POST['zip'])."',
		'".remSpecialChars($phone)."','".$_POST['username']."','".md5($_POST['password'])."',
		'".$active."','".$payment_status."','".$_POST['subsnl']."','".$currentTime."','".$mailactivated."','".$_SESSION['sess_lang']."',
		'".$_POST['apply_vat_exempt']."','".$vat_uid."','".$vat_exempted."','".$_POST['year']."','".$_POST['paypalemail']."',
		'".$_POST['worldpayid']."','".$_POST['ikoboid']."','".$_POST['ikoboipn']."','".$_POST['checkoutid']."','".$_POST['protxname']."',
		'".$_POST['protxpassword']."','".$_POST['authnetid']."','".$_POST['authnettranskey']."','".$_POST['reg_ip']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$userid=((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
	}
	$recipientId = $userid;

	include ("../mails/register.php");
	echo "<p align=center>$a_lang[REG_SUCCESS_MSG]</p>";
} else { ?>
<form action="registerclient.php" method="post">
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         <td><img src="../themes/<?=$setts['default_theme'];?>/img/system/reg1.gif"></td>
         <td width="100%"><table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg2">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[m_account_type]?>
                     </b></td>
                  <script type="text/javascript">
                    function changestatusacc(state)
                    {
                        if(state==1){
                            document.getElementById('companyfield').innerHTML = '<input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($_POST['companyname']);?>" size="40" maxlength="50">';
                        } else {
                            document.getElementById('companyfield').innerHTML = '<input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($_POST['companyname']);?>" size="40" maxlength="50" disabled>';
                        }
                    }
                </script>
                  <td  class="contentfont"><input name="accounttype" type="radio" onclick="changestatusacc(0);" class="contentfont" id="accounttype" value="0" <?=($_POST['accounttype']==0 || !isset($_POST['accounttype'])) ? 'checked' :'';?>>
                     Individual<br>
                     <input name="accounttype" type="radio" onclick="changestatusacc(1);" class="contentfont" id="accounttype" value="1" <?=($_POST['accounttype']==1) ? 'checked' :'';?>>
                     Business </td>
               </tr>
               <tr class="creg3">
                  <td width="11"  class="contentfont"><?=$imgarrowit;?></td>
                  <a name=FullName></a>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[fullname]?>
                     </b><br>
                  </td>
                  <td class="contentfont"><input name="name" type="text" class="contentfont" id="name" value="<?=$_POST['name'];?>" maxlength="50"></td>
               </tr>
               <tr class="creg2">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[m_company]?>
                     </b></td>
                  <td  class="contentfont" id="companyfield"><input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($_POST['companyname']);?>" size="40" maxlength="50" <?=($_POST['accounttype']==0)?'disabled':'';?>></td>
               </tr>
               <tr class="creg3">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[address]?>
                     </b></td>
                  <td  class="contentfont"><input name="address" type="text" class="contentfont" id="address" value="<?=$_POST['address'];?>" size="40" maxlength="50"></td>
               </tr>
               <tr class="creg2">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[city]?>
                     </b></td>
                  <td  class="contentfont"><input name="city" type="text" class="contentfont" id="city" value="<?=$_POST['city'];?>" size="40" maxlength="50"></td>
               </tr>
            </table>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td valign=top  class="contentfont"><b>
                     <?=$lang[state]?>
                     </b></td>
                  <td valign=top  class="contentfont"><b>
                     <?=$lang[zip]?>
                     </b></td>
                  <td valign=top ><span class="contentfont"><b>
                     <?=$lang[country]?>
                     </b></span></td>
               </tr>
               <tr class="creg2"><a name=State></a><a name=ZipCode></a>
                  <td valign=top><input name="state" type="text" id="state7" value="<?=$_POST['state'];?>" maxlength="50">
                  </td>
                  <td valign=top><input name="zip" type="text" id="zip" value="<?=$_POST['zip'];?>" size="12" maxlength="20">
                  </td>
                  <td valign=top><select name="country" class="contentfont" id="country">
                        <?
								$getCountries=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries ORDER BY theorder ASC, name ASC");
								while ($country = mysqli_fetch_array($getCountries)) {
									echo "<option value=\"".$country['name']."\" ".(($country['name']==$_POST['country'])?"selected":"").">".$country['name']."</option>";
								} ?>
                     </select>
                  </td>
               </tr>
            </table>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11"  class="contentfont"><?=$imgarrowit;?></td>
                  <a name=FullName></a>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[phone]?>
                     </b><br>
                  </td>
                  <td  class="contentfont"><b>(
                     <input name="dayphone1" class="contentfont" value="<?=$_POST['dayphone1'];?>" size="4" maxlength="5">
                     ) </b>
                     <input name=dayphone2 class="contentfont" value="<?=$_POST['dayphone2'];?>" size=10 maxlength=15></td>
               </tr>
            </table></td>
      </tr>
   </table>
   <hr noShade size=1>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         <td class="creg4"><img src="../themes/<?=$setts['default_theme'];?>/img/system/reg2.gif"></td>
         <td class="creg4" width="100%"><table width="100%" border="0" cellpadding="3" cellspacing="1">
               <tr>
                  <td class="contentfont"><font class="redfont">
                     <?=$lang[reg_note]?>
                     </font></td>
               </tr>
            </table>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg2" >
                  <td width="11"  class="contentfont"><?=$imgwarning;?>
                  </td>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[email]?>
                     </b> </td>
                  <td class="contentfont"><input name="email" type="text" class="contentfont" id="email" value="<?=$_POST['email'];?>" size="40" maxlength="120">
                  </td>
               </tr>
               <tr class="creg3">
                  <td  class="contentfont"><?=$imgwarning;?></td>
                  <td nowrap class="contentfont"><strong>
                     <?=$lang[email_retype];?>
                     </strong></td>
                  <td class="contentfont"><input name="email_check" type="text" class="contentfont" id="email_check" value="" size="40" maxlength="120"></td>
               </tr>
               <tr valign="top" class="creg2">
                  <td width="11"  class="contentfont"><?=$imgwarning;?>
                  </td>
                  <td nowrap class="contentfont"><strong>
                     <?=$lang[newssubscr]?>
                     </strong></td>
                  <td class="contentfont"><input name="subsnl" type="radio" value="Y" <? echo (($_POST['subsnl']=="Y")?"checked":"");?>>
                     <?=$lang[yes]?>
                     <input type="radio" name="subsnl" value="N" <? echo (($_POST['subsnl']=="N"||$_POST['subsnl']=="")?"checked":"");?>>
                     <?=$lang[no]?>
                  </td>
               </tr>
            </table></td>
      </tr>
   </table>
   <hr noShade size=1>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         <td class="creg4"><img src="../themes/<?=$setts['default_theme'];?>/img/system/reg3.gif"></td>
         <td class="creg4" width="100%"><table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <a name=UserId></a>
                  <td width="150" class="contentfont"><b>
                     <?=$lang[userid]?>
                     </b></td>
                  <td class="contentfont"><input name="username" type="text" id="username" value="<?=$_POST['username'];?>" size="40" maxlength="30"></td>
               </tr>
               <tr class="reguser">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td><?=$lang[userid_text]?></td>
               </tr>
               <tr class="creg2">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <a name=Password></a>
                  <td  class="contentfont"><b>
                     <?=$lang[reg_pass]?>
                     </b></td>
                  <td  class="contentfont"><input name=password type=password class="contentfont" id="password" size=40 maxlength="20"></td>
               </tr>
               <tr class="reguser">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td><?=$lang[reg_passtext]?></td>
               </tr>
               <tr class="creg3">
                  <td  class="contentfont"><?=$imgarrowit;?></td>
                  <a name=PasswordReType></a>
                  <td  class="contentfont"><b>
                     <?=$lang[reg_pass2]?>
                     </b></td>
                  <td class="contentfont"><input name=password2 type=password  id="password22" size=40 maxlength="20"></td>
               </tr>
            </table>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg2">
                  <td width="11"  class="contentfont"><?=$imgarrowit;?></td>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[reg_birth]?>
                     </b></td>
                  <a name=DOB></a>
                  <td  class="contentfont"><? if ($setts['birthdate_type']==0) { ?>
                     <select name="day" class="contentfont" id="day">
                        <option value="" selected>
                        <?=$lang[day]?>
                        </option>
                        <option value="01" <? echo ($_POST['day']==1) ? "selected":""; ?>>1</option>
                        <option value="02" <? echo ($_POST['day']==2) ? "selected":""; ?>>2</option>
                        <option value="03" <? echo ($_POST['day']==3) ? "selected":""; ?>>3</option>
                        <option value="04" <? echo ($_POST['day']==4) ? "selected":""; ?>>4</option>
                        <option value="05" <? echo ($_POST['day']==5) ? "selected":""; ?>>5</option>
                        <option value="06" <? echo ($_POST['day']==6) ? "selected":""; ?>>6</option>
                        <option value="07" <? echo ($_POST['day']==7) ? "selected":""; ?>>7</option>
                        <option value="08" <? echo ($_POST['day']==8) ? "selected":""; ?>>8</option>
                        <option value="09" <? echo ($_POST['day']==9) ? "selected":""; ?>>9</option>
                        <option value="10" <? echo ($_POST['day']==10) ? "selected":""; ?>>10</option>
                        <option value="11" <? echo ($_POST['day']==11) ? "selected":""; ?>>11</option>
                        <option value="12" <? echo ($_POST['day']==12) ? "selected":""; ?>>12</option>
                        <option value="13" <? echo ($_POST['day']==13) ? "selected":""; ?>>13</option>
                        <option value="14" <? echo ($_POST['day']==14) ? "selected":""; ?>>14</option>
                        <option value="15" <? echo ($_POST['day']==15) ? "selected":""; ?>>15</option>
                        <option value="16" <? echo ($_POST['day']==16) ? "selected":""; ?>>16</option>
                        <option value="17" <? echo ($_POST['day']==17) ? "selected":""; ?>>17</option>
                        <option value="18" <? echo ($_POST['day']==18) ? "selected":""; ?>>18</option>
                        <option value="19" <? echo ($_POST['day']==19) ? "selected":""; ?>>19</option>
                        <option value="20" <? echo ($_POST['day']==20) ? "selected":""; ?>>20</option>
                        <option value="21" <? echo ($_POST['day']==21) ? "selected":""; ?>>21</option>
                        <option value="22" <? echo ($_POST['day']==22) ? "selected":""; ?>>22</option>
                        <option value="23" <? echo ($_POST['day']==23) ? "selected":""; ?>>23</option>
                        <option value="24" <? echo ($_POST['day']==24) ? "selected":""; ?>>24</option>
                        <option value="25" <? echo ($_POST['day']==25) ? "selected":""; ?>>25</option>
                        <option value="26" <? echo ($_POST['day']==26) ? "selected":""; ?>>26</option>
                        <option value="27" <? echo ($_POST['day']==27) ? "selected":""; ?>>27</option>
                        <option value="28" <? echo ($_POST['day']==28) ? "selected":""; ?>>28</option>
                        <option value="29" <? echo ($_POST['day']==29) ? "selected":""; ?>>29</option>
                        <option value="30" <? echo ($_POST['day']==30) ? "selected":""; ?>>30</option>
                        <option value="31" <? echo ($_POST['day']==31) ? "selected":""; ?>>31</option>
                     </select>
                     /
                     <select name="month" class="contentfont" id="month">
                        <option value=""selected>
                        <?=$lang[month]?>
                        </option>
                        <option value="01" <? echo ($_POST['month']==1) ? "selected":""; ?>>
                        <?=$lang[jan]?>
                        </option>
                        <option value="02" <? echo ($_POST['month']==2) ? "selected":""; ?>>
                        <?=$lang[feb]?>
                        </option>
                        <option value="03" <? echo ($_POST['month']==3) ? "selected":""; ?>>
                        <?=$lang[mar]?>
                        </option>
                        <option value="04" <? echo ($_POST['month']==4) ? "selected":""; ?>>
                        <?=$lang[apr]?>
                        </option>
                        <option value="05" <? echo ($_POST['month']==5) ? "selected":""; ?>>
                        <?=$lang[may]?>
                        </option>
                        <option value="06" <? echo ($_POST['month']==6) ? "selected":""; ?>>
                        <?=$lang[jun]?>
                        </option>
                        <option value="07" <? echo ($_POST['month']==7) ? "selected":""; ?>>
                        <?=$lang[jul]?>
                        </option>
                        <option value="08" <? echo ($_POST['month']==8) ? "selected":""; ?>>
                        <?=$lang[aug]?>
                        </option>
                        <option value="09" <? echo ($_POST['month']==9) ? "selected":""; ?>>
                        <?=$lang[sep]?>
                        </option>
                        <option value="10" <? echo ($_POST['month']==10) ? "selected":""; ?>>
                        <?=$lang[oct]?>
                        </option>
                        <option value="11" <? echo ($_POST['month']==11) ? "selected":""; ?>>
                        <?=$lang[nov]?>
                        </option>
                        <option value="12" <? echo ($_POST['month']==12) ? "selected":""; ?>>
                        <?=$lang[dec]?>
                        </option>
                     </select>
                     / <b>
                     <? } ?>
                     <?=$lang[year]?>
                     </b>
                     <input name="year" type="text" class="contentfont" id="year" value="<?=$_POST['year'];?>" size="4" maxlength="4">
                  </td>
               </tr>
            </table>
            <? if ($setts['vat_rate']>0&&$setts['auto_vat_exempt']=="N") { ?>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <td class="contentfont" nowrap><b>
                     <?=$lang[apply_vat_exempt]?>
                     </b></td>
                  <td class="contentfont" width="100%"><input name="apply_vat_exempt" type="radio" value="Y" <? echo (($_POST['apply_vat_exempt']=="Y")?"checked":"");?>>
                     <?=$lang[yes]?>
                     <input type="radio" name="apply_vat_exempt" value="N" <? echo (($_POST['apply_vat_exempt']=="N"||$_POST['apply_vat_exempt']=="")?"checked":"");?>>
                     <?=$lang[no]?></td>
               </tr>
               <tr class="reguser">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td><?=$lang[apply_vat_msg]?></td>
               </tr>
               <tr class="creg2">
                  <td><?=$imgarrowit;?></td>
                  <td  class="contentfont" nowrap><b>
                     <?=$lang[vat_uid_number]?>
                     </b></td>
                  <td class="contentfont"><input name="vat_uid_number" type="text" class="contentfont" id="vat_uid_number" size=40 value="<?=$_POST['vat_uid_number'];?>"></td>
               </tr>
            </table>
            <? } ?>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <td width="150" class="contentfont"><b>
                     <?=$lang[vat_uid_number];?>
                     </b> </td>
                  <td class="contentfont" id="vatuidfield"><input name="vat_uid_number" type="text" id="vat_uid_number" value="<?=$_POST['vat_uid_number'];?>" size="40" maxlength="30">
                  </td>
               </tr>
            </table>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="1" class="creg5">
               <tr class="creg3">
                  <td width="11" class="contentfont"><?=$imgarrowit;?></td>
                  <td width="150"  class="contentfont"><b>
                     <?=$lang[reg_pin]?>
                     </b></td>
                  <a name=UserId></a>
                  <td class="contentfont"><? 			
							$_SESSION['pin_value']=md5(rand(2,99999999));
							$encodedPin = generatePin ($_SESSION['pin_value']);
							## create an image not a text for the pin
							$font  = 4;
							$width  = ImageFontWidth($font) * strlen($encodedPin);
							$height = ImageFontHeight($font);
				
							$im = @imagecreate ($width,$height);
							$background_color = imagecolorallocate ($im, 255, 255, 255); //white background
							$text_color = imagecolorallocate ($im, 0, 0,0);//black text
							imagestring ($im, $font, 0, 0,  $encodedPin, $text_color);
							imagejpeg($im,"../uplimg/tmp_pin_".$_SESSION['pin_value'].".jpg");
							echo "<img src=\"../uplimg/tmp_pin_".$_SESSION['pin_value'].".jpg\">";			
							imagedestroy($im); ?>
                     <input type="hidden" name="pin_value" value="<?=$_SESSION['pin_value'];?>"></td>
               </tr>
               <tr class="reguser">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td><?=$lang[pin_text]?>
                     </font></td>
               </tr>
               <tr class="creg2">
                  <td class="contentfont"><?=$imgarrowit;?></td>
                  <td  class="contentfont"><b>
                     <?=$lang[conf_pin]?>
                     </b></td>
                  <td><input name=pin2 type=text id="pin2" size=40></td>
               </tr>
            </table></td>
      </tr>
   </table>
   <hr noShade size=1>
   <table width="100%" border="0" cellpadding="6" cellspacing="0" class="regborder">
      <tr valign="top">
         <td class="creg4"><img src="../themes/<?=$setts['default_theme'];?>/img/system/reg4.gif"></td>
         <td class="creg4" width="100%"><input type="hidden" name="ack_terms" value="Y">
            <hr noShade size=1>
            <input type="hidden" name="reg_ip" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>">
            <input name="registerok" type=submit id="registerok" value="<?=$lang[reg_button]?>">
         </td>
      </tr>
   </table>
</form>
<? }
	include ("footer.php"); 
} ?>
