<?
## v5.24 -> apr. 07, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
@include_once ('../config/lang/'.$_SESSION['sess_lang'].'/site.lang');

if (isset($_POST['additemok'])) {
	$paymentMethods="";
	$pmethod = $_POST['pmethod'];
	for ($i=0;$i<count($_POST['pmethod']);$i++) {
		$paymentMethods.=$pmethod[$i]."<br>";
	}

	if ($_FILES['file']['name']!="") {
		$oldFile = getSqlField("SELECT picpath FROM probid_auctions WHERE id='".$_POST['id']."'","picpath");
		deleteFile("../",$oldFile);
		$fileExtension = getFileExtension($_FILES['file']['name']);
		$imageName = $_POST['id']."_mainpic.".$fileExtension;
		$isUpload = uploadFile($_FILES['file']['tmp_name'],$imageName,"../uplimg/",TRUE,'../');
		if ($isUpload) {
			$updatemainpic=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
			picpath='uplimg/".$imageName."' WHERE id='".$_POST['id']."'");
		}
	}
	
	$hpfeat = ($_POST['hpfeat']!="Y") ? "N" : "Y";
	$catfeat = ($_POST['catfeat']!="Y") ? "N" : "Y";
	$bolditem = ($_POST['bolditem']!="Y") ? "N" : "Y";
	$hlitem = ($_POST['hlitem']!="Y") ? "N" : "Y";
	$respr = ($_POST['resprice']>0) ? "Y" : "N";
	
	$name = remSpecialChars($_POST['name']);
	$description = remSpecialChars($_POST['description_main']);

	$updateitem=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
	itemname='".$name."',description='".$description."',
	quantity='".$_POST['quantity']."',auctiontype='".$_POST['auctiontype']."',
	bidstart='".$_POST['startprice']."',rp='".$respr."',rpvalue='".$_POST['resprice']."',
	bn='".$_POST['buynow']."',bnvalue='".$_POST['bnprice']."',bi='".$_POST['bidinc']."',
	bivalue='".$_POST['bidincvalue']."',country='".$_POST['country']."',
	zip='".$_POST['zip']."',sc='".$_POST['shipcond']."',scint='".$_POST['shipint']."',
	pm='".$paymentMethods."',category='".$_POST['category']."',
	hpfeat='".$hpfeat."',catfeat='".$catfeat."',bolditem='".$bolditem."',hlitem='".$hlitem."',
	private='".$_POST['privateauct']."',currency='".$_POST['currency']."',
	postage_costs='".$_POST['postage_costs']."',insurance='".$_POST['insurance']."',
	type_service='".$_POST['type_service']."',isswap='".$_POST['isswap']."',addlcategory='".$_POST['addlcategory']."',
	shipping_details='".$_POST['shipping_details']."',hpfeat_desc='".$_POST['hpfeat_desc']."', listin='".$_POST['listin']."'
	WHERE id='".$_POST['id']."'");
	
	### insert the custom fields for the auction (if available)
  	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_fields") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
  	$isFields = mysqli_num_rows($getFields);
  	if ($isFields) {
		$delFields = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fields_data WHERE auctionid='".$_POST['id']."'");
		while ($fields=mysqli_fetch_array($getFields)) {
			$box_value = "";
			if ($fields['boxtype']=="checkbox") {
				for ($i=0; $i<count($_POST['box'.$fields['boxid']]); $i++) 
					$box_value .= $_POST['box'.$fields['boxid']][$i]."; ";
			} else { 
				$box_value = $_POST['box'.$fields['boxid']];
			}
			$addFieldData = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fields_data
			(auctionid, ownerid, boxid, boxvalue) VALUES
			('".$_POST['id']."','".$_POST['ownerid']."','".$fields['boxid']."','".remSpecialChars($box_value)."')");
		}
	}		

	## first delete the old additional images if new ones are uploaded
	for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
		if($_FILES['addfile']['name'][$i]!="") {
			$getAddImages = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT name FROM probid_auction_images WHERE id='".$_POST['id']."'");
			while ($addImage = mysqli_fetch_array($getAddImages)) {
				deleteFile("../",$addImage['name']);
			}
	 		$deleteImages=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_images WHERE auctionid='".$_POST['id']."'");
		} 
	}
	
	## now upload the new additional pictures
	for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
		if($_FILES['addfile']['name'][$i]!="") {
			$fileExtension = getFileExtension($_FILES['addfile']['name'][$i]);
			$addImageName = $_POST['id']."_addpic".$i.".".$fileExtension;
			uploadFile($_FILES['addfile']['tmp_name'][$i],$addImageName,"../uplimg/",TRUE,'../');
			$insertImage = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO  probid_auction_images (name,auctionid) 
			VALUES ('uplimg/".$addImageName."','".$_POST['id']."')");
		} 
	}
}

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_auct.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[AUCT_MANAGE]; echo " / "; echo $a_lang[AUCT_EDIT];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<?  
if (isset($_POST['additemok'])) {
	// KEV CHANGES - update the categories!
	addcatcount ($_REQUEST['category'],$_REQUEST['id']);
	addcatcount ($_REQUEST['addlcategory'],$_REQUEST['id']);
	delcatcount ($_REQUEST['cat_prv'],$_REQUEST['id']);
	delcatcount ($_REQUEST['addlcat_prv'],$_REQUEST['id']);
	
	echo "$a_lang[AUCT_EDIT_UPDATE_SUC]";
	$link = "index.php";
	echo "<script>window.setTimeout('changeurl();',2500); function changeurl(){window.location='$link'}</script>";
} else {
	$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."'");

	## delete pictures
	if (!empty($_REQUEST['deletepic'])) {
		if (@eregi('mainpic', $_REQUEST['deletepic'])) {
			$isPic = getSqlNumber("SELECT id FROM probid_auctions WHERE picpath='".$auctionDetails['picpath']."'");
			if (!$isPic) @unlink($_REQUEST['mainpic']);
			$remPicDb = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET picpath='' WHERE id='".$auctionDetails['id']."'");
			$auctionDetails['picpath'] = '';
			
			## we also delete all additional pics
			$getAddImages = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT i.* FROM probid_auction_images i, probid_auctions a WHERE i.auctionid='".$auctionDetails['id']."' AND a.id=i.auctionid");
			while ($addImage = mysqli_fetch_array($getAddImages)) {
				deleteFile("",$addImage['name']);
			}
			$deleteImages=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_images WHERE auctionid='".$auctionDetails['id']."'");

		} else if (@eregi('addpic', $_REQUEST['deletepic'])) {
			$picDets = explode('_',$_REQUEST['deletepic']);
			$getAP = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT i.* FROM probid_auction_images i, probid_auctions a WHERE i.auctionid='".$auctionDetails['id']."' AND a.id=i.auctionid");
			$addPicRes = @mysql_result($getAP,$picDets[1],'name');
			$addPicId = @mysql_result($getAP,$picDets[1],'id');
			@unlink($addPicRes);
			$remAddPicDb = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_auction_images WHERE id='".$addPicId."'");
		}
	} 
	
	$nbAddPics = getSqlNumber("SELECT id FROM probid_auction_images WHERE auctionid='".$auctionDetails['id']."'");

	echo "<script language=\"JavaScript\"> \n";
	echo "	function delete_pic(theform,pic) { \n";
	echo "		if (confirm('".$a_lang[REMIMGCONF]."'))	{ \n";
	echo "			theform.deletepic.value = pic; \n";
	echo "			theform.submit(); \n";
	echo "		} else { \n";
	echo "			theform.del_chk_mainpic.checked = false; \n";
	for ($i=0; $i<$nbAddPics; $i++) echo "theform.del_chk_addpic".$i.".checked = false; \n";
	echo "		} \n";
	echo "	} \n";
	echo "</script>";	
	?>
<script language="JavaScript">
function changepage(theform) {
	if (theform.auctiontype.value=="dutch") {
		theform.respr[0].disabled=true;
		theform.respr[1].disabled=true;
		theform.resprice.disabled=true;
		theform.quantity.readOnly=false;
	} else {
		theform.respr[0].disabled=false;
		theform.respr[1].disabled=false;
		theform.resprice.disabled=false;
		theform.quantity.readOnly=true;
	}		
}
</script>
<table width="100%" border="0" cellpadding="4" cellspacing="2">
   <form action="editauction.php" method="post" enctype="multipart/form-data" name="sistep1">
      <input type="hidden" name="deletepic" value="">
      <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
      <!-- KEV - Added previous cat value -->
      <input type="hidden" name="cat_prv" value="<?=$auctionDetails['category'];?>">
      <input type="hidden" name="addlcat_prv" value="<?=$auctionDetails['addlcategory'];?>">
      <input type="hidden" name="ownerid" value="<?=$auctionDetails['ownerid'];?>">
      <tr align="center" class="c3">
         <td colspan="3"><b>
            <?=$a_lang[AUCT_EDIT_MODIFY];?>
            </b></td>
      </tr>
      <? 	$storeOwner = getSqlRow("SELECT store_active FROM probid_users WHERE id='".$auctionDetails['ownerid']."'"); ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$a_lang[LISTED_IN];?>
            </strong></td>
         <td colspan="2"><select name="listin">
               <option value="auction" selected>Auction</option>
               <? if ($storeOwner['store_active']==1&&$storeOwner['aboutpage_type']==2) { ?>
               <option value="store" <? echo ($auctionDetails['listin']=="store") ? "selected":""; ?>>Store</option>
               <option value="both" <? echo ($auctionDetails['listin']=="both") ? "selected":""; ?>>Both</option>
               <? } ?>
            </select></td>
      </tr>
      <tr class="c1">
         <td width="110" align="right"><b>
            <?=$a_lang[ITEM_NAME];?>
            </b></td>
         <td colspan="2"><input name="name" type="text" id="name" value="<? echo addSpecialChars($auctionDetails['itemname']);?>" maxlength="60"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><b>
            <?=$a_lang[DESCRIPTION];?>
            </b></td>
         <td colspan="2"><textarea name="description_main" cols="60" rows="6" id="description_main"><? echo addSpecialChars($auctionDetails['description']);?></textarea>
            <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("description_main");//Specify the id of the textarea here
		</script></td>
      </tr>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_UPLOADPIC];?>
            </b></td>
         <td><input type="file" name="file">
            <? if (!empty($auctionDetails['picpath'])) echo $a_lang[DELETE].": <input type=\"checkbox\" id=\"del_chk_mainpic\" onClick=\"delete_pic(this.form,'mainpic');\"> "; ?>
            <br>
            <?=$a_lang[AUCT_EDIT_FIELDOPT];?></td>
         <td><? 
	  	if ($auctionDetails['picpath']!="") echo "<img src=\"../".$auctionDetails['picpath']."?pid=".rand(1,10000)."\" width=\"80\" border=\"1\">";
		else echo "$a_lang[AUCT_EDIT_NOUPLOADPIC]."; ?>
         </td>
      </tr>
      <? if ($setts['pic_gal_active']==1) { ?>
      <tr class="c2" valign="top">
         <td align="right" ><b>
            <?=$a_lang[AUCT_EDIT_GALLERY];?>
            </b></td>
         <td colspan="2"><? 
				$getAddPics = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_images WHERE auctionid='".$auctionDetails['id']."'");
				$addPicCnt = 0;
				while ($addP = mysqli_fetch_array($getAddPics)) $addPic[$addPicCnt++] = $addP;
				
				for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
					echo "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\"> \n";
					echo "	<tr> \n";
					echo "		<td><input type=\"file\" name=\"addfile[]\" class=\"contentfont\"></td>";
					if (!empty($addPic[$i]['name'])) {
						echo "<td>&nbsp;Current Picture: <img src=\"../makethumb.php?pic=".$addPic[$i]['name']."&w=50&sq=Y&b=Y\" align=absmiddle></td>";
	      			echo "<td>".$a_lang[DELETE].": <input type=\"checkbox\" id=\"del_chk_addpic".$i."\" onClick=\"delete_pic(this.form,'addpic_".$i."');\"></td> ";
					}
					echo "	</tr>";
					echo "</table>";
				} ?>
            <br>
            <?=$a_lang[AUCT_EDIT_NOTE];?>
         </td>
      </tr>
      <? } ?>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_AUCTTYPE];?>
            </b></td>
         <td colspan="2"><select name="auctiontype" onChange="changepage(sistep1);">
               <option value="standard" <? echo (($auctionDetails['auctiontype']=="dutch")?"":"selected");?>>
               <?=$a_lang[AUCT_EDIT_STANDARD];?>
               </option>
               <option value="dutch" <? echo (($auctionDetails['auctiontype']=="dutch")?"selected":"");?>>
               <?=$a_lang[AUCT_EDIT_DUTCH];?>
               </option>
            </select></td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_ITEMQUANT];?>
            </b></td>
         <td colspan="2"><input name="quantity" type="text" id="quantity" value="<? echo (($auctionDetails['quantity']>=1)?$auctionDetails['quantity']:"1");?>" size="8">
         </td>
      </tr>
      <tr class="c1">
         <td align="right" nowrap><b>
            <?=$a_lang[AUCT_EDIT_AUCTSTARTS];?>
            </b></td>
         <td colspan="2"><input name="startprice" type="text" id="startprice" value="<?=$auctionDetails['bidstart'];?>" size="8">
            <?=$auctionDetails['currency'];?>
         </td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_RESERVPRICE];?>
            </b></td>
         <td colspan="2"><input type="hidden" name="resprice_prv" value="<?=$auctionDetails['rpvalue'];?>">
            <input name="resprice" type="text" id="resprice" value="<?=$auctionDetails['rpvalue'];?>" size="8">
            <?=$auctionDetails['currency'];?>
         </td>
      </tr>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_BUYNOW];?>
            </b></td>
         <td colspan="2"><input name="buynow" type="radio" value="N" <? echo(($auctionDetails['bn']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?>
            <input type="radio" name="buynow" value="Y" <? echo(($auctionDetails['bn']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input name="bnprice" type="text" id="bnprice" value="<?=$auctionDetails['bnvalue'];?>" size="8">
            <?=$auctionDetails['currency'];?>
         </td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_BIDINC];?>
            </b></td>
         <td colspan="2"><input name="bidinc" type="radio" value="0" <? echo (($auctionDetails['bi']=="0")?"checked":"");?>>
            <?=$a_lang[AUCT_EDIT_BIDINC1];?>
            <br>
            <input type="radio" name="bidinc" value="1" <? echo (($auctionDetails['bi']=="1")?"checked":"");?>>
            <?=$a_lang[AUCT_EDIT_BIDINC2];?>
            <input name="bidincvalue" type="text" id="bidincvalue" value="<?=$auctionDetails['bivalue'];?>" size="8">
            <?=$auctionDetails['currency'];?>
         </td>
      </tr>
      <? if ($setts['hp_feat']!=0||$setts['cat_feat']!=0||$setts['bold_item']!=0||$setts['hl_item']!=0) { ?>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_FEATITEM];?>
            </b></td>
         <td colspan="2"><? 
				if ($setts['hp_feat']!=0) {
					if ($auctionDetails['hpfeat']=="Y") { 
						echo "$a_lang[AUCT_EDIT_FEATITEM1]<br>	
						<input type=hidden name=hpfeat value=Y>
						<input type=hidden name=hpfeat_prv value=Y>";
					} else {
						echo "<input type=\"checkbox\" name=\"hpfeat\" value=\"Y\">$a_lang[AUCT_EDIT_FEATITEM2]<br>";
					}
				}
							
				if ($setts['cat_feat']!=0) {
					if ($auctionDetails['catfeat']=="Y") { 
						echo "$a_lang[AUCT_EDIT_FEATITEM3]<br>	
						<input type=hidden name=catfeat value=Y>
						<input type=hidden name=catfeat_prv value=Y>";
					} else {
						echo "<input type=\"checkbox\" name=\"catfeat\" value=\"Y\">$a_lang[AUCT_EDIT_FEATITEM4]<br>";
					}
				}
							
				if ($setts['bold_item']!=0) {
					if ($auctionDetails['bolditem']=="Y") { 
						echo "$a_lang[AUCT_EDIT_FEATITEM5]<br>	
						<input type=hidden name=bolditem value=Y>
						<input type=hidden name=bolditem_prv value=Y>";
					} else {
						echo "<input type=\"checkbox\" name=\"bolditem\" value=\"Y\">$a_lang[AUCT_EDIT_FEATITEM6]<br>";
					}
				}
						
				if ($setts['hl_item']!=0) {
					if ($auctionDetails['hlitem']=="Y") { 
						echo "$a_lang[AUCT_EDIT_FEATITEM7]<br>	
						<input type=hidden name=hlitem value=Y>
						<input type=hidden name=hlitem_prv value=Y>";
					} else {					
						echo "<input type=\"checkbox\" name=\"hlitem\" value=\"Y\">$a_lang[AUCT_EDIT_FEATITEM8]<br>";
					}
				} ?>
         </td>
      </tr>
      <? if ($setts['hpfeat_desc']=="Y"&&$setts['hp_feat']!=0) { ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$a_lang[hpfeat_desc]?>
            </strong></td>
         <td colspan="2"><? 
				echo "<textarea name=\"hpfeat_desc\" cols=\"50\" rows=\"4\">".$auctionDetails['hpfeat_desc']."</textarea><br>"; ?>
         </td>
      </tr>
      <? } ?>
      <? } ?>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[COUNTRY];?>
            </b></td>
         <td colspan="2"><? 
			  echo "<SELECT name=\"country\">";
			  $getCountries = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries");
			  while ($country = mysqli_fetch_array($getCountries)) {
				  echo "<OPTION value=\"".$country['name']."\" ".(($country['name']==$auctionDetails['country'])?"SELECTED":"").">".$country['name']."</option>";
			  }
			  echo "</SELECT>";
			  ?>
         </td>
      </tr>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_ZIP];?>
            </b></td>
         <td colspan="2"><input name="zip" type="text" id="zip" value="<?=$auctionDetails['zip'];?>" size="12"></td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_SHIPPING];?>
            </b></td>
         <td colspan="2"><input type="radio" name="shipcond" value="BP" <? echo (($auctionDetails['sc']=="BP")?"checked":"");?>>
            <?=$a_lang[AUCT_EDIT_SHIPBUYER];?>
            <br>
            <input type="radio" name="shipcond" value="SP" <? echo (($auctionDetails['sc']=="SP")?"checked":"");?>>
            <?=$a_lang[AUCT_EDIT_SHIPSELLER];?>
            <br>
            <input name="shipint" type="checkbox" id="shipint" value="Y" <? echo (($auctionDetails['scint']=="Y")?"checked":"");?>>
            <?=$a_lang[AUCT_EDIT_SHIPINT];?></td>
      </tr>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_PRIVATEAUCT];?>
            </b></td>
         <td colspan="2"><select name="privateauct" id="privateauct">
               <option value="N" selected>
               <?=$a_lang[NO];?>
               </option>
               <option value="Y" <? echo (($auctionDetails['private']=="Y")?"selected":"");?>>
               <?=$a_lang[YES];?>
               </option>
            </select>
         </td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_CURRENCY];?>
            </b></td>
         <? $getCurrencies=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_currencies"); ?>
         <td colspan="2"><select name="currency">
               <? 
					while ($currency = mysqli_fetch_array($getCurrencies)) {  
						echo "<option value=\"".$currency['symbol']."\" ".(($auctionDetails['currency']==$currency['symbol'])?"selected":"").">".$currency['symbol']." ".$currency['caption']."</option>"; 
					} ?>
            </select></td>
      </tr>
      <tr class="c1">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_SWAP];?>
            </b></td>
         <td colspan="2"><input name="isswap" type="radio" value="N" <? echo(($auctionDetails['isswap']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?>
            <input type="radio" name="isswap" value="Y" <? echo(($auctionDetails['isswap']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?></td>
      </tr>
      <tr class="c2">
         <td align="right"><b>
            <?=$a_lang[AUCT_EDIT_PM];?>
            </b></td>
         <td colspan="2"><? 
				$getPaymentMethods = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_methods");
				while ($paymentMethod = mysqli_fetch_array($getPaymentMethods)) {
					$isSelected="";
					if (@eregi($paymentMethod['name'],$auctionDetails['pm'])) $isSelected="checked";
					echo "<input type=\"checkbox\" name=\"pmethod[]\" value=\"".$paymentMethod['name']."\" ".$isSelected.">".$paymentMethod['name']."<br>";
				} ?>
         </td>
      </tr>
      <? if ($setts['shipping_costs']==1) { ?>
      <tr class="c1">
         <td align="right" valign="top"><b>
            <?=$a_lang[AUCT_EDIT_PC];?>
            </b></td>
         <td colspan="2"><?=$auctionDetails['currency'];?>
            <input name="postage_costs" type="text" id="postage_costs" value="<?=$auctionDetails['postage_costs'];?>" size="15"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><b>
            <?=$a_lang[AUCT_EDIT_INSUR];?>
            </b></td>
         <td colspan="2"><?=$auctionDetails['currency'];?>
            <input name="insurance" type="text" id="insurance" value="<?=$auctionDetails['insurance'];?>" size="15"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><strong>
            <?=$a_lang[SP_DETAILS]?>
            </strong></td>
         <td colspan="2"><textarea name="shipping_details" cols="50" rows="4"><? echo addSpecialChars($auctionDetails['shipping_details']);?></textarea></td>
      </tr>
      <tr class="c1">
         <td align="right" valign="top"><b>
            <?=$a_lang[AUCT_EDIT_TYPESERV];?>
            </b></td>
         <td colspan="2"><? 
				echo "<SELECT name=\"type_service\" class=\"contentfont\">";
				echo "<option value=\"\" selected>".$lang[servicetypeselect]."</option>";
				$getShippingOptions=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_shipping_options");
				while ($shOpt=mysqli_fetch_array($getShippingOptions)) {
					echo "<OPTION value=\"".$shOpt['name']."\" ".(($shOpt['name']==$auctionDetails['type_service'])?"SELECTED":"").">".$shOpt['name']."</option>";
				}
				echo "</SELECT>"; ?></td>
      </tr>
      <? }  
		$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_fields ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))); 
		$isFields = mysqli_num_rows($getFields);
		if ($isFields) {
			while ($fields=mysqli_fetch_array($getFields)) {
				$fieldData = getSqlRow("SELECT boxvalue FROM probid_fields_data WHERE auctionid='".$auctionDetails['id']."' AND boxid='".$fields['boxid']."'"); 
				if ($fields['boxtype']=="checkbox") $selectedValue = explode(";",$fieldData['boxvalue']); 
				else $selectedValue = $fieldData['boxvalue']; ?>
      <tr class="<? echo (($count++)%2==0)?"c2":"c1"; ?>">
         <td align="right"><strong>
            <?=$fields['boxname'];?>
            </strong></td>
         <td colspan="2"><? echo createField($fields['boxid'],$selectedValue); ?> </td>
      </tr>
      <? } 
  		} ?>
      <tr class="c2">
         <td colspan="3"><b>
            <?=$a_lang[AUCT_EDIT_CHOOSECAT];?>
            </b><br>
            <br>
            <input type="hidden" name="cat_prv" value="<?=$auctionDetails['category'];?>">
            <select name="category" class="contentfont">
               <?
					reset($cat_array);
					while (list($catid, $cat_array_details) = each ($cat_array)){
						list($catname, $userid) = $cat_array_details;
						if ($userid==0||($auctionEdit['listin']=="shop"&&$userid==$_SESSION['memberid'])) echo '<option value="'.$catid.'" '.(($auctionDetails['category'] ==$catid)?"selected":"").'>'.$catname.'</option>';
					} 
					?>
            </select>
      </tr>
      <tr class="c1">
         <td colspan="3"><b> <b>
            <?=$a_lang[AUCT_EDIT_CHOOSEADDLCAT];?>
            </b><br>
            <br>
            <input type="hidden" name="addlcat_prv" value="<?=$auctionDetails['addlcategory'];?>">
            <select name="addlcategory" class="contentfont">
               <?
					reset($cat_array);
					echo '<option value="" selected>-- no additional category --</option>';
					while (list($catid, $cat_array_details) = each ($cat_array)){
						list($catname, $userid) = $cat_array_details;
						if ($userid==0||($auctionEdit['listin']=="shop"&&$userid==$_SESSION['memberid'])) echo '<option value="'.$catid.'" '.(($auctionDetails['addlcategory'] ==$catid)?"selected":"").'>'.$catname.'</option>';
					} ?>
            </select></td>
      </tr>
      <tr align="center" class="c3">
         <td colspan="3"><input name="additemok" type="submit" id="additemok" value="<?=$a_lang[BUTT_MODIFY];?>"></td>
      </tr>
   </form>
</table>
<? } 
	include ("footer.php"); 
} ?>
