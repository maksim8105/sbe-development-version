<?
## v5.24 -> apr. 27, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

if ($_REQUEST['type']!="signup"&&$_REQUEST['type']!="setup") {
	header ("Location: managevouchers.php");
}

include ("../config/config.php");

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[VOUCHERS]; echo " / "; echo $a_lang[ADD_VOUCHER];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<? 
if ($_REQUEST['option']=="edit") {
	$voucher = getSqlRow("SELECT * FROM probid_vouchers WHERE id='".$_REQUEST['id']."'");
}
?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="managevouchers.php" method="post">
      <input type="hidden" name="type" value="<?=$_REQUEST['type'];?>">
      <input type="hidden" name="option" value="<?=$_REQUEST['option'];?>">
      <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
      <tr class="c3">
         <td align="center" colspan="2"><b>
            <?=$a_lang[ADD_EDIT_VOUCHER];?>
            </b></td>
      </tr>
      <tr class="c2">
         <td width="120"><b>
            <?=$a_lang[VOUCHER_TYPE];?>
            :</b></td>
         <td><?=strtoupper($_REQUEST['type']);?></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[VOUCHER_NAME];?>
            :</b></td>
         <td><input name="voucher_name" type="text" id="voucher_name" value="<?=$voucher['voucher_name'];?>" size="40"></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[REDUCTION];?>
            :</b></td>
         <td><? if ($_REQUEST['type']=="signup") { ?>
            <input name="reduction_percent" type="hidden" id="reduction_percent" value="100">
            100%
            <? } else { ?>
            <input name="reduction_percent" type="text" id="reduction_percent" value="<?=$voucher['reduction'];?>" size="9">
            %
            <? } ?></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[START_DATE];?>
            :</b></td>
         <td><? 
				if ($_REQUEST['option']!="edit") {
					echo $a_lang[NOW];
				} else {
					echo date($setts['date_format'],$voucher['regdate']);
				} ?></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[EXPIRATION_DATE];?>
            :</b></td>
         <td><? 
				if ($_REQUEST['option']=="edit") {
					echo ($voucher['expdate']>0) ? date($setts['date_format'],$voucher['expdate']) : $a_lang[NA];
					echo "<br>";
				} ?>
            <?=$a_lang[DURATION];?>
            :
            <input name="duration" type="text" id="duration" value="<?=$voucher['duration'];?>" size="9">
            <?=$a_lang[DAYS];?>
            <br>
            <?=$a_lang[VOUCHER_DURATION_NOTE];?></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[NB_USES];?>
            :</b></td>
         <td><input name="nbuses" type="text" id="nbuses" value="<?=$voucher['nbuses'];?>" size="9" <? echo ($_REQUEST['option']=="edit")?"readonly":"";?>>
            <br>
            <?=$a_lang[NOTE_VOUCHER_NB_USES];?></td>
      </tr>
      <? if ($voucher['nbuses']>0) { ?>
      <tr class="c2">
         <td><b>
            <?=$a_lang[NB_USES_LEFT];?>
            :</b></td>
         <td><?=$voucher['usesleft'];?></td>
      </tr>
      <? } ?>
      <? if ($_REQUEST['type']=="setup") { 
		if (trim($voucher['assignto'])=="") $assignto="asdsad"; 
		else $assignto=$voucher['assignto']; ?>
      <tr class="c2">
         <td><b>
            <?=$a_lang[ASSIGN_TO];?>
            :</b></td>
         <td><input name="assignto[]" type="checkbox" value="all" <? echo (@eregi(";all;",$assignto)) ? "checked":""; ?>>
            <?=$a_lang[ALL_FEES];?>
            <br>
            <br>
            <input name="assignto[]" type="checkbox" value="setup" <? echo (@eregi(";setup;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[AUCTION_SETUP_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="hpfeat" <? echo (@eregi(";hpfeat;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[HPFEAT_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="catfeat" <? echo (@eregi(";catfeat;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[CATFEAT_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="bold" <? echo (@eregi(";bold;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[BOLD_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="hl" <? echo (@eregi(";hl;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[HL_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="rp" <? echo (@eregi(";rp;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[RP_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="bn" <? echo (@eregi(";bn;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[BN_FEE];?>
            <br>
            <input name="assignto[]" type="checkbox" value="pic" <? echo (@eregi(";pic;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[PIC_FEE];?>
            <input name="assignto[]" type="checkbox" value="seccat" <? echo (@eregi(";seccat;",$voucher['assignto'])) ? "checked":""; ?>>
            <?=$a_lang[SECCAT_FEE];?>
         </td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[ASSIGN_TO_USERS];?>
            :</b></td>
         <td><select name="assigned_users[]" size="8" multiple id="assigned_users[]">
               <? $getUsers=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE active='1'");
					while ($user=mysqli_fetch_array($getUsers)) {
						echo "<option value=\"".$user['username']."\" ".((@eregi(";".$user['username'].";",$voucher['assigned_users']))?"selected":"").">".$user['username']."</option>";
					} ?>
            </select>
            <br>
            <?=$a_lang[VOUCHER_ASSIGN_USERS_NOTE];?></td>
      </tr>
      <? } ?>
      <tr class="c3">
         <td>&nbsp;</td>
         <td><input name="addvoucherok" type="submit" id="addvoucherok" value="<? echo ($_REQUEST['option']=="edit") ? $a_lang[BUTT_EDIT_VOUCHER] : $a_lang[BUTT_ADD_VOUCHER];?>"></td>
      </tr>
   </form>
</table>
<? include ("footer.php"); 
} ?>
