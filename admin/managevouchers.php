<?
## v5.24 -> apr. 27, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if ($_GET['option']=="delete") {
	$delVoucher=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_vouchers WHERE id='".$_GET['id']."'");
}

if (isset($_POST['addvoucherok'])) {
	$assignedUsers="";
	if (count($_POST['assigned_users'])>0) $assignedUsers.=";";
	for ($i=0;$i<count($_POST['assigned_users']);$i++) {
		$assignedUsers.=$_POST['assigned_users'][$i].";";
	}

	$assignedFees="";
	$allFees=FALSE;
	if (count($_POST['assignto'])>0) $assignedFees.=";";
	for ($i=0;$i<count($_POST['assignto']);$i++) {
		if ($$_POST['assignto'][$i]=="all") { 
			$assignedFees.="all;";
			$allFees = TRUE;
		}
	}
	if (!$allFees) {
		for ($i=0;$i<count($_POST['assignto']);$i++) {
			$assignedFees.=$_POST['assignto'][$i].";";
		}
	}

	$generate_voucher_code = substr(md5(uniqid(rand(2, 999999999))),-12);

	$currentTime = ($_POST['option']=="edit") ? getSqlField("SELECT regdate FROM probid_vouchers WHERE id='".$_POST['id']."'","regdate") : time();
	$expdate = 0;
	
	if ($_POST['duration']!="") $expdate = $currentTime + $_POST['duration'] * 24 * 60 * 60;
	$reduction = ($_POST['reduction_percent']>100) ? 100 : $_POST['reduction_percent'];
	$reduction = ($reduction<0) ? 0 : $reduction;
	
	if ($_POST['option']=="edit") {
		$editVoucher = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_vouchers SET voucher_name='".remSpecialChars($_POST['voucher_name'])."', 
		regdate='".$currentTime."', expdate='".$expdate."', reduction='".$reduction."',
		assigned_users='".$assignedUsers."',duration='".$_POST['duration']."',assignto='".$assignedFees."' 
		WHERE id='".$_POST['id']."'");
	} else {
		$addVoucher = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_vouchers 
		(voucher_name, voucher_code, voucher_type,regdate,expdate,nbuses, usesleft, reduction, assigned_users, active, duration, assignto) VALUES 
		('".remSpecialChars($_POST['voucher_name'])."', '".$generate_voucher_code."','".$_POST['type']."','".$currentTime."','".$expdate."',
		'".$_POST['nbuses']."','".$_POST['nbuses']."','".$reduction."','".$assignedUsers."','1','".$_POST['duration']."',
		'".$assignedFees."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
}

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[VOUCHERS]; echo " / "; echo $a_lang[MANAGE_VOUCHERS];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr class="c3">
      <td align="center"><b>
         <?=$a_lang[MANAGE_VOUCHERS];?>
         </b></td>
   </tr>
   <tr>
      <td align="center">[ <a href="addvoucher.php?type=signup">
         <?=$a_lang[ADD_SIGNUP_VOUCHER];?>
         </a> ] [ <a href="addvoucher.php?type=setup">
         <?=$a_lang[ADD_SETUP_VOUCHER];?>
         </a> ]</td>
   </tr>
   <tr>
      <td class="c4"><b>1.</b>
         <?=$a_lang[SIGNUP_VOUCHER];?></td>
   </tr>
   <tr>
      <td><table width="100%"  border="0" cellspacing="2" cellpadding="1" class="border">
            <tr class="c4">
               <td>Voucher Name </td>
               <td>Voucher Code</td>
               <td align="center" width="130">Reg. Date</td>
               <td align="center" width="130">Exp. Date</td>
               <td align="center" width="60"># of uses</td>
               <td align="center" width="70">Options</td>
            </tr>
            <? 
				$getSignupVouchers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_vouchers WHERE voucher_type='signup'");
				while ($signupVoucher=mysqli_fetch_array($getSignupVouchers)) { ?>
            <tr class="<? echo ($cnt++%2) ? "c1":"c2";?>">
               <td><?=$signupVoucher['voucher_name'];?></td>
               <td><?=$signupVoucher['voucher_code'];?></td>
               <td align="center"><?=date($setts['date_format'],$signupVoucher['regdate']);?></td>
               <td align="center"><? echo ($signupVoucher['expdate']>0) ? date($setts['date_format'],$signupVoucher['expdate']) : $a_lang[NA];?></td>
               <td align="center"><? echo ($signupVoucher['nbuses']>0) ? $signupVoucher['nbuses'] : $a_lang[NA];?></td>
               <td align="center">[ <a href="addvoucher.php?type=signup&option=edit&id=<?=$signupVoucher['id'];?>">Edit</a> ]<br>
                  [ <a href="managevouchers.php?option=delete&id=<?=$signupVoucher['id'];?>">Delete</a> ]</td>
            </tr>
            <? } ?>
         </table></td>
   </tr>
   <tr>
      <td class="c4"><b>2.</b>
         <?=$a_lang[SETUP_VOUCHERS];?></td>
   </tr>
   <tr>
      <td><table width="100%"  border="0" cellspacing="2" cellpadding="1" class="border">
            <tr class="c4">
               <td width="150">Voucher Name </td>
               <td width="95">Voucher Code</td>
               <td>Voucher Details</td>
               <td align="center" width="70">Options</td>
            </tr>
            <? 
				$getSetupVouchers=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_vouchers WHERE voucher_type='setup'");
				while ($setupVoucher=mysqli_fetch_array($getSetupVouchers)) { ?>
            <tr class="<? echo ($cnt++%2) ? "c1":"c2";?>">
               <td><?=$setupVoucher['voucher_name'];?></td>
               <td><?=$setupVoucher['voucher_code'];?></td>
               <td><?
					  	echo "<strong>Reduction</strong>: ".$setupVoucher['reduction']."%<br>";
					  	echo "<strong>Reg. Date</strong>: ".date($setts['date_format'],$setupVoucher['regdate'])."<br>";
					  	echo "<strong>Exp. Date</strong>: ".(($setupVoucher['expdate']>0) ? date($setts['date_format'],$setupVoucher['expdate']) : $a_lang[NA])."<br>";
					  	echo "<strong># of uses</strong>: ".(($setupVoucher['nbuses']>0) ? $setupVoucher['nbuses'] : $a_lang[NA])."<br>";
					  	echo "<strong>Fees Reduced</strong>: ".$setupVoucher['assignto']."<br>";
					  	echo "<strong>Applied to Users</strong>: ".(($setupVoucher['assigned_users']!="")?"some":"all")."<br>";
					  	?></td>
               <td align="center">[ <a href="addvoucher.php?type=setup&option=edit&id=<?=$setupVoucher['id'];?>">Edit</a> ]<br>
                  [ <a href="managevouchers.php?option=delete&id=<?=$setupVoucher['id'];?>">Delete</a> ]</td>
            </tr>
            <? } ?>
         </table></td>
   </tr>
   <tr class="c3">
      <td align="center"><b>
         <?=$a_lang[ADVERTISE_VOUCHERS];?>
         </b></td>
   </tr>
   <tr>
      <td align="center">[ <a href="annedit.php">
         <?=$a_lang[SEND_ANNOUNCEMENT];?>
         </a> ] [ <a href="sendnewsletter.php">
         <?=$a_lang[SEND_NEWSLETTER];?>
         </a> ]</td>
   </tr>
</table>
<? 	include ("footer.php"); 
} ?>
