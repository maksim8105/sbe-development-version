<?
## v5.22 -> oct. 12, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include_once ('../config/lang/'.$_SESSION['sess_lang'].'/site.lang');

if (isset($_GET['savesettsok'])) {
	for ($i=0; $i<count($_GET['id']); $i++) {
		$updateQuestion[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_public_msg SET content='".remSpecialChars($_GET['question'][$i])."' WHERE id='".$_GET['id'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$updateAnswer[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_public_msg SET content='".remSpecialChars($_GET['answer'][$i])."' WHERE answerid='".$_GET['id'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
}

if ($_GET['option']=="delete") { 
	$delQ = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_public_msg WHERE id='".$_GET['id']."'");
	$delA = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_public_msg WHERE answerid='".$_GET['id']."'");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_auct.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[AUCT_MANAGE]; echo " / "; echo $a_lang[PUBLIC_QUESTIONS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <tr class="c3"> 
    <td align="center"><b> 
      <?=strtoupper($a_lang[PUBLIC_QUESTIONS]);?> 
      </b></td> 
  </tr> 
  <?
	if ($_GET['start'] == "") $start = 0;
	else $start = $_GET['start'];
	$limit = 10;

	$nbMsg = getSqlNumber("SELECT * FROM probid_public_msg WHERE msgtype='Q'  
	ORDER BY auctionid DESC, regdate DESC"); 
	$getMsg = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_public_msg WHERE msgtype='Q'  
	ORDER BY auctionid DESC, regdate DESC LIMIT ".$start.",".$limit."");
	
	$additionalVars = "";
	?> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form action="<?=$_SERVER['PHP_SELF'];?>" method="get">
  <tr class="c3"> 
    <td valign="top"><strong><?=$a_lang[DETAILS];?></strong></td> 
    <td width="30%"><strong><?=$a_lang[QUESTION];?></strong></td> 
    <td width="30%"><strong><?=$a_lang[ANSWER];?></strong></td> 
    <td align="center" width="80"><strong><?=$a_lang[OPTIONS];?></strong></td> 	
  </tr> 
  <input type="hidden" name="start" value="<?=$_REQUEST['start'];?>">
  <? while ($msg = mysqli_fetch_array($getMsg)) { ?> 
  <input type="hidden" name="id[]" value="<?=$msg['id'];?>"> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
    <td>
	<strong><?=$a_lang[AUCTION_ID];?>: </strong> <a href="../auctiondetails.php?id=<?=$msg['auctionid'];?>" target="_blank">#<?=$msg['auctionid'];?></a><br />
	<strong><?=$a_lang[AUCTION_TITLE];?>: </strong> <?=getSqlField("SELECT itemname FROM probid_auctions WHERE id='".$msg['auctionid']."'","itemname"); ?><br />
	<strong><?=$a_lang[POSTED_BY];?>: </strong> <a href="userdetails.php?id=<?=$msg['posterid'];?>"><?=getSqlField("SELECT username FROM probid_users WHERE id='".$msg['posterid']."'","username"); ?></a><br />
	<strong><?=$a_lang[ANSWERED_BY];?>: </strong> <a href="userdetails.php?id=<?=$msg['ownerid'];?>"><?=getSqlField("SELECT username FROM probid_users WHERE id='".$msg['ownerid']."'","username"); ?></a><br />
	</td> 
    <td><textarea name="question[]" class="smallfont" id="question[]" style="{ width:100%; height=70px; }"><? echo addSpecialChars($msg['content']);?></textarea></td> 
    <td><textarea name="answer[]" class="smallfont" id="answer[]" style="{ width:100%; height=70px; }"><? echo addSpecialChars(getSqlField("SELECT content FROM probid_public_msg WHERE answerid='".$msg['id']."'","content"));?></textarea></td> 
    <td align="center" width="80"><a href="managepublicquestions.php?option=delete&id=<?=$msg['id'];?>&start=<?=$start;?>"><?=$a_lang[DELETE];?></a></td> 	
  </tr> 
  <? } ?> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
    <td colspan="4" align="center" class="c4"><input type="submit" name="savesettsok" value="<?=$a_lang[MAKE_CHANGES];?>"></td> 
  </tr> 
  </form>
</table> 
<table width="100%"  border="0" cellspacing="4" cellpadding="4"> 
  <tr> 
    <td align="center"><? paginate($start,$limit,$nbMsg,"managepublicquestions.php",$additionalVars); ?></td> 
  </tr> 
</table> 
<? 	include ("footer.php"); 
} ?>