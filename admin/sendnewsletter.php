<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");


if (isset($_POST['sendnlok'])) {
  if (is_array($_POST["lang"]))
  {
      $where = " AND mylang IN (";
      foreach ($_POST["lang"] as $row=>$key) {
          $i++;
          $where .="'".$key."'".($i<count($_POST["lang"])?',':'');
      }
      $where .= ")";
  
  }

	$newsletterContent=remSpecialChars($_POST['content']);
	if ($_POST['subject']!=""&&$_POST['content']!="") {
		$savedSettings="yes";
		if ($_POST['sendto']==1) $query="SELECT email FROM probid_users";
		if ($_POST['sendto']==2) $query="SELECT email FROM probid_users WHERE active=1";
		if ($_POST['sendto']==3) $query="SELECT email FROM probid_users WHERE active=0";
		if ($_POST['sendto']==4) $query="SELECT email FROM probid_users WHERE newsletter='Y'";

		$getUsers=mysqli_query($GLOBALS["___mysqli_ston"], $query);
		while ($userDetails = mysqli_fetch_array($getUsers)) {
			htmlmail($userDetails['email'],$_POST['subject'],'This message was sent in HTML',
			$setts['adminemail'],addSpecialChars($newsletterContent));
		}
	} else {
		$savedSettings="no";
	}
}

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[USER_NEWS];?>&nbsp;&nbsp;</td>
    <td><img src="images/end_part.gif"></td>
  </tr>
</table>
<br>
<? 
echo ($savedSettings=="yes")?"<p align=\"center\">$a_lang[USER_NEWS_SEND]</p>":""; 
echo ($savedSettings=="no")?"<p align=\"center\">$a_lang[WARN_NEWS_SEND].</p>":""; 
?>
<form action="sendnewsletter.php" method="post">
  <table width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr>
      <td colspan="2" align="center" class="c3"><b>
        <?=$a_lang[TITLE_NEWS_SEND];?>
        </b></td>
    </tr>
    <tr align="center" >
      <td colspan="2"><b><a href="nlsubscribers.php">
        <?=$a_lang[VIEW_NEWS_SUBSCRIB];?>
        </a></b></td>
    </tr>
    <tr class="c1">
      <td width="150"><?=$a_lang[NEWS_SEND_TO];?>
        :</td>
      <td><select name="sendto" id="sendto">
          <option value="1" selected>
          <?=$a_lang[NEWS_SEND_ALL];?>
          </option>
          <option value="2">
          <?=$a_lang[NEWS_SEND_ACTIVE];?>
          </option>
          <option value="3">
          <?=$a_lang[NEWS_SEND_SUSPEND];?>
          </option>
          <option value="4">
          <?=$a_lang[NEWS_SEND_SUBSCRIBERS];?>
          </option>
        </select></td>
    </tr>
    <tr class="c1">
        <td>Language:</td>
        <td>
            <?=myLangChecks($_POST["lang"])?>
        </td>
    </tr>
    <tr class="c2">
      <td><?=$a_lang[SUBJECT];?>
        :</td>
      <td><input name="subject" type="text" id="subject"></td>
    </tr>
    <tr class="c1">
      <td><?=$a_lang[ENTER_CONTENT];?>
        :</td>
      <td><textarea name="content" cols="45" rows="10" id="content"><?=addSpecialChars($newsletterContent);?></textarea>
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW];?></td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="c3"><input name="sendnlok" type="submit" id="sendnlok" value="<?=$a_lang[BUTT_NEWS_SEND];?>"></td>
    </tr>
  </table>
</form>
<? 	include ("footer.php"); 
} ?>
