<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "NB! Message encoding: UTF-8															\n".
				"																					\n".
				"Hea ".$buyer['name']."															\n".
				"																					\n".
				"Teie vahetustehingu pakkumine esemele ".$auction['itemname']." on müüja poolt tagasi lükatud.			\n".
				"																					\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Täname,																			\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon							\n".
				
				"																					\n".
				"-----------------------------------------									\n".
				"																					\n".

				"Здравствуйте, ".$buyer['name']."															\n".
				"																					\n".
				"Ваше предложение об обмене для лота ".$auction['itemname']." отклонено продавцом.			\n".
				"																					\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Спасибо,																			\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 							\n".
				
				"																					\n".
				"-----------------------------------------									\n".
				"																					\n".
				
				"Dear ".$buyer['name']."															\n".
				"																					\n".
				"You swap offer for ".$auction['itemname']." was rejected by the seller.			\n".
				"																					\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Thank you,																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8															<br>".
				"																					<br>".
				"Hea ".$buyer['name']."															<br>".
				"																					<br>".
				"Teie vahetustehingu pakkumine esemele ".$auction['itemname']." on müüja poolt tagasi lükatud.			<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia.</a><br>".
				"																					<br>".
				"Täname,																			<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon							<br>".
				
				"																					<br>".
				"-------------------------------------												<br>".
				"																					<br>".

				"Здравствуйте, ".$buyer['name']."															<br>".
				"																					<br>".
				"Ваше предложение обмена для лота ".$auction['itemname']." отклонено продавцом.			<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Для просмотра аукциона нажмите сюда.</a><br>".
				"																					<br>".
				"Спасибо,																			<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 							<br>".
				
				"																					<br>".
				"-------------------------------------												<br>".
				"																					<br>".
				
				"Dear ".$buyer['name']."															<br>".
				"																					<br>".
				"You swap offer for ".$auction['itemname']." was rejected by the seller.			<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
				"																					<br>".
				"Thank you,																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Swap Offer Rejected",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>