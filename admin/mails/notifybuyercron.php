<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($buyer['mail_auctionwon']==1) ? TRUE : FALSE;
$plainMessage ="Dear ".$name.",																							\n".
					"																												\n".
					"You have won an auction on ".$setts['sitename'].".											\n".
					"																												\n".
					"Auction # ".$auctionId."																				\n".
					"Name:	".$auctionName."																				\n".
					"Price:	".displayAmount($maxBid)."																	\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																												\n";

if ($auction['acceptdirectpayment'])
	$plainMessage.="The seller prefers PayPal payments, and you can pay for the item						\n".
						"you won through PayPal, by accessing the won items page in your						\n".
						"members area.																							\n".
						"																											\n";

$plainMessage.="To conclude this purchase please login to the member�s area � buying and visit the \n".
					"\"Items� I�ve Won in detail\" section.  From here click on the \"Message Board\" link next to each item won.\n";
					"																												\n".
					"This message board is your direct communication board with the seller.					\n".
					"																												\n".
					"Please use this board to ask any post sale questions you.									\n".
					"																												\n".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.\n".
					"																												\n".
					"Thank you,																									\n".
					"The ".$setts['sitename']." Staff";
				
$htmlMessage = "Dear ".$name.",																			<br>".
					"																								<br>".
					"You have won an auction on <b>".$setts['sitename']."</b>.					<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Auction # </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$auctionName."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Price:</td>													\n".
					"		<td>".$htmlfont.displayAmount($maxBid)."</td>							\n".
					"	</tr>																						\n";
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Auction Image:</td>								\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}
					"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>";

if ($auction['acceptdirectpayment'])
	$htmlMessage.=	"The seller prefers PayPal payments, and you can pay for the item		<br>".
						"you won through PayPal, by accessing the won items page in your		<br>".
						"members area.																			<br>".
						"																							<br>";
						
$htmlMessage.=	"To conclude this purchase please login to the member�s area � buying and visit the <br>".
					"\"Items� I�ve Won in detail\" section.  From here click on the \"Message Board\" link next to each item won.<br>";
					"																												<br>".
					"This message board is your direct communication board with the seller.					<br>".
					"																												<br>".
					"Please use this board to ask any post sale questions you.									<br>".
					"																												<br>".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.<br>".
					"																												<br>".
					"Thank you,																									<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($email,"Auction Closed",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);
?>