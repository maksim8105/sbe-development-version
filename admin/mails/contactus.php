<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "A new message has been sent through the contact us form. 				\n".
				"																		\n".
				"Name: ".$_POST['name']."												\n".
				"Email:	".$_POST['email']."												\n".
				"Username: ".$_POST['username']."										\n".
				"																		\n".
				"Question / Query: ".$_POST['question'];

$htmlMessage = 	"A new message has been sent through the contact us form. 				<br>".
				"																		<br>".
				"Name: ".$_POST['name']."												<br>".
				"Email:	".$_POST['email']."												<br>".
				"Username: ".$_POST['username']."										<br>".
				"																		<br>".
				"Question / Query: ".$_POST['question'];

htmlmail($setts['adminemail'],$setts['sitename']." - New Contact Message",
$plainMessage,$_POST['email'],$htmlMessage);
?>