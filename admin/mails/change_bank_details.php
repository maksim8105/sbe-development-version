<?php
## v5.24 -> apr. 06, 2006
$winnerDetails = getSqlRow("SELECT * FROM probid_winners WHERE id = '".$winnerid."'");
$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id = '".$winnerDetails['auctionid']."'");
$buyer = getSqlRow("SELECT * FROM probid_users WHERE id='".$winnerDetails['buyerid']."'");
$seller = getSqlRow("SELECT * FROM probid_users WHERE id='".$winnerDetails['sellerid']."'");
$bankdetails = htmlspecialchars(stripslashes($bankdetails));

$plainMessage = "Dear ".$buyer['name'].",												\n".
					 "																		\n".
					 "The seller has changed his bank details.                           	\n".
					 "																		\n".
					 "Auction # ".$auctionDetails['id']."	          						\n".
					 "Name:	".$auctionDetails['itemname']."									\n".
					 "Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auctionDetails['itemname'], 'id' => $auctionDetails['id']))."	\n".
					 "																		\n".
					 "New seller's bank details:												\n".
   				 $bankdetails."															\n".
   				 "																		\n".
   				 "You can also show them on the page:".$setts['siteurl']."login.php?redirect=sellerinfo\n".
   				 "																		\n".
    				 "Thank you,																\n".
					 "The ".$setts['sitename']." Staff";

$htmlMessage =  "Dear ".$buyer['name'].",												<br>".
					 "																		<br>".
					 "The seller has changed his bank details.                           	<br>".
					 "																		<br>".
					 "<table border=\"0\">													\n".
					 "	<tr>																\n".
				 	"		<td>".$htmlfont."Auction # </td>                                \n".
				 	"		<td>".$htmlfont.$auctionDetails['id']."</td>                    \n".
				 	"	</tr>																\n".
				 	"	<tr>																\n".
				 	"		<td>".$htmlfont."Name:</td>										\n".
				 	"		<td>".$htmlfont.$auctionDetails['itemname']."</td>				\n".
				 	"	</tr>																\n".
				 	"	<tr>																\n".
				 	"		<td>".$htmlfont."Auction URL:</td>								\n".
				 	"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auctionDetails['itemname'], 'id' => $auctionDetails['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auctionDetails['itemname'], 'id' => $auctionDetails['id']))."</a></td>\n".
				 	"	</tr>																\n".
				 	"	<tr>																\n".
				 	"		<td valign=top>".$htmlfont."New seller's bank details:</td>		\n".
				 	"		<td>".$htmlfont.str_replace("\r\n","<br>",$bankdetails)."</td>	\n".
				 	"	</tr>																\n".
				 	"	<tr>																\n".
				 	"		<td valign=top>".$htmlfont."You can also show them on the page:</td>\n".
				 	"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=sellerinfo\">click here</a></td>	\n".
				 	"	</tr>																\n".
				 	"</table>																\n".
				 	"																		<br>".
				 	"																		<br>".
				 	"Thank you,																<br>".
				 	"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Bank Details has changed!",$plainMessage,
$setts['adminemail'],$htmlMessage,'TRUE');
?>