<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$recipientId."'");

$plainMessage = "The following user has applied for VAT exempt:							\n"
				."																		\n"
				."Username: ".$userDetails['username']."								\n"
				."User ID: ".$recipientId."												\n"
				."																		\n"
				."Tax/VAT UID Number: ".$userDetails['vat_uid_number']."				\n"
				."																		\n"
				."To verify the validity of the VAT UID number, click on the link below:\n"
				."																		\n"
				."http://europa.eu.int/comm/taxation_customs/vies/en/vieshome.htm		\n"
				."NOTE: This link applies for the EU only.								\n"
				."																		\n"
				."You can activate VAT exempt for this user from the admin area,		\n"
				."users management page.												\n"
				."																		\n"
				."Best Regards,															\n"
				."The ".$setts['sitename']." Staff";

$htmlMessage = 	"The following user has applied for VAT exempt:							<br>"
				."																		<br>"
				."Username: ".$userDetails['username']."								<br>"
				."User ID: ".$recipientId."												<br>"
				."																		<br>"
				."Tax/VAT UID Number: ".$userDetails['vat_uid_number']."				<br>"
				."																		<br>"
				."To verify the validity of the VAT UID number, click on the link below:<br>"
				."																		<br>"
				."<a href=\"http://europa.eu.int/comm/taxation_customs/vies/en/vieshome.htm\">Click to verify VAT validity</a><br>"
				."NOTE: This link applies for the EU only.								<br>"
				."																		<br>"
				."You can activate VAT exempt for this user from the admin area,		<br>"
				."users management page.												<br>"
				."																		<br>"
				."Best Regards,															<br>"
				."The ".$setts['sitename']." Staff";
				
htmlmail($setts['adminemail'],"New VAT Exempt application",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>