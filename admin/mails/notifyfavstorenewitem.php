<?php
## v5.24 -> apr. 12, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = TRUE;
$plainMessage = "Dear ".$buyer['name']."													\n".
					 "																					\n".
					 "A new item, ".$auction['itemname']." has been added to one of your favourite stores.\n".
					 "																					\n".
					 "Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\n".
					 "																					\n".
					 "Store URL: ".$setts['siteurl'].processLink('shop', array('store' => $seller['store_name'], 'userid' => $seller['id']))."\n".
					 "																					\n".
					 "Best regards,																\n".
					 "The ".$setts['sitename']." Staff";

$htmlMessage =  "Dear ".$seller['name']."															<br>".
					 "																					<br>".
					 "Your item, ".$buyer['itemname']." has been added to one of your favourite stores.		<br>".
					 "																					<br>".
					 "<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
					 "																					<br>".
					 "<a href=\"".$setts['siteurl'].processLink('shop', array('store' => $seller['store_name'], 'userid' => $seller['id']))."\">Click here to view the shop.</a><br>".
					 "																					<br>".
					 "Best regards,																<br>".
					 "The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Favourite Store Notification",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>