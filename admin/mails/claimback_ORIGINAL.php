<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "End of auction fee claim back request:											\n".
				"																				\n".
				"From: ".$seller['name']."; Username: ".$seller['username']."					\n".
				"																				\n".
				"Auction ID: ".$auctionId."														\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."			\n".
				"																				\n".
				"Buyer ID: ".$buyer['id']."; Buyer Username: ".$buyer['username']."				\n".
				"																				\n".
				"Fee Amount: ".displayAmount($amount)."											\n".
				"																				\n".	
				"Transaction ID: ".$txnid."														\n".
				"																				\n".	
				"If the Transaction ID states 'Test Transaction', then disregard this message.	\n".
				"Otherwise, this ID means the Transaction ID from PayPal.";

$htmlMessage = 	"End of auction fee claim back request:											<br>".
				"																				<br>".
				"From: ".$seller['name']."; Username: ".$seller['username']."					<br>".
				"																				<br>".
				"Auction ID: ".$auctionId."														<br>".
				"Auction URL: <a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction</a><br>".
				"																				<br>".
				"Buyer ID: ".$buyer['id']."; Buyer Username: ".$buyer['username']."				<br>".
				"																				<br>".
				"Fee Amount: ".displayAmount($amount)."											<br>".
				"																				<br>".	
				"Transaction ID: ".$txnid."														<br>".
				"																				<br>".	
				"If the Transaction ID states 'Test Transaction', then disregard this message.	<br>".
				"Otherwise, this ID means the Transaction ID from PayPal.";

htmlmail($setts['adminemail'],"End of Auction Fee Claim Back Request",
$plainMessage,$seller['email'],$htmlMessage);
?>