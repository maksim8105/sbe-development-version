<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($user['mail_keywordmatch']==1) ? TRUE : FALSE;
$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$user['name'].",													\n".
				"																		\n".
				"Oksjon, mis vastab Teie võtmesõnale, \"".$keyword."\", on sisestatud meie baasi.\n".
				"																		\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Täname tähelepanu eest,												\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".

				"																		\n".
				"-------------------------------------------------------------------	\n".
				"																		\n".

				"Здравствуйте, ".$user['name'].",										\n".
				"																		\n".
				"Aукцион, соответсвующий Вашему ключевому слову, \"".$keyword."\", появился в нашей базе.\n".
				"																		\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Благодарим за внимание,												\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".

				"																		\n".
				"-------------------------------------------------------------------	\n".
				"																		\n".
				
				"Dear ".$user['name'].",												\n".
				"																		\n".
				"An auction matching one of your keywords, \"".$keyword."\", was entered into our database.\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." staff";

$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$user['name'].",													<br>".
				"																		<br>".
				"Oksjon, mis vastab Teie võtmesõnale, \"".$keyword."\", on sisestatud meie baasi.<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia</a><br>".
				"																		<br>".
				"Täname tähelepanu eest,												<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".

				"																		<br>".
				"-------------------------------------------------------------------	<br>".
				"																		<br>".

				"Здравствуйте, ".$user['name'].",										<br>".
				"																		<br>".
				"Aукцион, соответствующий Вашему ключевому слову, \"".$keyword."\", появился в нашей базе.<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Нажмите сюда для просмотра аукциона</a><br>".
				"																		<br>".
				"Благодарим за внимание,												<br>".
				"Администрация интернет-аукциона ".$setts['sitename']."					<br>".

				"																		<br>".
				"-------------------------------------------------------------------	<br>".
				"																		<br>".
				
				"Dear ".$user['name'].",												<br>".
				"																		<br>".
				"An auction matching one of your keywords, \"".$keyword."\", was entered into our database.<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click to view the auction</a><br>".
				"																		<br>".
				"Thank you for your interest,											<br>".
				"The ".$setts['sitename']." staff";

htmlmail($user['email'],"Keyword Match",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>