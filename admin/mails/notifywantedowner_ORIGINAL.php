<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$owner = getSqlRow ("SELECT * FROM probid_users WHERE id='".$ownerId."'");

$sendMail = ($owner['mail_wantedoffer']==1) ? TRUE : FALSE;
$plainMessage = "An offer has been posted on one of your wanted ads.								\n".
				"																					\n".
				"Wanted Ad URL: ".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."			\n".
				"																					\n".
				"Thank you,																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"An offer has been posted on one of your wanted ads.								<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl']."wanted.details.php?id=".$wantedAdId."\">Click here to view the wanted ad details page.</a><br>".
				"																					<br>".
				"Thank you,																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($owner['email'],"New Wanted Ad Offer.",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);

?>