<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$recipientId."'");

$plainMessage = "Dear ".$userDetails['name'].",											\n"
				."Thank you for your registration to ".$setts['sitename'].".			\n"
				."																		\n"
				."Your login information is as follows:									\n"
				."																		\n"
				."Username: ".$userDetails['username']."								\n"
				."Password: <your chosen password>										\n"
				."																		\n"
				."Your account will be manually activated by the site admin. 			\n"
				."You will be notified by email once your account will become active.	\n"
				."																		\n"
				."Best Regards,															\n"
				."The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$userDetails['name'].",											<br>"
				."Thank you for your registration to ".$setts['sitename'].".			<br>"
				."																		<br>"
				."Your login information is as follows:									<br>"
				."																		<br>"
				."Username: ".$userDetails['username']."								<br>"
				."Password: <em>your chosen password</em>								<br>"
				."																		<br>"
				."Your account will be manually activated by the site admin. 			<br>"
				."You will be notified by email once your account will become active.	<br>"
				."																		<br>"
				."Best Regards,															<br>"
				."The ".$setts['sitename']." Staff";
				
htmlmail($userDetails['email'],$setts['sitename']." Registration",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>