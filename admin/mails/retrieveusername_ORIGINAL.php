<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");

$plainMessage = "Dear Subscriber,														\n".
				"																		\n".
				"Your login information to ".$setts['sitename']." is:					\n".
				"																		\n".
				"	Username: ".$user['username']."										\n".
				"																		\n".
				"Best Regards,															\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear Subscriber,														<br>".
				"																		<br>".
				"Your login information to ".$setts['sitename']." is:					<br>".
				"																		<br>".
				"	Username: ".$user['username']."										<br>".
				"																		<br>".
				"Best Regards,															<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($user['email'],$setts['sitename']." Login Information",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>