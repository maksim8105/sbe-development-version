<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

for ($i=0; $i<count($cat_array); $i++) list($catid[$i], $catname[$i]) = each($cat_array);
for ($i=0; $i<count($catid); $i++) if ($catid[$i]==$auction['category']) $category_name = $catname[$i];

$plainMessage = "You have successfully relisted multiple auctions on ".$setts['sitename'].":	\n".
				"																				\n".
				"For more details please access the \"Selling\" tab in your members area. 		\n".
				"																				\n".
				"Thank you for your submission.													\n".
				"The ".$setts['sitename']." staff";
				
$htmlMessage = 	"You have successfully relisted multiple auctions on on ".$setts['sitename'].":	<br>".
				"																				<br>".
				"For more details please access the \"Selling\" tab in your members area. 		<br>".
				"																				<br>".
				"Thank you for your submission.													<br>".
				"The ".$setts['sitename']." staff";
				
htmlmail($user['email'],"Multiple Auctions Relist Confirmation",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>