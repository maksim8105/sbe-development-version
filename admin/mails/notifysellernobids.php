<?php
## v5.25 -> jun. 27, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($seller['mail_auctionclosed']==1) ? TRUE : FALSE;
$plainMessage =		"NB! Message encoding: UTF-8							\n".
					"																								\n".
					"Hea ".$name.",																			\n".
					"																								\n".
					"Oksjon nr ".$auctionId." on lõppenud ilma võitjata. Pakkumisi ei olnud või Teie reservhind ei olnud saavutatud.	\n".
					"																								\n".
					"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Täname,																					\n".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
					
					"																								\n".
					"----------------------------------------------------------------------							\n".
					"																								\n".

					"Здравствуйте, ".$name.",																			\n".
					"																								\n".
					"Аукцион #".$auctionId." завершился без победителя. Не поступило ставок либо указанная резервная цена не была достигнута.	\n".
					"																								\n".
					"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Спасибо,																					\n".
					"Администрация интернет-аукциона ".$setts['sitename']." 					\n".
					
					"																								\n".
					"----------------------------------------------------------------------							\n".
					"																								\n".
					
					"Dear ".$name.",																			\n".
					"																								\n".
					"Auction #".$auctionId." has closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.\n".
					"																								\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Thank you,																					\n".
					"The ".$setts['sitename']." Staff";
				
$htmlMessage =		"NB! Message encoding: UTF-8								<br>".
					"																								<br>".
					"Hea ".$name.",																			<br>".
					"																								<br>".
					"Oksjon nr ".$auctionId." on lõppenud ilma võitjata. Pakkumisi ei olnud või Teie reservhind ei olnud saavutatud.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n";
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Oksjoni pilt:</td>								\n".
						"		<td><img src=\"".$setts['siteurl']."makethumb.php?pic=".$setts['siteurl'].$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}
$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Oksjoni URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Täname,																					<br>".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon											<br>".
					
					"																								<br>".
					"-------------------------------------------------------										<br>".
					"																								<br>".

					"Здравствуйте, ".$name.",																			<br>".
					"																								<br>".
					"Аукцион #".$auctionId." завершился без победителя. Не поступило ставок либо указанная резервная цена не была достигнута.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n";
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Изображение аукциона:</td>								\n".
						"		<td><img src=\"".$setts['siteurl']."makethumb.php?pic=".$setts['siteurl'].$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}
$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."URL аукциона:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Спасибо,																					<br>".
					"Администрация интернет-аукциона ".$setts['sitename']." 												<br>".
					
					"																								<br>".
					"-------------------------------------------------------										<br>".
					"																								<br>".
					
					"Dear ".$name.",																			<br>".
					"																								<br>".
					"Auction #".$auctionId." has closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n";
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Auction Image:</td>								\n".
						"		<td><img src=\"".$setts['siteurl']."makethumb.php?pic=".$setts['siteurl'].$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}
$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Thank you,																					<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($email,"Auction Closed - Item Not Sold",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);

?>