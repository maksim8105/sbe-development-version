<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($seller['mail_auctionsold']==1) ? TRUE : FALSE;
$plainMessage =		"NB! Message encoding: UTF-8				\n".
					"																												\n".
					"Hea ".$seller['name']."																				\n".
					"																												\n".
					"Teie ese, ".$auction['itemname'].", on müüdud ja ostja kasutas 'Osta kohe' võimalust.		\n".
					"																												\n".
					"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																												\n".
					"Selle tehingu lõpetamiseks külastage palun oma kontot, valige 'Minu müüdud esemed' ja siis \n".
					"'Teadetetahvel' valitud eseme kõrval.  \n".
					"																												\n".
					"Teadetetahvel on ette nähtud Teie suhtlemiseks ostjaga.					\n".
					"																												\n".
					"Vältimaks võimalikke arusaamatusi kasutage palun alati küsimuste korral teadetetahvli.		\n".
					"																												\n".					
					"Täname,																									\n".
					"Virtuaalse oksjoni ".$setts['sitename']." meeskond																		\n".
					
					"																												\n".
					"--------------------------------------------							\n".
					"																												\n".

					"Здравствуйте, ".$seller['name']."																				\n".
					"																												\n".
					"Ваш лот, ".$auction['itemname'].", продан с помощью опции 'Купить сейчас'.		\n".
					"																												\n".
					"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																												\n".
					"Для завершения сделки войдите в свой аккаунт, откройте раздел 'Проданные мною лоты'  \n".
					"и возле выбранного лота нажмите 'Доска объявлений'.\n".
					"																												\n".
					"Доска объявлений предназначена для прямого общения с покупателем.					\n".
					"																												\n".
					"Во избежание возможных недоразумений всегда используйте доску объявлений, если у Вас есть вопросы к покупателю.	\n".
					"																												\n".
					"Спасибо,																									\n".
					"Администрация интернет-аукциона ".$setts['sitename']." 						\n".
					
					"																												\n".
					"--------------------------------------------							\n".
					"																												\n".

					"Dear ".$seller['name']."																				\n".
					"																												\n".
					"Your item, ".$auction['itemname']." has been bought using the BUY NOW feature.		\n".
					"																												\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																												\n".
					"To conclude this sale please login to the members area - selling and visit the 	\n".
					"\"Items I've Sold in detail\" section.  From here click on the \"Message Board\" 	\n".
					"link next to each item sold.																			\n".
					"																												\n".
					"This message board is your direct communication board with your buyer.					\n".
					"																												\n".
					"Please use this board to answer any questions the buyer may have regarding payment and delivery.\n".
					"																												\n".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.\n".
					"																												\n".
					"Thank you,																									\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage = 		"NB! Message encoding: UTF-8							<br>".
					"																												<br>".
					"Hea ".$seller['name']."																				<br>".
					"																												<br>".
					"Teie ese, ".$auction['itemname'].", on müüdud ja ostja kasutas 'Osta kohe' võimalust.		<br>".
					"																												<br>";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"Oksjoni pilt:																						<br>".
						"<img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"><br>".
						"																											<br>";
}

$htmlMessage.=	"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia.</a><br>".
					"																												<br>".
					"Selle tehingu lõpetamiseks külastage palun oma kontot, valige 'Minu müüdud esemed' ja siis <br>".
					"'Teadetetahvel' valitud eseme kõrval.<br>";
					"																												<br>".
					"Teadetetahvel on ette nähtud Teie suhtlemiseks ostjaga.					<br>".
					"																												<br>".
					"Vältimaks võimalikke arusaamatusi kasutage palun alati küsimuste korral teadetetahvli.		<br>".
					"																												<br>".
					"Täname,																									<br>".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon						<br>".
					
					"																												<br>".
					"-----------------------------------------------------------					<br>".
					"																												<br>".

					"Здравствуйте, ".$seller['name']."																				<br>".
					"																												<br>".
					"Ваш лот, ".$auction['itemname'].", был продан с помощью опции 'Купить сейчас'.		<br>".
					"																												<br>";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"Изображение:																						<br>".
						"<img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"><br>".
						"																											<br>";
}

$htmlMessage.=	"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Для просмотра аукциона нажмите сюда.</a><br>".
					"																												<br>".
					"Для завершения сделки войдите в свой аккаунт, откройте раздел 'Проданные мною лоты' <br>".
					"и возле выбранного лота нажмите 'Доска объявлений'.<br>";
					"																												<br>".
					"Доска объявлений предназначена для прямого общения с покупателем.					<br>".
					"																												<br>".
					"Во избежание возможных недоразумений всегда используйте доску объявлений, если у Вас есть вопросы к покупателю.	<br>".
					"																												<br>".
					"Спасибо,																									<br>".
					"Администрация интернет-аукциона ".$setts['sitename']." 							<br>".
					
					"																												<br>".
					"-----------------------------------------------------------					<br>".
					"																												<br>".
					
					"Dear ".$seller['name']."																				<br>".
					"																												<br>".
					"Your item, ".$auction['itemname']." has been bought using the BUY NOW feature.		<br>".
					"																												<br>";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"Auction Image:																						<br>".
						"<img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"><br>".
						"																											<br>";
}

$htmlMessage.=	"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
					"																												<br>".
					"To conclude this sale please login to the members area - selling and visit the 	<br>".
					"\"Items I've Sold in detail\" section.  From here click on the \"Message Board\" 	<br>".
					"link next to each item sold.																			<br>".
					"																												<br>".
					"This message board is your direct communication board with your buyer.					<br>".
					"																												<br>".
					"Please use this board to answer any questions the buyer may have regarding payment and delivery.<br>".
					"																												<br>".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.<br>".
					"																												<br>".
					"Thank you,																									<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"Auction Won with Buy Now",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>
