<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$sendMail = ($seller['mail_auctionclosed']==1) ? TRUE : FALSE;
$plainMessage = "Dear ".$name.",														\n".
				"																		\n".
				"Several of your auctions have closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"Dear ".$name.",														<br>".
				"																		<br>".
				"Several of your auctions have closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.	<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Multiple Auctions Closed - Items Not Sold",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);

?>