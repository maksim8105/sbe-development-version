<?php
## v5.24 -> apr. 05, 2006

##commented because of the new message board feature
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
$winner = getSqlRow ("SELECT * FROM probid_winners WHERE id='".$winnerId."'");

## $sendMail = ($seller['mail_buyerdetails']==1) ? TRUE : FALSE;

$sendMail = ($seller['mail_buyerdetails']==1);

if ($setts['enable_display_phone']=="N") $buyer['phone'] = "Undisclosed";

$plainMessage =		"NB! Message encoding: UTF-8								\n".
					"																								\n".
					"Hea ".$seller['name'].",																\n".
					"																								\n".
					"Olete müünud eseme süsteemis ".$setts['sitename'].".			\n".
					"																								\n".
					"Oksjoni ID: ".$auctionId."																\n".
					"Nimetus:	".$auction['itemname']."													\n".
					"Hind:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Ostja kontaktinfo:																	\n".
					"																								\n".
					"Kasutajanimi: ".$buyer['username']."																\n".
					"E-posti aadress: ".$buyer['email']."													\n".
					"Telefon: ".$buyer['phone']."															\n".
					"																								\n".
					"Võite samuti võtta ostjaga ühendust meie teadete tahvli abil. Klikkige lingile	\n".
					"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Täname,																					\n".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon								\n".
					
					"																								\n".
					"-------------------------------------------------------------	\n".
					"																								\n".

					"Здравствуйте, ".$seller['name'].",																\n".
					"																								\n".
					"Вы продали лот на странице ".$setts['sitename'].".			\n".
					"																								\n".
					"ID аукциона: ".$auctionId."																\n".
					"Название:	".$auction['itemname']."													\n".
					"Цена:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Контактные данные покупателя:																	\n".
					"																								\n".
					"Признак пользователя: ".$buyer['username']."																\n".
					"E-mail: ".$buyer['email']."													\n".
					"Телефон: ".$buyer['phone']."															\n".
					"																								\n".
					"Вы можете также связаться с покупателем через нашу доску объявлений. Нажмите на ссылку	\n".
					"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Спасибо,																					\n".
					"Администрация интернет-аукциона ".$setts['sitename']." 					\n".
					
					"																								\n".
					"-------------------------------------------------------------	\n".
					"																								\n".
					
					"Dear ".$seller['name'].",																\n".
					"																								\n".
					"You have sold the following item through ".$setts['sitename'].".			\n".
					"																								\n".
					"Auction # ".$auctionId."																\n".
					"Name:	".$auction['itemname']."													\n".
					"Price:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Buyer Contact Details:																	\n".
					"																								\n".
					"Login: ".$buyer['username']."																\n".
					"Phone: ".$buyer['phone']."															\n".
					"Email Address: ".$buyer['email']."													\n".
					"																								\n".
					"You can also contact the buyer in the message board. Follow the link	\n".
					"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Thank you,																					\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage = 		"NB! Message encoding: UTF-8									<br>".
					"																								<br>".
					"Hea ".$seller['name'].",																<br>".
					"																								<br>".
					"Olete müünud eseme süsteemis <b>".$setts['sitename']."</b>.<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Oksjoni ID: </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Nimetus:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Hind:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																					\n".
						"		<td>".$htmlfont."Oksjoni pilt:</td>									\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																					\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Oksjoni URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Ostja kontaktinfo:</b>														<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Kasutajanimi:</td>													\n".
					"		<td>".$htmlfont.$buyer['username']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."E-posti aadress: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$buyer['email']."\">".$buyer['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Telefon: </td>													\n".
					"		<td>".$htmlfont.$buyer['phone']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Klikkige paremal olevale lingile, et võtta ostjaga ühendust meie teadete tahvli abil</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Klikkige siia</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Täname,																					<br>".
					"Virtuaalse oksjoni ".$setts['sitename']." administratsioon										<br>".
					
					"																								<br>".
					"------------------------------------------------------	<br>".
					"																								<br>".

					"Здравствуйте, ".$seller['name'].",																<br>".
					"																								<br>".
					"Вы продали лот на странице <b>".$setts['sitename']."</b>.<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."ID аукциона: </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Название:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Цена:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																					\n".
						"		<td>".$htmlfont."Изображение:</td>									\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																					\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."URL аукциона:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Контактные данные покупателя:</b>														<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Признак пользователя:</td>													\n".
					"		<td>".$htmlfont.$buyer['username']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."E-mail: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$buyer['email']."\">".$buyer['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Телефон: </td>													\n".
					"		<td>".$htmlfont.$buyer['phone']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Нажмите на ссылку справа, чтобы связаться с покупателем через нашу доску объявлений</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Нажмите сюда</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Спасибо,																					<br>".
					"Администрация интернет-аукциона ".$setts['sitename']." 									<br>".
					
					"																								<br>".
					"------------------------------------------------------	<br>".
					"																								<br>".

					"Dear ".$seller['name'].",																<br>".
					"																								<br>".
					"You have sold the following item through <b>".$setts['sitename']."</b>.<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Auction # </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Price:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";

if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																					\n".
						"		<td>".$htmlfont."Auction Image:</td>									\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																					\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Buyer's Contact Details:</b>														<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Login:</td>													\n".
					"		<td>".$htmlfont.$buyer['username']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Phone: </td>													\n".
					"		<td>".$htmlfont.$buyer['phone']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Email Address: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$buyer['email']."\">".$buyer['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Follow the link on the right to contact the buyer in the message board</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Click Here</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Thank you,																					<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"Auction Closed - Item Sold!",$plainMessage,
$setts['adminemail'],$htmlMessage, $sendMail);
?>