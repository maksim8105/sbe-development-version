<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");

$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea kasutaja,															\n".
				"																		\n".
				"Teie sisselogimise info oksjonil ".$setts['sitename']." on:			\n".
				"																		\n".
				"	Kasutajanimi: ".$user['username']."									\n".
				"	Parool: ".$newPassword."											\n".
				"																		\n".
				"Lugupidamisega,														\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"-------------------------------										\n".
				"																		\n".

				"Уважаемый пользователь,												\n".
				"																		\n".
				"Ваша пользовательская информация на аукционе ".$setts['sitename'].":	\n".
				"																		\n".
				"	Имя пользователя: ".$user['username']."								\n".
				"	Пароль: ".$newPassword."											\n".
				"																		\n".
				"С уважением,															\n".
				"Администрация интернет-аукциона ".$setts['sitename']."					\n".
				
				"																		\n".
				"-------------------------------										\n".
				"																		\n".
				
				"Dear Subscriber,														\n".
				"																		\n".
				"Your login information to ".$setts['sitename']." is:					\n".
				"																		\n".
				"	Username: ".$user['username']."										\n".
				"	Password: ".$newPassword."											\n".
				"																		\n".
				"Best Regards,															\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea kasutaja,															<br>".
				"																		<br>".
				"Teie sisselogimise info oksjonil ".$setts['sitename']." on:			<br>".
				"																		<br>".
				"	Kasutajanimi: ".$user['username']."									<br>".
				"	Parool: ".$newPassword."											<br>".
				"																		<br>".
				"Lugupidamisega,														<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"----------------------------											<br>".
				"																		<br>".

				"Уважаемый пользователь,												<br>".
				"																		<br>".
				"Ваша пользовательская информация на аукционе ".$setts['sitename'].":	<br>".
				"																		<br>".
				"	Имя пользователя: ".$user['username']."								<br>".
				"	Пароль: ".$newPassword."											<br>".
				"																		<br>".
				"С уважением,															<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." Staff			<br>".
				
				"																		<br>".
				"----------------------------											<br>".
				"																		<br>".
				
				"Dear Subscriber,														<br>".
				"																		<br>".
				"Your login information to ".$setts['sitename']." is:					<br>".
				"																		<br>".
				"	Username: ".$user['username']."										<br>".
				"	Password: ".$newPassword."											<br>".
				"																		<br>".
				"Best Regards,															<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($user['email'],$setts['sitename']." Login Information",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>