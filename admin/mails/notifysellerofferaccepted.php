<?php
## v5.24 -> apr. 05, 2006
$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($seller['mail_auctionsold']==1) ? TRUE : FALSE;
$plainMessage ="Dear ".$seller['name']."																				\n".
					"																												\n".
					"Your item, ".$auction['itemname']." has been purchased by a buyer using the offer feature.\n".
					"																												\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\n".
					"																												\n".
					"To conclude this sale please login to the member�s area - selling and visit the 	\n".
					"\"Item�s I've Sold in detail\" section.  From here click on the \"Message Board\" 	\n".
					"link next to each item sold.																			\n".
					"																												\n".
					"This message board is your direct communication board with your buyer.					\n".
					"																												\n".
					"Please use this board to answer any questions the buyer may have regarding payment and delivery.\n".
					"																												\n".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.\n".
					"																												\n".
					"Thank you,																	\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage =	"Dear ".$seller['name']."																				<br>".
					"																												<br>".
					"Your item, ".$auction['itemname']." has been purchased by a buyer using the offer feature.<br>".
					"																												<br>".
					"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
					"																												<br>".
					"To conclude this sale please login to the member�s area - selling and visit the 	<br>".
					"\"Item�s I've Sold in detail\" section.  From here click on the \"Message Board\" 	<br>".
					"link next to each item sold.																			<br>".
					"																												<br>".
					"This message board is your direct communication board with your buyer.					<br>".
					"																												<br>".
					"Please use this board to answer any questions the buyer may have regarding payment and delivery.<br>".
					"																												<br>".
					"Important:- To help resolve any possible disputes ensure you use the board for all queries and updates.<br>".
					"																												<br>".
					"Thank you,																	<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"Auction Won with Swap feature.",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>