<?php
## v5.24 -> apr. 05, 2006
## commented because of the new message board feature
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");
$winner = getSqlRow ("SELECT * FROM probid_winners WHERE id='".$winnerId."'");

##$sendMail = ($buyer['mail_sellerdetails']==1);

$sendMail = ($buyer['mail_sellerdetails']==1) ? TRUE : FALSE;

if ($setts['enable_display_phone']=="N") $seller['phone'] = "Undisclosed";

$plainMessage ="Dear ".$buyer['name'].",																\n".
					"																								\n".
					"You have won the following item through ".$setts['sitename'].".			\n".
					"																								\n".
					"Auction # ".$auctionId."																\n".
					"Name:	".$auction['itemname']."													\n".
					"Price:	".displayAmount($winner['amount'],$auction['currency'])."		\n".
					"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
					"																								\n".
					"Seller Contact Details:																\n".
					"																								\n".
					"Name: ".$seller['name']."																\n".
					"Address: ".$seller['address'].", ".$seller['city'].", ".$seller['state'].", ".$seller['zip'].", ".$seller['country']."											\n".
					"																								\n".
					"Phone: ".$seller['phone']."															\n".
					"Email Address: ".$seller['email']."												\n".
					"																								\n".
					"You can also contact the seller in the message board. Follow the link  \n".
					$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId." 	\n".
					"																								\n".
					"Postage Details:																			\n".
					"																								\n".
					"Postage Costs: ".$auction['postage_costs']."									\n".
					"Insurance: ".$seller['insurance']."												\n".
					"Type of Service: ".$seller['type_service']."									\n".
					"																								\n".
					"Thank you,																					\n".
					"The ".$setts['sitename']." Staff";

$htmlMessage =	"Dear ".$buyer['name'].",																<br>".
					"																								<br>".
					"You have won the following item through <b>".$setts['sitename']."</b>.	<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Auction # </td>												\n".
					"		<td>".$htmlfont.$auctionId."</td>											\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$auction['itemname']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Price:</td>													\n".
					"		<td>".$htmlfont.displayAmount($winner['amount'],$auction['currency'])."</td>\n".
					"	</tr>																						\n";
					
if (!empty($auction['picpath'])) {
	$htmlMessage.=	"	<tr>																				\n".
						"		<td>".$htmlfont."Auction Image:</td>								\n".
						"		<td><img src=\"".$path."makethumb.php?pic=".$path.$auction['picpath']."&w=250&sq=Y\" border=\"1\"></td>\n".
						"	</tr>																				\n";
}

$htmlMessage.=	"	<tr>																						\n".
					"		<td>".$htmlfont."Auction URL:</td>											\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Seller's Contact Details:</b>													<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Name:</td>													\n".
					"		<td>".$htmlfont.$seller['name']."</td>										\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Address:</td>												\n".
					"		<td>".$htmlfont.$seller['address'].", ".$seller['city'].", ".$seller['state'].", ".$seller['zip'].", ".$seller['country']."</td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Phone: </td>													\n".
					"		<td>".$htmlfont.$seller['phone']."</td>									\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Email Address: </td>										\n".
					"		<td>".$htmlfont."<a href=\"mailto:".$seller['email']."\">".$seller['email']."</a></td>\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Follow the link on the right to contact the seller in the message board</td>\n".
					"		<td>".$htmlfont."<a href=\"".$setts['siteurl']."login.php?redirect=msgboard&winnerid=".$winnerId."\">Click Here</a></td>\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"<b>Postage Details:</b>																<br>".
					"																								<br>".
					"<table border=\"0\">																	\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Postage Costs: </td>										\n".
					"		<td>".$htmlfont.$auction['postage_costs']."</td>						\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Insurance: </td>											\n".
					"		<td>".$htmlfont.$auction['insurance']."</td>								\n".
					"	</tr>																						\n".
					"	<tr>																						\n".
					"		<td>".$htmlfont."Type of Service: </td>									\n".
					"		<td>".$htmlfont.$auction['type_service']."</td>							\n".
					"	</tr>																						\n".
					"</table>																					\n".
					"																								<br>".
					"Thank you,																					<br>".
					"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Auction Closed - Item Won!",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);
?>