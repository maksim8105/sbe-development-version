<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$bidder = getSqlRow ("SELECT * FROM probid_users WHERE id='".$bidderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($bidder['mail_outbid']==1) ? TRUE : FALSE;
$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$bidder['name'].",												\n".
				"																		\n".
				"Teie tehtud pakkumine on oksjonil ületatud.							\n".
				"																		\n".
				"Oksjoni URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Uue pakkumise tegemiseks kasutage oksjoni kirjelduse lehel 			\n". 
				"allpool olevat vormi.													\n".
				"																		\n".
				"Täname,																\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"-------------------------------------------------------				\n".
				"																		\n".

				"Здравствуйте, ".$bidder['name'].",												\n".
				"																		\n".
				"Ваша ставка на аукционе перебита.										\n".
				"																		\n".
				"URL аукциона: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Вы можете сделать новую ставку, используя форму в нижней части страницы	\n". 
				"с описанием аукциона.													\n".
				"																		\n".
				"Спасибо,																\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".
				
				"																		\n".
				"-------------------------------------------------------				\n".
				"																		\n".
				
				"Dear ".$bidder['name'].",												\n".
				"																		\n".
				"You have been outbid on an auction you placed a bid on.				\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"You can place another bid using the bid placement form from the bottom	\n". 
				"of the auction details page.											\n".
				"																		\n".
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$bidder['name'].",												<br>".
				"																		<br>".
				"Teie tehtud pakkumine on oksjonil ületatud.							<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Oksjoni vaatamiseks klikkige siia</a><br>".
				"																		<br>".
				"Uue pakkumise tegemiseks kasutage oksjoni kirjelduse lehel				<br>". 
				"allpool olevat vormi.													<br>".
				"																		<br>".
				"Täname,																<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"---------------------------------------------------					<br>".
				"																		<br>".

				"Здравствуйте, ".$bidder['name'].",										<br>".
				"																		<br>".
				"Ваша ставка на аукционе перебита.										<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Для просмотра аукциона нажмите сюда</a><br>".
				"																		<br>".
				"Вы можете сделать новую ставку, используя форму в нижней части страницы	<br>". 
				"с описанием аукциона.													<br>".
				"																		<br>".
				"Спасибо,																<br>".
				"Администрация интернет-аукциона ".$setts['sitename']." 				<br>".
				
				"																		<br>".
				"---------------------------------------------------					<br>".
				"																		<br>".
				
				"Dear ".$bidder['name'].",												<br>".
				"																		<br>".
				"You have been outbid on an auction you placed a bid on.				<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction</a><br>".
				"																		<br>".
				"You can place another bid using the bid placement form from the bottom	<br>". 
				"of the auction details page.											<br>".
				"																		<br>".
				"Thank you for your interest,											<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($bidder['email'],$setts['sitename']." - Outbid Notice",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>