<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$buyer = getSqlRow ("SELECT * FROM probid_users WHERE id='".$buyerId."'");
$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "Dear ".$buyer['name']."															\n".
				"																					\n".
				"You swap offer for ".$auction['itemname']." was rejected by the seller.			\n".
				"																					\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Thank you,																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$buyer['name']."															<br>".
				"																					<br>".
				"You swap offer for ".$auction['itemname']." was rejected by the seller.			<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
				"																					<br>".
				"Thank you,																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($buyer['email'],"Swap Offer Rejected",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>