<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$recipientId."'");

$plainMessage = "Dear ".$userDetails['name'].",											\n"
				."Thank you for your registration to ".$setts['sitename'].".			\n"
				."																		\n"
				."Your login information is as follows:									\n"
				."																		\n"
				."Username: ".$userDetails['username']."								\n"
				."Password: ".$_POST['password']."										\n"
				."																		\n"
				."The URL for the members area login is:								\n"
				."																		\n"
				.$setts['siteurl']."login.php											\n"
				."																		\n"
				."Best Regards,															\n"
				."The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$userDetails['name'].",											<br>"
				."Thank you for your registration to ".$setts['sitename'].".			<br>"
				."																		<br>"
				."Your login information is as follows:									<br>"
				."																		<br>"
				."Username: ".$userDetails['username']."								<br>"
				."Password: ".$_POST['password']."										<br>"
				."																		<br>"
				."The URL for the members area login is:								<br>"
				."																		<br>"
				."<a href=\"".$setts['siteurl']."login.php\">".$setts['siteurl']."login.php</a><br>"
				."																		<br>"
				."Best Regards,															<br>"
				."The ".$setts['sitename']." Staff";
htmlmail($userDetails['email'],$setts['sitename']." Login Information",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>