<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$getUser = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE id='".$userId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
$user = mysqli_fetch_array($getUser);

$sendMail = ($user['mail_accsusp']==1) ? TRUE : FALSE;
$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$user['name'].",													\n".
				"																		\n".
				"Teie kasutajakonto lehel ".$setts['sitename']." on deaktiveeritud, kuna		\n".
				"Teie krediit on ületanud suurema lubatud piiri.						\n".
				"																		\n".
				"Oma kasutajakonto taasaktiveerimiseks logige palun sisse:				\n".
				"																		\n".
				$setts['siteurl']."login.php											\n".
				"																		\n".
				"ja värskendage oma bilanssi.											\n".
				"																		\n".
				"Parimate soovidega,													\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"-------------------------------------------------------------------	\n".
				"																		\n".
				
				"Здравствуйте, ".$user['name'].",										\n".
				"																		\n".
				"Ваш аккаунт на аукционе ".$setts['sitename']." деактивирован, поскольку		\n".
				"Ваш кредит превысил наибольшую допустимую сумму.						\n".
				"																		\n".
				"Для активации своего аккаунта пройдите по ссылке:						\n".
				"																		\n".
				$setts['siteurl']."login.php											\n".
				"																		\n".
				"и пополните свой баланс.												\n".
				"																		\n".
				"С наилучшими пожеланиями,												\n".
				"Администрация интернет-аукциона ".$setts['sitename']." 				\n".
				
				"																		\n".
				"-------------------------------------------------------------------	\n".
				"																		\n".
				
				"Dear ".$user['name'].",												\n".
				"																		\n".
				"Your account on ".$setts['sitename']." was suspended, because your		\n".
				"account has gone over the maximum debit limit permitted by our site.	\n".
				"																		\n".
				"In order to activate your account, please log in to our site at:		\n".
				"																		\n".
				$setts['siteurl']."login.php											\n".
				"																		\n".
				"and clear your account balance.										\n".
				"																		\n".
				"Warm regards,															\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"NB! Messange encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$user['name'].",													<br>".
				"																		<br>".
				"Teie kasutajakonto lehel ".$setts['sitename']." on deaktiveeritud, kuna		<br>".
				"Teie krediit on ületanud suurema lubatud piiri.						<br>".
				"																		<br>".
				"Oma kasutajakonto taasaktiveerimiseks logige palun sisse:				<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl']."login.php\">Sisselogimiseks klikkige siia</a><br>".
				"																		<br>".
				"ja värskendage oma bilanssi.											<br>".
				"																		<br>".
				"Parimate soovidega,													<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"-------------------------------------------------------------------	<br>".
				"																		<br>".

				"Здравствуйте, ".$user['name'].",												<br>".
				"																		<br>".
				"Ваш аккаунт на аукционе ".$setts['sitename']." деактивирован, поскольку		<br>".
				"Ваш кредит превысил наибольшую допустимую сумму.						<br>".
				"																		<br>".
				"Для активации своего аккаунта пройдите по ссылке:						<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl']."login.php\">Нажмите сюда для входа</a><br>".
				"																		<br>".
				"и пополните свой баланс.												<br>".
				"																		<br>".
				"С наилучшими пожеланиями,												<br>".
				"Администрация интернет-аукциона ".$setts['sitename']."					<br>".
				
				"																		<br>".
				"-------------------------------------------------------------------	<br>".
				"																		<br>".
				
				"Dear ".$user['name'].",												<br>".
				"																		<br>".
				"Your account on ".$setts['sitename']." was suspended, because your		<br>".
				"account has gone over the maximum debit limit permitted by our site.	<br>".
				"																		<br>".
				"In order to activate your account, please click on the link below:		<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl']."login.php\">Click for the login page</a><br>".
				"																		<br>".
				"and clear your account balance.										<br>".
				"																		<br>".
				"Warm regards,															<br>".
				"The ".$setts['sitename']." Staff";
htmlmail($user['email'],$setts['sitename']." - Account Suspended",
$plainMessage,$setts['adminemail'],$htmlMessage, $sendMail);
?>