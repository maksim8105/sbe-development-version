<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage ="A new auction has been created/modified and needs admin approval.\n"
					."																		\n"
					."Please login to your site's admin area / auction approval page in order to review/approve the auction.\n"
					."																		\n"
					."Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\n";

$htmlMessage = "A new auction has been created/modified and needs admin approval.<br>"
					."																		<br>"
					."Please login to your site's admin area / auction approval page in order to review/approve the auction.<br>"
					."																		<br>"
					.$htmlfont."Auction URL: <a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."</a><br>";
				
htmlmail($setts['adminemail'],"Auction Approval - #".$auction['id'],$plainMessage,
$setts['adminemail'],$htmlMessage);
?>