<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "Dear ".$user['name']."													\n".
				"																		\n".
				"This is an invoice for clearing your account balance with our site,	\n".
				"".$setts['sitename']."													\n".
				"																		\n".
				"Your balance is: ".$setts['currency']." ".$user['balance']."			\n".
				"																		\n".
				"Please follow the link below to proceed to the payment gateway:		\n".
				"																		\n".
				"URL: ".$setts['siteurl']."invoice.php?id=".$user['id']."				\n".
				"																		\n".
				"Thank you in advance,													\n".
				"The ".$setts['sitename']." staff";
$htmlMessage = 	"Dear ".$user['name']."													<br>".
				"																		<br>".
				"This is an invoice for clearing your account balance with our site,	<br>".
				"".$setts['sitename']."													<br>".
				"																		<br>".
				"Your balance is: <strong>".$setts['currency']." ".$user['balance']."</strong><br>".
				"																		<br>".
				"Please <a href=\"".$setts['siteurl']."invoice.php?id=".$user['id']."\">click here</a> to proceed to the payment gateway.<br>".
				"																		<br>".
				"Thank you in advance,													<br>".
				"The ".$setts['sitename']." staff";

htmlmail($user['email'],"$setts[sitename] Invoice",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>