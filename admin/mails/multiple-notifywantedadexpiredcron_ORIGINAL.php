<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "Dear ".$name.",														\n".
				"																		\n".
				"Several wanted ads that you have placed on ".$setts['sitename']." have expired.\n".
				"																		\n".
				"To get more details about the wanted ads that have expired, please access\n".
				"																		\n".
				"".$setts['siteurl']."membersarea.php									\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"Dear ".$name.",														<br>".
				"																		<br>".
				"Several wanted ads that you have placed on ".$setts['sitename']." have expired.<br>.".
				"																		<br>".
				"To get more details about the wanted ad that have expired, please access<br>".
				"																		<br>".
				"[ <a href=\"".$setts['siteurl']."membersarea.php\">Members area</a> ]<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Multiple Wanted Ads Expired",$plainMessage,
$setts['adminemail'],$htmlMessage);

?>