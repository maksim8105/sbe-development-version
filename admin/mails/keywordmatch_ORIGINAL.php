<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$user = getSqlRow ("SELECT * FROM probid_users WHERE id='".$userId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$sendMail = ($user['mail_keywordmatch']==1) ? TRUE : FALSE;
$plainMessage = "Dear ".$user['name'].",												\n".
				"																		\n".
				"An auction matching one of your keywords, \"".$keyword."\", was entered into our database.\n".
				"																		\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."	\n".
				"																		\n".
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." staff";

$htmlMessage = 	"Dear ".$user['name'].",												<br>".
				"																		<br>".
				"An auction matching one of your keywords, \"".$keyword."\", was entered into our database.<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click to view the auction</a><br>".
				"																		<br>".
				"Thank you for your interest,											\n".
				"The ".$setts['sitename']." staff";

htmlmail($user['email'],"Keyword Match",
$plainMessage,$setts['adminemail'],$htmlMessage,$sendMail);
?>