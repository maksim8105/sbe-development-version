<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$recipientId."'");

$plainMessage = "A new registration was made on ".$setts['sitename'].".					\n"
				."																		\n"
				."User Details:															\n"
				."																		\n"
				."Username: ".$userDetails['username']."								\n"
				."User ID: ".$recipientId."												\n"
				."																		\n"
				."You will have to activate the account from the admin area, 			\n"
				."users management page.												\n"
				."																		\n"
				."Best Regards,															\n"
				."The ".$setts['sitename']." Staff";

$htmlMessage = 	"A new registration was made on ".$setts['sitename'].".					<br>"
				."																		<br>"
				."User Details:															<br>"
				."																		<br>"
				."Username: ".$userDetails['username']."								<br>"
				."User ID: ".$recipientId."												<br>"
				."																		<br>"
				."You will have to activate the account from the admin area, 			<br>"
				."users management page.												<br>"
				."																		<br>"
				."Best Regards,															<br>"
				."The ".$setts['sitename']." Staff";
				
htmlmail($setts['adminemail'],"New Registration",$plainMessage,
$setts['adminemail'],$htmlMessage);
?>