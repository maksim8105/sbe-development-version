<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$getUser = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE id='".$userId."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
$user = mysqli_fetch_array($getUser);

$sendMail = ($user['mail_accsusp']==1) ? TRUE : FALSE;
$plainMessage = "Dear ".$user['name'].",												\n".
				"																		\n".
				"Your account on ".$setts['sitename']." was suspended, because your		\n".
				"account has gone over the maximum debit limit permitted by our site.	\n".
				"																		\n".
				"In order to activate your account, please log in to our site at:		\n".
				"																		\n".
				$setts['siteurl']."login.php											\n".
				"																		\n".
				"and clear your account balance.										\n".
				"																		\n".
				"Warm regards,															\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$user['name'].",												<br>".
				"																		<br>".
				"Your account on ".$setts['sitename']." was suspended, because your		<br>".
				"account has gone over the maximum debit limit permitted by our site.	<br>".
				"																		<br>".
				"In order to activate your account, please click on the link below:		<br>".
				"																		<br>".
				"<a href=\"".$setts['siteurl']."login.php\">Click for the login page</a><br>".
				"																		<br>".
				"and clear your account balance.										<br>".
				"																		<br>".
				"Warm regards,															<br>".
				"The ".$setts['sitename']." Staff";
htmlmail($user['email'],$setts['sitename']." - Account Suspended",
$plainMessage,$setts['adminemail'],$htmlMessage, $sendMail);
?>