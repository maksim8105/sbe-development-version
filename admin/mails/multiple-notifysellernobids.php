<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$sendMail = ($seller['mail_auctionclosed']==1) ? TRUE : FALSE;
$plainMessage = "NB! Message encoding: UTF-8											\n".
				"																		\n".
				"Hea ".$name.",															\n".
				"																		\n".
				"Mitu Teie oksjonit on lõppenud ilma võitjata. Ei olnud pakkumisi või reservhind (kui see oli määratud) ei olnud saavutatud.\n".
				"																		\n".
				"Täname,																\n".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				\n".
				
				"																		\n".
				"--------------------------------------------------------				\n".
				"																		\n".

				"Здравствуйте, ".$name.",												\n".
				"																		\n".
				"Несколько Ваших аукционов завершились без победителя. Либо не поступило ставок, либо не была достигнута стоп-цена (если она была указана).\n".
				"																		\n".
				"Благодарим,															\n".
				"Администрация интернет-аукциона ".$setts['sitename']."					\n".
				
				"																		\n".
				"--------------------------------------------------------				\n".
				"																		\n".

				"Dear ".$name.",														\n".
				"																		\n".
				"Several of your auctions have closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.\n".
				"																		\n".
				"Thank you,																\n".
				"The ".$setts['sitename']." Staff";
				
$htmlMessage = 	"NB! Message encoding: UTF-8											<br>".
				"																		<br>".
				"Hea ".$name.",															<br>".
				"																		<br>".
				"Mitu Teie oksjonit on lõppenud ilma võitjata. Ei olnud pakkumisi või reservhind (kui see oli määratud) ei olnud saavutatud.	<br>".
				"																		<br>".
				"Täname,																<br>".
				"Virtuaalse oksjoni ".$setts['sitename']." administratsioon				<br>".
				
				"																		<br>".
				"-------------------------------------------------------				<br>".
				"																		<br>".

				"Здравствуйте, ".$name.",												<br>".
				"																		<br>".
				"Несколько Ваших аукционов завершились без победителя. Либо не поступило ставок, либо не была достигнута стоп-цена (если она была указана).	<br>".
				"																		<br>".
				"Благодарим,															<br>".
				"Администрация интернет-аукциона ".$setts['sitename']."					<br>".
				
				"																		<br>".
				"-------------------------------------------------------				<br>".
				"																		<br>".

				"Dear ".$name.",														<br>".
				"																		<br>".
				"Several of your auctions have closed without a winner. This is either because there were no bids, or if you had a reserve price, the reserve was not met.	<br>".
				"																		<br>".
				"Thank you,																<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($email,"Multiple Auctions Closed - Items Not Sold",$plainMessage,
$setts['adminemail'],$htmlMessage,$sendMail);

?>