### this is the function for the auction setup fee
function setupFee($startPrice,$currency,$auctionId,$isHp,$isCat,$isBold,$isHl,$isReserve,$picCount,$isSecondcat,$isBin,$editAuction=FALSE,$showOnly=FALSE,$voucherCode="",$providedCat=0, $displayMsgs = TRUE) {
	global $path, $setts, $lang, $fee;

	## v5.22 addon -> if the currency is different from the default one, then apply the converter.
	$exchange_rate = 1;
	if (trim($setts['currency'])!=trim($currency)) {
		$converter = getSqlField("SELECT converter FROM probid_currencies WHERE symbol = '".trim($currency)."'","converter");
		$converter = ($converter == 0) ? 1 : $converter;
		$exchange_rate = 1/$converter;
	}
	if($exchange_rate<=0) $exchange_rate = 1;

	##feeCategory -> its selected either from the providedCat field (sellitem-step12) or from the auction's main category
	if ($providedCat == 0) $category_id = getSqlField("SELECT category FROM probid_auctions WHERE id='".$auctionId."'","category");
	else $category_id = $providedCat;
	
	$category_id = getMainCat($category_id);
		
	$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
	$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
	
	$actItemA = getSqlField("SELECT active FROM probid_auctions WHERE id='".$auctionId."'","active");
	$userid = getSqlField("SELECT * FROM probid_auctions WHERE id='".$auctionId."'","ownerid");
	if (!$auctionId) $userid = $_SESSION['memberid'];
	#### check if the user is a preferred seller
	$prefSeller = "N";
	if ($setts['pref_sellers']=="Y") {
		$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
		WHERE id='".$userid."'","preferred_seller");
	}
	
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userid."'","payment_mode");
    if ($setts['account_mode_personal']==1) {
    	$account_mode_local = ($tmp) ? 2 : 1;
	} else $account_mode_local = $setts['account_mode'];
	
	#### calculate the amount that has to be paid for this auction's setup
	$amount=0;
	$currentBalance = getSqlField("SELECT balance FROM probid_users WHERE id='".$userid."'","balance");
	$newBalance = $currentBalance;
	if ($fee['is_setup_fee']=="Y"&&!$editAuction) {
		
		$listingFee = getSqlRow("SELECT * FROM probid_fees_tiers WHERE fee_from<=".$startPrice." AND fee_to>".$startPrice." AND fee_type='setup' AND category_id='".$category_id."'");
		if ($listingFee['calc_type']=="percent") {
			$amount+=applyVat(($listingFee['fee_amount']/100)*$startPrice*$exchange_rate,$userid);
			$invoiceAmount=applyVat(($listingFee['fee_amount']/100)*$startPrice*$exchange_rate,$userid);
		} else {
			$amount+=applyVat($listingFee['fee_amount'],$userid);
			$invoiceAmount=applyVat($listingFee['fee_amount'],$userid);
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"setup");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);
			
			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance) VALUES 
			('".$userid."','".$auctionId."','".remSpecialChars($lang[auc_setup_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['is_pic_fee']=="Y"&&$picCount>0) {
		if ($startPrice>$fee['piclimit']&&$fee['piclimit']!=0) {
			$amount+=applyVat($fee['val_pic_fee2'],$userid);
			$invoiceAmount=applyVat($fee['val_pic_fee2'],$userid);
		} else {
			$amount+=applyVat($fee['val_pic_fee'],$userid);
			$invoiceAmount=applyVat($fee['val_pic_fee'],$userid);
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"pic");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)	VALUES 
			('".$userid."','".$auctionId."','".remSpecialChars($lang[add_pic_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']!=""&&$isCat=="Y") {
		if ($fee['is_catfeat_percent']=="percent") {
			if ($startPrice>$fee['catfeatlimit']&&$fee['catfeatlimit']!=0) {
				$amount+=applyVat(($fee['val_catfeat_fee2']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_catfeat_fee2']/100)*$startPrice,$userid);
			} else {
				$amount+=applyVat(($fee['val_catfeat_fee']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_catfeat_fee']/100)*$startPrice,$userid);
			}
		} else {
			if ($startPrice>$fee['catfeatlimit']&&$fee['catfeatlimit']!=0) {
				$amount+=applyVat($fee['val_catfeat_fee2'],$userid);
				$invoiceAmount=applyVat($fee['val_catfeat_fee2'],$userid);
			} else {
				$amount+=applyVat($fee['val_catfeat_fee'],$userid);
				$invoiceAmount=applyVat($fee['val_catfeat_fee'],$userid);
			}
		}
		### if we are editing the auction, see if there are 2 cats and multiply the amount
		$secCat = getSqlField("SELECT addlcategory FROM probid_auctions WHERE id='".$auctionId."'","addlcategory");
		$featSecondcat = "N";
		if ($editAuction&&$secCat>0) $featSecondcat = "Y";
		
		if ($isSecondcat=="Y"||$featSecondcat=="Y") {
			$amount = $amount+$invoiceAmount;
			$invoiceAmount = $invoiceAmount*2;
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"catfeat");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[cat_page_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']!=""&&$isBold=="Y") {
		if ($fee['is_bolditem_percent']=="percent") {
			if ($startPrice>$fee['bolditemlimit']&&$fee['bolditemlimit']!=0) {
				$amount+=applyVat(($fee['val_bolditem_fee2']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_bolditem_fee2']/100)*$startPrice,$userid);
			} else {
				$amount+=applyVat(($fee['val_bolditem_fee']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_bolditem_fee']/100)*$startPrice,$userid);
			}
		} else {
			if ($startPrice>$fee['bolditemlimit']&&$fee['bolditemlimit']!=0) {
				$amount+=applyVat($fee['val_bolditem_fee2'],$userid);
				$invoiceAmount=applyVat($fee['val_bolditem_fee2'],$userid);
			} else { 
				$amount+=applyVat($fee['val_bolditem_fee'],$userid);
				$invoiceAmount=applyVat($fee['val_bolditem_fee'],$userid);
			}
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"bold");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[bold_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']!=""&&$isHl=="Y") {
		if ($fee['is_hlitem_percent']=="percent") {
			if ($startPrice>$fee['hlitemlimit']&&$fee['hlitemlimit']!=0) {
				$amount+=applyVat(($fee['val_hlitem_fee2']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_hlitem_fee2']/100)*$startPrice,$userid);
			}
			else { 
				$amount+=applyVat(($fee['val_hlitem_fee']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_hlitem_fee']/100)*$startPrice,$userid);
			}
		} else {
			if ($startPrice>$fee['hlitemlimit']&&$fee['hlitemlimit']!=0) {
				$amount+=applyVat($fee['val_hlitem_fee2'],$userid);
				$invoiceAmount=applyVat($fee['val_hlitem_fee2'],$userid);
			} else {
				$amount+=applyVat($fee['val_hlitem_fee'],$userid);
				$invoiceAmount=applyVat($fee['val_hlitem_fee'],$userid);
			}
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"hl");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[highl_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']!=""&&$isHp=="Y") {
		if ($fee['is_hpfeat_percent']=="percent") {
			if ($startPrice>$fee['hpfeatlimit']&&$fee['hpfeatlimit']!=0) {
				$amount+=applyVat(($fee['val_hpfeat_fee2']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_hpfeat_fee2']/100)*$startPrice,$userid);
			} else {
				$amount+=applyVat(($fee['val_hpfeat_fee']/100)*$startPrice,$userid);
				$invoiceAmount=applyVat(($fee['val_hpfeat_fee']/100)*$startPrice,$userid);
			}
		} else {
			if ($startPrice>$fee['hpfeatlimit']&&$fee['hpfeatlimit']!=0) {
				$amount+=applyVat($fee['val_hpfeat_fee2'],$userid);
				$invoiceAmount=applyVat($fee['val_hpfeat_fee2'],$userid);
			}
			else {
				$amount+=applyVat($fee['val_hpfeat_fee'],$userid);
				$invoiceAmount=applyVat($fee['val_hpfeat_fee'],$userid);
			}
		}

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"hpfeat");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[home_page_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']!=""&&$isReserve=="Y") {
		$amount+=applyVat($fee['val_rp_fee'] ,$userid);
		$invoiceAmount=applyVat($fee['val_rp_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"rp");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[res_price_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['second_cat_fee']>0&&$isSecondcat=="Y") {
		$amount+=applyVat($fee['second_cat_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['second_cat_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"seccat");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}

		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[second_cat_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}
	if ($fee['bin_fee']>0&&$isBin=="Y") {
		$amount+=applyVat($fee['bin_fee'],$userid) ;
		$invoiceAmount=applyVat($fee['bin_fee'],$userid);

		### if voucher is valid, calculate voucher reduction
		$voucher = checkSetupVoucher(trim($voucherCode),"bn");
		if ($voucher['valid']) {
			$voucher_reduction = $invoiceAmount * ($voucher['reduction']/100);
			$invoiceAmount = $invoiceAmount - $voucher_reduction;
			$amount = $amount - $voucher_reduction;
		}
	
		if ($account_mode_local==2&&!$showOnly) {
			### setts for the preferred sellers reduction
			$invoiceAmount = calcReduction ($invoiceAmount, $prefSeller);

			$newBalance += $invoiceAmount;
			$currentTime = time();
			$insertInvoice = mysql_query("INSERT INTO probid_invoices 
			(userid,auctionid,feename,feevalue,feedate,balance)
			VALUES ('".$userid."','".$auctionId."','".remSpecialChars($lang[bin_fee])."','".$invoiceAmount."','".$currentTime."','".$newBalance."')") or die(mysql_error());
		}
	}

	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	if ($amount==0) $setts['payment_gateway']="none";

	### setts for the preferred sellers reduction
	$amount = calcReduction ($amount, $prefSeller);

	if (!$showOnly) {
		if ($account_mode_local==1) {
			$paymentAmount=number_format($amount,2,'.','');
			### new function that displays the payment message and amount
			displayPaymentMessage($paymentAmount);
	
			### new procedure to list all active payment gateways
			if ($setts['payment_gateway']=="none") {
				if ($displayMsgs) echo "<p align=center class=contentfont>".$lang[auctactive]."<br><br>";
				$activateAuction = mysql_query("UPDATE probid_users SET 
				active = '1',payment_status='confirmed' WHERE id='".$userid."'") or die(mysql_error());
				## if there are no fees to pay, add counter
				$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$auctionId."'");
				if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
					addcatcount ($auctCat['category'],$auctionId);
					addcatcount ($auctCat['addlcategory'],$auctionId);
				}
			} else {
				$getPGs = mysql_query("SELECT * FROM probid_payment_gateways WHERE value='checked'");
				while ($selectedPGs = mysql_fetch_array($getPGs)) {
					if ($selectedPGs['name']=="Paypal") {
						$notifyUrl=$path."paymentprocess.php?table=2";
						paypalForm($auctionId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}	
					if ($selectedPGs['name']=="Nochex") {
						$notifyUrl=$path."nochexprocess.php?table=2";
						nochexForm($auctionId,$setts['paypalemail'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="2Checkout") {
						$notifyUrl=$path."checkoutprocess.php?table=2";
						checkoutForm($auctionId,$setts['checkoutid'],$paymentAmount,2);
					}
					if ($selectedPGs['name']=="Worldpay") {
						$notifyUrl=$path."worldpayprocess.php?table=2";
						worldpayForm($auctionId,$setts['worldpayid'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Ikobo") {
					 	$notifyUrl=$path."ikoboprocess.php?table=2";
						ikoboForm($auctionId,$setts['ikobombid'],$setts['ikoboipn'],$paymentAmount,$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Protx") {
						$notifyUrl=$path."protxprocess.php?table=2";
						protxForm($auctionId,$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}
					if ($selectedPGs['name']=="Authorize.Net") {
						$notifyUrl=$path."authorize.net.process.php?table=2";
						authorizeNetForm($auctionId,$paymentAmount,2);
					}
					if ($selectedPGs['name']=="Moneybookers") {
						$notifyUrl=$path."moneybookers.process.php?table=2";
						moneybookersForm($auctionId,$paymentAmount,2);
					}
					if ($selectedPGs['name']=="Test Mode") {
						$notifyUrl=$path."paymentsimulator.php?table=2";
						testmodeForm($auctionId,$setts['paypalemail'],$paymentAmount,$setts['currency'],$returnUrl,$failureUrl,$notifyUrl,2);
					}
				}
			}		
		} else if ($account_mode_local==2) {
			// account mode thing
			$userId = getSqlField("SELECT * FROM probid_auctions WHERE id='".$auctionId."'","ownerid");
			$balance = 0;
			$balance+=$amount;
			
			$updateBalance = mysql_query("UPDATE probid_users SET 
			balance=balance+".$balance." WHERE id='".$userId."'");
			$updateAuction = mysql_query("UPDATE probid_auctions SET 
			active = '1',payment_status='confirmed' WHERE id='".$auctionId."'");
			## since site is in account mode, add counter
			$auctCat = getSqlRow("SELECT category, addlcategory, closed, active, deleted FROM probid_auctions WHERE id='".$auctionId."'");
			if ($auctCat['closed']==0&&$auctCat['active']==1&&$auctCat['deleted']!=1) {
				addcatcount ($auctCat['category'],$auctionId);
				addcatcount ($auctCat['addlcategory'],$auctionId);
			}

			if ($displayMsgs) echo "<p align=center class=contentfont>".$lang[aucsubmitted1]." #".$auctionId." ".$lang[auctionactivated2]." ".displayAmount($amount,$setts['currency'], TRUE)." ".$lang[actfeeof1]."<br><br></p>";
		}	
	}
	return $amount;
}
