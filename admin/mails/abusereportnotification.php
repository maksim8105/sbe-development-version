<?
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$plainMessage = "A new abuse report was posted by '".$_SESSION['memberusern']."'		\n".
					 "																							\n".
					 "Please check the Admin Area -> User Management -> View Abuse Reports page for more details.".

$htmlMessage =  "A new abuse report was posted by '".$_SESSION['memberusern']."'		<br>".
					 "																							<br>".
					 "Please check the <strong>Admin Area -> User Management -> View Abuse Reports</strong> page for more details.".

htmlmail($setts['adminemail'],"New abuse report posted",
$plainMessage,$setts['adminemail'],$htmlMessage);
?>