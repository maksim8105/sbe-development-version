<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$seller = getSqlRow ("SELECT * FROM probid_users WHERE id='".$sellerId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "Dear ".$seller['name']."															\n".
				"																					\n".
				"A new public question was posted regarding your item, ".$auction['itemname']."		\n".
				"																					\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."				\n".
				"																					\n".
				"Thank you,																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Dear ".$seller['name']."															<br>".
				"																					<br>".
				"A new public question was posted regarding your item, ".$auction['itemname']."		<br>".
				"																					<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click here to view the auction details page.</a><br>".
				"																					<br>".
				"Thank you,																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($seller['email'],"New Public Question Posted on Item ID #".$auctionId,
$plainMessage,$setts['adminemail'],$htmlMessage);
?>