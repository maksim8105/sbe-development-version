<?php
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

$owner = getSqlRow ("SELECT * FROM probid_users WHERE id='".$ownerId."'");
$sender = getSqlRow ("SELECT * FROM probid_users WHERE id='".$senderId."'");
$auction = getSqlRow ("SELECT * FROM probid_auctions WHERE id='".$auctionId."'");

$plainMessage = "Hello ".$_POST['friendname'].",											\n".
				"																			\n".
				"Your friend, ".$sender['name'].", has sent you this auction for you to look at:\n".
				"																			\n".
				"Auction URL: ".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."		\n".
				"																			\n".
				"Additional comments: ".$_POST['comments']."								\n".
				"Best regards,																\n".
				"																			\n".
				"The ".$setts['sitename']." Staff";

$htmlMessage = 	"Hello ".$_POST['friendname'].",											<br>".
				"																			<br>".
				"Your friend, ".$sender['name'].", has sent you this auction for you to look at:<br>".
				"																			<br>".
				"<a href=\"".$setts['siteurl'].processLink('auctiondetails', array('itemname' => $auction['itemname'], 'id' => $auction['id']))."\">Click to view the auction</a><br>".
				"																			<br>".
				"Additional comments: ".$_POST['comments']."								<br>".
				"Best regards,																<br>".
				"																			<br>".
				"The ".$setts['sitename']." Staff";

htmlmail($_POST['friendemail'],"Check out this Auction",
$plainMessage,$sender['email'],$htmlMessage);
?>