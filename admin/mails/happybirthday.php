<?php
## v5.22 Mod -> For use with the Birthday Mod by Binarywebs

$userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$recipientId."'");
$prefDetails = getSqlRow("SELECT * FROM probid_gen_setts");
$discount = substr($prefDetails['pref_sellers_reduction'],0,3);
$oursubject = "Happy Birthday from ".$setts['sitename']."!";
$plainMessage = "";

$htmlMessage = "NB! Message encoding: UTF-8<BR><BR>

		Hea ".$userDetails['name'].", <BR><BR>
		Meie andmete järgi on Teil täna sünnipäev. <BR><BR><b>Palju õnne!</b><BR><BR>
		Sünnipäeva puhul anname tänaseks Teile kuldkliendi staatuse,<BR>
		millega kaasneb ".$discount."% allahindlus meie süsteemi hinnakirjast!<BR><BR>
		Ja veel kord palju õnne!<BR><BR>
		Virtuaalse oksjoni ".$setts['sitename']." meeskond<BR<BR>
		
		------------------------------------- <BR><BR>

		Здравствуйте, ".$userDetails['name'].", <BR><BR>
		Согласно нашим данным, у Вас сегодня день рождения. <BR><BR><b>Поздравляем Вас!</b><BR><BR>
		По случаю праздника мы даем Вам на сегодня статус элитного клиента,<BR>
		с которым Вы имеете скидку ".$discount."% от нашего прейскуранта!<BR><BR>
		Еще раз с днем рождения!<BR><BR>
		Администрация интернет-аукциона ".$setts['sitename']."<BR<BR>

		-------------------------------------<BR><BR>

		Dear ".$userDetails['name'].", <BR><BR>
		Our records show that today is your birthday. <BR><BR><b>Happy Birthday!</b><BR><BR>
		To honor you on this special occassion, we have given you a Preferred Sellers status<BR>
		which will allow you to save ".$discount."% off all our site fees today!<BR><BR>
		Once again, Happy Birthday!<BR><BR>
		The ".$setts['sitename']." Staff<BR<BR>";

htmlmail($userDetails['email'],$oursubject,$plainMessage,$setts['adminemail'],$htmlMessage);
?>
