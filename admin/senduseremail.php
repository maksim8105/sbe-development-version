<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if (isset($_POST['sendnlok'])) {
	$newsletterContent=remSpecialChars($_POST['content']);
	if ($_POST['subject']!=""&&$_POST['content']!="") {
		$savedSettings="yes";
		
		htmlmail($_POST['email'],$_POST['subject'],'This message was sent in HTML',
		$setts['adminemail'],addSpecialChars($newsletterContent));
	} else {
		$savedSettings="no";
	}
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_user.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[USER_NEWS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? 
echo ($savedSettings=="yes")?"<p align=\"center\">Email successfully sent.</p>":""; 
echo ($savedSettings=="no")?"<p align=\"center\">Error: The subject and body fields cannot be empty.</p>":""; 
?> 
<form action="senduseremail.php" method="post"> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <input type="hidden" name="email" value="<?=$_REQUEST['email'];?>">
  <input type="hidden" name="username" value="<?=$_REQUEST['username'];?>">
    <tr> 
      <td colspan="2" align="center" class="c3"><b> 
        <?=$a_lang[TITLE_INDIV_SEND];?> 
        </b></td> 
    </tr> 
    <tr class="c1"> 
      <td width="150"><?=$a_lang[SEND_EMAIL_TO];?>  
        :</td> 
      <td><strong><?=$a_lang[USERNAME];?></strong> : <?=$_REQUEST['username'];?><br>
      <strong><?=$a_lang[EMAIL];?></strong> : <?=$_REQUEST['email'];?></td>
    </tr> 
    <tr class="c2"> 
      <td><?=$a_lang[SUBJECT];?> 
        :</td> 
      <td><input name="subject" type="text" id="subject" value="<?=$_REQUEST['subject'];?>"></td> 
    </tr> 
    <tr class="c1"> 
      <td><?=$a_lang[ENTER_CONTENT];?> 
        :</td> 
      <td><textarea name="content" cols="45" rows="10" id="content"><?=addSpecialChars($newsletterContent);?></textarea> 
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script><br> 
        <?=$a_lang[HTML_ALLOW];?></td> 
    </tr> 
    <tr> 
      <td colspan="2" align="center" class="c3"><input name="sendnlok" type="submit" id="sendnlok" value="<?=$a_lang[BTN_SEND_EMAIL];?>"></td> 
    </tr> 
</table> 
</form> 
<? 	include ("footer.php"); 
} ?>