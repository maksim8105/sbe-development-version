<?
## v5.24 -> may. 02, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include_once ('../config/lang/'.$_SESSION['sess_lang'].'/site.lang');

if ($_GET['option']=="delete") {
	$deleteFeedback = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_feedbacks WHERE 
	id='".$_GET['id']."'");
}

if (isset($_GET['savesettsok'])) {
	for ($i=0; $i<count($_GET['id']); $i++) {
		(string) $addlQuery = NULL;
		if (!$_GET['submitted'][$i] && !empty($_GET['fbcontent'][$i])) $addlQuery = ", submitted=1, date='".$timeNowMysql."' ";
	
		$updateFb[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_feedbacks SET rate='".$_GET['fbrate'][$i]."', feedback='".remSpecialChars($_GET['fbcontent'][$i])."' ".$addlQuery." WHERE id='".$_GET['id'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
}

include ("header.php"); 

$isCRF = getSqlNumber("SELECT id FROM probid_custom_rep WHERE active=1"); ?>
<script language="javascript">
function popUp(URL) {
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=2,location=0,statusbar=1,menubar=0,resizable=0,width=650,height=525,left = 100,top = 134');");
}
</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[USER_FEEDBACK];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table border="0" cellpadding="4" cellspacing="2" class="border" align="center">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <tr class="c3">
         <td colspan="3"><strong>
            <?=$a_lang[FB_SEARCH];?>
         </strong></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FB_FROM_USER];?>
            :</td>
         <td colspan="2"><input name="keywords_from_username" type="text" id="keywords_from_username" value="<?=$_REQUEST['keywords_from_username'];?>"></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FB_TO_USER];?>
            :</td>
         <td colspan="2"><input name="keywords_to_username" type="text" id="keywords_to_username" value="<?=$_REQUEST['keywords_to_username'];?>"></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FB_BY_RATING];?>
            :</td>
         <td><select name="keywords_rating">
               <option value="" selected="selected">-- Any --</option>
               <option value="5" <? echo ($_REQUEST['keywords_rating']==5) ? "selected" : ""; ?>  style="color:#009933; ">5 ticks</option>
               <option value="4" <? echo ($_REQUEST['keywords_rating']==4) ? "selected" : ""; ?>  style="color:#009933; ">4 ticks</option>
               <option value="3" <? echo ($_REQUEST['keywords_rating']==3) ? "selected" : ""; ?>  style="color:#666666; ">3 ticks</option>
               <option value="2" <? echo ($_REQUEST['keywords_rating']==2) ? "selected" : ""; ?>  style="color:#FF0000; ">2 ticks</option>
               <option value="1" <? echo ($_REQUEST['keywords_rating']==1) ? "selected" : ""; ?>  style="color:#FF0000; ">1 ticks</option>
            </select></td>
         <td><input name="usersearchok" type="submit" id="usersearchok" value="<?=$a_lang[BUTT_SEARCH];?>"></td>
      </tr>
   </form>
</table>
<br>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr class="c3">
      <td align="center"><b>
         <?=$a_lang[USER_FEEDBACKS_MANAGEMENT];?>
         </b></td>
   </tr>
   <?
	if ($_GET['start'] == "") $start = 0;
	else $start = $_GET['start'];
	$limit = 10;
	$orderField = "id";
	$orderType = "DESC";
  	## if a search is made, create the WHERE clause
	if ($_REQUEST['keywords_from_username']!="") {
		## get all user ids for which the username matches
		$getFromUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_users WHERE username LIKE '%".$_REQUEST['keywords_from_username']."%'");
		$keywords_fromid = "";
		while ($fromUsers = mysqli_fetch_array($getFromUsers)) {
			$keywords_fromid .= $fromUsers['id']." ";
		}
		$keywords_fromid = trim($keywords_fromid);
		$keywords_fromid = @eregi_replace(" ",",",$keywords_fromid);
		if (empty($keywords_fromid)) $keywords_fromid = 0;
		$searchPattern .= " AND (fromid IN (".$keywords_fromid."))";
	}
	if ($_REQUEST['keywords_to_username']!="") {
		## get all user ids for which the username matches
		$getToUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_users WHERE username LIKE '%".$_REQUEST['keywords_to_username']."%'");
		$keywords_toid = "";
		while ($toUsers = mysqli_fetch_array($getToUsers)) {
			$keywords_toid .= $toUsers['id']." ";
		}
		$keywords_toid = trim($keywords_toid);
		$keywords_toid = @eregi_replace(" ",",",$keywords_toid);
		if (empty($keywords_toid)) $keywords_toid = 0;
		$searchPattern .= " AND (userid IN (".$keywords_toid."))";
	}
	if ($_REQUEST['keywords_rating']!="") {
		$searchPattern .= " AND rate='".$_REQUEST['keywords_rating']."'";
	}

	(string)$submPattern =  "(submitted=1 OR submitted=0)";
	
	if (@eregi('y', $_REQUEST['subm'])) $submPattern = "submitted=1";
	else if (@eregi('n', $_REQUEST['subm'])) $submPattern = "submitted=0";

	$nbFeedbacks = getSqlNumber("SELECT * FROM probid_feedbacks WHERE ".$submPattern." 
	".$searchPattern." 	
	ORDER BY ".$orderField." ".$orderType.""); 
	$getFeedbacks = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_feedbacks WHERE ".$submPattern."
	".$searchPattern." 
	ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");
	
	$additionalVars = "&keywords_from_username=".$_REQUEST['keywords_from_username']."&keywords_to_username=".$_REQUEST['keywords_to_username']."&keywords_rating=".$_REQUEST['keywords_rating'];
	?>
   <tr>
      <td align="center">[ <strong><?=$a_lang[VIEW];?>:</strong> <a href="editfeedbacks.php?start=<?=$start.$additionalVars;?>"><?=$a_lang[ALLFB];?></a> | 
			<a href="editfeedbacks.php?start=<?=$start.$additionalVars;?>&subm=y"><?=$a_lang[SUBMITTED];?></a> | 
			<a href="editfeedbacks.php?start=<?=$start.$additionalVars;?>&subm=n"><?=$a_lang[NOTSUBM];?></a> ] </td>
   </tr>
   <tr>
      <td><?=$a_lang[FBNOTE];?></td>
   </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr class="c4">
      <td width="115"><?=$a_lang[DETAILS];?></td>
      <td width="79" align="center"><?=$a_lang[RATE];?></td>
      <td><?=$a_lang[USER_FEEDBACK_COMM];?></td>
      <td width="70" align="center"><?=$a_lang[OPTIONS];?></td>
   </tr>
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="get">
      <input type="hidden" name="start" value="<?=$_REQUEST['start'];?>">
      <input type="hidden" name="keywords_from_username" value="<?=$_REQUEST['keywords_from_username'];?>">
      <input type="hidden" name="keywords_to_username" value="<?=$_REQUEST['keywords_to_username'];?>">
      <input type="hidden" name="keywords_rating" value="<?=$_REQUEST['keywords_rating'];?>">
      <? while ($feedback = mysqli_fetch_array($getFeedbacks)) { ?>
      <input type="hidden" name="id[]" value="<?=$feedback['id'];?>">
      <input type="hidden" name="submitted[]" value="<?=$feedback['submitted'];?>">
      <tr <? if ($feedback['submitted']==1) { ?>class="<? echo (($count++)%2==0)?"c1":"c2"; ?>" <? } else { ?> bgcolor="#4B6DE0" <? } ?>>
         <td valign="top" width="115"><?
				$fromUsername = getSqlField("SELECT username FROM probid_users WHERE id='".$feedback['fromid']."'","username");
				$toUsername = getSqlField("SELECT username FROM probid_users WHERE id='".$feedback['userid']."'","username");
				echo "<b>$a_lang[FROM]:</b> ".$fromUsername;
				echo "<br><b>$a_lang[TO]:</b> ".$toUsername;
				echo "<br><b>$a_lang[TYPE]:</b> ".$feedback['type'];
				if ($feedback['submitted']==1) echo "<br><b>$a_lang[DATE]:</b> ".displaydatetime($feedback['date'],$setts['date_format']);
				?>
         </td>
         <td align="center"><select name="fbrate[]">
               <option value="5" <?=($feedback['rate']==5)?"selected":"";?> style="color:#009933; ">5 ticks</option>
               <option value="4" <?=($feedback['rate']==4)?"selected":"";?> style="color:#009933; ">4 ticks</option>
               <option value="3" <?=($feedback['rate']==3)?"selected":"";?> style="color:#666666; ">3 ticks</option>
               <option value="2" <?=($feedback['rate']==2)?"selected":"";?> style="color:#FF0000; ">2 ticks</option>
               <option value="1" <?=($feedback['rate']==1)?"selected":"";?> style="color:#FF0000; ">1 ticks</option>
               <option value="0" <?=($feedback['rate']==0)?"selected":"";?>>n/a</option>
            </select></td>
         <td valign="top"><textarea name="fbcontent[]" class="smallfont" id="fbcontent[]" style="{ width:210px; height=70px; }"><? echo addSpecialChars($feedback['feedback']);?></textarea>
            <? if ($isCRF) { ?>
            <br />
            [ <strong><a href="javascript://" onclick="popUp('../repdetails.php?fbid=<?=$feedback['id'];?>');">
            <?=$lang[details];?>
            </a></strong> ]
            <? } ?>
         </td>
         <td align="center"  class="smallfont"><a href="<?=$_SERVER['PHP_SELF'];?>?option=delete&id=<?=$feedback['id'];?>">
            <?=$a_lang[DELETE];?>
            </a></td>
      </tr>
      <? } ?>
      <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
         <td colspan="4" align="center" class="c4"><input type="submit" name="savesettsok" value="<?=$a_lang[MAKE_CHANGES];?>"></td>
      </tr>
   </form>
</table>
<table width="100%"  border="0" cellspacing="4" cellpadding="4">
   <tr>
      <td align="center"><? paginate($start,$limit,$nbFeedbacks,"editfeedbacks.php",$additionalVars."&orderField=$orderField&orderType=$orderType&subm=".$_REQUEST['subm'].""); ?></td>
   </tr>
</table>
<? 	include ("footer.php"); 
} ?>
