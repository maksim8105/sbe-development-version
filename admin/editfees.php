<?
## v5.25 -> jun. 07, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

## initialize the selected category session variable
if (empty($_SESSION['sel_cat'])) $_SESSION['sel_cat'] = 0;

include ("../config/config.php");

if (isset($_POST['select_cat'])) {
	$_SESSION['sel_cat']=$_POST['sel_cat'];
}

if (isset($_POST['savesettsok'])) {
	switch ($_POST['page']) {
		case "signup":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];
			
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_signup_fee='".$set_button."', val_signup_fee='".$fee."' 
			WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "endauction":
			$set_button = (count($_POST['from'])==0) ? "N" : $_POST['setbutton'];
			
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_endauction_fee='".$set_button."', endauction_fee_applies='".$_POST['endauction_fee_applies']."' WHERE category_id='".$_SESSION['sel_cat']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			for ($i=0;$i<count($_POST['from']);$i++) {
				if (strtoupper($_POST['to'][$i])=="ABOVE") $_POST['to'][$i]=99999999999;
				
				$updateFields[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees_tiers SET
				fee_from='".$_POST['from'][$i]."',fee_to='".$_POST['to'][$i]."',
				fee_amount='".$_POST['amount'][$i]."',calc_type='".$_POST['calc_type'][$i]."' 
				WHERE id='".$_POST['id'][$i]."' AND category_id='".$_SESSION['sel_cat']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
			if (count($_POST['delete'])>0) {
				for ($i=0;$i<count($_POST['delete']);$i++) {
					$deleteFields[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees_tiers 
					WHERE id='".$_POST['delete'][$i]."' AND category_id='".$_SESSION['sel_cat']."'");
				}
			}
			if (strtoupper($_POST['newto'])=="ABOVE") $_POST['newto']=99999999999;
			if ($_POST['newfrom']!=""&&$_POST['newto']!=""&&$_POST['newamount']!=""&&is_numeric($_POST['newfrom'])&&is_numeric($_POST['newto'])&&is_numeric($_POST['newamount'])) {
				$insertFee = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fees_tiers
				(fee_from, fee_to, fee_amount, calc_type, fee_type, category_id) VALUES 
				(".$_POST['newfrom'].",".$_POST['newto'].",".$_POST['newamount'].",'".$_POST['newcalc_type']."','".$_POST['fee_type']."', '".$_SESSION['sel_cat']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
			break;
		case "hl":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_hlitem_fee='".$set_button."', val_hlitem_fee='".$fee."',
			hlitemlimit='".$_POST['tier']."', val_hlitem_fee2='".$_POST['secondvalue']."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "catfeat":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_catfeat_fee='".$set_button."', val_catfeat_fee='".$fee."',
			catfeatlimit='".$_POST['tier']."', val_catfeat_fee2='".$_POST['secondvalue']."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "swap":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_swap_fee='".$set_button."', val_swap_fee='".$fee."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "secondcat":
			if ($_POST['setbutton'] == "N") $value = 0;
			else $value = $_POST['value'];
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			second_cat_fee='".$value."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "bin":
			if ($_POST['setbutton'] == "N") $value = 0;
			else $value = $_POST['value'];
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			bin_fee='".$value."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "setup":
			$set_button = (count($_POST['from'])==0) ? "N" : $_POST['setbutton'];
			
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_setup_fee='".$set_button."' WHERE category_id='".$_SESSION['sel_cat']."'");
			for ($i=0;$i<count($_POST['from']);$i++) {
				if (strtoupper($_POST['to'][$i])=="ABOVE") $_POST['to'][$i]=99999999999;
				$updateFields[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees_tiers SET
				fee_from='".$_POST['from'][$i]."',fee_to='".$_POST['to'][$i]."',
				fee_amount='".$_POST['amount'][$i]."',calc_type='".$_POST['calc_type'][$i]."' 
				WHERE id='".$_POST['id'][$i]."' AND category_id='".$_SESSION['sel_cat']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
			if (count($_POST['delete'])>0) {
				for ($i=0;$i<count($_POST['delete']);$i++) {
					$deleteFields[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees_tiers 
					WHERE id='".$_POST['delete'][$i]."' AND category_id='".$_SESSION['sel_cat']."'");
				}
			}
			if (strtoupper($_POST['newto'])=="ABOVE") $_POST['newto']=99999999999;
			if ($_POST['newfrom']!=""&&$_POST['newto']!=""&&$_POST['newamount']!=""&&is_numeric($_POST['newfrom'])&&is_numeric($_POST['newto'])&&is_numeric($_POST['newamount'])) {
				$insertFee = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fees_tiers
				(fee_from, fee_to, fee_amount, calc_type, fee_type, category_id) VALUES 
				(".$_POST['newfrom'].",".$_POST['newto'].",".$_POST['newamount'].",'".$_POST['newcalc_type']."','".$_POST['fee_type']."', '".$_SESSION['sel_cat']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
			break;
		case "pic":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_pic_fee='".$set_button."', val_pic_fee='".$fee."',
			piclimit='".$_POST['tier']."', val_pic_fee2='".$_POST['secondvalue']."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "hpfeat":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_hpfeat_fee='".$set_button."', val_hpfeat_fee='".$fee."',
			hpfeatlimit='".$_POST['tier']."', val_hpfeat_fee2='".$_POST['secondvalue']."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "bold":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_bolditem_fee='".$set_button."', val_bolditem_fee='".$fee."',
			bolditemlimit='".$_POST['tier']."', val_bolditem_fee2='".$_POST['secondvalue']."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "rp":
			$fee = ($_POST['setbutton']=="N") ? 0 : $_POST['value'];
			$set_button = ($_POST['value']<=0) ? "N" : $_POST['setbutton'];

			$updateInformation=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_rp_fee='".$set_button."', val_rp_fee='".$fee."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "wantedad":
			if ($_POST['setbutton'] == "N") $value = 0;
			else $value = $_POST['value'];
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			wantedad_fee='".$value."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "custom_st":
			if ($_POST['setbutton'] == "N") $value = 0;
			else $value = $_POST['value'];
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			custom_st_fee='".$value."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "video":
			if ($_POST['setbutton'] == "N") $value = 0;
			else $value = $_POST['value'];
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			videofile_fee='".$value."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "relist_reduction":
			$value = ($_POST['value']<0) ? 0 : $_POST['value'];
			$value = ($_POST['value']>100) ? 100 : $value;
			
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			relist_fee_reduction='".$value."' WHERE category_id='".$_SESSION['sel_cat']."'");
			break;
		case "store":
			$fee_type = "flat";
			
			for ($i=0;$i<count($_POST['nb_items']);$i++) {
				$updateFields[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees_tiers SET
				store_nb_items='".$_POST['nb_items'][$i]."',
				fee_amount='".$_POST['amount'][$i]."',calc_type='".$fee_type."', store_recurring='".$_POST['recurring'][$i]."' 
				WHERE id='".$_POST['id'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
			if (count($_POST['delete'])>0) {
				for ($i=0;$i<count($_POST['delete']);$i++) {
					$deleteFields[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees_tiers 
					WHERE id='".$_POST['delete'][$i]."'");
					$inactStores = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
					store_account_type='0', store_active='0', store_lastpayment='0' WHERE 
					store_account_type='".$_POST['delete'][$i]."'");
				}
			}
			if ($_POST['newamount']!=""&&is_numeric($_POST['newnbitems'])&&is_numeric($_POST['newamount'])) {
				$insertFee = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fees_tiers
				(store_nb_items, fee_amount, calc_type, fee_type, store_recurring) VALUES 
				(".$_POST['newnbitems'].",".$_POST['newamount'].",'".$fee_type."','".$_POST['fee_type']."','".$_POST['newrecurring']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
			}
			## if there are no more store types, activate all stores
			$nbStoreTypes = getSqlRow("SELECT id FROM probid_fees_tiers WHERE fee_type='store'");
			## if there is at least one store type and the 
			$set_button = ($nbStoreTypes==0) ? "N" : $_POST['setbutton'];
			$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
			is_store_fee='".$set_button."'");

			### store related operations
			$isStoreFee = getSqlField("SELECT is_store_fee FROM probid_fees","is_store_fee");
			$currentTime = time();
			if ($isStoreFee=="N"||$nbStoreTypes==0) 
			$activateAllStores = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_account_type='0', store_active='1', store_lastpayment='0', store_nextpayment='0'  
			WHERE store_active='0'");
			if ($isStoreFee=="Y"&&$nbStoreTypes>0) {
				### get all users with active stores
				$getAllUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, store_account_type FROM probid_users WHERE store_active='1' AND active='1'");
				while($storeUsers = mysqli_fetch_array($getAllUsers)) {
					$isValidStore = getSqlNumber("SELECT id FROM probid_fees_tiers WHERE id='".$storeUsers['store_account_type']."'");
					if ($isValidStore==0) $invalidateStore[$storecnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active='0', store_account_type='0', store_nextpayment='0' WHERE id='".$storeUsers['id']."'");
				}
			}
			break;
	}
	$savedSettings="yes";
}

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_fees.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[FEES]; echo " / "; echo $a_lang[FEES_SETUP]; ?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table border="0" cellspacing="2" cellpadding="2" align="center" class="border">
   <form action="editfees.php" method="post">
      <input type="hidden" name="page" value="<?=$_REQUEST['page'];?>">
      <? $getCategories = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,name FROM probid_categories WHERE parent=0 AND custom_fees='Y' ORDER BY theorder,name"); ?>
      <tr class="c2">
         <td><?=$a_lang[CHOOSE_CAT];?>
            : </td>
         <td><select name="sel_cat">
               <option value="0" selected>General</option>
               <option value="0">------------------------</option>
               <? while($category=mysqli_fetch_array($getCategories)) { ?>
               <option value="<?=$category['id'];?>" <? echo (($category['id']==$_SESSION['sel_cat'])?"selected":"");?>>
               <?=$category['name'];?>
               </option>
               <? } ?>
            </select></td>
         <td><input type="submit" name="select_cat" value="<?=$a_lang[SELECT];?>"></td>
      </tr>
   </form>
</table>
<br>
<table width="90%" border="0" cellpadding="4" cellspacing="2" align="center" class="border">
   <tr class="c1">
      <td><? if (!$_SESSION['sel_cat']) { ?>
         <img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=signup">
         <?=$a_lang[FEES_SETUP_SIGNUP];?>
         </a>
         <? } ?></td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=setup">
         <?=$a_lang[FEES_SETUP_SETUP];?>
         </a></td>
   </tr>
   <tr class="c2">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=endauction">
         <?=$a_lang[FEES_SETUP_END];?>
         </a></td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=pic">
         <?=$a_lang[FEES_SETUP_IMAGE];?>
         </a></td>
   </tr>
   <tr class="c1">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=hl">
         <?=$a_lang[FEES_SETUP_HIGH];?>
         </a></td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=hpfeat">
         <?=$a_lang[FEES_SETUP_FEAT];?>
         </a> </td>
   </tr>
   <tr class="c2">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=catfeat">
         <?=$a_lang[FEES_SETUP_CATEGORY];?>
         </a> </td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=bold">
         <?=$a_lang[FEES_SETUP_BOLD];?>
         </a> </td>
   </tr>
   <tr class="c1">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=swap">
         <?=$a_lang[FEES_SETUP_SWAP];?>
         </a> </td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=rp">
         <?=$a_lang[FEES_SETUP_RESERVE];?>
         </a></td>
   </tr>
   <tr class="c2">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=secondcat">
         <?=$a_lang[FEES_SETUP_SECOND_CAT];?>
         </a> </td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=bin">
         <?=$a_lang[FEES_BIN_FEE];?>
         </a></td>
   </tr>
   <tr class="c1">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=custom_st">
         <?=$a_lang[CUSTOM_ST_FEE];?>
         </a> </td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=relist_reduction">
         <?=$a_lang[RELIST_RED_FEE];?>
         </a></td>
   </tr>
   <tr class="c2">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=video">
         <?=$a_lang[VIDEOFILE_FEE];?>
         </a> </td>
      <td></td>
   </tr>
   <? if (!$_SESSION['sel_cat']) { ?>
   <tr class="c1">
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=wantedad">
         <?=$a_lang[FEES_WANTEDAD];?>
         </a> </td>
      <td><img src="images/a.gif" align="absmiddle"> <a href="editfees.php?page=store">
         <?=$a_lang[FEES_STORE];?>
         </a></td>
   </tr>
   <? } ?>
</table>
<? if ($_REQUEST['page']!="") { ?>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[SETT_CHANGED]."</p>":"<br>"; ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <input type="hidden" name="page" value="<?=$_REQUEST['page'];?>">
      <? $siteFees = getSqlRow("SELECT * FROM probid_fees WHERE category_id='".$_SESSION['sel_cat']."'"); ?>
      <? if ($_REQUEST['page']=="signup") { ?>
      <!-- Signup fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_SIGNUP1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_SIGNUP2];?>
            .</td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_SIGNUP3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_signup_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_signup_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><?=$setts['currency'];?>
            <input name="value" type="text" id="value" value="<?=$siteFees['val_signup_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="endauction"||$_REQUEST['page']=="setup") { ?>
      <? if ($_REQUEST['page']=="endauction") $isFee=$siteFees['is_endauction_fee'];
	else $isFee=$siteFees['is_setup_fee']; ?>
      <tr class="c2">
         <td><? echo ($_REQUEST['page']=="endauction")? $a_lang[FEES_SETUP_END3] : $a_lang[FEES_SETUP_SETUP3];?> </td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($isFee=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($isFee=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <? if ($_REQUEST['page']=="endauction") { ?>
      <tr class="c1">
         <td><?=$a_lang[END_APPLIES_TO];?>
         </td>
         <td><input type="radio" name="endauction_fee_applies" value="s" checked>
            <?=$a_lang[SELLER];?>
            <input type="radio" name="endauction_fee_applies" value="b" <? echo (($siteFees['endauction_fee_applies']=="b")?"checked":"");?>>
            <?=$a_lang[BUYER];?></td>
      </tr>
      <? } ?>
      <input type="hidden" name="fee_type" value="<?=$_REQUEST['page'];?>">
      <!-- End of auction fee settings -->
      <tr>
         <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="4" class="border">
               <tr class="c3">
                  <td colspan="5" align="center"><b> <? echo ($_REQUEST['page']=="endauction")? $a_lang[FEES_SETUP_END] : $a_lang[FEES_SETUP_SETUP1];?> </b></td>
               </tr>
               <tr class="c4">
                  <td width="95"><?=$a_lang[FROM];?></td>
                  <td width="95"><?=$a_lang[TO];?></td>
                  <td width="95"><?=$a_lang[AMOUNT];?></td>
                  <td><?=$a_lang[FEETYPE];?></td>
                  <td width="80" align="center"><?=$a_lang[DELETE];?></td>
               </tr>
               <? $getTierFees=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='".$_REQUEST['page']."' AND category_id='".$_SESSION['sel_cat']."'"); 
			while ($tierFees=mysqli_fetch_array($getTierFees)) { ?>
               <input type="hidden" name="id[]" value="<?=$tierFees['id'];?>">
               <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
                  <td><input name="from[]" type="text" id="from[]" value="<?=$tierFees['fee_from'];?>" size="9"></td>
                  <td><input name="to[]" type="text" id="to[]" value="<? echo ($tierFees['fee_to']==99999999999)?"Above":$tierFees['fee_to'];?>" size="9"></td>
                  <td><input name="amount[]" type="text" id="amount[]" value="<?=$tierFees['fee_amount'];?>" size="8"></td>
                  <td><select name="calc_type[]" id="calc_type[]">
                        <option value="flat" selected>Flat</option>
                        <option value="percent" <? echo ($tierFees['calc_type']=="percent")?"selected":""; ?>>Percent</option>
                     </select></td>
                  <td align="center"><input type="checkbox" name="delete[]" value="<?=$tierFees['id'];?>"></td>
               </tr>
               <? } ?>
               <tr class="c4">
                  <td><?=$a_lang[ADD];?></td>
                  <td colspan="4">&nbsp;</td>
               </tr>
               <tr class="c1">
                  <td><input name="newfrom" type="text" id="newfrom" size="9"></td>
                  <td><input name="newto" type="text" id="newto" size="9"></td>
                  <td><input name="newamount" type="text" id="newamount" size="8"></td>
                  <td><select name="newcalc_type" id="newcalc_type">
                        <option value="flat">Flat</option>
                        <option value="percent">Percent</option>
                     </select></td>
                  <td>&nbsp;</td>
               </tr>
            </table></td>
      </tr>
      <tr>
         <td colspan=2><strong>IMPORTANT:</strong> Please enter the word "<strong>above</strong>" if you want to set the final tier.</td>
      </tr>
      <? } else if ($_REQUEST['page']=="hl") { ?>
      <!-- Highlighted auction fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_HIGH1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_HIGH2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_HIGH3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_hlitem_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_hlitem_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['val_hlitem_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="catfeat") { ?>
      <!-- Category featured fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_CATEGORY1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_CATEGORY2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_CATEGORY3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_catfeat_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_catfeat_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['val_catfeat_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="swap") { ?>
      <!-- Auction Swapping fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_SWAP1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_SWAP2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_SWAP3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_swap_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_swap_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><?=$setts['currency'];?>
            <input name="value" type="text" id="value" value="<?=$siteFees['val_swap_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="secondcat") { ?>
      <!-- Second category fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_SECOND_CAT1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_SECOND_CAT2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_SECOND_CAT3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['second_cat_fee'] > 0)?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['second_cat_fee'] <= 0)?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['second_cat_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="bin") { ?>
      <!-- Second category fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_BIN_FEE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_BIN_FEE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_BIN_FEE3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['bin_fee'] > 0)?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['bin_fee'] <= 0)?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['bin_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="pic") { ?>
      <!-- Additional pics fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_IMAGE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_IMAGE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_IMAGE3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_pic_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_pic_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['val_pic_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="hpfeat") { ?>
      <!-- Home page featured fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_FEAT1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_FEAT2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_FEAT3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_hpfeat_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_hpfeat_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['val_hpfeat_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="bold") { ?>
      <!-- Bold auction fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_BOLD1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_BOLD2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_BOLD3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_bolditem_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_bolditem_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['val_bolditem_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="rp") { ?>
      <!-- Reserve price fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_RESERVE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_RESERVE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_RESERVE3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_rp_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_rp_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['val_rp_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="wantedad") { ?>
      <!-- Wanted Ad setup fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_WANTEDAD_FEE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_WANTEDAD_FEE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_WANTEDAD_FEE3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['wantedad_fee'] > 0)?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['wantedad_fee'] <= 0)?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['wantedad_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="custom_st") { ?>
      <!-- Custom Start times fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_CUSTOMST_FEE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_CUSTOMST_FEE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_CUSTOMST_FEE3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['custom_st_fee'] > 0)?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['custom_st_fee'] <= 0)?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['custom_st_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="video") { ?>
      <!-- Video File upload fee settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_VIDEO_FEE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_VIDEO_FEE2];?></td>
      </tr>
      <tr class="c1">
         <td><?=$a_lang[FEES_SETUP_VIDEO_FEE3];?></td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['videofile_fee'] > 0)?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['videofile_fee'] <= 0)?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[FEE_AMOUNT];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['videofile_fee'];?>" size="20">
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="relist_reduction") { ?>
      <!-- Relisting Fees reduction settings -->
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[FEES_SETUP_RELISTRED_FEE1];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[FEES_SETUP_RELISTRED_FEE2];?></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[REDUCTION];?></td>
         <td><input name="value" type="text" id="value" value="<?=$siteFees['relist_fee_reduction'];?>" size="8"> %
         </td>
      </tr>
      <? } else if ($_REQUEST['page']=="store") { ?>
      <!-- Store Setup fee settings -->
      <input type="hidden" name="fee_type" value="<?=$_REQUEST['page'];?>">
      <tr>
         <td colspan=2><?=$a_lang[STORE_FEE_NOTE1];?></td>
      </tr>
      <tr>
         <td colspan=2><?=$a_lang[STORE_FEE_NOTE2];?></td>
      </tr>
      <tr class="c2">
         <td><? echo $a_lang[FEES_STORE];?> </td>
         <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_store_fee']=="Y")?"checked":"");?>>
            <?=$a_lang[YES];?>
            <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_store_fee']=="N")?"checked":"");?>>
            <?=$a_lang[NO];?></td>
      </tr>
      <tr>
         <td colspan="2"><table width="100%" border="0" cellspacing="2" cellpadding="4" class="border">
               <tr class="c3">
                  <td colspan="6" align="center"><b> <? echo $a_lang[FEES_STORE_TITLE];?> </b></td>
               </tr>
               <tr class="c4">
                  <td><?=$a_lang[NB_ITEMS];?></td>
                  <td width="90"><?=$a_lang[AMOUNT];?>
                     [
                     <?=$setts['currency'];?>
                     ]</td>
                  <td width="130"><?=$a_lang[RECURRING];?>
                     [
                     <?=$a_lang[DAYS];?>
                     ]</td>
                  <td width="70" align="center"><?=$a_lang[DELETE];?></td>
               </tr>
               <? $getTierFees=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='".$_REQUEST['page']."' ORDER BY fee_amount ASC"); 
					while ($tierFees=mysqli_fetch_array($getTierFees)) { ?>
               <input type="hidden" name="id[]" value="<?=$tierFees['id'];?>">
               <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
                  <td><input name="nb_items[]" type="text" id="nb_items[]" value="<?=$tierFees['store_nb_items'];?>" size="9"></td>
                  <td align="center"><input name="amount[]" type="text" id="amount[]" value="<?=$tierFees['fee_amount'];?>" size="8"></td>
                  <td align="center"><input name="recurring[]" type="text" id="recurring[]" value="<?=$tierFees['store_recurring'];?>" size="8"></td>
                  <td align="center"><input type="checkbox" name="delete[]" value="<?=$tierFees['id'];?>"></td>
               </tr>
               <? } ?>
               <tr class="c4">
                  <td><?=$a_lang[ADD];?></td>
                  <td colspan="5">&nbsp;</td>
               </tr>
               <tr class="c1">
                  <td><input name="newnbitems" type="text" id="newnbitems" size="9"></td>
                  <td align="center"><input name="newamount" type="text" id="newamount" size="8"></td>
                  <td align="center"><input name="newrecurring" type="text" id="newrecurring" size="8"></td>
                  <td>&nbsp;</td>
               </tr>
            </table></td>
      </tr>
      <tr>
         <td colspan=2><?=$a_lang[STORE_FEE_NOTE];?></td>
      </tr>
      <? } ?>
      <tr class="c3">
         <td colspan="2" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td>
      </tr>
   </form>
</table>
<? 	} 
	include ("footer.php"); 
} ?>
