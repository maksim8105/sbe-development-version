<?
## v5.23 -> dec. 14, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1; 

if (isset($_POST['addannok'])) {
	if ($_POST['option']=="edit") {
		$updateNews = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_announcements SET
		title = '".remSpecialChars($_POST['title'])."',
		content = '".remSpecialChars($_POST['content'])."',
		date = '".$_POST['date']."',
		lang = '".$_POST['lang']."' WHERE id='".$_POST['id']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));		
	} else {
		$insertNews = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_announcements (title,content,date,active,lang) 
		VALUES ('".remSpecialChars($_POST['title'])."','".remSpecialChars($_POST['content'])."','".$_POST['date']."',1,'".$_POST['lang']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));	
	}
}

if ($_GET['option']=="chstatus") {
	$changeStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_announcements SET 
	active='".$_GET['value']."' WHERE id='".$_GET['id']."'");
}

if ($_GET['option']=="delete") {
	$deleteNews = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_announcements WHERE id='".$_GET['id']."'");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_ANNOUNCEMENTS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[ANNS_TABLE_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
  <form action="annmanagement.php" method="post"> 
    <tr class="c3"> 
      <td align="center" colspan="4"><b> 
        <?=strtoupper($a_lang[EDIT_ANNOUNCEMENTS]);?> 
        </b></td> 
    </tr> 
    <?  
	$getAnnouncements = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_announcements"); 
	$nbAns = mysqli_num_rows($getAnnouncements); ?> 
    <tr> 
      <td colspan="4" align="center"><b> 
        <?=$a_lang[THERE_ARE];?> 
        <?=$nbAns;?> 
        <?=$a_lang[ACTIVE_ANNS];?> 
        .</b></td> 
    </tr> 
    <tr>  
    <tr class="c4"> 
      <td><?=$a_lang[AN_TITLE];?></td> 
	 <td align="center"><?=$a_lang[LANG];?> </td>
      <td width="120" align="center"><?=$a_lang[DATE_POSTED];?></td> 
      <td width="120" align="center"><?=$a_lang[OPERATIONS];?></td> 
    </tr> 
    <? while ($ans=mysqli_fetch_array($getAnnouncements)) { ?> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td><a href="../viewannouncement.php?id=<?=$ans['id'];?>"> 
        <?=$ans['title'];?> 
        </a></td> 
	  <td align="center"><?=$ans['lang'];?> </td>
      <td align="center"><? echo convertdate($ans['date'],"M. d, Y");?></td> 
      <td align="center"><b> <? 
	  	echo "<a href=\"annedit.php?option=edit&id=".$ans['id']."\">$a_lang[EDIT]</a><br>";
	  	if ($news['active']==1) { 
			echo "<a href=\"annmanagement.php?option=chstatus&id=".$ans['id']."&value=0\"> $a_lang[SUSPEND] </a>"; 
		} else { 
			echo "<a href=\"annmanagement.php?option=chstatus&id=".$ans['id']."&value=1\"> $a_lang[ACTIVATE]</a>"; 
		} ?> 
        <br> 
        <a href="annmanagement.php?option=delete&id=<?=$ans['id'];?>"> 
        <?=$a_lang[DELETE];?> 
        </a></b></td> 
    </tr> 
    <? } ?> 
    <tr> 
      <td colspan="4" align="center" class="c3"><b><a href="annedit.php"> 
        <?=$a_lang[ADD_ANNOUNCEMENT];?> 
        </a></b></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
