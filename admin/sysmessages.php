<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");


include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");

/* TRANSLATION */

// EST - prefix_english
//english == est ?! 

$lang["title_english"] = "[SBE SÜSTEEM]Teile on saadetud uus kiri";
$lang["body_english"] = "Kasutaja SBE SÜSTEEM saatis kiri teemaga ";

// RUS - prefix_b

$lang["title_b"] = "[SBE SÜSTEEM] Vam prishlo ot pismo";
$lang["body_b"] = "POlzovatel SBE SÜSTEEM otpravil vam pismo s temoi ";

// LAT - prefix_latvian

$lang["title_latvian"] = "[SBE SÜSTEEM] Vam prishlo ot pismo";
$lang["body_latvian"] = "POlzovatel SBE SÜSTEEM otpravil vam pismo s temoi ";

// ENG - prefix_a

$lang["title_a"] = "[SBE SÜSTEEM] Vam prishlo ot pismo";
$lang["body_a"] = "POlzovatel SBE SÜSTEEM otpravil vam pismo s temoi ";

/* TRANSLATION */

if ($_POST["act"]=="send")
{ 

  if ((strlen($_POST["subject"])>0) && (strlen($_POST["content"])>0))
  {
      if (is_array($_POST["lang"]))
      {
          $where = " AND mylang IN (";
          foreach ($_POST["lang"] as $row=>$key) {
              $i++;
              $where .="'".$key."'".($i<count($_POST["lang"])?',':'');
          }
          $where .= ")";
      
      }
      $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,email,mylang FROM probid_users WHERE active=1".$where);
      while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
        $arrUsers[] = $row; 
      }

      if (is_array($arrUsers))
      {
        foreach ($arrUsers as $row) {
            $query = "INSERT INTO probid_messages(senderid,receiverid,messagetype,message,messagetitle,opened,datentime) VALUES(0,".$row["id"].",1,'".nl2br($_POST["content"])."','".$_POST["subject"]."',0,'".strtotime(date("Y-m-d H:i:s"))."')";
            mysqli_query($GLOBALS["___mysqli_ston"], $query);
            $id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
          	$headers = "MIME-Version: 1.0" . "\r\n";
            $headers.= "Content-type: text/plain; charset=utf-8" . "\r\n";
            $headers.= "From:  Erakliendi ankeet veebilehelt" . "\r\n";
            
            $subject = $lang["title_".$row["mylang"]];
            
            $mail = $lang["body_".$row["mylang"]]. "'".$_POST["subject"]."'\n\n";
            $mail .="\n LINK: http://www.sbe.ee/membersarea.php?page=messages&msgid=".$id."&msgsid=".md5($id);
            mail($row["email"], $subject, $mail, $headers);
        }
    } else {
      echo "Net polzovatelej s dannim jazikom";
    }
  } 



}

?>
<form action="sysmessages.php" method="post">
  <table width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr>
      <td colspan="2" align="center" class="c3"><b>
        SEND MESSAGE TO USERS
        </b></td>
    </tr>
    <tr class="c2">
      <td><?=$a_lang[SUBJECT];?>
        :</td>
      <td><input name="subject" type="text" id="subject" size="40"></td>
    </tr>
    <tr class="c1">
        <td>Language:</td>
        <td>
            <?=myLangChecks($_POST["lang"],"radio")?>
        </td>
    </tr>
    <tr class="c2">
      <td><?=$a_lang[ENTER_CONTENT];?>
        :</td>
      <td><textarea name="content" cols="45" rows="10" id="content"><?=addSpecialChars($newsletterContent);?></textarea>
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW];?></td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="c3"><input name="act" type="hidden"  value="send"><input name="sendnlok" type="submit" id="sendnlok" value="SEND MESSAGE"></td>
    </tr>
  </table>
</form>
<?

include ("footer.php"); 
} ?>
