<?
## v5.25 -> jun. 05, 2006

session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("../config/lang/list.php");
	
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1;  
	
$_GET = array_map('stripslashes', $_GET); 
$_POST = array_map('stripslashes', $_POST); 

$chosen_lang = $_POST['lang'];

if (isset($_POST['savesettsok'])) {
	$edit = $_POST['orig']; 
	$filename = "../config/lang/".$chosen_lang."/site.lang";
	if (is_writable($filename)) {
		if (!$editfile = fopen($filename, 'w')) {
			$updated = "notopen";
			exit;
		}
		if (!fwrite($editfile, addSpecialChars($edit))) {
			$updated = "notwritten";
			exit;
		}
		$updated = "yes";    
		fclose($editfile);		
	} else {
		$updated = "notwritable";
	}
}

include ("header.php"); 
?>
	<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
	<tr> 
		<td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
		<td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
		<td>&nbsp;</td> 
	</tr> 
	<tr> 
		<td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_SITE_LANG];?>&nbsp;&nbsp;</td> 
		<td><img src="images/end_part.gif"></td> 
	</tr> 
	</table> 
	<br> 
	<table width="100%" border="0" cellspacing="2" cellpadding="4">
	<? if ($updated!="") { ?> 
	<tr class="c4"> 
		<td align="center" colspan="2"> 
		<? if ($updated=="yes") echo "The file $filename was updated";
		else if ($updated=="notwritable") echo "The file $filename is not writable";
		else if ($updated=="notopen")  echo "Cannot open file $filename";
		else if ($updated=="notwritten") echo "The file $filename is not editable";
		?></td> 
	</tr> 
	<? } ?>
	<form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
	<input type="hidden" name="lang" value="<?=$chosen_lang;?>">
	<tr>
		<td class="c3" align="right"><?=$a_lang[h_choose];?> 
		<? echo  "<select name=\"lang\">";
		for ($z=0; $z < $sizeofarray; $z++) {
			echo "<option value=\"$langlist[$z]\"";
			if($chosen_lang==$langlist[$z]) echo " selected=\"selected\"";
			echo ">$langlist[$z]\n";
		}
		echo "</select>"; ?>
		<input name="changelang" type="submit" id="changelang" value="<?=$a_lang[h_go];?>"> </td>
	</tr>
	<? if ($chosen_lang) { ?>
	<tr>
		<?
		$file1=fopen("../config/lang/".$chosen_lang."/site.lang","r");
		while (!feof ($file1)) {
			$file1_contents.=fgets($file1, 4096);
		}
		fclose ($file1);
		?>
		<td align="center"><textarea name="orig" cols="80" rows="27"><?=$file1_contents;?></textarea></td>
	</tr>
	<tr class="c3"> 
		<td align="center"><input name="savesettsok" type="submit" value="<?=$a_lang[BUTT_PROCESS];?>"></td> 
	</tr> 
	<? } ?>
	</form>
	</table>
</td>
<? 	include ("footer.php"); 
} ?>
