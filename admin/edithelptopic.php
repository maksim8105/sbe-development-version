<?
## v5.23 -> dec. 13, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1; 

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_HELP];?>&nbsp;&nbsp;</td>
    <td><img src="images/end_part.gif"></td>
  </tr>
</table>
<br>
<form action="helptopics.php" method="post">
  <table width="100%" border="0" cellpadding="4" cellspacing="2">
    <?
	if ($_GET['option']=="add") { 
		echo "<input type=hidden name=option value=add>"; 
	} else {
		echo "<input type=hidden name=id value=".$_GET['id'].">";
		$helpTopicDetails = getSqlRow("SELECT * FROM probid_help_topics WHERE id='".$_GET['id']."'");
	} ?>
    <tr>
      <td colspan="2" align="center" class="c3"><b>
        <?=$a_lang[ADD_EDIT_TOPIC];?>
        </b></td>
    </tr>
    <tr class="c1">
      <td width="150"><b>
        <?=$a_lang[TOPIC_NAME];?>
        :</b></td>
      <td><input type="text" name="name" value="<?=$helpTopicDetails['name'];?>">
        &nbsp;&nbsp;&nbsp; <? echo  "<select name=\"lang\">";
			for ($z=0; $z < $sizeofarray; $z++) {
				echo "<option value=\"$langlist[$z]\"";
				if($helpTopicDetails['lang']==$langlist[$z]) {
					echo " selected=\"selected\"";
				}
				echo ">$langlist[$z]\n";
			}	?></td>
    </tr>
    <tr valign="top" class="c2">
      <td><b>
        <?=$a_lang[ENTER_CONTENT];?>
        :</b></td>
      <td><textarea name="content" cols="45" rows="10" id="content"><?
 		echo @eregi_replace("<br>","\n",$helpTopicDetails['content']);
	  ?>
</textarea>
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW];?>
      </td>
    </tr>
    <tr class="c3">
      <td colspan="2" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVE];?>"></td>
    </tr>
  </table>
</form>
<? 	include ("footer.php"); 
} ?>
