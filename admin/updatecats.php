<?
## v5.25 -> jun. 05, 2006
////////////////////////////////////////////////////
// Multi-lingual module - additional file - updatecats.php
// Last Update : 18/02/2005 By Hobbs (Kevin Thomas)
////////////////////////////////////////////////////
/* 
Description: 
This file writes the required details from the probid_categories table in to
the probid_plain_categories table and also creates a flat file key pair array 
for use within the site (bulkkats.php and editauction.php so far)

Copyright:
These files are owned and are intended ONLY for use in the phpprobid auction script
http://www.phpprobid.com
Re-sale of the following code (in part or in whole) without prior written consent is 
expressly forbidden
*/

session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

	include_once ("../config/config.php");
	include ("header.php"); 
	
	$link = "cat_lang.php";
	
	// As we are running a script to REMEMBER if you make any adjustments to the cetgories, we may as move this server hog here too !
	// Start update plain cetgories table
	if (@!$open_file = fopen ("../config/lang/".$setts['default_lang']."/catsarray.php","w"))
	{
		echo "Sorry I cannot open the file '".$setts['default_lang']."/catsarray.php', please inform the administrator of this problem";
		exit;
	}
	
	$cat_array = array ();
	
	$getcats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories ORDER BY name");
	$get_cat_count = mysqli_num_rows($getcats);
	while ($crow = mysqli_fetch_array($getcats)) {
		$croot = $crow['id'];
		$catname = "";
		$cntr = 0;
		$ct=0;
		$number++;
		while ($croot>0) {
			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE id='$croot' ORDER BY name") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));	
			$crw = mysqli_fetch_array($sbcts);
			if($cntr == 0) {
				$catid[$ct] = $crw[id];
				$catname = $crw[name];
				$ct++;
			} else {
				if($crw['parent'] != $croot) {
					$catid[$ct] = $crw[id];
					$catname = $crw[name]." : ".$catname;
					$ct++;
				}
			}
			$cntr++;
			$croot = $crw['parent'];
		}
		
		$cat_array[$catid[0]] = array( remSpecialChars($catname), $crow['userid']) ;
	}
	
	asort($cat_array);
	

	(string) $newCatVar = NULL;
	
	$newCatVar = "<?\n";
	
	if (count($cat_array) > 0) {
		$newCatVar .='$cat_array = array (';
		while (list($catid, $cat_array_details) = each ($cat_array)){
			list($catname, $userid) = $cat_array_details;
			$newCatVar .= '"'.$catid.'" => array ("'.remSpecialChars($catname).'", '.$userid.'), ';
		}
		
		$newCatVar = substr($newCatVar,0,-2);
		
		$newCatVar .= "); ";
	}
	
	$newCatVar .= "\n ?>";

	fputs($open_file,$newCatVar); 	
	fclose($open_file);

	// End update categories plain table
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
	echo "<p class=contentfont align=center>categories saved, redirecting, please wait...<br><br>Please click <a href=$link>here</a><br>if the page does not refresh automatically</p><p>&nbsp;</p>";
	echo "<script>window.setTimeout('changeurl();',2500); function changeurl(){window.location='$link'}</script>";
	echo "</td><td>&nbsp;</td></tr></table>";
	include ("footer.php"); 
	$_SESSION['cats']="0";
} ?> 
