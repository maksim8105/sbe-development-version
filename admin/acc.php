<?
## v5.22 -> oct. 06, 2005
session_start();
## NOTE: v5.22 -> this page is fully converted into php, no more php embedded in html, to make it clearer when debugging it
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include ("header.php");

echo "<script language=\"JavaScript\" src=\"../CalendarPopup.js\"></script> 									";
echo "<script language=\"JavaScript\">document.write(getCalendarStyles());</script> 							";
echo "<script language=\"JavaScript\"> 																			";					
echo "	var start_date = new CalendarPopup(); 																	";
echo "	start_date.setReturnFunction(\"setMultipleValuesStart\"); 												";
echo "	function setMultipleValuesStart(y,m,d) { 																";
echo "		document.acc.start_yy.value=y; 																		";
echo "		document.acc.start_mm.selectedIndex=m; 																";
echo "		for (var i = 0; i < document.acc.start_dd.options.length; i++) { 									";
echo "			if (document.acc.start_dd.options[i].value == d) document.acc.start_dd.selectedIndex = i; 		";
echo "		} 																									";
echo "	} 																										";
echo "	var finish_date = new CalendarPopup(); 																	";
echo "	finish_date.setReturnFunction(\"setMultipleValuesFinish\"); 											";
echo "	function setMultipleValuesFinish(y,m,d) { 																";
echo "		document.acc.finish_yy.value=y; 																	";
echo "		document.acc.finish_mm.selectedIndex=m; 															";
echo "		for (var i = 0; i < document.acc.finish_dd.options.length; i++) { 									";
echo "			if (document.acc.finish_dd.options[i].value == d) document.acc.finish_dd.selectedIndex = i; 	";
echo "		} 																									";
echo "	} 																										";
echo "	function getDateString(y_obj,m_obj,d_obj) { 															";
echo "		var y = y_obj.options[y_obj.selectedIndex].value; 													";
echo "		var m = m_obj.options[m_obj.selectedIndex].value; 													";
echo "		var d = d_obj.options[d_obj.selectedIndex].value; 													";
echo "		if (y==\"\" || m==\"\") { return null; } 															";
echo "		if (d==\"\") { d=1; } 																				";
echo "		return str= y+'-'+m+'-'+d; 																			";
echo "	} 																										";
echo "</script> 																								";

echo '<form action="acc.php" method="post" name="acc"> ';
echo '	<input type="hidden" name="check" value="1"> ';
echo '	<table width="100%" border="0" cellpadding="0" cellspacing="0"> ';
echo '		<tr> ';
echo '			<td rowspan="2"><img src="images/i_fees.gif" border="0"></td> ';
echo '			<td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> ';
echo '			<td>&nbsp;</td> ';
echo '		</tr> ';
echo '		<tr> ';
echo '			<td width="100%" align="right" background="images/bg_part.gif" class="head">'.$a_lang[FEES_ACCOUNTING].' / '.$a_lang[ACC_VIEW].'&nbsp;&nbsp;</td> ';
echo '			<td><img src="images/end_part.gif"></td> ';
echo '		</tr> ';
echo '	</table> ';
echo '  <br> ';
echo '  <table width="100%" cellpadding="3"> ';
echo '		<tr class="c1" valign="top"> ';
echo '			<td><table border="0" cellspacing="0"> ';
echo '				<tr> ';
echo '	            	<td colspan="2"><b>Detalization:</b></td> ';
echo '          	</tr> ';
echo '          	<tr> ';
echo '            		<td><input type="radio" name="level" value="3" '.(($_REQUEST['level']==3)?"checked":"").'></td> ';
echo '            		<td>Monthly</td> ';
echo '          	</tr> ';
echo '          	<tr> ';
echo '            		<td><input type="radio" name="level" value="2" '.(($_REQUEST['level']==2)?"checked":"").'></td> ';
echo '            		<td>Weekly</td> ';
echo '          	</tr> ';
echo '          	<tr> ';
echo '            		<td><input type="radio" name="level" value="1" '.(($_REQUEST['level']==1)?"checked":"").'></td> ';
echo '            		<td>Daily</td> ';
echo '          	</tr> ';
echo '          	<tr> ';
echo '            		<td><input type="radio" name="level" value="0" '.(($_REQUEST['level']==0)?"checked":"").'></td> ';
echo '            		<td>All</td> ';
echo '          	</tr> ';
echo '        	</table></td> ';
echo '      <td><b>Select Period:</b> ';
echo '			<table> ';
echo '          	<tr> ';
echo '           		<td align="top">From:</td> ';
echo '            		<td><select name="start_mm"  class="contentfont"> ';
echo '                		<option> ';
echo '                		<option value="01" '.(($_REQUEST['start_mm']==1)?"selected":"").'>'.$lang[jan].' ';
echo '                		<option value="02" '.(($_REQUEST['start_mm']==2)?"selected":"").'>'.$lang[feb].' ';
echo '                		<option value="03" '.(($_REQUEST['start_mm']==3)?"selected":"").'>'.$lang[mar].' ';
echo '                		<option value="04" '.(($_REQUEST['start_mm']==4)?"selected":"").'>'.$lang[apr].' ';
echo '                		<option value="05" '.(($_REQUEST['start_mm']==5)?"selected":"").'>'.$lang[may].' ';
echo '                		<option value="06" '.(($_REQUEST['start_mm']==6)?"selected":"").'>'.$lang[jun].' ';
echo '                		<option value="07" '.(($_REQUEST['start_mm']==7)?"selected":"").'>'.$lang[jul].' ';
echo '                		<option value="08" '.(($_REQUEST['start_mm']==8)?"selected":"").'>'.$lang[aug].' ';
echo '                		<option value="09" '.(($_REQUEST['start_mm']==9)?"selected":"").'>'.$lang[sep].' ';
echo '                		<option value="10" '.(($_REQUEST['start_mm']==10)?"selected":"").'>'.$lang[oct].' ';
echo '                		<option value="11" '.(($_REQUEST['start_mm']==11)?"selected":"").'>'.$lang[nov].' ';
echo '                		<option value="12" '.(($_REQUEST['start_mm']==12)?"selected":"").'>'.$lang[dec].' ';
echo '             		</select> ';
echo '              	<select name="start_dd" class="contentfont"> ';
echo '                		<option> ';
echo '                		<option value="01" '.(($_REQUEST['start_dd']==1)?"selected":"").'>1 ';
echo '                		<option value="02" '.(($_REQUEST['start_dd']==2)?"selected":"").'>2 ';
echo '               		<option value="03" '.(($_REQUEST['start_dd']==3)?"selected":"").'>3 ';
echo '               		<option value="04" '.(($_REQUEST['start_dd']==4)?"selected":"").'>4 ';
echo '                		<option value="05" '.(($_REQUEST['start_dd']==5)?"selected":"").'>5 ';
echo '                		<option value="06" '.(($_REQUEST['start_dd']==6)?"selected":"").'>6 ';
echo '                		<option value="07" '.(($_REQUEST['start_dd']==7)?"selected":"").'>7 ';
echo '                		<option value="08" '.(($_REQUEST['start_dd']==8)?"selected":"").'>8 ';
echo '                		<option value="09" '.(($_REQUEST['start_dd']==9)?"selected":"").'>9 ';
echo '                		<option value="10" '.(($_REQUEST['start_dd']==10)?"selected":"").'>10 ';
echo '                		<option value="11" '.(($_REQUEST['start_dd']==11)?"selected":"").'>11 ';
echo '                		<option value="12" '.(($_REQUEST['start_dd']==12)?"selected":"").'>12 ';
echo '                		<option value="13" '.(($_REQUEST['start_dd']==13)?"selected":"").'>13 ';
echo '                		<option value="14" '.(($_REQUEST['start_dd']==14)?"selected":"").'>14 ';
echo '                		<option value="15" '.(($_REQUEST['start_dd']==15)?"selected":"").'>15 ';
echo '                		<option value="16" '.(($_REQUEST['start_dd']==16)?"selected":"").'>16 ';
echo '                		<option value="17" '.(($_REQUEST['start_dd']==17)?"selected":"").'>17 ';
echo '                		<option value="18" '.(($_REQUEST['start_dd']==18)?"selected":"").'>18 ';
echo '                		<option value="19" '.(($_REQUEST['start_dd']==19)?"selected":"").'>19 ';
echo '                		<option value="20" '.(($_REQUEST['start_dd']==20)?"selected":"").'>20 ';
echo '                		<option value="21" '.(($_REQUEST['start_dd']==21)?"selected":"").'>21 ';
echo '                		<option value="22" '.(($_REQUEST['start_dd']==22)?"selected":"").'>22 ';
echo '                		<option value="23" '.(($_REQUEST['start_dd']==23)?"selected":"").'>23 ';
echo '                		<option value="24" '.(($_REQUEST['start_dd']==24)?"selected":"").'>24 ';
echo '                		<option value="25" '.(($_REQUEST['start_dd']==25)?"selected":"").'>25 ';
echo '                		<option value="26" '.(($_REQUEST['start_dd']==26)?"selected":"").'>26 ';
echo '                		<option value="27" '.(($_REQUEST['start_dd']==27)?"selected":"").'>27 ';
echo '                		<option value="28" '.(($_REQUEST['start_dd']==28)?"selected":"").'>28 ';
echo '                		<option value="29" '.(($_REQUEST['start_dd']==29)?"selected":"").'>29 ';
echo '                		<option value="30" '.(($_REQUEST['start_dd']==30)?"selected":"").'>30 ';
echo '                		<option value="31" '.(($_REQUEST['start_dd']==31)?"selected":"").'>31 ';
echo '              	</select> ';
echo '              	<select name="start_yy" class="contentfont"> ';
echo '                		<option> ';
echo '                		<option value="2004" '.(($_REQUEST['start_yy']==2004)?"selected":"").'>2004 ';
echo '                		<option value="2005" '.(($_REQUEST['start_yy']==2005)?"selected":"").'>2005 ';
echo '                		<option value="2006" '.(($_REQUEST['start_yy']==2006)?"selected":"").'>2006 ';
echo '                		<option value="2007" '.(($_REQUEST['start_yy']==2007)?"selected":"").'>2007 ';
echo '                		<option value="2008" '.(($_REQUEST['start_yy']==2008)?"selected":"").'>2008 ';
echo '                		<option value="2009" '.(($_REQUEST['start_yy']==2009)?"selected":"").'>2009 ';
echo '                		<option value="2010" '.(($_REQUEST['start_yy']==2010)?"selected":"").'>2010 ';
echo '              	</select> ';
echo '            		</td> ';
echo '            		<td><a href="#" onClick="start_date.showCalendar(\'start_date\',getDateString(document.acc.start_yy,document.acc.start_mm,document.acc.start_dd)); return false;" name="start_date" id="start_date"><img src="../images/calendar_b2u.gif" border="0" align="absmiddle"></a> </td> ';
echo '          	</tr> ';
echo '          	<tr> ';
echo '            		<td align="top">To:</td> ';
echo '            		<td><select name="finish_mm"  class="contentfont"> ';
echo '                		<option> ';
echo '                		<option value="01" '.(($_REQUEST['finish_mm']==1)?"selected":"").'>'.$lang[jan].' ';
echo '                		<option value="02" '.(($_REQUEST['finish_mm']==2)?"selected":"").'>'.$lang[feb].' ';
echo '                		<option value="03" '.(($_REQUEST['finish_mm']==3)?"selected":"").'>'.$lang[mar].' ';
echo '                		<option value="04" '.(($_REQUEST['finish_mm']==4)?"selected":"").'>'.$lang[apr].' ';
echo '                		<option value="05" '.(($_REQUEST['finish_mm']==5)?"selected":"").'>'.$lang[may].' ';
echo '                		<option value="06" '.(($_REQUEST['finish_mm']==6)?"selected":"").'>'.$lang[jun].' ';
echo '                		<option value="07" '.(($_REQUEST['finish_mm']==7)?"selected":"").'>'.$lang[jul].' ';
echo '                		<option value="08" '.(($_REQUEST['finish_mm']==8)?"selected":"").'>'.$lang[aug].' ';
echo '                		<option value="09" '.(($_REQUEST['finish_mm']==9)?"selected":"").'>'.$lang[sep].' ';
echo '                		<option value="10" '.(($_REQUEST['finish_mm']==10)?"selected":"").'>'.$lang[oct].' ';
echo '                		<option value="11" '.(($_REQUEST['finish_mm']==11)?"selected":"").'>'.$lang[nov].' ';
echo '                		<option value="12" '.(($_REQUEST['finish_mm']==12)?"selected":"").'>'.$lang[dec].' ';
echo '              	</select> ';
echo '              	<select name="finish_dd" class="contentfont"> ';
echo '                		<option> ';
echo '                		<option value="01" '.(($_REQUEST['finish_dd']==1)?"selected":"").'>1 ';
echo '                		<option value="02" '.(($_REQUEST['finish_dd']==2)?"selected":"").'>2 ';
echo '                		<option value="03" '.(($_REQUEST['finish_dd']==3)?"selected":"").'>3 ';
echo '                		<option value="04" '.(($_REQUEST['finish_dd']==4)?"selected":"").'>4 ';
echo '                		<option value="05" '.(($_REQUEST['finish_dd']==5)?"selected":"").'>5 ';
echo '                		<option value="06" '.(($_REQUEST['finish_dd']==6)?"selected":"").'>6 ';
echo '                		<option value="07" '.(($_REQUEST['finish_dd']==7)?"selected":"").'>7 ';
echo '                		<option value="08" '.(($_REQUEST['finish_dd']==8)?"selected":"").'>8 ';
echo '                		<option value="09" '.(($_REQUEST['finish_dd']==9)?"selected":"").'>9 ';
echo '                		<option value="10" '.(($_REQUEST['finish_dd']==10)?"selected":"").'>10 ';
echo '                		<option value="11" '.(($_REQUEST['finish_dd']==11)?"selected":"").'>11 ';
echo '                		<option value="12" '.(($_REQUEST['finish_dd']==12)?"selected":"").'>12 ';
echo '                		<option value="13" '.(($_REQUEST['finish_dd']==13)?"selected":"").'>13 ';
echo '                		<option value="14" '.(($_REQUEST['finish_dd']==14)?"selected":"").'>14 ';
echo '                		<option value="15" '.(($_REQUEST['finish_dd']==15)?"selected":"").'>15 ';
echo '                		<option value="16" '.(($_REQUEST['finish_dd']==16)?"selected":"").'>16 ';
echo '                		<option value="17" '.(($_REQUEST['finish_dd']==17)?"selected":"").'>17 ';
echo '                		<option value="18" '.(($_REQUEST['finish_dd']==18)?"selected":"").'>18 ';
echo '                		<option value="19" '.(($_REQUEST['finish_dd']==19)?"selected":"").'>19 ';
echo '                		<option value="20" '.(($_REQUEST['finish_dd']==20)?"selected":"").'>20 ';
echo '                		<option value="21" '.(($_REQUEST['finish_dd']==21)?"selected":"").'>21 ';
echo '                		<option value="22" '.(($_REQUEST['finish_dd']==22)?"selected":"").'>22 ';
echo '                		<option value="23" '.(($_REQUEST['finish_dd']==23)?"selected":"").'>23 ';
echo '                		<option value="24" '.(($_REQUEST['finish_dd']==24)?"selected":"").'>24 ';
echo '                		<option value="25" '.(($_REQUEST['finish_dd']==25)?"selected":"").'>25 ';
echo '                		<option value="26" '.(($_REQUEST['finish_dd']==26)?"selected":"").'>26 ';
echo '                		<option value="27" '.(($_REQUEST['finish_dd']==27)?"selected":"").'>27 ';
echo '                		<option value="28" '.(($_REQUEST['finish_dd']==28)?"selected":"").'>28 ';
echo '                		<option value="29" '.(($_REQUEST['finish_dd']==29)?"selected":"").'>29 ';
echo '                		<option value="30" '.(($_REQUEST['finish_dd']==30)?"selected":"").'>30 ';
echo '                		<option value="31" '.(($_REQUEST['finish_dd']==31)?"selected":"").'>31 ';
echo '              	</select> ';
echo '              	<select name="finish_yy" class="contentfont"> ';
echo '                		<option> ';
echo '                		<option value="2004" '.(($_REQUEST['finish_yy']==2004)?"selected":"").'>2004 ';
echo '                		<option value="2005" '.(($_REQUEST['finish_yy']==2005)?"selected":"").'>2005 ';
echo '                		<option value="2006" '.(($_REQUEST['finish_yy']==2006)?"selected":"").'>2006 ';
echo '                		<option value="2007" '.(($_REQUEST['finish_yy']==2007)?"selected":"").'>2007 ';
echo '                		<option value="2008" '.(($_REQUEST['finish_yy']==2008)?"selected":"").'>2008 ';
echo '                		<option value="2009" '.(($_REQUEST['finish_yy']==2009)?"selected":"").'>2009 ';
echo '                		<option value="2010" '.(($_REQUEST['finish_yy']==2010)?"selected":"").'>2010 ';
echo '              	</select> ';
echo '            		</td> ';
echo '            		<td><a href="#" onClick="finish_date.showCalendar(\'finish_date\',getDateString(document.acc.finish_yy,document.acc.finish_mm,document.acc.finish_dd)); return false;" name="finish_date" id="finish_date"><img src="../images/calendar_b2u.gif" border="0" align="absmiddle"></a> </td> ';
echo '          	</tr> ';
echo '       	</table></td> ';
echo '    	</tr> ';
echo '    	<tr> ';
echo '      	<td colspan="2" align="center" class="c3"><strong>IMPORTANT: </strong> Only a maximum of 100 transactions are shown for each of your queries to limit server load. </td> ';
echo '    	</tr> ';
echo '    	<tr> ';
echo '      	<td colspan="2" align="center"><input type="submit" name="_display" value="Display Report"></td> ';
echo '    	</tr> ';
echo '  </table> ';
echo '</form> ';

if ($_REQUEST['check']) {
	if ($_REQUEST['level'] != 0) {
		$date_field = ($setts['account_mode'] == 1) ? 'paymentdate' : 'feedate';
    	switch ($_REQUEST['level']) {
			case '3':
				$group = "EXTRACT(YEAR_MONTH FROM FROM_UNIXTIME($date_field))";
				break;
			case '2':
				$group = "YEARWEEK(FROM_UNIXTIME($date_field))";
				break;
			case '1':
				$group = "TO_DAYS(FROM_UNIXTIME($date_field))";
				break;
		}
    	
		if ($setts['account_mode'] == 2) {
			$sql = "SELECT feedate, SUM(feevalue) AS totalfee, $group AS group_field FROM probid_invoices GROUP BY group_field ORDER BY group_field";
			$result = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
			$rows = array();
			while ($row = mysqli_fetch_assoc($result)) $rows[] = $row;
    	} else {
			$tables = array('probid_auctions', 'probid_users', 'probid_winners');
      		foreach ($tables as $table) {
				$sql = "SELECT paymentdate, SUM(amountpaid) AS totalfee, $group AS group_field FROM $table WHERE 
				payment_status = 'confirmed' AND amountpaid > 0 GROUP BY group_field ORDER BY group_field DESC";
				$getAuctions = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
        		
				while ($row = mysqli_fetch_array($getAuctions)) {
					$rows[$row['group_field']]['feedate'] = $row['paymentdate'];
					$rows[$row['group_field']]['totalfee'] += $row['totalfee'];
				}
			}
    	} 
		
		echo '<table width="100%"> ';
		echo '	<tr> ';
		echo '		<td>Period</td> ';
		echo '		<td>Total Fee</td> ';
		echo '  </tr> ';

		foreach($rows as $row) { 
    		switch ($_REQUEST['level']) {
				case '3':
					$start = getdate($row['feedate']);
					$start['mday'] = 1;
					$finish = $start;
					$finish['mday'] = strftime ("%d", mktime (0,0,0,$start['mon']+1, 1 ,$start['year']) - 86400);
					$title = date("F, Y", $row['feedate']);
					break;
				case '2':
					$start = getdate($row['feedate']);
					$offset = $start["wday"];
					$start = getdate($row['feedate'] - $offset * 86400);
					$finish = getdate($row['feedate'] + (6 - $offset) * 86400);
					$title = date("M. j, Y", $row['feedate'] - $offset * 86400) . " - " . date("M. j, Y", $row['feedate'] + (6 - $offset) * 86400);
					break;
				case '1':
					$start = getdate($row['feedate']);
					$finish = $start;
					$title = date("F, j, Y", $row['feedate']);
					break;
			} 
		  	
			echo '	<tr> ';
			echo '		<td><a href="acc.php?check=1&level=0&start_dd='.$start['mday'].'&start_mm='.$start['mon'].'&start_yy='.$start['year'].'&finish_dd='.$finish['mday'].'&finish_mm='.$finish['mon'].'&finish_yy='.$finish['year'].'">'.$title.'</a></td> ';
			echo '		<td>'.$row['totalfee'].'</td> ';
		  	echo '	</tr> ';
		} 
		echo '</table> ';
	} else {
    	if ($setts['account_mode'] == 2) {
			$tmp = array();
			if (intval($_REQUEST['start_yy']) && intval($_REQUEST['start_mm']) && intval($_REQUEST['start_dd']))
				$tmp[] = 'i.feedate >= UNIX_TIMESTAMP("' . intval($_REQUEST['start_yy']) . '-' . intval($_REQUEST['start_mm']) .'-' . intval($_REQUEST['start_dd']) . '")';
			if (intval($_REQUEST['finish_yy']) && intval($_REQUEST['finish_mm']) && intval($_REQUEST['finish_dd']))
				$tmp[] = 'i.feedate <= UNIX_TIMESTAMP("' . intval($_REQUEST['finish_yy']) . '-' . intval($_REQUEST['finish_mm']) .'-' . intval($_REQUEST['finish_dd']) . '") + 86400';
			$where = (sizeof($tmp)) ? 'AND ' . implode(' AND ', $tmp) : '';
			$sql = "SELECT SUM(i.feevalue) AS totalfee FROM probid_invoices i WHERE i.transtype = 'pending' $where";
			$total_pend = getSqlField($sql, "totalfee");
			$sql = "SELECT SUM(i.feevalue) AS totalfee FROM probid_invoices i WHERE i.transtype != 'pending' $where";
			$total_paid = getSqlField($sql, "totalfee");
			$sql = "SELECT i.userid, u.name, i.auctionid, a.itemname, a.startdate, u.balance, i.feename, i.feevalue, i.feedate, i.transtype, i.processor FROM 
			probid_invoices i, probid_users u, probid_auctions a WHERE 
			i.userid = u.id AND i.auctionid = a.id $where ORDER BY i.auctionid DESC, i.feedate DESC LIMIT 0,100";
			$result = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
			$auctions_list = array();
      		while ($row = mysqli_fetch_assoc($result)) {
				$auctions_list[$row['auctionid']][] = $row;
      		} 
			
			echo '<table width="100%"> ';
      		foreach ($auctions_list as $auction) {
        		$info = $auction[0];

			  	echo '	<tr class="c2" valign="top"> ';
    			echo '		<td colspan="2"><b>User:</b> <a href="userdetails.php?id='.$info['userid'].'">'.$info['name'].'</a> ';
				echo '		[<a href="acc_user.php?id='.$info['userid'].'">history</a>] <br> ';
      			echo '		<b>Auction:</b> <a href="../auctiondetails.php?id='.$info['auctionid'].'" target="_blank">'.$info['itemname'].'</a> <br> ';
      			echo '		<b>Date:</b> '.$info['startdate'].'<br> ';
      			echo ' 		<b>Balance:</b> <font color="red">'.$info['balance'].'</font> </td> ';
    			echo '		<td colspan="2" align="right"> ';
				if ($info['balance'] > 0) { 
	      			echo '<a href="../invoice_print.php?id='.$info['auctionid'].'&user_id='.$info['userid'].'" target="_blank">View Invoice</a><br> ';
      			}
    			echo ' 		</td> ';
  				echo ' 	</tr> ';

				foreach ($auction as $fee) {
  					echo '	<tr> ';
    				echo ' 		<td>'.$fee['feename'].'</td> ';
    				echo ' 		<td>'.$fee['feevalue'].'</td> ';
    				echo ' 		<td>'.date("M. j, Y H:i",$fee['feedate']).'</td> ';
    				echo ' 		<td>'.(($fee['transtype']=="pending")?"-":$fee['processor']).'</td> ';
  					echo ' 	</tr> ';
  				}
			}
  			
			echo '	<tr class="c4"> ';
    		echo ' 		<td colspan="4" align="right"> ';
			echo '		<b>Paid:'.(($total_paid) ? $total_paid : 0).'</b> | ';
			echo ' 		<b>Pending:'.(($total_pend) ? $total_pend : 0).'</b> | ';
			echo ' 		<b>Total:'.($total_paid+$total_pend).'</b> </td> ';
    		echo ' 	</tr> ';
			echo '</table> ';
		} else {
			// START -> Auction Setup Fee
			$tmp = array();
			if (intval($_REQUEST['start_yy']) && intval($_REQUEST['start_mm']) && intval($_REQUEST['start_dd']))
				$tmp[] = 'a.paymentdate >= UNIX_TIMESTAMP("' . intval($_REQUEST['start_yy']) . '-' . intval($_REQUEST['start_mm']) .'-' . intval($_REQUEST['start_dd']) . '")';
			if (intval($_REQUEST['finish_yy']) && intval($_REQUEST['finish_mm']) && intval($_REQUEST['finish_dd']))
				$tmp[] = 'a.paymentdate <= UNIX_TIMESTAMP("' . intval($_REQUEST['finish_yy']) . '-' . intval($_REQUEST['finish_mm']) .'-' . intval($_REQUEST['finish_dd']) . '") + 86400';
			$where = (sizeof($tmp)) ? 'AND ' . implode(' AND ', $tmp) : '';
			$sql = "SELECT a.id, a.itemname, a.ownerid, u.name, a.amountpaid, a.paymentdate, a.processor FROM probid_users u, probid_auctions a WHERE 
			a.ownerid = u.id AND a.payment_status = 'confirmed' AND 
			a.amountpaid > 0 $where ORDER BY a.paymentdate DESC LIMIT 0,100";
			
			$result = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
      		while ($row = mysqli_fetch_assoc($result))	$auctions[] = $row;
			// FINISH -> Auction Setup Fee
			
			// START -> User Signup Fee
			$tmp = array();
			if (intval($_REQUEST['start_yy']) && intval($_REQUEST['start_mm']) && intval($_REQUEST['start_dd']))
				$tmp[] = 'paymentdate >= UNIX_TIMESTAMP("' . intval($_REQUEST['start_yy']) . '-' . intval($_REQUEST['start_mm']) .'-' . intval($_REQUEST['start_dd']) . '")';
			if (intval($_REQUEST['finish_yy']) && intval($_REQUEST['finish_mm']) && intval($_REQUEST['finish_dd']))
				$tmp[] = 'paymentdate <= UNIX_TIMESTAMP("' . intval($_REQUEST['finish_yy']) . '-' . intval($_REQUEST['finish_mm']) .'-' . intval($_REQUEST['finish_dd']) . '") + 86400';
			$where = (sizeof($tmp)) ? 'AND ' . implode(' AND ', $tmp) : '';
			$sql = "SELECT * FROM probid_users WHERE 
			payment_status = 'confirmed' AND amountpaid > 0	$where ORDER BY paymentdate DESC LIMIT 0,100";
			$result = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
      		while ($row = mysqli_fetch_assoc($result))	$users[] = $row;
			// FINISH -> User Signup Fee

			// START -> End of Auction Fee
			$tmp = array();
			if (intval($_REQUEST['start_yy']) && intval($_REQUEST['start_mm']) && intval($_REQUEST['start_dd']))
				$tmp[] = 'w.paymentdate >= UNIX_TIMESTAMP("' . intval($_REQUEST['start_yy']) . '-' . intval($_REQUEST['start_mm']) .'-' . intval($_REQUEST['start_dd']) . '")';
			if (intval($_REQUEST['finish_yy']) && intval($_REQUEST['finish_mm']) && intval($_REQUEST['finish_dd']))
				$tmp[] = 'w.paymentdate <= UNIX_TIMESTAMP("' . intval($_REQUEST['finish_yy']) . '-' . intval($_REQUEST['finish_mm']) .'-' . intval($_REQUEST['finish_dd']) . '") + 86400';
			$where = (sizeof($tmp)) ? 'AND ' . implode(' AND ', $tmp) : '';
			$sql = "SELECT a.id, a.itemname, w.sellerid, u.name, w.amountpaid, w.paymentdate, w.processor FROM 
			probid_users u, probid_winners w, probid_auctions a WHERE 
			w.sellerid = u.id AND w.auctionid = a.id AND w.payment_status = 'confirmed' AND 
			w.amountpaid > 0 $where ORDER BY w.paymentdate DESC LIMIT 0,100";
			$result = mysqli_query($GLOBALS["___mysqli_ston"], $sql);
      		while ($row = mysqli_fetch_assoc($result)) $ends[] = $row;
			// FINISH -> End of Auction Fee
			
			$total = 0;
			$tables = array('probid_auctions', 'probid_users', 'probid_winners');
			foreach ($tables as $table) {
				$tmp = array();
				if (intval($_REQUEST['start_yy']) && intval($_REQUEST['start_mm']) && intval($_REQUEST['start_dd']))
					$tmp[] = $table . '.paymentdate >= UNIX_TIMESTAMP("' . intval($_REQUEST['start_yy']) . '-' . intval($_REQUEST['start_mm']) .'-' . intval($_REQUEST['start_dd']) . '")';
				if (intval($_REQUEST['finish_yy']) && intval($_REQUEST['finish_mm']) && intval($_REQUEST['finish_dd']))
					$tmp[] = $table . '.paymentdate <= UNIX_TIMESTAMP("' . intval($_REQUEST['finish_yy']) . '-' . intval($_REQUEST['finish_mm']) .'-' . intval($_REQUEST['finish_dd']) . '") + 86400';
				$where = (sizeof($tmp)) ? 'AND ' . implode(' AND ', $tmp) : '';
				$sql = "SELECT SUM(amountpaid) AS totalfee FROM $table WHERE payment_status = 'confirmed' AND amountpaid > 0 $where";
				$total += getSqlField($sql, "totalfee");
			} 
			
			echo '<table width="100%"> ';
  			echo ' 	<tr class="c4"> ';
    		echo ' 		<td colspan="5">Auction Setup Fees</td> ';
  			echo ' 	</tr> ';

			if (sizeof($auctions)) {
				$tmp = 0;
    			foreach ($auctions as $auction) { 
  					echo '	<tr class="'.((($tmp++)%2==0)?"c1":"c2").'"> ';
    				echo ' 		<td><a href="../auctiondetails.php?id='.$auction['id'].'" target="_blank">'.$auction['itemname'].'</a></td> ';
    				echo ' 		<td><a href="userdetails.php?id='.$auction['ownerid'].'">'.$auction['name'].'</a> ';
					echo ' 		[<a href="acc_user.php?id='.$auction['ownerid'].'">history</a>] </td> ';
    				echo '		<td>'.$auction['amountpaid'].' '.$setts['currency'].'</td> ';
    				echo '		<td>'.(($auction['paymentdate'] > 0)?date("M. j, Y H:i", $auction['paymentdate']):"n/a").'</td> ';
    				echo '		<td>'.$auction['processor'].'</td> ';
  					echo ' 	</tr> ';
  				}
  			} else { 
  				echo '	<tr> ';
    			echo '		<td colspan="5">None</td> ';
  				echo ' 	</tr> ';
 			} 
  			
			echo '	<tr class="c4"> ';
    		echo ' 		<td colspan="5">End of Auction Fees</td> ';
  			echo ' 	</tr> ';
  
  			if (sizeof($ends)) {
				$tmp = 0;
    			foreach ($ends as $end) { 
  					echo '	<tr class="'.((($tmp++)%2==0)?"c1":"c2").'"> ';
    				echo ' 		<td><a href="../auctiondetails.php?id='.$end['id'].'" target="_blank">'.$end['itemname'].'</a></td> ';
    				echo '		<td><a href="userdetails.php?id='.$end['sellerid'].'">'.$end['name'].'</a> ';
					echo '		[<a href="acc_user.php?id='.$end['sellerid'].'">history</a>] </td> ';
    				echo '		<td>'.$end['amountpaid'].' '.$setts['currency'].'</td> ';
    				echo '		<td>'.(($end['paymentdate'] > 0) ? date("M. j, Y H:i", $end['paymentdate']):"n/a").'</td> ';
    				echo '		<td>'.$end['processor'].'</td> ';
  					echo '	</tr> ';
  				}
  			} else {
  				echo ' 	<tr> ';
    			echo '		<td colspan="5">None</td> ';
  				echo '	</tr> ';
  			}
  
  			echo ' 	<tr class="c4"> ';
    		echo '		<td colspan="5">User Signup Fees</td> ';
  			echo '	</tr> ';
  			
			if (sizeof($users)) {
				$tmp = 0;
   	 			foreach ($users as $user) {
  					echo '	<tr class='.((($tmp++)%2==0)?"c1":"c2").'"> ';
    				echo '		<td colspan="2"><a href="userdetails.php?id='.$user['id'].'">'.$user['name'].'</a> ';
					echo '		[<a href="acc_user.php?id='.$user['id'].'">history</a>] </td> ';
    				echo '		<td>'.$user['amountpaid'].' '.$setts['currency'].'</td> ';
    				echo '		<td>'.(($user['paymentdate'] > 0)?date("M. j, Y H:i", $user['paymentdate']):"n/a").'</td> ';
    				echo '		<td>'.$user['processor'].'</td> ';
  					echo '	</tr> ';
				}
			} else {
  				echo '	<tr> ';
    			echo '		<td colspan="2">None</td> ';
  				echo '	</tr> ';
  			}
  			
			echo '	<tr class="c4"> ';
    		echo '		<td colspan="5" align="right"><b>Total: '.$total.'</b></td> ';
    		echo '	</tr> ';
			echo '</table> ';
    	}
  	}
}

include ("footer.php"); 
} ?>