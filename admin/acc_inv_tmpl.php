<?
## v5.20 -> may. 20, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include ("header.php");

if ($_REQUEST['check'])
{
  $sql = "UPDATE probid_gen_setts SET 
    invoice_header='" . ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['invoice_header']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : "")) . "',
    invoice_footer='" . ((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_REQUEST['invoice_footer']) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : "")) . "'";
  mysqli_query($GLOBALS["___mysqli_ston"], $sql);
}

$result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT invoice_header, invoice_footer FROM probid_gen_setts");
$invoice = mysqli_fetch_row($result);
?>

<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
    <td rowspan="2"><img src="images/i_fees.gif" border="0"></td> 
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
    <td>&nbsp;</td> 
  </tr> 
  <tr> 
    <td width="100%" align="right" background="images/bg_part.gif" class="head">Invoices Header and Footer Menu&nbsp;&nbsp;</td> 
    <td><img src="images/end_part.gif"></td> 
  </tr> 
</table>
<br>
<form action="acc_inv_tmpl.php" method="post">
<input type="hidden" name="check" value="1">
<table width="100%">
  <tr>
    <td>&nbsp;</td>
    <td>
      This option gives you the ability to customize your invoices.
      <br>
      The texts below are pre-appended and appended to the invoice body.
      <br><br>
      You <b>CANNOT</b> use html code here.
    </td>
  </tr>
  <tr class="c2" valign="top">
    <td><b>Invoice header</br></td>
    <td>
      <textarea name="invoice_header" cols="60" rows="10"><?=$invoice[0];?></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2"><p>&nbsp;</p></td>
  </tr>
  <tr class="c2" valign="top">
    <td><b>Invoice footer</br></td>
    <td>
      <textarea name="invoice_footer" cols="60" rows="10"><?=$invoice[1];?></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="Submit Changes"></td>
  </tr>
</table>
</form>
<? 	include ("footer.php"); 
} ?>
