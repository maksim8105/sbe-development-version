<?
## v5.20 -> may. 20, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_fees.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[FEES]; echo " / "; echo $a_lang[FEES_MAIN];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <tr class="c3"> 
    <td align="center"><b><?=$a_lang[SITE_ACCOUNTING];?></b></td> 
  </tr> 
  <tr> 
    <td><?=$a_lang[accmsg];?></td> 
  </tr> 
  <tr> 
    <td>
      <table width="100%"  border="0" cellspacing="2" cellpadding="2" class="border"> 
        <tr align="center" class="c4"> 
          <td><?=$a_lang[currpmode];?></td> 
          <td width="33%"><?=$a_lang[totprec];?></td> 
          <td width="33%"><?=$a_lang[totppend];?></td> 
        </tr> 
        <tr align="center" class="c3"> 
          <td><strong><? echo ($setts['account_mode']==1)?"$a_lang[live]":"$a_lang[account]"; ?></td> 
          <td><?
			if ($setts['account_mode']==1) {
				$amountPaid = getSqlField("SELECT sum(amountpaid) AS totalpaid FROM probid_users","totalpaid");
				$amountPaid += getSqlField("SELECT sum(amountpaid) AS totalpaid FROM probid_auctions","totalpaid");
				$amountPaid += getSqlField("SELECT sum(amountpaid) AS totalpaid FROM probid_winners","totalpaid");
				echo displayAmount($amountPaid);
			} else if ($setts['account_mode']==2) {
				$paymentsReceived = getSqlField("SELECT sum(feevalue) AS totalpaid FROM probid_invoices WHERE transtype='payment'","totalpaid");
				echo displayAmount($paymentsReceived,$setts['currency'],"YES");
			}
			?></td> 
          <td><? 
			if ($setts['account_mode']==1) {
				echo "-";
			} else if ($setts['account_mode']==2) {
				$paymentsPending = getSqlField("SELECT sum(balance) AS totalpending FROM probid_users WHERE active=1 AND balance>0","totalpending");
				echo displayAmount($paymentsPending,$setts['currency'],"YES");
			} ?></td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr>
    <td align="center" class="c3"><strong><?=$a_lang[detacc];?></strong></td>
  </tr>
  <tr> 
    <td><?
	  if ($setts['account_mode']==1) { ?> 
      <table width="100%"  border="0" cellspacing="2" cellpadding="2"> 
        <tr> 
          <td><?=$a_lang[accdesc];?></td> 
          <td align="center"><?=$a_lang[accdets];?></td> 
          <td align="center"><?=$a_lang[accamount];?></td> 
          <td align="center"><?=$a_lang[accdate];?></td> 
          <td align="center"><?=$a_lang[accprocessor];?></td> 
        </tr> 
        <?
  		$getAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auctions WHERE payment_status='confirmed' AND amountpaid>0");
		$cnt=0;
		while ($auctions = mysqli_fetch_array($getAuctions)) { 
			$description[$cnt]="Auction Setup Fee";
			$paymentAmount[$cnt]=$auctions['amountpaid'];
			$auctionId[$cnt]=$auctions['id'];
			$userId[$cnt]=$auctions['ownerid'];
			$processor[$cnt]=$auctions['processor'];
			$paymentDate[$cnt]=$auctions['paymentdate'];
			$cnt++;
		}
	  	$getUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE payment_status='confirmed' AND amountpaid>0");
		while ($users = mysqli_fetch_array($getUsers)) { 
			$description[$cnt]="User Signup Fee";
			$paymentAmount[$cnt]=$users['amountpaid'];
			$auctionId[$cnt]="-";
			$userId[$cnt]=$users['id'];
			$processor[$cnt]=$users['processor'];
			$paymentDate[$cnt]=$users['paymentdate'];
			$cnt++;
		}
	  	$getWinners = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_winners WHERE payment_status='confirmed' AND amountpaid>0");
		while ($winners = mysqli_fetch_array($getWinners)) { 
			$description[$cnt]="End of Auction Fee";
			$paymentAmount[$cnt]=$winners['amountpaid'];
			$auctionId[$cnt]=$winners['auctionid'];
			$userId[$cnt]=$winners['sellerid'];
			$processor[$cnt]=$winners['processor'];
			$paymentDate[$cnt]=$winners['paymentdate'];
			$cnt++;
		}
		for ($i = 0; $i<$cnt-1; $i++) {
			for ($j=$i+1; $j<$cnt; $j++) {
				if ($paymentDate[$i]>$paymentDate[$j]) {
					$tmp_order = $description[$i]; $description[$i] = $description[$j]; $description[$j] = $tmp_order;
					$tmp_order = $paymentAmount[$i]; $paymentAmount[$i] = $paymentAmount[$j]; $paymentAmount[$j] = $tmp_order;
					$tmp_order = $auctionId[$i]; $auctionId[$i] = $auctionId[$j]; $auctionId[$j] = $tmp_order;
					$tmp_order = $userId[$i]; $userId[$i] = $userId[$j]; $userId[$j] = $tmp_order;
					$tmp_order = $processor[$i]; $processor[$i] = $processor[$j]; $processor[$j] = $tmp_order;
					$tmp_order = $paymentDate[$i]; $paymentDate[$i] = $paymentDate[$j];	$paymentDate[$j] = $tmp_order;
				}
			}
		} 
		for ($i=0; $i<$cnt; $i++) { 
			echo "<tr class=\"".((($count++)%2==0)?"c1":"c2")."\">							\n";
			echo "	<td>".$description[$i]."</td>											\n";
			echo "	<td nowrap>U ID: ".$userId[$i]."<br>A ID: ".$auctionId[$i]."</td>		\n";
			echo "	<td nowrap>".displayAmount($paymentAmount[$i],$setts['currency'],"YES")."</td>\n";
			echo "	<td align=center>".(($paymentDate[$i]>0)?date("M. j, Y H:i",$paymentDate[$i]):"n/a")."</td>\n";
			echo "	<td>".$processor[$i]."</td>												\n";
	  		echo "</tr>";
	  	} ?> 
      </table> 
      <? } else if ($setts['account_mode']==2) { ?>
      <table width="100%"  border="0" cellspacing="2" cellpadding="2" class="border"> 
        <tr class="c4"> 
          <td><?=$a_lang[accdesc];?></td> 
          <td align="center"><?=$a_lang[accdets];?></td> 
          <td align="center"><?=$a_lang[accamount];?></td> 
          <td align="center"><?=$a_lang[accbalance];?></td> 
          <td align="center"><?=$a_lang[accdate];?></td> 
          <td align="center"><?=$a_lang[accprocessor];?></td> 
        </tr> 
		<? 
		$getPayments = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_invoices ORDER BY id DESC, feedate DESC");
		while ($payments = mysqli_fetch_array($getPayments)) { 
			echo "<tr class=\"".((($count++)%2==0)?"c1":"c2")."\" ".(($payments['transtype']!="pending")?"style=\"color=#FF0000; font-weight=bold;\"":"").">\n";
			echo "	<td>".$payments['feename']."</td>										\n";
			echo "	<td nowrap>U ID: ".$payments['userid']."<br>A ID: ".$payments['auctionid']."</td>\n";
			echo "	<td align=\"center\" nowrap>".displayAmount($payments['feevalue'],$setts['currency'],"YES")."</td>\n";
			echo "	<td align=\"center\" nowrap>".displayAmount($payments['balance'],$setts['currency'],"YES")."</td>\n";
			echo "	<td align=\"center\">".date("M. j, Y H:i",$payments['feedate'])."</td>					\n";
			echo "	<td align=\"center\">".(($payments['transtype']=="pending")?"-":$payments['processor'])."</td>\n";
	  		echo "</tr>";
	  	} 
	} ?>
	</table>
	</td> 
  </tr> 
</table> 
<? 	include ("footer.php"); 
} ?>
