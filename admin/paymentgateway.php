<?
## v5.22 -> sep. 20, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	$resetPGs = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_payment_gateways SET value=''");
	for ($i=0; $i<count($_POST['gateway']); $i++) {
		$updatePGs = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_payment_gateways SET
		value = 'checked' WHERE id='".$_POST['gateway'][$i]."'");
	}

	## if paypal isnt checked, disable paypal directpayment
	$isPP = getSqlRow("SELECT id FROM probid_payment_gateways WHERE UPPER(name)='PAYPAL' AND value='checked'");
	
	if (!$isPP) $disableDirectPayment = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_gen_setts SET paypaldirectpayment='0'"); 

	$update3=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_gen_setts SET 
	paypalemail='".$_POST['paypalemail']."',worldpayid='".$_POST['worldpayid']."',
	checkoutid='".$_POST['checkoutid']."',ikobombid='".$_POST['ikobombid']."',
	ikoboipn='".$_POST['ikoboipn']."',protxname='".$_POST['protxname']."',
	protxpass='".$_POST['protxpass']."',
	authnetid='".$_POST['authnetid']."', authnettranskey='".$_POST['authnettranskey']."',
	moneybookersemail='".$_POST['moneybookersemail']."'");
	$savedSettings = "yes";
}

if (getSqlNumber("SELECT * FROM probid_payment_gateways WHERE name='Authorize.Net'") <= 0) 
	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_payment_gateways VALUES('108','Authorize.Net','')");

if (getSqlNumber("SELECT * FROM probid_payment_gateways WHERE name='Nochex'") <= 0) 
	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_payment_gateways VALUES('100','Nochex','')");

if (getSqlNumber("SELECT * FROM probid_payment_gateways WHERE name='Moneybookers'") <= 0) 
	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_payment_gateways VALUES('110','Moneybookers','')");

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_fees.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[FEES]; echo " / "; echo $a_lang[SETUP_PAY_SETT];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[PG_CHANGED]."</p>":"<br>"; ?> 
<table width="550" border="0" cellspacing="2" cellpadding="4"> 
  <form name="form" action="paymentgateway.php" method="post"> 
    <tr class="c3"> 
      <td colspan="2" align="center"><b> 
        <?=$a_lang[SEL_PG];?> 
        </b></td> 
    </tr> 
    <tr class="c4"> 
      <td align="right"><b> 
        <?=$a_lang[ACTIVE];?> 
        :</b></td> 
      <td><b> 
        <? 
		$getActivePGs = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT name FROM probid_payment_gateways WHERE value='checked'");
		while($activePG = mysqli_fetch_array($getActivePGs)) {
			echo "[ <font color=\"#EEEE00\">".$activePG['name']."</font> ] ";
		}
		?> 
        </b></td> 
    </tr> 
    <? 
	$genSetts = getSqlRow("SELECT * FROM probid_gen_setts");
	$getPaymentGateways = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_gateways ORDER BY id"); 
	while ($paymentGateway = mysqli_fetch_array($getPaymentGateways)) { ?> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td align="right" width="150"> <? echo $paymentGateway['name'];?></td> 
      <td width="400"><input type="checkbox" name="gateway[]" value="<? echo $paymentGateway['id'];?>" <? echo $paymentGateway['value'];?> <? echo (($paymentGateway['id']==2)?"disabled":"");?>> </td> 
    </tr> 
    <? } ?> 
     <tr class="c4"> 
      <td align="right">Hansapank ID: </td> 
      <td><input name="HansapankID" type="text" id="HansapankID" value="<?=$genSetts['HansapankID'];?>" size="35"></td> 
    </tr> 
    <tr class="c1" align="center"> 
      <td colspan="2"><b>Hansapank Callback URL:</b><br> 
        www.hanza.net</td> 
    </tr>
    <tr class="c4"> 
      <td align="right">SEB ID: </td> 
      <td><input name="SEBID" type="text" id="SEBID" value="<?=$genSetts['SEBID'];?>" size="35"></td> 
    </tr> 
    <tr class="c1" align="center"> 
      <td colspan="2"><b>SEB Callback URL:</b><br> 
        www.seb.ee</td> 
    </tr>
    <tr class="c4"> 
      <td align="right">Paypal/Nochex Email Address:</td> 
      <td><input name="paypalemail" type="text" value="<?=$genSetts['paypalemail'];?>" size="35"></td> 
    </tr> 
    <tr class="c2" align="center"> 
      <td colspan="2" style="color:#FF0000;">Please note you will need to setup a PayPal IPN for all PayPal Payments, this can be done from your PayPal account, please find the IPN URL below</td> 
    </tr> 
    <tr class="c1" align="center"> 
      <td colspan="2"><b>Paypal Instant Payment Notification (IPN) URL:</b><br> 
        <?=$setts['siteurl'];?> 
        paymentprocess.php</td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">WorldPay ID: </td> 
      <td><input name="worldpayid" type="text" id="worldpayid" value="<?=$genSetts['worldpayid'];?>" size="35"></td> 
    </tr> 
    <tr class="c1" align="center"> 
      <td colspan="2"><b>WorldPay Callback URL:</b><br> 
        <?=$setts['siteurl'];?> 
        worldpayprocess.php</td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">2Checkout ID: </td> 
      <td><input name="checkoutid" type="text" id="checkoutid" value="<?=$genSetts['checkoutid'];?>"> </td> 
    </tr> 
    <tr align="center" class="c1"> 
      <td colspan="2"><b>2Checkout Callback URL:</b><br> 
        <?=$setts['siteurl'];?> 
        checkoutprocess.php</td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Ikobo Member ID: </td> 
      <td><input name="ikobombid" type="text" id="ikobombid" value="<?=$genSetts['ikobombid'];?>"></td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Ikobo IPN Password: </td> 
      <td><input name="ikoboipn" type="password" id="ikoboipn" value="<?=$genSetts['ikoboipn'];?>"> </td> 
    </tr> 
    <tr align="center" class="c1"> 
      <td colspan="2"><b>Ikobo Callback URL:</b><br> 
        <?=$setts['siteurl'];?> 
        ikoboprocess.php</td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Protx Vendor Name:</td> 
      <td><input name="protxname" type="text" id="protxname" value="<?=$genSetts['protxname'];?>"></td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Protx Password:</td> 
      <td><input name="protxpass" type="password" id="protxpass" value="<?=$genSetts['protxpass'];?>"> </td> 
    </tr> 
    <tr align="center" class="c1"> 
      <td colspan="2"><b>Protx Callback URL:</b><br> 
        <?=$setts['siteurl'];?> 
        protxprocess.php</td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Authorize.Net ID:</td> 
      <td><input name="authnetid" type="text" value="<?=$genSetts['authnetid'];?>" size="35"></td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Authorize.Net Transaction Key:</td> 
      <td><input name="authnettranskey" type="text" id="authnettranskey" value="<?=$genSetts['authnettranskey'];?>"> </td> 
    </tr> 
    <tr align="center" class="c1"> 
      <td colspan="2"><b>"Relay Response" URL:</b><br> 
        <?=$setts[siteurl];?> 
        authorize.net.process.php</td> 
    </tr> 
    <tr class="c4"> 
      <td align="right">Moneybookers Email:</td> 
      <td><input name="moneybookersemail" type="text" id="moneybookersemail" value="<?=$genSetts['moneybookersemail'];?>" size="35"> </td> 
    </tr> 
    <tr align="center" class="c1"> 
      <td colspan="2"><b>Return URL:</b><br> 
        <?=$setts[siteurl];?> 
        moneybookers.process.php</td> 
    </tr> 
    <tr> 
      <td colspan="2"><?=$a_lang[AUTH_LANG_NOTE];?></td> 
    </tr> 
    <tr> 
      <td colspan="2"><?=$a_lang[SETUP_PAY_NOTE];?></td> 
    </tr> 
    <tr> 
      <td colspan="2" align="center" class="c3"> <input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
