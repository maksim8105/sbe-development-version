<?
## v5.20 -> may. 20, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if ($option=="emptytable") {
	$delStats = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_stats");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_stats.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><?=$a_lang[STATS];?> 
&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? /*
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
  <tr class="c3"> 
    <td><b> 
      <?=$a_lang[STATS_NUM_AUCT];?> 
      </b></td> 
    <td width="100" align="right"> <b> <? echo getSqlNumber("SELECT * FROM probid_auctions"); ?> </b> </td> 
  </tr> 
  <tr class="c2"> 
    <td><?=$a_lang[STATS_OPEN_AUCT];?></td> 
    <td align="right"> <? echo getSqlNumber("SELECT * FROM probid_auctions WHERE active=1 AND closed=0"); ?> </td> 
  </tr> 
  <tr class="c1"> 
    <td><?=$a_lang[STATS_CLOSED_AUCT];?></td> 
    <td align="right"> <? echo getSqlNumber("SELECT * FROM probid_auctions WHERE active=1 AND closed=1"); ?> </td> 
  </tr> 
  <tr class="c2"> 
    <td><?=$a_lang[STATS_SUSP_AUCT];?></td> 
    <td align="right"> <? echo getSqlNumber("SELECT * FROM probid_auctions WHERE active=0"); ?> </td> 
  </tr> 
  <tr class="c1"> 
    <td><?=$a_lang[STATS_FEATURED_AUCT];?></td> 
    <td align="right"> <? echo getSqlNumber("SELECT * FROM probid_auctions WHERE hpfeat='Y' OR catfeat='Y' OR bolditem='Y' OR hlitem='Y' AND active=1 AND closed=0"); ?> </td> 
  </tr> 
  <tr class="c3"> 
    <td><b> 
      <?=$a_lang[STATS_NUMB_USERS];?> 
      </b></td> 
    <td align="right"><b> <? echo getSqlNumber("SELECT * FROM probid_users");	?> </b></td> 
  </tr> 
  <tr class="c1"> 
    <td><?=$a_lang[STATS_ACT_USERS];?></td> 
    <td align="right"> <? echo getSqlNumber("SELECT * FROM probid_users WHERE active=1"); ?> </td> 
  </tr> 
  <tr class="c2"> 
    <td><?=$a_lang[STATS_SUSP_USERS];?></td> 
    <td align="right"> <? echo getSqlNumber("SELECT * FROM probid_users WHERE active=0"); ?> </td> 
  </tr> 
  <tr class="c3"> 
    <td><b><a href="sitestats.php"> 
      <?=$a_lang[STATS_REFRESH];?> 
      </a></b></td> 
    <td align="right">&nbsp;</td> 
  </tr> 
</table> 
<br> 
<br> 
<?
function getnumber($table,$startdate,$enddate,$userid) {
	if ($table=="hits") $fq="SELECT id";
	if ($table=="unique") $fq="SELECT DISTINCT userip";
	$datefield="clickdate";
	$getnb=mysql_query("$fq FROM probid_stats WHERE $datefield<=$enddate AND $datefield>=$startdate");
	$result=mysql_num_rows($getnb);
	if ($result>0) return $result;
	else return "-";
}

?> 
<table width="100%"  border="0" cellpadding="4" cellspacing="2"> 
  <tr class="c3"> 
    <td colspan="2"><b> 
      <?=$a_lang[DETAILED_STATISTICS];?> 
      </b></td> 
  </tr> 
  <tr class="c1"> 
    <td><?=$a_lang[STATS_TOT_HITS];?></td> 
    <td width="35%" align="right"><? 
	$totalhits=getSqlNumber("SELECT id FROM probid_stats");
	echo $totalhits;
	?></td> 
  </tr> 
  <tr class="c2"> 
    <td><?=$a_lang[STATS_TOT_UNIQUE];?></td> 
    <td width="35%" align="right"><? echo getSqlNumber("SELECT DISTINCT userip FROM probid_stats");?></td> 
  </tr> 
  <tr class="c3"> 
    <td><strong> 
      <?=$a_lang[STATS_LAST_DAYS];?> 
      </strong></td> 
    <td width="35%" align="right">&nbsp;</td> 
  </tr> 
  	<?
	list($day,$month,$year)=explode("-",date("d-m-Y",time()));
	$todayzero=mktime(0,0,0,$month,$day,$year);
	$monthzero=mktime(0,0,0,$month,1,$year);
	$today=time();
	?> 
  <tr> 
    <td colspan="2"> <table width="100%" border="0" cellpadding="4" cellspacing="2" class="border"> 
        <tr class="c4"> 
          <td width="20%"><strong> 
            <?=$a_lang[STATS_ACTIVITY];?> 
            </strong></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*6));?></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*5));?></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*6));?></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*3));?></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*2));?></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*1));?></td> 
          <td width="10%" align="center"><? echo date("m/d",($todayzero-86400*0));?></td> 
          <td width="10%" align="center" nowrap><?=$a_lang[STATS_THIS_MOTHS];?></td> 
        </tr> 
        <tr class="c1"> 
          <td><?=$a_lang[STATS_HITS];?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*6),($todayzero-86400*6+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*5),($todayzero-86400*5+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*4),($todayzero-86400*4+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*3),($todayzero-86400*3+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*2),($todayzero-86400*2+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*1),($todayzero-86400*1+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($todayzero-86400*0),($todayzero-86400*0+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",($monthzero),($today),$userid);?></td> 
        </tr> 
        <tr class="c2"> 
          <td><?=$a_lang[STATS_UNIQUE_VISITS];?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*6),($todayzero-86400*6+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*5),($todayzero-86400*5+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*4),($todayzero-86400*4+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*3),($todayzero-86400*3+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*2),($todayzero-86400*2+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*1),($todayzero-86400*1+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($todayzero-86400*0),($todayzero-86400*0+86399),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",($monthzero),($today),$userid);?></td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr class="c3"> 
    <td><strong> 
      <?=$a_lang[STATS_LAST_MOTHS];?> 
      </strong></td> 
    <td width="35%" align="right">&nbsp;</td> 
  </tr> 
  <tr> 
    <td colspan="2"> <table width="100%" border="0" cellpadding="4" cellspacing="2" class="border"> 
        <tr class="c4"> 
          <td width="30%"><strong> 
            <?=$a_lang[STATS_ACTIVITY];?> 
            </strong></td> 
          <td width="10%" align="center"><? echo date("m/y",mktime(0,0,0,$month-5,$day,$year));?></td> 
          <td width="10%" align="center"><? echo date("m/y",mktime(0,0,0,$month-4,$day,$year));?></td> 
          <td width="10%" align="center"><? echo date("m/y",mktime(0,0,0,$month-3,$day,$year));?></td> 
          <td width="10%" align="center"><? echo date("m/y",mktime(0,0,0,$month-2,$day,$year));?></td> 
          <td width="10%" align="center"><? echo date("m/y",mktime(0,0,0,$month-1,$day,$year));?></td> 
          <td width="10%" align="center"><? echo date("m/y",mktime(0,0,0,$month-0,$day,$year));?></td> 
          <td width="10%" align="center"><?=$a_lang[STATS_OVERALL];?></td> 
        </tr> 
        <tr class="c1"> 
          <td><?=$a_lang[STATS_HITS];?></td> 
          <td align="center"><? echo getnumber("hits",mktime(0,0,0,$month-5,1,$year),mktime(0,0,0,$month-4,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",mktime(0,0,0,$month-4,1,$year),mktime(0,0,0,$month-3,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",mktime(0,0,0,$month-3,1,$year),mktime(0,0,0,$month-2,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",mktime(0,0,0,$month-2,1,$year),mktime(0,0,0,$month-1,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",mktime(0,0,0,$month-1,1,$year),mktime(0,0,0,$month-0,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",mktime(0,0,0,$month-0,1,$year),mktime(0,0,0,$month+1,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("hits",(0),($today),$userid);?></td> 
        </tr> 
        <tr class="c2"> 
          <td><?=$a_lang[STATS_UNIQUE_VISITS];?></td> 
          <td align="center"><? echo getnumber("unique",mktime(0,0,0,$month-5,1,$year),mktime(0,0,0,$month-4,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",mktime(0,0,0,$month-4,1,$year),mktime(0,0,0,$month-3,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",mktime(0,0,0,$month-3,1,$year),mktime(0,0,0,$month-2,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",mktime(0,0,0,$month-2,1,$year),mktime(0,0,0,$month-1,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",mktime(0,0,0,$month-1,1,$year),mktime(0,0,0,$month-0,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",mktime(0,0,0,$month-0,1,$year),mktime(0,0,0,$month+1,0,$year),$userid);?></td> 
          <td align="center"><? echo getnumber("unique",(0),($today),$userid);?></td> 
        </tr> 
      </table></td> 
  </tr> 
</table> 
<table width="100%" cellpadding="4" cellspacing="2"> 
  <tr class="c3"> 
    <td><strong> 
      <?=$a_lang[STATS_USER_BROWSER];?> 
      </strong></td> 
    <td width="35%" align="right">&nbsp;</td> 
  </tr> 
  <?
	$getBrowsers = mysql_query("SELECT DISTINCT userbrowser FROM probid_stats");
	while ($browser = mysql_fetch_array($getBrowsers)) {?> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
    <td><? echo $browser['userbrowser'];?></td> 
    <td width="35%" align="right" nowrap><?
	$browserHit = getSqlNumber("SELECT id FROM probid_stats WHERE userbrowser='".$browser['userbrowser']."'");
	$percentBrowser = ($browserHit/$totalhits)*100;
	echo $browserHit." (".number_format($percentBrowser,2,".",",")."%)";
	?></td> 
  </tr> 
  <? } ?> 
  <tr> 
    <td>&nbsp;</td> 
    <td width="35%" align="right">&nbsp;</td> 
  </tr> 
  <tr class="c3"> 
    <td><strong> 
      <?=$a_lang[STATS_LAST_REF];?> 
      </strong></td> 
    <td width="35%" align="right">&nbsp;</td> 
  </tr> 
  	<?
	$getReferrers=mysql_query("SELECT clickdate,referralsite FROM probid_stats ORDER BY clickdate DESC LIMIT 0,50");
	while ($referrer=mysql_fetch_array($getReferrers)) { ?> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
    <td><? echo (($referrer['referralsite']!="")? $referrer['referralsite'] :"-- direct hit --");?></td> 
    <td width="35%" align="right" nowrap><? echo date("M j, Y H:i:s",$referrer['clickdate']);?></td> 
  </tr> 
  <? } ?> 
</table> 
*/ ?>
<? 
	$nbStats = getSqlNumber("SELECT id FROM probid_stats");
	echo "<p align=\"center\">$a_lang[SITE_STATS_DISABLED]</p>";
	echo ($nbStats>0)? "<p align=center><strong><a href=\"sitestats.php?option=emptytable\">$a_lang[SITE_STATS_EMPTYTABLE]</a></strong></p>":"<p align=center><strong>$a_lang[SITE_STATS_TABLEEMPTY]</strong></p>";
	include ("footer.php"); 
} ?>
