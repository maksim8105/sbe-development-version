<?
## v5.24 -> apr. 07, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	$pid = $_POST['pid'];
	$payment = $_POST['payment'];
	$delete = $_POST['delete'];
	
	for ($i=0;$i<count($_POST['payment']);$i++) {
		$fileExtension = getFileExtension($ES['logourl']['name']);
		$cardImageName = uniqid(7,FALSE).".".$fileExtension;
		$isUpload = uploadFile($_FILES['logourl']['tmp_name'][$i],$cardImageName,"../cclogos/",TRUE,'../');
		if ($isUpload) {
			$updimg = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_payment_methods SET 
			logourl='cclogos/".$cardImageName."' WHERE id='".$pid[$i]."'");
		}
		$updatePaymentMethod[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_payment_methods SET 
		name='".remSpecialChars($payment[$i])."' WHERE id='".$pid[$i]."'");
	}
	
	if ($_POST['addc']!="") {
		$insertPaymentMethod = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_payment_methods (name) VALUES ('".remSpecialChars($_POST['addc'])."')");
		$newId = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
		$fileExtension = getFileExtension($_FILES['newlogo']['name']);
		$cardImageName = uniqid(7,FALSE).".".$fileExtension;
		$isUpload = uploadFile($_FILES['newlogo']['tmp_name'],$cardImageName,"../cclogos/",'../');
		if ($isUpload) {
			$updimg = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_payment_methods SET 
			logourl='cclogos/".$cardImageName."' WHERE id='".$newId."'");
		}
	}
	
	if (count($delete)>0) {
		for ($i=0;$i<count($delete);$i++) {
			$oldImage = getSqlField("SELECT * FROM probid_payment_methods WHERE name='".$delete[$i]."'","logourl");
			deleteFile("../",$oldImage);
			$deletePaymentMethod[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_payment_methods 
			WHERE name='".$delete[$i]."'");
		}
	}
	$savedSettings="yes";
	$arrangePaymentMethods=mysqli_query($GLOBALS["___mysqli_ston"], "ALTER TABLE probid_payment_methods ORDER BY name");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_tables.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[EDIT_TABLES]; echo " / "; echo $a_lang[EDIT_PAY_OPTIONS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[PAY_OPTIONS_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data"> 
    <tr> 
      <td colspan="3" align="center" class="c3"><b>PAYMENT SETTINGS </b></td> 
    </tr> 
    <tr class="c4"> 
      <td><?=$a_lang[PAY_OPTIONS_DESCLOGO];?></td> 
      <td width="80" align="center">&nbsp;</td> 
      <td width="80" align="center"><?=$a_lang[DELETE];?></td> 
    </tr> 
    <? $getsetts=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_payment_methods"); 
	while ($row=mysqli_fetch_array($getsetts)) { ?> 
    <input type="hidden" name="pid[]" value="<?=$row[id];?>"> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td><input name="payment[]" type="text" value="<?=$row['name'];?>" size="40"> 
        <br> 
        <input name="logourl[]" type="file" id="logourl[]"></td> 
      <td align="center"> <? if ($row[logourl]!="") echo "<img src=\"../$row[logourl]?tempid=".rand(1,10000)."\" border=0>";?> 
&nbsp;</td> 
      <td align="center"><input type="checkbox" name="delete[]" value="<?=$row['name'];?>"></td> 
    </tr> 
    <? } ?> 
    <tr class="c4"> 
      <td colspan="3"><?=$a_lang[PAY_OPTIONS_ADD];?> 
        <input name="addc" type="text" id="addc" size="40"></td> 
    </tr> 
    <tr class="c4"> 
      <td colspan="3"><?=$a_lang[PAY_OPTIONS_LOGO];?> 
        <input name="newlogo" type="file" id="newlogo"></td> 
    </tr> 
    <tr> 
      <td colspan="3" class="c3" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
