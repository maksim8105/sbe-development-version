<?
## v5.22 -> sep. 22, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	$fee_type = "flat";
			
	for ($i=0;$i<count($_POST['nb_items']);$i++) {
		$updateFields[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees_tiers SET
		store_name='".remSpecialChars($_POST['store_name'][$i])."',
		store_nb_items='".$_POST['nb_items'][$i]."',
		fee_amount='".$_POST['amount'][$i]."',calc_type='".$fee_type."', store_recurring='".$_POST['recurring'][$i]."' 
		WHERE id='".$_POST['id'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	if (count($_POST['delete'])>0) {
		for ($i=0;$i<count($_POST['delete']);$i++) {
			$deleteFields[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees_tiers 
			WHERE id='".$_POST['delete'][$i]."'");
			$inactStores = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
			store_account_type='0', store_active='0', store_lastpayment='0' WHERE 
			store_account_type='".$_POST['delete'][$i]."'");
		}
	}
	if ($_POST['newstorename']!=""&&$_POST['newamount']!=""&&is_numeric($_POST['newnbitems'])&&is_numeric($_POST['newamount'])) {
		$insertFee = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fees_tiers
		(store_name, store_nb_items, fee_amount, calc_type, fee_type, store_recurring) VALUES 
		('".remSpecialChars($_POST['newstorename'])."',".$_POST['newnbitems'].",".$_POST['newamount'].",'".$fee_type."','store','".$_POST['newrecurring']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	## if there are no more store types, activate all stores
	$nbStoreTypes = getSqlRow("SELECT id FROM probid_fees_tiers WHERE fee_type='store'");
	## if there is at least one store type and the 
	$set_button = ($nbStoreTypes==0) ? "N" : $_POST['setbutton'];
	$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fees SET 
	is_store_fee='".$set_button."'");
	
	### store related operations
	$isStoreFee = getSqlField("SELECT is_store_fee FROM probid_fees","is_store_fee");
	$currentTime = time();
	if ($isStoreFee=="N"||$nbStoreTypes==0) 
	$activateAllStores = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_account_type='0', store_active='1', store_lastpayment='0', store_nextpayment='0'  
	WHERE store_active='0'");
	if ($isStoreFee=="Y"&&$nbStoreTypes>0) {
		### get all users with active stores
		$getAllUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, store_account_type FROM probid_users WHERE store_active='1' AND active='1'");
		while($storeUsers = mysqli_fetch_array($getAllUsers)) {
			$isValidStore = getSqlNumber("SELECT id FROM probid_fees_tiers WHERE id='".$storeUsers['store_account_type']."'");
			if ($isValidStore==0) $invalidateStore[$storecnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET store_active='0', store_account_type='0', store_nextpayment='0' WHERE id='".$storeUsers['id']."'");
		}
	}
	$savedSettings="yes";
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_fees.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"> <? echo $a_lang[STORES]; echo " / "; echo $a_lang[STORES_MANAGEMENT]; ?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<? 
echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[SETT_CHANGED]."</p>":"<br>"; 
$siteFees = getSqlRow("SELECT * FROM probid_fees");
?>
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form action="<?=$_SERVER['PHP_SELF'];?>" method="post"> 
    <input type="hidden" name="page" value="<?=$_REQUEST['page'];?>"> 
    <!-- Store Setup fee settings --> 
    <input type="hidden" name="fee_type" value="<?=$_REQUEST['page'];?>"> 
	<tr>
		<td colspan=2><?=$a_lang[STORE_FEE_NOTE1];?></td>
	</tr>
	<tr>
		<td colspan=2><?=$a_lang[STORE_FEE_NOTE2];?></td>
	</tr>
	<tr class="c2">
      <td> <? echo $a_lang[FEES_STORE];?> </td> 
      <td><input type="radio" name="setbutton" value="Y" <? echo (($siteFees['is_store_fee']=="Y")?"checked":"");?>> 
        <?=$a_lang[YES];?> 
        <input type="radio" name="setbutton" value="N" <? echo (($siteFees['is_store_fee']=="N")?"checked":"");?>> 
        <?=$a_lang[NO];?></td> 
    </tr> 
    <tr> 
      <td colspan="2"> <table width="100%" border="0" cellspacing="2" cellpadding="4" class="border"> 
          <tr class="c3"> 
            <td colspan="7" align="center"><b> <? echo $a_lang[FEES_STORE_TITLE];?> </b></td> 
          </tr> 
          <tr class="c4">
            <td><?=$a_lang[SUBSCRIPTION_NAME];?></td> 
            <td width="80" align="center"><?=$a_lang[NB_ITEMS];?></td> 
            <td width="80" align="center"><?=$a_lang[AMOUNT];?> [ <?=$setts['currency'];?> ]</td> 
            <td width="80" align="center"><?=$a_lang[RECURRING];?> [ <?=$a_lang[DAYS];?> ]</td> 
            <td width="70" align="center"><?=$a_lang[DELETE];?></td> 
          </tr> 
          <? $getTierFees=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fees_tiers WHERE fee_type='store' ORDER BY fee_amount ASC"); 
		  while ($tierFees=mysqli_fetch_array($getTierFees)) { ?> 
          <input type="hidden" name="id[]" value="<?=$tierFees['id'];?>"> 
          <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
            <td><input name="store_name[]" type="text" id="store_name[]" value="<?=$tierFees['store_name'];?>" size="25"></td> 
            <td><input name="nb_items[]" type="text" id="nb_items[]" value="<?=$tierFees['store_nb_items'];?>" size="9"></td> 
            <td align="center"><input name="amount[]" type="text" id="amount[]" value="<?=$tierFees['fee_amount'];?>" size="8"></td> 
            <td align="center"><input name="recurring[]" type="text" id="recurring[]" value="<?=$tierFees['store_recurring'];?>" size="8"></td> 
            <td align="center"><input type="checkbox" name="delete[]" value="<?=$tierFees['id'];?>"></td> 
          </tr> 
          <? } ?> 
          <tr class="c4">
            <td><?=$a_lang[ADD];?></td> 
            <td colspan="6">&nbsp;</td> 
          </tr> 
          <tr class="c1">
            <td><input name="newstorename" type="text" id="newstorename" size="25"></td> 
            <td><input name="newnbitems" type="text" id="newnbitems" size="9"></td> 
            <td align="center"><input name="newamount" type="text" id="newamount" size="8"></td> 
            <td align="center"><input name="newrecurring" type="text" id="newrecurring" size="8"></td> 
            <td>&nbsp;</td> 
          </tr> 
        </table></td> 
    </tr> 
	<tr>
		<td colspan=2><?=$a_lang[STORE_FEE_NOTE];?></td>
	</tr>
    <tr class="c3"> 
      <td colspan="2" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? include ("footer.php"); 
} ?>
