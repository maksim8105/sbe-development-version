<?
## v5.20 -> may. 20, 2005
////////////////////////////////////////////////////
// Multi-lingual module - additional file - cat_lang.php
// Last Update : 18/02/2005 By Hobbs (Kevin Thomas)
////////////////////////////////////////////////////
/* 
Description: 
This file creates the category.lang file for the sites default langauge
The file (even if blank) must exist before this script is run

Copyright:
These files are owned and are intended ONLY for use in the phpprobid auction script
http://www.phpprobid.com
Re-sale of the following code (in part or in whole) without prior written consent is 
expressly forbidden
*/

session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

	include_once ("../config/config.php");
	include ("header.php"); 
	if (@!$open_file = fopen ("../config/lang/".$setts['default_lang']."/category.lang","w"))
	{
		echo 'Sorry I cannot open the file "category.lang", please inform the administrator of this problem';
		exit;
	}
	
	$category_lang .= "<?\n";
	
	$getCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, name FROM probid_categories ORDER BY name");
	while ($cat=mysqli_fetch_array($getCats)) { 
		$category_lang .= '$c_lang';
		$category_lang .= '[';
		$category_lang .= $cat[id];
		$category_lang .= ']="';
		$category_lang .= remSpecialChars($cat[name]);
		$category_lang .= '";';
		$category_lang .= "\n";
	}
	$category_lang .= "?>";
	
	fputs($open_file,$category_lang); 	
	fclose($open_file);
	if ($setts['mlc']=="Y") $link = 'cat_lang_edit.php';
	else $link = 'table.categories.php';
	// End update categories plain table
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
	echo "<p class=contentfont align=center>".$setts['default_lang']." language categories file saved, redirecting, please wait...<br><br>Please click <a href='".$link."'>here</a><br>if the page does not refresh automatically</p><p>&nbsp;</p>";
	echo "<script>window.setTimeout('changeurl();',2500); function changeurl(){window.location='$link'}</script>";
	echo "</td><td>&nbsp;</td></tr></table>";
	include ("footer.php"); 
} ?>