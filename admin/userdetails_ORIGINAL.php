<?
## v5.25 -> jun. 15, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("header.php");

if (isset($_REQUEST['addnotesok'])) {
	$addNote = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_admin_notes (userid, comment, regdate) VALUES 
	('".$_REQUEST['id']."', '".remSpecialChars($_GET['user_admin_note'])."', '".time()."')");
}

if ($_GET['option']=='rem_comment') {
	$delComment = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_admin_notes WHERE id='".$_REQUEST['comment_id']."'");
}
if (isset($_POST['submitok'])) {
	$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	balance='".$_POST['balance']."' WHERE id='".$_POST['id']."'");
	## add the credit in the accounting table
	$payment_gross = $_POST['balance_old']-$_POST['balance'];	
	$currentTime = time();
	
	$adjReason = (!empty($_POST['adjustment_reason'])) ? ' - '.$_POST['adjustment_reason'] : '';
	$insertInvoice = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_invoices 
	(userid,feename,feevalue,feedate,balance,transtype,processor) VALUES 
	('".$_POST['id']."','".$a_lang[ADMIN_CREDIT_ADJUST]." ".$adjReason."','".$payment_gross."','".$currentTime."','".$_POST['balance']."','payment','ADMIN')");
}

if (isset($_POST['chcredit'])) {
	$updateBalance = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	allowed_credit='".$_POST['allowed_credit']."' WHERE id='".$_POST['id']."'");
} ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[USER_DETAILS];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr class="c3">
      <td align="center" colspan="2"><b>
         <?=$a_lang[USER_DETAILS];?>
         </b></td>
   </tr>
   <? $userDetails = getSqlRow("SELECT * FROM probid_users WHERE id='".$_REQUEST['id']."'"); ?>
   <tr class="c1">
      <td><?=$a_lang[USERNAME];?>
         : </td>
      <td><b>
         <?=$userDetails['username'];?>
         </b></td>
   </tr>
   <tr class="c2">
      <td><?=$a_lang[FULL_NAME];?>
         : </td>
      <td><b>
         <?=$userDetails['name'];?>
         </b></td>
   </tr>
   <tr class="c1">
      <td><?=$a_lang[datejoined];?>
         : </td>
      <td><strong><? echo date("M. j, Y H:i",$userDetails['regdate']);?></strong></td>
   </tr>
   <tr class="c1">
      <td><?=$a_lang[EMAIL];?>
         : </td>
      <td><b>
         <?=$userDetails['email'];?>
         </b></td>
   </tr>
   <tr class="c1">
      <td><?=$a_lang[COMPANYNAME];?>
         : </td>
      <td><b>
         <?=($userDetails['companyname']!="") ? strToForm($userDetails['companyname']) : $a_lang[NO_COMPANY];?>
         </b></td>
   </tr>
   <? if ($setts['birthdate_type']==0) { ?>
   <tr class="c2">
      <td><?=$a_lang[DATE_OF_BIRTH];?>
         : </td>
      <td><b>
         <?=$userDetails['birthdate'];?>
         </b></td>
   </tr>
   <? } else { ?>
   <tr class="c2">
      <td><?=$a_lang[YEAR_OF_BIRTH];?>
         : </td>
      <td><b>
         <?=$userDetails['birthdate_year'];?>
         </b></td>
   </tr>
   <? } ?>
   <tr class="c1">
      <td><?=$a_lang[ADDRESS];?>
         : </td>
      <td><b>
         <?=$userDetails['address'];?>
         <br>
         <? echo $userDetails['zip'].", ".$userDetails['city'].", ".$userDetails['state'].", ".$userDetails['country'];?></b></td>
   </tr>
   <tr class="c2">
      <td><?=$a_lang[PHONE_NUMBER];?>
         : </td>
      <td><b>
         <?=$userDetails['phone'];?>
         </b></td>
   </tr>
   <tr class="c1">
      <td><?=$a_lang[ACCOUNT_STATUS];?>
         : </td>
      <td><b><? echo (($userDetails['active']==0)?"$a_lang[INACTIVE]":"$a_lang[ACTIVE]");?></b></td>
   </tr>
   <tr class="c2">
      <td><?=$a_lang[ITEMS_SOLD];?>
         : </td>
      <td><b>
         <?=$userDetails['items_sold'];?>
         </b></td>
   </tr>
   <tr class="c1">
      <td><?=$a_lang[ITEMS_BOUGHT];?>
         : </td>
      <td><b>
         <?=$userDetails['items_bought'];?>
         </b></td>
   </tr>
   <tr class="c1"> 
   		<td>User IP : </td> 
   		<td><b><?=$userDetails['reg_ip'];?></b></td> 
   </tr>
   <? 
	$tmp = getSqlField("SELECT payment_mode FROM probid_users WHERE id='".$userDetails['id']."'","payment_mode");
	if ($setts['account_mode_personal']==1) {
   	$account_mode_local = ($tmp) ? 2 : 1;
  	} else $account_mode_local = $setts['account_mode'];
  	
	if ($account_mode_local==2) { ?>
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
      <tr class="c4">
         <td><?=$a_lang[ACCOUNT_BALANCE];?>
            : </td>
         <td><?=$setts['currency'];?>
            <input name="balance_old" type="hidden" value="<?=$userDetails['balance'];?>">
            <input name="balance" type="text" value="<?=$userDetails['balance'];?>" size="15">
            <?=$a_lang[ADJ_REASON];?>
            :
            <input name="adjustment_reason" type="text" value="" size="25">
            <input type="submit" name="submitok" value="<?=$a_lang[BUTT_BALANCE];?>"></td>
      </tr>
      <tr class="c2">
         <td colspan="2"><?=$a_lang[USER_DETAILS_NOTE];?>
         </td>
      </tr>
   </form>
   <? } ?>
   <? if ($setts['account_mode_personal']==1 && $userDetails['payment_mode']==1) { ?>
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
      <tr class="c4">
         <td><?=$a_lang[ACCOUNT_CREDIT];?>
            : </td>
         <td><?=$setts['currency'];?>
            <input name="allowed_credit" type="text" value="<?=$userDetails['allowed_credit'];?>" size="15">
            <input type="submit" name="chcredit" value="<?=$a_lang[BUTT_CREDIT];?>"></td>
      </tr>
   </form>
   <? } ?>
</table>
<?
mysqli_query($GLOBALS["___mysqli_ston"], "CREATE TABLE IF NOT EXISTS `probid_iphistory` (
`memberid` INT NOT NULL, 
`time1` INT NOT NULL, 
`time2` INT NOT NULL, 
`ip` VARCHAR(20) NOT NULL)"); ?>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr class="c4">
      <td align="center" colspan="3"><b>IP Address History</b></td>
   </tr>
   <tr class="c3">
      <td align="center"><b>IP Address</b></td>
      <td width="33%" align="center"><b>Start Time</b></td>
      <td width="33%" align="center"><b>End Time</b></td>
   </tr>
   <?
	$a = 1;
	$q = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT time1, time2, ip FROM `probid_iphistory` WHERE memberid='" . $_REQUEST['id'] . "' ORDER by time1 DESC");
	if (mysqli_num_rows($q) > 0) {
		while ($r = mysqli_fetch_row($q)) {
			if ($r[1] < 1) $r[1] = $r[0]; ?>
   <tr class="c<?=(($a % 2) + 1)?>">
      <td align="center"><?=$r[2]?></td>
      <td align="center"><?=strftime("%b %d %Y (%H:%M)", $r[0])?></td>
      <td align="center"><?=strftime("%b %d %Y (%H:%M)", $r[1])?></td>
   </tr>
   <?		$a++;
		}
	} else { ?>
   <tr class="c<?=(($a % 2) + 1)?>">
      <td align="center" colspan="3">This user hasn't logged on since you started logging IPs!</td>
   </tr>
   <? } ?>
</table>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="get">
      <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
      <tr class="c4">
         <td align="center" colspan="2"><b>Notes on this User </b></td>
      </tr>
		<?
		$getNotes = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_admin_notes WHERE userid='".$_REQUEST['id']."' ORDER BY id DESC");
		while ($note = mysqli_fetch_array($getNotes)) {
			echo '<tr> ';
			echo '	<td><strong>Added:</strong> '.date($setts['date_format'], $note['regdate']).'<br>';
			echo '		<strong>Comment:</strong> '.$note['comment'].'</td>';
			echo '	<td width="100" align="center"><a href="userdetails.php?id='.$_REQUEST['id'].'&option=rem_comment&comment_id='.$note['id'].'">Remove</a></td>';
			echo '</tr>';
		}
		?>
      <tr class="c4">
         <td align="center" colspan="2"><b>Add Note </b></td>
      </tr>
      <tr class="c1">
         <td align="center" colspan="2"><textarea name="user_admin_note" style=" height: 100px; width: 95%; "></textarea></td>
      </tr>
      <tr class="c2">
         <td align="center" colspan="2"><input type="submit" name="addnotesok" value="Submit" /></td>
      </tr>
   </form>
</table>
<? include ("footer.php");
} ?>
