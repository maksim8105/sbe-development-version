<?
## v5.22 -> oct. 27, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");
?>

<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
    <td rowspan="2"><img src="images/i_user.gif" border="0"></td> 
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
    <td>&nbsp;</td> 
  </tr> 
  <tr> 
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[FEES_ACCOUNTING]; echo " / "; echo $a_lang[ACC_DEBTORS];?>&nbsp;&nbsp;</td> 
    <td><img src="images/end_part.gif"></td> 
  </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
<? 
	$start = (!$_REQUEST['start']) ? 0 : $_REQUEST['start'];
	$orderField = (!$_REQUEST['orderField']) ? "id" : $_REQUEST['orderField'];
	$limit = 20;
	if (!$_REQUEST['orderType'])
	{
		$orderType = "DESC";
		$newOrder="ASC";
	} else {
		$orderType=$_REQUEST['orderType'];
		$newOrder=($orderType=="ASC")?"DESC":"ASC";
	}
	$totalUsers = getSqlNumber("SELECT * FROM probid_users WHERE balance > 0 ORDER BY ".$orderField." ".$orderType.""); 
	$usersQuery = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE balance > 0.00	".$searchPattern." ORDER BY ".$orderField." ".$orderType." LIMIT ".$start.",".$limit."");
?> 
  <tr> 
    <td colspan="5"><?=$a_lang[YOUR_QWERY_MATCHED];?> 
      <?=$totalUsers;?> 
      <?=$a_lang[RESULTS];?>.
    </td> 
  </tr> 
  <tr class="c4"> 
    <td width="85"><a href="acc_owe.php?start=<?=$_REQUEST['start'];?>&orderField=username&orderType=<?=$newOrder.$additionalVars;?>"><font color="#EEEE00"><?=$a_lang[USERNAME];?></font></a></td> 
    <td><a href="acc_owe.php?start=<?=$_REQUEST['start'];?>&orderField=name&orderType=<?=$newOrder.$additionalVars;?>"><font color="#EEEE00"><?=$a_lang[USER_DETAILS];?></font></a></td> 
    <td width="105" align="center"><a href="acc_owe.php?start=<?=$_REQUEST['start'];?>&orderField=balance&orderType=<?=$newOrder.$additionalVars;?>"><font color="#EEEE00"><?=$a_lang[ACCOUNT_BALANCE];?></font></a></td> 
    <td width="45" align="center"><a href="acc_owe.php?start=<?=$_REQUEST['start'];?>&orderField=active&orderType=<?=$newOrder.$additionalVars;?>"><font color="#EEEE00"><?=$a_lang[STATUS];?></font></a></td> 
    <td align="center" width="105"><?=$a_lang[OPTIONS];?></td> 
  </tr> 
  <? while ($usersArray=mysqli_fetch_array($usersQuery)) { ?> 
  <tr class="<? echo (($count++)%2==0)?"c1":"c2";?>"> 
    <td><?=$usersArray['username'];?> 
      <br> 
      [ <a href="userdetails.php?id=<?=$usersArray['id'];?>"> 
      <?=$a_lang[USER_MANAGE_DETAILS];?> 
      </a> ]<br>
	  [ <a href="userbids.php?id=<?=$usersArray['id'];?>"> 
      <?=$a_lang[VIEW_BIDS];?> 
      </a> ]</td> 
    <td><?
	echo "<b>$a_lang[NAME] :</b> ".$usersArray['name']."<br>"
		."<b>$a_lang[COUNTRY] :</b> ".$usersArray['country']."<br>"
		."<b>$a_lang[EMAIL] :</b> ".$usersArray['email'];
	if ($usersArray['balance']<=0) {
		$thevar = "Credit";
		$balance = abs($usersArray['balance']);
	} else {
		$thevar = "Debit";
		$balance = $usersArray['balance'];
	}
	if ($setts['private_site']=="Y") {
		echo "<br><strong>".$a_lang[ENABLED_SELLER]."</strong> : <br>".
		(($usersArray['is_seller']=="N") ? 
		"[ $a_lang[NO] ] -> [ <a href=\"usersmanagement.php?option=sell&value=Y&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[ENABLE]</a> ]" :
		"[ <font color=\"#FF0000\">$a_lang[YES]</font> ] -> [ <a href=\"usersmanagement.php?option=sell&value=N&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[DISABLE]</a> ]");
	}
	if ($setts['pref_sellers']=="Y") {
		echo "<br><strong>".$a_lang[PREF_SELLER]."</strong> : <br>".
		(($usersArray['preferred_seller']=="N") ? 
		"[ $a_lang[NO] ] -> [ <a href=\"usersmanagement.php?option=pref_sell&value=Y&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[ENABLE]</a> ]" :
		"[ <font color=\"#FF0000\">$a_lang[YES]</font> ] -> [ <a href=\"usersmanagement.php?option=pref_sell&value=N&id=".$usersArray['id']."&start=".$_REQUEST['start']."&orderField=".$_REQUEST['orderField'].$additionalVars."\">$a_lang[DISABLE]</a> ]");
	}
	/*if ($setts[account_mode]==2) { 
		echo "<br><b>$a_lang[ACCOUNT_BALANCE]: </b>".(($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar."<br> <a href=\"userdetails.php?id=".$usersArray['id']."\"> --> $a_lang[EDIT_BALANCE]</a>"; 
		if ($usersArray['balance']>0) echo "<br><a href=\"usersmanagement.php?option=invoice&id=".$usersArray['id']."\">$a_lang[SEND_INVOICE]</a>";
	}*/ ?> </td> 
	<td align="center"><?
	if ($setts[account_mode]==2) { 
		echo (($balance==0)?"$setts[currency] $balance":displayAmount($balance))." ".$thevar."<br> <a href=\"userdetails.php?id=".$usersArray['id']."\">$a_lang[EDIT_BALANCE]</a>"; 
		if ($usersArray['balance']>0) echo "<br><a href=\"usersmanagement.php?option=invoice&id=".$usersArray['id']."\">$a_lang[SEND_INVOICE]</a>";
	} else { echo "n/a"; } ?>
    <br><a href="acc_user.php?id=<?=$usersArray['id'];?>">View History</a></td>
    <td align="center"> <? 
	if ($usersArray['active']==1) echo "$a_lang[ACTIVE]";
	else echo "$a_lang[INACTIVE]"; ?> </td> 
    <td align="center" nowrap>
	<a href="senduseremail.php?username=<?=$usersArray['username'];?>&email=<?=$usersArray['email'];?>"><?=$a_lang[EMAIL_THIS_USER];?></a><br>
	<? if ($usersArray['active']==1) echo "<a href=usersmanagement.php?option=chstatus&id=".$usersArray['id']."&value=0>$a_lang[SUSPEND_ACCOUNT]</a>";
	else echo "<a href=usersmanagement.php?option=chstatus&id=".$usersArray['id']."&value=1>$a_lang[ACTIVATE_ACCOUNT]</a>"; ?> 
      <br> 
      <a href="usersmanagement.php?option=delete&id=<?=$usersArray['id'];?>"> 
      <?=$a_lang[DELETE_ACCOUNT];?> 
    </a></td> 
  </tr> 
  <? } ?> 
</table> 
<table width="100%"  border="0" cellspacing="4" cellpadding="4"> 
  <tr> 
    <td align="center"><? paginate($start,$limit,$totalUsers,"acc_owe.php",$additionalVars."&orderField=$orderField&orderType=$orderType"); ?></td> 
  </tr> 
</table> 
<? 	include ("footer.php"); 
} ?>