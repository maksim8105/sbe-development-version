<?
## v5.20 -> may. 20, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1; 

if (isset($_POST['addnewsok'])) {
	if ($_POST['option']=="edit") {
		$updateNews = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_news SET
		title = '".remSpecialChars($_POST['title'])."',
		content = '".remSpecialChars($_POST['content'])."',
		date = '".$_POST['date']."',
		lang = '".$_POST['lang']."' WHERE id='".$_POST['id']."'");
		
	} else {
		$insertNews = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_news (title,content,date,active,lang) 
		VALUES ('".remSpecialChars($_POST['title'])."','".remSpecialChars($_POST['content'])."','".$_POST['date']."',1,'".$_POST['lang']."')");	
	}
}

if ($_GET['option']=="chstatus") {
	$changeStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_news SET 
	active='".$_GET['value']."' WHERE id='".$_GET['id']."'");
}

if ($_GET['option']=="delete") {
	$deleteNews = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_news WHERE id='".$_GET['id']."'");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[ADD_NEWS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[NEWS_TABLE_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
  <form action="newsmanagement.php" method="post"> 
    <tr class="c3"> 
      <td align="center" colspan="4"><b> 
        <?=$a_lang[SC_EDITNEWS_PAGES];?> 
        </b></td> 
    </tr> 
    <?  
	$getNews = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_news ORDER BY date DESC"); 
	$nbNews = mysqli_num_rows($getNews); ?> 
    <tr> 
      <td colspan="4" align="center"><b> 
        <?=$a_lang[THERE_ARE];?> 
        <?=$nbNews;?> 
        <?=$a_lang[ACTIVE_NEWS_DATA];?> 
        .</b></td> 
    </tr> 
    <tr> 
      <td colspan="4" align="center" class="c3"><b><a href="addnews.php"> 
        <?=$a_lang[ADD_NEWS];?>
        </a></b></td> 
    </tr> 
    <tr>  
    <tr class="c4"> 
      <td><?=$a_lang[NEWS_TITLE];?></td> 
	 <td align="center"><?=$a_lang[LANG];?> </td>
      <td width="120" align="center"><?=$a_lang[DATE_POSTED];?></td> 
      <td width="120" align="center"><?=$a_lang[OPERATIONS];?></td> 
    </tr> 
    <? while ($news=mysqli_fetch_array($getNews)) { ?> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td><a href="../displaynews.php?id=<?=$news['id'];?>"> 
        <?=$news['title'];?> 
        </a></td> 
	  <td align="center"><?=$news['lang'];?> </td>
      <td align="center"><? echo convertdate($news['date'],"M. d, Y");?></td> 
      <td align="center"><b> <? 
	  	echo "<a href=\"addnews.php?option=edit&id=".$news['id']."\">$a_lang[EDIT]</a><br>";
	  	if ($news['active']==1) { 
			echo "<a href=\"newsmanagement.php?option=chstatus&id=".$news['id']."&value=0\"> $a_lang[SUSPEND] </a>"; 
		} else { 
			echo "<a href=\"newsmanagement.php?option=chstatus&id=".$news['id']."&value=1\"> $a_lang[ACTIVATE]</a>"; 
		} ?> 
        <br> 
        <a href="newsmanagement.php?option=delete&id=<?=$news['id'];?>"> 
        <?=$a_lang[DELETE];?> 
        </a></b></td> 
    </tr> 
    <? } ?> 
   </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
