<?
## v5.24 -> apr. 06, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

if ($_REQUEST['parent']=="") $parent=0;

if (isset($_POST['generatorok'])) {
	$getAllCats = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT * FROM probid_categories ORDER BY parent ASC');
	while ($gencat = mysqli_fetch_array($getAllCats)) { 
		$isSubcat=0;
		$isSubcat = getSqlNumber("SELECT id FROM probid_categories WHERE parent='".$gencat['id']."'");
		if ($isSubcat>0) $updateCat = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET issubcat='>' WHERE id='".$gencat['id']."'");
	}
}

if (isset($_POST['savesettsok'])) {
	$_SESSION['cats']="1";
	$c=10000;
	$order = $_POST['order'];
	$name = $_POST['name'];
	$id = $_POST['id'];
	$delete = $_POST['delete'];
	$hidden = $_POST['hidden'];

	for ($i=0;$i<count($_POST['name']);$i++) {
		if ($order[$i]=="") {
			$order[$i] = $c; 
			$c++;
		}
		
		$updCatsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET 
		name='".remSpecialChars($name[$i])."', theorder='".$order[$i]."' 
		WHERE id='".$id[$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	
	$nullHidden = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET hidden=0 WHERE parent='".$_POST['parent']."'");
	for ($i=0;$i<count($_POST['hidden']);$i++) {
		$updHiddenCats = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET
		hidden='1' WHERE id='".$_POST['hidden'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}

	$isMainCat = ($_POST['parent']==0) ? "Y" : "N";

	if ($isMainCat == "Y") {
		$nullSpecialFees = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET custom_fees='N'");
		for ($i=0;$i<count($_POST['custom_fees']);$i++) {
			$updCustomFees = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET
			custom_fees='Y' WHERE id='".$_POST['custom_fees'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
		
		$getSpecialFeesCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent='0'");
		while ($specialFeesCats = mysqli_fetch_array($getSpecialFeesCats)) {
			### add the column in the probid_fees table
			## if the fee column is not already inserted, insert it, otherwise dont!
			$isFeeLine = getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$specialFeesCats['id']."'");
			if (!$isFeeLine&&$specialFeesCats['custom_fees']=="Y") {
				$insertFees = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_fees (category_id) VALUES
				('".$specialFeesCats['id']."')");
			} 
			//else if ($isFeeLine&&$specialFeesCats['custom_fees']=="N") {
			//	$removeFees = mysql_query("DELETE FROM probid_fees WHERE category_id='".$specialFeesCats['id']."'");
			//	$removeFeesTiers = mysql_query("DELETE FROM probid_fees WHERE category_id='".$specialFeesCats['id']."'");
			//}
		}
	
		$getSpCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent=0 AND custom_fees='N'");
		while ($spCat = mysqli_fetch_array($getSpCats)) {
			$delFees = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees WHERE category_id='".$spCat['id']."'");
			$delFeesTiers = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees_tiers WHERE category_id='".$spCat['id']."'");
		}
	}
	
	for ($i=0;$i<count($_POST['delete']);$i++) {
		### first delete any possible special fees that are for those categories
		$delFees = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees WHERE category_id='".$delete[$i]."'");
		$delFeesTiers = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fees_tiers WHERE category_id='".$delete[$i]."'");
		
		$delCatsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_categories 
		WHERE id='".$delete[$i]."'");
	}
	
	$searchSubsQuery = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent!=0");
	while ($subcatsArray = mysqli_fetch_array($searchSubsQuery)) {
		if (getSqlNumber("SELECT * FROM probid_categories WHERE id='".$subcatsArray['parent']."'")==0) {
			$deleterow=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_categories WHERE parent='".$subcatsArray['parent']."'");
		}
	}
	
	for ($i=0; $i<count($_POST['newname']); $i++) {	
		if ($_POST['newname'][$i]!="") {
			$c++;
			$insertCatQuery = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_categories 
			(parent, name, theorder) VALUES 
			('".$_REQUEST['parent']."','".remSpecialChars($_POST['newname'][$i])."','".$c."')");
		}
	}
	$savedSettings="yes";
}

include ("header.php"); ?>
<script language="javascript">
function popUp(URL) {
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=2,location=0,statusbar=1,menubar=0,resizable=0,width=550,height=395,left = 80,top = 80');");
} 
</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_tables.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[CATEGORIES]; echo " / "; echo $a_lang[EDIT_CAT];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<?=$a_lang[CONFLICT_CATS_MSG];?>
<br>
<br>
<div align="center"><strong><a href="table.conflictcats.php">
   <?=$a_lang[VIEW_CONFLICT_CATS];?>
   </a></strong></div>
<br>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[CATEGORY_UPDATED]."</p>":""; ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <input type="hidden" name="parent" value="<?=$_REQUEST['parent'];?>">
      <tr>
         <td align="center" class="c3"><b>
            <?=$a_lang[CATEGORY_SETT];?>
            </b></td>
      </tr>
      <tr valign="top">
         <td><?
				if($_REQUEST['parent'] > 0) {
					$croot = $_REQUEST['parent'];
					$nav = "";
					$cntr = 0;
					while ($croot>0) {
						$crw = getSqlRow("SELECT * FROM probid_categories WHERE id='$croot'");
						if($cntr == 0) {
							$nav = $crw['name'];
						} else {
							if($_REQUEST['parent'] != $croot) {
								$nav = "<a href=\"".$_SERVER['PHP_SELF']."?parent=".$crw['id']."&name=".$crw['name']."\">$crw[name]</a> > ".$nav;
							}
						}
						$cntr++;
						$croot = $crw['parent'];
					}
					echo "<A HREF=\"table.categories.php\"><b> $a_lang[CATEGORY]:</b></A> ".$nav;
				} ?>
         </td>
      </tr>
      <tr valign="top">
         <? $categoriesQuery=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories 
		  	WHERE parent='".$_REQUEST['parent']."' ORDER BY theorder ASC, name ASC"); ?>
         <td align="center"><table width="100%" border="0" cellpadding="4" cellspacing="2">
               <tr class="c4">
                  <td width="20">&nbsp;</td>
                  <td><?=$a_lang[NAME];?></td>
                  <td width="80" align="center"><?=$a_lang[ORDER_LIST];?></td>
                  <? if ($_REQUEST['parent']==0) { ?>
                  <td width="60" align="center"><?=$a_lang[SPECIAL_FEES];?></td>
                  <? } ?>
                  <td width="60" align="center"><?=$a_lang[HIDDEN];?></td>
                  <td width="60" align="center"><?=$a_lang[DELETE];?></td>
               </tr>
               <? while ($categoriesArray = mysqli_fetch_array($categoriesQuery)) { 
				  	## get conflicts
				  	$isConflict = getSqlNumber("SELECT id FROM probid_categories WHERE name LIKE '".$categoriesArray['name']."%' AND parent='".$_REQUEST['parent']."'");
				 	?>
               <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
                  <td><a href="<?=$_SERVER['PHP_SELF'];?>?parent=<?=$categoriesArray['id'];?>"><img src="images/catplus.gif" alt="<?=$a_lang[CATEGORY_SUBALT];?>" width="20" height="20" border="0"></a></td>
                  <td><input name="name[]" type="text" id="name[]" value="<?=$categoriesArray['name'];?>" style="width:65%">
                     <? echo ($categoriesArray['userid']>0) ? ' &nbsp; <strong>Owner ID:</strong> [ <a href="userdetails.php?id='.$categoriesArray['userid'].'"> '.$categoriesArray['userid'].'</a> ]' : ''; ?>
                     <input type="hidden" name="id[]" value="<?=$categoriesArray['id'];?>">
                     <? echo ($categoriesArray['parent']==0) ? 
			  				"<br>[ <a href=\"javascript:popUp('table.categories.options.php?catid=".$categoriesArray['id']."');\">".$a_lang[EDIT_OPTS_CAT]."</a> ] " : "";
			  				echo ($isConflict>1)?"<br>$a_lang[CONFLICT_CAT_WARNING]":""; ?> </td>
                  <? if ($categoriesArray['theorder']>=1000||$categoriesArray['theorder']==0) $categoriesArray['theorder']=""; ?>
                  <td align="center"><input name="order[]" type="text" id="order[]" value="<?=$categoriesArray['theorder'];?>" size="8"></td>
                  <? if ($_REQUEST['parent']==0) { ?>
                  <td align="center"><? if ($categoriesArray['userid']==0) { ?><input name="custom_fees[]" type="checkbox" id="custom_fees[]" value="<?=$categoriesArray['id'];?>" <? echo ($categoriesArray['custom_fees']=="Y")?"checked":"";?>><? } ?></td>
                  <? } ?>
                  <td align="center"><input name="hidden[]" type="checkbox" id="hidden[]" value="<?=$categoriesArray['id'];?>" <? echo ($categoriesArray['hidden']==1)?"checked":"";?>></td>
                  <td align="center"><input name="delete[]" type="checkbox" id="delete[]" value="<?=$categoriesArray['id'];?>"></td>
               </tr>
               <? } ?>
               <tr class="c4">
                  <td>&nbsp;</td>
                  <td><?=$a_lang[CATEGORY_ADD];?></td>
                  <td align="center">&nbsp;</td>
                  <? if ($_REQUEST['parent']==0) { ?>
                  <td align="center">&nbsp;</td>
                  <? } ?>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
               </tr>
               <tr class="c1">
                  <td>&nbsp;</td>
                  <td><?
							## addon so more categories can be added simultaneously
							$newAuctsCounter = 5;
							for ($i=0; $i<$newAuctsCounter; $i++) echo "<input name=\"newname[]\" type=\"text\" id=\"newname[]\"><br>\n"; ?></td>
                  <td align="center">&nbsp;</td>
                  <? if ($_REQUEST['parent']==0) { ?>
                  <td align="center">&nbsp;</td>
                  <? } ?>
                  <td align="center">&nbsp;</td>
                  <td align="center">&nbsp;</td>
               </tr>
            </table></td>
      </tr>
      <tr valign="top" class="c3">
         <td align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>">
            &nbsp; 
            &nbsp;
            <input name="generatorok" type="submit" value="<?=$a_lang[BUTT_CATS_GENERATOR];?>"></td>
      </tr>
      <tr valign="top" class="c1">
         <td><?=$a_lang[NOTE_CAT_GEN];?></td>
      </tr>
   </form>
</table>
<? include ("footer.php"); 
} ?>
