<?
## v5.24 -> apr. 04, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1; 

if (isset($_POST['savesettsok'])) {
	for ($i=0;$i<count($_POST['order']);$i++) {
		$updateFields = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_custom_rep SET 
		fieldorder='".$_POST['order'][$i]."'
		WHERE boxid='".$_POST['boxid'][$i]."'") or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
}

if ($_GET['option']=="chstatus") {
	$changeStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_custom_rep SET 
	active='".$_GET['value']."' WHERE boxid='".$_GET['id']."'");
	$changeStatusData = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_custom_rep_data SET 
	active='".$_GET['value']."' WHERE boxid='".$_GET['id']."'");
}

if ($_GET['option']=="delete") {
	$deleteFields = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_custom_rep WHERE boxid='".$_GET['id']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	$deleteFieldData= mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_custom_rep_data WHERE boxid='".$_GET['id']."'");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0" /></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1" /></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[MANAGE_CUST_REP_FLDS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif" /></td> 
   </tr> 
</table> 
<br /> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[FIELDS_TABLE_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
    <tr class="c3"> 
      <td align="center" colspan="4"><b> 
        <?=$a_lang[MANAGE_CUST_REP_FLDS];?> 
        </b></td> 
    </tr> 
   <tr> 
	<form action="custrepfldmanagement.php" method="post">
    <tbody>
       <tr class="c4"> 
      <td width="50" align="center"><?=$a_lang[BOXID]." / ".$a_lang[ORDER];?></td> 
	 <td align="center"><?=$a_lang[REP_FIELD_SAMPLE];?> </td>
      <td width="120" align="center"><?=$a_lang[OPERATIONS];?></td> 
    </tr> 
    <? 
	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, active,fieldorder, usertype FROM probid_custom_rep ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	$fldCnt = 0;
	while ($fields=mysqli_fetch_array($getFields)) { ?> 
	</tbody>
    <input type="hidden" name="boxid[]" value="<?=$fields['boxid'];?>" />
    <tbody>
       <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td align="center">[ <strong><?=$fields['boxid'];?></strong> ]<br />
         <input type="text" size="6" name="order[]" value="<?=$fields['fieldorder'];?>" /></td> 
	  <td>
	  <? echo "<table width=\"100%\" cellpadding=\"3\"><tr class=\"".(($count%2==0)?"c1":"c2")."\"><td nowrap>".$fields['boxname']."</td><td width=\"100%\">".createField($fields['boxid'],"",'probid_custom_rep')."</td></tr></table>"; ?>
	  <? echo "<br><strong>".$a_lang[APPLIES_TO]."</strong>: [ ".$fields['usertype']." ]"; ?>
	  </td>
      <td align="center"><b> <? 
	  	echo "<a href=\"editcustrepfld.php?option=edit&id=".$fields['boxid']."\">$a_lang[EDIT]</a><br>";
	  	if ($fields['active']==1) { 
			echo "<a href=\"custrepfldmanagement.php?option=chstatus&id=".$fields['boxid']."&value=0\"> $a_lang[SUSPEND] </a>"; 
		} else { 
			echo "<a href=\"custrepfldmanagement.php?option=chstatus&id=".$fields['boxid']."&value=1\"> $a_lang[ACTIVATE]</a>"; 
		} ?> 
        <br /> 
        <a href="custrepfldmanagement.php?option=delete&amp;id=<?=$fields['boxid'];?>"> 
        <?=$a_lang[DELETE];?> 
        </a></b></td> 
    </tr> 
    <? } ?> 
    </tbody>
    <tbody>
       <tr> 
      <td colspan="4" align="center" class="c3"><b><a href="editcustrepfld.php?option=add"> 
        <input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>" />
      </a></b></td> 
    </tr> 
	</tbody>
	</form>
    </tr>
    <tr> 
      <td colspan="4" align="center" class="c3"><b><a href="editcustrepfld.php?option=add"> 
        <?=$a_lang[ADD_CUST_REP_FIELD];?> 
        </a></b></td> 
    </tr> 
</table> 
<? include ("footer.php"); 
} ?>
