<? 
## v5.24 -> apr. 07, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

if (isset($_POST['savesettsok'])) { 
	$fileExtension = getFileExtension($_FILES['thefile']['name']);
	$imageName = "category_img_".$_POST['catid'].".".$fileExtension;
	$isUpload = uploadFile($_FILES['thefile']['tmp_name'],$imageName,"../uplimg/",TRUE,'../');
	if ($isUpload) {
		$updimg = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET 
		imagepath='uplimg/".$imageName."' WHERE id='".$_POST['catid']."'");
	}
	$updateCategory = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET 
	hover_title='".remSpecialChars($_POST['hover_title'])."',
	meta_description='".remSpecialChars($_POST['meta_description'])."',
	meta_keywords='".remSpecialChars($_POST['meta_keywords'])."'
	WHERE id='".$_POST['catid']."'");
}	

if ($_GET['option']=="delimg") {
	$imgName = getSqlField("SELECT imagepath FROM probid_categories WHERE id='".$_GET['catid']."'","imagepath");
	if (trim($imgName)!="") $delResult = deleteFile("../",$imgName);
	if ($delResult) mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_categories SET imagepath='' WHERE id='".$_GET['catid']."'");
}
include ("../config/lang/$setts[admin_lang]/admin.lang"); 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?=$a_lang[EDIT_CATS];?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<? 
if (isset($_POST['savesettsok'])) { 
	echo "<p align=\"center\" class=\"verdana12black\">$a_lang[SETTS_SAVED_SUCCESS]<br><br>
	[ <a href=\"javascript:window.close(this);\">$a_lang[CLOSE_WINDOW]</a> ]</p>";
} else {
	if ($delResult) echo "<p align=\"center\" class=\"verdana12black\">$a_lang[CAT_IMG_DELETED]<br></p>";

	$category = getSqlRow("SELECT * FROM probid_categories WHERE id = '".$_REQUEST['catid']."'"); 
?>
<table width="100%"  border="0" cellspacing="1" cellpadding="3">
<form action="<?=$_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="catid" value="<?=$_REQUEST['catid'];?>">
  <tr>
    <td colspan="2" class="c4"><? echo $a_lang[CATEGORY]." : ".$category['name']; ?></td>
    </tr>
  <tr class="c1">
    <td width="140" align="right">[ <strong><?=$a_lang[HOVER_TITLE];?></strong> ]<br><?=$a_lang[HOVER_TITLE_MSG];?></td>
    <td><input name="hover_title" type="text" id="hover_title" value="<?=$category['hover_title'];?>" size="45"></td>
  </tr>
  <tr class="c2">
    <td width="140" align="right">[ <strong><?=$a_lang[META_DESC];?></strong> ]<br><?=$a_lang[META_DESC_MSG];?> </td>
    <td><textarea name="meta_description" cols="45" rows="4" id="meta_description"><?=$category['meta_description'];?></textarea></td>
  </tr>
  <tr class="c1">
    <td width="140" align="right">[ <strong><?=$a_lang[META_KEYS];?></strong> ]<br><?=$a_lang[META_KEYS_MSG];?> </td>
    <td><textarea name="meta_keywords" cols="45" rows="4" id="meta_keywords"><?=$category['meta_keywords'];?></textarea></td>
  </tr>
  <tr class="c2">
    <td width="140" align="right">[ <strong><?=$a_lang[CAT_IMG];?></strong> ]<br><?=$a_lang[CAT_IMG_MSG];?></td>
    <td><input name="thefile" type="file" id="thefile"></td>
  </tr>
  <? if ($category['imagepath']!="") { ?>
  <tr class="c2">
    <td width="140" align="right">[ <strong>Current Image</strong> ]<br></td>
    <td><img src="<? echo "../".$category['imagepath'];?>?id=<?=rand(2,99999); ?>"><br><br>
	[ <a href="table.categories.options.php?catid=<?=$_REQUEST['catid'];?>&option=delimg"><?=$a_lang[DEL_CAT_IMG];?></a> ]</td>
  </tr>
  <? } ?>
  <tr align="center">
    <td colspan="2" class="c4"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td>
    </tr>
  </form>
</table>
<? } ?> 
</body>
</html>
<? } ?>