<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");
@include_once ("../config/lang/".$_SESSION['sess_lang']."/catsarray.php");

if ($_GET['option']=="approve_all") { 
	$approveAll = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET approved=1");
	$activateConfirmed = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET active=1 WHERE payment_status='confirmed' AND approved='1'");	
}
if (isset($_POST['savesettsok'])) { 
	$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_gen_setts SET 
	enable_auctions_approval='".$_POST['enable_auctions_approval']."'");
	if (!$updateInformation) echo "Database Error: ".((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false));
	$savedSettings = "yes";

	(string)$all_subcats = NULL;
	for ($i=0;$i<count($_POST['categories']);$i++) {
		$all_subcats .= $_POST['categories'][$i].',';
		$catname = getSqlField("SELECT name FROM probid_categories WHERE id='".$_POST['categories'][$i]."'","name");
		
		## get all subcats

		if ($catname!="") {
			while (list($cat_array_id, $cat_array_details)=each($cat_array)) {
				list($cat_array_name, $userid) = $cat_array_details;
				$strResult = strpos($cat_array_name,$catname);
				if (trim($strResult)=="0") $cat_id[$catcnt++] = $cat_array_id;
			}
			if (count($cat_id)>0) $all_subcats .= implode (",",$cat_id).',';
		}
	}		
	$updateApprovalCats = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_gen_setts SET approval_categories='".$all_subcats."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
}

include ("header.php"); ?>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">    <!--    
	function MoveOption(objSourceElement, objTargetElement) {        
		var aryTempSourceOptions = new Array();        
		var x = 0;                //looping through source element to find selected options        
		for (var i = 0; i < objSourceElement.length; i++) {            
			if (objSourceElement.options[i].selected) {                //need to move this option to target element                
				var intTargetLen = objTargetElement.length++;                
				objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;                
				objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;            
			} else {                //storing options that stay to recreate select element                
				var objTempValues = new Object();                
				objTempValues.text = objSourceElement.options[i].text;                
				objTempValues.value = objSourceElement.options[i].value;                
				aryTempSourceOptions[x] = objTempValues;                
				x++;            
			}        
		}                //resetting length of source        
		objSourceElement.length = aryTempSourceOptions.length;        //looping through temp array to recreate source select element        
		for (var i = 0; i < aryTempSourceOptions.length; i++) {            
			objSourceElement.options[i].text = aryTempSourceOptions[i].text;            
			objSourceElement.options[i].value = aryTempSourceOptions[i].value;            
			objSourceElement.options[i].selected = false;        
		}    
	}    //-->    
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript"> <!-- 
	function SelectOption(objTargetElement) { 	
		for (var i = 0; i < objTargetElement.length; i++) { 
			objTargetElement.options[i].selected = true; 
		} 
	} //--> 
</SCRIPT>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_genset.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? 
			echo $a_lang[ENA_DIS]." / "; 
			echo $a_lang[ENABLE_AUCTIONS_APPROVAL]; ?>
         &nbsp;&nbsp; </td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<? 
echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[SETT_CHANGED]."</p>":""; 
$genSetts = getSqlRow("SELECT * FROM probid_gen_setts");
?>
<table width="100%" border="0" cellpadding="4" cellspacing="2">
   <form method="post" action="<?=$_SERVER['PHP_SELF'];?>" name="MoveList" onSubmit="SelectOption(this.categories)">
      <tr align="center" class="c3">
         <td colspan="2"><b>
            <?=strtoupper($a_lang[ENABLE_AUCTIONS_APPROVAL]);?>
            </b></td>
      </tr>
      <tr class="c1">
         <td nowrap="nowrap"><?=$a_lang[ENA_AAPR_1];?></td>
         <td width="100%"><input type="radio" name="enable_auctions_approval" value="N" <? echo (@eregi('N', $genSetts['enable_auctions_approval']))?"checked":""; ?>>
            <?=$a_lang[ENA_AAPR_2];?> <br />
            <input type="radio" name="enable_auctions_approval" value="Y"  <? echo (@eregi('Y', $genSetts['enable_auctions_approval']))?"checked":""; ?>>
            <?=$a_lang[ENA_AAPR_3];?> </td>
      </tr>
      <? if (@eregi('N', $genSetts['enable_auctions_approval'])) { ?>
      <tr class="c2">
         <td nowrap="nowrap"><strong><?=$a_lang[ENA_AAPR_4];?></strong></td>
         <td>[ <a href="usersmanagement.php"><?=$a_lang[ENA_AAPR_5];?></a> ] </td>
      </tr>
      <tr class="c1">
         <td nowrap="nowrap"><strong><?=$a_lang[ENA_AAPR_6];?></strong></td>
         <td><table width="100%" border="0" cellspacing="2" cellpadding="2">
               <tr>
                  <td width="35%">[ <?=$a_lang[ENA_AAPR_7];?> ] </td>
                  <td width="10%">&nbsp;</td>
                  <td width="35%">[ <?=$a_lang[ENA_AAPR_8];?> ] </td>
               </tr>
               <? $approvedCats = (!empty($genSetts['approval_categories'])) ? substr($genSetts['approval_categories'],0,-1) : 0; ?>
               <tr>
                  <td><select name="store_categories" size="10" multiple="multiple" id="store_categories" style="width: 100%;">
                        <? $getcats=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent='0' AND id NOT IN (".$approvedCats.") ORDER BY theorder ASC, name ASC");
								while ($cat_row=mysqli_fetch_array($getcats)) {
									echo "<option value=\"".$cat_row['id']."\">".$cat_row['name']."</option>";
								} ?>
                     </select></td>
                  <td align="center"><input type="button" name="Disable" value=" -&gt; " style="width: 50px;" onclick="MoveOption(this.form.store_categories, this.form.categories)" />
                     <br />
                     <br />
                     <input type="button" name="Enable" value=" &lt;- " style="width: 50px;" onclick="MoveOption(this.form.categories, this.form.store_categories)" /></td>
                  <td><select name="categories[]" size="10" multiple="multiple" id="categories" style="width: 100%;">
                        <? $getcats=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent='0' AND id IN (".$approvedCats.") ORDER BY theorder ASC, name ASC");
								while ($cat_row=mysqli_fetch_array($getcats)) {
									echo "<option value=\"".$cat_row['id']."\" selected>".$cat_row['name']."</option>";
								} ?>
                     </select></td>
               </tr>
            </table></td>
      </tr>
      <? } ?>
      <tr class="c2">
         <td nowrap="nowrap"><strong><?=$a_lang[ENA_AAPR_9];?></strong></td>
         <td>[ <a href="auctionapproval.php?option=approve_all"><?=$a_lang[ENA_AAPR_10];?></a> ] </td>
      </tr>
      <tr align="center" class="c3">
         <td colspan="2"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td>
      </tr>
   </form>
</table>
<? include ("footer.php"); 
} ?>
