<?
## v5.23 -> dec. 13, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");
include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1;  

if (isset($_POST['savesettsok'])) {
	for ($i=0;$i<count($_POST['content']);$i++) {
		if (trim($_POST['content'][$i])=='n/a') { 
			mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_pages (pagename,content,lang) VALUES 
			('about','Please enter content','".$langlist[$i]."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		} else {
			mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_pages SET content='".remSpecialChars($_POST['content'][$i])."' WHERE 
			pagename='about' AND lang='".$langlist[$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		}
		$savedSettings="yes";
	}
	$updatePage = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_layout_setts SET is_about='".$_POST['is_about']."'");
}

include_once ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_ABOUT];?>&nbsp;&nbsp;</td>
    <td><img src="images/end_part.gif"></td>
  </tr>
</table>
<br>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[SETT_CHANGED]."</p>":""; ?>
<form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
  <table width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr class="c3">
      <td colspan="2" align="center"><b>
        <?=$a_lang[ABOUT_PAGE_SETTINGS];?>
        </b></td>
    </tr>
    <? $pageSettings=getSqlRow("SELECT * FROM probid_layout_setts"); ?>
    <tr>
      <td colspan="2"><?=$a_lang[ABOUT_PAGE_MESSAGE];?></td>
    </tr>
    <tr class="c1">
      <td width="150"><b>
        <?=$a_lang[ABOUT_ACTIVATE];?>
        </b></td>
      <td><input type="radio" name="is_about" value="Y" <? echo (($pageSettings['is_about']=="Y")?"checked":"");?>>
        <?=$a_lang[YES];?>
        <input type="radio" name="is_about" value="N" <? echo (($pageSettings['is_about']=="N")?"checked":"");?>>
        <?=$a_lang[NO];?>
      </td>
    </tr>
    <? for ($z=0; $z < $sizeofarray; $z++) { ?>
    <tr class="<? echo (($count++)%2==0) ? "c2":"c1"; ?>">
      <td valign="top"><b>
        <?=$a_lang[ENTER_CONTENT];?>
        :</b><br>
        <?=$a_lang[LANG]." : ".$langlist[$z]?></td>
      <td><textarea name="content[]" cols="45" rows="10" id="content<?=$z;?>"><? echo getSqlField("SELECT * FROM probid_pages WHERE pagename='about' AND lang='".$langlist[$z]."'","content");?></textarea>
        <script> 
			var oEdit<?=$z;?> = new InnovaEditor("oEdit<?=$z;?>");
			oEdit<?=$z;?>.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit<?=$z;?>.height=350;
			oEdit<?=$z;?>.REPLACE("content<?=$z;?>");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW];?></td>
    </tr>
    <? } ?>
    <tr class="c3">
      <td colspan="2" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td>
    </tr>
  </table>
</form>
<? 	include ("footer.php"); 
} ?>
