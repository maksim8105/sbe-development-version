<?
## v5.22 -> sep. 22, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if (isset($_POST['sendemailok'])) {
	$getInactiveUsers = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_users WHERE active=0");
	while ($userArray = mysqli_fetch_array($getInactiveUsers)) {
		$nbAuctions = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$userArray['id']."'");
		$nbBids = getSqlNumber("SELECT id FROM probid_bids WHERE bidderid='".$userArray['id']."'");
		$nbFeedbacks = getSqlNumber("SELECT userid FROM probid_feedbacks WHERE userid='".$userArray['id']."'");
		if ($nbAuctions==0&&$nbBids==0&&$nbFeedbacks==0) {
			## we have deleted the user
			if ($setts['account_mode']==1) {
				if ($fee['is_signup_fee']=="Y"&&$fee['val_signup_fee']>0) $regConfirm=0;
				else $regConfirm=1;
			} else if ($setts['account_mode']==2) $regConfirm=1;
			$recipientId = $userArray['id'];
			if ($regConfirm==1) {
				include ("../mails/register_confirm.php");
			} else {
				## this doesnt need to be included here
				## include ("mails/register.php");
			}				
		}
	}
	echo "<script>document.location='index.php'</script>";
} else if (isset($_POST['cancelok'])) {
	echo "<script>document.location='index.php'</script>";
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_user.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[ACTIVATION_EMAILS];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post"> 
    <tr class="c3"> 
      <td align="center"><b> 
        <?=$a_lang[ACTIVATION_EMAILS_TITLE];?> 
        </b></td> 
    </tr> 
    <tr align="center" class="c1"> 
      <td><?=$a_lang[ACTIVATION_EMAILS_MESSAGE];?></td> 
    </tr> 
    <tr class="c3"> 
      <td align="center"><input name="sendemailok" type="submit" id="sendemailok" value="<?=$a_lang[BUTT_PROCEED];?>"> 
&nbsp; 
        <input name="cancelok" type="submit" id="cancelok" value="<?=$a_lang[BUTT_CANCEL];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
