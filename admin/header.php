<? 
## v5.24 -> apr. 05, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

include ("../config/lang/$setts[admin_lang]/admin.lang"); 
@include_once ('../config/lang/'.$_SESSION['sess_lang'].'/catsarray.php');

$canView = TRUE;
if ($_SESSION['adminlevel']>1) {
	if (@eregi("usersmanagement.php",$_SERVER['PHP_SELF'])||@eregi("auctionsmanamgement.php",$_SERVER['PHP_SELF'])||
	@eregi("editauction.php",$_SERVER['PHP_SELF'])||@eregi("registerclient.php",$_SERVER['PHP_SELF'])||
	@eregi("senduseremail.php",$_SERVER['PHP_SELF'])||@eregi("userdetails.php",$_SERVER['PHP_SELF'])||
	@eregi("userbids.php",$_SERVER['PHP_SELF'])||@eregi("edituser.php",$_SERVER['PHP_SELF'])||
	@eregi("acc_user.php",$_SERVER['PHP_SELF'])||@eregi("index.php",$_SERVER['PHP_SELF'])) $canView = TRUE;
	else $canView = FALSE;
}

if (!$canView) echo "<script>document.location.href='index.php?viewmsg=1'</script>"; ?>
<html>
<head>
<title><? echo $setts['sitename']." ".$a_lang[ADMIN_AREA];?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<link href="style.css" rel="stylesheet" type="text/css">
<link href="../calendar.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="dtree.js"></script>
<script type="text/javascript" src="../my.js"></script>
<script type="text/javascript" src="main.js"></script>
<script language=JavaScript src='../scripts/innovaeditor.js'></script>
</head><body leftmargin="0" topmargin="0" vlink="#003399"> 
<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr height="4"> 
    <td bgcolor="#F4F6F8"><img src="images/pixel.gif" width="1" height="4"></td> 
  </tr> 
  <tr height="1"> 
    <td bgcolor="#93A5BC"><img src="images/pixel.gif" width="1" height="1"></td> 
  </tr> 
  <tr height="1"> 
    <td bgcolor="#F4F6F8"><img src="images/pixel.gif" width="1" height="1"></td> 
  </tr> 
</table> 
<table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/bgheader.gif"> 
  <tr> 
    <td width="231" height="67"><img src="images/probidlogo.gif" width="231" height="67"></td> 
    <td> <table border="0" cellpadding="0" cellspacing="0"> 
        <tr> 
          <td colspan=3><img src="images/afterlogo.gif"></td> 
        </tr> 
        <tr> 
          <td><a href="index.php"><img src="images/b_admin.gif" border="0"></a></td> 
          <td><a href="../index.php" target="_blank"><img src="images/b_site.gif" border="0"></a></td> 
          <td><a href="index.php?option=logout"><img src="images/b_logout.gif" border="0"></a></td> 
        </tr> 
      </table></td> 
  </tr> 
  <tr height="15"> 
    <td bgcolor="#4192DE" height="20" colspan="2">&nbsp;&nbsp;<strong>Current Version:</strong> v<?=$currentVersion;?> </td> 
  </tr> 
  <tr height="1"> 
    <td bgcolor="#2D6498" height="1" colspan="2"><img src="images/pixel.gif" width="1" height="1"></td> 
  </tr> 
  <tr height="5"> 
    <td bgcolor="#FFFFFF" height="5" colspan="2"><img src="images/pixel.gif" width="1" height="5"></td> 
  </tr> 
</table> 
<? if ($_SESSION['cats']=="1"){?>
<table width="100%" bgcolor="red"><tr><td align="center"><font size="+2" color="#ffffff">You have made changes to your category structure, please update those changes by clicking <a href="updatecats.php"><font size="+2" color="yellow">HERE</font></a> when you have finished</font></td></tr></table>
<? } ?>
 <table width="100%" border="0" cellspacing="4" cellpadding="0"> 
 <? $numauctions=getSqlNumber("SELECT * FROM probid_auctions WHERE closed=0"); ?> 
  <tr> 
     <td width="200" valign="top"><? 
	 if (@eregi("index.php",$_SERVER['PHP_SELF'])) include_once ("status.php"); 
	 else include_once ("leftmenu.php");
	 ?></td> 
     <td width="2" background="images/vline.gif"><img src="images/pixel.gif" height="1" width="2"></td> 
     <td  valign="top"> 