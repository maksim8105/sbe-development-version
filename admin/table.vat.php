<?
## v5.24 -> apr. 06, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

$users_vat = array(
	'a' => 'a Business with VAT registration number specified',
   'b' => 'a Business without VAT registration number specified',
   'c' => 'an Individual with VAT registration number specified',
   'd' => 'an Individual without VAT registration number specified'
);

if (isset($_POST['savesettsok'])) {
	if (count($_POST['delete'])>0) {
		for ($i=0;$i<count($_POST['delete']);$i++) {
			$deleteCurrency[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_vat_setts
			                                    WHERE id='".$_POST['delete'][$i]."'");
            mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_vat_rates WHERE vat_id='".$_POST['delete'][$i]."'");
		}
	}
	$savedSettings="yes";
	$arrangeCurrencies=mysqli_query($GLOBALS["___mysqli_ston"], "ALTER TABLE probid_vat_setts ORDER BY symbol");
}

if (isset($_POST['update'])) {
	if(isset($_POST['countries'])) $tmp_countries = implode(',',$_POST['countries']);
   
	if(isset($_POST['users1'])) $tmp_users1 = implode(',',@$_POST['users1']);
	else $tmp_users1 = '';
	
	if(isset($_POST['users2'])) $tmp_users2 = implode(',',@$_POST['users2']);
   else $tmp_users2 = '';

   $tmp_amount = (double)$_POST['amount'];
   $tmp_symbol = preg_replace('/[^\\w]+/','',$_POST['symbol']);

   if( $tmp_countries && $tmp_amount && ($tmp_amount!=0) && $tmp_symbol ) {
		if(isset($_POST['editid'])){
      	$old_countries=getSqlField("SELECT countries FROM probid_vat_setts WHERE id = '".$_POST['editid']."'",'countries');
         $old_amount=getSqlField("SELECT amount FROM probid_vat_setts WHERE id = '".$_POST['editid']."'",'amount');
         mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_vat_setts SET symbol = '".$tmp_symbol."',
			amount = '".$tmp_amount."', countries = '".$tmp_countries."', users_sale_vat = '".$tmp_users1."',
			users_no_vat = '".$tmp_users2."' WHERE id = '".$_POST['editid']."'");
			
			if($old_amount!=$tmp_amount) {
				mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_vat_rates WHERE vat_id='".$_POST['editid']."'");
            foreach($_POST['countries'] as $v){
            	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_vat_rates(vat_id,country,rate) VALUES 
					('".$_POST['editid']."','".$v."','".$tmp_amount."')");
				}
			} elseif($old_countries!=$tmp_countries){
         	$old_countries_array = explode(',',$old_countries);
            $tmp_countries_array = explode(',',$tmp_countries);
            	
				foreach($old_countries_array as $v){
            	if(!in_array($v,$tmp_countries_array)){
               	mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_vat_rates WHERE vat_id='".$_POST['editid']."' AND country='".$v."'");
					}
				}
            foreach($tmp_countries_array as $v){
            	if(!in_array($v,$old_countries_array)){
               	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_vat_rates(vat_id,country,rate) VALUES
						('".$_POST['editid']."','".$v."','".$tmp_amount."')");
					}
				}
			}
		} else {
      	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_vat_setts(symbol,amount,countries,users_sale_vat,users_no_vat) VALUES 
			('".$tmp_symbol."','".$tmp_amount."','".$tmp_countries."','".$tmp_users1."','".$tmp_users2."')");
			$vat_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
         foreach($_POST['countries'] as $v){
         	mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_vat_rates(vat_id,country,rate) VALUES 
				('".$vat_id."','".$v."','".$tmp_amount."')");
			}
		}
	}
	header("Location: table.vat.php");
}

if(isset($_POST['updatevatrates'])){
	if(is_array($_POST['rates'])){
   	$savedSettings="yes";
      foreach($_POST['rates'] as $k=>$v){
      	mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_vat_rates SET rate='".$v."' WHERE id='".$k."'");
		}
	}
}

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_fees.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[EDIT_VAT_SETTS];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<? if(!isset($_GET['action'])):?>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[VATSETTS_UPDATED]."</p>":""; ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post">
      <tr class="c3">
         <td colspan="5" align="center"><b>
            <?=$a_lang[EDIT_VATSETTS];?>
            </b></td>
      </tr>
      <!-- HEADERS -->
      <tr class="c4">
         <td><?=$a_lang[TAX_NAME];?></td>
         <td><?=$a_lang[RATE].',%';?></td>
         <td><?=$a_lang[COUNTRIES];?></td>
         <td align="center"><?=$a_lang[EDIT];?></td>
         <td align="center"><?=$a_lang[DELETE];?></td>
      </tr>
      <!-- HEADERS -->
      <!-- CONTENT ROWS -->
      <? $getvat=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_vat_setts");

    	while ($row=mysqli_fetch_array($getvat)) {
        	$countries = explode(',',$row['countries']);
        	$countries_string = '';
        	$getcountries = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries");
        	while($country = mysqli_fetch_array($getcountries)) {
         	if(in_array($country['id'],$countries))$countries_string .= $country['name'].', ';
        	}
        	$countries_string = rtrim(trim($countries_string),',');

			/*
        	$users1_string = '';
        	$users1 = explode(',',$row['users_sale_vat']);
        	foreach($users1 as $v){
         	$users1_string .= $users_vat[$v].'<br>';
        	}

        	$users2_string = '';
        	$users2 = explode(',',$row['users_no_vat']);
        	foreach($users2 as $v){
         	$users2_string .= $users_vat[$v].'<br>';
        	}
        	*/ ?>
      <input type="hidden" name="id[]" value="<?=$row['id'];?>">
      <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
         <td><?=$row['symbol'];?></td>
         <td align="right"><?=$row['amount'];?></td>
         <td><?=$countries_string;?>
            <br>
            <a href="<?=$_SERVER['PHP_SELF'];?>?action=edittax&id=<?=$row['id'];?>">
            <?=$a_lang[CUSTOM_TAX_RATES];?>
            </a></td>
         <td align="center"><a href="<?=$_SERVER['PHP_SELF']; ?>?action=edit&id=<?=$row['id'];?>">
            <?=$a_lang[EDIT];?>
            </a></td>
         <td align="center"><input type="checkbox" name="delete[]" value="<?=$row['id'];?>"></td>
      </tr>
      <? } ?>
      <!-- CONTENT ROWS -->
      <!-- FOOTER -->
      <tr>
         <td colspan="5" align="center"><a href="<?=$_SERVER['PHP_SELF']; ?>?action=add">
            <?=$a_lang[ADD];?>
            </a></td>
      </tr>
      <tr class="c3">
         <td colspan="5" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SUBMIT];?>"></td>
      </tr>
      <!-- FOOTER -->
   </form>
</table>
<? elseif( ($_GET['action']=='add')||($_GET['action']=='edit') ):?>
<!-- ADD/EDIT VAT FORM -->
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post">
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[ADD_NEW_VATSETTS];?>
            </b></td>
      </tr>
      <? if(isset($_GET['id'])):?>
      <?
		$id = (int)$_GET['id'];
      $vatrow = getSqlRow("SELECT * FROM probid_vat_setts WHERE id='".$id."'"); ?>
      <input type="hidden" name="editid" value="<?=$id;?>">
      <? endif?>
      <tr class="c1">
         <td><?=$a_lang[TAX_NAME];?></td>
         <td><input type="text" name="symbol" value="<?=$vatrow['symbol'];?>"></td>
      </tr>
      <tr class="c2">
         <td><?=$a_lang[RATE].',%';?></td>
         <td><input type="text" name="amount" value="<?=$vatrow['amount'];?>"></td>
      </tr>
      <tr class="c1">
         <td valign="top"><?=$a_lang[COUNTRIES];?></td>
         <td><?
				$countries = explode(',',$vatrow['countries']);
            $countries_block = '<select name="countries[]" multiple style="height:300px;">';
            $getcountries = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries");
            while($country = mysqli_fetch_array($getcountries)) {
            	if(in_array($country['id'],$countries)){
               	$selected = ' selected';
					} else {
               	$selected = '';
					}
               $countries_block .= '<option value="'.$country['id'].'"'.$selected.'>'.$country['name']."\n";
				}
            $countries_block .= '</select>';
            echo $countries_block; ?>
         </td>
      </tr>
      <tr class="c2">
         <td valign="top"><?=$a_lang[USERS1];?></td>
         <td><?
				$users1 = explode(',',$vatrow['users_sale_vat']);
            $users1_block = '';
            foreach($users_vat as $k=>$v) {
            	if(in_array($k,$users1)){
               	$checked = ' checked';
					} else {
               	$checked = '';
					}
               $users1_block .= '<input type="checkbox" name="users1[]" value="'.$k.'"'.$checked.'><span class="valign">'.$v."</span><br>\n";
				}
            echo $users1_block; ?>
         </td>
      </tr>
      <tr class="c1">
         <td valign="top"><?=$a_lang[USERS2];?></td>
         <td><?
				$users2 = explode(',',$vatrow['users_no_vat']);
				$users2_block = '';
				foreach($users_vat as $k=>$v) {
            	if(in_array($k,$users2)){
               	$checked = ' checked';
					} else {
               	$checked = '';
					}
               $users2_block .= '<input type="checkbox" name="users2[]" value="'.$k.'"'.$checked.'>'.$v."<br>\n";
				}
            echo $users2_block; ?>
         </td>
      </tr>
      <tr class="c3">
         <td colspan="2" align="center"><input type="submit" name="update" value="<?=$a_lang[BUTT_SAVE];?>"></td>
      </tr>
   </form>
</table>
<!-- ADD/EDIT VAT FORM -->
<? elseif($_GET['action']=='edittax'):?>
<!-- EDIT TAX RATES FORM -->
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
      <tr class="c3">
         <td colspan="2" align="center"><b>
            <?=$a_lang[EDIT_VAT_RATES];?>
            </b></td>
      </tr>
      <tr class="c4">
         <td><?=$a_lang[COUNTRY];?></td>
         <td><?=$a_lang[RATE].',%';?></td>
      </tr>
      <?
		$vat_id = (int)$_GET['id'];
		$get_vat_rates = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_vat_rates WHERE vat_id = '".$vat_id."'"); ?>
      <input type="hidden" name="vat_id" value="<?=$vat_id;?>">
      <? while($vat_rate = mysqli_fetch_array($get_vat_rates)):?>
      <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
         <td><?=getSqlField("SELECT name FROM probid_countries WHERE id='".$vat_rate['country']."'",'name');?></td>
         <td><input type="text" name="rates[<?=$vat_rate['id'];?>]" value="<?=$vat_rate['rate'];?>"></td>
      </tr>
      <? endwhile;?>
      <tr class="c3">
         <td colspan="2" align="center"><input type="submit" name="updatevatrates" value="<?=$a_lang[BUTT_SAVE];?>"></td>
      </tr>
   </form>
</table>
<!-- EDIT TAX RATES FORM -->
<? endif?>
<? include ("footer.php");
} ?>
