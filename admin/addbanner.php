<?
## v5.24 -> apr. 04, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("header.php"); ?>
<script language="JavaScript">
function thebannertype(theform) {
	if (theform.bannertype.value=="code") {
		theform.name.readOnly=true;
		theform.company.readOnly=true;
		theform.email.readOnly=true;
		theform.url.readOnly=true;
		
		theform.file.readOnly=true;
		theform.textunder.readOnly=true;
		theform.alttext.readOnly=true;
		theform.views_p.readOnly=true;
		theform.clicks_p.readOnly=true;
		theform.bannercode.readOnly=false;
	} else {	
		theform.name.readOnly=false;
		theform.company.readOnly=false;
		theform.email.readOnly=false;
		theform.url.readOnly=false;
		theform.file.readOnly=false;
		theform.textunder.readOnly=false;
		theform.alttext.readOnly=false;
		theform.views_p.readOnly=false;
		theform.clicks_p.readOnly=false;
		theform.bannercode.readOnly=true;
	}
}
</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[BANNER_AD_CONTROL];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<? 
if ($_REQUEST['option']=="edit") {
	$advert = getSqlRow("SELECT * FROM probid_adverts WHERE id='".$_REQUEST['id']."'");
}
?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="advertsmanagement.php" method="post" enctype="multipart/form-data" name="addbanner">
      <input type="hidden" name="option" value="<?=$_REQUEST['option'];?>">
      <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
      <tr class="c3">
         <td align="center" colspan="2"><b>
            <?=$a_lang[ADD_NEW_BANNER];?>
            </b></td>
      </tr>
      <tr>
         <td colspan="2"><?=$a_lang[BANNER_MESSAGE1];?></td>
      </tr>
      <tr class="c1">
         <td width="120"><b>
            <?=$a_lang[BANNER_TYPE];?>
            :</b></td>
         <td><select name="bannertype" id="bannertype" onChange="thebannertype(addbanner);">
               <option value="custom" selected>
               <?=$a_lang[SELECT_BANNER_CUSTOM];?>
               </option>
               <option value="code" <? echo ($advert['type']=="code") ? "selected" : ""; ?>>
               <?=$a_lang[SELECT_BANNER_CODE];?>
               </option>
            </select></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[USER_NAME];?>
            :</b></td>
         <td><input name="name" type="text" id="name" value="<?=$advert['name'];?>"></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[COMPANY];?>
            :</b></td>
         <td><input name="company" type="text" id="company" value="<?=$advert['company'];?>"></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[EMAIL];?>
            :</b></td>
         <td><input name="email" type="text" id="email" value="<?=$advert['email'];?>"></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[URL];?>
            :</b></td>
         <td><input name="url" type="text" id="url" value="<?=$advert['url'];?>">
            <br>
            <?=$a_lang[BANNER_MESSAGE2];?></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[BANNER_IMG];?>
            :</b></td>
         <td><input type="file" name="file"></td>
      </tr>
      <? if ($_REQUEST['option']=="edit"&&$advert['imgpath']!="") { ?>
      <tr class="c2">
         <td><b>
            <?=$a_lang[CURRENT_BANNER];?>
            :</b></td>
         <td><img src="../<?=$advert['imgpath'];?>" alt="<?=$advert['alttext'];?>"></td>
      </tr>
      <? } ?>
      <tr class="c1">
         <td><b>
            <?=$a_lang[BANNER_UNDER];?>
            :</b></td>
         <td><input name="textunder" type="text" id="textunder" value="<?=$advert['textunder'];?>">
            <br>
            <?=$a_lang[BANNER_MESSAGE3];?></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[BANNER_ALT];?>
            :</b></td>
         <td><input name="alttext" type="text" id="alttext" value="<?=$advert['alttext'];?>">
            <br>
            <?=$a_lang[BANNER_MESSAGE3];?></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[VIEWS_PURCHASED];?>
            :</b></td>
         <td><input name="views_p" type="text" id="views_p" size="8" value="<?=$advert['views_p'];?>">
            <br>
            <?=$a_lang[BANNER_MESSAGE4];?></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[CLICKS_PURCHASED];?>
            :</b></td>
         <td><input name="clicks_p" type="text" id="clicks_p" size="8" value="<?=$advert['clicks_p'];?>">
            <br>
            <?=$a_lang[BANNER_MESSAGE5];?></td>
      </tr>
      <? reset($cat_array); ?>
      <tr class="c1">
         <td><b>
            <?=$a_lang[CATEGORIES];?>
            :</b></td>
         <td><select name="categories[]" size="10" multiple id="categories[]">
               <? while (list($catid, $cat_array_details) = each ($cat_array)) {
						list($catname, $userid) = $cat_array_details;						
						if ($userid==0) echo "<option value=\"".$catname."\" ".((@eregi($catname,$advert['cat_filter']))?"selected":"").">".$catname."</option>";
					} ?>
            </select>
            <br>
            <?=$a_lang[BANNER_MESSAGE6];?></td>
      </tr>
      <tr class="c2">
         <td><b>
            <?=$a_lang[KEYWORDS];?>
            :</b></td>
         <td><textarea name="keywords" rows="4" id="keywords"><?=$advert['keyword_filter'];?></textarea>
            <br>
            <?=$a_lang[BANNER_MESSAGE7];?></td>
      </tr>
      <tr class="c1">
         <td><b>
            <?=$a_lang[BANNER_CODE];?>
            :</b></td>
         <td><textarea name="bannercode" cols="40" rows="6" <? echo ($advert['type']!="code") ? "readonly" : ""; ?> id="bannercode"><?=$advert['banner_code'];?></textarea>
            <br>
            <?=$a_lang[BANNER_MESSAGE8];?></td>
      </tr>
      <tr class="c3">
         <td>&nbsp;</td>
         <td><input name="addbannerok" type="submit" id="addbannerok" value="<? echo ($_REQUEST['option']=="edit") ? $a_land[BUTT_EDIT_BANNER] : $a_lang[BUTT_ADD_BANNER];?>"></td>
      </tr>
   </form>
</table>
<? include ("footer.php"); 
} ?>
