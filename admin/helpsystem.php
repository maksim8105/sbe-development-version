<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");
include ("header.php");

if (!($_POST["mylang"])) $_POST["mylang"] = "b"; // RUS default
if (($_POST["act"]=="update")) $_POST["action"] = "cat_update"; // Some fix for update :)

// Action

switch ($_POST["action"]) 
{
  case "cat_insert":
    mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_help_categories(name,lang) VALUES('".$_POST["cat_name"]."','".$_POST["mylang"]."')");
    break;
  case "delete":
    if (is_array($_POST["cats"]) AND sizeof($_POST["cats"])>0) 
    {
      foreach ($_POST["cats"] as $row) 
      {
        $counter++;
        $ids.=$row.($counter<count($_POST["cats"])?',':'');    
      }
         $query = "DELETE FROM probid_help_categories WHERE id IN(".$ids.")";
         mysqli_query($GLOBALS["___mysqli_ston"], $query);
         $delete_themes = "DELETE FROM probid_help_themes WHERE catid IN(".$ids.")";
         mysqli_query($GLOBALS["___mysqli_ston"], $delete_themes);
    } 
  	break;
  case "cat_update":
    $query = "UPDATE probid_help_categories SET name='".$_POST["name"]."', pos=".$_POST["pos"]." WHERE id=".$_POST["cid"]."";
    mysqli_query($GLOBALS["___mysqli_ston"], $query);
    break;
  case "top_insert":
    $query = "INSERT INTO probid_help_themes(title,content,catid,lang,pos,words,description) VALUES('".$_POST["top_title"]."','".addSpecialChars($_POST["content"])."',".$_POST["catbox"].",'".$_POST["mylang"]."',".$_POST["top_pos"].",'".$_POST["words"]."','".$_POST['description']."')";
    mysqli_query($GLOBALS["___mysqli_ston"], $query);
    break;
  case "delete_topic":
    if (is_array($_POST["tops"]) AND sizeof($_POST["tops"])>0) 
    {
      foreach ($_POST["tops"] as $row) 
      {
        $counter++;
        $ids.=$row.($counter<count($_POST["tops"])?',':'');    
      }
         $query = "DELETE FROM probid_help_themes WHERE id IN(".$ids.")";
         mysqli_query($GLOBALS["___mysqli_ston"], $query);
    } 
  	break;
  case "top_update":
    $query = "UPDATE probid_help_themes SET description='".$_POST['description']."', words='".$_POST["words"]."', catid=".$_POST["catbox"].",title='".$_POST["top_title"]."',pos=".$_POST["top_pos"].",content='".$_POST["content"]."' WHERE id=".$_POST["topid"]."";
    mysqli_query($GLOBALS["___mysqli_ston"], $query);
    break;

}


?>
<form name="selRoot" method="post" action="helpsystem.php">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;"> 
  <tr> 
    <td width="200">
        SELECT ROOT LANGUAGE
    </td>
    <td width="200">
        <?=myLang($_POST["mylang"])?>
    </td>
    <td>
        <input type="submit" value="OK">
    </td>
  </tr>
</table>
</form>

<form name="addCat" method="post" action="helpsystem.php">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;"> 
<tr>
    <td><h2>Add Category</h2></td>
</tr>
  <tr>
      <td colspan="2">Category name</td>
  </tr>
  <tr>
      <td width="200"><input type="text" size="100" value="" name="cat_name"></td>
      <td><input type="submit" value="Add"><input type="hidden" name="action" value="cat_insert"><input type="hidden" value="<?=$_POST["mylang"]?>" name="mylang"></td>
  </tr>
 </table> 
</form>

<form name="dataForm" method="post" action="helpsystem.php">
<table border="0" cellpadding="0" cellspacing="0" style="width:70%;"> 
<tr>
    <td colspan="4"><h2>List of Categories</h2></td>
</tr>
<tr>
    <td width="5%">#</td>
    <td width="80%">name</td>
    <td width="5%">position</td>
    <td width="10%"><input type="checkbox" onclick="javascript:checker()" title="Check all" name="master_box"/></td>
</tr>
<?

// GET categories list

  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_help_categories WHERE lang='".$_POST["mylang"]."'");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $arrCategories[] = $row; 
  }
  
  if ((is_array($arrCategories)) && (sizeof($arrCategories)>0))
  {
    // list
      foreach ($arrCategories as $row) 
      {
              $counter++;
      if (($row['id'] == $_POST['cats'][0]) && ($_POST['action']!=='delete'))  {
        ?><tr>
        <td><?=$counter?></td>
        <td><input class="edit" type="text" size="100" value="<?=$row['name']?>" name="name"></td>
        <td><input class="edit" type="text" size="20" value="<?=$row['pos']?>" name="pos"></td>
        <td><input type="submit" value="UPDATE" name="button"><input type="hidden" value="<?=$row['id']?>" name="cid"><input type="hidden" value="update" name="act"></td>
        </tr><?        
      
      } else {     
        ?><tr>
        <td><?=$counter?></td>
        <td><?=$row['name']?></td>
        <td align="center"><?=$row['pos']?></td>
        <td><input type="checkbox" value="<?=$row['id']?>" name="cats[]"/></td>
        </tr><?
      }
      }
  }

?>
</table>
<table style="width:70%;" cellpadding="0">
<tr>
    <td align="right">Action:
        <select name="action" class="action">
          <option value="edit">Edit</option>
          <option value="delete">Delete</option>
        </select>
        <input type="hidden" value="<?=$_POST['mylang']?>" name="mylang">
        <input class="buttons" value="OK" type="button" onclick="dataForm.submit();">
  </td>
</tr>
</table>
</form>
<?

function catList($arr,$selected)
{
  if (is_array($arr))
  {
    $select = "<select name='catbox'>";
    foreach ($arr as $row)
    {
      $select .= "<option ".($selected==$row["id"]?'selected':'')." value='".$row["id"]."'>".$row["name"];
    }
    $select .= "</select>";
    return $select;
  }
  return "Empty categories list. Please add category";
}

// Topics List

  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_help_themes WHERE lang='".$_POST["mylang"]."' ORDER BY catid ASC");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $arrTopcis[] = $row; 
  }


?>


<form name="topicForm" method="post" action="helpsystem.php">
<table border="0" cellpadding="0" cellspacing="0" style="width:70%;"> 
<tr>
    <td colspan="4"><h2>List of Topics</h2></td>
</tr>
<tr>
    <td width="5%">#</td>
    <td width="65%">title</td>
    <td width="20%">Category</td>
    <td width="5%">position</td>
    <td width="5%"><input type="checkbox" onclick="javascript:t_checker()" title="Check all" name="mbox"/></td>
</tr>
<?

// GET Topics List 

  
  if ((is_array($arrTopcis)) && (sizeof($arrTopcis)>0))
  {
    // list
    $counter = 0;
      foreach ($arrTopcis as $row) 
      {
              $counter++;
      if (($row['id'] == $_POST['tops'][0]) && ($_POST['action']!=='delete'))  {
      
        $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_help_themes WHERE id=".$_POST['tops'][0]."");
        while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
          $topData[] = $row; 
        }  
        $act_name = "top_update";  



      } else {     
        ?><tr>
        <td><?=$counter?></td>
        <td><?=$row['title']?></td>
        <td><?=catList($arrCategories,$row["catid"])?></td>
        <td align="center"><?=$row['pos']?></td>
        <td><input type="checkbox" value="<?=$row['id']?>" name="tops[]"/></td>
        </tr><?
      }
      }
  }

if (!$act_name) 
{
  $act_name="top_insert";
  $list = $_POST["catbox"];
} else {
  $list = $topData[0]["catid"];
}

?>
</table>
<table style="width:70%;" cellpadding="0">
<tr>
    <td align="right">Action:
        <select name="action" class="action">
          <option value="edit_topic">Edit</option>
          <option value="delete_topic">Delete</option>
        </select>
        <input type="hidden" value="<?=$_POST['mylang']?>" name="mylang">
        <input class="buttons" value="OK" type="button" onclick="topicForm.submit();">
  </td>
</tr>
</table>
</form>



<form name="addTopic" method="post" action="helpsystem.php">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
<tr>
    <td><h2>Add Topic</h2></td>
</tr>
<tr>
    <td>Select Category</td>
</tr>
<tr>
    <td><?=catList($arrCategories,$list)?></td>
</tr>
<tr>
    <td>Topic Theme</td>
</tr>
<tr>
    <td><input type="text" size="100" value="<?=$topData[0]["title"]?>" name="top_title"></td>
</tr>
<tr>
    <td>Topic pos:<input type="text" size="5" value="<?=$topData[0]["pos"]?>" name="top_pos"></td>
</tr>
<tr>
    <td>Topic Content</td>
</tr>
  <tr>
      <td><textarea name="content" cols="45" rows="10" id="content"><?=$topData[0]["content"]?></textarea>
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="80%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW];?></td>
  </tr>
<tr>
     <td>
     <b>Keywords</b>
     </td>
</tr>
<tr>
     <td>
     <textarea name="words" rows="10" cols="100"><?=$topData[0]['words']?></textarea>
     </td>
</tr>
<tr>
     <td>
     <b>Description</b>
     </td>
</tr>
<tr>
     <td>
     <textarea name="description" rows="10" cols="100"><?=$topData[0]['description']?></textarea>
     </td>
</tr>
<tr>
    <td><input type="submit" value="Save"><input type="hidden" value="<?=$topData[0]["id"]?>" name="topid"><input type="hidden" value="<?=$_POST['mylang']?>" name="mylang"><input type="hidden" name="action" value="<?=$act_name?>"></td>
</tr>
</table>
</form>

<?
    



include ("footer.php"); 
} 
?>