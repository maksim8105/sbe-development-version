<?
## v5.24 -> may. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	for ($i=0;$i<count($_POST['id']);$i++) {
		$UpdateBlockedUser[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_blocked_users SET 
		block_reason='".remSpecialChars($_POST['block_reason'][$i])."' WHERE
		id='".$_POST['id'][$i]."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	
	for ($i=0;$i<count($_POST['delete']);$i++) {
		$deleteBlockedUser[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_blocked_users 
		WHERE id='".$_POST['delete'][$i]."'");
	}
	$savedSettings="yes";

}
include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_tables.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[BLOCKED_USERS];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[BLOCKED_USERS_UPDATED]."</p>":""; ?>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
   <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post">
      <tr class="c3">
         <td colspan="3" align="center"><b>
            <?=$a_lang[BLOCKED_USERS];?>
            </b></td>
      </tr>
      <tr class="c4">
         <td width="80"><?=$a_lang[BLOCKERBLOCKED];?></td>
         <td><?=$a_lang[BLOCK_DESCRIPTION];?></td>
         <td width="80" align="center"><?=$a_lang[DELETE];?></td>
      </tr>
      <? $getBlocked=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT b.*, u.username AS owner_username FROM probid_blocked_users b, probid_users u WHERE b.ownerid=u.id ORDER BY id DESC"); 
		while ($blocked=mysqli_fetch_array($getBlocked)) { ?>
		<input type="hidden" name="id[]" value="<?=$blocked['id'];?>">
      <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>">
         <td><strong><?=$a_lang[BLOCKER];?>:</strong> <?=$blocked['owner_username'];?><br />
				<strong><?=$a_lang[BLOCKED_USR];?>:</strong> <?=$blocked['username'];?></td>
         <td><textarea name="block_reason[]" rows="6" cols="60"><?=$blocked['block_reason'];?></textarea></td>
         <td align="center"><input type="checkbox" name="delete[]" value="<?=$blocked['id'];?>"></td>
      </tr>
      <? } ?>
      <tr class="c3">
         <td colspan="3" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td>
      </tr>
   </form>
</table>
<? 	include ("footer.php"); 
} ?>
