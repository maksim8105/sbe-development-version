<?
## v5.22 -> sep. 22, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

if (isset($_POST['savesettsok'])) {
	$from = $_POST['from']; 
	$to = $_POST['to'];
	$inc = $_POST['inc'];
	$delete = $_POST['delete'];
	$emptyTable = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bid_increments");
	
	for ($i=0;$i<count($from);$i++) {
		## if an increment is smaller than 0.01, it will be converted to 0.01
		$increment = ($inc[$i]<=0.01) ? 0.01 : $inc[$i];
		$insertc[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bid_increments (bfrom,bto,increment) VALUES 
		('".$from[$i]."','".$to[$i]."','".$increment."')");
	}
	
	if ($_POST['newfrom']!=""&&$_POST['newto']!=""&&$_POST['newinc']!=""&&$_POST['newfrom']<$_POST['newto']) {
		$increment = ($_POST['newinc']<=0.01) ? 0.01 : $_POST['newinc'];
		$insertnewc = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bid_increments 
		(bfrom,bto,increment) VALUES 
		('".$_POST['newfrom']."','".$_POST['newto']."','".$increment."')");
	}
	
	if (count($delete)>0) {
		for ($i=0;$i<count($delete);$i++) {
			$deletec[$i]=mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_bid_increments WHERE bfrom='".$delete[$i]."'");
		}
	
	}
	$savedSettings="yes";
	$arrangec=mysqli_query($GLOBALS["___mysqli_ston"], "ALTER TABLE probid_bid_increments ORDER BY bfrom");
	
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_tables.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[EDIT_TABLES]; echo " / "; echo $a_lang[EDIT_BID_INC];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[BID_INC_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <form action="<?=$_SERVER['PHP_SELF']; ?>" method="post"> 
    <tr class="c3"> 
      <td colspan="4" align="center"><b> 
        <?=$a_lang[BID_INC_SETT];?> 
        </b></td> 
    </tr> 
    <tr class="c4"> 
      <td width="80"><?=$a_lang[FROM];?></td> 
      <td><?=$a_lang[TO];?></td> 
      <td><?=$a_lang[INCREMENT];?></td> 
      <td width="80" align="center"><?=$a_lang[DELETE];?></td> 
    </tr> 
    <? $getIncrements=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bid_increments"); 
	while ($row=mysqli_fetch_array($getIncrements)) { ?> 
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td><input name="from[]" type="text" id="from[]" value="<?=$row['bfrom'];?>" size="8"></td> 
      <td><input name="to[]" type="text" id="to[]" value="<?=$row['bto'];?>" size="8"></td> 
      <td><input name="inc[]" type="text" id="inc[]" value="<?=$row['increment'];?>" size="8"></td> 
      <td align="center"><input type="checkbox" name="delete[]" value="<?=$row['bfrom'];?>"></td> 
    </tr> 
    <? } ?> 
    <tr class="c4"> 
      <td><?=$a_lang[ADD];?></td> 
      <td colspan="2">&nbsp;</td> 
      <td>&nbsp;</td> 
    </tr> 
    <tr class="c1"> 
      <td><input name="newfrom" type="text" id="newfrom" size="8"></td> 
      <td><input name="newto" type="text" id="newto" size="8"></td> 
      <td><input name="newinc" type="text" id="newinc" size="8"></td> 
      <td>&nbsp;</td> 
    </tr> 
    <tr class="c3"> 
      <td colspan="4" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
