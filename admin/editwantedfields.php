<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

if ($_POST['submission']=="yes") {
	if ($_POST['option']=="edit") {
		$deleteBox = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_wanted_fields WHERE boxid='".$_POST['id']."'");
		$boxId = $_POST['id'];
	} else {
		### get the boxid
		$lastId = getSqlField("SELECT id FROM probid_wanted_fields ORDER BY id DESC LIMIT 0,1","id");
		if (trim($lastId) == "n/a") $boxId = 1;
		else $boxId = $lastId + 1;
	}
		
	for ($i=0; $i<count($_POST['boxvalue']); $i++) {
		if (trim($_POST['boxvalue'][$i])!=""||$i==0) 
		$addBox[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_wanted_fields 
		(boxid, boxname, boxtype, boxvalue, boxcaption, boxorder, mandatory, categoryid) VALUES
		('".$boxId."', '".remSpecialChars($_POST['boxname'])."', '".$_POST['boxtype']."', '".remSpecialChars($_POST['boxvalue'][$i])."', 
		'".remSpecialChars($_POST['boxcaption'][$i])."', '".$_POST['boxorder'][$i]."', '".$_POST['mandatory']."', '".$_POST['categoryid']."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
	header ("Location: wantedfieldsmanagement.php");
} else {
	if ($_REQUEST['option']=="edit") {
		$fields = getSqlRow ("SELECT * FROM probid_wanted_fields WHERE boxid='".$_REQUEST['id']."'");
	}
	include ("header.php"); ?>
<script language="JavaScript">
function submitform(theform) {
	theform.submission.value = "";
	theform.submit();
}
</script>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[AUCT_MANAGE]." / ". (($_REQUEST['option']=="edit") ? $a_lang[EDIT_WANTED_FIELD] : $a_lang[ADD_WANTED_FIELD]);?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
  <form action="editwantedfields.php" method="post" name="fieldsform"> 
    <input type="hidden" name="option" value="<?=$_REQUEST['option'];?>"> 
    <input type="hidden" name="submission" value="yes"> 
    <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>"> 
    <tr class="c3"> 
      <td colspan="2" align="center"><b> <? echo (($_REQUEST['option']=="edit") ? $a_lang[EDIT_WANTED_FIELD] : $a_lang[ADD_WANTED_FIELD]); ?> </b></td> 
    </tr> 
    <?
	  if ($_POST['boxname']==""&&$fields['boxname']=="") $boxname = "";
	  else if ($_POST['boxname']==""&&$fields['boxname']!="") $boxname = $fields['boxname'];
	  else $boxname = $_POST['boxname'];
    ?> 
    <tr class="c1"> 
      <td width="100" align="right"><b> 
        <?=$a_lang[BOX_NAME];?> 
        </b></td> 
      <td><input name="boxname" type="text" id="boxname" value="<? echo addSpecialChars($boxname);?>"> </td> 
    </tr> 
    <?
	  if ($_POST['boxtype']==""&&$fields['boxtype']=="") $boxtype = "text"; 
	  else if ($_POST['boxtype']==""&&$fields['boxtype']!="") $boxtype = $fields['boxtype']; 
	  else $boxtype = $_POST['boxtype']; 
	 ?> 
    <tr class="c2"> 
      <td align="right"><b> 
        <?=$a_lang[BOX_TYPE];?> 
        </b></td> 
      <td> <select name="boxtype" class="contentfont" id="boxtype" onChange="submitform(fieldsform);"> 
          <?
		$getTypes = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_fields_types"); 
		while ($boxType = mysqli_fetch_array($getTypes)) {
			echo "<option value=\"".$boxType['fieldtype']."\" ".(($boxType['fieldtype']==$boxtype)?"selected":"").">".$boxType['fieldtype']."</option>";
		} ?> 
        </select> </td> 
    </tr> 
    <tr class="c1"> 
      <td align="right"><b> 
        <?=$a_lang[BOX_CAPT];?> 
        </b></td> 
      <td> <?	  
	    echo "<table cellpadding=\"0\" cellspacing=\"1\" border=\"0\">\n";
		echo "<tr><td align=\"center\"><strong>".$a_lang[BOX_VALUE]."</strong></td><td align=\"center\"><strong>".$a_lang[BOX_CAPTION]."</strong></td><td align=\"center\"><strong>".$a_lang[BOX_ORDER]."</strong></td></tr>";
		$maxCaptions = getSqlField("SELECT maxfields FROM probid_fields_types WHERE fieldtype = '".$boxtype."'","maxfields"); 
		for ($i=0; $i<$maxCaptions; $i++) {
			if ($_REQUEST['option']=="edit"&&count($_POST['boxvalue'])<=0) {
				$defVals = getSqlRow ("SELECT boxvalue, boxcaption, boxorder FROM probid_wanted_fields WHERE boxid='".$_REQUEST['id']."' LIMIT $i,1");
				
				$boxValueDefault = ($defVals['boxvalue']!="n/a") ? $defVals['boxvalue'] : "" ;
				$boxCaptionDefault =  ($defVals['boxcaption']!="n/a") ? $defVals['boxcaption'] : "";
				$boxOrderDefault =  ($defVals['boxorder']!="n/a") ? $defVals['boxorder'] : "";
			} else if ($_REQUEST['option']=="edit"&&count($_POST['boxvalue'])>0) {
			} else {
			}
			echo "<tr>
					<td><input type=\"text\" name=\"boxvalue[]\" size=\"25\" value=\"".$boxValueDefault."\"></td>
					<td><input type=\"text\" name=\"boxcaption[]\" size=\"25\" value=\"".$boxCaptionDefault."\"></td>
					<td><input type=\"text\" name=\"boxorder[]\" size=\"7\" value=\"".$boxOrderDefault."\"></td>
				  </tr>";  
		}	  
		echo "</table>";
	  ?> </td> 
    </tr> 
    <? $getCategories = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,name FROM probid_categories WHERE parent=0 AND hidden=0 ORDER BY theorder,name");?> 
    <tr class="c2"> 
      <td align="right"><b> 
        <?=$a_lang[CATEGORY];?> 
        </b></td> 
      <td><select name="categoryid"> 
		 <option value="0" selected><?=$a_lang[ALL];?></option> 
         <? while($category=mysqli_fetch_array($getCategories)) { ?> 
         <option value="<?=$category['id'];?>" <? echo (($category['id']==$fields['categoryid'])?"selected":"");?>>
		 <?=$category['name'];?>   
         </option> 
		 <? } ?>
        </select> </td> 
    </tr> 
    <?
	  if ($_POST['mandatory']==""&&$fields['mandatory']=="") $mandatory = 0;
	  else if ($_POST['mandatory']==""&&$fields['mandatory']!="") $mandatory = $fields['mandatory'];
	  else $mandatory = $_POST['mandatory'];
	?> 
    <tr class="c1"> 
      <td align="right"><b> 
        <?=$a_lang[BOX_MANDATORY];?> 
        </b></td> 
      <td><input type="radio" name="mandatory" value="1" <? echo ($mandatory==1)?"checked":""; ?>> 
        <?=$a_lang[YES];?> 
        <input type="radio" name="mandatory" value="0" <? echo ($mandatory==0)?"checked":""; ?>> 
        <?=$a_lang[NO];?></td> 
    </tr> 
    <tr class="c3"> 
      <td align="right">&nbsp;</td> 
      <td><input name="addnewsok" type="submit" id="addnewsok" value="<?=$a_lang[BUTT_SUBMIT];?>"></td> 
    </tr> 
  </form> 
</table> 
</td> 
<? 	include ("footer.php");
	} 
} ?> 
