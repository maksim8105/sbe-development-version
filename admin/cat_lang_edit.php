<?
## v5.24 -> may. 15, 2006
////////////////////////////////////////////////////
// Multi-lingual module - additional file - cat_lang_edit.php
// Last Update : 18/02/2005 By Hobbs (Kevin Thomas)
////////////////////////////////////////////////////
/* 
Description: 
This file writes allow the user to edit additional category names by viewing the
site default language against the chosen language side by side (hopefully for easier viewing)
The details are then saved to the relevant config/lang/../category.lang file and a key pair array 
is generated for the required language * see note in updatecats.php *

Copyright:
These files are owned and are intended ONLY for use in the phpprobid auction script
http://www.phpprobid.com
Re-sale of the following code (in part or in whole) without prior written consent is 
expressly forbidden
*/

session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("../config/lang/list.php");
	
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1;  
	
if (get_magic_quotes_gpc()) { 
    $_GET = array_map('stripslashes', $_GET); 
    $_POST = array_map('stripslashes', $_POST); 
}

$chosen_lang = $_POST['lang'];

if (isset($_POST['savesettsok'])) {
	$edit = remSpecialChars($_POST['edit']); 
	if (get_magic_quotes_gpc()) $edit = stripslashes($edit);
	$edit = addSpecialChars($edit, TRUE);

	$filename = "../config/lang/".$chosen_lang."/category.lang";
	if (is_writable($filename)) {
	    if (!$editfile = fopen($filename, 'w')) {
			$updated = "notopen";
	        exit;
	    }
	    if (!fwrite($editfile, $edit)) {
			$updated = "notwritten";
	        exit;
	    }
    	$updated = "yes";    
	    fclose($editfile);		
	} else {
		$updated = "notwritable";
	}

if (@!$open_file = fopen ("../config/lang/".$chosen_lang."/catsarray.php","w")) {
	echo "Sorry I cannot open the file '".$chosen_lang."/catsarray.php', please inform the administrator of this problem";
	exit;
}
include ("../config/lang/".$chosen_lang."/category.lang");
$category_plain .="<?\n";
$category_plain .='$cat_array = array (';

$getcats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,parent FROM probid_categories ORDER BY name");
$get_cat_count = mysqli_num_rows($getcats);
while ($crow = mysqli_fetch_array($getcats)) {
	$croot = $crow['id'];
	$catname = $c_lang[$crw['id']];
	$cntr = 0;
	$ct=0;
	$number++;
	while ($croot>0) {
		$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,parent FROM probid_categories WHERE id='$croot' ORDER BY name") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	 				
		$crw = mysqli_fetch_array($sbcts);
		if($cntr == 0) {
			$catid[$ct] = $crw[id];
			$catname = $c_lang[$crw['id']];
			$ct++;
		} else {
			if($crw['parent'] != $croot) {
				$catid[$ct] = $crw[id];
		 		$catname = $c_lang[$crw['id']]." : ".$catname;
				$ct++;
			}
		}
		$cntr++;
		$croot = $crw['parent'];
	}
	if (getSqlNumber("SELECT id FROM probid_categories WHERE parent='".$catid[0]."'")==0) {
		if ($number==$get_cat_count) {
			$category_plain .='"'.$catid[0].'"=>"'.remSpecialChars($catname).'"';
		} else {
			$category_plain .='"'.$catid[0].'"=>"'.remSpecialChars($catname).'",';
		}
	}
}
$category_plain .=");\n ?>";
fputs($open_file,$category_plain); 	
fclose($open_file);

}
include ("header.php"); 
?>
	<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
	<tr> 
		<td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
		<td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
		<td>&nbsp;</td> 
	</tr> 
	<tr> 
		<td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[CATEGORIES]; echo " / "; echo $a_lang[h_edit_cat_lang];?>&nbsp;&nbsp;</td> 
		<td><img src="images/end_part.gif"></td> 
	</tr> 
	</table> 
	<br> 
	<table width="100%" border="0" cellspacing="2" cellpadding="4">
	<? if ($updated!="") { ?> 
	<tr class="c4"> 
		<td align="center" colspan="2"> 
		<? if ($updated=="yes") echo "The file $filename was updated";
		else if ($updated=="notwritable") echo "The file $filename is not writable";
		else if ($updated=="notopen")  echo "Cannot open file $filename";
		else if ($updated=="notwritten") echo "The file $filename is not editable";
		?></td> 
	</tr> 
	<? } ?>
	<form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
	<input type="hidden" name="pass_lang" value="<?=$chosen_lang;?>">
	<tr>
		<td colspan="2" class="c3" align="right"><?=$a_lang[h_choose];?> 
		<? echo  "<select name=\"lang\">";
		for ($z=0; $z < $sizeofarray; $z++) {
			if ($setts['default_lang']!=$langlist[$z]) {
				echo "<option value=\"$langlist[$z]\"";
				if($chosen_lang==$langlist[$z]) {
					echo " selected=\"selected\"";
				}
			echo ">$langlist[$z]\n";
			}
		} 
		echo "</select>"; ?>
		<input name="changelang" type="submit" id="changelang" value="<?=$a_lang[h_go];?>"> </td>
	</tr>
	<? if ($chosen_lang) { ?>
	<tr>
		<?
		$file1=fopen("../config/lang/".$setts['default_lang']."/category.lang","r");
		while (!feof ($file1)) {
			$file1_contents.=fgets($file1, 4096);
		}
		fclose ($file1);
		?>
		<td align="center"><textarea name="orig" cols="50" rows="30" readonly><?=$file1_contents;?></textarea></td>
		<?
		$file2=fopen("../config/lang/".$chosen_lang."/category.lang","r");
		while (!feof ($file2)) {
			$file2_contents.= fgets($file2, 4096);
		}
		fclose ($file2);
		?>
		<td align="center"><textarea name="edit" id="edit" cols="50" rows="30"><?=$file2_contents;?></textarea></td>
	</tr>
	<tr class="c3"> 
		<td align="center" colspan="2"><input name="savesettsok" type="submit" value="<?=$a_lang[BUTT_PROCESS];?>"></td> 
	</tr> 
	<? } ?>
	</form>
	</table>
</td>
<? 	include ("footer.php"); 
} ?>
