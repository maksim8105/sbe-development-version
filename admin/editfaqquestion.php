<?
## v5.23 -> dec. 13, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_FAQ];?>&nbsp;&nbsp;</td>
    <td><img src="images/end_part.gif"></td>
  </tr>
</table>
<br>
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[SETT_CHANGED]."</p>":""; ?>
<form action="editquestions.php" method="post">
  <table width="100%" border="0" cellspacing="2" cellpadding="4">
    <input type="hidden" name="section" value=<?=$_REQUEST['section'];?>>
    <?
	if ($_GET['option']=="add") { 
		echo "<input type=hidden name=option value=add>"; 
	} else {
		echo "<input type=hidden name=id value=".$_GET['id'].">";
		$faqQuestion = getSqlRow("SELECT * FROM probid_faq_questions WHERE id='".$_GET['id']."'");
	} ?>
    <tr class="c3">
      <td colspan="2" align="center"><b>
        <?=$a_lang[ADD_EDIT_QUESTION];?>
        </b></td>
    </tr>
    <tr class="c1">
      <td width="150"><b>
        <?=$a_lang[FAQ_QUESTION_NAME];?>
        :</b></td>
      <td><input type="text" name="name" value="<?=$faqQuestion['name'];?>"></td>
    </tr>
    <tr class="c2">
      <td valign="top"><b>
        <?=$a_lang[ENTER_CONTENT];?>
        :</b></td>
      <td><textarea name="content" cols="45" rows="10" id="content"><?=$faqQuestion['content'];?>
</textarea>
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW];?></td>
    </tr>
    <tr class="c3">
      <td colspan="2" align="center"><input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVE];?>"></td>
    </tr>
  </table>
</form>
<? 	include ("footer.php"); 
} ?>
