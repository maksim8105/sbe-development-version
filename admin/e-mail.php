<?
## v5.21 -> jul. 14, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");


if ($_POST['option']=="write") {
	// Let's make sure the file exists and is writable first.
	if (get_magic_quotes_gpc()) {
	   $edit = stripslashes($_POST['editmail']);
	} else {
	   $edit = $_POST['editmail'];
	}

	$filename = "../mails/".$_POST['page'].".php";
	if (is_writable($filename)) {
	    if (!$editfile = fopen($filename, 'w')) {
			$updated = "notopen";
	        exit;
	    }
	    if (!fwrite($editfile, $edit)) {
			$updated = "notwritten";
	        exit;
	    }
    	$updated = "yes";    
	    fclose($editfile);		
	} else {
		$updated = "notwritable";
	}
}	

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]; echo " / "; echo $a_lang[EDIT_SYS_EMAIL];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <tr> 
    <td align="center" class="c3"><b> 
      <?=$a_lang[EDIT_SYS_EMAIL];?> 
      </b></td> 
  </tr>
  <tr>
    <td align="center" class="c1">[ <a href="editemails.php">Back to the file list</a> ] </td>
  </tr> 
</table> 
<table width="100%" border="0" cellspacing="2" cellpadding="4"> 
  <? if ($updated!="") { ?> 
  <tr class="c4"> 
    <td align="center"> <? 
	if ($updated=="yes") echo "The file $filename was updated";
	else if ($updated=="notwritable") echo "The file $filename is not writable";
	else if ($updated=="notopen")  echo "Cannot open file $filename";
	else if ($updated=="notwritten") echo "The file $filename is not editable";
	?> </td> 
  </tr> 
  <? } ?> 
  <form action="<?=$_SERVER['PHP_SELF'];?>" method="post"> 
    <input type="hidden" name="page" value="<?=$_REQUEST['page'];?>"> 
    <input type="hidden" name="option" value="write"> 
    <?
	$file1=fopen("../mails/".$_REQUEST['page'].".php","r");
	while (!feof ($file1)) {
		$file1_contents.=fgets($file1, 4096);
	}
	fclose ($file1);
	?> 
    <tr class="c1"> 
      <td align="center"><textarea name="editmail"  cols="80" rows="27"><?=$file1_contents;?></textarea></td> 
    </tr> 
    <tr class="c3"> 
      <td align="center"><input name="Submit" type="submit" value="<?=$a_lang[BUTT_PROCESS];?>"></td> 
    </tr> 
  </form> 
</table> 
<? 	include ("footer.php"); 
} ?>
