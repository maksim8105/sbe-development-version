<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");

$phone="(".$dayphone1.") ".$dayphone2;
?>
<link href="../themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../themes/<?=$setts['default_theme'];?>/main.js" type="text/javascript"></script>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td rowspan="2"><img src="images/i_user.gif" border="0"></td>
      <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
      <td>&nbsp;</td>
   </tr>
   <tr>
      <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[USER_MANAGE]; echo " / "; echo $a_lang[CHANGE_DETS];?>&nbsp;&nbsp;</td>
      <td><img src="images/end_part.gif"></td>
   </tr>
</table>
<br>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
   <tr>
      <td><? include ("../formchecker.php"); ?>
      </td>
   </tr>
</table>
<? 
if ($_REQUEST['action']=="updateinfo") { 
	$payment_update_string = '';
	$payment_update_string .= "paypalemail='".$_POST['paypalemail']."',";
	$payment_update_string .= "worldpayid='".$_POST['worldpayid']."',";
	$payment_update_string .= "ikoboid='".$_POST['ikoboid']."',ikoboipn='".$_POST['ikoboipn']."',";
	$payment_update_string .= "checkoutid='".$_POST['checkoutid']."',";
	$payment_update_string .= "protxname='".$_POST['protxname']."',protxpassword='".$_POST['protxpassword']."',";
	$payment_update_string .= "authnetid='".$_POST['authnetid']."',authnettranskey='".$_POST['authnettranskey']."',";
	$payment_update_string .= "nochexemail='".$_POST['nochexemail']."',";
	$payment_update_string = rtrim($payment_update_string,',');

	$vat_uid = remSpecialChars($_POST['vat_uid_number']);

	$companyname = ($_POST['accounttype']==1) ? "'".((isset($GLOBALS["___mysqli_ston"]) && is_object($GLOBALS["___mysqli_ston"])) ? mysqli_real_escape_string($GLOBALS["___mysqli_ston"], $_POST['companyname']) : ((trigger_error("Error. Please contact administration.", E_USER_ERROR)) ? "" : ""))."'" : 'NULL';

	$updateInformation = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
	name='".remSpecialChars($_POST['name'])."',companyname=".$companyname.", address='".remSpecialChars($_POST['address'])."',city='".remSpecialChars($_POST['city'])."',
	state='".remSpecialChars($_POST['state'])."',country='".remSpecialChars($_POST['country'])."',zip='".remSpecialChars($_POST['zip'])."',
	phone='".remSpecialChars($_POST['phone'])."',email='".$_POST['email']."',newsletter='".$_POST['subsnl']."',
	apply_vat_exempt='".$_POST['apply_vat_exempt']."',vat_uid_number='".$vat_uid."',
   ".$payment_update_string."
	WHERE id='".$_REQUEST['id']."'") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	
	if ($_POST['password']!="") {
		$updatePassword = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_users SET 
		password='".md5($_POST['password'])."' WHERE id='".$_REQUEST['id']."'");
	}
	
	echo "<p align=center>$a_lang[REG_UPDATE_SUCCESS_MSG]</p>";
} else { 
	$user = getSqlRow("SELECT * FROM probid_users WHERE id='".$_REQUEST['id']."'"); ?>
<form action="edituser.php" method="post">
   <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
   <table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
      <tr>
         <td colspan="3" class="c1"><b>
            <?=$lang[editpersonalinfo]?>
            </b></td>
      </tr>
      <tr valign="top" class="c3">
         <td width="120" nowrap class="contentfont"><?=$lang[fullname]?>
         </td>
         <td width="270" class="contentfont"><input name="name" type="text" class="contentfont" id="name" value="<? echo (trim($_POST['name'])!="") ? $_POST['name'] : $user['name'];?>" maxlength="50"></td>
         <td width="100%" class="smallfont"><?=$lang[enterfullname]?></td>
      </tr>
      <tr class="c2">
         <td  class="contentfont"><?=$lang[m_account_type]?></td>
         <td  class="contentfont"><input name="accounttype" type="radio" class="contentfont" id="accounttype" value="0" <?=$user['companyname'] ? '' :'checked';?>>
            Individual<br>
            <input name="accounttype" type="radio" class="contentfont" id="accounttype" value="1" <?=$user['companyname'] ? 'checked' :'';?>>
            Business </td>
         <td  class="contentfont"></td>
      </tr>
      <tr class="c3">
         <td  class="contentfont"><?=$lang[m_company]?></td>
         <td  class="contentfont"><input name="companyname" type="text" class="contentfont" id="companyname" value="<?=strToForm($user['companyname']);?>" maxlength="50"></td>
         <td  class="contentfont"></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[address]?></td>
         <td class="contentfont"><input name="address" type="text" class="contentfont" id="address" value="<? echo (trim($_POST['address'])!="") ? $_POST['address'] : $user['address'];?>" maxlength="50"></td>
         <td class="smallfont"><?=$lang[enteraddress]?></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?=$lang[city]?></td>
         <td class="contentfont"><input name="city" type="text" class="contentfont" id="city" value="<? echo (trim($_POST['city'])!="") ? $_POST['city'] : $user['city'];?>" maxlength="50"></td>
         <td class="smallfont"><?=$lang[entercity]?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[state]?></td>
         <td class="contentfont"><input name="state" type="text" class="contentfont" id="name3" value="<? echo (trim($_POST['state'])!="") ? $_POST['state'] : $user['state'];?>" maxlength="50"></td>
         <td class="smallfont"><?=$lang[enterstate]?></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?=$lang[country]?></td>
         <td class="contentfont"><select name="country" class="contentfont">
               <?
					$selected_country =  (trim($_POST['country'])!="") ? $_POST['country'] : $user['country'];
					$getCountries=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_countries ORDER BY theorder ASC, name ASC");
					while ($country = mysqli_fetch_array($getCountries)) {
						echo "<option value=\"".$country['name']."\" ".(($country['name']==$selected_country)?"selected":"").">".$country['name']."</option>";
					} ?>
            </select></td>
         <td class="smallfont"><?=$lang[entercountry]?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[zip]?></td>
         <td class="contentfont"><input name="zip" type="text" id="state" value="<? echo (trim($_POST['zip'])!="") ? $_POST['zip'] : $user['zip'];?>" maxlength="20"></td>
         <td class="smallfont"><?=$lang[enterzip]?></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?=$lang[phone]?></td>
         <td class="contentfont"><input name="phone" type="text" id="state2" value="<? echo (trim($_POST['phone'])!="") ? $_POST['phone'] : $user['phone'];?>" maxlength="20"></td>
         <td class="smallfont"><?=$lang[enterphone]?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[email]?></td>
         <td class="contentfont"><input name="email" type="text" id="state3" value="<? echo (trim($_POST['email'])!="") ? $_POST['email'] : $user['email'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?=$lang[enteremail]?></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?=$lang[username]?></td>
         <td class="contentfont"><?=$user['username'];?>
         </td>
         <td class="smallfont"><?=$lang[enterusername]?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[newssubscr]?></td>
         <td class="contentfont"><input name="subsnl" type="radio" value="Y" <? echo (($user['newsletter']=="Y")?"checked":"");?>>
            <?=$lang[yes]?>
            <input type="radio" name="subsnl" value="N" <? echo (($user['newsletter']=="N"||$user['newsletter']=="")?"checked":"");?>>
            <?=$lang[no]?>
         </td>
         <td class="smallfont"><?=$lang[enternewssubscr]?></td>
      </tr>
      <? if ($setts['vat_rate']>0&&$setts['auto_vat_exempt']=="N") { ?>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><b>
            <?=$lang[apply_vat_exempt]?>
            </b></td>
         <input name="apply_vat_exempt_old" type="hidden" value="<? echo (trim($_POST['apply_vat_exempt'])!="") ? $_POST['apply_vat_exempt'] : $user['apply_vat_exempt'];?>">
         <td class="contentfont"><input name="apply_vat_exempt" type="radio" value="Y" <? echo (($user['apply_vat_exempt']=="Y")?"checked":"");?>>
            <?=$lang[yes]?>
            <input type="radio" name="apply_vat_exempt" value="N" <? echo (($user['apply_vat_exempt']=="N"||$user['apply_vat_exempt']=="")?"checked":"");?>>
            <?=$lang[no]?></td>
         <td class="smallfont">&nbsp;</td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><b>
            <?=$lang[vat_uid_number]?>
            </b></td>
         <td class="contentfont"><input name="vat_uid_number" type="text" id="vat_uid_number" size=40 value="<? echo (trim($_POST['vat_uid_number'])!="") ? $_POST['vat_uid_number'] : $user['vat_uid_number'];?>"></td>
         <td class="smallfont"><?=$lang[apply_vat_msg]?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><b>
            <?=$lang[vat_exempt_status]?>
            </b></td>
         <td class="contentfont"><? echo ($user['vat_exempted']=="Y") ? $lang[exempted] : $lang[not_exempted];?></td>
         <td class="smallfont"></td>
      </tr>
      <? } ?>
      <? $dirpay_enabled = getSqlField("SELECT paypaldirectpayment FROM probid_gen_setts",'paypaldirectpayment');?>
      <? if($dirpay_enabled):?>
      <tr>
         <td colspan="3" class="c1"><b>
            <?=$lang[m_pay_systems]?>
            </b></td>
      </tr>
      <? $paypal_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=1",'status'); ?>
      <? if($paypal_enabled):?>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?='Paypal Email';?></td>
         <td class="contentfont"><input name="paypalemail" type="text" id="state3" value="<? echo (trim($_POST['paypalemail'])!="") ? $_POST['paypalemail'] : $user['paypalemail'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your PayPal registration email';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>Paypal Instant Payment Notification (IPN) URL:</b><br>
            <?=$setts['siteurl'];?>
            paymentprocess.php</td>
      </tr>
      <? endif ?>
      <? $nochex_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=4",'status'); ?>
      <? if($nochex_enabled):?>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?='Nochex Email';?></td>
         <td class="contentfont"><input name="nochexemail" type="text" id="state3" value="<? echo (trim($_POST['nochexemail'])!="") ? $_POST['nochexemail'] : $user['nochexemail'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Nochex registration email';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>Nochex Instant Payment Notification (IPN) URL:</b><br>
            <?=$setts['siteurl'];?>
            nochexprocess.php</td>
      </tr>
      <? endif ?>
      <? $worldpay_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=2",'status'); ?>
      <? if($worldpay_enabled):?>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?='WorldPay ID';?></td>
         <td class="contentfont"><input name="worldpayid" type="text" id="worldpayid" value="<? echo (trim($_POST['worldpayid'])!="") ? $_POST['worldpayid'] : $user['worldpayid'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your WorldPay ID';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>WorldPay Callback URL:</b><br>
            <?=$setts['siteurl'];?>
            worldpayprocess.php</td>
      </tr>
      <? endif?>
      <? $ikobo_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=3",'status'); ?>
      <? if($ikobo_enabled):?>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?='Ikobo Member ID';?></td>
         <td class="contentfont"><input name="ikoboid" type="text" id="ikoboid" value="<? echo (trim($_POST['ikoboid'])!="") ? $_POST['ikoboid'] : $user['ikoboid'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Ikobo Member ID';?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?='Ikobo IPN Password';?></td>
         <td class="contentfont"><input name="ikoboipn" type="password" id="ikoboipn" value="<? echo (trim($_POST['ikoboipn'])!="") ? $_POST['ikoboipn'] : $user['ikoboipn'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Ikobo IPN Password';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>Ikobo Callback URL:</b><br>
            <?=$setts['siteurl'];?>
            ikoboprocess.php</td>
      </tr>
      <? endif?>
      <? $checkout_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=5",'status'); ?>
      <? if($checkout_enabled):?>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?='2Checkout ID';?></td>
         <td class="contentfont"><input name="checkoutid" type="text" id="checkoutid" value="<? echo (trim($_POST['checkoutid'])!="") ? $_POST['checkoutid'] : $user['checkoutid'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your 2Checkout ID';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>2Checkout Callback URL:</b><br>
            <?=$setts['siteurl'];?>
            checkoutprocess.php</td>
      </tr>
      <? endif?>
      <? $protx_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=7",'status'); ?>
      <? if($protx_enabled):?>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?='Protx Vendor Name';?></td>
         <td class="contentfont"><input name="protxname" type="text" id="protxname" value="<? echo (trim($_POST['protxname'])!="") ? $_POST['protxname'] : $user['protxname'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Protx Vendor Name';?></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?='Protx Password';?></td>
         <td class="contentfont"><input name="protxpassword" type="password" id="protxpassword" value="<? echo (trim($_POST['protxpassword'])!="") ? $_POST['protxpassword'] : $user['protxpassword'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Protx Password';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>Protx Callback URL:</b><br>
            <?=$setts['siteurl'];?>
            protxprocess.php</td>
      </tr>
      <? endif?>
      <? $auth_enabled = getSqlField("SELECT status FROM probid_direct_payment WHERE id=6",'status'); ?>
      <? if($auth_enabled):?>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?='Authorize.Net ID';?></td>
         <td class="contentfont"><input name="authnetid" type="text" id="authnetid" value="<? echo (trim($_POST['authnetid'])!="") ? $_POST['authnetid'] : $user['authnetid'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Authorize.Net ID';?></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?='Authorize.Net Transaction Key';?></td>
         <td class="contentfont"><input name="authnettranskey" type="text" id="authnettranskey" value="<? echo (trim($_POST['authnettranskey'])!="") ? $_POST['authnettranskey'] : $user['authnettranskey'];?>" size="30" maxlength="120"></td>
         <td class="smallfont"><?='Enter your Authorize.Net Transaction Key';?></td>
      </tr>
      <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
         <td><b>"Relay Response" URL:</b><br>
            <?=$setts['siteurl'];?>
            authorize.net.process.php</td>
      </tr>
      <? endif?>
      <tr>
         <td colspan="3"><b>NOTE:</b> In order for the Authorize.Net Payment Method to work correctly,
            you <b>must</b> add this URL to your Authorize.Net account by setting it as the main URL for "Relay Response".
            Please consult Authorize.Net support for how to do this!<br>
            <br>
            <b>NOTE:</b> In order for WorldPay or 2Checkout to work correctly, you will have to activate the callback
            on the settings menu of your chosen payment gateway. Use the URL provided under the ID of each payment gateway as callback URL.<br>
            <br>
            If you choose to use 2Checkout, all the fees will be calculated in USD </td>
      </tr>
      <? endif?>
      <tr>
         <td colspan="3" class="c1"><b>
            <?=$lang[m_vat_setts]?>
            </b></td>
      </tr>
      <!--
    	<tr valign="top" class="c2">
      	<td nowrap class="contentfont"><?=$lang[m_use_vat];?></td>
	      <td class="contentfont">
            <input name="usevat" type="radio" value="1" <?=(($user['vatrate']!=0)?'checked':'');?>><?=$lang[yes];?>
            <input name="usevat" type="radio" value="0" <?=(($user['vatrate']==0)?'checked':'');?>><?=$lang[no];?>
   	   </td>
      	<td>&nbsp;</td>
	    </tr>
   	<tr valign="top" class="c3">
      	<td nowrap class="contentfont"><?=$lang[m_vat_rate];?></td>
			<td class="contentfont">
            <input name="vatrate" type="text" id="vatrate" value="<?=$user['vatrate'];?>" size="10" maxlength="30">%
      	</td>
      	<td class="smallfont"><?=$lang[m_vat_rate];?></td>
    	</tr>
    	-->
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[vat_uid_number];?></td>
         <td class="contentfont"><input name="vat_uid_number" type="text" id="vat_uid_number" value="<?=$user['vat_uid_number'];?>" size="40" maxlength="30">
         </td>
         <td class="smallfont"><?='Enter your VAT Registration Number';?></td>
      </tr>
      <tr>
         <td colspan="3" class="c1"><b>
            <?=$lang[changepass]?>
            </b></td>
      </tr>
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?=$lang[pass]?></td>
         <td class="contentfont"><input name="password" type="password" id="password" maxlength="20"></td>
         <td class="smallfont"><?=$lang[enterpass]?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[confirmpass]?></td>
         <td class="contentfont"><input name="password2" type="password" id="password2" maxlength="20"></td>
         <td class="smallfont"><?=$lang[enterconfirmpass]?></td>
      </tr>
      <tr>
         <td colspan="3" align="center" class="c4"><input name="updatembinfook" type="submit" id="updatembinfook" value="<?=$lang[updatebutton]?>"></td>
      </tr>
   </table>
</form>
<? }
	include ("footer.php"); 
} ?>
