<?
## v5.24 -> apr. 05, 2006
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");


include ("header.php"); 
include ("../config/lang/$setts[admin_lang]/site.lang");

function format_date($date, $mode, $delimiter = ' - ', $clock = 0) {
//2002-04-16 22:01:45
//0123456789012345678
 global $msg;
 global $month;
 global $month2;
 $year=substr($date, 0, 4);
 $month_val=substr($date, 5, 2);
 $month_num=intval($month_val);
 $day=substr($date, 8, 2);
 $hour=substr($date, 11, 2);
 $min=substr($date, 14, 2);
 $sec=substr($date, 17, 2);
 //10.03.2002 - 02:10
 if ($mode==1) {$output=$day.".".$month_val.".".$year." - ".$hour.":".$min;}
 //10.03.2002
 if ($mode==2) {$output=$day.".".$month_val.".".$year;}
 //12:15
 if ($mode==3) {$output=$hour.":".$min;}
 //20. october 2004
 if ($mode==4) {$output="$day. $month[$month_num] $year";}
 //octjabrja 8, 2004
 if ($mode==5) {$output="$month2[$month_num] $day, $year";}
 if ($mode==7) {$output=$day.'.'.$month_val.'.'.$year; $clock = 1;} //alfa.fi
 if ($mode==6) {
	# from dd.mm.yyyy - hh:mm
	# to yyyy-mm-dd hh:mm
	$d_length  = strlen($delimiter);
	$pos = strpos($date, $delimiter);
	if ($pos > 0) {
		$aeg_ee  = substr($date, 0, $pos);
		$time_ee = trim(substr($date, $pos + $d_length));
	} else {
		$aeg_ee  = $date;
	}
	$format = 'dd.mm.yyyy';
	$month  = substr($aeg_ee, strpos($format, 'mm'), 2);
	$day    = substr($aeg_ee, strpos($format, 'dd'), 2);
	if(strstr($format, 'yyyy')) {
		$year = substr($aeg_ee, strpos($format, 'yyyy'), 4);
	} else {
		$year = '20'.substr($aeg_ee, strpos($format, 'yy'), 2);
	}
	$output = $year.'-'.$month.'-'.$day.($time_ee ? " ".$time_ee : "");
 }
 if ($clock) $output.=" ".$msg[1000][35];
 return $output;
}

function usersTrans($arg) {

    $arrSort = array("tdate","name","description","amount","balance","id");
    $arrAw = array("asc","desc");
    
    if (($_POST['sort'])!='') {
    
      if ((in_array($_POST["sort"],$arrSort)) && (in_array($_POST["arrow"],$arrAw)))
      {
        $order = " ORDER BY ".$arg['sort']." ".$arg['arrow']."";
      }
    
      
    } else {
        $order = " ORDER BY probid_users_transactions.id ASC";
    }
    
     if (($_POST['start_date']!='' AND $_POST['end_date']!='') AND (strtotime($_POST['start_date']) <= strtotime($_POST['end_date']))) 
     {
        $period = "AND probid_users_transactions.tdate BETWEEN '".$_POST['start_date']." 00:00:00' AND '".$_POST['end_date']." 23:59:59'";
     } else {
        $period = "AND (MONTH(CURDATE()) = MONTH(probid_users_transactions.tdate))";
     }     
     
     if (($_POST["username"])!="")
     {
       $username=" AND probid_users.username='".$_POST["username"]."' ";
     }
     
     $criterias = $period.$username;
     

     
    $result = @mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  
            probid_users_transactions.balance AS tbalance,
            probid_users_transactions.id,
            probid_users_transactions.description,
            probid_users_transactions.amount,
            probid_users_transactions.op,
            probid_users_transactions.name,
            probid_users.username as login, 
            probid_users_transactions.tdate
            FROM probid_users_transactions LEFT JOIN probid_users ON probid_users.id=probid_users_transactions.userid
            WHERE probid_users_transactions.op LIKE '%%' ".$criterias." ".$order."");

    while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
      $myTrans[] = $row; 
    }
return $myTrans;
}

$results = usersTrans($_POST);


?>
<form name="dataForm" method="post" action="userstransactions.php">
<table width="100%" border="0" cellpadding="4" cellspacing="4" align="center" class="border">
<tr>
    <td>
        <table>
          <tr>
              <td>Period</td>
              <td>
                <table>
                <tr>
                    <td><input class="date" type="text"  name="start_date" value="<?=$_POST['start_date']?>"/></td>
                    <td><a href="#" onclick="displayDatePicker('start_date');"><img alt="" border="0" src="<?="../images/calendar.jpg"?>"></a></td>
                    <td> - </td>
                    <td><input class="date" type="text"  name="end_date" value="<?=$_POST['end_date']?>"/></td>
                    <td><a href="#" onclick="displayDatePicker('end_date');"><img alt="" border="0" src="<?="../images/calendar.jpg"?>"></a></td>
                </tr>
                </table>
              </td>
          </tr>
          <tr>
              <td>Username</td><td><input id="req_user" style="font-weight:bold;" type="text" value="<?=$_POST["username"]?>" name="username"></td>
          </tr>
          <tr>
              <td><input type="submit" value="Show"></td>
          </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table style="width:100%;">
        <tr>
        <td>#<a href="javascript:sortBy('id','asc');"><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('id','desc')"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Date<a href="javascript:sortBy('tdate','asc');"><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('tdate','desc')"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Name<a href="javascript:sortBy('name','asc');"><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('name','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Description<a href="javascript:sortBy('description','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('description','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Amount<a href="javascript:sortBy('amount','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('amount','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        <td>Balance<a href="javascript:sortBy('balance','asc');" ><img border="0" src="../images/sortup.gif"></a>
                 <a href="javascript:sortBy('balance','desc');"><img border="0" src="../images/sortdown.gif"></a>
        </td>
        </tr>
        <?
        if ((is_array($results)) && (sizeof($results)>0)) 
        {
            foreach ($results as $t) 
            {
              if ($data['p']>1) 
              {
                $number=(20*($data['p']-1));
                $x++;
              } else {
                $number++;
              }
                ?><tr class="<?=(($number%2==0)?'c2':'c1')?>"><td><?=$number?></td><td width="120"><?=format_date($t["tdate"],1)?></td><td><?=utf8_encode($t["name"])?>(<a href="#" onclick="javascript:filterBy('username','<?=$t["login"]?>')"><?=$t["login"]?></a>)</td><td><?=$t["description"]?></td><td style="<?=($t["op"]=="in"?'color:green':'color:red')?>"><?=($t["op"]=="in"?'+':'-')?><?=str_replace(".",",",money_format('%i',$t["amount"]))?> EEK</td><td><?=str_replace(".",",",money_format('%i',$t["tbalance"]))?> EEK </td></tr><?
            }
        } else {
        
          ?><tr><td colspan="6" align="center"> Транзакций не найдено </td></tr><?
        
        }
        ?>
    </td>
</tr>
<tr>
    <td>
    <input type="hidden" value="<?=$_POST["arrow"]?>"  name="arrow" id="arrow"/>
    <input type="hidden" value="<?=$_POST["sort"]?>"  name="sort" id="sort"/>    
    </td>
</tr>
</table>
</form>
<?

include ("footer.php"); 
} ?>
