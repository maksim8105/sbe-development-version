<?
## v5.23 -> dec. 13, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include ("../config/config.php");

include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1; 

if ($_REQUEST['option']=="edit") {
	$ans = getSqlRow ("SELECT * FROM probid_announcements WHERE id='".$_REQUEST['id']."'");
}
include ("header.php"); ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><img src="images/i_content.gif" border="0"></td>
    <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[SITE_CONTENT]." / ".$a_lang[EDIT_ANNOUNCEMENTS];?>&nbsp;&nbsp;</td>
    <td><img src="images/end_part.gif"></td>
  </tr>
</table>
<br>
<form action="annmanagement.php" method="post">
  <table width="100%" border="0" cellpadding="4" cellspacing="2">
    <input type="hidden" name="option" value="<?=$_REQUEST['option'];?>">
    <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
    <tr class="c3">
      <td colspan="2" align="center"><b>
        <?=$a_lang[ADD_ANNOUNCEMENT];?>
        </b></td>
    </tr>
    <tr class="c1">
      <td width="100" align="right"><b>
        <?=$a_lang[TITLE];?>
        </b></td>
      <td><input name="title" type="text" id="title" value="<? echo addSpecialChars($ans['title']);?>">
        &nbsp;&nbsp;&nbsp; <? echo  "<select name=\"lang\">";
			for ($z=0; $z < $sizeofarray; $z++) {
				echo "<option value=\"$langlist[$z]\"";
				if($news['lang']==$langlist[$z]) {
					echo " selected=\"selected\"";
				}
				echo ">$langlist[$z]\n";
			}	?></td>
    </tr>
    <tr class="c2">
      <td align="right" valign="top"><b>
        <?=$a_lang[CONTENT];?>
        </b></td>
      <td><textarea id="content" name="content" cols="45" rows="10"><?=addSpecialChars($ans['content']);?>
</textarea>
        <script> 
			var oEdit1 = new InnovaEditor("oEdit1");
			oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
			oEdit1.height=300;
			oEdit1.REPLACE("content");//Specify the id of the textarea here
		</script>
        <br>
        <?=$a_lang[HTML_ALLOW_SHORT];?></td>
    </tr>
    <tr class="c1">
      <td align="right" valign="top"><b>
        <?=$a_lang[DATE];?>
        </b></td>
      <td><input name="date" type="text" id="date" value="<? echo ($_REQUEST['option']=="edit") ? $ans['date'] : date("Y-m-d");?>">
        <br>
        <?=$a_lang[KEEP_DATE_FORMAT];?>
      </td>
    </tr>
    <tr class="c3">
      <td align="right">&nbsp;</td>
      <td><input name="addannok" type="submit" id="addannok" value="<?=$a_lang[BUTT_SUBMIT];?>"></td>
    </tr>
  </table>
</form>
<? 	include ("footer.php"); 
} ?>
