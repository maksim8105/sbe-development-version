<?
## v5.20 -> may. 20, 2005
session_start();
if ($_SESSION['adminarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("../config/config.php");

include ("../config/lang/list.php");
$langlist = explode(" ", $langlist);
$sizeofarray = count($langlist)-1; 

if (isset($_POST['savesettsok'])) {
	for ($i=0;$i<count($_POST['order']);$i++) {
		$updateFields = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fields SET 
		fieldorder='".$_POST['order'][$i]."'
		WHERE boxid='".$_POST['boxid'][$i]."'") or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	}
}

if ($_GET['option']=="chstatus") {
	$changeStatus = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fields SET 
	active='".$_GET['value']."' WHERE boxid='".$_GET['id']."'");
	$changeStatusData = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_fields_data SET 
	active='".$_GET['value']."' WHERE boxid='".$_GET['id']."'");
}

if ($_GET['option']=="delete") {
	$deleteFields = mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fields WHERE boxid='".$_GET['id']."'");
	$deleteFieldData= mysqli_query($GLOBALS["___mysqli_ston"], "DELETE FROM probid_fields_data WHERE boxid='".$_GET['id']."'");
}

include ("header.php"); ?>
 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr> 
     <td rowspan="2"><img src="images/i_content.gif" border="0"></td> 
     <td width="100%"><img src="images/pixel.gif" height="24" width="1"></td> 
     <td>&nbsp;</td> 
   </tr> 
  <tr> 
     <td width="100%" align="right" background="images/bg_part.gif" class="head"><? echo $a_lang[AUCT_MANAGE]; echo " / "; echo $a_lang[FIELDS_MANAG];?>&nbsp;&nbsp;</td> 
     <td><img src="images/end_part.gif"></td> 
   </tr> 
</table> 
<br> 
<? echo ($savedSettings=="yes")?"<p align=\"center\">".$a_lang[FIELDS_TABLE_UPDATED]."</p>":""; ?> 
<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
    <tr class="c3"> 
      <td align="center" colspan="4"><b> 
        <?=$a_lang[FIELDS_MANAG];?> 
        </b></td> 
    </tr> 
    <tr> 
	<form action="fieldsmanagement.php" method="post">
    <tr class="c4"> 
      <td width="50" align="center"><?=$a_lang[BOXID]." / ".$a_lang[ORDER];?></td> 
	 <td align="center"><?=$a_lang[FIELD_SAMPLE];?> </td>
      <td width="120" align="center"><?=$a_lang[OPERATIONS];?></td> 
    </tr> 
    <? 
	$getFields = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT boxid, boxname, active,fieldorder, categoryid FROM probid_fields ORDER BY fieldorder ASC") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
	$fldCnt = 0;
	while ($fields=mysqli_fetch_array($getFields)) { ?> 
	<input type="hidden" name="boxid[]" value="<?=$fields['boxid'];?>">
    <tr class="<? echo (($count++)%2==0)?"c1":"c2"; ?>"> 
      <td align="center">[ <strong><?=$fields['boxid'];?></strong> ]<br><input type="text" size="6" name="order[]" value="<?=$fields['fieldorder'];?>"></td> 
	  <td>
	  <? echo "<table width=\"100%\" cellpadding=\"3\"><tr class=\"".(($count%2==0)?"c1":"c2")."\"><td nowrap>".$fields['boxname']."</td><td width=\"100%\">".createField($fields['boxid'],"")."</td></tr></table>"; ?>
	  <br>
	  [ <strong><?=$a_lang[CATEGORY];?></strong>: <? echo ($fields['categoryid']==0) ? $a_lang[ALL] : getSqlField("SELECT name FROM probid_categories WHERE id='".$fields['categoryid']."'","name"); ?> ]
	  </td>
      <td align="center"><b> <? 
	  	echo "<a href=\"editfields.php?option=edit&id=".$fields['boxid']."\">$a_lang[EDIT]</a><br>";
	  	if ($fields['active']==1) { 
			echo "<a href=\"fieldsmanagement.php?option=chstatus&id=".$fields['boxid']."&value=0\"> $a_lang[SUSPEND] </a>"; 
		} else { 
			echo "<a href=\"fieldsmanagement.php?option=chstatus&id=".$fields['boxid']."&value=1\"> $a_lang[ACTIVATE]</a>"; 
		} ?> 
        <br> 
        <a href="fieldsmanagement.php?option=delete&id=<?=$fields['boxid'];?>"> 
        <?=$a_lang[DELETE];?> 
        </a></b></td> 
    </tr> 
    <? } ?> 
    <tr> 
      <td colspan="4" align="center" class="c3"><b><a href="editfields.php?option=add"> 
        <input type="submit" name="savesettsok" value="<?=$a_lang[BUTT_SAVESET];?>">
      </a></b></td> 
    </tr> 
	</form>
    <tr> 
      <td colspan="4" align="center" class="c3"><b><a href="editfields.php?option=add"> 
        <?=$a_lang[ADD_FIELD];?> 
        </a></b></td> 
    </tr> 
</table> 
<? 	include ("footer.php"); 
} ?>
