<?
## v5.24 -> jun. 02, 2006
session_start();
include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

header5("$lang[bidconfirmation]"); ?>
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="subitem">
  <tr class="contentfont"> 
	    <td width="1%">
	  <? if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { 
	  echo "<img src='themes/".$setts['default_theme']."/img/system/status1.gif' vspace=5>";
	  } else { echo "<img src='themes/".$setts['default_theme']."/img/system/status.gif' vspace=5>"; }?>
	  </td>
		<td>
		<? if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { ?>
	  <?=$lang[welcome];?>, <br><b><? echo $_SESSION['membername']; ?></b>
	  <? } else { echo $lang[status_bidder_seller]; }?>
		</td>
		<td class="contentfont" width="50%" align="right">
		>><a href="<?=processLink('auctiondetails', array('id' => $_GET['id'])); ?>"><?=$lang[retdetailspage];?></a>&nbsp;&nbsp;</td>
   </tr> 
</table> 
<br>
<?  
if ($_SESSION['show_msg']==1) {
	$_SESSION['show_msg']=0;
	$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_GET['id']."' AND active=1");
	$auctionId = $_GET['id']; 	
	$sellerId = $auctionDetails['ownerid'];
	$sendEmail = getSqlField("SELECT default_bid_placed_email FROM probid_users WHERE id='".$auctionDetails['ownerid']."'","default_bid_placed_email");
	if ($sendEmail=="Y") include_once("mails/notifysellerbid.php"); ?>
  <p align="center"><b><?=$lang[bidsubmitted1];?></b></p>
  <table width="100%" border="0" cellpadding="4" cellspacing="2" class="errormessage">
	 	<tr>
	 		<td><?=$lang[bidsubmitted3];?></td>
	 	</tr>
	 </table>
	<table width="100%" border="0" cellpadding="4" cellspacing="2"> 
   <tr class="c1" width="150"> 
     <td align="right"><b><?=$lang[itemname]?>:</b></td>
     <td><b><?=$auctionDetails['itemname'];?></b></td> 
	 <tr class="c3">
			<td width="150" align="right"><b><?=$lang[timeleft]?>:</b></td>
			<td>
			<? 
			$daysLeft = daysleft($auctionDetails['enddate'],$setts['date_format']);
			$timeLeft = timeleft($auctionDetails['enddate'],$setts['date_format']);
			echo ($daysLeft>0) ? $timeLeft : $lang[bidclosed]; ?>
			</td>
		</tr>
		<tr class="c2">
			<td align="right" width="150"><b><?=$lang[currbid]?>:</b></td>
			<td><? echo displayAmount($auctionDetails['maxbid'],$auctionDetails['currency']);?></td>
		</tr>
		<tr class="c5">
			<td align="right" width="150"></td>
			<td></td>
		</tr>		
		<tr>
			<td align="right" width="150"></td>
			<td class="contentfont"><?=$lang[bidsubmitted2];?></td>
		</tr>
	</table>
	<? } else { echo "<p align=center>".$lang[error_refresh]."</p>"; } ?>
<? include ("themes/".$setts['default_theme']."/footer.php"); ?>