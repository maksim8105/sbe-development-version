<?php 
require_once ("config/config.php");
require_once("class.xpath.php");
require_once("download.functions.php");

if (!$_POST["method"]) $_POST["method"]=1;


  $result = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT  * FROM probid_auctions_downloads_lists WHERE owner_id=".$_SESSION["memberid"]." ORDER BY id ASC");
  while ($row= mysqli_fetch_array($result,  MYSQLI_ASSOC)) {
    $ls[] = $row; 
  }
 ?>

<form name="parserForm" method="post" action="get.php">
<table cellpadding="4" cellspacing="0" width="60%" class="mainTable">
<tr>
	<td align="right" width="120">Method:</td>
	<td><input id="s1" type="radio" name="method" value="1" onclick="switchLbl('Auction ID:',1)" <?php echo (($_POST["method"]==1)?"checked":""); ?> >Simple <input id="s2" type='radio' name='method' onclick="switchLbl('Auction owner:',2);" value="2" <?php echo(($_POST["method"]==2)?"checked":""); ?> > Multi</td>
</tr>
<tr>
	<td align="right">Select list:</td>
	<td><?=ListSel($ls,$_POST["list_id"]);?></td>
</tr>
<tr>
	<td align="right"><div id="lbl"></div></td>
	<td><input style="font-family:Tahoma;font-size:10px;font-weight:bold;" type="text" id="u1" size="20" name="url" value="<?php echo $_POST["url"];?>"> <div id="comment" style="color:gray;"></div></td>
</tr>
<tr>
	<td></td>
	<td><input id="selected" type="hidden"  value="<?php echo $_POST["method"];?>"><input type="button" class="buttons" onclick="prepare();" value=" OK "></td>
</tr>
</table>
</form>
<script type="text/javascript">

function switchLbl(param,sel) {
	var div = document.getElementById("lbl");
	div.innerHTML = param;
	if (sel==1) {
		div_c.innerHTML = "e.g. 15294109"

	} else {
		div_c.innerHTML = "e.g. http://www.osta.ee/index.php?fuseaction=search.searchseller&q[seller]=MD82"
	}
}
function prepare() {
document.parserForm.submit();

}

var sel = document.getElementById("selected");
var div = document.getElementById("lbl");
var div_c = document.getElementById("comment");
if (sel.value==1) {
	div.innerHTML = "Auction ID:";
	div_c.innerHTML = "e.g. 15294109"

} else {
	div.innerHTML = "Auction owner:";
	div_c.innerHTML = "e.g. http://www.osta.ee/index.php?fuseaction=search.searchseller&q[seller]=MD82"
}
</script>
<?php

if (!isset($_POST['url'])) exit;

if ($_POST["method"]==1) {

	if (intval($_POST['url'])==0) exit;

	$grab_path = trim("http://www.osta.ee/".$_POST['url']);
	$grab_path = str_replace("&amp;","&",$grab_path);

	$url = urlencode($grab_path);
	@$html = new DOMDocument();

	// Elapsed time script

	list($usec, $sec) = explode(' ', microtime());
	$script_start = (float) $sec + (float) $usec;

	@$html->loadHtmlFile($url); 

	list($usec, $sec) = explode(' ', microtime());
	$script_end = (float) $sec + (float) $usec;

	// Elapsed time sctipt



	$description = new xpath($html, '//div[@class="itemdesc"]'); // div id name
	$description->return = "string";
	$description->childnodes = 1;
	$description->forbidden = array("Body");
	$description->execute();

	$title = new xpath($html, '//td[@class="title"]'); //Grab h1 data
	$title->execute();

	$image = new xpath($html, '//*[@id="image_big"]'); //Grab Images
	$image->return = "string";
	$image->attribute = "src";
	$image->execute();

	$adparams = new xpath($html, '//table[@class="details_main"]/tr[1]/td[1]'); //fetch tables
	$adparams->childnodes = 1;
	$adparams->return = "array";
	$adparams->execute();

	$location = new xpath($html, '//td[@class="w200 txtb"]	'); //fetch tables
	$location->return = "string";
	$location->childnodes = 10;
	$location->forbidden = array("Body");
	$location->execute();

	$current_price = new xpath($html, '//td[@id="current-price"]	');
	$current_price->execute();

	$next_bid = new xpath($html, '//*[@id="bid-increment"]');
	$next_bid->execute();

	$buynow = new xpath($html, '//div[@id="buynowform"]');
	$buynow->execute();

	$quantity = new xpath($html, '//*[@id="quantity-left-2"]');
	$quantity->execute();

	$date_end = new xpath($html, '//*[@id="date-end"]');
	$date_end->execute();

	$date_start = new xpath($html, '//td[@class="small"]');
	$date_start->return = "array";
	$date_start->execute();

	$adetails = new xpath($html, '//td[@class="w150"]');
	$adetails->return = "array";
	$adetails->execute();

	$category = new xpath($html, '//div[@id="breadcrumbs"]');
	$category->execute();

	$itemname = $title->get();
	if (!isset($itemname)) exit;


	$info[] = array(
	//'category' => $category->get(),
	'title_name' => $title->get(),	
	'current_price' => $current_price->format_price($current_price->get()),
	'currency' => $current_price->get_currency($current_price->get()),
	'bid_increment' => $next_bid->format_price($next_bid->get()),
	'buynow' => $buynow->format_price($buynow->clear_buynow($buynow->get())),
	'description' => $description->get(),
	'transport_details'=> $adetails->parse_transport($adetails->get()),
	'payment_details' => $adetails->parse_payment($adetails->get()),
	'start_date' => $date_start->parse_start($date_start->get()),
	'end_date' => $date_end->get(),
	'quantity' => intval($quantity->get()),
	'location' => $location->replace_loc($location->get()),
	'image' => $image->get()
	);

} else {

	if (strlen($_POST['url'])<3) exit;

	$grab_path = trim($_POST["url"]);
	$grab_path = str_replace("&amp;","&",$grab_path);

	$url = urlencode($grab_path);
	@$html = new DOMDocument();

	// Elapsed time script

	list($usec, $sec) = explode(' ', microtime());
	$script_start = (float) $sec + (float) $usec;

	@$html->loadHtmlFile($url); 

	list($usec, $sec) = explode(' ', microtime());
	$script_end = (float) $sec + (float) $usec;

	// Elapsed time sctipt

	$id = new xpath($html,'//a'); //fetch tables
	$id->return = "array";
	$id->attribute = "href";
	$id->execute();

	$data = array();
	$data = $id->get();

	// Remove slash from string
	for ($i=0;$i<count($data);$i++) {
		$temp[] = str_replace("/","",$data[$i]);
	}

	// Collecting ID

	for ($i=0;$i<count($temp);$i++) {
		if (intval($temp[$i]) > 0) {
			$auctions[] = $temp[$i];
		}
	}
}

if (is_array($auctions)) {

		// Elapsed time script

	list($usec, $sec) = explode(' ', microtime());
	$script_start = (float) $sec + (float) $usec;

	for ($i=0;$i<count($auctions);$i++) {

		$grab_path = trim("http://www.osta.ee/".$auctions[$i]);
		$grab_path = str_replace("&amp;","&",$grab_path);

		$url = urlencode($grab_path);
		@$html = new DOMDocument();
		@$html->loadHtmlFile($url); 

		$description = new xpath($html, '//div[@class="itemdesc"]'); // div id name
		$description->return = "string";
		$description->childnodes = 1;
		$description->forbidden = array("Body");
		$description->execute();

		$title = new xpath($html, '//td[@class="title"]'); //Grab h1 data
		$title->execute();

		$image = new xpath($html, '//*[@id="image_big"]'); //Grab Images
		$image->return = "string";
		$image->attribute = "src";
		$image->execute();

		$adparams = new xpath($html, '//table[@class="details_main"]/tr[1]/td[1]'); //fetch tables
		$adparams->childnodes = 1;
		$adparams->return = "array";
		$adparams->execute();

		$location = new xpath($html, '//td[@class="w200 txtb"]	'); //fetch tables
		$location->return = "string";
		$location->childnodes = 10;
		$location->forbidden = array("Body");
		$location->execute();

		$current_price = new xpath($html, '//td[@id="current-price"]	');
		$current_price->execute();

		$next_bid = new xpath($html, '//*[@id="bid-increment"]');
		$next_bid->execute();

		$buynow = new xpath($html, '//div[@id="buynowform"]');
		$buynow->execute();

		$quantity = new xpath($html, '//*[@id="quantity-left-2"]');
		$quantity->execute();

		$date_end = new xpath($html, '//*[@id="date-end"]');
		$date_end->execute();

		$date_start = new xpath($html, '//td[@class="small"]');
		$date_start->return = "array";
		$date_start->execute();

		$adetails = new xpath($html, '//td[@class="w150"]');
		$adetails->return = "array";
		$adetails->execute();

		$category = new xpath($html, '//div[@id="breadcrumbs"]');
		$category->execute();




		$info[] = array(
		//'category' => $category->get(),
		'title_name' => $title->get(),	
		'current_price' => $current_price->format_price($current_price->get()),
		'currency' => $current_price->get_currency($current_price->get()),
		'bid_increment' => $next_bid->format_price($next_bid->get()),
		'buynow' => $buynow->format_price($buynow->clear_buynow($buynow->get())),
		'description' => $description->get(),
		'transport_details'=> $adetails->parse_transport($adetails->get()),
		'payment_details' => $adetails->parse_payment($adetails->get()),
		'start_date' => $date_start->parse_start($date_start->get()),
		'end_date' => $date_end->get(),
		'quantity' => intval($quantity->get()),
		'location' => $location->replace_loc($location->get()),
		'image' => $image->get()
		);


	}
	list($usec, $sec) = explode(' ', microtime());
	$script_end = (float) $sec + (float) $usec;
}



if (is_array($info) && sizeof($info)>0) {
	//print_r($info);
	for ($z=0;$z<count($info);$z++) {
		
		$list_id = $_POST["list_id"];
		
		$s_inc = "NO";
		if ($_POST["item_shipincl"]=="on") $s_inc = "YES";
		
		// AUCTION TYPE STANDARD / DUTCH
		
		$auctype = "STANDARD";
		$rp = "N"; // Default value, n/a
		$bn = "N";
		if (intval($info[$z]["quantity"])>0) $auctype = "DUTCH";
		if (intval($info[$z]["buynow"])>0) $bn = "Y";   

		$paymentMethods="";
		for ($m=0;$m<count($_POST['pmethod']);$m++) {
			$paymentMethods.=$_POST['pmethod'][$m]."<br>";
		} 
		// 1. Categories synchronization
		// 2. Convert date
			$s_d = substr($info[$z]["start_date"],0,2);
			$s_m = substr($info[$z]["start_date"],3,2);
			$s_y = substr($info[$z]["start_date"],6,4);

			$e_d = substr($info[$z]["end_date"],0,2);
			$e_m = substr($info[$z]["end_date"],3,2);
			$e_y = substr($info[$z]["end_date"],6,4);

			$min = substr($info[$z]["end_date"],11,2);
			$sec = substr($info[$z]["end_date"],14,2);
			
			$start_date = $s_y."-".$s_m."-".$s_d." ".$min.":".$sec.":"."00";
			$end_date = $e_y."-".$e_m."-".$e_d." ".$min.":".$sec.":"."00";

			@$start = mktime(0, 0, 0, $s_m, $s_d, $s_y);
			@$end = mktime(0, 0, 0, $e_m, $e_d, $e_y);
			$duration = round(($end - $start)/(60*60*24));

			$_SESSION["memberid"] = 28;


			 $query = "INSERT INTO probid_auctions_downloads(pm,auctype,owner_id, list_id, itemname, description, category_id, addcat_id, country, quantity, price, reserve_price, buy_now, currency, start_date, end_date, duration, s_included, pat_details, bn, rp
			) VALUES(
			'".$info[$z]["payment_details"]."','".$auctype."',".intval($_SESSION["memberid"]).",".$list_id.",'".$info[$z]["title_name"]."','".$info[$z]["description"]."',".intval($cat).",".intval($addcat).",'".$info[$z]["location"]."','".$info[$z]["quantity"]."',
			'".$info[$z]["current_price"]."','".$_POST["item_reserve_price"]."','".$info[$z]["buynow"]."','".$info[$z]["currency"]."','".$start_date."','".$end_date."',".$duration.",'".$s_inc."','".htmlspecialchars($info[$z]["transport_details"])."','".$bn."','".$rp."')";

			
			$result = mysqli_query($GLOBALS["___mysqli_ston"], $query);
			$new_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);

		// 3. Upload images
		// 3.1 Parsing images
			if ($_POST["method"]==1) $auctions[] = $_POST["url"];
			$grab_path = trim("http://www.osta.ee/index.php?fuseaction=item.images&item_id=".$auctions[$z]."");
			$grab_path = str_replace("&amp;","&",$grab_path);

			$url = urlencode($grab_path);
			@$html = new DOMDocument();
			@$html->loadHtmlFile($url);

			$images = new xpath($html, '//img'); //Grab Images
			$images->return = "array";
			$images->attribute = "src";
			$images->execute();
			
			$arrImages = $images->get();

			if ((is_array($arrImages)) && (sizeof($arrImages)>1)) {
				//3.2 Remove unuseful image
					unset($arrImages[0]);
			}

			
		// 3.3 Copying images from URL
			$limit = 5; // Supports only 5 image pro auction
			for ($x=1;$x<=count($arrImages);$x++) {
				if ($x<$limit) {
					$ext = getExtension($arrImages[$x]);
					@mkdir("images_temp/".$_SESSION["memberid"]."/",0777);
					@mkdir("images_temp/".$_SESSION["memberid"]."/".$new_id."",0777);
					@copy($arrImages[$x],"images_temp/".$_SESSION["memberid"]."/".$new_id."/pic_".$x.".".$ext);
					$files.= "pic_".$x.".".$ext.";";
				}
			}
			mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions_downloads SET images='".$files."' WHERE id=".$new_id."");
			

		$ext="";
		$files="";
	}
}

?>
<br />
<table cellpadding="4" cellspacing="0" width="40%">
<tr>
	<td colspan="2" align="center"><b>Results</b></td>
</tr>
<tr>
	<td align="right" width="120">Items imported:</td>
	<td><?=count($info)?></td>
</tr>
<tr>
	<td align="right" width="120">Elapsed time:</td>
	<td><?=$elapsed_time = round($script_end - $script_start, 3)?> s</td>
</tr>
</table>
?>
