<?
## v5.24 -> apr. 06, 2005
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] ".$_SESSION['memberusern']);

include("membersmenu.php"); 

## only generate the page if the member is the winner of the auction he is trying to pay for

$isWinner = getSqlNumber("SELECT id FROM probid_winners WHERE buyerid='".$_SESSION['memberid']."' AND auctionid='".$_GET['auctionid']."' AND payment_status='confirmed' AND directpayment_paid='0'");

$sellerId = getSqlField("SELECT ownerid FROM probid_auctions WHERE id='".$_GET['auctionid']."'","ownerid");
$auctionCurrency = getSqlField("SELECT currency FROM probid_auctions WHERE id='".$_GET['auctionid']."'","currency");
$isSeller = getSqlNumber("SELECT id FROM probid_users WHERE id='".$sellerId."' AND active='1'");

if ($isSeller&&$isWinner) {
	$seller = getSqlRow("SELECT * FROM probid_users WHERE id='".$sellerId."'"); ?>

<table width="100%" border="0" cellpadding="5" cellspacing="4">
   <tr align="center" class="c1">
      <td colspan="6"><?=$lang[paywithpaypalto]?>
         <?=$seller['username'];?>
         .
         <?=$lang[selitemsfrombelow];?></td>
   </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="4" class="errormessage">
   <tr>
      <td><?=$lang[checkoutnote]?></td>
   </tr>
</table>
<form action="<?=$_SERVER['PHP_SELF'];?>" method="get" name="cartform">
   <table width="100%" border="0" cellpadding="3" cellspacing="4">
      <tr class="c4">
         <td width="80" align="center"><strong>
            <?=$lang[select]?>
            </strong></td>
         <td width="80" align="center"><strong>
            <?=$lang[item_id]?>
            </strong></td>
         <td><strong>
            <?=$lang[itemname]?>
            </strong></td>
         <td width="140" align="center"><strong>
            <?=$lang[purchasedate]?>
            </strong></td>
         <td width="80" align="center"><strong>
            <?=$lang[price]?>
            </strong></td>
         <td width="80" align="center"><strong>
            <?=$lang[shipping2];?>
            </strong></td>
         <td width="80" align="center"><strong>
            <?=$lang[insurance];?>
            </strong></td>
         <td width="80" align="center"><strong>
            <?=$lang[quant]?>
            </strong></td>
      </tr>
      <tr class="c5">
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
         <td><img src="themes/<?=$setts['default_theme'];?>/img/pixel.gif" width="1" height="1"></td>
      </tr>
      <input type="hidden" name="auctionid" value="<?=$_GET['auctionid'];?>">
      <? 
		$w_id=",";
		for ($i=0; $i<count($_REQUEST['wid']); $i++) $w_id .= $_REQUEST['wid'][$i].","; 
		
		$getAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT w.auctionid, w.id, a.itemname, w.amount, a.postage_costs, a.insurance, a.auctiontype, w.quant_offered, a.enddate, a.currency FROM 
		probid_auctions a, probid_winners w WHERE a.ownerid='".$sellerId."' AND a.id=w.auctionid AND a.ownerid=w.sellerid AND w.buyerid='".$_SESSION['memberid']."' AND 
		a.active=1 AND w.payment_status='confirmed' AND w.directpayment_paid=0 AND a.acceptdirectpayment=1 AND a.currency='".$auctionCurrency."'");
		while ($wonAuction = mysqli_fetch_array($getAuctions)) { ?>
      <tr class="<? echo (($count++)%2==0)?"c2":"c3";?>" valign="top">
         <td align="center"><input name="wid[]" type="checkbox" value="<?=$wonAuction['id'];?>" <? echo (@eregi(",".$wonAuction['id'].",",$w_id)) ? "checked" : ""; ?>></td>
         <td align="center">#
            <?=$wonAuction['auctionid'];?></td>
         <td><?=$wonAuction['itemname'];?></td>
         <td align="center"><? echo displaydatetime($wonAuction['enddate'],$setts['date_format']);?></td>
         <td align="center"><?=displayAmount($wonAuction['amount'],$wonAuction['currency'],"YES"); ?></td>
         <td align="center"><?=displayAmount($wonAuction['postage_costs'],$wonAuction['currency'],"YES"); ?></td>
         <td align="center"><?=displayAmount($wonAuction['insurance'],$wonAuction['currency'],"YES"); ?></td>
         <td align="center"><? echo ($wonAuction['auctiontype']=="standard") ? "1" : $wonAuction['quant_offered'];?></td>
      </tr>
      <? } ?>
   </table>
   <br>
   <?
	$total_price = 0;
	$total_postage = 0;
	$total_insurance = 0;
	for ($i=0; $i<count($_REQUEST['wid']);$i++) { 
		$details = getSqlRow("SELECT w.id, w.amount, a.postage_costs, a.insurance, a.auctiontype, w.quant_offered FROM 
		probid_auctions a, probid_winners w WHERE a.ownerid='".$sellerId."' AND a.id=w.auctionid AND a.ownerid=w.sellerid AND w.buyerid='".$_SESSION['memberid']."' AND 
		a.active=1 AND w.payment_status='confirmed' AND w.directpayment_paid=0 AND a.acceptdirectpayment=1 AND w.id='".$_REQUEST['wid'][$i]."'");

		$quant = ($details['auctiontype']=="standard") ? 1 : $details['quant_offered'];
		$total_price += $quant * $details['amount'];
		$total_postage += $quant * $details['postage_costs'];
		$total_insurance += $quant * $details['insurance'];
	}
	if ($_REQUEST['total_insurance']!="") $total_insurance = $_REQUEST['total_insurance']; ?>
   <table width=""  border="0" cellspacing="3" cellpadding="2" align="center" class="border">
      <tr class="c2">
         <td><strong>
            <?=$lang[totalprice];?>
            </strong></td>
         <td><input name="total_price" type="text" size="20" readonly value="<?=number_format($total_price,2,".","");?>" style="text-align:right;"></td>
      </tr>
      <tr class="c3">
         <td><strong>
            <?=$lang[totalshipping];?>
            </strong></td>
         <td><input name="total_shipping" type="text" size="20" readonly value="<?=number_format($total_postage,2,".","");?>" style="text-align:right;"></td>
      </tr>
      <tr class="c2">
         <td><strong>
            <?=$lang[totalinsurance];?>
            </strong></td>
         <td><input name="total_insurance" type="text" size="20" value="<?=number_format($total_insurance,2,".","");?>" style="text-align:right;"></td>
      </tr>
      <tr>
         <td colspan="2" align="center"><input name="submit" type="submit" value="<?=$lang[updcart];?>"></td>
      </tr>
   </table>
</form>
<br>
<? 
if (count($_REQUEST['wid'])>0) {
	$paymentAmount = $total_price + $total_postage + $total_insurance;
	$shipping = $total_postage + $total_insurance;
	$notifyUrl=$path."paymentprocess.php?table=101";
	$returnUrl=$path."paymentdone.php";
	$failureUrl=$path."paymentfailed.php";
	
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"4\" cellpadding=\"4\" class=\"paymenttable\">\n";
  	echo "	<tr>\n";
	### the PG image will go below
	echo "    	<td width=\"160\" class=\"paytable1\"><img src=\"themes/".$setts[default_theme]."/img/system/paypal_logo.gif\"></td>\n";
	### the description will go below
	echo "	 	<td class=\"paytable2\" width=\"100%\">$lang[paypal_description_new]<br>$lang[ppyouhave] <strong>".displayAmount($paymentAmount,$auctionCurrency)."</strong></td>\n";
	### the payment gateway from will go below
	$dpemail = getSqlField("SELECT u.paypalemail as paypalemail FROM probid_users AS u, probid_auctions AS a WHERE u.id=".$_SESSION['memberid']." AND a.id='".$_GET['auctionid']."'","paypalemail");
	echo "		<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">				\n";
	echo "		<td class=\"paytable3\">								\n".
		  "		 <input type=\"hidden\" name=\"cmd\" value=\"_cart\">								\n".
	     "		 <input type=\"hidden\" name=\"upload\" value=\"1\">							\n".
		  "	 	 <input type=\"hidden\" name=\"business\" value=\"".$dpemail."\">		\n".
    	  "		 <input type=\"hidden\" name=\"currency_code\" value=\"".$auctionCurrency."\">		\n".
		  "		 <input type=\"hidden\" name=\"receiver_email\" value=\"".$dpemail."\">\n".
	     "		 <input type=\"hidden\" name=\"return\" value=\"".$returnUrl."\">					\n".
	     "		 <input type=\"hidden\" name=\"cancel_return\" value=\"".$failureUrl."\">			\n";

	## this setting is for the direct payment feature
	echo "		 <input type=\"hidden\" name=\"handling_cart\" value=\"".$total_insurance."\">\n";
	if (count($_REQUEST['wid'])>0) {
		for ($i=0; $i<count($_REQUEST['wid']);$i++) { 
			$cartid=$i+1;
			$details = getSqlRow("SELECT w.id,a.itemname, w.amount, a.postage_costs, a.insurance, a.auctiontype, w.quant_offered,w.auctionid, a.currency FROM 
			probid_auctions a, probid_winners w WHERE a.ownerid='".$sellerId."' AND a.id=w.auctionid AND a.ownerid=w.sellerid AND w.buyerid='".$_SESSION['memberid']."' AND 
			a.active=1 AND w.payment_status='confirmed' AND w.directpayment_paid=0 AND a.acceptdirectpayment=1 AND w.id='".$_REQUEST['wid'][$i]."'");
			
			//echo "<input type=\"hidden\" name=\"add\" value=\"".$details['auctionid']."\">\n";
			echo "<input type=\"hidden\" name=\"item_name_$cartid\" value=\"".$details['itemname']."\">\n";
			echo "<input type=\"hidden\" name=\"item_number_$cartid\" value=\" #".$details['auctionid']."\">\n";
			echo "<input type=\"hidden\" name=\"amount_$cartid\" value=\"".number_format($details['amount'],2,".","")."\">\n";
			echo "<input type=\"hidden\" name=\"shipping_$cartid\" value=\"".number_format($details['postage_costs'],2,".","")."\">\n";
			echo "<input type=\"hidden\" name=\"shipping2_$cartid\" value=\"".number_format($details['postage_costs'],2,".","")."\">\n";
			$quant = ($details['auctiontype']=="standard") ? 1 : $details['quant_offered'];
		 	//echo "<input type=\"hidden\" name=\"undefined_quantity\" value=\"0\">\n";
			echo "<input type=\"hidden\" name=\"quantity_$cartid\" value=\"".$quant."\">\n";
			echo "<input type=\"hidden\" name=\"currency_code_$cartid\" value=\"".$details['currency']."\">\n";
		
			$transactionId.=$details['auctionid']."_".$_SESSION['memberid'].";";
		}
	}
	$transactionId.="101";
	echo "<input type=\"hidden\" name=\"custom\" value=\"".$transactionId."\">\n";
	
	echo " 		 <input type=\"hidden\" name=\"notify_url\" value=\"".$notifyUrl."\">				\n".
	     " 		 <input name=\"submit\" type=\"image\" src=\"themes/".$setts[default_theme]."/img/system/but_pay.gif\" border=\"0\">\n";
	echo "		</td></form>\n";
  	echo "	</tr>\n";
	echo "</table>";
} else echo "<p align=center>$lang[selatleastonemsg]</p>";?>
<br>
<? } else {
	echo "<p>$lang[checkoutpageerror]</p>";
}

include ("themes/".$setts['default_theme']."/footer.php"); } ?>
