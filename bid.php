<?
## v5.24 -> apr. 06, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php?auctionid=".$_REQUEST['auctionid']."'</script>";
} else {

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

## ADD AUTHENTICATION - ALEKSEI

$userDetails = getSqlRow("SELECT `authenticated`, `isikukood`, `realname` FROM probid_users WHERE id='".$_SESSION['memberid']."'");
if ( 'Y' == strtoupper( $userDetails['authenticated'] ) )
{

$action = $_REQUEST['action'];
$auctionEnddate = getSqlField("SELECT enddate FROM probid_auctions WHERE id='".$_POST['auctionid']."'","enddate");
$daysleft=daysleft($auctionEnddate,$setts['date_format']);
$auctionTemp = getSqlRow("SELECT id, quantity, auctiontype, bidstart, bn, bnvalue, bi, bivalue, nrbids, 
maxbid, currency, deleted, ownerid FROM probid_auctions WHERE id='".$_POST['auctionid']."'");

## calculate the min bid
if ($auctionTemp['auctiontype']=="standard") {
	if ($auctionTemp['maxbid']==0) $minimumBid = $auctionTemp['bidstart'];
	else $minimumBid = setMinBid($auctionTemp['maxbid'],$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
} else {
	$maximumBid = $auctionTemp['maxbid'];
	$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE 
	bidamount='".$maximumBid."' AND auctionid='".$auctionTemp['id']."'");
	$the_quantity = 0;
	while ($bids = mysqli_fetch_array($getBids)) {
		$the_quantity += $bids['quantity'];
	}
	if ($auctionTemp['maxbid']==0) $minimumBid=$auctionTemp['bidstart'];
	else if ($the_quantity<$auctionTemp['quantity']) $minimumBid=$auctionTemp['maxbid'];
	else $minimumBid=setMinBid($auctionTemp['maxbid'],$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
} 

$minbid = $minimumBid;
$isBanned = getSqlNumber("SELECT id FROM probid_blocked_users WHERE userid='".$_SESSION['memberid']."' AND ownerid='".$auctionTemp['ownerid']."'");

if ($isBanned) {
	$bannedDetails = getSqlRow("SELECT * FROM probid_blocked_users WHERE userid='".$_SESSION['memberid']."' AND ownerid='".$auctionTemp['ownerid']."'");
   echo '<table width="100%" border="0" cellspacing="2" cellpadding="2" class="errormessage"> ';
	echo '	<tr> ';
	echo '		<td>'.(($bannedDetails['show_reason']) ? "<strong>".$lang[excludedmsg_reason]."</strong><br>".$bannedDetails['block_reason'] : $lang[excludedmsg_noreason]).'</td> ';
	echo '	</tr> ';
	echo '</table> ';
} else if ($auctionTemp['bidstart']==$auctionTemp['bnvalue']&&$auctionTemp['bn']=="Y") {
	echo "<p align=center class=contentfont>".$lang[buynowonlyauctionalalert]."</p>";
} else if ($daysleft <=0) {
	echo "<p align=center class=contentfont>".$lang[nomorebidding]."</p>";
} else if ($auctionTemp['deleted']==1) {
	echo "<p align=center class=contentfont>".$lang[nomorebidding]."</p>";
} else { ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td class="contentfont"><br>
         <? 
			if ($_POST['maxbid']!=""&&!is_numeric($_POST['maxbid'])) $maxbid = 0;
			else $maxbid = $_POST['maxbid'];
			$enterbid = "yes";
			if ($maxbid<$minbid) { 
				$message="<strong>$lang[err_maxbid]</strong>";
				$enterbid="no";
				$action="bid_NE";
			} else {
				## bid placing begins here
		
				## v5.22 additions
				## first loop until bid flag is 0 
				$bid_loop = 1;
				while ($bid_loop == 1) {
					$bid_loop = getSqlField("SELECT bid_in_progress FROM probid_auctions WHERE id='".$auctionTemp['id']."'","bid_in_progress");
				}
				## v5.22 addition -> we will add a flag to the auction saying that a bid is placed
				$addInProgressFlag = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET bid_in_progress='1' WHERE id='".$auctionTemp['id']."'");
				$underReserve = FALSE;
				if (isset($_POST['confirmbidok'])) {
					$resArray = getSqlRow("SELECT rp, rpvalue FROM probid_auctions WHERE id='".$_POST['auctionid']."'");
					$isReserve = $resArray['rp'];
					$reserveValue = $resArray['rpvalue'];
					if ($maxbid < $reserveValue && $isReserve == "Y") { 
						$underReserve = TRUE;
					} else if ($maxbid >= $reserveValue && $isReserve == "Y" && $minbid <= $reserveValue) $minbid=$reserveValue;
				}
			
				if (isset($_POST['confirmbidok'])&&$enterbid=="yes"&&$auctionTemp['auctiontype']=="standard") {
					## bid submission for standard auctions
					if ($auctionTemp['nrbids']==0) {
						$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
						(auctionid,bidderid,bidamount,date,proxy) VALUES
						
						('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$minbid."','".$timeNowMysql."','".$maxbid."')");
						##('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$maxbid."','".$timeNowMysql."','".$maxbid."')");
						## Proxy bid repair - ALEKSEI HODUNKOV
						
						
						
						$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
						$insertProxyBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_proxybid 
						(bidderid,bidamount,auctionid) VALUES 
						('".$_SESSION['memberid']."','".$maxbid."','".$_POST['auctionid']."')");
						$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
						(biddername,auctionid,amount,date,bidid) VALUES 
						
						('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$minbid."','".$timeNowMysql."','".$bid_id."')");
						##('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$maxbid."','".$timeNowMysql."','".$bid_id."')");
						## Proxy bid repair - ALEKSEI HODUNKOV
						
						
						$updatAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
						
						maxbid='".$minbid."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");
						##maxbid='".$maxbid."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");
						## Proxy bid repair - ALEKSEI HODUNKOV
												
						
						$action="bid_SUBMITTED";
					} else if ($auctionTemp['nrbids']>0) {
						$proxyDetails = getSqlRow("SELECT * FROM probid_proxybid WHERE auctionid='".$_POST['auctionid']."'");
						if ($_SESSION['memberid']==$proxyDetails['bidderid']) {
							## user shouldnt be able to lower their proxy bid, allows users to meet the reserve by updating their proxy bid
							if ($maxbid < $proxyDetails['bidamount']) {
								$currency=getSqlField("SELECT * FROM probid_auctions WHERE id='".$_POST['auctionid']."'", "currency");
								$message="<strong>$lang[err_proxytoolow] (" . displayAmount($proxyDetails['bidamount'], $currency) . ")</strong>";
								$action="bid_NE";
							} else {
								$updateProxyBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_proxybid SET 
								bidamount='".$maxbid."' WHERE auctionid='".$_POST['auctionid']."'");
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$updateProxyBid2 = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET proxy='".$maxbid."' WHERE 
								valja=0 AND invalid=0 AND auctionid='".$_POST['auctionid']."' AND bidderid='".$_SESSION['memberid']."'");
								$action="BID_IncPROXY";
								$auctArray = getSqlRow("SELECT maxbid, rp, rpvalue FROM probid_auctions WHERE id='".$_POST['auctionid']."'");
								$currentHighBid = $auctArray['maxbid'];
								$isReserve = $auctArray['rp'];
								$reserveValue = $auctArray['rpvalue'];
								if ($isReserve == "Y" && $currentHighBid < $reserveValue) {
									$invalidatePreviousBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
									invalid=1 WHERE auctionid='".$_POST['auctionid']."' AND bidderid='".$_SESSION['memberid']."'");
									## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
									$outbidBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET valja=1 WHERE auctionid='".$_POST['auctionid']."'");
									if ($maxbid > $reserveValue) $bidToSet = $reserveValue;
									else $bidToSet = $maxbid;
									$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
									(auctionid,bidderid,bidamount,date,proxy) VALUES 
									('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$bidToSet."','".$timeNowMysql."','".$maxbid."')");
									$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
									$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
									(biddername,auctionid,amount,date,bidid) VALUES 
									('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$bidToSet."','".$timeNowMysql."','".$bid_id."')");
									$updatAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
									maxbid='".$bidToSet."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");						
									$action="bid_SUBMITTED";
								}
							}
							// End Change
						} else {
							if ($proxyDetails['bidamount']>=$maxbid) {
								$invalidatePreviousBid=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
								invalid=1 WHERE auctionid='".$_POST['auctionid']."' AND bidderid='".$_SESSION['memberid']."'");
								$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
								(auctionid,bidderid,bidamount,date,proxy) VALUES
								('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$maxbid."','".$timeNowMysql."','".$maxbid."')");
								$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
								$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
								(biddername,auctionid,amount,date,bidid) VALUES 
								('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$maxbid."','".$timeNowMysql."','".$bid_id."')");
								$updatAuction=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
								nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");
		
								$newbid=setMinBid($maxbid,$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
								
								if ($newbid>=$proxyDetails['bidamount']) $bidToSet=$proxyDetails['bidamount'];
								else $bidToSet=$newbid;
		
								$minbid=setMinBid($bidToSet,$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);	
								$invalidatePreviousBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
								invalid=1 WHERE auctionid='".$_POST['auctionid']."' AND bidderid='".$proxyDetails['bidderid']."'");
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$outbidBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
								valja=1 WHERE auctionid='".$_POST['auctionid']."'");
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
								(auctionid,bidderid,bidamount,date,valja,proxy) VALUES
								('".$_POST['auctionid']."','".$proxyDetails['bidderid']."','".$bidToSet."','".$timeNowMysql."',0,'".$proxyDetails['bidamount']."')");
								$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
								$proxyBidderName=getSqlField("SELECT username FROM probid_users WHERE id='".$proxyDetails['bidderid']."'","username");
		
								$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
								(biddername,auctionid,amount,date,bidid) VALUES 
								('".$proxyBidderName."','".$_POST['auctionid']."','".$bidToSet."','".$timeNowMysql."','".$bid_id."')");
								$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
								maxbid='".$bidToSet."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");
								$action="bid_NE";
								$message="<strong>$lang[err_bidnothigh]</strong>";
							} else {
								## also submit the proxy bid in probid_bids and probid_bids_history. (v5.23c addition)
								$invalidateProxyBid = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
								invalid=1 WHERE auctionid='".$_POST['auctionid']."' AND bidderid='".$proxyDetails['bidderid']."'");
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$addProxyBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
								(auctionid,bidderid,bidamount,date,valja,proxy) VALUES
								('".$_POST['auctionid']."','".$proxyDetails['bidderid']."','".$proxyDetails['bidamount']."','".$timeNowMysql."',1,'".$proxyDetails['bidamount']."')");
								$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
								$proxyBidderName=getSqlField("SELECT username FROM probid_users WHERE id='".$proxyDetails['bidderid']."'","username");
								$addProxyBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
								(biddername,auctionid,amount,date,bidid) VALUES 
								('".$proxyBidderName."','".$_POST['auctionid']."','".$proxyDetails['bidamount']."','".$timeNowMysql."','".$bid_id."')"); 
								//$newbid=setMinBid($minbid,$bi,$bivalue,$auctiontype);
								$invalidatePreviousBid=mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
								invalid=1 WHERE auctionid='".$_POST['auctionid']."' AND bidderid='".$_SESSION['memberid']."'");
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$outbidBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET 
								valja=1 WHERE auctionid='".$_POST['auctionid']."'");
								
								## $newbid=$maxbid;
								## Proxy bid repair - ALEKSEI HODUNKOV
								$newbid=$minbid;
								
								//echo $newbid."I";
								while ($newbid<=$proxyDetails['bidamount']) {
									$newbid=setMinBid($newbid,$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);	
								}
								if ($newbid>=$maxbid) $bidToSet=$maxbid;
								else $bidToSet=$newbid;
		
								$minbid=setMinBid($bidToSet,$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);	
								$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
								(auctionid,bidderid,bidamount,date,proxy) VALUES
								('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$bidToSet."','".$timeNowMysql."','".$maxbid."')");
								$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
								$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
								(biddername,auctionid,amount,date,bidid) VALUES 
								('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$bidToSet."','".$timeNowMysql."','".$bid_id."')");
								$updateProxy = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_proxybid SET 
								bidderid='".$_SESSION['memberid']."',bidamount='".$maxbid."' WHERE auctionid='".$_POST['auctionid']."'");
								$updateAuction = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
								maxbid='".$bidToSet."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");						
								$action="bid_SUBMITTED";
							}
						}
					}
				} ## end of bid submission for standard auctions
		
				### Dutch auctions
				if (isset($_POST['confirmbidok'])&&$enterbid=="yes"&&$auctionTemp['auctiontype']=="dutch") {
					## bid submission for dutch auctions
					if ($_POST['quantity']<=$auctionTemp['quantity']) {
						if ($auctionTemp['nrbids']==0) {
							$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
							(auctionid,bidderid,bidamount,date,quantity,proxy) VALUES
							('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$minbid."','".$timeNowMysql."','".$_POST['quantity']."','".$maxbid."')");
							$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
							$updateProxy = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_proxybid 
							(bidderid,bidamount,auctionid) VALUES 
							('".$_SESSION['memberid']."','".$maxbid."','".$_POST['auctionid']."')");
							$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
							(biddername,auctionid,amount,date,quantity,bidid)	VALUES 
							('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$minbid."','".$timeNowMysql."','".$_POST['quantity']."','".$bid_id."')");
							$updateAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
							maxbid='".$minbid."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");						
							$action="bid_SUBMITTED";
							//echo "BID0";
						} else {
							$action="bid_SUBMITTED";
							$proxyDetails = getSqlRow("SELECT * FROM probid_proxybid WHERE auctionid='".$_POST['auctionid']."'");
							### calculate minimum bid to enter
							## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
							$getQuantity = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE valja=0 AND invalid=0 AND auctionid='".$_POST['auctionid']."'");
							while ($quantityRow=mysqli_fetch_array($getQuantity)) {
								$totalQuantity+=$quantityRow['quantity'];
							}
							$quantityLeft = $auctionTemp['quantity'] - $totalQuantity;
								
							$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
							(auctionid,bidderid,bidamount,date,quantity,proxy) VALUES 
							('".$_POST['auctionid']."','".$_SESSION['memberid']."','".$minbid."','".$timeNowMysql."','".$_POST['quantity']."','".$maxbid."')");				
							$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
							$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history 
							(biddername,auctionid,amount,date,quantity,bidid) VALUES 
							('".$_SESSION['memberusern']."','".$_POST['auctionid']."','".$minbid."','".$timeNowMysql."','".$_POST['quantity']."','".$bid_id."')");
							$updateAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
							maxbid='".$minbid."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");						
							$currentProxy = $proxyDetails['bidamount'];
							if ($maxbid>=$proxyDetails['bidamount']) {
								$updateProxy = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_proxybid SET 
								bidderid='".$_SESSION['memberid']."',bidamount='".$maxbid."' WHERE auctionid='".$_POST['auctionid']."'");
								$currentProxy = $maxbid;
							}	
													
							if ($_POST['quantity']>$quantityLeft) {
								### v5.20 -> we out all bids
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$outbidBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET valja=1 WHERE auctionid='".$_POST['auctionid']."' AND valja=0 AND invalid=0");
															
								### v5.20 -> new dutch auction bidding function
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE  auctionid='".$_POST['auctionid']."' AND valja='1' AND invalid='0' AND deleted!='1' AND proxy>='".$maxbid."' ORDER BY id ASC");
								$nbOutbidBids = mysqli_num_rows($getBids);
																
								$cnt = 0;
									
								$bids_id = array();	$bids_bidderid = array(); $bids_bidamount = array(); $bids_quantity = array();
								$bids_proxy = array(); $bids_out = array();	$bids_invalid = array();
															
								while ($bids = mysqli_fetch_array($getBids)) {
									$bids_id[$cnt] = $bids['id'];
									$bids_bidderid[$cnt] = $bids['bidderid'];
									$bids_bidamount[$cnt] = $bids['bidamount'];
									$bids_quantity[$cnt] = $bids['quantity'];
									$bids_proxy[$cnt] = $bids['proxy'];
									## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
									$bids_out[$cnt] = $bids['valja'];
									$bids_invalid[$cnt] = $bids['invalid'];
									
									$cnt++;
								}
																
								$loop = ($nbOutbidBids>0) ? TRUE : FALSE;
								$firstCnt = 0;
								$lastCnt = $cnt-1;
								while ($loop) {
									$minActiveBid = $bids_bidamount[$lastCnt];
									for ($i=$firstCnt; $i<$cnt; $i++)
										if ($minActiveBid > $bids_bidamount[$i]) $minActiveBid = $bids_bidamount[$i];
									
									//echo "BB: ".$bids_proxy[$firstCnt]." ".$minActiveBid." FCNT: ".$bids_id[$firstCnt]." LCNT: ".$bids_id[$lastCnt]."<br>";	
										
									$current_maxbid = getSqlField("SELECT maxbid FROM probid_auctions WHERE id='".$_POST['auctionid']."'","maxbid");
									$newbid=setMinBid($current_maxbid,$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
		
									if ($bids_proxy[$firstCnt] > $minActiveBid&&$bids_id[$firstCnt]!=$bids_id[$lastCnt]) {
										//echo "H: ".$bids_proxy[$firstCnt]."<br>";
										if ($newbid>$bids_proxy[$firstCnt]) $newbid=$bids_proxy[$firstCnt];
										## placebid - newbid
										$insertBid = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids 
										(auctionid,bidderid,bidamount,date,quantity,proxy) VALUES 
										('".$_POST['auctionid']."','".$bids_bidderid[$firstCnt]."','".$newbid."','".$timeNowMysql."',
										'".$bids_quantity[$firstCnt]."','".$bids_proxy[$firstCnt]."')") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));				
										$bid_id = ((is_null($___mysqli_res = mysqli_insert_id($GLOBALS["___mysqli_ston"]))) ? false : $___mysqli_res);
										
										$bidder_name = getSqlField("SELECT username FROM probid_users WHERE id='".$bids_bidderid[$firstCnt]."'","username");
																								
										$insertBidHistory = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_bids_history (biddername,auctionid,amount,date,quantity,bidid) 
										VALUES ('".$bidder_name."','".$_POST['auctionid']."','".$newbid."','".$timeNowMysql."','".$bids_quantity[$firstCnt]."','".$bid_id."')");
										if ($newbid>$current_maxbid)
										$updateAuctions = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET maxbid='".$newbid."',nrbids=nrbids+1 WHERE id='".$_POST['auctionid']."'");						
																
										//$setInvalidBid = mysql_query("UPDATE probid_bids SET invalid=1 WHERE id='".$bids_id[$firstCnt]."'");
										$newBidsPlaced++;
																		
										$bids_id[$cnt] = $bid_id;
										$bids_bidderid[$cnt] = $bids_bidderid[$firstCnt];
										$bids_bidamount[$cnt] = $newbid;
										$bids_quantity[$cnt] = $bids_quantity[$firstCnt];
										$bid_proxy[$cnt] = $bids_proxy[$firstCnt];
										$bids_out[$cnt] = 1;
										$bids_invalid[$cnt] = 0;
																		
										$bids_invalid[$firstCnt] = 1;
										$cnt++;
										$firstCnt++;
										$lastCnt++;
									} else {
										$remQuant[$remcnt] = getSqlField("SELECT sum(quantity) AS sum_quant FROM probid_bids WHERE auctionid='".$_POST['auctionid']."' AND invalid='0' AND proxy>'".$newbid."'","sum_quant");
										if ($remQuant[$remcnt]>$auctionTemp['quantity']) $loop = TRUE;
										else $loop = FALSE;
										$remcnt++;
									}
									//$tmpcnt++;
									//if ($tmpcnt>10) $loop = FALSE;
								}
								
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV								
								for ($i=0; $i<$cnt; $i++) {
									$updBid[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET valja='".$bids_out[$i]."', invalid='".$bids_invalid[$i]."' WHERE id='".$bids_id[$i]."'");
								}
																														
								### activate all bids up to the available quantity
								$o_cnt=0;
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$getAllOutBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE auctionid='".$_POST['auctionid']."' AND invalid='0' AND valja='1' AND deleted!='1' ORDER BY id DESC");
								while ($allOutBids = mysqli_fetch_array($getAllOutBids)) {
									$outbid_id[$o_cnt] = $allOutBids['id']; 
									$outbid_quantity[$o_cnt] = $allOutBids['quantity'];
									$o_cnt++; 
								}
									
								$rem_qnt = $auctionTemp['quantity'];
								for ($i=0; $i<$o_cnt; $i++) {
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
									if ($rem_qnt>0) $activateBid[$i] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET valja='0' WHERE id='".$outbid_id[$i]."'");
									$rem_qnt-=$outbid_quantity[$i];
								}
									
								### now equalize all active bids to the lowest active bid.
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$lowestActiveBid = getSqlField("SELECT proxy FROM probid_bids WHERE auctionid='".$_POST['auctionid']."' AND invalid='0' AND valja='0' AND deleted!='1' ORDER BY proxy ASC LIMIT 0,1","proxy");
								$newbid=setMinBid($lowestActiveBid,$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
								
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV			
								$equalizeActiveBids = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET bidamount='".$lowestActiveBid."' WHERE auctionid='".$_POST['auctionid']."' AND invalid='0' AND valja='0' AND deleted!='1'");
								## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
								$getAB = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, bidamount FROM probid_bids WHERE  auctionid='".$_POST['auctionid']."' AND invalid='0' AND valja='0' AND deleted!='1'");
								while ($updHist = mysqli_fetch_array($getAB)) {
									$updBH[$ucnt] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids_history SET amount='".$updHist['bidamount']."' WHERE bidid='".$updHist['id']."'");
									$updateMaxBid[$ucnt++] = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET maxbid='".$updHist['bidamount']."' WHERE id='".$_POST['auctionid']."'");						
								}
							}
						} ## end of dutch auction bidding process
					} else {
						$message="<strong>$lang[err_quant]</strong>";
						$action="bid_NE";
					}
				} ## end of bid submission for dutch auctions
				## v5.22 addition -> we will now reset the in progress flag
				$removesInProgressFlag = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET bid_in_progress='0' WHERE id='".$auctionTemp['id']."'");
				
				if ($underReserve) { echo "<p align=center class=contentfont>".$lang[underresdesc]."</p>"; $action_sec = "bid_NE"; $action = "bid_NE"; } 
				## bid placing ends here
			}
			if ($action=="bid_NE"||$action=="bid_CONF") { ?>
         <p align="center">
            <?=$message;?>
         </p>
         <form action="<?=$_SERVER['PHP_SELF'];?>" method="post">
            <input type="hidden" name="action_sec" value="<?=$action_sec;?>">
            <input type="hidden" name="itemname" value="<?=remSpecialChars($_POST['itemname']);?>">
            <input type="hidden" name="auctionid" value="<?=$_POST['auctionid'];?>">
            <table width="100%" border="0" cellpadding="4" cellspacing="2">
               <tr class="c1">
                  <td align="right"><strong>
                     <?=$lang[itemname]?>
                     :</strong></td>
                  <td><strong>
                     <?=$_POST['itemname'];?>
                     </strong></td>
               </tr>
               <tr class="c5">
                  <td></td>
                  <td></td>
               </tr>
               <tr class="c3">
                  <td width="150" align="right"><strong>
                     <?=$lang[yourbid]?>
                     :</strong></td>
                  <td><input name="maxbid" type="text" id="maxbid" value="<?=$maxbid;?>" size="15"></td>
               </tr>
               <? if ($auctionTemp['auctiontype']=="dutch") { ?>
               <tr class="c2">
                  <td align="right"><strong>
                     <?=$lang[quant]?>
                     :</strong></td>
                  <td valign="top"><input name="quantity" type="text" id="quantity" value="<?=$_POST['quantity'];?>" size="8"></td>
               </tr>
               <? } else { ?>
               <input name="quantity" type="hidden" id="quantity" value="1">
               <? } ?>
               <?
					$auctionTemp = getSqlRow("SELECT id, quantity, auctiontype, bidstart, bn, bnvalue, bi, bivalue, nrbids, 
					maxbid, currency, deleted FROM probid_auctions WHERE id='".$_POST['auctionid']."'");
					
					## calculate the min bid
					if ($auctionTemp['auctiontype']=="standard") {
						if ($auctionTemp['maxbid']==0) $minimumBid = $auctionTemp['bidstart'];
						else $minimumBid = setMinBid($auctionTemp['maxbid'],$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
					} else {
						$maximumBid = $auctionTemp['maxbid'];
						$getBids = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_bids WHERE 
						bidamount='".$maximumBid."' AND auctionid='".$auctionTemp['id']."'");
						$the_quantity = 0;
						while ($bids = mysqli_fetch_array($getBids)) {
							$the_quantity += $bids['quantity'];
						}
						if ($auctionTemp['maxbid']==0) $minimumBid=$auctionTemp['bidstart'];
						else if ($the_quantity<$auctionTemp['quantity']) $minimumBid=$auctionTemp['maxbid'];
						else $minimumBid=setMinBid($auctionTemp['maxbid'],$auctionTemp['bi'],$auctionTemp['bivalue'],$auctionTemp['auctiontype']);
					} 
					
					$minbid = $minimumBid; ?>
               <tr class="c2">
                  <td align="right"><strong>
                     <?=$lang[minbid]?>
                     :</strong></td>
                  <td><?
							$currency=getSqlField("SELECT * FROM probid_auctions WHERE id='".$_POST['auctionid']."'","currency");
							echo displayAmount($minbid,$currency);?></td>
               </tr>
               <tr class="c3" valign="top">
                  <td align="right"><b>
                     <?=$lang[shipping]?>
                     :</b></td>
                  <td><? 
							$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_POST['auctionid']."' AND active=1");
							if ($auctionDetails['sc']=="BP") echo "".$lang[buyerpaysshipment].",&nbsp;";
							else echo "".$lang[sellerpaysshipment].",&nbsp;";
							if ($auctionDetails['scint']=="Y") echo "$lang[sellershipinternat]"; 
							else echo "$lang[seller_not_shipinternat]";
							if ($setts['shipping_costs']==1) {
								echo "<br>";
								if ($auctionDetails['postage_costs']>0)
									echo "<strong>".$lang[postagecosts].":</strong> ".displayAmount($auctionDetails['postage_costs'],$auctionDetails['currency'],TRUE)."; "; 
									if (trim($auctionDetails['insurance'])!="")
									echo "<strong>".$lang[insurance].":</strong> ".$auctionDetails['insurance']."; "; 
									if (trim($auctionDetails['type_service'])!="")
									echo "<strong>".$lang[servicetype].":</strong> ".$auctionDetails['type_service']."; ";
							} ?>
                  </td>
               </tr>
               <tr class="c2">
                  <td align="right"><b>
                     <?=$lang[payment]?>
                     :</b></td>
                  <td><?
							$auctionDetails = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_POST['auctionid']."' AND active=1");
							$paymentMethods=explode("<br>",addSpecialChars($auctionDetails['pm']));
							$nbPaymentMethods=count($paymentMethods);
						 
							for ($i=0;$i<$nbPaymentMethods;$i++) { 
								echo $paymentMethods[$i];
								if (trim($paymentMethods[$i+1])!="") echo ", ";
							} ?></td>
               </tr>
               <?
					$printed_checkbox = 0;
					if(($setts['enable_vat']==1)&&($auctionDetails['apply_vat']==1)) {
						// get VAT permissions
						$buyer = getSqlRow("SELECT * FROM probid_users WHERE id=".$_SESSION['memberid']);
						$seller = getSqlRow("SELECT * FROM probid_users WHERE id=".$auctionDetails['ownerid']);
						$country_id = getSqlField("SELECT id FROM probid_countries WHERE name='".$seller['country']."'",'id');
						$user_vat = getSqlRow("SELECT * FROM probid_vat_setts WHERE LOCATE(',".$country_id.",',CONCAT(',',countries,','))>0");
						
						if($user_vat){
							$user_types = explode(',',$user_vat['users_no_vat']);
							if(($buyer['companyname']=="")&&($buyer['vat_uid_number']==""))$user_type='d';
							if(($buyer['companyname']=="")&&($buyer['vat_uid_number']!=""))$user_type='c';
							if(($buyer['companyname']!="")&&($buyer['vat_uid_number']==""))$user_type='b';
							if(($buyer['companyname']!="")&&($buyer['vat_uid_number']!=""))$user_type='a';
							if(!in_array($user_type,$user_types)) {
								$vat_rate = getSqlField("SELECT rate FROM probid_vat_rates WHERE vat_id = '".$user_vat['id']."'", 'rate');
								$msg = str_replace('{AMOUNT}',$vat_rate,$lang[m_confirm_vat]);
								$msg = str_replace('{VATNAME}',$user_vat['symbol'],$msg);
								$printed_checkbox = 1; ?>
               <tr class="c3">
                  <td align="right" valign="top"><b>
                     <?=$user_vat['symbol']?>
                     :</b> </td>
                  <td><input name="vatconfirm" id="vatconfirm" type="checkbox" value="1" style="margin:0px; padding:0px">
                     <label for="vatconfirm">
                     <?=$msg;?>
                     </label>
                  </td>
               </tr>
               		<?	} // if(in_array($user_type,$user_types))
						} // if($user_vat)
					} // if(($setts['enable_vat']==1)&&($auctionDetails['apply_vat']==1))
         
					if($printed_checkbox == 0) { ?>
               <input type="hidden" name="vatconfirm" value="1">
               <? } ?>
               <? if ($auctionDetails['private']=="Y") { ?>
               <tr class="c4">
                  <td align="right"></td>
                  <td class="redfont"><?=$lang[warning_bid_priv_auct];?></td>
               </tr>
               <? } ?>
            </table>
            <table width="100%" border="0" cellpadding="4" cellspacing="2" class="errormessage">
               <tr>
                  <td width="150" align="center"><input name="confirmbidok" type="submit" id="confirmbidok" value="<?=$lang[submit]?>">
                  </td>
                  <td><?=$lang[confirmbidterms]?></td>
               </tr>
            </table>
            <div>
               <?=$lang[confirmbidagreeing]?>
            </div>
         </form>
         <font class="menu">
			<a href="<?=processLink('auctiondetails', array('id' => $_POST['auctionid']));?>">
         <?=$lang[retdetailspage]?>
         </a></font>
         <? } else if ($action=="bid_SUBMITTED") { 
	  		## Implementing the Anti Sniping feature - updated in v5.00
			## Now the sniping feature can be controlled from the admin area
			if ($setts['sniping_feature']=="Y") {
				$snipingExtension = $setts['sniping_duration']; ## extension in minutes
			
				$auctionEnddate = getSqlField("SELECT enddate FROM probid_auctions WHERE id='".$_POST['auctionid']."'","enddate");
				$secondsLeft = daysleft($auctionEnddate,$setts['date_format']);
				$minutesLeft = $secondsLeft/60;
				if ($minutesLeft<$snipingExtension) {
					$splitted = explode (" ",$auctionEnddate);
					$ourdate = explode ("-",$splitted[0]);
					list($ryear, $rmonth, $rday) = $ourdate;
					$ourtime = explode (":",$splitted[1]);
					list($rhour,$rmin,$rsec) = $ourtime;
		
					$enddateTimestamp = mktime($rhour,$rmin,$rsec,$rmonth,$rday,$ryear); 
					$enddateTimestamp = $enddateTimestamp + (($snipingExtension-$minutesLeft)*60);
					$enddate = strftime("%Y-%m-%d %H:%M:%S",$enddateTimestamp);
					$updateEnddate = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET 
					enddate='".$enddate."' WHERE id='".$_POST['auctionid']."'");
				}
			}
			## end of Anti Sniping Feature implementation
			
			$getItemWatch=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_auction_watch");
			while ($itemWatch=mysqli_fetch_array($getItemWatch)) {
				if ($itemWatch['auctionid']==$_POST['auctionid']) {
					$auctionId = $_POST['auctionid'];
					$watcherId = $itemWatch['userid'];
					include("mails/itemwatch.php");
				}
			}
			## MySQL syntax error fix: field OUT renamed to VALJA - ALEKSEI HODUNKOV
			$getOutbidBids=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT bidderid, auctionid, id FROM probid_bids WHERE 
			auctionid='".$_POST['auctionid']."' AND valja=1 AND invalid=0 AND emailsent=0");
			while($outbidBids=mysqli_fetch_array($getOutbidBids)) {
				$auctionId=$outbidBids['auctionid'];
				$bidderId=$outbidBids['bidderid'];
				include ("mails/outbid.php");
				$updateEmailsSent = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_bids SET emailsent=1 WHERE bidderid='".$outbidBids['bidderid']."' AND auctionid='".$outbidBids['auctionid']."'");
			}
			## to stop people from refrashing and the bid being posted more than once.
			$_SESSION['show_msg']=1;
			echo "<script>document.location.href='bid.confirm.php?id=".$_POST['auctionid']."'</script>";
		} else if ($action=="BID_IncPROXY") {
			echo "<br><br><p align=center class=contentfont><b>$lang[proxybidupdated]</b></p>	\n";
	        echo "<br><div align=\"center\" class=\"contentfont\">								\n";
			echo "<a href=\"".processLink('auctiondetails', array('id' => $_POST['auctionid']))."\">".$lang[retdetailspage]."</a></div><br>"; 
		} ?>
      </td>
   </tr>
</table>
<? 
}
}	
else
{
	// Я неавторизованный пользователь
	include 'pangalink/authenticate.php';
}
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
