<?
## v5.25 -> jun. 08, 2006
session_start();
if ($_SESSION['membersarea']!="Active") {
	echo "<script>document.location.href='login.php'</script>";
} else {

if ($_SESSION['is_seller']!="Y") header ("Location: membersarea.php");

include_once ("config/config.php");

include ("themes/".$setts['default_theme']."/header.php");

header7("<b>$lang[memarea_title]:</b> $lang[memarea_hello] $membername ");


include("membersmenu.php");

if ($_REQUEST['action']!="updateitem") {
	echo "<script language=\"JavaScript\"> 						\n";
	echo "	function changepage(theform) { 						\n";
	echo "		if (theform.auctiontype.value==\"dutch\") { 	\n";
	echo "	   	theform.respr[0].disabled=true; 				\n";
	echo "	   	theform.respr[1].disabled=true; 				\n";
	echo "	   	theform.resprice.disabled=true; 				\n";
	echo "	   	theform.quantity.readOnly=false; 			\n";
	echo "		} else { 												\n";
	echo "	   	theform.respr[0].disabled=false; 			\n";
	echo "	   	theform.respr[1].disabled=false; 			\n";
	echo "	   	theform.resprice.disabled=false; 			\n";
	echo "	   	theform.quantity.readOnly=true; 				\n";
	echo "		}		 													\n";
	echo "	} ";
	echo "</script>";
} 




## delete pictures
$_REQUEST['description']=$_POST['description_main'];
include ("formchecker.php");

if ($_REQUEST['action']=="updateitem") {
	$paymentMethods="";
	for ($i=0;$i<count($_POST['pmethod']);$i++) {
		$paymentMethods.=$_POST['pmethod'][$i]."<br>";
	}
		
	$auctOld = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");
	$addImgCntOld = getSqlNumber("SELECT id FROM probid_auction_images WHERE auctionid='".$_POST['id']."'");
	$today=date("Y-m-d H:i:s");
	$timeNowOffset = convertdate($today,"Y-m-d H:i:s");
	$notStarted = getSqlNumber("SELECT id FROM probid_auctions WHERE 
	startdate>'".$timeNowOffset."' AND closed=1 
	AND active=1 AND deleted!=1 AND swapped!=1 AND payment_status='confirmed' AND 
	id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");	
	
	if ($notStarted>0) { 
		if ($_REQUEST['starttime']=="NOW") {
			$startDate = date( "Y-m-d H:i:s", time() );
			$closed = 0;
		} else {
			$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");
			$startDate = date("Y-m-d H:i:s", 
  			mktime($_REQUEST['shour']-$diff,$_REQUEST['sminute'],0,$_REQUEST['date17_month'],$_REQUEST['date17_date'],$_REQUEST['date17_year'])); 
			$closed = 1;
		}
	} else {
		$startDate = $auctOld['startdate'];
		$closed = 0;
	}
	
	if ($_REQUEST['endtime']=="duration") {
		$closingdate = closingdate($startDate,$_REQUEST['duration']);
	} else {
		$diff = getSqlField("SELECT value FROM probid_timesettings WHERE active='selected'","value");
		$closingdate = date("Y-m-d H:i:s", 
  		mktime($_REQUEST['eshour']-$diff,$_REQUEST['esminute'],0,$_REQUEST['date18_month'],$_REQUEST['date18_date'],$_REQUEST['date18_year'])); 
	}

	$mainImgMaxSize = $setts['pic_gal_max_size']*1024;

	### this is the saver for the main image
	$tempNumber = md5(uniqid(rand(2, 999999999)));
	if ($_FILES['file']['name']!=""||$_POST['mainpicurl']) {
		if ($_FILES['file']['size']<$mainImgMaxSize||$_POST['mainpicurl']) {
			$oldMainImage = getSqlField("SELECT picpath FROM probid_auctions WHERE id='".$_POST['id']."'","picpath");
			if (!preg_match("/^http:\/\//is",$oldMainImage)) {
				deleteFile("",$oldMainImage);
			}
			if ($_POST['mainpicurl']) {
				$imageName="http://".str_replace("http://","",$_POST['mainpicurl']);
				$isUpload=true;
			} else {
				$fileExtension = getFileExtension($_FILES['file']['name']);
				$imageName = "mb".$_SESSION['memberid']."_".$tempNumber."_mainpic.".$fileExtension;
				$isUpload = uploadFile($_FILES['file']['tmp_name'],$imageName,"uplimg/");
				$imageName="uplimg/".$imageName;
			}
			if ($isUpload) {
				$updateAuction = mysql_query("UPDATE probid_auctions SET 
				picpath='".$imageName."' WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");
			}
		} else {
			echo "$lang[auc_errpicsize1] $setts[pic_gal_max_size] kb<br>$lang[auc_errpicsize2]<br>";
		}			
	}
	## upload video
	$videoMaxSize = $setts['video_gal_max_size']*1024;
	if ($_FILES['videofile']['name']!=""||$_POST['videofileurl']) {
		if ($_FILES['videofile']['size']<$videoMaxSize||$_POST['videofileurl']) {
			$oldVideoFile = getSqlField("SELECT videofile_path FROM probid_auctions WHERE id='".$_POST['id']."'","videofile_path");
			if (!preg_match("/^http:\/\//is",$oldVideoFile)) {
				deleteFile("",$oldVideoFile);
			}
			if ($_POST['videofileurl']) {
				$videoName="http://".str_replace("http://","",$_POST['videofileurl']);
				$isUpload=true;
			} else {
				$fileExtension = getFileExtension($_FILES['videofile']['name']);
				$videoName = "mb".$_SESSION['memberid']."_".$tempNumber."_video.".$fileExtension;
				$isUpload = uploadFile($_FILES['videofile']['tmp_name'],$videoName,"uplimg/");
				$videoName="uplimg/".$videoName;
			} 
			if ($isUpload) {
				$updateAuction = mysql_query("UPDATE probid_auctions SET 
				videofile_path='".$videoName."' WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");
			} else echo $lang[movieuplerror]." ".fileUploadError($_FILES['videofile']['error'])."<br>";
		} else {
			echo "<strong>$lang[error_a]</strong> $lang[auc_errvideosize1] $setts[video_gal_max_size] kb<br>";;
		}			
	}

	## v5.24 tweak - imgMaxSize will also contain if the main pic exists (so that add pics cant be added with no existing main pic)
	$isMainPic = getSqlNumber("SELECT id FROM probid_auctions WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."' AND picpath!=''");
	$imgMaxSize = ($isMainPic) ? ($setts['pic_gal_max_size']*1024) : 0;

	$hpfeat = ($_POST['hpfeat']!="Y") ? "N" : "Y";
	$catfeat = ($_POST['catfeat']!="Y") ? "N" : "Y";
	$bolditem = ($_POST['bolditem']!="Y") ? "N" : "Y";
	$hlitem = ($_POST['hlitem']!="Y") ? "N" : "Y";
	$respr = ($_POST['resprice']>0) ? "Y" : "N";

	$name = titleResize($_POST['name']);
	$name = remSpecialChars($name);
	$description = remSpecialChars($_POST['description_main']);
	$getWordFilter = mysql_query("SELECT * FROM probid_wordfilter");
	while ($wordFilter = mysql_fetch_array($getWordFilter)) {
		$name=eregi_replace($wordFilter['word']," ",$name);
		$description=eregi_replace($wordFilter['word']," ",$description);
	}
	
	$keywords=$name." ".$description;
	$cnt1=0;
	for ($i=0;$i<count($_FILES['addfile']);$i++) {
		if($_FILES['addfile']['tmp_name'][$i]!=""&&$_FILES['addfile']['tmp_name'][$i]!="none"&&$_FILES['addfile']['size'][$i]<$imgMaxSize) {
			$cnt1++;
		}
	}

	$category_id = getMainCat($_POST['category']);
		
	$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
	$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");
	
	$topay="";
	if ($fee['is_setup_fee']=="Y") $topay.="";
	if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&$cnt1>0) { 
		$topay.="[PICFEE]";
		$isFee['pic_count'] = $cnt1;
	}
	if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&$hlitem=="Y"&&$auctOld['hlitem']!="Y") {
		$topay.="[HLITEMFEE]";
		$isFee['hl']="Y";
	} 
	if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&$bolditem=="Y"&&$auctOld['bolditem']!="Y") {
		$topay.="[BOLDFEE]";
		$isFee['bold']="Y";
	} 
	if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&$hpfeat=="Y"&&$auctOld['hpfeat']!="Y") {
		$topay.="[HPFEATFEE]";
		$isFee['hpfeat']="Y";
	} 
	if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&$catfeat=="Y"&&$auctOld['catfeat']!="Y") {
		$topay.="[CATFEATFEE]";
		$isFee['catfeat']="Y";
	} 
	if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&$respr=="Y"&&$auctOld['rp']!="Y") {
		$topay.="[RPFEE]";
		$isFee['rp']="Y";
	} 
	if ($fee['bin_fee']>0&&$_POST['buynow']=="Y"&&$auctOld['bn']!="Y") {
		$topay.="[BNFEE]";
		$isFee['bin']="Y";
	}
	if ($fee['custom_st_fee']>0&&$_POST['endtime']=="customtime"&&$auctOld['endtime_type']!="customtime") {
		$topay.="[CUSTOMSTFEE]";
		$isFee['customst']="Y";
	}
	if ($fee['videofile_fee']>0&&($_FILES['videofile']['name']!=""||$_POST['videofileurl'])&&$auctOld['videofile_path']=="") {
		$topay.="[VIDEOFEE]";
		$isFee['videofee']="Y";
	}
	if ($fee['second_cat_fee']>0&&$_REQUEST['addlcategory']>0&&$auctOld['addlcategory']<=0) {
		$topay.="[SCFEE]";
		$isFee['secondcat']="Y";
	} 

	$getVo = checkSetupVoucher(trim($_REQUEST['voucher_code']));
	if ($getVo['nbuses']>0) $updateVoucher=mysql_query("UPDATE probid_vouchers SET usesleft=usesleft-1 WHERE id='".$getVo['id']."'");

	$payAmount = setupFee($_REQUEST['startprice'],$_POST['currency'],$_POST['id'],$isFee,TRUE,TRUE,$_POST['voucher_code']);

	if ($topay!=""&&!freeFees($_SESSION['memberid'])&&$auctOld['listin']!="store"&&$payAmount>0) {
		$payment_status="unconfirmed";
		$active=0;
	} else {
		$payment_status="confirmed";
		$active=1;
	}
	
	$keywords = $name." ".$description;

	$pay_system_update = '';
   $acceptdirectpayment=0;
   if($_POST['acceptdirectpayment']){
		$acceptdirectpayment=1;
      foreach($_POST['acceptdirectpayment'] as $k=>$v){
      	if ($v) $pay_system_update .= $k . ',';
      }
      $pay_system_update = rtrim($pay_system_update,',');
   }
 //26.06.2014 MK Updated quantity fix SET always as 1 - Original should be // quantity='".$_POST['quantity']//
	$updateitem=mysql_query("UPDATE probid_auctions SET 
	itemname='".$name."',description='".$description."',
	quantity=1, auctiontype='".$_POST['auctiontype']."',
	bidstart='".$_POST['startprice']."',rp='".$respr."',rpvalue='".$_POST['resprice']."',
	bn='".$_POST['buynow']."',bnvalue='".$_POST['bnprice']."',bi='".$_POST['bidinc']."',
	bivalue='".$_POST['bidincvalue']."',duration='".$_POST['duration']."',country='".remSpecialChars($_POST['country'])."',
	zip='".$_POST['zip']."',sc='".$_POST['shipcond']."',scint='".$_POST['shipint']."',
	pm='".$paymentMethods."',category='".$_POST['category']."',startdate='".$startDate."',enddate='".$closingdate."',keywords='".$keywords."',
	hpfeat='".$hpfeat."',catfeat='".$catfeat."',bolditem='".$bolditem."',hlitem='".$hlitem."',
	private='".$_POST['privateauct']."',currency='".$_POST['currency']."',
	postage_costs='".$_POST['postage_costs']."',insurance='".$_POST['insurance']."',
	type_service='".$_POST['type_service']."',isswap='".$_POST['isswap']."',
	acceptdirectpayment='".$acceptdirectpayment."',
	addlcategory='".$_POST['addlcategory']."',shipping_details='".remSpecialChars($_POST['shipping_details'])."',
	hpfeat_desc='".$_POST['hpfeat_desc']."',
	active='".$active."', closed='".$closed."', payment_status='".$payment_status."', listin='".$_POST['listin']."',
   accept_payment_systems = '".$pay_system_update."',
	auto_relist='".$_POST['auto_relist']."', auto_relist_bids='".$_POST['auto_relist_bids']."', 
	endtime_type='".$_POST['endtime']."', approved='0', 
	offer_range_min='".$_POST['offer_range_min']."', offer_range_max='".$_POST['offer_range_max']."',
	auto_relist_nb='".$_POST['auto_relist_nb']."'    
	WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'") or die(mysql_error());
	
	auctionApproval($_POST['id'], $_SESSION['memberid']);

	### insert the custom fields for the auction (if available)
  	$getFields = mysql_query("SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_fields") or die(mysql_error()); 
  	$isFields = mysql_num_rows($getFields);
  	if ($isFields) {
		$delFields = mysql_query("DELETE FROM probid_fields_data WHERE auctionid='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");
		while ($fields=mysql_fetch_array($getFields)) {
			$box_value = "";
			if ($fields['boxtype']=="checkbox") {
				for ($i=0; $i<count($_POST['box'.$fields['boxid']]); $i++) 
					$box_value .= $_POST['box'.$fields['boxid']][$i]."; ";
			} else { 
				$box_value = $_POST['box'.$fields['boxid']];
			}
			$addFieldData = mysql_query("INSERT INTO probid_fields_data
			(auctionid, ownerid, boxid, boxvalue) VALUES
			('".$_POST['id']."','".$_SESSION['memberid']."','".$fields['boxid']."','".remSpecialChars($box_value)."')");
		}
	}		

	for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
		if(($_FILES['addfile']['name'][$i]!=""||$_POST['picurl'][$i])&&$_FILES['addfile']['size'][$i]<$imgMaxSize) {
			$getAddImages = mysql_query ("SELECT name FROM probid_auction_images WHERE auctionid='".$_POST['id']."'");
			while ($addImage = mysql_fetch_array($getAddImages)) {
				deleteFile("",$addImage['name']);
			}
	 		$deleteImages=mysql_query("DELETE FROM probid_auction_images WHERE auctionid='".$_POST['id']."'");
		} 
	}
	
	## now upload the new additional pictures
	for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
		if($_FILES['addfile']['name'][$i]!=""||$_POST['picurl'][$i]) {
			if ($_FILES['addfile']['size'][$i]<$imgMaxSize) {
				if ($_POST['picurl'][$i]) $addImageName="http://".str_replace("http://","",$_POST['picurl'][$i]);
				else {
					$fileExtension = getFileExtension($_FILES['addfile']['name'][$i]);
					$addImageName = "mb".$_SESSION['memberid']."_".$tempNumber."_addpic".$i.".".$fileExtension;
					uploadFile($_FILES['addfile']['tmp_name'][$i],$addImageName,"uplimg/");
					$addImageName = "uplimg/".$addImageName;
				}
				$insertImage = mysql_query("INSERT INTO  probid_auction_images (name,auctionid) 
				VALUES ('".$addImageName."','".$_POST['id']."')");
			} else {
				echo "<strong>$lang[error_a]</strong> $lang[auc_errpicsize3] #".($i+1)." $lang[auc_errpicsize4] $setts[pic_gal_max_size] kb $lang[img_or_error]<br>";				
			}
		} 
	}
	
	$auctNew = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");

	if ($topay!=""&&!freeFees($_SESSION['memberid'])&&$auctNew['listin']!="store"&&$payAmount>0) {
		## we delete the count because it will be activated by the setup fee
		if ($auctOld['active']==1&&$auctOld['closed']==0&&$auctOld['deleted']!=1) {
			delcatcount ($auctOld['category'],$_POST['id']);
			delcatcount ($auctOld['addlcategory'],$_POST['id']);
		}
		## inactivate auction
		$inactAuct = mysql_query("UPDATE probid_auctions SET payment_status='unconfirmed', active='0' WHERE id='".$_POST['id']."' AND ownerid='".$_SESSION['memberid']."'");
		### the function for the auction setup fees
		$paymentAmount = setupFee($_REQUEST['startprice'],$_POST['currency'],$_POST['id'],$isFee,TRUE,FALSE,$_POST['voucher_code']);
		## or Rollback the transaction
		echo "<p align=\"center\">																\n";
		echo "<form action=\"membersarea.php\" method=\"POST\">									\n";
		echo "	<input type=\"hidden\" name=\"page\" value=\"selling\">							\n";
		echo "	<input type=\"hidden\" name=\"option\" value=\"rollback\">						\n";
		echo "	<input type=\"hidden\" name=\"addpics\" value=\"rollback\">						\n";
		echo "	<input type=\"hidden\" name=\"addpics_cnt\" value=\"".$addImgCntOld."\">		\n";
		echo "	<input type=\"hidden\" name=\"hlitem\" value=\"".$auctOld['hltiem']."\">		\n";
		echo "	<input type=\"hidden\" name=\"bolditem\" value=\"".$auctOld['bolditem']."\">	\n";
		echo "	<input type=\"hidden\" name=\"hpfeat\" value=\"".$auctOld['hpfeat']."\">		\n";
		echo "	<input type=\"hidden\" name=\"catfeat\" value=\"".$auctOld['catfeat']."\">		\n";
		echo "	<input type=\"hidden\" name=\"respr\" value=\"".$auctOld['rp']."\">				\n";
		echo "	<input type=\"hidden\" name=\"auctionid\" value=\"".$_POST['id']."\">			\n";
		echo "	<input type=\"hidden\" name=\"payment_amount\" value=\"".$paymentAmount."\">	\n";
		echo "	<input type=\"hidden\" name=\"active\" value=\"".$auctOld['active']."\">		\n";
		echo "	<input type=\"hidden\" name=\"payment_status\" value=\"".$auctOld['payment_status']."\">\n";
		echo "	<input type=\"submit\" name=\"rollbackok\" value=\"$lang[rollbacktransaction]\">\n";
		echo "</form></p>";
	} else {
		if ($auctNew['active']==1&&$auctNew['closed']==0&&$auctNew['deleted']!=1&&$auctNew['listin']!="store") {
			addcatcount ($_REQUEST['category'],$_POST['id']);
			addcatcount ($_REQUEST['addlcategory'],$_POST['id']);
		}
		if ($auctOld['active']==1&&$auctOld['closed']==0&&$auctOld['deleted']!=1&&$auctNew['listin']!="store") {
			delcatcount ($auctOld['category'],$_POST['id']);
			delcatcount ($auctOld['addlcategory'],$_POST['id']);
		}
		echo "<br><p class=contentfont align=center>$lang[aucupdate_success]</p><p>&nbsp;</p>";
		$link = "membersarea.php?page=selling";
		echo "<script>window.setTimeout('changeurl();',2500); function changeurl(){window.location='$link'}</script>";
	}
} else {
	$auction = getSqlRow("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
	$isAuction = getSqlNumber("SELECT * FROM probid_auctions WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");

	## delete pictures
	if (!empty($_REQUEST['deletepic'])) {
		if (eregi('mainpic', $_REQUEST['deletepic'])) {
			$isPic = getSqlNumber("SELECT id FROM probid_auctions WHERE picpath='".$auction['picpath']."'");
			if (!$isPic) @unlink($_REQUEST['mainpic']);
			$remPicDb = mysql_query("UPDATE probid_auctions SET picpath='' WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
			$auction['picpath'] = '';
			
			## we also delete all additional pics
			$getAddImages = mysql_query("SELECT i.* FROM probid_auction_images i, probid_auctions a WHERE i.auctionid='".$_REQUEST['id']."' AND a.id=i.auctionid AND a.ownerid='".$_SESSION['memberid']."'");
			while ($addImage = mysql_fetch_array($getAddImages)) {
				deleteFile("",$addImage['name']);
			}
			$deleteImages=mysql_query("DELETE FROM probid_auction_images WHERE auctionid='".$_POST['id']."'");

		} else if (eregi('addpic', $_REQUEST['deletepic'])) {
			$picDets = explode('_',$_REQUEST['deletepic']);
			$getAP = mysql_query("SELECT i.* FROM probid_auction_images i, probid_auctions a WHERE i.auctionid='".$_REQUEST['id']."' AND a.id=i.auctionid AND a.ownerid='".$_SESSION['memberid']."'");
			$addPicRes = @mysql_result($getAP,$picDets[1],'name');
			$addPicId = @mysql_result($getAP,$picDets[1],'id');
			@unlink($addPicRes);
			$remAddPicDb = mysql_query("DELETE FROM probid_auction_images WHERE id='".$addPicId."'");
		} else if (eregi('video', $_REQUEST['deletepic'])) {
			$isVideo = getSqlNumber("SELECT id FROM probid_auctions WHERE videofile_path='".$auction['videofile_path']."'");
			if (!$isVideo) @unlink($_REQUEST['videofile_path']);
			$remPicDb = mysql_query("UPDATE probid_auctions SET videofile_path='' WHERE id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'");
			$auction['videofile_path'] = '';
		}
	}
	
	## v5.22 addon -> remember changes in case the info is incomplete
	$auctionEdit = array();
	$auctionEdit['listin'] = (trim($_POST['listin'])!="") ? $_POST['listin'] : $auction['listin'];
	$auctionEdit['itemname'] = (trim($_POST['name'])!="") ? remSpecialChars($_POST['name']) : $auction['itemname'];
	$auctionEdit['description'] = (trim($_POST['description_main'])!="") ? remSpecialChars($_POST['description_main']) : $auction['description'];
	$auctionEdit['auctiontype'] = (trim($_POST['auctiontype'])!="") ? $_POST['auctiontype'] : $auction['auctiontype'];
	
	$auctionEdit['quantity'] = (trim($_POST['quantity'])!="") ? $_POST['quantity'] : $auction['quantity'];
	$auctionEdit['quantity'] = (eregi("standard", $auctionEdit['auctiontype'])) ? 1 : $auctionEdit['quantity'];
	
	$auctionEdit['bidstart'] = (trim($_POST['startprice'])!="") ? $_POST['startprice'] : $auction['bidstart'];
	$auctionEdit['rp'] = (trim($_POST['respr'])!="") ? $_POST['respr'] : $auction['rp'];
	$auctionEdit['rpvalue'] = (trim($_POST['resprice'])!="") ? $_POST['resprice'] : $auction['rpvalue'];
	$auctionEdit['bn'] = (trim($_POST['buynow'])!="") ? $_POST['buynow'] : $auction['bn'];
	$auctionEdit['bnvalue'] = (trim($_POST['bnprice'])!="") ? $_POST['bnprice'] : $auction['bnvalue'];
	$auctionEdit['bi'] = (trim($_POST['bidinc'])!="") ? $_POST['bidinc'] : $auction['bi'];
	$auctionEdit['bivalue'] = (trim($_POST['bidincvalue'])!="") ? $_POST['bidincvalue'] : $auction['bivalue'];
	$auctionEdit['hpfeat_desc'] = (trim($_POST['hpfeat_desc'])!="") ? remSpecialChars($_POST['hpfeat_desc']) : $auction['hpfeat_desc'];
	$auctionEdit['country'] = (trim($_POST['country'])!="") ? $_POST['country'] : $auction['country'];
	$auctionEdit['zip'] = (trim($_POST['zip'])!="") ? $_POST['zip'] : $auction['zip'];
	$auctionEdit['sc'] = (trim($_POST['shipcond'])!="") ? $_POST['shipcond'] : $auction['sc'];
	$auctionEdit['scint'] = (trim($_POST['shipint'])!="") ? $_POST['shipint'] : $auction['scint'];
	$auctionEdit['duration'] = (trim($_POST['duration'])!="") ? $_POST['duration'] : $auction['duration'];
	$auctionEdit['private'] = (trim($_POST['privateauct'])!="") ? $_POST['privateauct'] : $auction['private'];
	$auctionEdit['currency'] = (trim($_POST['currency'])!="") ? $_POST['currency'] : $auction['currency'];
	$auctionEdit['isswap'] = (trim($_POST['isswap'])!="") ? $_POST['isswap'] : $auction['isswap'];
	##$auctionEdit['acceptdirectpayment'] = (trim($_POST['acceptdirectpayment'])!="") ? $_POST['acceptdirectpayment'] : $auction['acceptdirectpayment'];
	##$auctionEdit['directpaymentemail'] = (trim($_POST['directpaymentemail'])!="") ? $_POST['directpaymentemail'] : $auction['directpaymentemail'];
	$auctionEdit['postage_costs'] = (trim($_POST['postage_costs'])!="") ? $_POST['postage_costs'] : $auction['postage_costs'];
	$auctionEdit['insurance'] = (trim($_POST['insurance'])!="") ? $_POST['insurance'] : $auction['insurance'];
	$auctionEdit['shipping_details'] = (trim($_POST['shipping_details'])!="") ? remSpecialChars($_POST['shipping_details']) : $auction['shipping_details'];
	$auctionEdit['type_service'] = (trim($_POST['type_service'])!="") ? $_POST['type_service'] : $auction['type_service'];
	$auctionEdit['category'] = (trim($_POST['category'])!="") ? $_POST['category'] : $auction['category'];
	$auctionEdit['addlcategory'] = (trim($_POST['addlcategory'])!="") ? $_POST['addlcategory'] : $auction['addlcategory'];
	$auctionEdit['auto_relist'] = (trim($_POST['auto_relist'])!="") ? $_POST['auto_relist'] : $auction['auto_relist'];
	$auctionEdit['auto_relist_bids'] = (trim($_POST['auto_relist_bids'])!="") ? $_POST['auto_relist_bids'] : $auction['auto_relist_bids'];
	$auctionEdit['auto_relist_nb'] = (trim($_POST['auto_relist_nb'])!="") ? $_POST['auto_relist_nb'] : $auction['auto_relist_nb'];
	$auctionEdit['endtime'] = (trim($_POST['endtime'])!="") ? $_POST['endtime'] : $auction['endtime_type'];
	$auctionEdit['offer_range_min'] = (trim($_POST['offer_range_min'])!="") ? $_POST['offer_range_min'] : $auction['offer_range_min'];
	$auctionEdit['offer_range_max'] = (trim($_POST['offer_range_max'])!="") ? $_POST['offer_range_max'] : $auction['offer_range_max'];
	
	$mainCat_primary = getMainCat($auctionEdit['category']);
	$mainCat_secondary = getMainCat($auctionEdit['addlcategory']);
	
	## get time before auction ends in seconds
	## if under the set number of hours are left, the auction owner wont be able to edit it anymore.
	$minHours = 12; ## set to 12 hours.
	$timeLeft = daysleft($auction['enddate'],$setts['date_format']);
	$isTime = $timeLeft - ($minHours * 60 * 60);
	if ($isAuction==0) {
		echo "<br><p align=center class=contentfont>$lang[editauctionerror1]</p>\n";
		echo "<p class=contentfont align=center><strong><a href=membersarea.php?page=selling>$lang[editreditectmsg]</a></strong></p>";
	} else if ($auction['nrbids']>0) {
		echo "<br><p align=center class=contentfont>$lang[editauctionerror2]</p>\n";
		echo "<p class=contentfont align=center><strong><a href=membersarea.php?page=selling>$lang[editreditectmsg]</a></strong></p>";
	} else if ($isTime<=0) {
		echo "<br><p align=center class=contentfont>$lang[editauctionerror3]</p>\n";
		echo "<p class=contentfont align=center><strong><a href=membersarea.php?page=selling>$lang[editreditectmsg]</a></strong></p>";
	} else { 
	## first of all check if the auction is not started yet
		$today=date("Y-m-d H:i:s");
		$timeNowOffset = convertdate($today,"Y-m-d H:i:s");
		$notStarted = getSqlNumber("SELECT id FROM probid_auctions WHERE 
		startdate>'".$timeNowOffset."' AND closed=1 
		AND active=1 AND deleted!=1 AND swapped!=1 AND payment_status='confirmed' AND 
		id='".$_REQUEST['id']."' AND ownerid='".$_SESSION['memberid']."'"); 

		$nbAddPics = getSqlNumber("SELECT id FROM probid_auction_images WHERE auctionid='".$auction['id']."'");

		echo "<script language=\"JavaScript\"> \n";
		echo "	function delete_pic(theform,pic) { \n";
		echo "		if (confirm('".$lang[remimgconf]."'))	{ \n";
		echo "			theform.deletepic.value = pic; \n";
		echo "			theform.onsubmit(); \n";
		echo "			theform.submit(); \n";
		echo "		} else { \n";
		echo "			theform.del_chk_mainpic.checked = false; \n";
		if ($setts['video_gal_max_size']) echo "theform.del_chk_video.checked = false; \n";
		for ($i=0; $i<$nbAddPics; $i++) echo "theform.del_chk_addpic".$i.".checked = false; \n";
		echo "		} \n";
		echo "	} \n";
		echo "</script>"; ?>

<SCRIPT LANGUAGE="JavaScript" SRC="CalendarPopup.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">document.write(getCalendarStyles());</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" ID="js17">
var cal17 = new CalendarPopup();
cal17.setReturnFunction("setMultipleValues4");
function setMultipleValues4(y,m,d) {
	document.forms[2].date17_year.value=y;
	document.forms[2].date17_month.selectedIndex=m;
	for (var i=0; i<document.forms[2].date17_date.options.length; i++) {
		if (document.forms[2].date17_date.options[i].value==d) {
			document.forms[2].date17_date.selectedIndex=i;
			}
		}
	}
function getDateString(y_obj,m_obj,d_obj) {
	var y = y_obj.options[y_obj.selectedIndex].value;
	var m = m_obj.options[m_obj.selectedIndex].value;
	var d = d_obj.options[d_obj.selectedIndex].value;
	if (y=="" || m=="") { return null; }
	if (d=="") { d=1; }
	return str= y+'-'+m+'-'+d;
	}
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" ID="js18">
var cal18 = new CalendarPopup();
cal18.setReturnFunction("setMultipleValues5");
function setMultipleValues5(y,m,d) {
	document.forms[2].date18_year.value=y;
	document.forms[2].date18_month.selectedIndex=m;
	for (var i=0; i<document.forms[2].date18_date.options.length; i++) {
		if (document.forms[2].date18_date.options[i].value==d) {
			document.forms[2].date18_date.selectedIndex=i;
			}
		}
	}
function getDateString(y_obj,m_obj,d_obj) {
	var y = y_obj.options[y_obj.selectedIndex].value;
	var m = m_obj.options[m_obj.selectedIndex].value;
	var d = d_obj.options[d_obj.selectedIndex].value;
	if (y=="" || m=="") { return null; }
	if (d=="") { d=1; }
	return str= y+'-'+m+'-'+d;
	}
</SCRIPT>
<script language="JavaScript">
function submitform() {
	document.sistep1.onsubmit();
	document.sistep1.submit();
}
</script>
<script language="javascript">
	function is_numeric(num) {
		var exp = new RegExp("^[0-9]*$","g");
		return exp.test(num);
	}
	function revertMaxRelist(theform) {
		if (theform.auto_relist_nb.value<1 || !is_numeric(theform.auto_relist_nb.value)) {
			theform.auto_relist_nb.value = 1;
		} else if (theform.auto_relist_nb.value><?=$setts['nb_autorelist_max'];?>) { 
			theform.auto_relist_nb.value = <?=$setts['nb_autorelist_max'];?>;
		}
	}
</script>

<form action="editauction.php" method="post" enctype="multipart/form-data" name="sistep1">
   <input type="hidden" name="deletepic" value="">
   <input type="hidden" name="id" value="<?=$_REQUEST['id'];?>">
   <input type="hidden" name="active_prv" value="<?=$auction['active'];?>">
   <input type="hidden" name="payment_status_prv" value="<?=$auction['payment_status'];?>">
   <input type="hidden" name="cat_prv" value="<?=$auction['category'];?>">
   <input type="hidden" name="addlcat_prv" value="<?=$auction['addlcategory'];?>">
   <table width="100%" border="0" cellpadding="4" cellspacing="4" class="border">
      <tr align="center" class="c1">
         <td colspan="3"><?=$lang[modifyauc]?></td>
      </tr>
      <tr class="c4">
         <td colspan="3" align="center"><strong><? echo auctionListedIn($auction['id']); ?></strong></td>
      </tr>
		<?
		$approvalCats = ','.$setts['approval_categories'].',';
		##echo "A:".$approvalCats."B: ".$mainCat_primary." ".$mainCat_secondary;
		$isApprovalUser = getSqlField("SELECT auction_approval FROM probid_users WHERE id='".$_SESSION['memberid']."'", "auction_approval");
		$isApprovalCats = (eregi($auctionEdit['category'], $approvalCats) || eregi($auctionEdit['addlcategory'], $approvalCats)) ? 'Y' : 'N';
		
		$isApproval = (eregi('Y', $isApprovalUser) || eregi('Y', $isApprovalCats) || eregi('Y', $setts['enable_auctions_approval'])) ? 'Y' : 'N';

		if (eregi('Y', $isApproval)) { ?>
      <tr>
         <td colspan="3" align="center" class="errormessage"><?=$lang[editingwillsuspendauct];?></td>
      </tr>
      <? }
		$storeOwner = getSqlRow("SELECT store_active, aboutpage_type FROM probid_users WHERE id='".$auction['ownerid']."'");
		
		
		$nbSV = getSqlNumber("SELECT id FROM probid_vouchers WHERE voucher_type='setup' AND active='1'");
		if ($auctionEdit['listin']!="store"&&$nbSV>0) { ?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[voucher_code];?>
            </strong></td>
         <td colspan="2"><input name="voucher_code" type="text" id="voucher_code" size=40 value="<?=$_REQUEST['voucher_code'];?>">
            <br>
            <?=$lang[voucher_text]?></td>
      </tr>
      <? } ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[listedin];?>
            </strong></td>
         <td colspan="2"><input name="listin" type="radio" value="auction" checked <? echo ($auctionEdit['listin']=="store")?"disabled":"";?>>
            <?=$lang[auction];?>
            <br>
            <? if ($storeOwner['store_active']==1&&$storeOwner['aboutpage_type']==2) { ?>
            <input name="listin" type="radio" value="store" <? echo ($auctionEdit['listin']=="store") ? "checked":"";?>>
            <?=$lang[store];?>
            <br>
            <input name="listin" type="radio" value="both" <? echo ($auctionEdit['listin']=="both") ? "checked":"";?> <? echo ($auctionEdit['listin']=="store")?"disabled":"";?>>
            <?=$lang[both];?>
            <? } ?></td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[itemname]?>
            </strong></td>
         <td><input name="name" type="text" class="contentfont" id="name" value="<? echo addSpecialChars($auctionEdit['itemname']);?>" size="87"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><strong>
            <?=$lang[descr]?>
            </strong></td>
         <td colspan="2"><textarea name="description_main" cols="45" rows="8" class="contentfont" id="description_main"><? echo addSpecialChars($auctionEdit['description']);?></textarea>
            <script> 
					var oEdit1 = new InnovaEditor("oEdit1");
					oEdit1.width="100%";//You can also use %, for example: oEdit1.width="100%"
					oEdit1.height=300;
					oEdit1.REPLACE("description_main");//Specify the id of the textarea here
				</script>
         </td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[uploadpic]?>
            </strong><br />
            <?=$lang[optfield];?></td>
         <td><table border="0" cellspacing="2" cellpadding="2">
               <tr>
                  <td><input type="file" name="file" class="contentfont">
                     <br>
                     <b>
                     <?=$lang['orurl']?>
                     </b>http://
                     <input type="text" name="mainpicurl">
                  </td>
                  <? if (!empty($auction['picpath'])) { ?>
                  <td><? echo "&nbsp;$lang[current_pic]: <img src=\"makethumb.php?pic=".$auction['picpath']."&w=50&sq=Y&b=Y\" align=absmiddle> ";?> </td>
                  <td><? echo $lang[delete].": <input type=\"checkbox\" id=\"del_chk_mainpic\" onClick=\"delete_pic(this.form,'mainpic');\"> "; ?></td>
                  <? } ?>
               </tr>
            </table>
            <?=$lang[mainpicdelnote];?></td>
      </tr>
      <? if ($setts['pic_gal_active']==1) { ?>
      <tr class="c2" valign="top">
         <td align="right" ><strong>
            <?=$lang[picturegallery]?>
            </strong><br />
            <? echo "$lang[youcanupload1] $setts[pic_gal_max_nb] $lang[youcanupload2]"; ?></td>
         <td colspan="2"><? 
				$getAddPics = mysql_query("SELECT * FROM probid_auction_images WHERE auctionid='".$auction['id']."'");
				$addPicCnt = 0;
				while ($addP = mysql_fetch_array($getAddPics)) $addPic[$addPicCnt++] = $addP;
				
				for ($i=0;$i<$setts['pic_gal_max_nb'];$i++) {
					echo "<table border=\"0\" cellspacing=\"2\" cellpadding=\"2\"> \n";
					echo "	<tr> \n";
					echo "		<td><input type=\"file\" name=\"addfile[]\" class=\"contentfont\">";
					echo '			<br><b>'.$lang['orurl'].' </b>http:// <input type="text" name="picurl['.$i.']"></td>';
					if (!empty($addPic[$i]['name'])) {
						echo "<td>&nbsp;$lang[current_pic]: <img src=\"makethumb.php?pic=".$addPic[$i]['name']."&w=50&sq=Y&b=Y\" align=absmiddle></td>";
	      			echo "<td>".$lang[delete].": <input type=\"checkbox\" id=\"del_chk_addpic".$i."\" onClick=\"delete_pic(this.form,'addpic_".$i."');\"></td> ";
					}
					echo "	</tr>";
					echo "</table>";
				} ?>
         </td>
      </tr>
      <? } ?>
      <tr class="c2">
         <td align="right" valign="top">&nbsp;</td>
         <td valign="top" colspan="2"><?=$lang[imgmax_note1]." ".$setts['pic_gal_max_size']."KB";?>
            <? echo ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&!freeFees($_SESSION['memberid'])) ?"<br>$lang[picpaymentnote]":""; ?> </td>
      </tr>
		<? if ($setts['video_gal_max_size']) { ?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[uploadmov]?>
            </strong><br />
            <?=$lang[optfield];?></td>
         <td><table border="0" cellspacing="2" cellpadding="2">
               <tr>
                  <td><input type="file" name="videofile" class="contentfont">
                     <br>
                     <b>
                     <?=$lang['orurl']?>
                     </b>http://
                     <input type="text" name="videofileurl">
                  </td>
                  <? if (!empty($auction['videofile_path'])) { ?>
                  <td class="smallfont"><? echo "&nbsp[ <strong>$lang[movieuploaded]</strong> ]";?> </td>
                  <td><? echo $lang[delete].": <input type=\"checkbox\" id=\"del_chk_video\" onClick=\"delete_pic(this.form,'video');\"> "; ?></td>
                  <? } ?>
               </tr>
            </table></td>
      </tr>
		<? } ?>
      <!--08052014 MK Eliminatged dutch auctions-->
	  <!--tr class="c3">
         <td align="right"><strong>
            <?=$lang[auctype]?>
            </strong></td>
         <td colspan="2"><select name="auctiontype" class="contentfont" onchange="submitform();">
               <option value="standard" <? echo (($auctionEdit['auctiontype']=="dutch")?"":"selected");?>>
               <?=$lang[standardauc]?>
               </option>
               <option value="dutch" <? echo (($auctionEdit['auctiontype']=="dutch")?"selected":"");?>>
               <?=$lang[dutchauc]?>
               </option>
            </select></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[quant]?>
            </strong></td>
         <td colspan="2"><input name="quantity" type="text" class="contentfont" id="quantity" value="<? echo (($auctionEdit['quantity']>=1)? $auctionEdit['quantity']:"1");?>" size="8" <? echo ($auctionEdit['auctiontype']=="dutch")?"":"readonly";?>></td>
      </tr-->
      <tr class="c3">
         <td align="right" nowrap><strong>
            <?=$lang[aucstarts]?>
            </strong></td>
         <td colspan="2"><input name="startprice" type="text" class="contentfont" id="startprice" value="<?=$auctionEdit['bidstart'];?>" size="8">
            <?=$auctionEdit['currency'];?>
            <? if ($setts['buyout_process']==0) { ?><br><?=$lang[buynowonlyexplanation];?><? } ?></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[resprice]?>
            </strong></td>
         <td colspan="2"><input type="hidden" name="resprice_prv" value="<?=$auctionEdit['rpvalue'];?>">
            <input name="resprice" type="text" class="contentfont" id="resprice" value="<?=$auctionEdit['rpvalue'];?>" size="8" <? echo ($auctionEdit['auctiontype']=="dutch")?"readonly":"";?>>
            <?=$auctionEdit['currency'];?>
            <a href="javascript:popUpSmall('popup_rp.php');">
            <?=$lang[whatsthis]?>
            </a> </td>
      </tr>
		<? if ($setts['buyout_process']==0) { ?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[buynow]?>
            </strong></td>
         <td colspan="2"><input type="hidden" name="bn_prv" value="<?=$auctionEdit['bn'];?>">
            <input name="buynow" type="radio" value="N" <? echo(($auctionEdit['bn']=="N")?"checked":"");?>>
            <?=$lang[no]?>
            <input type="radio" name="buynow" value="Y" <? echo(($auctionEdit['bn']=="Y")?"checked":"");?>>
            <?=$lang[yes]?>
            <input name="bnprice" type="text" id="bnprice" value="<?=$auctionEdit['bnvalue'];?>" size="8">
            <?=$auctionEdit['currency'];?>
            <a href="javascript:popUpSmall('popup_bn.php');">
            <?=$lang[whatsthis]?>
            </a> </td>
      </tr>
		<? } else if ($setts['buyout_process']==1) { ?>
		<tr class="c2">
			<td align="right"><strong>
				<?=$lang[makeoffer]?>
				</strong></td>
			<td colspan="2"><input type="hidden" name="bn_prv" value="<?=$auctionEdit['bn'];?>">
            <input name="buynow" type="radio" value="N" <? echo(($auctionEdit['bn']=="N")?"checked":"");?>>
            <?=$lang[no]?>
            <input type="radio" name="buynow" value="Y" <? echo(($auctionEdit['bn']=="Y")?"checked":"");?>>
            <?=$lang[yes]?><br />
				<?=$lang[offerrange];?>: 
				<input name="offer_range_min" type="text" class="contentfont" id="offer_range_min" value="<?=$auctionEdit['offer_range_min'];?>" size="8">
				<?=$lang[to];?>
				<input name="offer_range_max" type="text" class="contentfont" id="offer_range_max" value="<?=$auctionEdit['offer_range_max'];?>" size="8"> <?=$auctionEdit['currency'];?></td>
		</tr>
		<? } ?>
     
	<!--08.05.2014 Eliminated Custom Bid scale-->
	 <!--tr class="c2">
         <td align="right"><strong>
            <?=$lang[bidincr]?>
            </strong></td>
         <td colspan="2"><input name="bidinc" type="radio" value="0" <? echo (($auctionEdit['bi']=="0")?"checked":"");?>>
            <?=$lang[builtinincr]?>
            <br>
            <input type="radio" name="bidinc" value="1" <? echo (($auctionEdit['bi']=="1")?"checked":"");?>>
            <?=$lang[customincr]?>
            <input name="bidincvalue" type="text" class="contentfont" id="bidincvalue" value="<?=$auctionEdit['bivalue'];?>" size="8">
            <?=$auctionEdit['currency'];?>
         </td>
      </tr-->
      <? if ($notStarted>0) { 
		$_REQUEST['starttime']="custom";
		$startDate_temp = displaydatetime($auction['startdate'], "Y-m-d H:i:s");
		$date = explode(" ",$startDate_temp);
		$date_day = explode("-",$date[0]);
		$date_hour = explode(":",$date[1]);
		$_REQUEST['date17_year'] = $date_day[0];
		$_REQUEST['date17_month'] = $date_day[1];
		$_REQUEST['date17_date'] = $date_day[2];
		$_REQUEST['shour'] = $date_hour[0];
		$_REQUEST['sminute'] = $date_hour[1];
		
		?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[auctionstarttime];?>
            </strong></td>
         <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td width="100"><input name="starttime" type="radio" value="NOW" checked="checked" />
                     <?=$lang[now];?></td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td><input name="starttime" type="radio" value="custom" <? echo ($_REQUEST['starttime']=="custom")?"checked":"";?> />
                     <?=$lang[customtime];?></td>
                  <td><select name="date17_month" id="date17_month" class="contentfont">
                        <option> </option>
                        <option value="01" <? echo ($_REQUEST['date17_month']==1)?"selected":"";?>>
                        <?=$lang[jan]?>
                        </option>
                        <option value="02" <? echo ($_REQUEST['date17_month']==2)?"selected":"";?>>
                        <?=$lang[feb]?>
                        </option>
                        <option value="03" <? echo ($_REQUEST['date17_month']==3)?"selected":"";?>>
                        <?=$lang[mar]?>
                        </option>
                        <option value="04" <? echo ($_REQUEST['date17_month']==4)?"selected":"";?>>
                        <?=$lang[apr]?>
                        </option>
                        <option value="05" <? echo ($_REQUEST['date17_month']==5)?"selected":"";?>>
                        <?=$lang[may]?>
                        </option>
                        <option value="06" <? echo ($_REQUEST['date17_month']==6)?"selected":"";?>>
                        <?=$lang[jun]?>
                        </option>
                        <option value="07" <? echo ($_REQUEST['date17_month']==7)?"selected":"";?>>
                        <?=$lang[jul]?>
                        </option>
                        <option value="08" <? echo ($_REQUEST['date17_month']==8)?"selected":"";?>>
                        <?=$lang[aug]?>
                        </option>
                        <option value="09" <? echo ($_REQUEST['date17_month']==9)?"selected":"";?>>
                        <?=$lang[sep]?>
                        </option>
                        <option value="10" <? echo ($_REQUEST['date17_month']==10)?"selected":"";?>>
                        <?=$lang[oct]?>
                        </option>
                        <option value="11" <? echo ($_REQUEST['date17_month']==11)?"selected":"";?>>
                        <?=$lang[nov]?>
                        </option>
                        <option value="12" <? echo ($_REQUEST['date17_month']==12)?"selected":"";?>>
                        <?=$lang[dec]?>
                        </option>
                     </select>
                     <select name="date17_date" class="contentfont">
                        <option> </option>
                        <option value="01" <? echo ($_REQUEST['date17_date']==1)?"selected":"";?>>1 </option>
                        <option value="02" <? echo ($_REQUEST['date17_date']==2)?"selected":"";?>>2 </option>
                        <option value="03" <? echo ($_REQUEST['date17_date']==3)?"selected":"";?>>3 </option>
                        <option value="04" <? echo ($_REQUEST['date17_date']==4)?"selected":"";?>>4 </option>
                        <option value="05" <? echo ($_REQUEST['date17_date']==5)?"selected":"";?>>5 </option>
                        <option value="06" <? echo ($_REQUEST['date17_date']==6)?"selected":"";?>>6 </option>
                        <option value="07" <? echo ($_REQUEST['date17_date']==7)?"selected":"";?>>7 </option>
                        <option value="08" <? echo ($_REQUEST['date17_date']==8)?"selected":"";?>>8 </option>
                        <option value="09" <? echo ($_REQUEST['date17_date']==9)?"selected":"";?>>9 </option>
                        <option value="10" <? echo ($_REQUEST['date17_date']==10)?"selected":"";?>>10 </option>
                        <option value="11" <? echo ($_REQUEST['date17_date']==11)?"selected":"";?>>11 </option>
                        <option value="12" <? echo ($_REQUEST['date17_date']==12)?"selected":"";?>>12 </option>
                        <option value="13" <? echo ($_REQUEST['date17_date']==13)?"selected":"";?>>13 </option>
                        <option value="14" <? echo ($_REQUEST['date17_date']==14)?"selected":"";?>>14 </option>
                        <option value="15" <? echo ($_REQUEST['date17_date']==15)?"selected":"";?>>15 </option>
                        <option value="16" <? echo ($_REQUEST['date17_date']==16)?"selected":"";?>>16 </option>
                        <option value="17" <? echo ($_REQUEST['date17_date']==17)?"selected":"";?>>17 </option>
                        <option value="18" <? echo ($_REQUEST['date17_date']==18)?"selected":"";?>>18 </option>
                        <option value="19" <? echo ($_REQUEST['date17_date']==19)?"selected":"";?>>19 </option>
                        <option value="20" <? echo ($_REQUEST['date17_date']==20)?"selected":"";?>>20 </option>
                        <option value="21" <? echo ($_REQUEST['date17_date']==21)?"selected":"";?>>21 </option>
                        <option value="22" <? echo ($_REQUEST['date17_date']==22)?"selected":"";?>>22 </option>
                        <option value="23" <? echo ($_REQUEST['date17_date']==23)?"selected":"";?>>23 </option>
                        <option value="24" <? echo ($_REQUEST['date17_date']==24)?"selected":"";?>>24 </option>
                        <option value="25" <? echo ($_REQUEST['date17_date']==25)?"selected":"";?>>25 </option>
                        <option value="26" <? echo ($_REQUEST['date17_date']==26)?"selected":"";?>>26 </option>
                        <option value="27" <? echo ($_REQUEST['date17_date']==27)?"selected":"";?>>27 </option>
                        <option value="28" <? echo ($_REQUEST['date17_date']==28)?"selected":"";?>>28 </option>
                        <option value="29" <? echo ($_REQUEST['date17_date']==29)?"selected":"";?>>29 </option>
                        <option value="30" <? echo ($_REQUEST['date17_date']==30)?"selected":"";?>>30 </option>
                        <option value="31" <? echo ($_REQUEST['date17_date']==31)?"selected":"";?>>31 </option>
                     </select>
                     <select name="date17_year" class="contentfont">
                        <option> </option>
                     <option value="2010" <? echo ($_REQUEST['date17_year']==2010)?"selected":"";?>>2010 </option>
                      <OPTION VALUE="2011" <? echo ($_REQUEST['date17_year']==2011)?"selected":"";?>>2011
                     <OPTION VALUE="2012" <? echo ($_REQUEST['date17_year']==2012)?"selected":"";?>>2012
                     <OPTION VALUE="2013" <? echo ($_REQUEST['date17_year']==2013)?"selected":"";?>>2013    
                     <OPTION VALUE="2014" <? echo ($_REQUEST['date17_year']==2014)?"selected":"";?>>2014  
                     <OPTION VALUE="2015" <? echo ($_REQUEST['date17_year']==2015)?"selected":"";?>>2015     
                     </select>
                     <a href="#" onclick="cal17.showCalendar('anchor17',getDateString(document.forms[2].date17_year,document.forms[2].date17_month,document.forms[2].date17_date)); return false;" title="cal17.showCalendar('anchor17',getDateString(document.forms[2].date17_year,document.forms[2].date17_month,document.forms[2].date17_date)); return false;" name="anchor17" id="anchor17"><img src="themes/<?=$setts['default_theme'];?>/img/system/calendar_b2u.gif" border="0" align="absmiddle" /></a>
                     <select name="shour"  class="contentfont" id="shour">
                        <option value="0" selected="selected">00</option>
                        <option value="1" <? echo (($_REQUEST['shour']==1)?"selected":"");?>>01</option>
                        <option value="2" <? echo (($_REQUEST['shour']==2)?"selected":"");?>>02</option>
                        <option value="3" <? echo (($_REQUEST['shour']==3)?"selected":"");?>>03</option>
                        <option value="4" <? echo (($_REQUEST['shour']==4)?"selected":"");?>>04</option>
                        <option value="5" <? echo (($_REQUEST['shour']==5)?"selected":"");?>>05</option>
                        <option value="6" <? echo (($_REQUEST['shour']==6)?"selected":"");?>>06</option>
                        <option value="7" <? echo (($_REQUEST['shour']==7)?"selected":"");?>>07</option>
                        <option value="8" <? echo (($_REQUEST['shour']==8)?"selected":"");?>>08</option>
                        <option value="9" <? echo (($_REQUEST['shour']==9)?"selected":"");?>>09</option>
                        <option value="10" <? echo (($_REQUEST['shour']==10)?"selected":"");?>>10</option>
                        <option value="11" <? echo (($_REQUEST['shour']==11)?"selected":"");?>>11</option>
                        <option value="12" <? echo (($_REQUEST['shour']==12)?"selected":"");?>>12</option>
                        <option value="13" <? echo (($_REQUEST['shour']==13)?"selected":"");?>>13</option>
                        <option value="14" <? echo (($_REQUEST['shour']==14)?"selected":"");?>>14</option>
                        <option value="15" <? echo (($_REQUEST['shour']==15)?"selected":"");?>>15</option>
                        <option value="16" <? echo (($_REQUEST['shour']==16)?"selected":"");?>>16</option>
                        <option value="17" <? echo (($_REQUEST['shour']==17)?"selected":"");?>>17</option>
                        <option value="18" <? echo (($_REQUEST['shour']==18)?"selected":"");?>>18</option>
                        <option value="19" <? echo (($_REQUEST['shour']==19)?"selected":"");?>>19</option>
                        <option value="20" <? echo (($_REQUEST['shour']==20)?"selected":"");?>>20</option>
                        <option value="21" <? echo (($_REQUEST['shour']==21)?"selected":"");?>>21</option>
                        <option value="22" <? echo (($_REQUEST['shour']==22)?"selected":"");?>>22</option>
                        <option value="23" <? echo (($_REQUEST['shour']==23)?"selected":"");?>>23</option>
                     </select>
                     :
                     <select name="sminute" class="contentfont" id="sminute">
                        <option value="00" selected="selected">00</option>
                        <option value="15" <? echo (($_REQUEST['sminute']==15)?"selected":"");?>>15</option>
                        <option value="30" <? echo (($_REQUEST['sminute']==30)?"selected":"");?>>30</option>
                        <option value="45" <? echo (($_REQUEST['sminute']==45)?"selected":"");?>>45</option>
                     </select></td>
               </tr>
            </table></td>
      </tr>
      <? } 
		$endDate_temp = displaydatetime($auction['enddate'], "Y-m-d H:i:s");
		
		$edate = explode(" ",$endDate_temp);
		$edate_day = explode("-",$edate[0]);
		$edate_hour = explode(":",$edate[1]);
		$_REQUEST['date18_year'] = $edate_day[0];
		$_REQUEST['date18_month'] = $edate_day[1];
		$_REQUEST['date18_date'] = $edate_day[2];
		$_REQUEST['eshour'] = $edate_hour[0];
		$_REQUEST['esminute'] = $edate_hour[1];
		?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[endtime]?>
            </strong></td>
         <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td width="100"><input name="endtime" type="radio" value="duration" checked="checked" />
                     <?=$lang[duration];?></td>
                  <td><? echo "<SELECT name=\"duration\" class=\"contentfont\">";
							$getdurations=mysql_query("SELECT * FROM probid_auction_durations");
							while ($row=mysql_fetch_array($getdurations)) {
								echo "<OPTION value=\"".$row['days']."\" ".(($row['days']==$auctionEdit['duration'])?"SELECTED":"").">".$row['description']."</option>";
							}
							echo "</SELECT>";
							?></td>
               </tr>
               <tr>
                  <td><input name="endtime" type="radio" value="customtime" <? echo ($auctionEdit['endtime']=="customtime") ? "checked" : ""; ?> />
                     <?=$lang[customtime];?></td>
                  <td><select name="date18_month" id="date18_month" class="contentfont">
                        <option> </option>
                        <option value="01" <? echo ($_REQUEST['date18_month']==1)?"selected":"";?>>
                        <?=$lang[jan]?>
                        </option>
                        <option value="02" <? echo ($_REQUEST['date18_month']==2)?"selected":"";?>>
                        <?=$lang[feb]?>
                        </option>
                        <option value="03" <? echo ($_REQUEST['date18_month']==3)?"selected":"";?>>
                        <?=$lang[mar]?>
                        </option>
                        <option value="04" <? echo ($_REQUEST['date18_month']==4)?"selected":"";?>>
                        <?=$lang[apr]?>
                        </option>
                        <option value="05" <? echo ($_REQUEST['date18_month']==5)?"selected":"";?>>
                        <?=$lang[may]?>
                        </option>
                        <option value="06" <? echo ($_REQUEST['date18_month']==6)?"selected":"";?>>
                        <?=$lang[jun]?>
                        </option>
                        <option value="07" <? echo ($_REQUEST['date18_month']==7)?"selected":"";?>>
                        <?=$lang[jul]?>
                        </option>
                        <option value="08" <? echo ($_REQUEST['date18_month']==8)?"selected":"";?>>
                        <?=$lang[aug]?>
                        </option>
                        <option value="09" <? echo ($_REQUEST['date18_month']==9)?"selected":"";?>>
                        <?=$lang[sep]?>
                        </option>
                        <option value="10" <? echo ($_REQUEST['date18_month']==10)?"selected":"";?>>
                        <?=$lang[oct]?>
                        </option>
                        <option value="11" <? echo ($_REQUEST['date18_month']==11)?"selected":"";?>>
                        <?=$lang[nov]?>
                        </option>
                        <option value="12" <? echo ($_REQUEST['date18_month']==12)?"selected":"";?>>
                        <?=$lang[dec]?>
                        </option>
                     </select>
                     <select name="date18_date" class="contentfont">
                        <option> </option>
                        <option value="01" <? echo ($_REQUEST['date18_date']==1)?"selected":"";?>>1 </option>
                        <option value="02" <? echo ($_REQUEST['date18_date']==2)?"selected":"";?>>2 </option>
                        <option value="03" <? echo ($_REQUEST['date18_date']==3)?"selected":"";?>>3 </option>
                        <option value="04" <? echo ($_REQUEST['date18_date']==4)?"selected":"";?>>4 </option>
                        <option value="05" <? echo ($_REQUEST['date18_date']==5)?"selected":"";?>>5 </option>
                        <option value="06" <? echo ($_REQUEST['date18_date']==6)?"selected":"";?>>6 </option>
                        <option value="07" <? echo ($_REQUEST['date18_date']==7)?"selected":"";?>>7 </option>
                        <option value="08" <? echo ($_REQUEST['date18_date']==8)?"selected":"";?>>8 </option>
                        <option value="09" <? echo ($_REQUEST['date18_date']==9)?"selected":"";?>>9 </option>
                        <option value="10" <? echo ($_REQUEST['date18_date']==10)?"selected":"";?>>10 </option>
                        <option value="11" <? echo ($_REQUEST['date18_date']==11)?"selected":"";?>>11 </option>
                        <option value="12" <? echo ($_REQUEST['date18_date']==12)?"selected":"";?>>12 </option>
                        <option value="13" <? echo ($_REQUEST['date18_date']==13)?"selected":"";?>>13 </option>
                        <option value="14" <? echo ($_REQUEST['date18_date']==14)?"selected":"";?>>14 </option>
                        <option value="15" <? echo ($_REQUEST['date18_date']==15)?"selected":"";?>>15 </option>
                        <option value="16" <? echo ($_REQUEST['date18_date']==16)?"selected":"";?>>16 </option>
                        <option value="17" <? echo ($_REQUEST['date18_date']==17)?"selected":"";?>>17 </option>
                        <option value="18" <? echo ($_REQUEST['date18_date']==18)?"selected":"";?>>18 </option>
                        <option value="19" <? echo ($_REQUEST['date18_date']==19)?"selected":"";?>>19 </option>
                        <option value="20" <? echo ($_REQUEST['date18_date']==20)?"selected":"";?>>20 </option>
                        <option value="21" <? echo ($_REQUEST['date18_date']==21)?"selected":"";?>>21 </option>
                        <option value="22" <? echo ($_REQUEST['date18_date']==22)?"selected":"";?>>22 </option>
                        <option value="23" <? echo ($_REQUEST['date18_date']==23)?"selected":"";?>>23 </option>
                        <option value="24" <? echo ($_REQUEST['date18_date']==24)?"selected":"";?>>24 </option>
                        <option value="25" <? echo ($_REQUEST['date18_date']==25)?"selected":"";?>>25 </option>
                        <option value="26" <? echo ($_REQUEST['date18_date']==26)?"selected":"";?>>26 </option>
                        <option value="27" <? echo ($_REQUEST['date18_date']==27)?"selected":"";?>>27 </option>
                        <option value="28" <? echo ($_REQUEST['date18_date']==28)?"selected":"";?>>28 </option>
                        <option value="29" <? echo ($_REQUEST['date18_date']==29)?"selected":"";?>>29 </option>
                        <option value="30" <? echo ($_REQUEST['date18_date']==30)?"selected":"";?>>30 </option>
                        <option value="31" <? echo ($_REQUEST['date18_date']==31)?"selected":"";?>>31 </option>
                     </select>
                     <select name="date18_year" class="contentfont">
                        <option> </option>
                     <OPTION VALUE="2010" <? echo ($_REQUEST['date18_year']==2010)?"selected":"";?>>2010
                     <OPTION VALUE="2011" <? echo ($_REQUEST['date18_year']==2011)?"selected":"";?>>2011
                     <OPTION VALUE="2012" <? echo ($_REQUEST['date18_year']==2012)?"selected":"";?>>2012
                     <OPTION VALUE="2013" <? echo ($_REQUEST['date18_year']==2013)?"selected":"";?>>2013    
                     <OPTION VALUE="2014" <? echo ($_REQUEST['date18_year']==2014)?"selected":"";?>>2014  
                     <OPTION VALUE="2015" <? echo ($_REQUEST['date18_year']==2015)?"selected":"";?>>2015  
                     </select>
                     <a href="#" onclick="cal18.showCalendar('anchor18',getDateString(document.forms[2].date18_year,document.forms[2].date18_month,document.forms[2].date18_date)); return false;" title="cal18.showCalendar('anchor18',getDateString(document.forms[2].date18_year,document.forms[2].date18_month,document.forms[2].date18_date)); return false;" name="anchor18" id="anchor18"><img src="themes/<?=$setts['default_theme'];?>/img/system/calendar_b2u.gif" border="0" align="absmiddle" /></a>
                     <select name="eshour"  class="contentfont" id="eshour">
                        <option value="0" selected="selected">00</option>
                        <option value="1" <? echo (($_REQUEST['eshour']==1)?"selected":"");?>>01</option>
                        <option value="2" <? echo (($_REQUEST['eshour']==2)?"selected":"");?>>02</option>
                        <option value="3" <? echo (($_REQUEST['eshour']==3)?"selected":"");?>>03</option>
                        <option value="4" <? echo (($_REQUEST['eshour']==4)?"selected":"");?>>04</option>
                        <option value="5" <? echo (($_REQUEST['eshour']==5)?"selected":"");?>>05</option>
                        <option value="6" <? echo (($_REQUEST['eshour']==6)?"selected":"");?>>06</option>
                        <option value="7" <? echo (($_REQUEST['eshour']==7)?"selected":"");?>>07</option>
                        <option value="8" <? echo (($_REQUEST['eshour']==8)?"selected":"");?>>08</option>
                        <option value="9" <? echo (($_REQUEST['eshour']==9)?"selected":"");?>>09</option>
                        <option value="10" <? echo (($_REQUEST['eshour']==10)?"selected":"");?>>10</option>
                        <option value="11" <? echo (($_REQUEST['eshour']==11)?"selected":"");?>>11</option>
                        <option value="12" <? echo (($_REQUEST['eshour']==12)?"selected":"");?>>12</option>
                        <option value="13" <? echo (($_REQUEST['eshour']==13)?"selected":"");?>>13</option>
                        <option value="14" <? echo (($_REQUEST['eshour']==14)?"selected":"");?>>14</option>
                        <option value="15" <? echo (($_REQUEST['eshour']==15)?"selected":"");?>>15</option>
                        <option value="16" <? echo (($_REQUEST['eshour']==16)?"selected":"");?>>16</option>
                        <option value="17" <? echo (($_REQUEST['eshour']==17)?"selected":"");?>>17</option>
                        <option value="18" <? echo (($_REQUEST['eshour']==18)?"selected":"");?>>18</option>
                        <option value="19" <? echo (($_REQUEST['eshour']==19)?"selected":"");?>>19</option>
                        <option value="20" <? echo (($_REQUEST['eshour']==20)?"selected":"");?>>20</option>
                        <option value="21" <? echo (($_REQUEST['eshour']==21)?"selected":"");?>>21</option>
                        <option value="22" <? echo (($_REQUEST['eshour']==22)?"selected":"");?>>22</option>
                        <option value="23" <? echo (($_REQUEST['eshour']==23)?"selected":"");?>>23</option>
                     </select>
                     :
                     <select name="esminute" class="contentfont" id="esminute">
                        <option value="00" selected="selected">00</option>
                        <option value="15" <? echo (($_REQUEST['esminute']==15)?"selected":"");?>>15</option>
                        <option value="30" <? echo (($_REQUEST['esminute']==30)?"selected":"");?>>30</option>
                        <option value="45" <? echo (($_REQUEST['esminute']==45)?"selected":"");?>>45</option>
                     </select>
                  </td>
               </tr>
            </table></td>
      </tr>
      <? if ($setts['hp_feat']!=0||$setts['cat_feat']!=0||$setts['bold_item']!=0||$setts['hl_item']!=0) { ?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[featitem]?>
            </strong></td>
         <td colspan="2"><? 
				if ($setts['hp_feat']!=0) {
					if ($auction['hpfeat']=="Y") { 
						echo "$lang[featdhomepage]<br>									\n";
						echo "<input type=hidden name=hpfeat value=Y>					\n";
						echo "<input type=hidden name=hpfeat_prv value=Y>";
					} else {
						echo "<input type=\"checkbox\" name=\"hpfeat\" value=\"Y\" ".(($_REQUEST['hpfeat']=="Y")?"checked":"").">$lang[feathomepage]<br>";
					}
				}
				if ($setts['cat_feat']!=0) {
					if ($auction['catfeat']=="Y") { 
						echo "$lang[featdcatpage]<br>									\n";
						echo "<input type=hidden name=catfeat value=Y>					\n";
						echo "<input type=hidden name=catfeat_prv value=Y>";
					} else {
						echo "<input type=\"checkbox\" name=\"catfeat\" value=\"Y\" ".(($_REQUEST['catfeat']=="Y")?"checked":"").">$lang[featcatpage]<br>";
					}
				}
				if ($setts['bold_item']!=0) {
					if ($auction['bolditem']=="Y") { 
						echo "$lang[itembold]<br>										\n";
						echo "<input type=hidden name=bolditem value=Y>					\n";
						echo "<input type=hidden name=bolditem_prv value=Y>";
					} else {
						echo "<input type=\"checkbox\" name=\"bolditem\" value=\"Y\" ".(($_REQUEST['bolditem']=="Y")?"checked":"").">$lang[bolditem]<br>";
					}
				}
				if ($setts['hl_item']!=0) {
					if ($auction['hlitem']=="Y") { 
						echo "$lang[itemhighlighted]<br>								\n";
						echo "<input type=hidden name=hlitem value=Y>					\n";
						echo "<input type=hidden name=hlitem_prv value=Y>";
					} else {					
						echo "<input type=\"checkbox\" name=\"hlitem\" value=\"Y\" ".(($_REQUEST['hlitem  ']=="Y")?"checked":"").">$lang[highlighteditem]<br>";
					}
				} ?>
         </td>
      </tr>
      <? if ($setts['hpfeat_desc']=="Y"&&$setts['hp_feat']!=0) { ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[hpfeat_desc]?>
            </strong></td>
         <td colspan="2"><? echo "<textarea name=\"hpfeat_desc\" cols=\"50\" rows=\"4\">".$auctionEdit['hpfeat_desc']."</textarea><br>"; ?> </td>
      </tr>
      <? } ?>
      <? } ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[country]?>
            </strong></td>
         <td colspan="2"><? 
				echo "<SELECT name=\"country\" class=\"contentfont\">";
				$getCountries=mysql_query("SELECT * FROM probid_countries");
				while ($countryDetails=mysql_fetch_array($getCountries)) {
					echo "<OPTION value=\"".$countryDetails['name']."\" ".(($countryDetails['name']==$auctionEdit['country'])?"SELECTED":"").">".$countryDetails['name']."</option>";
				}
				echo "</SELECT>"; ?>
         </td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[zip]?>
            </strong></td>
         <td colspan="2"><input name="zip" type="text" class="contentfont" id="zip" value="<?=$auctionEdit['zip'];?>" size="12"></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[shippingcond]?>
            </strong></td>
         <td colspan="2"><input type="radio" name="shipcond" value="BP" <? echo (($auctionEdit['sc']=="BP")?"checked":"");?>>
            <?=$lang[buyerpaysshipment]?>
            <br>
            <input type="radio" name="shipcond" value="SP" <? echo (($auctionEdit['sc']=="SP")?"checked":"");?>>
            <?=$lang[sellerpaysshipment]?>
            <br>
            <input name="shipint" type="checkbox" id="shipint" value="Y" <? echo (($auctionEdit['scint']=="Y")?"checked":"");?>>
            <?=$lang[sellershipinternat]?>
         </td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[privateauc]?>
            </strong></td>
         <td colspan="2"><select name="privateauct" id="privateauct" class="contentfont">
               <option value="Y" <? echo (($auctionEdit['private']=="Y")?"selected":"");?>>
               <?=$lang[yes]?>
               </option>
               <option value="N" <? echo (($auctionEdit['private']!="Y")?"selected":"");?>>
               <?=$lang[no]?>
               </option>
            </select>
            <a href="javascript:popUpSmall('popup_pa.php');">
            <?=$lang[whatsthis]?>
            </a></td>
      </tr>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[auccurr]?>
            </strong></td>
         <? $getcurrencies=mysql_query("SELECT * FROM probid_currencies"); ?>
         <td colspan="2"><select name="currency">
               <? while ($ccrow=mysql_fetch_array($getcurrencies)) {  
						echo "<option value=\"".$ccrow['symbol']."\" ".(($auctionEdit['currency']==$ccrow['symbol'])?"selected":"").">".$ccrow['symbol']." ".$ccrow['caption']."</option>"; 
		  			} ?>
            </select></td>
      </tr>
      <? if ($setts['swap_items']==1) { ?>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[acceptswap]?>
            </strong></td>
         <td colspan="2"><input name="isswap" type="radio" value="N" <? echo(($auctionEdit['isswap']=="N")?"checked":"");?>>
            <?=$lang[no]?>
            <input type="radio" name="isswap" value="Y" <? echo(($auctionEdit['isswap']=="Y")?"checked":"");?>>
            <?=$lang[yes]?></td>
      </tr>
      <? } ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[auto_relist];?>
            </strong></td>
         <td colspan="2"><input name="auto_relist" type="radio" value="N" checked>
            <?=$lang[no]?>
            <input type="radio" name="auto_relist" value="Y" <? echo(($auctionEdit['auto_relist']=="Y")?"checked":"");?>>
            <?=$lang[yes]?></td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[auto_relist_bids];?>
            </strong></td>
         <td colspan="2"><input name="auto_relist_bids" type="radio" value="N" checked>
            <?=$lang[no]?>
            <input type="radio" name="auto_relist_bids" value="Y" <? echo(($auctionEdit['auto_relist_bids']=="Y")?"checked":"");?>>
            <?=$lang[yes]?>
            <br>
            <?=$lang[auto_relist_msg];?></td>
      </tr>
      <tr class="c3">
         <td align="right"><strong>
            <?=$lang[auto_relist_nb];?>
            </strong></td>
         <td colspan="2"><input name="auto_relist_nb" type="text" value="<?=$auctionEdit['auto_relist_nb'];?>" size="8"  onchange="revertMaxRelist(sistep1);">
				<br>
   	      <?=$lang[auto_relist_nb_msg];?></td>
      </tr>
      <? if ($setts['paypaldirectpayment']):?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[directpayment]?>
            </strong></td>
         <td colspan="2"><?
            $selectedPaymentSystems = explode(',',$auction['accept_payment_systems']);
            $getPaymentSystems = mysql_query("SELECT * FROM probid_direct_payment WHERE status=1");
            $seller = getSqlRow("SELECT * FROM probid_users WHERE id='".$auction['ownerid']."'");
				while($paySystem = mysql_fetch_array($getPaymentSystems)){
              	$enabled = false;
            	switch($paySystem['id']){
						case 1:
                    	if($seller['paypalemail']) $enabled = true;
                     break;
                  case 2:
                     if($seller['worldpayid']) $enabled = true;
                     break;
                  case 3:
                     if($seller['ikoboid']&&$seller['ikoboipn']) $enabled = true;
                     break;
                  case 4:
                     if($seller['nochexemail']) $enabled = true;
                     break;
                  case 5:
                     if($seller['checkoutid']) $enabled = true;
                     break;
                  case 6:
                     if($seller['authnetid']&&$seller['authnettranskey']) $enabled = true;
                     break;
                  case 7:
                     if($seller['protxname']&&$seller['protxpassword']) $enabled = true;
                     break;
               }
				
            	echo '<input type="checkbox" value="1" name="acceptdirectpayment['.$paySystem['id'].']"'.((in_array($paySystem['id'],$selectedPaymentSystems)&&$enabled) ? " checked" : '').' '.($enabled?'':'disabled').'>'.$paySystem['name'].'<br>';
            }
        	?>
         </td>
      </tr>
      <? endif ?>
      <tr class="c2">
         <td align="right"><strong>
            <?=$lang[paymethods]?>
            </strong></td>
         <td colspan="2"><? 
				$getPaymentMethods=mysql_query("SELECT * FROM probid_payment_methods");
				while ($paymentMethod=mysql_fetch_array($getPaymentMethods)) {
					$issel="";
					if (eregi($paymentMethod['name'],$auction['pm'])) $issel="checked";
					echo "<input type=\"checkbox\" name=\"pmethod[]\" value=\"".$paymentMethod['name']."\" ".$issel.">".$paymentMethod['name']."<br>";
				} ?>
         </td>
      </tr>
      <? if ($setts['shipping_costs']==1) { ?>
      <tr class="c3">
         <td align="right" valign="top"><strong>
            <?=$lang['postagecosts']?>
            </strong></td>
         <td colspan="2"><?=$auctionEdit['currency'];?>
            <input class="contentfont" name="postage_costs" type="text" id="postage_costs" value="<?=$auctionEdit['postage_costs'];?>" size="15"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><strong>
            <?=$lang[insurance]?>
            </strong></td>
         <td colspan="2"><?=$auctionEdit['currency'];?>
            <input name="insurance" type="text" id="insurance" value="<?=$auctionEdit['insurance'];?>" size="15"></td>
      </tr>
      <tr class="c2">
         <td align="right" valign="top"><strong>
            <?=$lang[sp_details]?>
            </strong></td>
         <td colspan="2"><textarea name="shipping_details" cols="50" rows="4"><? echo addSpecialChars($auctionEdit['shipping_details']);?></textarea></td>
      </tr>
      <tr class="c3">
         <td align="right" valign="top"><strong>
            <?=$lang[servicetype]?>
            </strong></td>
         <td colspan="2"><? 
				echo "<SELECT name=\"type_service\" class=\"contentfont\">";
				echo "<option value=\"\" selected>".$lang[servicetypeselect]."</option>";
				$getShippingOptions=mysql_query("SELECT * FROM probid_shipping_options");
				while ($shOpt=mysql_fetch_array($getShippingOptions)) {
					echo "<OPTION value=\"".$shOpt['name']."\" ".(($shOpt['name']==$auctionEdit['type_service'])?"SELECTED":"").">".$shOpt['name']."</option>";
				}
				echo "</SELECT>"; ?></td>
      </tr>
      <? }  
		$getFields = mysql_query("SELECT DISTINCT boxid, boxname, boxtype, active FROM probid_fields WHERE 
		(categoryid='".$mainCat_primary."' OR categoryid='".$mainCat_secondary."' OR categoryid='0') ORDER BY fieldorder ASC") or die(mysql_error()); 
		$isFields = mysql_num_rows($getFields);
		if ($isFields) {
			while ($fields=mysql_fetch_array($getFields)) {
				$fieldData = getSqlRow("SELECT boxvalue FROM probid_fields_data WHERE auctionid='".$auction['id']."' AND ownerid='".$_SESSION['memberid']."' AND boxid='".$fields['boxid']."'"); 
				if ($fields['boxtype']=="checkbox") $selectedValue = explode(";",$fieldData['boxvalue']); 
				else $selectedValue = $fieldData['boxvalue']; ?>
      <tr class="<? echo (($count++)%2==0)?"c2":"c3"; ?>">
         <td align="right"><strong>
            <?=$fields['boxname'];?>
            </strong></td>
         <td colspan="2"><? echo createField($fields['boxid'],$selectedValue); ?> </td>
      </tr>
      <? } 
  		} ?>
      <tr class="c2">
         <td colspan="3"><strong>
            <?=$lang[choosecat]?>
            </strong><br>
            <br>
            <input type="hidden" name="cat_prv" value="<?=$auction['category'];?>">
            <select name="category" class="contentfont" onchange="submitform();">
               <?
					while (list($catid, $cat_array_details) = each ($cat_array)){
						list($catname, $userid) = $cat_array_details;
						$x = getSqlRow("SELECT parent FROM probid_categories WHERE id=".$catid."");
						if ($userid==0||($auctionEdit['listin']=="store"&&$userid==$_SESSION['memberid'])) 
							echo '<option value="'.$catid.'" '.(($auctionEdit['category'] ==$catid)?"selected":"").'>'.($c_lang[$x["parent"]]!=''?$c_lang[$x["parent"]].':':'').' '.$c_lang[$catid].'</option>';
					} 
					?>
            </select></td>
      </tr>
<?
       if ($setts['secondcategory']) { ?>
      <tr class="c2">
         <td colspan="3"><strong>
            <?=$lang[chooseaddlcat]?>
            </strong><br>
            <br>
            <input type="hidden" name="addlcat_prv" value="<?=$auction['addlcategory'];?>">
            <select name="addlcategory" class="contentfont" onchange="submitform();">
               <?
					reset($cat_array);
					echo '<option value="" selected>-- no additional category --</option>';
					while (list($catid, $cat_array_details) = each ($cat_array)){
						list($catname, $userid) = $cat_array_details;
						
						 $x = getSqlRow("SELECT parent FROM probid_categories WHERE id=".$catid."");
						if ($userid==0||($auctionEdit['listin']=="store"&&$userid==$_SESSION['memberid'])) echo '<option value="'.$catid.'" '.(($auctionEdit['addlcategory'] ==$catid)?"selected":"").'>'.($c_lang[$x["parent"]]!=''?$c_lang[$x["parent"]].':':'').' '.$c_lang[$catid].'</option>';
					} ?>
            </select></td>
      </tr>
      <? } ?>
      <tr align="center" class="c4">
         <td colspan="3"><input name="additemok" type="submit" id="additemok" value="<?=$lang[modify]?>"></td>
      </tr>
   </table>
</form>
<? 	}
	}
	include ("themes/".$setts['default_theme']."/footer.php"); 
} ?>
