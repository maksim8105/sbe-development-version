<?php

/**
 * Отображение форм для оплаты счёта в настройках у пользователя
 * в заданном порядке
 *
 * $paymentAmount - сумма платежа
 * $description - описание платежа
 * 4 - Id изменяемой таблицы
 */

$description = 'Oksjonitasu SBE ' . $_SESSION['membername'];

// Hansapank
if ($selectedPGs['name']=="Hansapank") {
	hansaPangalinkForm($_SESSION['memberid'],$paymentAmount, $description, 4);
}

// SEB Uhispank
if ($selectedPGs['name']=="SEB") {
	sebPangalinkForm($_SESSION['memberid'],$paymentAmount, $description, 4);
}

// Krediidipank
if ($selectedPGs['name']=="Krediidipank") {
	krepPangalinkForm($_SESSION['memberid'],$paymentAmount, $description, 4 );
}

// Sampopank
if ($selectedPGs['name']=="Sampopank") {
	sampoPangalinkForm($_SESSION['memberid'],$paymentAmount, $description, 4);
}

?>