<?php

class Pangalink
{
	var
		$config     = array(), // Массив конфигурации
		$pangalinks = array(), // Инициализированные пангалинки
		$debugMode  = false,   // Режим отладки
		$thisDir;              // Текущая директория

	/**
	 * Конструктор
	 * Устанавливает приватные и публичные ключи и настройки полей форм
	 */
	function Pangalink( $config, $debugMode = false )
	{
		// Режим отладки
		$this->debugMode = $debugMode;

		$this->thisDir = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;

		// Загрузить конфигурацию
		if ( ! $this->loadConfig( $config ) )
		{
			$this->printDebugMessage( sprintf( 'Не могу загрузить конфигурацию Pangalink (%s)', __LINE__ ), E_USER_WARNING );
		}
	}

	/**
	 * Инициализация конфигурации pangalink
	 */
	function loadConfig( $config )
	{
		if ( is_array( $config ) && count( $config ) )
		{
			$this->config = $config;
		}
		elseif ( is_string( $config ) && is_readable( $this->thisDir . $config ) )
		{
			$this->config = parse_ini_file( $this->thisDir . $config, true );
		}
		else
		{
			$this->printDebugMessage( sprintf( 'Не задана конфигурация Pangalink (%s)', __LINE__ ), E_USER_WARNING );
			return false;
		}

		return true;
	}

	function getPangaList()
	{
		$panga_list = array();

		foreach ( $this->config as $VK_SND_ID => $panga_config )
		{
			$panga_list[ $VK_SND_ID ] = array(
				'panga_name' => $panga_config['panga_name'],
				'panga_url'  => $panga_config['panga_url'],
				'image_url'  => $panga_config['image_url'],
			);
		}

		return $panga_list;
	}

	/**
	 * Был ли получен в текущей сессии банковский ответ
	 */
	function hasPangalinkReply()
	{
		if ( isset( $_REQUEST['VK_SND_ID'] ) && is_string( $_REQUEST['VK_SND_ID'] ) && '' !== trim( $_REQUEST['VK_SND_ID'] ) )
		{
			return $this->pangalinkExists( $_REQUEST['VK_SND_ID'] );
		}

		return null;
	}

	/**
	 * Есть ли данные для работы с указанным банком
	 */
	function pangalinkExists( $VK_SND_ID )
	{
		return array_key_exists( $VK_SND_ID, $this->config );
	}

	/**
	 * Был ли инициализирован конкретный pangalink
	 */
	function pangalinkInitialized( $VK_SND_ID )
	{
		return array_key_exists( $VK_SND_ID, $this->pangalinks );
	}

	/**
	 * Инициализировать Pangalink
	 *
	 * @return ConcretePangalink
	 */
	function initializePangalink( $VK_SND_ID = null )
	{
		if ( is_null( $VK_SND_ID ) )
		{
			if ( true === $this->hasPangalinkReply() )
			{
				return $this->initializePangalink( $_REQUEST['VK_SND_ID'] );
			}
			else
			{
				$this->printDebugMessage( sprintf( 'Не указан VK_SND_ID (%s)', __LINE__ ), E_USER_WARNING );
				return false;
			}
		}
		elseif ( $this->pangalinkInitialized( $VK_SND_ID ) )
		{
			return $this->pangalinks[ $VK_SND_ID ];
		}

		if ( $this->pangalinkExists( $VK_SND_ID ) )
		{
			include_once $this->thisDir . 'ConcretePangalink.class.php';

			// Инициализируем пангалинк
			$this->pangalinks[ $VK_SND_ID ] =& new ConcretePangalink( $VK_SND_ID, $this );

			return $this->pangalinks[ $VK_SND_ID ];
		}
		else
		{
			$this->printDebugMessage( sprintf( 'Pangalink "%s" не найден! (%s)', $VK_SND_ID, __LINE__ ), E_USER_WARNING );
			return false;
		}
	}

	function get( $key, $default = array() )
	{
		if ( array_key_exists( $key, $this->config ) )
		{
			return $this->config[ $key ];
		}
		else
		{
			return $default;
		}
	}

	/**
	 * Вывести отладочное сообщение
	 */
	function PrintDebugMessage( $debugMessage, $errorType = null )
	{
		if ( $this->debugMode )
		{
			/*
			// Запостить сообщение об ошибке
			if ( ! is_null( $errorType ) )
			{
				user_error( $debugMessage, $errorType );
			}
			*/

			echo '<div style="border: 1px solid red">';

			if ( is_array( $debugMessage ) )
			{
				echo '<pre>';
				print_r( $debugMessage );
				echo '</pre>';
			}
			else
			{
				echo '<b>Debug:</b> ' . $debugMessage;
			}

			echo '</div>';
		}
	}
}

?>