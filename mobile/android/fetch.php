<?php

$file_path = substr(__FILE__,0,strlen(__FILE__)-25);

if(file_exists("conf.php"))	{
	require_once("conf.php");
	require_once('class.html2text.inc');
}

function getExtension($str) {

         $i = strrpos($str,".");
         if (!$i) { return ""; } 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
}

function getSqlField($sqlQuery,$field) {
	$isQuery = getSqlNumber($sqlQuery);
	$query = @mysqli_query($GLOBALS["___mysqli_ston"], $sqlQuery) or die (((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))."<br>INVALID QUERY: ".$sqlQuery);
	if ($isQuery>0) {			$result=@mysqli_fetch_assoc($query)[$field];
	} else $result="n/a";
	@((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

function getChild($id)	{
	$result = array();
	$sub_query_begin = "SELECT id, parent FROM probid_categories WHERE ";
	$sub_query = $sub_query_begin."parent = ".$id.";";
	$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $sub_query);
	if(mysqli_num_rows($tmp) > 0)	{
		while($row =  mysqli_fetch_array($tmp))	{
			$result = array_merge((array)$result, (array)getChild($row['id']));
		}
	}	else	{
		$t = $id;
		$result = array_merge((array)$result, (array)$t);
	}
	return $result;
}

function getSqlNumber($sqlQuery) {
	$query=@mysqli_query($GLOBALS["___mysqli_ston"], $sqlQuery);
	$result=@mysqli_num_rows($query);
	@((mysqli_free_result($query) || (is_object($query) && (get_class($query) == "mysqli_result"))) ? true : false);
	return $result;
}

if(isset ($_GET["req"]))	{

	if($_GET["req"] == cat && isset ($_GET["lang"]))	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			
			$query = "SELECT probid_categories.id, probid_categories.name FROM probid_categories WHERE ";
			$sub_query_begin = "SELECT DISTINCT id, parent FROM probid_categories WHERE ";
			$sub_query = "";
			$parent_array = array();
			$tmp_cat = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT category FROM probid_auctions WHERE probid_auctions.bidstart = 0.01 AND enddate > now() AND probid_auctions.nrbids = 0 AND probid_auctions.auctiontype = 'standard'");
			while($row = mysqli_fetch_array($tmp_cat))	{
				$end = true;
				$category = $row['category'];
				while ($end)	{
					$sub_query .= $sub_query_begin; $sub_query .= "id = ".$category.";";
					$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $sub_query);
					while($r = mysqli_fetch_assoc($tmp))	{
						if($r['parent'] == 0)	{
							$parent_array[] = $r['id'];
							$end = false;
						}	else	{
							$category = $r['parent'];
						}
					}
					$sub_query = "";
				}
			}
			$parent_array = array_unique($parent_array);
			foreach($parent_array as $p)	{
				$query .= " id = ".$p." OR ";
			}
			$query = substr($query, 0, -4); 
			$query .= " ORDER BY name;";
			$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $query);	
			$file_path .= $lang_path;
			if($_GET["lang"] == "ru")	{
				$file_path .= $lang_rus;
			}	else if($_GET["lang"] == "et")	{
				$file_path .= $lang_est;
			}	else if($_GET["lang"] == "en")	{
				$file_path .= $lang_eng;
			}
			require_once($file_path .= $category_lang); 
			$result[] = array();
			while($row = mysqli_fetch_assoc($tmp))	{
				$c = $c_lang[$row['id']];
				$h2t =& new html2text($c);
				$pm = $h2t->get_text();
				utf8_encode($c);
				$responce = $row['id'].",".$pm.";";
				echo ($responce);
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		} 
	}
	
	if($_GET["req"] == login)	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			if(isset($_GET[usr]) && isset($_GET[pass]))	{
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT Count(*) as 'count' FROM probid_users WHERE username='".$_GET[usr]."' AND password = '".$_GET[pass]."';");
				
				while($row = mysqli_fetch_array($tmp))	{
					$responce = $row['count'];
					if($responce == 1)	{
						$usr_id = getSqlNumber("SELECT id FROM probid_users WHERE username='".$_GET[usr]."'");
						$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT Count(*) as 'count' FROM probid_blocked_users WHERE userid='".$usr_id."'");
						$row = mysqli_fetch_array($tmp);
						$isBanned = $row['count'];
						if($isBanned == 1)	{
							$responce = -1;
						}
					}
				}
				echo($responce);
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}
	
	if($_GET["req"] == user_id)	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			if(isset($_GET['username']))	{
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id FROM probid_users WHERE username='".$_GET['username']."';");
				while($row = mysqli_fetch_array($tmp))	{
					$responce = $row['id'];
				}
				echo($responce);
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}
	
	// Get auction list (whole list)
	if($_GET["req"] == auct_list)	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			if(isset($_GET[count]) && isset($_GET[amount]))	{
			//AND enddate > now()
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT probid_auctions.id, probid_auctions.itemname, probid_auctions.picpath FROM probid_auctions WHERE probid_auctions.bidstart = 0.01 AND enddate > now() AND probid_auctions.nrbids = 0 AND probid_auctions.auctiontype = 'standard' ORDER BY probid_auctions.startdate ASC LIMIT ".$_GET["count"]." , ".$_GET["amount"].";");
				while($row = mysqli_fetch_array($tmp))	{
					if(!file_exists ($row['picpath']))	{
						if(strlen($row['picpath']) > 8)	{
							$img_src = $file_path."/"; $img_src .= $row['picpath'];
							$extension = getExtension($img_src);
  							$extension = strtolower($extension);
							if($extension=="jpg" || $extension=="jpeg" )	{
								$src = imagecreatefromjpeg($img_src);
							}	else if($extension=="png")	{
								$src = imagecreatefrompng($img_src);
							}	else 	{
								$src = imagecreatefromgif($img_src);
							}
							list($width,$height)=getimagesize($img_src);
							$newwidth=70;
							$newheight=($height/$width)*$newwidth;
							$temp=imagecreatetruecolor($newwidth,$newheight);
							imagecopyresampled($temp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
							$filename = $row['picpath'];
							imagejpeg($temp,$filename,70);
						}
					}
					$responce = $row['id']."&coma".$row['itemname']."&coma".$row['picpath']."&end";
					echo($responce);
				}
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT Count(*) as 'count' FROM probid_auctions WHERE probid_auctions.bidstart = 0.01 AND enddate > now() AND probid_auctions.nrbids = 0 AND probid_auctions.auctiontype = 'standard';");

				$responce2 = "&devider";

				while($row = mysqli_fetch_array($tmp))	{
					$responce2 .= $row['count'];
				}
				echo($responce2);
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}
	
	// Get auction list by some category
	if($_GET["req"] == auct_list_cat && isset ($_GET["cat_id"]) && isset($_GET["count"]) && isset($_GET["amount"]))	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			
			$query = "SELECT probid_auctions.id, probid_auctions.itemname, probid_auctions.picpath FROM probid_auctions WHERE probid_auctions.bidstart = 0.01 AND enddate > now() AND probid_auctions.nrbids = 0 AND probid_auctions.auctiontype = 'standard' AND (";
			$sub_query_begin = "SELECT id, parent FROM probid_categories WHERE ";
			$sub_query = $sub_query_begin."parent = ".$_GET['cat_id'].";";
			$category_array = array();
			$category_array = getChild($_GET['cat_id']);
			foreach($category_array as $p)	{
				$query .= " category = ".$p." OR ";
			}
			$query = substr($query, 0, -4); $query .= ") ORDER BY probid_auctions.startdate ASC LIMIT ".$_GET["count"]." , ".$_GET["amount"].";";
			$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $query);	
			while($row = mysqli_fetch_array($tmp))	{
				if(!file_exists ($row['picpath']))	{
					if(strlen($row['picpath']) > 8)	{
						$img_src = $file_path."/"; $img_src .= $row['picpath'];
						$extension = getExtension($img_src);
  						$extension = strtolower($extension);
						if($extension=="jpg" || $extension=="jpeg" )	{
							$src = imagecreatefromjpeg($img_src);
						}	else if($extension=="png")	{
							$src = imagecreatefrompng($img_src);
						}	else 	{
							$src = imagecreatefromgif($img_src);
						}
						list($width,$height)=getimagesize($img_src);
						$newwidth=70;
						$newheight=($height/$width)*$newwidth;
						$temp=imagecreatetruecolor($newwidth,$newheight);
						imagecopyresampled($temp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
						$filename = $row['picpath'];
						imagejpeg($temp,$filename,70);
					}
				}
				$responce = $row['id']."&coma".$row['itemname']."&coma".$row['picpath']."&end";
				echo($responce);
			}
			
			$responce2 = "&devider";

			$query = "SELECT Count(*) as 'count' FROM probid_auctions WHERE probid_auctions.bidstart = 0.01 AND enddate > now() AND probid_auctions.nrbids = 0 AND probid_auctions.auctiontype = 'standard' AND (";
			$sub_query_begin = "SELECT id, parent FROM probid_categories WHERE ";
			$sub_query = $sub_query_begin."parent = ".$_GET['cat_id'].";";
			$category_array = array();
			$category_array = getChild($_GET['cat_id']);
			foreach($category_array as $p)	{
				$query .= " category = ".$p." OR ";
			}
			$query = substr($query, 0, -4); $query .= ");";
			$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $query);
			while($row = mysqli_fetch_array($tmp))	{
				$responce2 .= $row['count'];
				echo($responce2);
			}
				
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		} 
	} 
	
	if($_GET["req"] == img_add)	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			if(isset($_GET[auct_id]))	{
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT probid_auctions.picpath FROM probid_auctions WHERE probid_auctions.id = ".$_GET["auct_id"].";");
				
				while($row = mysqli_fetch_array($tmp))	{
					$path = split ('/', $row['picpath']);
					$new_pic_path = $path[0]."2/".$path[1];
					if(!file_exists ($new_pic_path))	{
						if(strlen($new_pic_path) > 8)	{
							$img_src = $file_path."/"; $img_src .= $row['picpath'];
							$extension = getExtension($img_src);
  							$extension = strtolower($extension);
							if($extension=="jpg" || $extension=="jpeg" )	{
								$src = imagecreatefromjpeg($img_src);
							}	else if($extension=="png")	{
								$src = imagecreatefrompng($img_src);
							}	else 	{
								$src = imagecreatefromgif($img_src);
							}
							list($width,$height)=getimagesize($img_src);
							$newwidth=200;
							$newheight=($height/$width)*$newwidth;
							$temp=imagecreatetruecolor($newwidth,$newheight);
							imagecopyresampled($temp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
							$filename = $new_pic_path;
							imagejpeg($temp,$filename,75);
						}
					}

					// Gather responce
					$responce = $new_pic_path."&coma";
					echo($responce);
				}
				
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT probid_auction_images.name FROM probid_auction_images WHERE probid_auction_images.auctionid = ".$_GET["auct_id"].";");
				while($row = mysqli_fetch_array($tmp))	{
					$path = split ('/', $row['name']);
					$new_pic_path = $path[0]."2/".$path[1];
					if(!file_exists ($row['name']))	{
						if(strlen($row['name']) > 8)	{
							$img_src = $file_path."/"; $img_src .= $row['name'];
							$extension = getExtension($img_src);
							$extension = strtolower($extension);
							if($extension=="jpg" || $extension=="jpeg" )	{
								$src = imagecreatefromjpeg($img_src);
							}	else if($extension=="png")	{
								$src = imagecreatefrompng($img_src);
							}	else 	{
								$src = imagecreatefromgif($img_src);
							}
							list($width,$height)=getimagesize($img_src);
							$newwidth=200;
							$newheight=($height/$width)*$newwidth;
							$temp=imagecreatetruecolor($newwidth,$newheight);
							imagecopyresampled($temp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
							$filename = $new_pic_path;
							imagejpeg($temp,$filename,70);
						}
					}
					$responce = $new_pic_path."&coma";
					echo($responce);
				}
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}
	
	//Checking auction whether it already has a bid, before opening it and showing to the user
	if($_GET["req"] == check_auct)	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			if(isset($_GET[auct_id]))	{
				$tmp = getSqlNumber("SELECT Count(*) as 'count' FROM probid_auctions WHERE probid_auctions.bidstart = 0.01 AND probid_auctions.nrbids = 0 AND probid_auctions.auctiontype = 'standard' AND probid_auctions.id = ".$_GET["auct_id"].";");
				if($tmp == 1)	{
					echo("exists");
				}	else	{
					echo("not_exists");
				}
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}
	
	//Getting auction details
	if($_GET["req"] == auct)	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			if(isset($_GET[auct_id]))	{
				$tmp = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT probid_auctions.id, probid_auctions.itemname, probid_auctions.description, probid_auctions.picpath, probid_auctions.shipping_details, probid_auctions.pm FROM probid_auctions WHERE probid_auctions.id = ".$_GET["auct_id"].";");
				while($row = mysqli_fetch_array($tmp))	{
					$path = split ('/', $row['picpath']);
					$new_pic_path = $path[0]."2/".$path[1];
					if(!file_exists ($new_pic_path))	{
						if(strlen($new_pic_path) > 8)	{
							$img_src = $file_path."/"; $img_src .= $row['picpath'];
							$extension = getExtension($img_src);
  							$extension = strtolower($extension);
							if($extension=="jpg" || $extension=="jpeg" )	{
								$src = imagecreatefromjpeg($img_src);
							}	else if($extension=="png")	{
								$src = imagecreatefrompng($img_src);
							}	else 	{
								$src = imagecreatefromgif($img_src);
							}
							list($width,$height)=getimagesize($img_src);
							$newwidth=200;
							$newheight=($height/$width)*$newwidth;
							$temp=imagecreatetruecolor($newwidth,$newheight);
							imagecopyresampled($temp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
							$filename = $new_pic_path;
							imagejpeg($temp,$filename,75);
						}
					}

					// Gather responce
					$responce = $row['id']."&coma".$row['itemname']."&coma".$row['description']."&coma".$new_pic_path."&coma".$row['shipping_details']."&coma".$row['pm'];
					echo($responce);
				}
				
					// Change clicks count for this auction
	
					## Check for "in progress", wait ifin progress
					$bid_loop = 1;
					while ($bid_loop == 1) {
						$bid_loop = getSqlField("SELECT bid_in_progress FROM probid_auctions WHERE id='".$_GET[auct_id]."'","bid_in_progress");
					}

					$addInProgressFlag = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET bid_in_progress='1' WHERE id='".$_GET[auct_id]."'");
					$addClicks = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET clicks=clicks+1 WHERE id='".$_GET[auct_id]."'");
					$removeInProgressFlag = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET bid_in_progress='0' WHERE id='".$_GET[auct_id]."'");
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}

	// Getting the amount of auctions by category
	if($_GET["req"] == imei && isset($_GET["imei"]))	{
		$con = ($GLOBALS["___mysqli_ston"] = mysqli_connect($dbhost,  $dbuser,  $dbpass, $dbname));
		if(!$con)	{
			echo "connection";
		}	else	{
			
			$query = "SELECT Count(*) as 'count' FROM probid_mobile_statistics WHERE probid_mobile_statistics.imei LIKE '%".$_GET[imei]."%'";
			$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $query);
			while($row = mysqli_fetch_array($tmp))	{
				$responce = $row['count'];
				if($responce == 0)	{
					$addInProgressFlag = mysqli_query($GLOBALS["___mysqli_ston"], "INSERT INTO probid_mobile_statistics (platform, imei) VALUES ('Android', '".$_GET["imei"]."')");
				}
			}
			
			$tmp = mysqli_query($GLOBALS["___mysqli_ston"], $query);
			while($row = mysqli_fetch_array($tmp))	{
				$responce = $row['count'];
				echo($responce);
			}
			((is_null($___mysqli_res = mysqli_close($con))) ? false : $___mysqli_res);
		}
	}

}
?>
