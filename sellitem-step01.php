<? 
## v5.24 -> apr. 11, 2006
if ( !defined('INCLUDED') ) { die("Access Denied"); }

if ($_SESSION['membersarea']=="Active") { 
?>
<SCRIPT language=javascript>
<!--
var c = new Array();
function x(intParent, intListPos, strCategory, strCategoryID){	c[intParent][intListPos] = new Option(strCategory,strCategoryID); }
<? 
$getcats = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT * FROM probid_categories ORDER BY id ASC');
while ($categ = mysqli_fetch_array($getcats)) { ?>
c[<?=$categ[id];?>] = new Array(); 
<? } 
$i=0;
$j=0;
$getsubcats = mysqli_query($GLOBALS["___mysqli_ston"], 'SELECT * FROM probid_categories WHERE parent!=0 ORDER BY parent ASC, theorder ASC, name ASC');
while ($subcat = mysqli_fetch_array($getsubcats)) { 
$parent[$i]=$subcat[parent];
if ($parent[$i]!=$parent[$i-1]) $j=0;
else $j++;
$i++;
$todisplay = @eregi_replace("&#039;","`",$c_lang[$subcat[id]]);
$todisplay = @eregi_replace('&quot;','"',$todisplay);
?>
x(<?=$subcat[parent];?>,<?=$j;?>,'<? echo $todisplay." ".$subcat[issubcat];?>',"<?=$subcat[id];?>");
<? } ?>
// -->
</SCRIPT>
<SCRIPT language=javascript>
<!--
var blnIE;	//IE? for table highlighting etc...
var	strEmptyString;
var	strCatSelectedString;
var hexEnabled = "#ffffff";
var hexDisabled = "#dddddd";
var hexHighlightOn = "#FFB300";
var hexHighlightOff = "#ffffff";

//arrayLevel 0 for top level, 1 for next level down etc...
function populate(arrayLevel)
{
	//get the value of the selected index (click)
	strOptionValue = (docSelectorArray[arrayLevel].options[docSelectorArray[arrayLevel].selectedIndex].value);
	
	//if value is 0 then is a spacer option - move their choice to the bottom option
	if (strOptionValue == 0){

		if (docSelectorArray[arrayLevel].selectedIndex != 0){
			docSelectorArray[arrayLevel].selectedIndex	= docSelectorArray[arrayLevel].selectedIndex - 1;
			strOptionValue = (docSelectorArray[arrayLevel].options[docSelectorArray[arrayLevel].selectedIndex].value);
		}

		//if they have clicked in an empty table then dont do anything
		else {
			return;	
		}
	}

	//get the option text so we can see if we have a leaf or a branch (" >")
	strOptionText = (docSelectorArray[arrayLevel].options[docSelectorArray[arrayLevel].selectedIndex].text);
	
	//clear lower level select boxes------------------------------------------
	//outerloop through select boxes, starting from the one below this click
	for (i = arrayLevel+1; i < intLevels; i++){

		//inner loop through the number of items in select boxes
		for (j = docSelectorArray[i].length-2; j >= 0; j--){
			docSelectorArray[i].options[j] = null;
		}
		
		//keep box a consistent width
		docSelectorArray[i].options[0] = new Option(strEmptyString, "0");

		//disable and unhighlight
		if (blnIE){
			docSelectorArray[i].style.background = hexDisabled;
			docTableArray[i].style.background = hexHighlightOff;
		}
		docSelectorArray[i].disabled = true;
		//docImageArray[i].src = docImageSrcOffArray[i];
		
	}

	//need another branch (branch or top level)-------------------------------
	if ((strOptionText.indexOf(" >") != -1)){

		//clear any value from the category box
		addlcategory.value = "";

		//disable the next button
		submitButton.disabled = true;
		
		//get the next array and the number of options in it
		intNextArray = docSelectorArray[arrayLevel].options[docSelectorArray[arrayLevel].selectedIndex].value;
				
		//populate the next level if they didn't choose the "Select a Category" option
		if (intNextArray != 0){				

			//enable next arrayLevel
			docSelectorArray[arrayLevel+1].disabled = false;
	
			//IE only
			if (blnIE){
				docSelectorArray[arrayLevel+1].style.background = hexEnabled;		//color of next select box is now enabled
				docTableArray[arrayLevel].style.background = hexHighlightOff;		//this table now unhighlighted			
				docTableArray[arrayLevel+1].style.background = hexHighlightOn;		//next table now highlighted			
				docTableArray[intTableSubmit].style.background = hexHighlightOff;		//Next button table unhighlighted			
			}
			
			//get the number of options for the next level and populate the options
			intNumberOptions = c[intNextArray].length;
			
			//populate array with options
			for (i = 0; i < intNumberOptions; i++){
				docSelectorArray[arrayLevel+1].options[i] = c[intNextArray][i];
			}

			//put a new option at the end of the list so that we have a consistent size on the box
			docSelectorArray[arrayLevel+1].options[intNumberOptions] = new Option(strEmptyString, "0");
			
			//unhighlight any previous selection
			if (docSelectorArray[arrayLevel+1].selectedIndex != -1) {
				docSelectorArray[arrayLevel+1].selectedIndex = -1;
			}
		}
	}
	
	//have reached a leaf, populate the others with end text-------------------------------------------------
	else {
		//get the option for this selection		
		if (strOptionValue != 0) {
			addlcategory.value = strOptionValue;
		}

		//outerloop through select boxes, starting from the one below this click
		for (i = arrayLevel+1; i < intLevels; i++){
			//populate with end text
			docSelectorArray[i].options[0] = new Option(strCatSelectedString, "0");
		}

		//enable and hightlight submit button
		submitButton.disabled = false;	
		
			//IE only
			if (blnIE){
				docTableArray[arrayLevel].style.background = hexHighlightOff;
				docTableArray[intTableSubmit].style.background = hexHighlightOn;		
			}		
	}
}

//arrayLevel 0 for top level, 1 for next level down etc...
function prePopulate(mCat)
{

	//check mCat is sane
	if (mCat.length < 5) return;					//must be at least 2 levels
	if (mCat.length % 5 != 0) return;				//each mCat is 5 chars long
	if (mCat.charAt(mCat.length - 1) != '-') return;//last char of last mCat is '-'

	var mCatArray = mCat.split('-');	//this is now an array of strings, mCatArray[0] = '0011' etc...
	var intMcat							//integer from string mcat '0011' -> 11 etc...
	var strOptionText					//text of option selected to see if we are a leaf or not
	var intNextArray					//next array down from the one we are currently working on
	var intNumberOptions				//number of options for the next level


	
	//outer loop down through the levels
	for (intArrayLevel = 0; intArrayLevel < (mCatArray.length - 1); intArrayLevel++){

		//set integer version of mCat
		intMcat = parseInt(mCatArray[intArrayLevel],10);

		//inner loop - find the matching mCat and set it
		for (intArrayOption = 0; intArrayOption < docSelectorArray[intArrayLevel].length; intArrayOption++){
		
			//found a match
			if (docSelectorArray[intArrayLevel].options[intArrayOption].value == intMcat){

				//so select the index and get the text
				docSelectorArray[intArrayLevel].selectedIndex = intArrayOption;
				strOptionText = (docSelectorArray[intArrayLevel].options[intArrayOption].text);

				//unset the selected image and table highlight for the current level
				//docImageArray[intArrayLevel].src = docImageSrcOffArray[intArrayLevel];
				if (blnIE){
					docTableArray[intArrayLevel].style.background = hexHighlightOff;
				}
				
				//do we need another branch or have we reached the leaf?
				//need another branch (branch or top level)-------------------------------
				if (strOptionText.indexOf(" >") != -1){

					//set the selected image and table highlight for this next level
					//docImageArray[intArrayLevel+1].src = docImageSrcOnArray[intArrayLevel+1];
					if (blnIE){
						docTableArray[intArrayLevel+1].style.background = hexHighlightOn;	
						docSelectorArray[intArrayLevel+1].style.background = hexEnabled;
					}

					//enable this next level
					docSelectorArray[intArrayLevel+1].disabled = false;

					//get the next array
					intNextArray = docSelectorArray[intArrayLevel].options[docSelectorArray[intArrayLevel].selectedIndex].value;

					//get the number of options for the next level and populate the options
					intNumberOptions = c[intNextArray].length;
					for (i = 0; i < intNumberOptions; i++){
						docSelectorArray[intArrayLevel+1].options[i] = c[intNextArray][i];
					}

					//put a new option at the end of the list so that we have a consistent size on the box
					docSelectorArray[intArrayLevel+1].options[intNumberOptions] = new Option(strEmptyString, "0");

					//if we are 1 level from the end then check and select the next option if appropriate
					if (intArrayLevel == 2){

						//get last mCat
						intMcat = parseInt(mCatArray[intArrayLevel+1],10);

						//loop through the last category and find the appropriate category
						for (i = 0; i < intNumberOptions; i++){
							if (docSelectorArray[intArrayLevel+1].options[i].value == intMcat){
								
								//set the selected index and populate the category box
								docSelectorArray[intArrayLevel+1].selectedIndex = i;
								addlcategory.value = intMcat;

								//unset the selected image and table highlight for the current level
								//docImageArray[intArrayLevel+1].src = docImageSrcOffArray[intArrayLevel];

								//enable and highlight submit button
								submitButton.disabled = false;	

								if (blnIE){
									docTableArray[intArrayLevel+1].style.background = hexHighlightOff;								
									docTableArray[intTableSubmit].style.background = hexHighlightOn;									
								}

								//break now to stop looping through the final box
								break;
							}
						}
						//found the final cat so stop here
						return;
					}
				}

				//have reached a leaf, populate the others with end text------------------
				else {
					//populate the category box
					addlcategory.value = docSelectorArray[intArrayLevel].options[intArrayOption].value;

					//outerloop through select boxes, starting from the one above this click
					for (i = intArrayLevel+1; i < intLevels; i++){
						//populate with end text
						docSelectorArray[i].options[0] = new Option(strCatSelectedString, "0");
						docSelectorArray[i].disabled = true;		
					}
	
					//enable and highlight submit button
					submitButton.disabled = false;	
					if (blnIE){					
						docTableArray[intTableSubmit].style.background = hexHighlightOn;
					}
				}
				//break out of this loop, as we have found a match and do not need to continue checking
				break;
			}
		}
	}
}

// -->
</SCRIPT>
 <table width="100%" border="0" cellpadding="2" cellspacing="2"> 
  <tr class="contentfont" align="center"> 
     <td class="c1" width="20%"><?=$lang[sellstep1];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep3];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep4];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep5];?></td> 
     <td class="c4" width="20%"><?=$lang[sellstep6];?></td> 
   </tr> 
</table>
<br>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="errormessage">
		<tr>
			<td><?=$lang[sel_second_cat_msg];?><br>
			<?
			$prefSeller = "N";
			if ($setts['pref_sellers']=="Y") {
				$prefSeller = getSqlField("SELECT preferred_seller FROM probid_users 
				WHERE id='".$_SESSION['memberid']."'","preferred_seller");
				$reduction = (100-$setts['pref_sellers_reduction'])/100;
			}
			
			if ($fee['second_cat_fee']>0&&!freeFees($_SESSION['memberid'])) {
				echo $lang[please_note].
				"<strong>".displayAmount(applyVat(calcReduction($fee['second_cat_fee'],$prefSeller),$_SESSION['memberid']))."</strong>".
				$lang[charge_sec_cat];
				echo "";
			}?></td>
		</tr>
	</table>
	<table width="90%" border="0" cellspacing="1" cellpadding="1" align="center">
	<tr>
		<td class="contentfont">
		<?=$imgarrow;?><? echo "<a href=\"sellitem.php?step=step1&category=".$_REQUEST['category']."&addlcategory=0&listin=".$_REQUEST['listin']."\">".$lang[noaddlcat]."</a>";?>
		</td>
	</tr>
	</table>
<? 
## if store is active and listin!=auction only activate the store categories
$adCatQuery = "";
if ($storeActive&&$_REQUEST['listin']!="auction") {
	$selectedCats = getSqlField("SELECT store_categories FROM probid_users WHERE id='".$_SESSION['memberid']."'","store_categories");
	if ($selectedCats!="") {
		$store_cats_array = substr($selectedCats,0,-1);
		$adCatQuery = " AND id IN ($store_cats_array) ";
	}
}

if ($_REQUEST['listin']=="store") $adCatQuery .= " AND (userid=0 OR userid='".$_SESSION['memberid']."') ";
else $adCatQuery .= " AND userid=0 ";

$getMainCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id,name,issubcat FROM probid_categories WHERE parent=0 ".$adCatQuery." ORDER BY theorder ASC, name ASC");
$cnt = 0;
$catId = array();
$hasSubcat = array();

while ($row=mysqli_fetch_array($getMainCats)) {
	$catId[$cnt]=$row['id'];
	$hasSubcat[$cnt]=$row['issubcat'];
	$cnt++;
}
?> 
 <br>
 <table width="90%" border="0" cellspacing="1" cellpadding="3" align="center" class="border">
	<tr>
		<td class="c4"><b>&nbsp;&nbsp;&raquo;&nbsp;<?=$lang[select_second_cat];?></b></td>
	</tr> 
  <tr class="contentfont"> 
    <td width="100%" class="c2"><TABLE cellSpacing=5 cellPadding=2 border=0 width=100%> 
        <INPUT type="hidden" value="1" name="submitted"> 
        <tr vAlign=top bgColor=#ffffff> 
          <TD align=middle width=50%> <TABLE id=table_0 cellSpacing=5 border=0 width=100%> 
              <tr> 
                <TD> <SELECT class="contentfont" id="selector_0" onchange="populate(0)" size="5" name="selector_0" style="width: 100%; "> 
                    <? 
				for ($i=0; $i<$cnt; $i++) {
					echo "<option value=\"".$catId[$i]."\">".$c_lang[$catId[$i]]." ".$hasSubcat[$i]."</option>";
				} ?> 
                  </SELECT> </TD> 
              </tr> 
            </TABLE></TD> 
          <TD align=middle width=50%> <TABLE id=table_1 cellSpacing=5 border=0 width=100%> 
              <tr> 
                <TD><SELECT class="contentfont" id="selector_1" onchange="populate(1)" size="5" name="selector_1" style="width: 100%; "> 
                    <OPTION value=""></OPTION> 
                  </SELECT></TD> 
              </tr> 
            </TABLE></TD> 
        </tr> 
        <tr vAlign=top bgColor=#ffffff> 
          <TD align=middle> <TABLE id=table_2 cellSpacing=5 border=0 width=100%> 
              <tr> 
                <TD><SELECT class="contentfont" id="selector_2" onchange="populate(2)" size="5" name="selector_2" style="width: 100%; "> 
                    <OPTION value=""></OPTION> 
                  </SELECT></TD> 
              </tr> 
            </TABLE></TD> 
          <TD align=middle> <TABLE id=table_3 cellSpacing=5 border=0 width=100%> 
              <tr> 
                <TD><SELECT class="contentfont" id="selector_3" 
                        onchange="populate(3)" size="5" name="selector_3" style="width: 100%; "> 
                    <OPTION value=""></OPTION> 
                  </SELECT></TD> 
              </tr> 
            </TABLE></TD> 
        </tr> 
        <tr vAlign=top bgColor=#ffffff> 
          <TD align=middle> <TABLE id=table_4 cellSpacing=5 border=0 width=100%> 
              <tr> 
                <TD><SELECT class="contentfont" id="selector_4" onchange="populate(4)" size="5" name="selector_4" style="width: 100%; "> 
                    <OPTION value=""></OPTION> 
                  </SELECT></TD> 
              </tr> 
            </TABLE></TD> 
          <TD align=middle> <TABLE id=table_5 cellSpacing=5 border=0 width=100%> 
              <tr> 
                <TD><SELECT class="contentfont" id="selector_5" 
                        onchange="populate(5)" size="5" name="selector_5" style="width: 100%; "> 
                    <OPTION value=""></OPTION> 
                  </SELECT></TD> 
              </tr> 
            </TABLE></TD> 
        </tr> 
      </TABLE></td> 
  </tr> 
  <? $hasAuctions = getSqlNumber("SELECT id FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."'");
  if ($hasAuctions) { ?> 
  <tr> 
    <td class="c4"><b>&nbsp;&nbsp;&raquo;&nbsp;<?=$lang[orselectprevusedcats];?> </b></td> 
  </tr> 
  <tr class="contentfont"> 
    <td class="c2" style="padding: 10px;"> 
      <? 
	$getPrevMainCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT DISTINCT addlcategory FROM probid_auctions WHERE ownerid='".$_SESSION['memberid']."' AND (addlcategory!='0' OR addlcategory!='') ORDER BY id DESC LIMIT 0,5");
	while ($prevMC = mysqli_fetch_array($getPrevMainCats)) { 
		$nav = "";
		$parent=$prevMC['addlcategory'];
	 	if($parent > 0) {
	 		$croot = $parent;
	 		$cntr = 0;
	 		while ($croot>0) {
	 			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], " SELECT id,parent FROM probid_categories WHERE id='".$croot."' ") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));

				$crw = mysqli_fetch_array($sbcts);
	 			if($cntr == 0) {
	 				$nav = $c_lang[$crw['id']];
	 			} else {
	 				if($parent != $croot) {
	 					$nav = $c_lang[$crw['id']]." > ".$nav;
	 				}
	 			}
	 			$cntr++;
	 			$croot = $crw['parent'];
	 		}
	 	} 
		echo "".$imgarrow."<a href=\"sellitem.php?step=step1&category=".$_REQUEST['category']."&addlcategory=".$prevMC['addlcategory']."&listin=".$_REQUEST['listin']."\">".$nav."</a><br>";
	} ?> </td> 
  </tr> 
  <? } ?> 
  <tr class="contentfont"> 
    <input type="hidden" name="step" value="step1"> 
    <INPUT id="category_ID" type="hidden" name="addlcategory"> 
    <td class="c4" align="right">
    <TABLE id=table_submit cellSpacing=3 border=0> 
      <tr> 
        <TD><INPUT id=submit_button2 type=submit value="<?=$lang[nextstep]?>" name=submit_button2></TD> 
      </tr> 
    </TABLE>
     </td> 
  </tr> 
</table> 
<SCRIPT language=javascript>
<!--
	//IE but not a Mac
	if ((navigator.appVersion.indexOf("MSIE")!= -1) && (navigator.appVersion.indexOf("Mac") == -1)){
		strEmptyString = "                                                     ";
		strCatSelectedString = "Category Selected - Click Next.           ";

		//can we use this function? (forget about early IE browsers)
		blnIE = (document.getElementById) ? true : false;
	}
	else{
		strEmptyString = "--------------------------------------";
		strCatSelectedString =	"----Category Selected - Click Next-----";
	}
	
	//IE only
	if (blnIE){

		//tables to border
		var docTableArray = new Array;
		var intTableSubmit = 0;

		//tables : hard code for now
		docTableArray[0] = document.getElementById('table_0');
		docTableArray[1] = document.getElementById('table_1');
		docTableArray[2] = document.getElementById('table_2');
		docTableArray[3] = document.getElementById('table_3');
		docTableArray[4] = document.getElementById('table_4');
		docTableArray[5] = document.getElementById('table_5');
		docTableArray[6] = document.getElementById('table_submit');						
		intTableSubmit = 6;

		docTableArray[0].style.background = hexHighlightOn;	//highlight the first box for them to select
	}

	var i = 0;

	//selectors to populate
	var docSelectorArray = new Array;
	var intLevels; // = categoryLevel.length; //number of levels in our form
	var selectorLoop = 0;
	
	//submit button
	var submitButton;
	
	//category_ID
	var addlcategory;

	//build small arrays that we can index by id later
	for (i = 0; i < document.sistep1.elements.length; i++){
				
			//cat selectors
			if (document.sistep1.elements[i].name.indexOf("selector_") != -1){
				docSelectorArray[selectorLoop] = document.sistep1.elements[i];
				selectorLoop++;
			}
			
			//categoryID
			else if (document.sistep1.elements[i].name.indexOf("category") != -1){
				addlcategory = document.sistep1.elements[i];
			}
			
			//submit button
			else if (document.sistep1.elements[i].name.indexOf("submit_button2") != -1){
				submitButton = document.sistep1.elements[i];
			}
	}
	
	intLevels = docSelectorArray.length;	//number of levels in our form	
	submitButton.disabled = true;			//disable, will be enabled if appropriate

	for (i = 1; i < selectorLoop; i++){
		docSelectorArray[i].disabled = true;	//disable all these boxes
		if (blnIE){
			docSelectorArray[i].style.background = hexDisabled;	//set an obvious disabled colour
		}
	}
//-->
</SCRIPT> 
<? } else { echo "<p align=center class=errorfont>$lang[err_relogin]</p>"; } ?>