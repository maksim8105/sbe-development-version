<? 
## v5.24 -> may. 03, 2006
if ($_SESSION['membersarea']=="Active"||$_SESSION['accsusp']==2) { 

	if ( !defined('INCLUDED') ) { die("Access Denied"); }

	if (isset($_POST['calculateok']) && is_numeric($_POST['startprice'])) {

		$category_id = getMainCat($_POST['category']);
			
		$category_id = (getSqlNumber("SELECT category_id FROM probid_fees WHERE category_id='".$category_id."'")>0) ? $category_id : 0;
		$fee = getSqlRow ("SELECT * FROM probid_fees WHERE category_id = '".$category_id."'");

		$topay="";
		if ($fee['is_setup_fee']=="Y") $topay.="";
		if ($fee['is_pic_fee']=="Y"&&$fee['val_pic_fee']>0&&@eregi('Y', $_POST['ispics'])) { 
			$topay.="[PICFEE]";
			$isFee['pic_count'] = 1;
		}
		if ($fee['is_hlitem_fee']=="Y"&&$fee['val_hlitem_fee']>0&&@eregi('Y', $_POST['ishlitem'])) {
			$topay.="[HLITEMFEE]";
			$isFee['hl']="Y";
		} 
		if ($fee['is_bolditem_fee']=="Y"&&$fee['val_bolditem_fee']>0&&@eregi('Y', $_POST['isbolditem'])) {
			$topay.="[BOLDFEE]";
			$isFee['bold']="Y";
		} 
		if ($fee['is_hpfeat_fee']=="Y"&&$fee['val_hpfeat_fee']>0&&@eregi('Y', $_POST['ishpfeat'])) {
			$topay.="[HPFEATFEE]";
			$isFee['hpfeat']="Y";
		} 
		if ($fee['is_catfeat_fee']=="Y"&&$fee['val_catfeat_fee']>0&&@eregi('Y', $_POST['iscatfeat'])) {
			$topay.="[CATFEATFEE]";
			$isFee['catfeat']="Y";
		} 
		if ($fee['is_rp_fee']=="Y"&&$fee['val_rp_fee']>0&&@eregi('Y', $_POST['isreserve'])) {
			$topay.="[RPFEE]";
			$isFee['rp']="Y";
		} 
		if ($fee['bin_fee']>0&&@eregi('Y', $_POST['isbuyout'])) {
			$topay.="[BNFEE]";
			$isFee['bin']="Y";
		}
		if ($fee['custom_st_fee']>0&&@eregi('Y', $_POST['iscustomst'])) {
			$topay.="[CUSTOMSTFEE]";
			$isFee['customst']="Y";
		}
		if ($fee['second_cat_fee']>0&&@eregi('Y', $_POST['issecondcat'])) {
			$topay.="[SCFEE]";
			$isFee['secondcat']="Y";
		} 
		if ($fee['videofile_fee']>0&&@eregi('Y', $_POST['ismovie'])) {
			$topay.="[VIDEOFEE]";
			$isFee['videofee']="Y";
		} 
		
		$payAmount = setupFee($_REQUEST['startprice'],$_POST['currency'],0,$isFee,FALSE,TRUE,"",$_REQUEST['category']);
  	} ?>

<table border="0" cellpadding="4" cellspacing="4" align="center" class="border">
   <tr class="c1">
      <td colspan=2 align="center"><b>
         <?=$lang[feescalculator]?>
         </b></td>
   </tr>
   <? if (isset($_POST['calculateok'])) { ?>
	<tr>
		<td colspan=2 align="center" class="c4" height="50"><? 
			if ($_REQUEST['startprice']>0) {
				echo "<p align=center><strong>[ $lang[feescalcresulted]: ".displayAmount($payAmount, $setts['currency'], "YES")." ]</strong></p>";
			} else echo "<p align=center>$lang[feescalcerror]</p>"; ?></td>
	</tr>
   <? } ?>
   <form action="membersarea.php?page=feescalculator" method="post">
      <tr valign="top" class="c3">
         <td nowrap class="contentfont"><?=$lang[startprice];?></td>
         <td class="contentfont" width="100%"><input name="startprice" type="text" class="contentfont" id="startprice" value="<?=$_REQUEST['startprice'];?>" size="8" />
				<? $getCurrencies=mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_currencies"); ?>
	         <select name="currency">
   	         <? 
					$defCurrency = (!empty($_POST['currency'])) ? $_POST['currency'] : $setts['currency'];
					while ($currencyList=mysqli_fetch_array($getCurrencies)) {  
						echo "<option value=\"".$currencyList['symbol']."\" ".(($defCurrency == $currencyList['symbol'])?"selected":"").">".$currencyList['symbol']." ".$currencyList['caption']."</option>"; 
		  			} ?>
            </select>
			</td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[maincat_parent];?></td>
         <td class="contentfont"><select name="category" class="contentfont">
               <option value="0" selected="selected">--
               <?=$lang[general];?>
               --
               <?
					reset($cat_array);
					$getMainCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE parent=0 AND userid=0");
					while ($mainCat = mysqli_fetch_array($getMainCats)) {
						echo '<option value="'.$mainCat['id'].'" '.(($_REQUEST['category'] ==$mainCat['id'])?"selected":"").'>'.$mainCat['name'].'</option>';
					} 
					?>
            </select></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[listaddcat];?> </td>
         <td class="contentfont"><input name="issecondcat" type="radio" value="Y" checked />
            <?=$lang[yes];?>
            <input name="issecondcat" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['issecondcat'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[addbuyout];?> </td>
         <td class="contentfont"><input name="isbuyout" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="isbuyout" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['isbuyout'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[addreserve];?> </td>
         <td class="contentfont"><input name="isreserve" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="isreserve" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['isreserve'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[uploadaddpics];?> </td>
         <td class="contentfont"><input name="ispics" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="ispics" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['ispics'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[uploadmovie];?> </td>
         <td class="contentfont"><input name="ismovie" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="ismovie" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['ismovie'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[featonhp];?> </td>
         <td class="contentfont"><input name="ishpfeat" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="ishpfeat" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['ishpfeat'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[featoncatp];?> </td>
         <td class="contentfont"><input name="iscatfeat" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="iscatfeat" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['iscatfeat'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[highlightitem];?> </td>
         <td class="contentfont"><input name="ishlitem" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="ishlitem" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['ishlitem'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[bolditemprev];?> </td>
         <td class="contentfont"><input name="isbolditem" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="isbolditem" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['isbolditem'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr valign="top" class="c2">
         <td nowrap class="contentfont"><?=$lang[usecustomstendtimes];?> </td>
         <td class="contentfont"><input name="iscustomst" type="radio" value="Y" checked="checked" />
            <?=$lang[yes];?>
            <input name="iscustomst" type="radio" value="N" <? echo (!@eregi('Y', $_REQUEST['iscustomst'])) ? 'checked' : ''; ?> />
            <?=$lang[no];?></td>
      </tr>
      <tr>
         <td colspan="2" align="center" class="c4"><input name="calculateok" type="submit" id="calculateok" value="<?=$lang[calculate]?>"></td>
      </tr>
   </form>
</table>
<? } else { echo "$lang[err_relogin]"; } ?>
