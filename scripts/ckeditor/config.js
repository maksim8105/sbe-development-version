/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	  config.width = '90%';
    config.toolbar_Basic = [ [ 'Source', '-', 'Bold', 'Italic', 'Color' ] ];
    config.toolbar_Full = [
    ['Source','-','Save','NewPage','Preview','-','Templates'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['Link','Unlink','Anchor'],
    ['Styles','Format','Font','FontSize'],
    ]; 	
};
