<?
## v5.24 -> apr. 04, 2006
session_start();
include_once ("config/config.php");
include_once ("config/lang/$setts[default_lang]/site.lang");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>
<?=$setts['sitename'];?> - <?=$lang[subm_answ];?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lang[codepage];?>">
<link href="themes/<?=$setts['default_theme'];?>/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
-->
</style>
<script language="JavaScript"><!--
function copyForm() {
    opener.document.hiddenForm.content.value = document.popupForm.content.value;
    opener.document.hiddenForm.pqAnswer.value = document.popupForm.pqAnswer.value;
    opener.document.hiddenForm.id.value = document.popupForm.id.value;
    opener.document.hiddenForm.answerid.value = document.popupForm.answerid.value;
    opener.document.hiddenForm.submit();
    window.close();
    return false;
}
//--></SCRIPT>
</head>
<body>
<table border="0" width="100%" cellpadding="2" cellspacing="2" class="border contentfont">
   <form name="popupForm" onSubmit="return copyForm()">
		<input type="hidden" name="id" value="<?=$_GET['id'];?>">
		<input type="hidden" name="answerid" value="<?=$_GET['answerid'];?>">		
		<input type="hidden" name="pqAnswer" value="yes">		
      <tr class="c3">
         <td><strong><?=$lang[subm_answ];?></strong>:</td>
			<? 
			$content = getSqlField("SELECT content FROM probid_public_msg WHERE answerid='".$_GET['answerid']."'","content"); 
			$content = (!@eregi("n/a",$content)) ? $content : ""; ?>
         <td><textarea name="content" style="width:400px; height: 60px;"><?=$content; ?></textarea></td>
      </tr>
      <tr>
         <td colspan="2" align="center" class="c4"><input type="button" value="Proceed" onClick="copyForm()"></td>
      </tr>
   </form>
</table>
</body>
</html>
