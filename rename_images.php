<?
##################################################
## ADDON FOR v5.25 TO FIX IMAGE DISPLAY ISSUES.	##
##################################################
include ('config/config.php');

$img_directory = 'uplimg/';
$imgDir=opendir($img_directory);

$changed = 0;

while ($file = readdir($imgDir)) {
	if($file != '..' && $file !='.' && $file !='' && $file !='index.htm') {
		$file_array = explode(".",$file);
	
		$nb_array = count($file_array);
		$ext_cnt = count($file_array) - 1;
	
		$extension = ($nb_array<=1) ? '' : $file_array[$ext_cnt];

		$extension = trim($extension);
		if (empty($extension)) {
			$oldName = $img_directory.$file;
			$newName = $img_directory.$file.'.img';
			rename($oldName, $newName );
			$updateTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auctions SET picpath='".$newName."' WHERE picpath LIKE '%".$oldName."%'");
			$updateAddTable = mysqli_query($GLOBALS["___mysqli_ston"], "UPDATE probid_auction_images SET name='".$newName."' WHERE name LIKE '%".$oldName."%'");
			$changed ++;
		}
	}
}

closedir($imgDir);
clearstatcache(); 

echo "Operation Completed. $changed files altered.";
?>