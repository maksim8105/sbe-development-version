<?
## v5.25 -> jun. 05, 2006

session_start();
if ($_SESSION['membersarea']!="Active"&&$_SESSION['accsusp']!=2) {
	echo "<script>document.location.href='login.php'</script>";
} else {

	include_once ("config/config.php");
	
	include ("themes/".$setts['default_theme']."/header.php");
	
   echo '<table width="100%" border="0" cellpadding="4" cellspacing="4" class="border"> ';
	echo '	<tr> ';
	echo '		<td colspan="2" align="center" class="c1">'.$lang[store_setup_page].' - '.$lang[catsmanagement].'</td> ';
	echo '	</tr> ';
	echo '	<tr class="c2"> ';
	echo '		<td class="contentfont">	';
		
	$link = "membersarea.php?page=cats_management";
		
	// As we are running a script to REMEMBER if you make any adjustments to the cetgories, we may as move this server hog here too !
	// Start update plain cetgories table
	if (@!$open_file = fopen ("config/lang/".$setts['default_lang']."/catsarray.php","w")) {
		echo "$lang[gencatsmsg1] '".$setts['default_lang']."/catsarray.php', $lang[gencatsmsg2]";
		exit;
	}
		
	$cat_array = array ();
	
	$getcats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories ORDER BY name");
	$get_cat_count = mysqli_num_rows($getcats);
	while ($crow = mysqli_fetch_array($getcats)) {
		$croot = $crow['id'];
		$catname = "";
		$cntr = 0;
		$ct=0;
		$number++;
		while ($croot>0) {
			$sbcts = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT * FROM probid_categories WHERE id='$croot' ORDER BY name") or die(((is_object($GLOBALS["___mysqli_ston"])) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));	
			$crw = mysqli_fetch_array($sbcts);
			if($cntr == 0) {
				$catid[$ct] = $crw[id];
				$catname = $crw[name];
				$ct++;
			} else {
				if($crw['parent'] != $croot) {
					$catid[$ct] = $crw[id];
					$catname = $crw[name]." : ".$catname;
					$ct++;
				}
			}
			$cntr++;
			$croot = $crw['parent'];
		}
		
		$cat_array[$catid[0]] = array( remSpecialChars($catname), $crow['userid']) ;
	}
	
	asort($cat_array);
	

	(string) $newCatVar = NULL;
	
	$newCatVar = "<?\n";
	
	if (count($cat_array) > 0) {
		$newCatVar .='$cat_array = array (';
		while (list($catid, $cat_array_details) = each ($cat_array)){
			list($catname, $userid) = $cat_array_details;
			$newCatVar .= '"'.$catid.'" => array ("'.remSpecialChars($catname).'", '.$userid.'), ';
		}
		
		$newCatVar = substr($newCatVar,0,-2);
	
		$newCatVar .= "); ";
	}
	
	$newCatVar .= "\n ?>";

	fputs($open_file,$newCatVar); 	
	fclose($open_file);
	
	if (@!$open_file = fopen ("config/lang/".$setts['default_lang']."/category.lang","w"))
	{
		echo $lang[gencatsmsg3];
		exit;
	}
	
	$category_lang .= "<?\n";
	
	$getCats = mysqli_query($GLOBALS["___mysqli_ston"], "SELECT id, name FROM probid_categories ORDER BY name");
	while ($cat=mysqli_fetch_array($getCats)) { 
		$category_lang .= '$c_lang';
		$category_lang .= '[';
		$category_lang .= $cat[id];
		$category_lang .= ']="';
		$category_lang .= remSpecialChars($cat[name]);
		$category_lang .= '";';
		$category_lang .= "\n";
	}
	$category_lang .= "?>";
	
	fputs($open_file,$category_lang); 	
	fclose($open_file);
	
	// End update categories plain table
	echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
	echo "<p class=contentfont align=center>$lang[gencatsmsg4]</p><p>&nbsp;</p>";
	echo "<script>window.setTimeout('changeurl();',2500); function changeurl(){window.location='$link'}</script>";
	echo "</td></tr></table>";

	echo '		</td> ';
	echo '	</tr> ';
	echo '</table> ';
	include ("themes/".$setts['default_theme']."/footer.php"); 
	$_SESSION['cats']="0";
} ?> 
